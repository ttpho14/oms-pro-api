package database

import (
	"../setting"
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"
)

//var Client *mongo.Client
//var db = "test"
var Database *mongo.Database
var Client *mongo.Client

func ConnectionInIt() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// Set client options
	//clientOptions := options.Client().ApplyURI(os.Getenv("MongodbUri"))
	clientOptions := options.Client().ApplyURI(setting.MongodbUri)
	// Connect to MongoDB
	client, err := mongo.Connect(ctx, clientOptions)

	if err != nil {
		fmt.Println("ERROR mongo connect: " + err.Error())
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		fmt.Println("ERROR client ping: " + err.Error())
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")

	Client = client

	//Database = client.Database(os.Getenv("Database"))
	Database = Client.Database(setting.Database)

}
