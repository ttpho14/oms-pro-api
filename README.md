# **OMS Pro API**

## **_Version 4.8_**
- ##### Add value to fields in Web hook - Milestone validate current sales order status.
1. Sales order status can't be one of these : CANCELLED, DELIVERED, SHIPPED, RETURNED.
- ##### Fix web hook error message. 
1. Create function to reduce error response process in web hook.
2. Error message in queue will be equal to response message.
- ##### Add shipping detail to Order cancellation and Return Order.

## **_Version 4.9_**
- ##### Fix response format for Security Role table.
- ##### Dashboard : Convert and Group by GMT+8 time.

## **_Version 5.0_**
- ##### Fix Update User
1. Can update the email. But can't update to existing email.
2. Log New password when create, change or reset.
3. API for User Password : GET ..../UserPassword
- ##### Turn on Update Address API
- ##### Include Security Detail to Security Role 
- ##### Fix Validation for detail of Sales Order , Cancel and Return order

## **_Version 5.1_**
- ##### Fix when Order status changed to "Shipped" >>>> Decrease Order quantity in Inventory status
- ##### When triggered to security role >>> Add function if it is missing or a new function.
- ##### When create order cancel. Add the whole Order Cancel to Json of Queue from OMS to SIA and YCH.
- ##### Update Return status >>>> Change API for updating.

## **_Version 5.2_**
- ##### Update Security Role >>>>>> Detail
- ##### Web hook order pack >>>>> Include order pack table to sales order response

## **_Version 5.3_**
- ##### Check if there is new function for security detail >>>> Insert New Security Detail within security key of Security Role.
- ##### Validation for Security Role and Security Detail when Create and Update 
- ##### Add field "image_url" to sales order detail, order cancellation detail, return detail models.
- ##### Add field "change_password" (boolean) to User model
- ##### Set "change_password" = true when reset password.
- ##### Web hook Milestone API : When sales order detail have either cancel_quantity or return_quantity greater than 0. >>>>>> DO NOT UPDATE STATUS FOR THIS LINE. Others line would be updated as normal.

## **_Version 5.4_**
- ##### When change password , set "change_password" field to false 
- ##### Update Order Pack : Check Order Number, and line number with insensitive case
- ##### Response for update return status >>>>> Return with details.
- ##### Fix Status filter for Cancel Order List 

## **_Version 5.5_**
- ##### Web hook API >>>>> Up case to compare with order number (Milestone , Order Pack, Update Sales Order Label URL)
- ##### Milestone API >>>>>>> Source Object ID to Queue will be the first order_no
- ##### Create Return status table >>>>>> Just like sales order status
- ##### Change logic for Return API :
1. returnObj.TotalAfterTax += sum ( returnDetail.PriceAfterTax * returnDetail.Quantity + returnDetail.FreightAmount - returnDetail.LineDiscountAmount)
2. Recount Return Payment Amount

## **_Version 5.6_**
- ##### Web hook API - Order Pack : When Order Item status == Accepted, Rejected >>>>>>> Update for Return Order detail.

## **_Version 5.7_**
- ##### Add default value "country" to security detail value
- ##### Add 2 parameters when GET Sales Order List and count : "from_date" and "to_date" , filter with "order_date field"
- ##### Sales Order , Return and Cancel Order  : GET API list will sort by doc_date

## **_Version 5.8_**
- ##### Fix security role and detail.

## **_Version 5.9_**
- ##### Create Integration Service API 
1. GET List of Integration Service : ......./IntegrationService
2. POST command to Integration Service : ....../IntegrationService/command (start, stop ,restart,...)
- ##### Export Sales Order
- ##### API Update milestone: 
1. If sales channel id = setting.SalesChannelPumaCom >>>>> Update tracking number in webhook milestone
2. if channel = puma . com && status == PACKED  >> insert queue: fromApp = OMS, toApp =CyberSource, sourcetype = captures, jsonData = Sales Order with details
- ##### Create sales order >>> If sales Channel name = setting.SalesChannelPumaCom >>>>> Lookup in application SFCC ,else lookup in SIA Application
- ##### Create Cancel Order 
1. if channel id = puma. com >>>>>>>  Insert Queue from OMS to Email , source type = order_cancellation, json data = all order cancellation response
2. if channel id = puma. com  && all line orderqty = cancel qty >> insert queue: fromApp = OMS, toApp = CYBERSOURCE, sourcetype = reversals
- ##### Insert Inventory Log when update Inventory Status
- ##### Add fields to Sales Order exports
  - `OrderNo`       
  -	`OrderDate`      
  -	`Customer`       
  - `Country`        
  -	`SalesChannel`   
  -	`Status`         
  -	`OrderAmount`    
  -	`EAN`            
  -	`Description`    
  -	`Size`           
  -	`Color`          
  -	`BasePrice`      
  -	`OrderQuantity`  
  -	`CancelQuantity` 
  -	`ReturnQuantity` 
  -	`DiscountAmount` 
  -	`ShippingFee`    
  -	`SubTotal`       
  
## **_Version 6.0_**
##### **_1. Queue status_** (Inactive)
- `PROCESSING` : error message = ''  &&  status = false 
- `SUCCESS` : status = true  >> Success
- `FAIL` : status = False + error message <> ''  >> Fail
##### **_2. Fix Sales Order Export_**
- Add match
- Fix order amount <<<<< total_after_tax
##### **_3. Add Params to GET Sales Order List_**
- `email` : filter by email of header
- `EAN` : product code of sales order details.

##### **_4. Add Value 'integration_service' to Security Role_**

##### **_5.Hard code "Reason Code" and "Reason Description" for Warehouse shortage_**
- `ReasonCode` = 200
- `ReasonDescription` = Warehouse Shortage 
- Remove "cancel_request" field in `OrderCancellationRequest` Model
- Replace logic to insert queue for SIA and YCH : 
    - cancelRequest = true >>>>> reason_id = 200

## **_Version 6.1_**
##### **_1. Integration Log_**
- API : 
    - CreateIntegrationLog : **POST** ......../Integration
    - GetIntegrationLog : **GET** .........../Integration 
        - Params : 
            - `from_date`
            - `to_date`
            - `limit`
            - `page`
            - `service_name`
            - `function_name`
##### **_2. Add parameter to Get Sales Order count_**
- `email`
- `ean`
##### **_3. Add UOM field to Product , Detail of Sales , Cancel ,Return Order_**
##### **_4. New param filter for queue list "web_status" (also for count)_**
- `PROCESSING` : error message = ''  &&  status = false 
- `SUCCESS` : status = true  >> Success
- `FAIL` : status = False + error message <> ''  >> Fail
##### **_5. Remove insert queue for SIA when change order status = DELIVERED || HANDOVER_**

## **_Version 6.2_**
##### **_1. Add Integration Log for Web hook_**
`service.CreateInventoryLog("OMS.Pro."+enum.AppIdYCH,"UpdateWarehouseCancelOrder",string(body),"","")`

##### **_2. Change Logic to calculate return total_after_tax_**
- `returnObj.TotalAfterTax += returnDetail.PriceAfterTax*returnDetail.Quantity + returnDetail.FreightAmount`

##### **_3. Add API Log_**
- GET List : `......./ApiLog`
- GET Count of list : `......./ApiLog/count`

## **_Version 6.3_**
##### **_1. Add application id field into API Log_**
- Get Application by token when validate token.

##### **_2. Turn OFF API log_**

## **_Version 6.4_**
##### **_1. Insert queue for SFCC_**
- If **Sales Channel ID = "setting.SalesChannelPumaCom" && (Order Status = SHIPPED || DELIVERED)** >>>>> Insert queue **OMS >> SFCC**
##### **_2. Check if User Is Active >>> LOGIN, CHANGE PASSWORD, RESET PASSWORD_**

##### **_3. Create Return order >> If Sales Channel Id = "setting.SalesChannelPumaCom" >>> QUEUE OMS >> SFCC_**

##### **_4. Add Phone no to Sales Order Update Request_**

##### **_5. Fix bug when create new Order Cancellation, it does not update the order status of Sales Order if cancel all line_**

## **_Version 6.5_**
##### **_1. Add filter for  GET inventory log_**
- `product_id`
- `site_id`

##### **_2. Add count for  GET inventory log_**
##### **_3. Update Sales Order >>> Shipping Detail >>> shipping_label_url_**
- If missing >> add new line and update shippinglabel_url
##### **_4. Empty Error message when create new Integration File in queue list_**
##### **_5. Param for integration log_**
- `log_1`   >>>>>>>
- `log_2`      >>>>>>>>> LIKE operation 
- `log_3`   >>>>>>>

## **_Version 6.6_**
##### **_1. Insert Queue for several cases_**
- Sales Order: If channel = setting.SalesChannelPumaCom
    - If status =PACKED > queue  to CyberSource (captures)
- Return: If channel=PUMA.com
    - if status=RETURN
        > Insert Queue SFCC
    - if status=RECEIVED
        > Insert Queue SFCC
    - if status=REJECTED
        > Insert Queue SFCC
    - if status=ACCEPTED
        > Insert Queue SFCC
        _> Insert Queue CyberSource_ (refunds)
    - if status=REFUNDED
        > Insert Queue SFCC

## **_Version 6.7_**
##### **_1. Add TransactionId to Sales Order Payment_**
##### **_2. Shortage json when GET Sales Order list_**
##### **_3. Add 2 fields in to Queue model_**
- `retry_times`
- `retry_days`
##### **_4. Comment code when update order status = PACKED_**
- ~~Queue OMS >>> SIA~~ (removed)
##### **_5. Add YchToken._**
##### **_6. Add condition when Webhook received order status to change equal to "PICKED"_**
> if (sales channel == setting.SalesChannelZalora || setting.SalesChannelLazada) >>> Insert queue to SIA

## **_Version 6_**
##### **_1. Add API to recalculate the Order quantity_**
##### **_2. Basic User Logout API_**
##### **_3. Fix Recalculate Inventory quantity_**
- `On-Order Quantity  = sum(SalesOrderDetail.OrderQty - SalesOrderDetail.CancelQty - SalesOrderDetail.ReturnQty) 
where Status in (New, Processing, Picked, Packed)`
- `Available = YCH Onhand - OnOrder Qty`
##### **_4. Fix create customer when create Sales Order_**
- If email = `noreply@rciapac.com` >> Check SalesOrder.ShippingAddress.SPhone >> Create Customer with email : `[phoneNo]@puma.com`
##### **_5. Assign Country ,Site, Currency Key to Sales Order when have the ID_**
- `salesOrder.CountryKey`
- `salesOrder.SiteKey`
- `salesOrder.CurrencyKey`
##### **_6. UpdateMilestone_**
- If SalesChannel == "setting.SalesChannelPumaCom" >>> Allow changing status.
- If OrderStatus == "HANDOVER" >>>> OrderStatus = "HANDOVER"
##### **_7. Update response json after create return order_**
##### **_8. Update Milstone API_**
- API UpdateMileStone, if order status =  Handover/Shipped >> Update shipping status = SHIPPED
- API UpdateMileStone, if order status = DELIVERED/HANDOVER/SHIPPED >> Update doc line status for details : DELIVERED/HANDOVER/SHIPPED
##### **_9. Create new API_**
 - Sales Order Payment Capture : `POST` `.........../SalesOrderPaymentCapture`
 - Sales Order Payment Refund : `POST` `............/SalesOrderPaymentRefund`
 - Sales Order Payment Reverse : `POST` `.........../SalesOrderPaymentReverse`
##### **_10. Remove duplicate Queue from OMS to SFCC and Email_**
- ~~From `OMS` to `SFCC` : return_order (removed)~~
- ~~From `OMS` to `Email` : return_order (removed)~~
- From `OMS` to `SFCC` : `return`
- From `OMS` to `Email` : `return`
##### **_11. New API : User License_**
- When user login without license limit exceed (NUMBER_OF_LICENSE in .env file) >>>> Write 1 line into User License collection
- When user logout, delete one line in User License with exact `email`.
- GET All User License : `GET` `....../UserLicense`
- GET Count all User License : `GET` `....../UserLicense/count`
- DELETE User License : `DELETE` `....../UserLicense/{_id}`
##### **_12. Issue when create Sales Order_**
- If `Customer Name` is missing, put “N/A”
- If `Phone No (salesOrder.ShippingAddress.PhoneNo)` is missing, put “N/A”
##### **_13. When create customer by sales order_**
- If Sales Channel Id == "setting.SalesChannelPumaCom" 
    > customer.DocumentId = sales.RefNo1
##### **_14. Fix Queue for SAP and CyberSource_**
- Insert SAP queue when order is DELIVERED and not flag for SAP.
- Insert queue for CyberSouce when partial cancel
##### **_15. Copy Sales Order Payment to Return and Cancellation Order Payment_**
##### **_16. Update Return Order when update Order Pack_**
##### **_17. If cancel all the lines >> Insert Queue from OMS to CyberSource : reversals_**
##### **_18. Order Pack update Return Order Status_**
##### **_19. Update Return Order status when create Sales Order Payment Refund_**
- Add field `ReturnKey` into `SalesOrderPaymentRefundRequest` model
- `SalesOrder.OrderStatus` = `RETURNED`
- `SalesOrder.PaymentStatus` = `REFUNDED`;
- `Return.DocStatus` = `ACCEPTED`
- `Return.PaymentStatus` = `REFUNDED`;
##### **_20. Update Order Cancellation status when create Sales Order Payment Reverse_**
- Add field `OrderCancellationKey` into `SalesOrderPaymentReverseRequest` model
##### **_21. Get Product by Product ID and by Key_**
- Get Product with new field
    + `return_qty`
    + `cancel_qty`
- Get InventoryStatus with new field
    + `new_qty`
    + `processing_qty`
    + `picked_qty`
    + `packed_qty`
    + `return_qty`
    + `cancel_qty`
##### **_22. Validation for Site_**
- Check Site ID exists
##### **_23. Global search_**
- Sales Order : `doc_num`
- Return  : `doc_num`
- Order Cancellation : `doc_num`
- Product :`product_id`
- Customer : `email`, `phone_no`
##### **_24. Fix Bug : Assign exact doc line status and pass to queue_**
##### **_25. Create Sales Order Payment Refund >>> Remove update Return doc status to ACCEPTED_**
##### **_26. Enable User login with number of License_**
##### **_27. Fix Return and Order Cancellation >> Total After Tax_**
- `orderCancellation.TotalAfterTax` += `orderCancellationDetail.PriceAfterTax` x `orderCancellationDetail.Quantity` - `orderCancellationDetail.LineDiscountAmount`
- `returnObj.TotalAfterTax` += `returnDetail.PriceAfterTax` x `returnDetail.Quantity` + `returnDetail.FreightAmount` - `returnDetail.LineDiscountAmount`
##### **_28. Fix Return Quantity in each Return Header_**
##### **_29. Fix Return and Cancel doc date when create New one_**
- `DocDate` = currentTime
##### **_30. Lookup Reason Key by Reason ID if missing_**
##### **_31. Export Return and Cancel order_**
- `.........../Return/export`
- `.........../OrderCancellation/export`
##### **_32. Reduce fields when get Return and Cancel Order, include count inside response when get All_**
##### **_33. Add queue when Create Return_**
- If salesChannel = "setting.SalesChannelPumaCom" >> Add queue OMS to YCH , source_object_type = "return"
##### **_34. Include count inside get all Order Cancellation and Return Order_**
##### **_35. Add fields SID to Sales Order And Return model_**
##### **_36. Make Sales Channel ID to constant_**
- `setting.SalesChannelPumaCom` >>> "PUMA.COM"
- `setting.SalesChannelZalora` >>> "ZALORA"
- `setting.SalesChannelLazada` >>> "LAZADA"
##### **_37. Fix Total after tax when create Return Order_**
- ~~returnObj.TotalAfterTax = returnObj.TotalAfterTax + returnObj.FreightAmount + returnObj.TaxAmount - returnObj.DiscountAmount~~
- `returnObj.TotalAfterTax` = `returnObj.TotalAfterTax` - `returnObj.DiscountAmount`
##### **_38. Add Sales Order Payment Capture,Refund,Reverse to Get Order Cancellation by Key,DocNum_**
##### **_39. Add Error Code to Error response (big change)_**
## **_Version 7_**
##### **_1. Fix Order Pack validation_**
- If Order item exists >>> Handle Return process
- If Packing Package Exits >>> handle Sales Order Process
- If OrderItem.Item.Status == "ACCEPT" >>>> "ACCEPTED", "REJECT" >> "REJECTED"
- If All Return detail has the same status >> Update Return header status. (ACCEPTED, REJECTED)
##### **_2. Add Order Date field when get all Order Cancellation and Return_**
##### **_3. Fix Export return and cancel order with param (any case)_**
##### **_4. Add `Total Order Amount` to Get Cancel and Return by Key and Doc Num API_**
##### **_5. Add `Freight Amount` input field when create new Cancel and Return order_**
##### **_6. Fix Order Pack update_**
- Move Update return status outside of if else statement
##### **_7. Add line status after base price field into Sales Order, Cancel and Return Order export API_**
- `{"line_status", detailStr + ".doc_line_status"}`
##### **_8. Reject Sales Order with condition_**
- `Creating` : New Order has Doc Date little than `2020-10-01 GMT+00:00` >>> `REJECT`
- `Update Sales Order from SIA` :
    - Update `SHIPPED` order >>> If current status is not `PACKED` >>> `REJECT`
    - Update `DELIVERED` order >>> If current status is not `SHIPPED` >>> `REJECT`
##### **_9. Add 2 fields into Return Order_**
- `Pickup date` , `Instruction`




