package setting

import (
	"os"
	"strconv"

	"github.com/subosito/gotenv"
	"gopkg.in/oauth2.v3"
)

var (
	BasePath            string
	MongodbUri          string
	Database            string
	ServerPort          string
	ClientDomain        string
	CertificateFilePath string
	PrivateKeyFilePath  string
	OmsClientId         string
	OmsSecretKey        string
	SystemSecretKey     string
	IsHttps             bool
	Key                 string
	NumberOfLicense     int64
	Token               oauth2.TokenInfo
)

const (
	ApplicationTable                     = "application"
	AddressTable                         = "address"
	ApplicationConfigurationTable        = "application_configuration"
	BrandTable                           = "brand"
	BrandRegistrationTable               = "brand_registration"
	BundleTable                          = "bundle"
	BundleDetailTable                    = "bundle_detail"
	CategoryTable                        = "category"
	CategoryDetailTable                  = "category_detail"
	CountryTable                         = "country"
	CurrencyTable                        = "currency"
	CustomerTable                        = "customer"
	CustomerGroupTable                   = "customer_group"
	FreightTable                         = "freight"
	InventoryLogTable                    = "inventory_log"
	InventoryStatusTable                 = "inventory_status"
	IntegrationFileTable                 = "integration_file"
	IntegrationServiceTable              = "integration_service"
	IntegrationLogTable                  = "integration_log"
	OrderCancellationTable               = "order_cancellation"
	OrderCancellationDetailTable         = "order_cancellation_detail"
	OrderCancellationPaymentTable        = "order_cancellation_payment"
	OrderCancellationCommentTable        = "order_cancellation_comment"
	OrderCancellationShippingDetailTable = "order_cancellation_shipping_detail"
	PaymentTypeTable                     = "payment_type"
	PriceListTable                       = "price_list"
	PriceListDetailTable                 = "price_list_detail"
	ProductTable                         = "product"
	ProductGroupTable                    = "product_group"
	ProductMediaTable                    = "product_media"
	ProductVariantTable                  = "product_variant"
	QueueTable                           = "queue"
	ReasonTable                          = "reason"
	RegionTable                          = "region"
	ReturnTable                          = "return"
	ReturnDetailTable                    = "return_detail"
	ReturnPaymentTable                   = "return_payment"
	ReturnCommentTable                   = "return_comment"
	ReturnFreightTable                   = "return_freight"
	ReturnStatusTable                    = "return_status"
	ReturnShippingDetailTable            = "return_shipping_detail"
	SalesChannelTable                    = "sales_channel"
	SalesInvoiceTable                    = "sales_invoice"
	SalesInvoiceDetailTable              = "sales_invoice_detail"
	SalesInvoiceFreightTable             = "sales_invoice_freight"
	SalesOrderTable                      = "sales_order"
	SalesOrderStatusTable                = "sales_order_status"
	SalesOrderCommentTable               = "sales_order_comment"
	SalesOrderPackageTable               = "sales_order_package"
	SalesOrderDetailTable                = "sales_order_detail"
	SalesOrderDiscountTable              = "sales_order_discount"
	SalesOrderFreightTable               = "sales_order_freight"
	SalesOrderPaymentTable               = "sales_order_payment"
	SalesOrderPaymentCaptureTable        = "sales_order_payment_capture"
	SalesOrderPaymentRefundTable         = "sales_order_payment_refund"
	SalesOrderPaymentReverseTable        = "sales_order_payment_reverse"
	SalesOrderShippingDetailTable        = "sales_order_shipping_detail"
	SecurityDetailTable                  = "security_detail"
	SecurityRoleTable                    = "security_role"
	SiteTable                            = "site"
	TaxCodeTable                         = "tax_code"
	TenantTable                          = "tenant"
	TenantSettingTable                   = "tenant_setting"
	UserTable                            = "user"
	UserLicenseTable                     = "user_license"
	UserPasswordTable                    = "user_password"
	UserBrandDetailTable                 = "user_brand_detail"
	UserDefineFieldTable                 = "user_define_field"
	VendorTable                          = "vendor"
	VendorGroupTable                     = "vendor_group"
	OrderPackTable                       = "order_pack"
	OrderPackItemTable                   = "order_pack_item"
	OrderPackPackageTable                = "order_pack"
	ApiLogTable                          = "api_log"
	SalesChannelPumaCom                  = "PUMA.COM"
	SalesChannelZalora                   = "ZALORA"
	SalesChannelLazada                   = "LAZADA"
)

const (
	YchToken            string = "IRRTNABDISWLCSAB"
	DaySecond           int64  = 86400
	DefaultFromDateUnix int64  = 915148800
	DefaultToDateUnix   int64  = 253400616000
	Version             string = "Version 7.9 - 04/11/2020 12:39 PM"
)

func InIt() {
	_ = gotenv.Load()
	Key = os.Getenv("KEY")
	BasePath = os.Getenv("BASE_PATH")
	MongodbUri = os.Getenv("MONGODB_URI")
	Database = os.Getenv("DATABASE")
	ServerPort = os.Getenv("SERVER_PORT")
	ClientDomain = os.Getenv("CLIENT_DOMAIN")
	SystemSecretKey = os.Getenv("SYSTEM_SECRET_KEY")
	OmsClientId = os.Getenv("OMS_CLIENT_ID")
	OmsSecretKey = os.Getenv("OMS_SECRET_KEY")
	CertificateFilePath = os.Getenv("CERTIFICATE_FILE_PATH")
	PrivateKeyFilePath = os.Getenv("PRIVATE_KEY_FILE_PATH")
	IsHttps, _ = strconv.ParseBool(os.Getenv("IS_HTTPS"))

	NumberOfLicenseInt, _ := strconv.Atoi(os.Getenv("NUMBER_OF_LICENSE"))
	NumberOfLicense = int64(NumberOfLicenseInt)
}
