package service

import (
	"../database"
	"../model"
	"../setting"
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"time"
)

func GetCustomerSequenceNumber() (customerNumber string, err error) {
	var customer model.CustomerAutoIncrement
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_ = database.Database.Collection(setting.CustomerTable).FindOne(ctx, bson.M{"_id": "customer_number"}).Decode(&customer)

	if customer.ID == "" {
		_, _ = database.Database.Collection(setting.CustomerTable).InsertOne(ctx, bson.M{"_id": "customer_number","sequence_number":1})
		customer.SequenceNumber = 1
	}

	customerNumber = fmt.Sprintf("%08d", customer.SequenceNumber)

	return customerNumber, nil
}

func UpdateCustomerSequenceNumber() (err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if _, err = database.Database.Collection(setting.CustomerTable).UpdateOne(ctx, bson.M{"_id": "customer_number"}, bson.M{"$inc": bson.M{"sequence_number": 1}}); err != nil {
		return errors.New("Update Sequence number of Customer ERROR - " + err.Error())
	}
	return nil
}

