package service

import (
	"../database"
	"../model"
	"../model/enum"
	"../setting"
	"./helper"
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"gopkg.in/oauth2.v4/errors"
	"time"
)

func HandleSalesOrderDetailStatusChange(docNum string, docLineNum string, newStatus enum.OrderLineStatus) (err error) {
	var (
		salesOrderDetail     model.SalesOrderDetail
		salesOrderDetailColl = database.Database.Collection(setting.SalesOrderDetailTable)
		currentTime          = time.Now()
	)

	newStatus, err = newStatus.IsValid()
	if err != nil {
		return errors.New(fmt.Sprintf("New Order line status '%s' is invalid", newStatus))
	}

	salesOrderDetail, err = GetSalesOrderDetailByDocLineNum(docNum, docLineNum)
	if err != nil {
		return errors.New(fmt.Sprintf("Get Sales Order Detail ERROR - '%s'", err.Error()))
	}

	//b, err := json.Marshal(salesOrderDetail)
	//if err != nil {
	//	return errors.New(fmt.Sprintf("Marshal sales order ERROR - '%s'", err.Error()))
	//}

	//if salesOrderDetail.DocLineStatus == newStatus {
	//	return errors.New(fmt.Sprintf("New Order line status '%s' same with old order line status", newStatus))
	//}

	//if !IsValidOrderLineStatusChange(salesOrderDetail.DocLineStatus, newStatus) {
	//	return errors.New(fmt.Sprintf("Order line status '%s' is not allowed to change to '%s'", salesOrderDetail.DocLineStatus, newStatus))
	//}

	switch newStatus {
	case enum.OrderLineStatusNew:
	case enum.OrderLineStatusOnHold:
	case enum.OrderLineStatusCancelled:
	case enum.OrderLineStatusPicked:
	case enum.OrderLineStatusPacked:
	case enum.OrderLineStatusReadyToShip:
	case enum.OrderLineStatusShipped:
	case enum.OrderLineStatusInTransit:
	case enum.OrderLineStatusHandover:
	case enum.OrderLineStatusDelivered:
	case enum.OrderLineStatusReturned:
	case enum.OrderLineStatusReceived:
	case enum.OrderLineStatusRejected:
	case enum.OrderLineStatusAccepted:
	case enum.OrderLineStatusComplete:
	case enum.OrderLineStatusRefunded:
	case enum.OrderLineStatusError:
	}

	filter := bson.M{"_id": bsonx.ObjectID(salesOrderDetail.ID)}
	update := bson.M{"$set": bson.M{"doc_line_status": newStatus, "updated_date": currentTime}}
	if _, err = salesOrderDetailColl.UpdateOne(context.Background(), filter, update); err != nil {
		return errors.New(fmt.Sprintf("Update Sales order detail status ERROR - " + err.Error()))
	}

	if salesOrderDetail.DocLineStatus.ToCorrectCase() != newStatus {
		if err = CreateSalesOrderStatusChange(salesOrderDetail.DocNum, salesOrderDetail.DocLineNum, string(salesOrderDetail.DocLineStatus.ToCorrectCase()), string(newStatus)); err != nil {
			return errors.New(fmt.Sprintf("Create Sales Order Status change ERROR - " + err.Error()))
		}
	}

	return nil
}

func IsValidOrderLineStatusChange(oldStatus enum.OrderLineStatus, newStatus enum.OrderLineStatus) bool {
	switch oldStatus {
	case "":
		return helper.IsItemExistsInArray([]enum.OrderLineStatus{enum.OrderLineStatusNew}, newStatus)
	case enum.OrderLineStatusNew:
		return helper.IsItemExistsInArray([]enum.OrderLineStatus{enum.OrderLineStatusOnHold, enum.OrderLineStatusPicked, enum.OrderLineStatusPacked}, newStatus)
	case enum.OrderLineStatusOnHold:
		return false
	case enum.OrderLineStatusCancelled:
		return false
	case enum.OrderLineStatusPicked:
		return helper.IsItemExistsInArray([]enum.OrderLineStatus{enum.OrderLineStatusPacked}, newStatus)
	case enum.OrderLineStatusPacked:
		return helper.IsItemExistsInArray([]enum.OrderLineStatus{enum.OrderLineStatusReadyToShip}, newStatus)
	case enum.OrderLineStatusReadyToShip:
		return helper.IsItemExistsInArray([]enum.OrderLineStatus{enum.OrderLineStatusShipped}, newStatus)
	case enum.OrderLineStatusShipped:
		return helper.IsItemExistsInArray([]enum.OrderLineStatus{enum.OrderLineStatusDelivered, enum.OrderLineStatusInTransit}, newStatus)
	case enum.OrderLineStatusInTransit:
		return helper.IsItemExistsInArray([]enum.OrderLineStatus{enum.OrderLineStatusDelivered}, newStatus)
	case enum.OrderLineStatusDelivered:
		return helper.IsItemExistsInArray([]enum.OrderLineStatus{enum.OrderLineStatusReturned}, newStatus)
	case enum.OrderLineStatusReturned:
		return helper.IsItemExistsInArray([]enum.OrderLineStatus{enum.OrderLineStatusReceived}, newStatus)
	case enum.OrderLineStatusReceived:
		return helper.IsItemExistsInArray([]enum.OrderLineStatus{enum.OrderLineStatusAccepted, enum.OrderLineStatusRejected}, newStatus)
	case enum.OrderLineStatusRejected:
		return false
	case enum.OrderLineStatusAccepted:
		return helper.IsItemExistsInArray([]enum.OrderLineStatus{enum.OrderLineStatusRefunded}, newStatus)
	case enum.OrderLineStatusRefunded:
		return false
	default: // Case when Old Status == Closed ===>>> Will return false
		return false
	}
}

func GetSalesOrderDetailByDocLineNum(docNum string, docLineNum string) (sDetail model.SalesOrderDetail, err error) {
	var coll = database.Database.Collection(setting.SalesOrderDetailTable)

	filter := bson.M{"doc_num": docNum, "doc_line_num": docLineNum}
	err = coll.FindOne(context.Background(), filter).Decode(&sDetail)
	if err != nil {
		return model.SalesOrderDetail{}, errors.New("Get Sales Order Detail by Doc line num ERROR - " + err.Error())
	}

	return sDetail, nil
}

func CheckOtherSalesOrderDetailStatus(docNum string, status enum.OrderLineStatus) {
}

func SalesOrderDetailUpdate(filter interface{}, update interface{}) (err error) {
	return nil
}
