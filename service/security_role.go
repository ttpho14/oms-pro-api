package service

import (
	"../database"
	"../model"
	"../model/enum"
	"../setting"
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"strings"
	"time"
)

func DefaultSecurityDetailList(securityKey primitive.ObjectID) (defaultValue []model.SecurityDetail, err error) {
	var (
		values = []string{ // 19 function
			setting.QueueTable,setting.IntegrationServiceTable,
			setting.TenantTable, setting.ApplicationTable, setting.TaxCodeTable, setting.UserTable ,setting.SalesChannelTable, setting.BrandTable, setting.RegionTable, setting.CurrencyTable, setting.SiteTable, setting.PaymentTypeTable, setting.FreightTable, setting.ReasonTable+"_CODE", setting.SecurityRoleTable, setting.CountryTable,
			setting.CustomerTable, setting.ProductTable,
			setting.SalesOrderTable, "CANCEL_ORDER", "RETURN_ORDER",
		}
		currentTime = time.Now()
	)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	cursor, err := database.Database.Collection(setting.SecurityDetailTable).Find(ctx, bson.M{"security_key": bsonx.ObjectID(securityKey)})
	if err != nil {
		if err.Error() != "mongo: no documents in result" {
			return defaultValue, errors.New("Find security detail ERROR - " + err.Error())
		}
	} else {
		defer cursor.Close(ctx)
		for cursor.Next(ctx) {
			var securityDetail model.SecurityDetail
			err := cursor.Decode(&securityDetail)
			if err != nil {
				return defaultValue, errors.New("Decode Sales Order with Details ERROR" + err.Error())
			}

			for k := 0; k < len(values); k++ {
				if strings.ToLower(securityDetail.SourceObjectType) == strings.ToLower(values[k]) {
					values = append(values[:k], values[k+1:]...)
				}
			}

		}
	}

	if len(values) > 0 {
		for _, v := range values {
			var detail model.SecurityDetail
			detail.ID = primitive.NewObjectID()
			detail.SourceObjectType = strings.ToLower(v)
			detail.AccessRight = enum.AccessRightNo
			detail.SecurityKey = securityKey
			detail.ObjectType = setting.SecurityDetailTable
			detail.CreatedDate = currentTime
			detail.UpdatedDate = currentTime

			defaultValue = append(defaultValue, detail)
		}
	}


	return defaultValue, nil
}
