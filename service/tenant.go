package service

import (
	"../database"
	"../model"
	"../setting"
	"./helper"
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"gopkg.in/oauth2.v4/errors"
	"time"
)

func CheckDefaultTenantSetting(tenantKey primitive.ObjectID) error {
	var (
		defaultSettings = []string{"smtp_server", "smtp_email", "smtp_password", "smtp_ssl", "smtp_port", "decimal_quantity", "decimal_price", "decimal_percent", "decimal_amount"}
		settingIds      model.TenantSettingIds
		tenantSetting   model.TenantSetting
		currentTime     = time.Now()
	)

	collection := database.Database.Collection(setting.TenantSettingTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	match := bson.D{{"$match", bson.D{{"tenant_key", bsonx.ObjectID(tenantKey)}}}}
	group := bson.D{{"$group", bson.D{
		{"_id", "$tenant_key"},
		{"setting_ids", bson.D{{"$push", "$setting_id"},
		}}}}}
	project := bson.D{{"$project", bson.D{{"_id", 0}, {"setting_ids", 1}}}}

	pipeLine := mongo.Pipeline{
		match,
		group,
		project,
	}

	cursor, err := collection.Aggregate(ctx, pipeLine) //findOptions)
	if err != nil {
		return err
	}
	defer cursor.Close(ctx)
	if cursor.Next(ctx) {
		err := cursor.Decode(&settingIds)
		if err != nil {
			return err
		}
	}
	if err := cursor.Err(); err != nil {
		return err
	}

	for _, settingId := range defaultSettings {
		if !helper.IsItemExistsInArray(settingIds.TenantSettingIds, settingId) {
			tenantSetting.ID = primitive.NewObjectIDFromTimestamp(currentTime)
			tenantSetting.TenantKey = tenantKey
			tenantSetting.SettingId = settingId
			tenantSetting.CreatedDate = currentTime
			tenantSetting.CreatedBy = primitive.NewObjectIDFromTimestamp(currentTime)
			tenantSetting.CreatedApplicationKey = primitive.NewObjectIDFromTimestamp(currentTime)
			tenantSetting.UpdatedDate = currentTime
			tenantSetting.UpdatedBy = primitive.NewObjectIDFromTimestamp(currentTime)
			tenantSetting.CreatedApplicationKey = primitive.NewObjectIDFromTimestamp(currentTime)

			if _, err = collection.InsertOne(ctx, tenantSetting); err != nil {
				return errors.New(fmt.Sprintf("Insert tenant setting '%s' ERROR - %s", settingId, err.Error()))
			}
		}
	}

	return nil
}
