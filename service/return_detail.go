package service

import (
	"../database"
	"../model"
	"../model/enum"
	"../setting"
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"time"
)

func HandleReturnDetailStatusChange(docNum string, docLineNum string, newStatus enum.ReturnDetailStatus) (err error) {
	var (
		returnDetail     model.ReturnDetail
		returnDetailColl = database.Database.Collection(setting.ReturnDetailTable)
		currentTime      = time.Now()
	)
	ctx, cancel := context.WithTimeout(context.Background(),15*time.Second)
	defer cancel()

	newStatus, err = newStatus.IsValid()
	if err != nil {
		return errors.New(fmt.Sprintf("New Return Detail status '%s' is invalid", newStatus))
	}

	returnDetail, err = GetReturnDetailByDocLineNum(docNum, docLineNum)
	if err != nil {
		return errors.New(fmt.Sprintf("Get Return Order Detail ERROR - '%s'", err.Error()))
	}

	//b, err := json.Marshal(returnDetail)
	//if err != nil {
	//	return errors.New(fmt.Sprintf("Marshal sales order ERROR - '%s'", err.Error()))
	//}

	//if returnDetail.DocLineStatus == newStatus {
	//	return errors.New(fmt.Sprintf("New Order line status '%s' same with old order line status", newStatus))
	//}

	//if !IsValidOrderLineStatusChange(returnDetail.DocLineStatus, newStatus) {
	//	return errors.New(fmt.Sprintf("Order line status '%s' is not allowed to change to '%s'", returnDetail.DocLineStatus, newStatus))
	//}




	switch newStatus {
	case enum.ReturnDetailStatusReceived:
	case enum.ReturnDetailStatusReturned:
	case enum.ReturnDetailStatusAccepted:
	case enum.ReturnDetailStatusRejected:
	}

	filter := bson.M{"_id": bsonx.ObjectID(returnDetail.ID)}
	update := bson.M{"$set": bson.M{"doc_line_status": newStatus, "updated_date": currentTime}}
	if _, err = returnDetailColl.UpdateOne(context.Background(), filter, update); err != nil {
		return errors.New(fmt.Sprintf("Update Sales order detail status ERROR - " + err.Error()))
	}

	countFilter := bson.M{
		"doc_num":      docNum,
		"doc_line_num": docLineNum,
		"to_status":    newStatus,
	}

	count, err := database.Database.Collection(setting.ReturnStatusTable).CountDocuments(ctx, countFilter)
	if err != nil {
		return errors.New(fmt.Sprintf("Count Return Order with new status ERROR - " + err.Error()))
	}

	if count == 0 {
		if err = CreateReturnStatusChange(docNum, docLineNum, string(returnDetail.DocLineStatus.ToCorrectCase()), string(newStatus)); err != nil {
			return errors.New(fmt.Sprintf("Create Return Order Status change ERROR - " + err.Error()))
		}
	}

	return nil
}

func GetReturnDetailByDocLineNum(docNum string, docLineNum string) (sDetail model.ReturnDetail, err error) {
	var coll = database.Database.Collection(setting.ReturnDetailTable)

	filter := bson.M{"doc_num": docNum, "doc_line_num": docLineNum}
	err = coll.FindOne(context.Background(), filter).Decode(&sDetail)
	if err != nil {
		return sDetail, errors.New("Get Return Detail by Doc line num ERROR - " + err.Error())
	}

	return sDetail, nil
}