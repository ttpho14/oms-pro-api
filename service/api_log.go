package service

import (
	"../database"
	"../model"
	"../setting"
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

func InsertAPILog(applicationId, method, functionName, requestBody, responseBody string) (err error) {
	var log = model.ApiLog{
		ID:            primitive.NewObjectID(),
		ApplicationId: applicationId,
		Method:        method,
		Function:      functionName,
		RequestBody:   requestBody,
		ResponseBody:  responseBody,
		Date:          time.Now(),
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if _, err = database.Database.Collection(setting.ApiLogTable).InsertOne(ctx, log); err != nil {
		return errors.New("Insert API log ERROR - " + err.Error())
	}

	return nil
}

func UpdateAPILog(applicationId, method, functionName, requestBody, responseBody string) (err error) {
	var log = model.ApiLog{
		ApplicationId: applicationId,
		Method:        method,
		Function:      functionName,
		RequestBody:   requestBody,
		ResponseBody:  responseBody,
		Date:          time.Now(),
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if _, err = database.Database.Collection(setting.ApiLogTable).InsertOne(ctx, log); err != nil {
		return errors.New("Insert API log ERROR - " + err.Error())
	}

	return nil
}

/*
Method       string             `json:"method" bson:"method" example:"PUT"`
	Function     string             `json:"function" bson:"function" example:"GetSalesOrderById"`
	Date         time.Time          `json:"date" bson:"date"`
	RequestBody  string             `json:"request_body" bson:"request_body" example:"request body"`
	ResponseBody string             `json:"response_body" bson:"response_body" example:"response body"`
*/
