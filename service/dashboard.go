package service

import (
	"../model/enum"
	"go.mongodb.org/mongo-driver/bson"
)

func DashboardMakeFilter(period enum.DashboardPeriod, countryId, salesChannelId string) (matchFilter bson.D, orderDetailGroup, dateDesc bson.M, err error) {
	var andArray bson.A
	currentTimeGMT8 := bson.M{"date": "$$NOW", "timezone": "+08:00"}
	createdDateTimeGMT8 := bson.M{"date": "$created_date", "timezone": "+08:00"}
	switch period {
	case enum.DashboardPeriodToday, enum.DashboardPeriodYesterday:
		var dateAdd int64 = 0
		if period == enum.DashboardPeriodYesterday {
			dateAdd = dateAdd + (-1 * 24 * 60 * 60000)
		}
		dateTimeGMT8 := bson.M{
			"date":     bson.M{"$add": bson.A{"$$NOW", dateAdd}},
			"timezone": "+08:00",
		}

		//matchFilter with current day.
		andArray = bson.A{
			bson.M{"$eq": bson.A{bson.M{"$year": createdDateTimeGMT8}, bson.M{"$year": dateTimeGMT8}}},
			bson.M{"$eq": bson.A{bson.M{"$month": createdDateTimeGMT8}, bson.M{"$month": dateTimeGMT8}}},
			bson.M{"$eq": bson.A{bson.M{"$dayOfMonth": createdDateTimeGMT8}, bson.M{"$dayOfMonth": dateTimeGMT8}}},
		}
		//bson.M for group in order detail >>>>> Group for specific hour
		orderDetailGroup = bson.M{
			"hour": bson.M{"$hour": createdDateTimeGMT8},
		}

		//bson.M for date when project the order Detail >>>>>> Specific Hour , concat with ":00"
		dateDesc = bson.M{"$toString": "$_id.hour"}
		break
	case enum.DashboardPeriodThisWeek, enum.DashboardPeriodLastWeek:
		var weekAdd int64 = 0
		if period == enum.DashboardPeriodLastWeek {
			weekAdd = -1
		}

		//matchFilter with current day.
		andArray = bson.A{
			bson.M{"$eq": bson.A{bson.M{"$year": createdDateTimeGMT8}, bson.M{"$year": currentTimeGMT8}}},
			bson.M{"$eq": bson.A{bson.M{"$week": createdDateTimeGMT8}, bson.M{"$add": bson.A{bson.M{"$week": currentTimeGMT8}, weekAdd}}}},
		}
		//bson.M for group in order detail >>>>> Group for specific hour
		orderDetailGroup = bson.M{
			"day_of_week": bson.M{"$dayOfWeek": createdDateTimeGMT8},
		}

		//bson.M for date when project the order Detail >>>>>> Specific Hour , concat with ":00"
		dateDesc = bson.M{"$toString": "$_id.day_of_week"}
		break
	case enum.DashboardPeriodThisMonth, enum.DashboardPeriodLastMonth:
		var monthAdd int64 = 0
		if period == enum.DashboardPeriodLastMonth {
			monthAdd = -1
		}

		//matchFilter with current day.
		andArray = bson.A{
			bson.M{"$eq": bson.A{bson.M{"$year": createdDateTimeGMT8}, bson.M{"$year": currentTimeGMT8}}},
			bson.M{"$eq": bson.A{bson.M{"$month": createdDateTimeGMT8}, bson.M{"$add": bson.A{bson.M{"$month": currentTimeGMT8}, monthAdd}}}},
		}
		//bson.M for group in order detail >>>>> Group for specific hour
		orderDetailGroup = bson.M{
			"day":   bson.M{"$dayOfMonth": createdDateTimeGMT8},
			"month": bson.M{"$month": createdDateTimeGMT8},
			"year":  bson.M{"$year": createdDateTimeGMT8},
		}

		//bson.M for date when project the order Detail >>>>>> Specific Hour , concat with ":00"
		dateDesc = bson.M{
			"$concat": bson.A{
				bson.M{"$toString": "$_id.day"},
				"/",
				bson.M{"$toString": "$_id.month"},
			},
		}
		break
	case enum.DashboardPeriodThisYear, enum.DashboardPeriodLastYear:
		var yearAdd int64 = 0
		if period == enum.DashboardPeriodLastYear {
			yearAdd = -1
		}

		//matchFilter with current day.
		andArray = bson.A{
			bson.M{"$eq": bson.A{bson.M{"$year": createdDateTimeGMT8}, bson.M{"$add": bson.A{bson.M{"$year": currentTimeGMT8}, yearAdd}}}},
		}

		//bson.M for group in order detail >>>>> Group for specific hour
		orderDetailGroup = bson.M{
			"month": bson.M{"$month": createdDateTimeGMT8},
			"year":  bson.M{"$year": createdDateTimeGMT8},
		}

		//bson.M for date when project the order Detail >>>>>> Specific Hour , concat with ":00"
		dateDesc = bson.M{
			"$concat": bson.A{
				bson.M{"$toString": "$_id.month"},
				"/",
				bson.M{"$toString": "$_id.year"},
			},
		}
		break
	default:
		//matchFilter with current day.
		andArray = bson.A{
			bson.M{"$eq": bson.A{bson.M{"$year": createdDateTimeGMT8}, bson.M{"$year": currentTimeGMT8}}},
		}
		//bson.M for group in order detail >>>>> Group for specific hour
		orderDetailGroup = bson.M{
			"month": bson.M{"$month": createdDateTimeGMT8},
			"year":  bson.M{"$year": createdDateTimeGMT8},
		}

		//bson.M for date when project the order Detail >>>>>> Specific Hour , concat with ":00"
		dateDesc = bson.M{
			"$concat": bson.A{
				bson.M{"$toString": "$_id.month"},
				"/",
				bson.M{"$toString": "$_id.year"},
			},
		}
		break
	}

	if countryId != "" {
		andArray = append(andArray, bson.M{"$eq": bson.A{"$country_from_site.country_id", countryId}})
	}

	if salesChannelId != "" {
		andArray = append(andArray, bson.M{"$eq": bson.A{bson.M{"$toUpper": "$sales_channel_id"}, bson.M{"$toUpper": salesChannelId}}})
	}

	matchFilter = bson.D{
		{"$match", bson.M{
			"$expr": bson.M{
				"$and": andArray,
			},
		}},
	}

	return matchFilter, orderDetailGroup, dateDesc, nil
}
