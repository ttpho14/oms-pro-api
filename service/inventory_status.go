package service

import (
	"../database"
	"../model"
	"../setting"
	"context"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"gopkg.in/oauth2.v4/errors"
	"time"
)

func UpdateInventoryStatusFunc(productId, siteId string, onHandQty float64, orderQty float64, isOverrideOnHand bool) (invSta model.InventoryStatus, err error) {
	collection := database.Database.Collection(setting.InventoryStatusTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{"product_id": productId, "site_id": siteId}
	if err = collection.FindOne(ctx, filter).Decode(&invSta); err != nil {
		return model.InventoryStatus{}, errors.New("Find Inventory ERROR - " + err.Error())
	}

	if isOverrideOnHand {
		invSta.OnHandQty = onHandQty
		invSta.YchQty = onHandQty
	} else {
		invSta.OnHandQty = invSta.OnHandQty + onHandQty // Positive when
		invSta.OnOrderQty = invSta.OnOrderQty + orderQty
	}

	invSta.OnAvailableQty = invSta.OnHandQty - invSta.OnOrderQty
	invSta.UpdatedDate = time.Now()

	if _, err = collection.UpdateOne(ctx, filter, bson.M{"$set": invSta}); err != nil {
		return model.InventoryStatus{}, errors.New("Update ERROR - " + err.Error())
	}

	var invLog model.InventoryLog

	_ = copier.Copy(&invLog, &invSta)
	invLog.ID = primitive.NewObjectID()

	if _, err = database.Database.Collection(setting.InventoryLogTable).InsertOne(ctx, invLog); err != nil {
		return model.InventoryStatus{}, errors.New("Insert Inventory Log ERROR - " + err.Error())
	}

	return invSta, nil
}
