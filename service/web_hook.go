package service

import (
	"encoding/json"
	"net/http"

	aHelper "../api/api_helper"
	"../model"
)

func WebHookErrorResponse(fromAppId, toAppId, sourceObjType, sourceObjId, body, errMsg string, response http.ResponseWriter) {
	var res model.ResponseResult
	res.Message = errMsg
	byteRes, _ := json.Marshal(res)
	_ = InsertQueue(fromAppId, toAppId, sourceObjType, sourceObjId, body, string(byteRes), body, errMsg, false)
	aHelper.ErrorResponse(response, http.StatusInternalServerError, 0, errMsg)
}
