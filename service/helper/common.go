package helper

import (
	"../../database"
	"context"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"io"
	"log"
	"reflect"
	"regexp"
	"runtime"
	"strings"
	"time"
)

func IsItemExistsInArray(slice interface{}, item interface{}) bool {
	s := reflect.ValueOf(slice)

	if s.Kind() != reflect.Slice {
		panic("Invalid data-type")
	}

	for i := 0; i < s.Len(); i++ {
		if s.Index(i).Interface() == item {
			return true
		}
	}

	return false
}

func RemoveElementFromSlice(slice []interface{}, index int64) []interface{} {
	slice = append(slice[:index], slice[index+1:]...)
	return slice
}

func GetKeyFromId(objectType, idField, id string) (primitive.ObjectID, error) {
	type tempt struct {
		ID primitive.ObjectID `json:"_id" bson:"_id"`
	}
	var t tempt
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	match := bson.D{
		{"$match", bson.M{idField: RegexStringID(id)}},
	}
	project := bson.D{
		{"$project", bson.M{"_id": 1}},
	}

	pipeLine := mongo.Pipeline{
		match,
		project,
	}

	cursor, err := database.Database.Collection(objectType).Aggregate(ctx, pipeLine)
	if err != nil {
		return primitive.NilObjectID, errors.New(fmt.Sprintf("Find record in collection '%s' ERROR", objectType))
	}
	defer cursor.Close(ctx)
	if cursor.Next(ctx) {
		if err := cursor.Decode(&t); err != nil {
			return primitive.NilObjectID, errors.New("Decode record ERROR" + err.Error())
		}
	}
	if err := cursor.Err(); err != nil {
		return primitive.NilObjectID, errors.New("Fetch Cursor ERROR" + err.Error())
	}

	//if err := database.Database.Collection(objectType).FindOne(ctx, bson.M{idField: id}).Decode(&t); err != nil {
	//	return primitive.NilObjectID, errors.New(fmt.Sprintf("Find record in collection '%s' ERROR", objectType))
	//}

	return t.ID, nil
}

func IsZeroInterface(x interface{}) bool {
	return reflect.DeepEqual(x, reflect.Zero(reflect.TypeOf(x)).Interface())
}

func ModelToInterface(model interface{}) (interface{}, error) {
	var output bson.M
	b, _ := json.Marshal(model)

	if err := json.Unmarshal(b, &output); err != nil {
		return nil, errors.New("Parse model to interface ERROR - " + err.Error())
	}

	return output, nil
}

func GetFunctionName(i interface{}) string {
	str := runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()

	strArr := strings.Split(str, "/")

	return strings.Split(strArr[len(strArr)-1], ".")[1]
}

func Encrypt(stringToEncrypt string, keyString string) (encryptedString string, err error) {

	//Since the key is in string, we need to convert decode it to bytes
	key, _ := hex.DecodeString(keyString)
	plaintext := []byte(stringToEncrypt)

	//Create a new Cipher Block from the key
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", errors.New("New Cipher ERROR - " + err.Error())
	}

	//Create a new GCM - https://en.wikipedia.org/wiki/Galois/Counter_Mode
	//https://golang.org/pkg/crypto/cipher/#NewGCM
	aesGCM, err := cipher.NewGCM(block)
	if err != nil {
		return "", errors.New("New GCM ERROR - " + err.Error())
	}

	//Create a nonce. Nonce should be from GCM
	nonce := make([]byte, aesGCM.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return "", errors.New("Read Full ERROR - " + err.Error())
	}

	//Encrypt the data using aesGCM.Seal
	//Since we don't want to save the nonce somewhere else in this case, we add it as a prefix to the encrypted data. The first nonce argument in Seal is the prefix.
	ciphertext := aesGCM.Seal(nonce, nonce, plaintext, nil)
	return fmt.Sprintf("%x", ciphertext), nil
}

func Decrypt(encryptedString string, keyString string) (decryptedString string, err error) {

	key, _ := hex.DecodeString(keyString)
	enc, _ := hex.DecodeString(encryptedString)

	//Create a new Cipher Block from the key
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", errors.New("New Cipher ERROR - " + err.Error())
	}

	//Create a new GCM
	aesGCM, err := cipher.NewGCM(block)
	if err != nil {
		return "", errors.New("New GCM ERROR - " + err.Error())
	}

	//Get the nonce size
	nonceSize := aesGCM.NonceSize()

	//Extract the nonce from the encrypted data
	nonce, ciphertext := enc[:nonceSize], enc[nonceSize:]

	//Decrypt the data
	plaintext, err := aesGCM.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		return "", errors.New("Open ERROR - " + err.Error())
	}

	return fmt.Sprintf("%s", plaintext), nil
}

func WebEncrypt(rawPassword string) (newPassword string) {
	var (
		charCode = []rune(rawPassword)
	)

	for i, _ := range charCode {
		charCode[i] = charCode[i] + 21
	}

	return string(charCode)

}

func WebDecrypt(encryptedPass string) (rawPassword string) {
	var (
		charCode = []rune(encryptedPass)
	)

	for i, _ := range charCode {
		charCode[i] = charCode[i] - 21
	}

	return string(charCode)
}

func CheckValueExists(sourceObjectType, fieldName, value string) (b bool, err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	count, err := database.Database.Collection(sourceObjectType).CountDocuments(ctx, bson.M{fieldName: value})
	if err != nil {
		return false, errors.New("Count document ERROR - " + err.Error())
	}

	if count == 0 {
		return false, errors.New(fmt.Sprintf("%s with value '%s' doesn't exists ", fieldName, value))
	}

	return true, nil
}

func RegexStringID(str string) (b bson.M) {
	return bson.M{"$regex": primitive.Regex{Pattern: "^" + str + "$", Options: "i"}}
}

func RegexStringLike(str string) (b bson.M) {
	return bson.M{"$regex": primitive.Regex{Pattern: str, Options: "i"}}
}

func AddStringToKeyOfMap(str string, m map[string]interface{}) (newMap map[string]interface{}) {
	newMap = make(map[string]interface{})

	for keyName, obj := range m {
		newMap[str+keyName] = obj
	}

	return newMap
}

func RegexOnlyNumber(str string) (string, error) {
	reg, err := regexp.Compile("[^0-9]+")
	if err != nil {
		log.Fatal(err)
	}
	numberOnly := reg.ReplaceAllString(str, "")

	return numberOnly, err
}

func IsEmptyString(old, new string) string {
	if old == "" {
		return new
	}
	return old
}
