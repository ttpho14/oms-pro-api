package service

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"

	"../database"
	"../model"
	"../model/enum"
	"../setting"
	"./helper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

func HandleOrderStatusChange(docNum string, newStatus enum.OrderStatus, isUpdateDetail bool) (sales model.SalesOrder, err error) {
	var (
		salesOrder           model.SalesOrderResponse
		salesOrderCollection = database.Database.Collection(setting.SalesOrderTable)
		currentTime          = time.Now()
	)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	newStatus, err = newStatus.IsValid()
	if err != nil {
		return sales, errors.New(fmt.Sprintf("New Order status '%s' is invalid", newStatus))
	}

	salesOrder, err = GetSalesOrderWithDetails(docNum)
	if err != nil {
		return sales, errors.New(fmt.Sprintf("Get Sales Order with Details ERROR - '%s'", err.Error()))
	}
	//if salesOrder.OrderStatus == newStatus {
	//	return errors.New(fmt.Sprintf("New Order Status '%s' same with old order status", newStatus))
	//}

	//if !IsValidOrderStatusChange(salesOrder.OrderStatus, newStatus) {
	//	return errors.New(fmt.Sprintf("Order Status '%s' is not allowed to change to '%s'", salesOrder.OrderStatus, newStatus))
	//}

	filter := bson.M{"_id": bsonx.ObjectID(salesOrder.ID)}
	update := bson.M{"$set": bson.M{"order_status": newStatus, "updated_date": currentTime}}

	if _, err = salesOrderCollection.UpdateOne(ctx, filter, update); err != nil {
		return sales, errors.New(fmt.Sprintf("Update Sales Order Status ERROR - " + err.Error()))
	}

	countFilter := bson.M{
		"doc_num":      docNum,
		"doc_line_num": "",
		"to_status":    newStatus,
	}

	count, err := database.Database.Collection(setting.SalesOrderStatusTable).CountDocuments(ctx, countFilter)
	if err != nil {
		return sales, errors.New(fmt.Sprintf("Count Sales Order with new status ERROR - " + err.Error()))
	}

	if count == 0 {
		if err = CreateSalesOrderStatusChange(salesOrder.DocNum, "", string(salesOrder.OrderStatus.ToCorrectCase()), string(newStatus)); err != nil {
			return sales, errors.New(fmt.Sprintf("Create Sales Order Status change ERROR - " + err.Error()))
		}
	}

	salesOrder.OrderStatus = newStatus
	salesOrder.UpdatedDate = currentTime

	salesOrder.SalesChannelId = strings.ToUpper(salesOrder.SalesChannelId)

	switch newStatus {
	case enum.OrderStatusNew:
		for _, orderDetail := range salesOrder.SalesOrderDetail {
			if _, err := UpdateInventoryStatusFunc(orderDetail.ProductCode, salesOrder.SiteId, 0, orderDetail.Quantity, false); err != nil {
				return sales, err
			}
		}
	case enum.OrderStatusProcessing:
		for index, orderDetail := range salesOrder.SalesOrderDetail {
			err = HandleSalesOrderDetailStatusChange(salesOrder.DocNum, orderDetail.DocLineNum, enum.OrderLineStatusProcessing)
			if err != nil {
				fmt.Println(fmt.Sprintf("Sales Order Detail - '%s' Status Change ERROR - %s", orderDetail.DocLineNum, err.Error()))
			} else {
				fmt.Println(fmt.Sprintf("Sales Order Detail - '%s' Status Change SUCCESS", orderDetail.DocLineNum))
				salesOrder.SalesOrderDetail[index].DocLineStatus = enum.OrderLineStatusProcessing
			}
		}
	case enum.OrderStatusOnHold:
	case enum.OrderStatusCancelled:
		//for index, orderDetail := range salesOrder.SalesOrderDetail {
		//	if _, err := UpdateInventoryStatusFunc(orderDetail.ProductCode, salesOrder.SiteId, 0, (orderDetail.Quantity)*(-1), false); err != nil {
		//		return sales, err
		//	}
		//}
	case enum.OrderStatusPicked:
		for index, orderDetail := range salesOrder.SalesOrderDetail {
			//invStatusFilter := bson.M{"site_id": salesOrder.Site.SiteId, "product_id": orderDetail.ProductCode}
			//err = invStatusCollection.FindOne(context.Background(), invStatusFilter).Decode(&invStatus)
			//if err != nil {
			//	return err
			//}
			//
			//invStatus.OnOrderQty = invStatus.OnOrderQty + orderDetail.Quantity
			////invStatus.OnAvailableQty = invStatus.OnHandQty - (invStatus.OnOrderQty - invStatus.ShippedQty - orderDetail.CancelQuantity)
			//
			//update := bson.M{"$set": invStatus}
			//_, err = invStatusCollection.UpdateOne(context.Background(), invStatusFilter, update)
			//if err != nil {
			//	return err
			//}
			//Update line status ==> Use HandleSalesOrderDetailStatusChange
			if isUpdateDetail && orderDetail.ReturnQuantity == 0 && orderDetail.CancelQuantity == 0 {
				err = HandleSalesOrderDetailStatusChange(salesOrder.DocNum, orderDetail.DocLineNum, enum.OrderLineStatusPicked)
				if err != nil {
					fmt.Println(fmt.Sprintf("Sales Order Detail - '%s' Status Change ERROR - %s", orderDetail.DocLineNum, err.Error()))
				} else {
					fmt.Println(fmt.Sprintf("Sales Order Detail - '%s' Status Change SUCCESS", orderDetail.DocLineNum))
					salesOrder.SalesOrderDetail[index].DocLineStatus = enum.OrderLineStatusPicked
				}
			}
		}

		if salesOrder.SalesChannelId == setting.SalesChannelLazada || salesOrder.SalesChannelId == setting.SalesChannelZalora {
			b, _ := json.Marshal(salesOrder)

			err = InsertQueue(enum.AppIdOMS, enum.AppIdSIA, setting.SalesOrderTable, salesOrder.DocNum, string(b), "", "", "", false)
			if err != nil {
				return sales, err
			}
		}

	case enum.OrderStatusPacked:
		for index, orderDetail := range salesOrder.SalesOrderDetail {
			//invStatusFilter := bson.M{"site_id": salesOrder.Site.SiteId, "product_id": orderDetail.ProductCode}
			//err = invStatusCollection.FindOne(context.Background(), invStatusFilter).Decode(&invStatus)
			//if err != nil {
			//	return err
			//}
			//
			//invStatus.OnOrderQty = invStatus.OnOrderQty + orderDetail.Quantity
			////invStatus.OnAvailableQty = invStatus.OnHandQty - (invStatus.OnOrderQty - invStatus.ShippedQty - orderDetail.CancelQuantity)
			//
			//update := bson.M{"$set": invStatus}
			//_, err = invStatusCollection.UpdateOne(context.Background(), invStatusFilter, update)
			//if err != nil {
			//	return err
			//}
			//Update line status ==> Use HandleSalesOrderDetailStatusChange
			if isUpdateDetail && orderDetail.ReturnQuantity == 0 && orderDetail.CancelQuantity == 0 {
				err = HandleSalesOrderDetailStatusChange(salesOrder.DocNum, orderDetail.DocLineNum, enum.OrderLineStatusPacked)
				if err != nil {
					fmt.Println(fmt.Sprintf("Sales Order Detail - '%s' Status Change ERROR - %s", orderDetail.DocLineNum, err.Error()))
				} else {
					fmt.Println(fmt.Sprintf("Sales Order Detail - '%s' Status Change SUCCESS", orderDetail.DocLineNum))
					salesOrder.SalesOrderDetail[index].DocLineStatus = enum.OrderLineStatusPacked
				}
			}
		}

		//err = InsertQueue(enum.AppIdOMS, enum.AppIdSIA, setting.SalesOrderTable, salesOrder.DocNum, string(b), "", "", "", false)
		//if err != nil {
		//	return sales, err
		//}

		if salesOrder.SalesChannelId == setting.SalesChannelPumaCom {
			b, _ := json.Marshal(salesOrder)
			if err = InsertQueue(enum.AppIdOMS, enum.AppIdCyberSource, "captures", salesOrder.DocNum, string(b), "", "", "", false); err != nil {
				return sales, err
			}
		}
	case enum.OrderStatusShipped:
		// Update Inventory Status
		/*
			for index, orderDetail := range salesOrder.SalesOrderDetail {
				invStatusFilter := bson.M{"site_id": salesOrder.Site.SiteId, "product_id": orderDetail.ProductCode}

				err = invStatusCollection.FindOne(ctx, invStatusFilter).Decode(&invStatus)
				if err != nil {
					return err
				}

				invStatus.ShippedQty = invStatus.ShippedQty + orderDetail.ShippedQuantity
				//invStatus.OnAvailableQty = invStatus.OnHandQty - (invStatus.OnOrderQty - invStatus.ShippedQty - orderDetail.CancelQuantity)

				update := bson.M{"$set": invStatus}
				_, err = invStatusCollection.UpdateOne(ctx, invStatusFilter, update)
				if err != nil {
					return err
				}
			}
		*/
		for index, orderDetail := range salesOrder.SalesOrderDetail {
			if _, err := UpdateInventoryStatusFunc(orderDetail.ProductCode, salesOrder.SiteId, (orderDetail.Quantity)*(-1), (orderDetail.Quantity)*(-1), false); err != nil {
				return sales, err
			}

			if isUpdateDetail && orderDetail.ReturnQuantity == 0 && orderDetail.CancelQuantity == 0 {
				if err = HandleSalesOrderDetailStatusChange(salesOrder.DocNum, orderDetail.DocLineNum, enum.OrderLineStatusShipped); err != nil {
					fmt.Println(fmt.Sprintf("Sales Order Detail - '%s' Status Change ERROR - %s", orderDetail.DocLineNum, err.Error()))
				} else {
					fmt.Println(fmt.Sprintf("Sales Order Detail - '%s' Status Change SUCCESS", orderDetail.DocLineNum))
					salesOrder.SalesOrderDetail[index].DocLineStatus = enum.OrderLineStatusShipped
				}
			}
		}

		if _, err = salesOrderCollection.UpdateOne(ctx, filter, bson.M{"$set": bson.M{"shipping_status": "SHIPPED"}}); err != nil {
			return sales, errors.New(fmt.Sprintf("Update Shipping status to [SHIPPED] ERROR - " + err.Error()))
		}

		if salesOrder.SAPExport == false {
			salesOrder.SAPExport = true
			b, err := json.Marshal(salesOrder)
			if err != nil {
				return sales, errors.New(fmt.Sprintf("Marshal Sales order ERROR - '%s'", err.Error()))
			}

			if _, err = salesOrderCollection.UpdateOne(ctx, filter, bson.M{"$set": bson.M{"sap_export": salesOrder.SAPExport}}); err != nil {
				return sales, errors.New(fmt.Sprintf("Update Sales Order SAP Export ERROR - " + err.Error()))
			}

			err = InsertQueue(enum.AppIdOMS, enum.AppIdSAP, setting.SalesOrderTable, salesOrder.DocNum, string(b), "", "", "", false)
			if err != nil {
				return sales, err
			}
		}

		if salesOrder.SalesChannelId == setting.SalesChannelPumaCom {
			b, _ := json.Marshal(salesOrder)
			if err = InsertQueue(enum.AppIdOMS, enum.AppIdSFCC, setting.SalesOrderTable, salesOrder.DocNum, string(b), "", "", "", false); err != nil {
				return sales, err
			}
			if err = InsertQueue(enum.AppIdOMS, enum.AppIdEmail, "shipping_notification", salesOrder.DocNum, string(b), "", "", "", false); err != nil {
				return sales, err
			}
		}
	case enum.OrderStatusDelivered:
		//filter := bson.M{"_id": bsonx.ObjectID(salesOrder.ID)}
		//update := bson.M{"$set": bson.M{"order_status": newStatus, "updated_date": currentTime}}
		//
		//_, err = salesOrderCollection.UpdateOne(ctx, filter, update)
		//if err != nil {
		//	return errors.New(fmt.Sprintf("Update Sales Order Status ERROR - " + err.Error()))
		//}
		for index, orderDetail := range salesOrder.SalesOrderDetail {
			if orderDetail.DocLineStatus == enum.OrderLineStatusShipped && isUpdateDetail && orderDetail.ReturnQuantity == 0 && orderDetail.CancelQuantity == 0 {
				err = HandleSalesOrderDetailStatusChange(salesOrder.DocNum, orderDetail.DocLineNum, enum.OrderLineStatusDelivered)
				if err != nil {
					fmt.Println(fmt.Sprintf("Sales Order Detail - '%s' Status Change ERROR - %s", orderDetail.DocLineNum, err.Error()))
				} else {
					fmt.Println(fmt.Sprintf("Sales Order Detail - '%s' Status Change SUCCESS", orderDetail.DocLineNum))
					salesOrder.SalesOrderDetail[index].DocLineStatus = enum.OrderLineStatusDelivered
				}
			}
		}

		b, _ := json.Marshal(salesOrder)

		if salesOrder.SAPExport == false {
			salesOrder.SAPExport = true
			b, _ = json.Marshal(salesOrder)

			if _, err = salesOrderCollection.UpdateOne(ctx, filter, bson.M{"$set": bson.M{"sap_export": salesOrder.SAPExport}}); err != nil {
				return sales, errors.New(fmt.Sprintf("Update Sales Order SAP Export ERROR - " + err.Error()))
			}

			err = InsertQueue(enum.AppIdOMS, enum.AppIdSAP, setting.SalesOrderTable, salesOrder.DocNum, string(b), "", "", "", false)
			if err != nil {
				return sales, err
			}
		}

		if salesOrder.SalesChannelId == setting.SalesChannelPumaCom {
			if err = InsertQueue(enum.AppIdOMS, enum.AppIdSFCC, setting.SalesOrderTable, salesOrder.DocNum, string(b), "", "", "", false); err != nil {
				return sales, err
			}
			if err = InsertQueue(enum.AppIdOMS, enum.AppIdEmail, "order_delivery", salesOrder.DocNum, string(b), "", "", "", false); err != nil {
				return sales, err
			}
		}
	case enum.OrderStatusReturned:
		//filter := bson.M{"_id": bsonx.ObjectID(salesOrder.ID)}
		//update := bson.M{"$set": bson.M{"order_status": newStatus, "updated_date": currentTime}}
		//
		//_, err = salesOrderCollection.UpdateOne(ctx, filter, update)
		//if err != nil {
		//	return errors.New(fmt.Sprintf("Update Sales Order Status ERROR - " + err.Error()))
		//}
	case enum.OrderStatusHandover:
		for index, orderDetail := range salesOrder.SalesOrderDetail {
			if isUpdateDetail && orderDetail.ReturnQuantity == 0 && orderDetail.CancelQuantity == 0 {
				err = HandleSalesOrderDetailStatusChange(salesOrder.DocNum, orderDetail.DocLineNum, enum.OrderLineStatusHandover)
				if err != nil {
					fmt.Println(fmt.Sprintf("Sales Order Detail - '%s' Status Change ERROR - %s", orderDetail.DocLineNum, err.Error()))
				} else {
					fmt.Println(fmt.Sprintf("Sales Order Detail - '%s' Status Change SUCCESS", orderDetail.DocLineNum))
					salesOrder.SalesOrderDetail[index].DocLineStatus = enum.OrderLineStatusHandover
				}
			}
		}
		if _, err = salesOrderCollection.UpdateOne(ctx, filter, bson.M{"$set": bson.M{"shipping_status": "SHIPPED"}}); err != nil {
			return sales, errors.New(fmt.Sprintf("Update Shipping status to [SHIPPED] ERROR - " + err.Error()))
		}

	case enum.OrderStatusClosed:
		//filter := bson.M{"_id": bsonx.ObjectID(salesOrder.ID)}
		//update := bson.M{"$set": bson.M{"order_status": newStatus, "updated_date": currentTime}}
		//
		//_, err = salesOrderCollection.UpdateOne(ctx, filter, update)
		//if err != nil {
		//	return errors.New(fmt.Sprintf("Update Sales Order Status ERROR - " + err.Error()))
		//}
	}

	if err = salesOrderCollection.FindOne(ctx, filter).Decode(&sales); err != nil {
		return sales, errors.New("Find Sales Order ERROR - " + err.Error())
	}

	return sales, nil
}

func IsValidOrderStatusChange(oldStatus enum.OrderStatus, newStatus enum.OrderStatus) bool {
	switch oldStatus {
	case "":
		return helper.IsItemExistsInArray([]enum.OrderStatus{enum.OrderStatusNew}, newStatus)
	case enum.OrderStatusNew:
		return helper.IsItemExistsInArray([]enum.OrderStatus{enum.OrderStatusOnHold, enum.OrderStatusProcessing}, newStatus)
	case enum.OrderStatusOnHold:
		return helper.IsItemExistsInArray([]enum.OrderStatus{enum.OrderStatusNew, enum.OrderStatusProcessing}, newStatus)
	case enum.OrderStatusCancelled:
		return helper.IsItemExistsInArray([]enum.OrderStatus{enum.OrderStatusClosed}, newStatus)
	case enum.OrderStatusProcessing:
		return helper.IsItemExistsInArray([]enum.OrderStatus{enum.OrderStatusPicked, enum.OrderStatusPacked, enum.OrderStatusCancelled}, newStatus)
	case enum.OrderStatusPicked:
		return helper.IsItemExistsInArray([]enum.OrderStatus{enum.OrderStatusPacked}, newStatus)
	case enum.OrderStatusPacked:
		return helper.IsItemExistsInArray([]enum.OrderStatus{enum.OrderStatusShipped}, newStatus)
	case enum.OrderStatusShipped:
		return helper.IsItemExistsInArray([]enum.OrderStatus{enum.OrderStatusDelivered}, newStatus)
	case enum.OrderStatusDelivered:
		return helper.IsItemExistsInArray([]enum.OrderStatus{enum.OrderStatusReturned}, newStatus)
	case enum.OrderStatusReturned:
		return helper.IsItemExistsInArray([]enum.OrderStatus{enum.OrderStatusClosed}, newStatus)
	case enum.OrderStatusClosed:
		return false
	// Default : Case when Old Status == Closed ===>>> Will return false
	default:
		return false
	}
}

func GetSalesOrderWithDetails(docNum string) (salesOrder model.SalesOrderResponse, err error) {
	salesOrderCollection := database.Database.Collection(setting.SalesOrderTable)

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	//------------------- GET SALES ORDER ------------------
	//Match with Id
	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.D{{"doc_num", docNum}},
			},
			},
		},
	}}
	//Look up for foreign key
	lookupCountry := bson.D{
		{"$lookup", bson.D{
			{"from", "country"},
			{"localField", "country_key"},
			{"foreignField", "_id"},
			{"as", "country"},
		},
		}}
	lookupSite := bson.D{
		{"$lookup", bson.D{
			{"from", "site"},
			{"localField", "site_key"},
			{"foreignField", "_id"},
			{"as", "site"},
		},
		}}
	lookupSalesChannel := bson.D{
		{"$lookup", bson.D{
			{"from", "sales_channel"},
			{"localField", "sales_channel_key"},
			{"foreignField", "_id"},
			{"as", "sales_channel"},
		},
		}}
	lookupTenant := bson.D{
		{"$lookup", bson.D{
			{"from", "tenant"},
			{"localField", "tenant_key"},
			{"foreignField", "_id"},
			{"as", "tenant"},
		},
		}}
	lookupCustomer := bson.D{
		{"$lookup", bson.D{
			{"from", "customer"},
			{"localField", "customer_key"},
			{"foreignField", "_id"},
			{"as", "customer"},
		},
		}}
	lookupCurrency := bson.D{
		{"$lookup", bson.D{
			{"from", "currency"},
			{"localField", "currency_key"},
			{"foreignField", "_id"},
			{"as", "currency"},
		},
		}}
	lookupSalesOrderDetails := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", "sales_order_details"},
		},
		}}
	lookupSalesOrderComments := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderCommentTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", "sales_order_comments"},
		},
		}}
	lookupSalesOrderFreights := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderFreightTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", "sales_order_freights"},
		},
		}}
	lookupSalesOrderDiscounts := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderDiscountTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", "sales_order_discounts"},
		},
		}}
	lookupSalesOrderShippingDetails := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderShippingDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", "sales_order_shipping_details"},
		},
		}}
	lookupSalesOrderPayments := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderPaymentTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", "sales_order_payments"},
			},
		}}
	lookupSalesOrderPaymentCaptures := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderPaymentCaptureTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", setting.SalesOrderPaymentCaptureTable + "s"},
			},
		}}
	lookupSalesOrderPaymentRefunds := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderPaymentRefundTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", setting.SalesOrderPaymentRefundTable + "s"},
			},
		}}
	lookupSalesOrderPaymentReverses := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderPaymentReverseTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", setting.SalesOrderPaymentReverseTable + "s"},
			},
		}}
	lookupSalesOrderStatus := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderStatusTable},
			{"let", bson.M{"dn": "$doc_num"}},
			{"pipeline", bson.A{
				bson.M{"$match": bson.M{
					"$expr": bson.M{
						"$and": bson.A{
							bson.M{"$eq": bson.A{"$doc_num", "$$dn"}},
							bson.M{"$eq": bson.A{"$doc_line_num", ""}},
						},
					},
				}},
			}},
			{"as", setting.SalesOrderStatusTable},
		},
		}}
	lookupOrderPack := bson.D{
		{"$lookup", bson.D{
			{"from", setting.OrderPackTable},
			{"localField", "doc_num"},
			{"foreignField", "order_no"},
			{"as", setting.OrderPackTable},
		},
		}}

	//Unwind array to object
	unwindOrderPack := bson.D{
		{"$unwind", bson.D{
			{"path", "$" + setting.OrderPackTable},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindCountry := bson.D{
		{"$unwind", bson.D{
			{"path", "$country"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindSite := bson.D{
		{"$unwind", bson.D{
			{"path", "$site"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindSalesChannel := bson.D{
		{"$unwind", bson.D{
			{"path", "$sales_channel"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindTenant := bson.D{
		{"$unwind", bson.D{
			{"path", "$tenant"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindCustomer := bson.D{
		{"$unwind", bson.D{
			{"path", "$customer"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindCurrency := bson.D{
		{"$unwind", bson.D{
			{"path", "$currency"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	sumSalesOrderDetailQuantity := bson.D{
		{"$addFields", bson.D{
			{"order_quantity", bson.D{
				{"$sum", "$sales_order_details.quantity"},
			}},
		}},
	}
	sumSalesOrderDetailReturnQuantity := bson.D{
		{"$addFields", bson.D{
			{"return_quantity", bson.D{
				{"$sum", "$sales_order_details.return_quantity"},
			}},
		}},
	}
	sumSalesOrderDetailCancelQuantity := bson.D{
		{"$addFields", bson.D{
			{"cancel_quantity", bson.D{
				{"$sum", "$sales_order_details.cancel_quantity"},
			}},
		}},
	}
	//sort, skip and limit
	pipeline := mongo.Pipeline{
		match,
		lookupCountry,
		lookupSalesChannel,
		lookupSite,
		lookupTenant,
		lookupCustomer,
		lookupCurrency,
		lookupSalesOrderDetails,
		lookupSalesOrderComments,
		lookupSalesOrderFreights,
		lookupSalesOrderShippingDetails,
		lookupSalesOrderPayments,
		lookupSalesOrderPaymentCaptures,
		lookupSalesOrderPaymentRefunds,
		lookupSalesOrderPaymentReverses,
		lookupSalesOrderDiscounts,
		lookupSalesOrderStatus,
		lookupOrderPack,
		unwindOrderPack,
		unwindCountry,
		unwindSite,
		unwindSalesChannel,
		unwindTenant,
		unwindCustomer,
		unwindCurrency,
		sumSalesOrderDetailQuantity,
		sumSalesOrderDetailReturnQuantity,
		sumSalesOrderDetailCancelQuantity,
	}

	cursor, err := salesOrderCollection.Aggregate(ctx, pipeline) //findOptions)
	if err != nil {
		return model.SalesOrderResponse{}, err
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		err := cursor.Decode(&salesOrder)
		if err != nil {
			return model.SalesOrderResponse{}, err
		}
		//salesOrders = append(salesOrders, salesOrder)
	}
	if err := cursor.Err(); err != nil {
		return model.SalesOrderResponse{}, err
	}

	if salesOrder.ID == primitive.NilObjectID {
		return model.SalesOrderResponse{}, errors.New("No document in Database ")
	}

	return salesOrder, nil
}

func CreateCustomerFromSalesOrder(sales model.SalesOrder) (customer model.Customer, err error) {
	var (
		currentTime = time.Now()
	)

	coll := database.Database.Collection(setting.CustomerTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//-----------------------------
	//address.CreatedBy = sales.CreatedBy
	//address.CreatedApplicationKey = sales.CreatedApplicationKey
	//address.CreatedDate = time.Now()
	//address.UpdatedBy = sales.UpdatedBy
	//address.UpdatedApplicationKey = sales.UpdatedApplicationKey
	//address.UpdatedDate = time.Now()

	customer.ID = primitive.NewObjectIDFromTimestamp(time.Now())
	customer.Email = sales.Email
	customer.FirstName = sales.CustomerName
	customer.PhoneNo = sales.ShippingAddress.SPhone
	customer.MobileNo = sales.ShippingAddress.SPhone
	customer.SalesChannelKey = sales.SalesChannelKey
	customer.IsDeleted = false
	customer.IsActive = true
	customer.ObjectType = setting.CustomerTable
	//---------------------------
	customer.CreatedBy = sales.CreatedBy
	customer.CreatedApplicationKey = sales.CreatedApplicationKey
	customer.CreatedDate = currentTime
	customer.UpdatedBy = sales.UpdatedBy
	customer.UpdatedApplicationKey = sales.UpdatedApplicationKey
	customer.UpdatedDate = currentTime

	if strings.ToUpper(sales.SalesChannelId) == setting.SalesChannelPumaCom {
		customer.DocumentId = sales.RefNo1
	}

	//Default Shipping Address
	shippingAddress := model.Address{
		ID:                    primitive.NewObjectIDFromTimestamp(time.Now()),
		SourceObjectType:      setting.CustomerTable,
		SourceKey:             customer.ID,
		TenantKey:             primitive.NilObjectID,
		AddressType:           "Shipping",
		FirstName:             sales.ShippingAddress.SFirstName,
		LastName:              sales.ShippingAddress.SLastName,
		Address1:              sales.ShippingAddress.SAddress1,
		Address2:              sales.ShippingAddress.SAddress2,
		Address3:              sales.ShippingAddress.SAddress3,
		City:                  sales.ShippingAddress.SCity,
		ZipCode:               sales.ShippingAddress.SZipCode,
		State:                 sales.ShippingAddress.SState,
		Country:               sales.ShippingAddress.SCountry,
		Phone1:                sales.ShippingAddress.SPhone,
		EmailAddress:          sales.ShippingAddress.SEmailAddress,
		IsDefault:             true,
		IsActive:              true,
		ObjectType:            setting.AddressTable,
		IsDeleted:             false,
		CreatedDate:           currentTime,
		CreatedBy:             customer.CreatedBy,
		CreatedApplicationKey: customer.CreatedApplicationKey,
		UpdatedDate:           currentTime,
		UpdatedBy:             customer.UpdatedBy,
		UpdatedApplicationKey: customer.UpdatedApplicationKey,
	}

	billingAddress := model.Address{
		ID:                    primitive.NewObjectIDFromTimestamp(time.Now()),
		SourceObjectType:      setting.CustomerTable,
		SourceKey:             customer.ID,
		TenantKey:             primitive.NilObjectID,
		AddressType:           "Billing",
		FirstName:             sales.ShippingAddress.SFirstName,
		LastName:              sales.ShippingAddress.SLastName,
		Address1:              sales.ShippingAddress.SAddress1,
		Address2:              sales.ShippingAddress.SAddress2,
		Address3:              sales.ShippingAddress.SAddress3,
		City:                  sales.ShippingAddress.SCity,
		ZipCode:               sales.ShippingAddress.SZipCode,
		State:                 sales.ShippingAddress.SState,
		Country:               sales.ShippingAddress.SCountry,
		Phone1:                sales.ShippingAddress.SPhone,
		EmailAddress:          sales.ShippingAddress.SEmailAddress,
		IsDefault:             true,
		IsActive:              true,
		ObjectType:            setting.AddressTable,
		IsDeleted:             false,
		CreatedDate:           currentTime,
		CreatedBy:             customer.CreatedBy,
		CreatedApplicationKey: customer.CreatedApplicationKey,
		UpdatedDate:           currentTime,
		UpdatedBy:             customer.UpdatedBy,
		UpdatedApplicationKey: customer.UpdatedApplicationKey,
	}

	_, err = database.Database.Collection(setting.AddressTable).InsertOne(ctx, shippingAddress)
	if err != nil {
		return customer, errors.New("Insert Default Shipping Address ERROR - " + err.Error())
	}

	_, err = database.Database.Collection(setting.AddressTable).InsertOne(ctx, billingAddress)
	if err != nil {
		return customer, errors.New("Insert Default Billing Address ERROR - " + err.Error())
	}

	customer.DefaultShippingAddressKey = shippingAddress.ID
	customer.DefaultBillingAddressKey = billingAddress.ID

	customer.CustomerNumber, err = GetCustomerSequenceNumber()
	if err != nil {
		return customer, errors.New("Get Customer Number ERROR - " + err.Error())
	}

	_, err = coll.InsertOne(ctx, customer)
	if err != nil {
		return customer, errors.New("Insert customer ERROR - " + err.Error())
	}

	if err = UpdateCustomerSequenceNumber(); err != nil {
		return customer, errors.New("Update Customer Number ERROR - " + err.Error())
	}

	return customer, nil

}

func GetApplicationConfigValue(appId, configId string) (value string, err error) {
	var applicationConfig model.ApplicationConfiguration
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//Get Site Id
	appMatch := bson.D{
		{"$match", bson.M{"application_id": appId}},
	}
	lookupAppConfig := bson.D{
		{"$lookup", bson.D{
			{"from", setting.ApplicationConfigurationTable},
			{"let", bson.M{"key": "$_id"}},
			{"pipeline", bson.A{
				bson.M{"$match": bson.M{
					"$expr": bson.M{
						"$and": bson.A{
							bson.M{"$eq": bson.A{"$application_key", "$$key"}},
							bson.M{"$eq": bson.A{bson.M{"$toUpper": "$configuration_id"}, bson.M{"$toUpper": configId}}},
						},
					},
				}},
			}},
			{"as", setting.ApplicationConfigurationTable},
		},
		}}
	unwindAppConfig := bson.D{
		{"$unwind", bson.D{
			{"path", "$" + setting.ApplicationConfigurationTable},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	projectAppConfig := bson.D{
		{"$project", bson.D{
			{"_id", 0},
			{"configuration_id", "$" + setting.ApplicationConfigurationTable + ".configuration_id"},
			{"configuration_value", "$" + setting.ApplicationConfigurationTable + ".configuration_value"},
		}},
	}
	pipeLine := mongo.Pipeline{
		appMatch,
		lookupAppConfig,
		unwindAppConfig,
		projectAppConfig,
	}

	cursor, err := database.Database.Collection(setting.ApplicationTable).Aggregate(ctx, pipeLine)
	if err != nil {
		return "", errors.New("Find ERROR" + err.Error())
	}
	defer cursor.Close(ctx)
	if cursor.Next(ctx) {
		_ = cursor.Decode(&applicationConfig)
	}
	if err := cursor.Err(); err != nil {
		return "", errors.New("Fetch Cursor ERROR" + err.Error())
	}

	return applicationConfig.ConfigurationValue, nil
}
