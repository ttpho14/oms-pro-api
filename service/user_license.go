package service

import (
	"../database"
	"../model"
	"../setting"
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

func CreateUserLicense(email string) (err error) {
	collection := database.Database.Collection(setting.UserLicenseTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	count, err := collection.CountDocuments(ctx, bson.M{})
	if err != nil {
		return errors.New("Count License ERROR - " + err.Error())
	}

	if count >= setting.NumberOfLicense {
		return errors.New(fmt.Sprintf("License limit exceeded - Maximum License User [%v] ", setting.NumberOfLicense))
	} else {
		var (
			userLicense model.UserLicense
			currentTime = time.Now()
		)
		userLicense.ID = primitive.NewObjectIDFromTimestamp(currentTime)
		userLicense.Email = email
		userLicense.DateTime = currentTime
		if _, err = collection.InsertOne(ctx, userLicense); err != nil {
			return errors.New("Insert User License ERROR - " + err.Error())
		}
	}

	return nil
}

func DeleteUserLicense(email string) (err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if _, err = database.Database.Collection(setting.UserLicenseTable).DeleteOne(ctx, bson.M{"email": email}); err != nil {
		return errors.New(fmt.Sprintf("Delete User License with email [%s] ERROR - %s",email,err.Error()))
	}

	return nil
}
