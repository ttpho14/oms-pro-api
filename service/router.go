package service

import (
	"../model"
	sHelper "./helper"
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
)

func RouterHandleRequestAndResponse(f http.HandlerFunc, w http.ResponseWriter, r *http.Request, application model.Application) (err error) {
	var (
		bufResponse bytes.Buffer
		//bufResponse1 bytes.Buffer
		bodyRequest interface{}
	)

	//------------- HANDLE REQUEST -------------
	//Duplicate Request Body to read Twice.
	//bodyRequestReader := io.TeeReader(r.Body, &bufRequest)

	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return errors.New("Read All request body ERROR - "+err.Error())
	}

	if err = r.Body.Close();err != nil {
		return errors.New("Close body ERROR - "+err.Error())
	}  //  must close

	r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

	if err = json.Unmarshal(bodyBytes, &bodyRequest); err != nil {
		return errors.New("Unmarshal body ERROR - "+err.Error())
	}

	//---------------- HANDLE RESPONSE -----------------
	//Record the response
	responseRecorder := httptest.NewRecorder()
	f(responseRecorder, r)

	//Write header and header
	for k, v := range responseRecorder.Header() {
		w.Header()[k] = v
	}
	w.WriteHeader(responseRecorder.Code)

	bodyResponseReader := io.TeeReader(responseRecorder.Body, &bufResponse)

	responseBodyBytes, err := ioutil.ReadAll(bodyResponseReader)
	if err != nil {
		return errors.New("Read all response body ERROR - "+err.Error())
	}


	if _, err := bufResponse.WriteTo(w); err != nil {
		return errors.New("Write to response ERROR - "+err.Error())
	}

	_ = InsertAPILog(application.ApplicationId, r.Method, sHelper.GetFunctionName(f), string(bodyBytes), string(responseBodyBytes))

	return nil
}
