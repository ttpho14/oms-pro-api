package service

import (
	"../database"
	"../model"
	"../setting"
	"context"
	"gopkg.in/oauth2.v4/errors"
	"time"
)

func CreateIntegrationLog(serviceName , functionName , log1 , log2 , log3 string) (err error) {
	collection := database.Database.Collection(setting.IntegrationLogTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var log = model.NewIntegrationLog(serviceName , functionName , log1 , log2 , log3)

	if _, err = collection.InsertOne(ctx,log);err != nil {
		return errors.New("Insert Integration Log ERROR - "+err.Error())
	}

	return nil
}
