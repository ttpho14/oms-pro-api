package service

import (
	"../model"
	"../model/enum"
	"fmt"
	"gopkg.in/oauth2.v4/errors"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func GetAllIntegrationServiceInfo() (allServices []model.IntegrationService, err error) {
	var (
		statusProcessList = map[enum.IntegrationService]string{
			enum.IntegrationServiceSIA:            "Sell In All Integration (Order, Inventory)",
			enum.IntegrationServiceEmail:          "Sending Email",
			enum.IntegrationServiceYCHOrder:       "YCH Order Integration",
			enum.IntegrationServiceYCHMY:          "YCH Inventory File Integration for Malaysia",
			enum.IntegrationServiceYCHSG:          "YCH Inventory File Integration for Singapore",
			enum.IntegrationServiceSAPMY:          "SAP Integration for Malaysia",
			enum.IntegrationServiceSAPSG:          "SAP Integration for Singapore",
			enum.IntegrationServiceNovoMind:       "NovoMind Product Master Integration",
			enum.IntegrationServiceSFCC:           "Puma.Com integration",
			enum.IntegrationServiceCyberSource:    "Payment gateway integration",
			enum.IntegrationServiceAPI:            "OMS Pro API (Web, YCH, 3rd Party)",
			enum.IntegrationServiceAPIIntegration: "OMS Pro API for Integration services",
			enum.IntegrationServiceMongo:          "Mongo DB",
			enum.IntegrationServiceWebApp:         "OMS Web App",
		}
		agrStatus  = "check"
		nameSys    = "systemctl"
		serviceObj model.IntegrationService
	)
	for svStatus, desc := range statusProcessList {
		serviceName := string(svStatus)
		serviceObj.ServiceName = svStatus
		serviceObj.Description = desc

		// Get PID
		serviceObj.PID, err = GetIntegrationServiceInfo("pidof", serviceName)
		if err != nil {
			return allServices, errors.New("Cannot get PID of process" + err.Error())
		}
		serviceObj.PID = strings.TrimSpace(strings.ReplaceAll(serviceObj.PID, "\n", ""))

		// Get CPU Info
		cpuService, err := GetIntegrationServiceInfo("ps", "-p", serviceObj.PID, "-o", "%cpu")
		if err != nil {
			return allServices, errors.New("Cannot get CPU and MEM process" + err.Error())
		}

		//Get Memory Info
		memoryService, err := GetIntegrationServiceInfo("ps", "-p", serviceObj.PID, "-o", "%mem")
		if err != nil {
			return allServices, errors.New("Cannot get CPU and MEM process" + err.Error())
		}

		//Assign CPU and Memory value
		cpuArr := strings.Split(cpuService, "\n")
		memoryArr := strings.Split(memoryService, "\n")
		var (
			cpuTotal    float64
			memoryTotal float64
		)
		for i, _ := range cpuArr {
			if i > 0 {
				cpu, _ := strconv.ParseFloat(strings.TrimSpace(cpuArr[i]), 64)
				memory, _ := strconv.ParseFloat(strings.TrimSpace(memoryArr[i]), 64)
				cpuTotal += cpu
				memoryTotal += memory
			}
		}

		serviceObj.CPU = fmt.Sprintf("%f", cpuTotal)
		serviceObj.Memory = fmt.Sprintf("%f", memoryTotal)

		//Get Service status
		serviceObj.Status, err = GetIntegrationServiceInfo(nameSys, agrStatus, serviceName)
		if err != nil {
			return allServices, errors.New("Cannot get Status serviceName" + err.Error())
		}

		serviceObj.Status = strings.TrimSpace(strings.ReplaceAll(serviceObj.Status, "\n", ""))
		//Start if it is stopped.

		allServices = append(allServices, serviceObj)
	}

	return allServices, nil
}

func GetIntegrationServiceInfo(name string, agr ...string) (respInfo string, err error) {
	cmdStatus := exec.Command(name, agr...)
	out, err := cmdStatus.CombinedOutput()
	if err != nil {
		if exitErr, ok := err.(*exec.ExitError); ok {
			fmt.Printf("systemctl finished with non-zero: %s", exitErr)
		} else {
			fmt.Printf("failed to run systemctl: %s", err)
			os.Exit(1)
		}
	}
	respInfo = string(out)
	return respInfo, nil
}

func IntegrationServiceCommand(name string, agr ...string) (err error) {
	cmd := exec.Command(name, agr...)
	err = cmd.Run()
	if err != nil {
		return errors.New("Execute command ERROR - " + err.Error())
	}

	return nil
}
