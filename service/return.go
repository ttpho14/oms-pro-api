package service

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"

	"../database"
	"../model"
	"../model/enum"
	"../setting"
	"./helper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

func HandleReturnStatusChange(docNum string, newStatus enum.ReturnStatus) (returnObj model.Return, err error) {
	var (
		ret        model.ReturnResponse
		updateBson bson.D
		returnColl = database.Database.Collection(setting.ReturnTable)
		//invStatus   model.InventoryStatus
	)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	newStatus, err = newStatus.IsValid()
	if err != nil {
		return returnObj, errors.New(fmt.Sprintf("New Return status '%s' is invalid", newStatus))
	}

	//invStatusCollection := database.Database.Collection(setting.InventoryStatusTable)

	ret, err = GetReturnWithDetails(docNum)
	if err != nil {
		return returnObj, errors.New(fmt.Sprintf("Get Return Order with Details ERROR - '%s'", err.Error()))
	}

	//if newStatus != enum.ReturnStatusNew && ret.DocStatus == newStatus {
	//	return errors.New(fmt.Sprintf("New Return Status '%s' same with old Return status", newStatus))
	//} else {
	//
	//}return

	//if !IsValidReturnStatusChange(ret.DocStatus, newStatus) {
	//	return errors.New(fmt.Sprintf("Return Status '%s' is not allowed to change to '%s'", ret.DocStatus, newStatus))
	//}

	ret.DocStatus = newStatus

	b, _ := json.Marshal(ret)

	switch newStatus {
	case enum.ReturnStatusRejected:
		if strings.ToUpper(ret.SalesChannelId) == setting.SalesChannelPumaCom {
			if err = InsertQueue(enum.AppIdOMS, enum.AppIdSFCC, setting.ReturnTable, ret.DocNum, string(b), "", "", "", false); err != nil {
				return returnObj, err
			}
		}

	case enum.ReturnStatusReceived:
		for index, detail := range ret.ReturnDetail {
			err = HandleReturnDetailStatusChange(ret.DocNum, detail.DocLineNum, enum.ReturnDetailStatusReceived)
			if err != nil {
				fmt.Println(fmt.Sprintf("Return Detail - '%s' Status Change ERROR - %s", detail.DocLineNum, err.Error()))
			} else {
				fmt.Println(fmt.Sprintf("Return Detail - '%s' Status Change SUCCESS", detail.DocLineNum))
				ret.ReturnDetail[index].DocLineStatus = enum.ReturnDetailStatusReceived
			}
		}

		if strings.ToUpper(ret.SalesChannelId) == setting.SalesChannelPumaCom {
			b, _ = json.Marshal(ret)
			if err = InsertQueue(enum.AppIdOMS, enum.AppIdSFCC, setting.ReturnTable, ret.DocNum, string(b), "", "", "", false); err != nil {
				return returnObj, err
			}
			if err = InsertQueue(enum.AppIdOMS, enum.AppIdEmail, "return_received", ret.DocNum, string(b), "", "", "", false); err != nil {
				return returnObj, err
			}
		}

	case enum.ReturnStatusReturned:
		if ret.SAPExport == false {
			ret.SAPExport = true
			b, _ = json.Marshal(ret)
			if err = InsertQueue(enum.AppIdOMS, enum.AppIdSAP, setting.ReturnTable, ret.DocNum, string(b), "", "", "", false); err != nil {
				return returnObj, err
			}
		}

		if strings.ToUpper(ret.SalesChannelId) == setting.SalesChannelPumaCom {
			b, _ = json.Marshal(ret)
			if err = InsertQueue(enum.AppIdOMS, enum.AppIdSFCC, setting.ReturnTable, ret.DocNum, string(b), "", "", "", false); err != nil {
				return returnObj, err
			}
			if err = InsertQueue(enum.AppIdOMS, enum.AppIdEmail, setting.ReturnTable, ret.DocNum, string(b), "", "", "", false); err != nil {
				return returnObj, err
			}
		}

	case enum.ReturnStatusAccepted:
		if ret.SAPExport == false {
			ret.SAPExport = true
			b, _ = json.Marshal(ret)

			if err = InsertQueue(enum.AppIdOMS, enum.AppIdSAP, setting.ReturnTable, ret.DocNum, string(b), "", "", "", false); err != nil {
				return returnObj, err
			}
		}

		if strings.ToUpper(ret.SalesChannelId) == setting.SalesChannelPumaCom {
			b, _ = json.Marshal(ret)
			if err = InsertQueue(enum.AppIdOMS, enum.AppIdSFCC, setting.ReturnTable, ret.DocNum, string(b), "", "", "", false); err != nil {
				return returnObj, err
			}
			if err = InsertQueue(enum.AppIdOMS, enum.AppIdCyberSource, "refunds", ret.DocNum, string(b), "", "", "", false); err != nil {
				return returnObj, err
			}
		}

	case enum.ReturnStatusRefunded:
		/*
			if strings.ToUpper(ret.SalesChannelId) == setting.SalesChannelPumaCom {
				if err = InsertQueue(enum.AppIdOMS, enum.AppIdSFCC, setting.ReturnTable, ret.DocNum, string(b), "", "", "", false); err != nil {
					return returnObj, err
				}
				if err = InsertQueue(enum.AppIdOMS, enum.AppIdEmail, "refunded_order", ret.DocNum, string(b), "", "", "", false); err != nil {
					return returnObj,err
				}
			}
		*/
	case enum.ReturnStatusClosed:
	}

	if ret.SAPExport == true {
		updateBson = append(updateBson, bson.E{Key: "sap_export", Value: ret.SAPExport})
	}

	updateBson = append(updateBson,
		bson.E{Key: "doc_status", Value: newStatus},
		bson.E{Key: "updated_date", Value: time.Now()},
	)

	filter := bson.M{"_id": bsonx.ObjectID(ret.ID)}
	update := bson.M{"$set": updateBson}

	_, err = returnColl.UpdateOne(ctx, filter, update)
	if err != nil {
		return returnObj, errors.New(fmt.Sprintf("Update Return Order Status ERROR - " + err.Error()))
	}

	countFilter := bson.M{
		"doc_num":      docNum,
		"doc_line_num": "",
		"to_status":    newStatus,
	}

	count, err := database.Database.Collection(setting.ReturnStatusTable).CountDocuments(ctx, countFilter)
	if err != nil {
		return returnObj, errors.New(fmt.Sprintf("Count Return Order with new status ERROR - " + err.Error()))
	}

	if count == 0 {
		if err = CreateReturnStatusChange(ret.DocNum, "", string(ret.DocStatus.ToCorrectCase()), string(newStatus)); err != nil {
			return returnObj, errors.New(fmt.Sprintf("Create Return Order Status change ERROR - " + err.Error()))
		}
	}

	if err = returnColl.FindOne(ctx, filter).Decode(&returnObj); err != nil {
		return returnObj, errors.New(fmt.Sprintf("Find Return Order ERROR - " + err.Error()))
	}

	return returnObj, nil
}

func HandleReturnPaymentStatusChange(docNum string, newStatus string) (returnObj model.Return, err error) {
	var (
		ret        model.ReturnResponse
		updateBson bson.D
		returnColl = database.Database.Collection(setting.ReturnTable)
	)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//newStatus, err = newStatus.IsValid()
	//if err != nil {
	//	return returnObj,errors.New(fmt.Sprintf("New Return status '%s' is invalid", newStatus))
	//}

	newStatus = strings.ToUpper(strings.ReplaceAll(strings.Trim(newStatus, " "), " ", "_"))

	//invStatusCollection := database.Database.Collection(setting.InventoryStatusTable)

	ret, err = GetReturnWithDetails(docNum)
	if err != nil {
		return returnObj, errors.New(fmt.Sprintf("Get Return Order with Details ERROR - '%s'", err.Error()))
	}

	//if newStatus != enum.ReturnStatusNew && ret.DocStatus == newStatus {
	//	return returnObj,errors.New(fmt.Sprintf("New Return Status '%s' same with old Return status", newStatus))
	//} else {
	//
	//}

	//if !IsValidReturnStatusChange(ret.DocStatus, newStatus) {
	//	return returnObj,errors.New(fmt.Sprintf("Return Status '%s' is not allowed to change to '%s'", ret.DocStatus, newStatus))
	//}
	ret.PaymentStatus = newStatus

	switch newStatus {
	case "REFUNDED":
		b, err := json.Marshal(ret)
		if err != nil {
			return returnObj, errors.New(fmt.Sprintf("Marshal Return Order ERROR - '%s'", err.Error()))
		}

		if ret.SAPExport == false {
			//ret.PaymentStatus = strings.ToUpper(strings.ReplaceAll(strings.Trim(ret.PaymentStatus, " "), " ", "_"))
			//if ret.PaymentStatus == newStatus {
			//}
			ret.SAPExport = true

			b, err := json.Marshal(ret)
			if err != nil {
				return returnObj, errors.New(fmt.Sprintf("Marshal Return Order ERROR - '%s'", err.Error()))
			}

			if err = InsertQueue(enum.AppIdOMS, enum.AppIdSAP, setting.ReturnTable, ret.DocNum, string(b), "", "", "", false); err != nil {
				return returnObj, err
			}
		}

		if strings.ToUpper(ret.SalesChannelId) == setting.SalesChannelPumaCom {
			if err = InsertQueue(enum.AppIdOMS, enum.AppIdSFCC, setting.ReturnTable, ret.DocNum, string(b), "", "", "", false); err != nil {
				return returnObj, err
			}
			if err = InsertQueue(enum.AppIdOMS, enum.AppIdEmail, "refunded_order", ret.DocNum, string(b), "", "", "", false); err != nil {
				return returnObj, err
			}
		}

	}

	if ret.SAPExport == true {
		updateBson = append(updateBson, bson.E{Key: "sap_export", Value: ret.SAPExport})
	}

	updateBson = append(updateBson,
		bson.E{Key: "payment_status", Value: ret.PaymentStatus},
		bson.E{Key: "updated_date", Value: time.Now()},
	)

	filter := bson.M{"_id": bsonx.ObjectID(ret.ID)}
	update := bson.M{"$set": updateBson}

	_, err = returnColl.UpdateOne(context.Background(), filter, update)
	if err != nil {
		return returnObj, errors.New(fmt.Sprintf("Update Return Order Status ERROR - " + err.Error()))
	}

	if err = returnColl.FindOne(ctx, filter).Decode(&returnObj); err != nil {
		return returnObj, errors.New(fmt.Sprintf("Find Return Order ERROR - " + err.Error()))
	}

	return returnObj, nil
}

func GetReturnWithDetails(docNum string) (ret model.ReturnResponse, err error) {
	returnCollection := database.Database.Collection(setting.ReturnTable)

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	//------------------- GET Return Order ------------------
	//Match with Id
	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.D{{"doc_num", docNum}},
			},
			},
		},
	}}
	//Look up for foreign key
	//lookupCountry := bson.D{
	//	{
	//		"$lookup", bson.D{
	//		{"from", "country"},
	//		{"localField", "country_key"},
	//		{"foreignField", "_id"},
	//		{"as", "country"},
	//	},
	//	}}
	lookupDetails := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.ReturnDetailTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", "return_details"},
			},
		}}
	lookupPayments := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.ReturnPaymentTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", "return_payments"},
			},
		}}
	lookupComment := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.ReturnCommentTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", setting.ReturnCommentTable + "s"},
			},
		}}

	lookupShippingDetail := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.ReturnShippingDetailTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", setting.ReturnShippingDetailTable + "s"},
			},
		}}

	lookupStatus := bson.D{
		{"$lookup", bson.D{
			{"from", setting.ReturnStatusTable},
			{"let", bson.M{"dn": "$doc_num"}},
			{"pipeline", bson.A{
				bson.M{"$match": bson.M{
					"$expr": bson.M{
						"$and": bson.A{
							bson.M{"$eq": bson.A{"$doc_num", "$$dn"}},
							bson.M{"$eq": bson.A{"$doc_line_num", ""}},
						},
					},
				}},
			}},
			{"as", setting.ReturnStatusTable},
		},
		}}

	lookupReturnDetails := bson.D{
		{"$lookup", bson.D{
			{"from", setting.ReturnDetailTable},
			{"localField", "base_doc_key"},
			{"foreignField", "doc_key"},
			{"as", setting.ReturnDetailTable},
		},
		}}

	sumReturnDetailQuantity := bson.D{
		{"$addFields", bson.D{
			{"order_quantity", bson.D{
				{"$sum", "$" + setting.ReturnDetailTable + ".quantity"},
			}},
		}},
	}
	sumReturnQuantity := bson.D{
		{"$addFields", bson.D{
			{"return_quantity", bson.D{
				{"$sum", "$" + setting.ReturnDetailTable + ".return_quantity"},
			}},
		}},
	}
	//Unwind array to object
	//unwindCurrency := bson.D{
	//	{"$unwind", bson.D{
	//		{"path", "$currency"},
	//		{"preserveNullAndEmptyArrays", true}},
	//	},
	//}
	//sort, skip and limit
	pipeline := mongo.Pipeline{
		match,
		lookupDetails,
		lookupPayments,
		lookupComment,
		lookupShippingDetail,
		lookupStatus,
		lookupReturnDetails,
		sumReturnDetailQuantity,
		sumReturnQuantity,
	}

	cursor, err := returnCollection.Aggregate(ctx, pipeline) //findOptions)
	if err != nil {
		return model.ReturnResponse{}, err
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		err := cursor.Decode(&ret)
		if err != nil {
			return model.ReturnResponse{}, err
		}
		//salesOrders = append(salesOrders, salesOrder)
	}
	if err := cursor.Err(); err != nil {
		return model.ReturnResponse{}, err
	}

	if ret.ID == primitive.NilObjectID {
		return model.ReturnResponse{}, errors.New("No document in Database")
	}

	return ret, nil
}

func IsValidReturnStatusChange(oldStatus enum.ReturnStatus, newStatus enum.ReturnStatus) bool {
	switch oldStatus {
	case "":
		return helper.IsItemExistsInArray([]enum.ReturnStatus{}, newStatus)
	case enum.ReturnStatusReturned:
		return helper.IsItemExistsInArray([]enum.ReturnStatus{}, newStatus)
	default: // Case when Old Status == Closed ===>>> Will return false
		return false
	}
}
