package service

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"

	"../database"
	"../model"
	"../model/enum"
	"../setting"
	"./helper"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

func CreateOrderCancellation(cancelReq model.OrderCancellationRequest, body []byte) (orderCancellationResponse model.OrderCancellationResponse, err error) {
	var (
		collection           *mongo.Collection
		orderCancellation    model.OrderCancellation
		salesOrder           model.SalesOrder
		currentTime          = time.Now()
		salesOrderDetailColl = database.Database.Collection(setting.SalesOrderDetailTable)
		inventoryStatusColl  = database.Database.Collection(setting.InventoryStatusTable)
	)

	collection = database.Database.Collection(setting.OrderCancellationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Minute)
	defer cancel()

	//err = json.NewDecoder(request.Body).Decode(&cancelReq)

	if err = cancelReq.Validation(); err != nil {
		return orderCancellationResponse, errors.New("Validation - " + err.Error())
	}

	_ = database.Database.Collection(setting.SalesOrderTable).FindOne(ctx, bson.M{"doc_num": cancelReq.BaseDocNum}).Decode(&salesOrder)

	count, err := database.Database.Collection(setting.OrderCancellationTable).CountDocuments(ctx, bson.M{"base_doc_num": cancelReq.BaseDocNum})
	if err != nil {
		return orderCancellationResponse, errors.New("Count Return document ERROR - " + err.Error())
	}

	_ = copier.Copy(&orderCancellation, &salesOrder)

	_ = copier.Copy(&orderCancellation, &cancelReq)

	orderCancellation.ID = primitive.NewObjectIDFromTimestamp(time.Now())
	orderCancellation.DocNum = fmt.Sprintf("%s_CC%02d", orderCancellation.BaseDocNum, count+1)
	orderCancellation.BaseDocKey = salesOrder.ID
	orderCancellation.OrderDate = salesOrder.DocDate
	orderCancellation.DocDate = currentTime
	orderCancellation.TotalOrderAmount = salesOrder.TotalAfterTax
	if orderCancellation.ReasonKey == primitive.NilObjectID {
		orderCancellation.ReasonKey, _ = helper.GetKeyFromId(setting.ReasonTable, "reason_id", orderCancellation.ReasonId)
	}
	//orderCancellation.DocDate = time.Unix(cancelReq.DocDate, 0)
	//orderCancellation.DocDueDate = time.Unix(cancelReq.DocDueDate, 0)
	//orderCancellation.SiteId = salesOrder.SiteId
	//orderCancellation.AccountCode = salesOrder.AccountCode
	orderCancellation.DocStatus = enum.CancelOrderStatusCancelled
	//orderCancellation.Email = salesOrder.Email
	//orderCancellation.BillingAddress = salesOrder.BillingAddress
	//orderCancellation.BillingAddressKey = salesOrder.ShippingAddressKey
	//orderCancellation.ShippingAddress = salesOrder.ShippingAddress
	//orderCancellation.ShippingAddressKey = salesOrder.ShippingAddressKey
	orderCancellation.ObjectType = setting.OrderCancellationTable
	orderCancellation.CreatedDate = currentTime
	orderCancellation.UpdatedDate = currentTime

	if _, err = collection.InsertOne(ctx, orderCancellation); err != nil {
		return orderCancellationResponse, errors.New("Insert Order Cancellation ERROR - " + err.Error())
	}

	orderCancellation.TotalAfterTax = 0

	// Order Cancellation Detail
	for _, detail := range cancelReq.OrderCancellationDetailRequest {
		var (
			inventoryStatus                 model.InventoryStatus
			orderCancellationDetail         model.OrderCancellationDetail
			orderCancellationDetailResponse model.OrderCancellationDetailResponse
			salesOrderDetail                model.SalesOrderDetail
		)
		orderCancellationDetailColl := database.Database.Collection(setting.OrderCancellationDetailTable)

		salesOrderDetailFilter := bson.M{"doc_num": cancelReq.BaseDocNum, "doc_line_num": detail.BaseDocLineNum}
		_ = salesOrderDetailColl.FindOne(ctx, salesOrderDetailFilter).Decode(&salesOrderDetail)

		detailCount, err := orderCancellationDetailColl.CountDocuments(ctx, bson.M{"base_doc_num": orderCancellation.BaseDocNum, "base_doc_line_num": salesOrderDetail.DocLineNum})
		if err != nil {
			return orderCancellationResponse, errors.New("Count Return Detail ERROR - " + err.Error())
		}

		_ = copier.Copy(&orderCancellationDetail, &salesOrderDetail)

		_ = copier.Copy(&orderCancellationDetail, &detail)

		orderCancellationDetail.ID = primitive.NewObjectIDFromTimestamp(time.Now())
		orderCancellationDetail.DocKey = orderCancellation.ID
		orderCancellationDetail.DocNum = orderCancellation.DocNum
		orderCancellationDetail.DocLineNum = fmt.Sprintf("%s_CC%02d", salesOrderDetail.DocLineNum, detailCount+1)
		orderCancellationDetail.BaseDocKey = salesOrder.ID
		orderCancellationDetail.BaseDocNum = orderCancellation.BaseDocNum
		orderCancellationDetail.BaseDocLineKey = salesOrderDetail.ID
		orderCancellationDetail.BaseDocLineNum = salesOrderDetail.DocLineNum
		//orderCancellationDetail.DocLineStatus = enum.CancelOrderStatusLineCancelled
		//orderCancellationDetail.ProductCode = salesOrderDetail.ProductCode
		//orderCancellationDetail.ProductColor = salesOrderDetail.ProductColor
		//orderCancellationDetail.ProductKey = salesOrderDetail.ProductKey
		//orderCancellationDetail.ProductCode = salesOrderDetail.ProductCode
		orderCancellationDetail.ObjectType = setting.OrderCancellationDetailTable
		orderCancellationDetail.OrderQuantity = salesOrderDetail.Quantity
		orderCancellationDetail.CreatedDate = currentTime
		orderCancellationDetail.UpdatedDate = currentTime

		// Insert Order Cancellation Detail
		if _, err := orderCancellationDetailColl.InsertOne(ctx, orderCancellationDetail); err != nil {
			return orderCancellationResponse, errors.New("Insert Order Cancellation Detail ERROR - " + err.Error())
		}

		// Recalculate OrderCancellation
		orderCancellation.TotalAfterTax += orderCancellationDetail.PriceAfterTax*orderCancellationDetail.Quantity - orderCancellationDetail.LineDiscountAmount

		// Update Order Cancellation detail
		salesOrderDetail.CancelQuantity = salesOrderDetail.CancelQuantity + orderCancellationDetail.Quantity
		salesOrderDetail.UpdatedDate = currentTime
		salesOrderDetailUpdate := bson.M{"$set": salesOrderDetail}
		if _, err := salesOrderDetailColl.UpdateOne(ctx, salesOrderDetailFilter, salesOrderDetailUpdate); err != nil {
			return orderCancellationResponse, errors.New("Update Order Cancellation Detail ERROR - " + err.Error())
		}

		//Get Inventory Status and Update OnOrder >>>>> Update On Available Quantity
		inventoryFilter := bson.M{"product_id": orderCancellationDetail.ProductCode, "site_id": salesOrder.SiteId}
		if err = inventoryStatusColl.FindOne(ctx, inventoryFilter).Decode(&inventoryStatus); err != nil {
			return orderCancellationResponse, errors.New("Find Product Code and Site Id in Inventory Status ERROR - " + err.Error())
		}

		inventoryStatus.OnOrderQty = inventoryStatus.OnOrderQty - orderCancellationDetail.Quantity
		inventoryStatus.OnAvailableQty = inventoryStatus.OnHandQty - inventoryStatus.OnOrderQty
		inventoryStatus.UpdatedDate = currentTime

		inventoryStatusUpdate := bson.M{"$set": inventoryStatus}
		if _, err = inventoryStatusColl.UpdateOne(ctx, inventoryFilter, inventoryStatusUpdate); err != nil {
			return orderCancellationResponse, errors.New("Update Inventory Status ERROR - " + err.Error())
		}

		if salesOrderDetail.CancelQuantity == salesOrderDetail.Quantity {
			if err = HandleSalesOrderDetailStatusChange(salesOrderDetail.DocNum, salesOrderDetail.DocLineNum, enum.OrderLineStatusCancelled); err != nil {
				return orderCancellationResponse, errors.New("Handle Order Cancellation Detail Status change ERROR - " + err.Error())
			}
		}

		_ = copier.Copy(&orderCancellationDetailResponse, &orderCancellationDetail)
		orderCancellationResponse.OrderCancellationDetailResponse = append(orderCancellationResponse.OrderCancellationDetailResponse, orderCancellationDetailResponse)
		//uiOrderCancellationDetail = append(uiOrderCancellationDetail, orderCancellationDetail)
	}

	orderCancellation.TotalAfterTax = orderCancellation.TotalAfterTax + orderCancellation.FreightAmount + orderCancellation.TaxAmount - orderCancellation.DiscountAmount

	_ = copier.Copy(&orderCancellationResponse, &orderCancellation)

	if _, err = collection.UpdateOne(ctx, bson.M{"doc_num": orderCancellation.DocNum}, bson.M{"$set": bson.M{"total_after_tax": orderCancellation.TotalAfterTax}}); err != nil {
		return orderCancellationResponse, errors.New("Update Order Cancellation Total after tax ERROR - " + err.Error())
	}

	//Order cancellation payment
	/*
		for _, payment := range cancelReq.OrderCancellationPaymentRequest {
			var (
				orderCancellationPayment         model.OrderCancellationPayment
				orderCancellationPaymentResponse model.OrderCancellationPaymentResponse
			)
			orderCancellationPaymentColl := database.Database.Collection(setting.OrderCancellationPaymentTable)
			_ = copier.Copy(&orderCancellationPayment, &payment)
			orderCancellationPayment.ID = primitive.NewObjectIDFromTimestamp(time.Now())
			orderCancellationPayment.DocKey = orderCancellation.ID
			orderCancellationPayment.ObjectType = setting.OrderCancellationPaymentTable
			orderCancellationPayment.CreatedDate = currentTime
			orderCancellationPayment.UpdatedDate = currentTime
			if _, err := orderCancellationPaymentColl.InsertOne(ctx, orderCancellationPayment); err != nil {
				return orderCancellationResponse, errors.New("Insert Order Cancellation Payment ERROR - " + err.Error())
			}

			_ = copier.Copy(&orderCancellationPaymentResponse, &orderCancellationPayment)
			orderCancellationResponse.OrderCancellationPaymentResponse = append(orderCancellationResponse.OrderCancellationPaymentResponse, orderCancellationPaymentResponse)
		}
	*/

	var salesOrderPayments []model.SalesOrderPayment
	salesOrderPaymentCursor, err := database.Database.Collection(setting.SalesOrderPaymentTable).Find(ctx, bson.M{"doc_key": bsonx.ObjectID(salesOrder.ID)})
	if err != nil {
		return orderCancellationResponse, errors.New("Find Base Sales Order Payment ERROR - " + err.Error())
	}

	if err = salesOrderPaymentCursor.All(ctx, &salesOrderPayments); err != nil {
		return orderCancellationResponse, errors.New("Fetch Base Sales Order Payment into array model ERROR - " + err.Error())
	}

	for _, payment := range salesOrderPayments {
		var (
			orderCancellationPayment         model.OrderCancellationPayment
			orderCancellationPaymentResponse model.OrderCancellationPaymentResponse
		)
		orderCancellationPaymentColl := database.Database.Collection(setting.OrderCancellationPaymentTable)
		_ = copier.Copy(&orderCancellationPayment, &payment)
		orderCancellationPayment.ID = primitive.NewObjectID()
		orderCancellationPayment.DocKey = orderCancellation.ID
		orderCancellationPayment.ObjectType = setting.OrderCancellationPaymentTable
		orderCancellationPayment.CreatedDate = currentTime
		orderCancellationPayment.UpdatedDate = currentTime
		if _, err := orderCancellationPaymentColl.InsertOne(ctx, orderCancellationPayment); err != nil {
			return orderCancellationResponse, errors.New("Insert Order Cancellation Payment ERROR - " + err.Error())
		}

		_ = copier.Copy(&orderCancellationPaymentResponse, &orderCancellationPayment)
		orderCancellationResponse.OrderCancellationPaymentResponse = append(orderCancellationResponse.OrderCancellationPaymentResponse, orderCancellationPaymentResponse)
	}

	for _, comment := range cancelReq.OrderCancellationCommentRequest {
		var (
			orderCancellationComment         model.OrderCancellationComment
			orderCancellationCommentResponse model.OrderCancellationCommentResponse
		)
		orderCancellationCommentColl := database.Database.Collection(setting.OrderCancellationCommentTable)
		_ = copier.Copy(&orderCancellationComment, &comment)
		orderCancellationComment.ID = primitive.NewObjectIDFromTimestamp(time.Now())
		orderCancellationComment.DocKey = orderCancellation.ID
		orderCancellationComment.ObjectType = setting.OrderCancellationCommentTable
		orderCancellationComment.CreatedDate = currentTime
		orderCancellationComment.UpdatedDate = currentTime
		if _, err := orderCancellationCommentColl.InsertOne(ctx, orderCancellationComment); err != nil {
			return orderCancellationResponse, errors.New("Insert Order Cancellation Comment ERROR - " + err.Error())
		}
		_ = copier.Copy(&orderCancellationCommentResponse, &orderCancellationComment)
		orderCancellationResponse.OrderCancellationCommentResponse = append(orderCancellationResponse.OrderCancellationCommentResponse, orderCancellationCommentResponse)
	}

	for _, shipping := range cancelReq.OrderCancellationShippingDetailRequest {
		var (
			orderCancellationShippingDetail         model.OrderCancellationShippingDetail
			orderCancellationShippingDetailResponse model.OrderCancellationShippingDetailResponse
		)
		orderCancellationCommentColl := database.Database.Collection(setting.OrderCancellationShippingDetailTable)
		_ = copier.Copy(&orderCancellationShippingDetail, &shipping)
		orderCancellationShippingDetail.ID = primitive.NewObjectIDFromTimestamp(time.Now())
		orderCancellationShippingDetail.DocKey = orderCancellation.ID
		orderCancellationShippingDetail.ObjectType = setting.OrderCancellationShippingDetailTable
		orderCancellationShippingDetail.CreatedDate = currentTime
		orderCancellationShippingDetail.UpdatedDate = currentTime
		if _, err := orderCancellationCommentColl.InsertOne(ctx, orderCancellationShippingDetail); err != nil {
			return orderCancellationResponse, errors.New("Insert Order Cancellation Shipping Detail ERROR - " + err.Error())
		}
		_ = copier.Copy(&orderCancellationShippingDetailResponse, &orderCancellationShippingDetail)
		orderCancellationResponse.OrderCancellationShippingDetailResponse = append(orderCancellationResponse.OrderCancellationShippingDetailResponse, orderCancellationShippingDetailResponse)
	}

	//if len(uiOrderCancellationDetail) > 0 {
	//	collection = database.Database.Collection(setting.OrderCancellationDetailTable)
	//	// Order Cancellation Detail
	//	_, err = collection.InsertMany(context.TODO(), uiOrderCancellationDetail)
	//	if err != nil {
	//		aHelper.ErrorResponse(response, http.StatusInternalServerError,0, "Insert Order Cancellation Detail ERROR - "+err.Error())
	//		return
	//	}
	//} else {
	//	aHelper.ErrorResponse(response, http.StatusInternalServerError,0, "Please enter at least one line for order cancellation detail")
	//	return
	//}

	//if len(uiOrderCancellationPayment) > 0 {
	//	collection = database.Database.Collection(setting.OrderCancellationPaymentTable)
	//	// Order Cancellation Detail
	//	_, err = collection.InsertMany(context.TODO(), uiOrderCancellationPayment)
	//	if err != nil {
	//		aHelper.ErrorResponse(response, http.StatusInternalServerError,0, "Insert Order Cancellation Payment ERROR - "+err.Error())
	//		return
	//	}
	//}

	//if len(uiOrderCancellationComment) > 0 {
	//	collection = database.Database.Collection(setting.OrderCancellationCommentTable)
	//	// Order Cancellation Detail
	//	_, err = collection.InsertMany(context.TODO(), uiOrderCancellationPayment)
	//	if err != nil {
	//		aHelper.ErrorResponse(response, http.StatusInternalServerError,0, "Insert Order Cancellation Comment ERROR - "+err.Error())
	//		return
	//	}
	//}

	byteOrderCancel, _ := json.Marshal(orderCancellationResponse)

	countLine, _ := salesOrderDetailColl.CountDocuments(ctx, bson.M{"doc_num": salesOrder.DocNum})
	countLineStatus, _ := salesOrderDetailColl.CountDocuments(ctx, bson.M{"doc_num": salesOrder.DocNum, "doc_line_status": enum.OrderLineStatusCancelled})

	toUpdateSalesChannelId := strings.ToUpper(orderCancellationResponse.SalesChannelId)

	if countLine == countLineStatus {
		_, err = HandleOrderStatusChange(salesOrder.DocNum, enum.OrderStatusCancelled, false)
		if err != nil {
			return orderCancellationResponse, errors.New("Handle Order Cancellation Status change ERROR - " + err.Error())
		}
	}

	if toUpdateSalesChannelId == setting.SalesChannelPumaCom {
		if err = InsertQueue(enum.AppIdOMS, enum.AppIdEmail, setting.OrderCancellationTable, orderCancellationResponse.DocNum, string(byteOrderCancel), "", "", "", false); err != nil {
			return orderCancellationResponse, errors.New(fmt.Sprintf("Insert Queue from [%s] to [%s] ERROR - %s", enum.AppIdOMS, enum.AppIdEmail, err.Error()))
		}

		//If cancel all detail in order
		if countLine == countLineStatus {
			if err = InsertQueue(enum.AppIdOMS, enum.AppIdCyberSource, "reversals", orderCancellationResponse.DocNum, string(byteOrderCancel), "", "", "", false); err != nil {
				return orderCancellationResponse, errors.New(fmt.Sprintf("Insert Queue from [%s] to [%s] ERROR - %s", enum.AppIdOMS, enum.AppIdCyberSource, err.Error()))
			}
		}
	}

	if cancelReq.CancelRequest == true {
		if toUpdateSalesChannelId == setting.SalesChannelPumaCom {
			err = InsertQueue(enum.AppIdOMS, enum.AppIdSFCC, setting.OrderCancellationTable, orderCancellationResponse.DocNum, string(byteOrderCancel), "", "", "", false)
			if err != nil {
				return orderCancellationResponse, errors.New(fmt.Sprintf("Insert Queue from [%s] to [%s] ERROR - %s", enum.AppIdOMS, enum.AppIdSFCC, err.Error()))
			}
		} else if toUpdateSalesChannelId == setting.SalesChannelLazada || toUpdateSalesChannelId == setting.SalesChannelZalora {
			err = InsertQueue(enum.AppIdOMS, enum.AppIdSIA, setting.OrderCancellationTable, orderCancellationResponse.DocNum, string(byteOrderCancel), "", "", "", false)
			if err != nil {
				return orderCancellationResponse, errors.New(fmt.Sprintf("Insert Queue from [%s] to [%s] ERROR - %s", enum.AppIdOMS, enum.AppIdSIA, err.Error()))
			}
		}

		err = InsertQueue(enum.AppIdOMS, enum.AppIdYCH, setting.OrderCancellationTable, orderCancellationResponse.DocNum, string(byteOrderCancel), "", "", "", false)
		if err != nil {
			return orderCancellationResponse, errors.New(fmt.Sprintf("Insert Queue from [%s] to [%s] ERROR - %s", enum.AppIdOMS, enum.AppIdYCH, err.Error()))
		}
	}

	return orderCancellationResponse, nil
}
