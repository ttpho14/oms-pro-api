package service

import (
	"../database"
	"../model"
	"../setting"
	"context"
	"errors"
	"time"
)

func InsertUserPassword(email, password string) (err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var userPass = model.NewUserPassword(email, password)

	if _, err = database.Database.Collection(setting.UserPasswordTable).InsertOne(ctx, userPass); err != nil {
		return errors.New("Insert User Password ERROR - " + err.Error())
	}

	return nil
}
