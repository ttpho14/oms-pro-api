package service

import (
	"../database"
	"../model"
	"../setting"
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

func InsertQueue(fromAppId, toAppId, sourceObjType, sourceObjId, jsonData, responseData, sendData, errMsg string, status bool) (err error) {

	var (
		currentTime = time.Now()
		col         = database.Database.Collection(setting.QueueTable)
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	/// Assign value for queue
	var queue = model.Queue{
		ID:                primitive.NewObjectIDFromTimestamp(currentTime),
		FromApplicationId: fromAppId,
		ToApplicationId:   toAppId,
		SourceObjectType:  sourceObjType,
		SourceObjectId:    sourceObjId,
		JsonData:          jsonData,
		SendData:          sendData,
		ResponseData:      responseData,
		Status:            status,
		ErrorMessage:      errMsg,
		CreatedDate:       currentTime,
		UpdatedDate:       currentTime,
	}

	//queue.ID = primitive.NewObjectID()
	//queue.FromApplicationId = fromAppId
	//queue.ToApplicationId = toAppId
	//queue.SourceObjectType = sourceObjType
	//queue.SourceObjectId = sourceObjId
	//queue.JsonData = jsonData
	//queue.ResponseData = responseData
	//queue.SendData = sendData
	//queue.Status = status
	//queue.CreatedDate = currentTime
	//queue.UpdatedDate = currentTime

	_, err = col.InsertOne(ctx, queue)
	if err != nil {
		return errors.New(fmt.Sprintf("Insert Queue from [%s] to [%s] ERROR - %s", fromAppId, toAppId, err.Error()))
	}
	return nil
}
