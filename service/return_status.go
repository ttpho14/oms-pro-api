package service

import (
	"../database"
	"../model"
	"../setting"
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

func CreateReturnStatusChange(docNum, docLineNum, fromStatus, toStatus string) (err error) {
	var (
		sOS = model.SalesOrderStatus{
			ID:         primitive.NewObjectIDFromTimestamp(time.Now()),
			DocNum:     docNum,
			DocLineNum: docLineNum,
			FromStatus: fromStatus,
			ToStatus:   toStatus,
			Date:       time.Now(),
		}
		collection = database.Database.Collection(setting.ReturnStatusTable)
	)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if _, err = collection.InsertOne(ctx, sOS); err != nil {
		return errors.New("Insert Sales Order Status change ERROR - " + err.Error())
	}

	return nil
}
