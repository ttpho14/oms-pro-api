package validation

import (
	"../database"
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/go-playground/validator.v9"
	"net/http"
	"regexp"
)

var response http.ResponseWriter
var request *http.Request

func ValidateKey(key primitive.ObjectID, colName string) {
	fmt.Println("validate key: ", key)
	if !primitive.ObjectID.IsZero(key) {
		collection := database.Database.Collection(colName)
		//ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		//defer cancel()
		filter := bson.M{"_id": bson.M{"$eq": key}}
		var result bson.M
		err := collection.FindOne(context.TODO(), filter).Decode(&result)
		if err != nil {
			err = mongo.ErrNoDocuments
			response.WriteHeader(http.StatusConflict)
			response.Write([]byte(colName + "_key does not exist"))
			return
		}
		return
	}
	response.Write([]byte(colName + "_key is invalid"))
	return
}

func IsValidEmail(email string) bool{
	return regexp.MustCompile(`^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$`).MatchString(email)
}

func CheckPhone(phone string) {
	re := regexp.MustCompile(`^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$`)
	if !re.MatchString(phone) {
		response.WriteHeader(http.StatusBadRequest)
		return
	}
}

func CheckNul(name interface{}) {
	validate := validator.New()
	err := validate.Struct(name)
	if err != nil {
		response.WriteHeader(http.StatusNotFound)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
}
