package logger

import (
	"os"
	"time"

	"github.com/Sirupsen/logrus"
	"gopkg.in/natefinch/lumberjack.v2"
)

//Logger var
var Logger = logrus.New()

func InIt() {
	//gotenv.Load(".env")
	var filePath = os.Getenv("LOGGER_PATH")
	filePath = filePath + "/" + time.Now().Format("01-02-2006") + ".log"

	Logger.SetFormatter(&logrus.TextFormatter{
		DisableColors:   false,
		TimestampFormat: "02-01-2006 15:04:05",
	})
	Logger.SetOutput(&lumberjack.Logger{
		Filename: filePath,
		MaxSize:  1, // megabytes
	})
}
