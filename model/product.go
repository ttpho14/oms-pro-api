package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Product struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5f02aab989bfe6971405ae74"`
	ProductGroupKey       primitive.ObjectID `json:"product_group_key" bson:"product_group_key" example:"5f02aab989bfe6971405ae74"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5f02aab973afa1a45e4562cc"`
	BrandKey              primitive.ObjectID `json:"brand_key" bson:"brand_key" example:"5f02aab975e9f8a68137c7b4"`
	ProductId             string             `json:"product_id" bson:"product_id" example:"et" validate:"required"`
	ShortDescription      string             `json:"short_description" bson:"short_description" example:"irure"`
	LongDescription       string             `json:"long_description" bson:"long_description" example:"consequat"`
	HtmlDescription       string             `json:"html_description" bson:"html_description" example:"commodo"`
	Barcode               string             `json:"barcode" bson:"barcode" example:"amet" validate:"required"`
	BasePrice             float64            `json:"base_price" bson:"base_price" example:"170.79"`
	IsTaxInclusive        bool               `json:"is_tax_inclusive" bson:"is_tax_inclusive" example:"true"`
	DefaultSiteKey        primitive.ObjectID `json:"default_site_key" bson:"default_site_key" example:"5f02aab920a063e46ff9e0f2"`
	DefaultSalesTaxKey    primitive.ObjectID `json:"default_sales_tax_key" bson:"default_sales_tax_key" example:"5f02aab9d4df30c931d81e37"`
	DefaultPurchaseTaxKey primitive.ObjectID `json:"default_purchase_tax_key" bson:"default_purchase_tax_key" example:"5f02aab9935b889de00d7acd"`
	OnHandQty             int64              `json:"on_hand_qty" bson:"on_hand_qty" example:"230"`
	OnOrderQty            int64              `json:"on_order_qty" bson:"on_order_qty" example:"230"`
	OnCommitQty           int64              `json:"on_commit_qty" bson:"on_commit_qty" example:"230"`
	OnAvailableQty        int64              `json:"on_available_qty" bson:"on_available_qty" example:"230"`
	ProductCost           float64            `json:"product_cost" bson:"product_cost" example:"488.95"`
	IsMatrix              bool               `json:"is_matrix" bson:"is_matrix" example:"false"`
	IsParent              bool               `json:"is_parent" bson:"is_parent" example:"false"`
	ProductMatrixKey      primitive.ObjectID `json:"product_matrix_key" bson:"product_matrix_key" example:"5f02aab98e460b4c21fa1b22"`
	VendorKey             primitive.ObjectID `json:"vendor_key" bson:"vendor_key" example:"5f02aab98e460b4c21fa1b22"`
	Height                float64            `json:"height" bson:"height" example:"310.12"`
	Length                float64            `json:"length" bson:"length" example:"310.12"`
	Width                 float64            `json:"width" bson:"width" example:"310.12"`
	Weight                float64            `json:"weight" bson:"weight" example:"310.12"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"false"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"eiusmod"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2016-06-13T05:22:29 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efc1cfa46e9968f9c6d9d7c"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5efc1cfaaec3d46ad7bf90df"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2017-08-14T06:19:22 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efc1cfadbb8358614063344"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5efc1cfa023ed7deb7430b8c"`
	Color                 string             `json:"color" bson:"color" example:"eiusmod"`
	Size                  string             `json:"size" bson:"size" example:"eiusmod"`
	Uom                   string             `json:"uom" bson:"uom" example:"eiusmod"`
	ImageUrl              string             `json:"image_url" bson:"image_url" example:""`
}

type ProductRequest struct {
	ProductGroupKey       primitive.ObjectID `json:"product_group_key" bson:"product_group_key" example:"5f02aab989bfe6971405ae74"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5f02aab973afa1a45e4562cc"`
	BrandKey              primitive.ObjectID `json:"brand_key" bson:"brand_key" example:"5f02aab975e9f8a68137c7b4"`
	ProductId             string             `json:"product_id" bson:"product_id" example:"et" validate:"required"`
	ShortDescription      string             `json:"short_description" bson:"short_description" example:"irure"`
	LongDescription       string             `json:"long_description" bson:"long_description" example:"consequat"`
	HtmlDescription       string             `json:"html_description" bson:"html_description" example:"commodo"`
	Barcode               string             `json:"barcode" bson:"barcode" example:"amet" validate:"required"`
	BasePrice             float64            `json:"base_price" bson:"base_price" example:"170.79"`
	IsTaxInclusive        bool               `json:"is_tax_inclusive" bson:"is_tax_inclusive" example:"true"`
	DefaultSiteKey        primitive.ObjectID `json:"default_site_key" bson:"default_site_key" example:"5f02aab920a063e46ff9e0f2"`
	DefaultSalesTaxKey    primitive.ObjectID `json:"default_sales_tax_key" bson:"default_sales_tax_key" example:"5f02aab9d4df30c931d81e37"`
	DefaultPurchaseTaxKey primitive.ObjectID `json:"default_purchase_tax_key" bson:"default_purchase_tax_key" example:"5f02aab9935b889de00d7acd"`
	OnHandQty             int64              `json:"on_hand_qty" bson:"on_hand_qty" example:"230"`
	OnOrderQty            int64              `json:"on_order_qty" bson:"on_order_qty" example:"230"`
	OnCommitQty           int64              `json:"on_commit_qty" bson:"on_commit_qty" example:"230"`
	OnAvailableQty        int64              `json:"on_available_qty" bson:"on_available_qty" example:"230"`
	ProductCost           float64            `json:"product_cost" bson:"product_cost" example:"488.95"`
	IsMatrix              bool               `json:"is_matrix" bson:"is_matrix" example:"false"`
	IsParent              bool               `json:"is_parent" bson:"is_parent" example:"false"`
	ProductMatrixKey      primitive.ObjectID `json:"product_matrix_key" bson:"product_matrix_key" example:"5f02aab98e460b4c21fa1b22"`
	VendorKey             primitive.ObjectID `json:"vendor_key" bson:"vendor_key" example:"5f02aab98e460b4c21fa1b22"`
	Height                float64            `json:"height" bson:"height" example:"310.12"`
	Length                float64            `json:"length" bson:"length" example:"310.12"`
	Width                 float64            `json:"width" bson:"width" example:"310.12"`
	Weight                float64            `json:"weight" bson:"weight" example:"310.12"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"false"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efc1cfa46e9968f9c6d9d7c"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5efc1cfaaec3d46ad7bf90df"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efc1cfadbb8358614063344"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5efc1cfa023ed7deb7430b8c"`
	Color                 string             `json:"color" bson:"color" example:"eiusmod"`
	Size                  string             `json:"size" bson:"size" example:"eiusmod"`
	Uom                   string             `json:"uom" bson:"uom" example:"eiusmod"`
	ImageUrl              string             `json:"image_url" bson:"image_url" example:""`
}

type ProductResponse struct {
	ID                    primitive.ObjectID        `json:"_id,omitempty" bson:"_id,omitempty" example:"5f02aab989bfe6971405ae74"`
	ProductGroupKey       primitive.ObjectID        `json:"product_group_key" bson:"product_group_key" example:"5f02aab989bfe6971405ae74"`
	TenantKey             primitive.ObjectID        `json:"tenant_key" bson:"tenant_key" example:"5f02aab973afa1a45e4562cc"`
	BrandKey              primitive.ObjectID        `json:"brand_key" bson:"brand_key" example:"5f02aab975e9f8a68137c7b4"`
	ProductId             string                    `json:"product_id" bson:"product_id" example:"et" validate:"required"`
	ShortDescription      string                    `json:"short_description" bson:"short_description" example:"irure"`
	LongDescription       string                    `json:"long_description" bson:"long_description" example:"consequat"`
	HtmlDescription       string                    `json:"html_description" bson:"html_description" example:"commodo"`
	Barcode               string                    `json:"barcode" bson:"barcode" example:"amet" validate:"required"`
	BasePrice             float64                   `json:"base_price" bson:"base_price" example:"170.79"`
	IsTaxInclusive        bool                      `json:"is_tax_inclusive" bson:"is_tax_inclusive" example:"true"`
	DefaultSiteKey        primitive.ObjectID        `json:"default_site_key" bson:"default_site_key" example:"5f02aab920a063e46ff9e0f2"`
	DefaultSalesTaxKey    primitive.ObjectID        `json:"default_sales_tax_key" bson:"default_sales_tax_key" example:"5f02aab9d4df30c931d81e37"`
	DefaultPurchaseTaxKey primitive.ObjectID        `json:"default_purchase_tax_key" bson:"default_purchase_tax_key" example:"5f02aab9935b889de00d7acd"`
	OnHandQty             int64                     `json:"on_hand_qty" bson:"on_hand_qty" example:"230"`
	OnOrderQty            int64                     `json:"on_order_qty" bson:"on_order_qty" example:"230"`
	OnCommitQty           int64                     `json:"on_commit_qty" bson:"on_commit_qty" example:"230"`
	OnAvailableQty        int64                     `json:"on_available_qty" bson:"on_available_qty" example:"230"`
	ReturnQty             int64                     `json:"return_qty" bson:"return_qty" example:"230"`
	CancelQty             int64                     `json:"cancel_qty" bson:"cancel_qty" example:"230"`
	ProductCost           float64                   `json:"product_cost" bson:"product_cost" example:"488.95"`
	IsMatrix              bool                      `json:"is_matrix" bson:"is_matrix" example:"false"`
	IsParent              bool                      `json:"is_parent" bson:"is_parent" example:"false"`
	ProductMatrixKey      primitive.ObjectID        `json:"product_matrix_key" bson:"product_matrix_key" example:"5f02aab98e460b4c21fa1b22"`
	VendorKey             primitive.ObjectID        `json:"vendor_key" bson:"vendor_key" example:"5f02aab98e460b4c21fa1b22"`
	Height                float64                   `json:"height" bson:"height" example:"310.12"`
	Length                float64                   `json:"length" bson:"length" example:"310.12"`
	Width                 float64                   `json:"width" bson:"width" example:"310.12"`
	Weight                float64                   `json:"weight" bson:"weight" example:"310.12"`
	IsActive              bool                      `json:"is_active" bson:"is_active" example:"false"`
	IsDeleted             bool                      `json:"is_deleted" bson:"is_deleted" example:"false"`
	ObjectType            string                    `json:"object_type" bson:"object_type" example:"eiusmod"`
	CreatedDate           time.Time                 `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2016-06-13T05:22:29 -07:00"`
	CreatedBy             primitive.ObjectID        `json:"created_by" bson:"created_by" example:"5efc1cfa46e9968f9c6d9d7c"`
	CreatedApplicationKey primitive.ObjectID        `json:"created_application_key" bson:"created_application_key" example:"5efc1cfaaec3d46ad7bf90df"`
	UpdatedDate           time.Time                 `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2017-08-14T06:19:22 -07:00"`
	UpdatedBy             primitive.ObjectID        `json:"updated_by" bson:"updated_by" example:"5efc1cfadbb8358614063344"`
	UpdatedApplicationKey primitive.ObjectID        `json:"updated_application_key" bson:"updated_application_key" example:"5efc1cfa023ed7deb7430b8c"`
	Color                 string                    `json:"color" bson:"color" example:"eiusmod"`
	Size                  string                    `json:"size" bson:"size" example:"eiusmod"`
	Uom                   string                    `json:"uom" bson:"uom" example:"eiusmod"`
	ImageUrl              string                    `json:"image_url" bson:"image_url" example:""`
	InventoryStatus       []InventoryStatusResponse `json:"inventory_status" bson:"inventory_status" example:"eiusmod"`
}
