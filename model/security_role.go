package model

import (
	"../database"
	"../setting"
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type SecurityRole struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5efc529fbbfa67be2f63b486"`
	SecurityRoleId        string             `json:"security_role_id" bson:"security_role_id" example:"nostrud"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5f02ae4ef996f570ddce7d82"`
	Description           string             `json:"description" bson:"description" example:"eu"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"eiusmod"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2019-05-16T03:20:31 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef30139b1fe3ab144ba7c2f"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef301392fdd17f6073bdd9c"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2019-12-30T09:05:14 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef301394a64c2e6ca57838c"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef3013963eba4f8a7fa139f"`
}

type SecurityRoleRequest struct {
	SecurityRoleId        string             `json:"security_role_id" bson:"security_role_id" example:"nostrud"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5f02ae4ef996f570ddce7d82"`
	Description           string             `json:"description" bson:"description" example:"eu"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"eiusmod"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef30139b1fe3ab144ba7c2f"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef301392fdd17f6073bdd9c"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef301394a64c2e6ca57838c"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef3013963eba4f8a7fa139f"`
	//SecurityDetail        []SecurityDetailRequest `json:"security_details" bson:"security_details"`
}

type SecurityRoleResponse struct {
	ID                    primitive.ObjectID       `json:"_id,omitempty" bson:"_id,omitempty" example:"5efc529fbbfa67be2f63b486"`
	SecurityRoleId        string                   `json:"security_role_id" bson:"security_role_id" example:"nostrud"`
	TenantKey             primitive.ObjectID       `json:"tenant_key" bson:"tenant_key" example:"5f02ae4ef996f570ddce7d82"`
	Description           string                   `json:"description" bson:"description" example:"eu"`
	IsActive              bool                     `json:"is_active" bson:"is_active" example:"true"`
	IsDeleted             bool                     `json:"is_deleted" bson:"is_deleted" example:"false"`
	ObjectType            string                   `json:"object_type" bson:"object_type" example:"eiusmod"`
	CreatedDate           time.Time                `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2019-05-16T03:20:31 -07:00"`
	CreatedBy             primitive.ObjectID       `json:"created_by" bson:"created_by" example:"5ef30139b1fe3ab144ba7c2f"`
	CreatedApplicationKey primitive.ObjectID       `json:"created_application_key" bson:"created_application_key" example:"5ef301392fdd17f6073bdd9c"`
	UpdatedDate           time.Time                `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2019-12-30T09:05:14 -07:00"`
	UpdatedBy             primitive.ObjectID       `json:"updated_by" bson:"updated_by" example:"5ef301394a64c2e6ca57838c"`
	UpdatedApplicationKey primitive.ObjectID       `json:"updated_application_key" bson:"updated_application_key" example:"5ef3013963eba4f8a7fa139f"`
	SecurityDetail        []SecurityDetailResponse `json:"security_details" bson:"security_details"`
}

type SecurityRoleUpdateRequest struct {
	SecurityRoleId        string                        `json:"security_role_id,omitempty" bson:"security_role_id,omitempty" example:"nostrud"`
	TenantKey             primitive.ObjectID            `json:"tenant_key,omitempty" bson:"tenant_key,omitempty" example:"5f02ae4ef996f570ddce7d82"`
	Description           string                        `json:"description,omitempty" bson:"description,omitempty" example:"eu"`
	IsActive              bool                          `json:"is_active,omitempty" bson:"is_active,omitempty" example:"true"`
	ObjectType            string                        `json:"object_type,omitempty" bson:"object_type,omitempty" example:"eiusmod"`
	UpdatedBy             primitive.ObjectID            `json:"updated_by,omitempty" bson:"updated_by,omitempty" example:"5ef301394a64c2e6ca57838c"`
	UpdatedApplicationKey primitive.ObjectID            `json:"updated_application_key,omitempty" bson:"updated_application_key,omitempty" example:"5ef3013963eba4f8a7fa139f"`
	SecurityDetail        []SecurityDetailUpdateRequest `json:"security_details,omitempty" bson:"security_details,omitempty"`
}

func (s SecurityRoleRequest) Validation() (err error) {
	if err = s.CheckSecurityRoleId(); err != nil {
		return errors.New("Check Security Role Id ERROR - " + err.Error())
	}
	return nil
}

func (s SecurityRoleRequest) CheckSecurityRoleId() (err error) {

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{
		"security_role_id": bson.M{"$regex": primitive.Regex{Pattern: "^" + s.SecurityRoleId + "$", Options: "i"}},
	}

	count, err := database.Database.Collection(setting.SecurityRoleTable).CountDocuments(ctx, filter)
	if err != nil {
		return errors.New("Count Document ERROR - " + err.Error())
	}
	if count > 0 {
		return errors.New("Security Role Id is already exists ")
	}
	return nil
}

func (s SecurityRoleUpdateRequest) Validation(key primitive.ObjectID) (a SecurityRoleUpdateRequest, err error) {
	if err = s.CheckSecurityRoleId(key); err != nil {
		return a, errors.New("Check Security Role Id ERROR - " + err.Error())
	}

	for i, d := range s.SecurityDetail {
		s.SecurityDetail[i], err = d.Validation()
		if err != nil {
			return a, errors.New("Validate Security Update Detail ERROR - " + err.Error())
		}
	}

	return s, nil
}

func (s SecurityRoleUpdateRequest) CheckSecurityRoleId(key primitive.ObjectID) (err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{
		"_id":              bson.M{"$ne": bsonx.ObjectID(key)},
		"security_role_id": bson.M{"$regex": primitive.Regex{Pattern: "^" + s.SecurityRoleId + "$", Options: "i"}},
	}

	count, err := database.Database.Collection(setting.SecurityRoleTable).CountDocuments(ctx, filter)
	if err != nil {
		return errors.New("Count Document ERROR - " + err.Error())
	}
	if count > 0 {
		return errors.New("Security Role Id is already exists ")
	}
	return nil
}
