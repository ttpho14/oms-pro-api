package model

import (
	"../model/enum"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

// ReturnDetail struct
type ReturnDetail struct {
	ID                    primitive.ObjectID      `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef2c053f7a34f7669bb8246"`
	DocKey                primitive.ObjectID      `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246"`
	DocNum                string                  `json:"doc_num" bson:"doc_num" example:"ABC_RC01"`
	DocLineNum            string                  `json:"doc_line_num" bson:"doc_line_num" example:"ABC_RC01"`
	DocLineStatus         enum.ReturnDetailStatus `json:"doc_line_status" bson:"doc_line_status" example:"Picking, Packing, Transferring, Transit 1, Transit 2"`
	ProductKey            primitive.ObjectID      `json:"product_key" bson:"product_key" example:"5ef2c053f7a34f7669bb8246"`
	ProductCode           string                  `json:"product_code" bson:"product_code" example:"0"`
	ProductSize           string                  `json:"product_size" bson:"product_size" example:"XL" validate:"required"`
	ProductColor          string                  `json:"product_color" bson:"product_color" example:"red" validate:"required"`
	ProductUom            string                  `json:"product_uom" bson:"product_uom" example:"eiusmod"`
	Description           string                  `json:"description" bson:"description" example:"Description"`
	BaseObjectType        string                  `json:"base_object_type" bson:"base_object_type" example:"type"`
	BaseDocKey            primitive.ObjectID      `json:"base_doc_key" bson:"base_doc_key" example:"5ef2c053f7a34f7669bb8246"`
	BaseDocLineKey        primitive.ObjectID      `json:"base_doc_line_key" bson:"base_doc_line_key" example:"5ef2c053f7a34f7669bb8246"`
	BaseDocNum            string                  `json:"base_doc_num" bson:"base_doc_num" example:"type"`
	BaseDocLineNum        string                  `json:"base_doc_line_num" bson:"base_doc_line_num" example:"type"`
	Quantity              float64                 `json:"quantity" bson:"quantity" example:"0"`
	OrderQuantity         float64                 `json:"order_quantity" bson:"order_quantity" example:"0"`
	Price                 float64                 `json:"price" bson:"price" example:"0"`
	FreightAmount         float64                 `json:"freight_amount" bson:"freight_amount" example:"0" validate:"required"`
	LineDiscountAmount    float64                 `json:"line_discount_amount" bson:"line_discount_amount" example:"0"`
	LineDiscountPercent   float64                 `json:"line_discount_percent" bson:"line_discount_percent" example:"0"`
	HeaderDiscountAmount  float64                 `json:"header_discount_amount" bson:"header_discount_amount" example:"0"`
	PriceAfterDiscount    float64                 `json:"price_after_discount" bson:"price_after_discount" example:"0"`
	TaxCodeKey            primitive.ObjectID      `json:"tax_code_key" bson:"tax_code_key" example:"5ef2c053f7a34f7669bb8246"`
	TaxRate               float64                 `json:"tax_rate" bson:"tax_rate" example:"0"`
	PriceAfterTax         float64                 `json:"price_after_tax" bson:"price_after_tax" example:"0"`
	ReturnSiteKey         primitive.ObjectID      `json:"origin_site_key" bson:"origin_site_key" example:"5ef2c053f7a34f7669bb8246"`
	Remark                string                  `json:"remark" bson:"remark" example:"Remark"`
	AddValue              string                  `json:"add_value" bson:"add_value" example:"type"`
	ShippingAddressKey    primitive.ObjectID      `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053f7a34f7669bb8246"`
	ShippingAddress       string                  `json:"shipping_address" bson:"shipping_address" example:"908 Juliana Place, Chautauqua, Montana, 1055"`
	BillingAddressKey     primitive.ObjectID      `json:"bill_address_key" bson:"bill_address_key" example:"5ef2c053f7a34f7669bb8246"`
	BillingAddress        string                  `json:"bill_address" bson:"bill_address" example:"908 Juliana Place, Chautauqua, Montana, 1055"`
	ImageUrl              string                  `json:"image_url" bson:"image_url" example:""`
	ObjectType            string                  `json:"object_type" bson:"object_type" example:"type"`
	CreatedDate           time.Time               `json:"created_date" bson:"created_date" example:"2016-07-19T04:53:39 -07:00"`
	CreatedBy             primitive.ObjectID      `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID      `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedDate           time.Time               `json:"updated_date" bson:"updated_date" example:"2016-07-19T04:53:39 -07:00"`
	UpdatedBy             primitive.ObjectID      `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID      `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
}

// ReturnDetailRequest struct
type ReturnDetailRequest struct {
	//DocLineStatus         string             `json:"doc_line_status" bson:"doc_line_status" example:"Picking, Packing, Transferring, Transit 1, Transit 2"`
	//ProductKey            primitive.ObjectID `json:"product_key" bson:"product_key" example:"5ef2c053f7a34f7669bb8246"`
	//ProductCode           string             `json:"product_code" bson:"product_code" example:"0"`
	//ProductSize           string             `json:"product_size" bson:"product_size" example:"XL" validate:"required"`
	//ProductColor          string             `json:"product_color" bson:"product_color" example:"red" validate:"required"`
	//Description           string             `json:"description" bson:"description" example:"Description"`
	//BaseObjectType        string             `json:"base_object_type" bson:"base_object_type" example:"type"`
	//BaseDocKey            primitive.ObjectID `json:"base_doc_key" bson:"base_doc_key" example:"5ef2c053f7a34f7669bb8246"`
	//BaseDocLineKey        primitive.ObjectID `json:"base_doc_line_key" bson:"base_doc_line_key" example:"5ef2c053f7a34f7669bb8246"`
	//BaseDocNum            string             `json:"base_doc_num" bson:"base_doc_num" example:"type"`
	BaseDocLineNum string  `json:"base_doc_line_num" bson:"base_doc_line_num" example:"type"`
	Quantity       float64 `json:"quantity" bson:"quantity" example:"0"`
	//OrderQuantity         float64            `json:"order_quantity" bson:"order_quantity" example:"0"`
	//Price                 float64            `json:"price" bson:"price" example:"0"`
	//FreightAmount         float64            `json:"freight_amount" bson:"freight_amount" example:"0" validate:"required"`
	//LineDiscountAmount    float64            `json:"line_discount_amount" bson:"line_discount_amount" example:"0"`
	//LineDiscountPercent   float64            `json:"line_discount_percent" bson:"line_discount_percent" example:"0"`
	//HeaderDiscountAmount  float64            `json:"header_discount_amount" bson:"header_discount_amount" example:"0"`
	//PriceAfterDiscount    float64            `json:"price_after_discount" bson:"price_after_discount" example:"0"`
	//TaxCodeKey            primitive.ObjectID `json:"tax_code_key" bson:"tax_code_key" example:"5ef2c053f7a34f7669bb8246"`
	//TaxRate               float64            `json:"tax_rate" bson:"tax_rate" example:"0"`
	//PriceAfterTax         float64            `json:"price_after_rate" bson:"price_after_rate" example:"0"`
	//ReturnSiteKey         primitive.ObjectID `json:"origin_site_key" bson:"origin_site_key" example:"5ef2c053f7a34f7669bb8246"`
	//Remark                string             `json:"remark" bson:"remark" example:"Remark"`
	//AddValue              string             `json:"add_value" bson:"add_value" example:"type"`
	//ShippingAddressKey    primitive.ObjectID `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053f7a34f7669bb8246"`
	//ShippingAddress       string             `json:"shipping_address" bson:"shipping_address" example:"908 Juliana Place, Chautauqua, Montana, 1055"`
	//BillingAddressKey     primitive.ObjectID `json:"bill_address_key" bson:"bill_address_key" example:"5ef2c053f7a34f7669bb8246"`
	//BillingAddress        string             `json:"bill_address" bson:"bill_address" example:"908 Juliana Place, Chautauqua, Montana, 1055"`
	//ObjectType            string             `json:"object_type" bson:"object_type" example:"type"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
}

type ReturnDetailResponse struct {
	ID                    primitive.ObjectID      `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef2c053f7a34f7669bb8246"`
	DocKey                primitive.ObjectID      `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246"`
	DocNum                string                  `json:"doc_num" bson:"doc_num" example:"ABC_RC01"`
	DocLineNum            string                  `json:"doc_line_num" bson:"doc_line_num" example:"ABC_RC01"`
	DocLineStatus         enum.ReturnDetailStatus `json:"doc_line_status" bson:"doc_line_status" example:"Picking, Packing, Transferring, Transit 1, Transit 2"`
	ProductKey            primitive.ObjectID      `json:"product_key" bson:"product_key" example:"5ef2c053f7a34f7669bb8246"`
	ProductCode           string                  `json:"product_code" bson:"product_code" example:"0"`
	ProductSize           string                  `json:"product_size" bson:"product_size" example:"XL" validate:"required"`
	ProductColor          string                  `json:"product_color" bson:"product_color" example:"red" validate:"required"`
	ProductUom            string                  `json:"product_uom" bson:"product_uom" example:"eiusmod"`
	Description           string                  `json:"description" bson:"description" example:"Description"`
	BaseObjectType        string                  `json:"base_object_type" bson:"base_object_type" example:"type"`
	BaseDocKey            primitive.ObjectID      `json:"base_doc_key" bson:"base_doc_key" example:"5ef2c053f7a34f7669bb8246"`
	BaseDocLineKey        primitive.ObjectID      `json:"base_doc_line_key" bson:"base_doc_line_key" example:"5ef2c053f7a34f7669bb8246"`
	BaseDocNum            string                  `json:"base_doc_num" bson:"base_doc_num" example:"type"`
	BaseDocLineNum        string                  `json:"base_doc_line_num" bson:"base_doc_line_num" example:"type"`
	Quantity              float64                 `json:"quantity" bson:"quantity" example:"0"`
	OrderQuantity         float64                 `json:"order_quantity" bson:"order_quantity" example:"0"`
	Price                 float64                 `json:"price" bson:"price" example:"0"`
	FreightAmount         float64                 `json:"freight_amount" bson:"freight_amount" example:"0" validate:"required"`
	LineDiscountAmount    float64                 `json:"line_discount_amount" bson:"line_discount_amount" example:"0"`
	LineDiscountPercent   float64                 `json:"line_discount_percent" bson:"line_discount_percent" example:"0"`
	HeaderDiscountAmount  float64                 `json:"header_discount_amount" bson:"header_discount_amount" example:"0"`
	PriceAfterDiscount    float64                 `json:"price_after_discount" bson:"price_after_discount" example:"0"`
	TaxCodeKey            primitive.ObjectID      `json:"tax_code_key" bson:"tax_code_key" example:"5ef2c053f7a34f7669bb8246"`
	TaxRate               float64                 `json:"tax_rate" bson:"tax_rate" example:"0"`
	PriceAfterTax         float64                 `json:"price_after_tax" bson:"price_after_tax" example:"0"`
	ReturnSiteKey         primitive.ObjectID      `json:"origin_site_key" bson:"origin_site_key" example:"5ef2c053f7a34f7669bb8246"`
	Remark                string                  `json:"remark" bson:"remark" example:"Remark"`
	AddValue              string                  `json:"add_value" bson:"add_value" example:"type"`
	ShippingAddressKey    primitive.ObjectID      `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053f7a34f7669bb8246"`
	ShippingAddress       string                  `json:"shipping_address" bson:"shipping_address" example:"908 Juliana Place, Chautauqua, Montana, 1055"`
	BillingAddressKey     primitive.ObjectID      `json:"bill_address_key" bson:"bill_address_key" example:"5ef2c053f7a34f7669bb8246"`
	BillingAddress        string                  `json:"bill_address" bson:"bill_address" example:"908 Juliana Place, Chautauqua, Montana, 1055"`
	ImageUrl              string                  `json:"image_url" bson:"image_url" example:""`
	ObjectType            string                  `json:"object_type" bson:"object_type" example:"type"`
	CreatedDate           time.Time               `json:"created_date" bson:"created_date" example:"2016-07-19T04:53:39 -07:00"`
	CreatedBy             primitive.ObjectID      `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID      `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedDate           time.Time               `json:"updated_date" bson:"updated_date" example:"2016-07-19T04:53:39 -07:00"`
	UpdatedBy             primitive.ObjectID      `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID      `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
}

type ReturnDetailUpdateRequest struct {
	BaseDocLineNum string                  `json:"base_doc_line_num,omitempty" bson:"base_doc_line_num,omitempty" example:"type"`
	DocLineStatus  enum.ReturnDetailStatus `json:"doc_line_status,omitempty" bson:"doc_line_status,omitempty" example:"Picking, Packing, Transferring, Transit 1, Transit 2"`
}
