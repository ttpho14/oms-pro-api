package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type UserDefineField struct {
	ID                  primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5efc0ed38d1020f1211f75f6"`
	SourceObjectType    string             `json:"source_object_type" bson:"source_object_type" example:"et"`
	UserDefineFieldName string             `json:"user_define_field_name" bson:"user_define_field_name" example:"5efc0ed3290f0e31b7a484a8"`
	Description         string             `json:"description" bson:"description" example:"aliqua"`
	DataType            string             `json:"data_type" bson:"data_type" example:"ipsum"`

	ObjectType            string             `json:"object_type" bson:"object_type" example:"nisi"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"false"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	CreatedDate           time.Time          `json:"created_date" bson:"created_date" example:"2016-07-19T04:53:39 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef96db2dc3410a231844080"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef96db2dc3410a231844080"`
	UpdatedDate           time.Time          `json:"updated_date" bson:"updated_date" example:"2016-07-19T04:53:39 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef96db23c574ffde7644a58"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef96db23c574ffde7644a58"`
}

type AddUserDefineField struct {
	SourceObjectType    string `json:"source_object_type" bson:"source_object_type" example:"et"`
	UserDefineFieldName string `json:"user_define_field_name" bson:"user_define_field_name" example:"5efc0ed3290f0e31b7a484a8"`
	Description         string `json:"description" bson:"description" example:"aliqua"`
	DataType            string `json:"data_type" bson:"data_type" example:"ipsum"`

	ObjectType            string             `json:"object_type" bson:"object_type" example:"nisi"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"false"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef96db2dc3410a231844080"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef96db2dc3410a231844080"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef96db23c574ffde7644a58"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef96db23c574ffde7644a58"`
}
