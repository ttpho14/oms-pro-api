package model

import (
	"../model/enum"
	"errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type SecurityDetail struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5efc529fbbfa67be2f63b486"`
	SecurityKey           primitive.ObjectID `json:"security_key" bson:"security_key" example:"5f02b5ffaa01ccf9d485ae18"`
	SourceObjectType      string             `json:"source_object_type" bson:"source_object_type" example:"veniam"`
	AccessRight           enum.AccessRight   `json:"access_right" bson:"access_right" example:"sunt"`
	UserKey               primitive.ObjectID `json:"user_key" bson:"user_key" example:"5f02b5ff0aae4d9508820483"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"eiusmod"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2016-06-13T05:22:29 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efc1cfa46e9968f9c6d9d7c"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5efc1cfaaec3d46ad7bf90df"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2017-08-14T06:19:22 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efc1cfadbb8358614063344"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5efc1cfa023ed7deb7430b8c"`
}

type SecurityDetailRequest struct {
	SecurityKey           primitive.ObjectID `json:"security_key" bson:"security_key" example:"5f02b5ffaa01ccf9d485ae18"`
	SourceObjectType      string             `json:"source_object_type" bson:"source_object_type" example:"veniam"`
	AccessRight           enum.AccessRight   `json:"access_right" bson:"access_right" example:"sunt"`
	UserKey               primitive.ObjectID `json:"user_key" bson:"user_key" example:"5f02b5ff0aae4d9508820483"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"eiusmod"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efc1cfa46e9968f9c6d9d7c"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5efc1cfaaec3d46ad7bf90df"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efc1cfadbb8358614063344"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5efc1cfa023ed7deb7430b8c"`
}

type SecurityDetailResponse struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5efc529fbbfa67be2f63b486"`
	SecurityKey           primitive.ObjectID `json:"security_key" bson:"security_key" example:"5f02b5ffaa01ccf9d485ae18"`
	SourceObjectType      string             `json:"source_object_type" bson:"source_object_type" example:"veniam"`
	AccessRight           enum.AccessRight   `json:"access_right" bson:"access_right" example:"sunt"`
	UserKey               primitive.ObjectID `json:"user_key" bson:"user_key" example:"5f02b5ff0aae4d9508820483"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"eiusmod"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2016-06-13T05:22:29 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efc1cfa46e9968f9c6d9d7c"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5efc1cfaaec3d46ad7bf90df"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2017-08-14T06:19:22 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efc1cfadbb8358614063344"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5efc1cfa023ed7deb7430b8c"`
}

type SecurityDetailUpdateRequest struct {
	ID          primitive.ObjectID `json:"_id" bson:"_id"`
	AccessRight enum.AccessRight   `json:"access_right" bson:"access_right" example:"sunt"`
}

func (sD SecurityDetailUpdateRequest) Validation() (d SecurityDetailUpdateRequest, err error) {
	d, err = sD.CheckAccessRight()
	if err != nil {
		return d, errors.New("Validate Access Right ERROR - " + err.Error())
	}

	return d, nil
}

func (sD SecurityDetailUpdateRequest) CheckAccessRight() (d SecurityDetailUpdateRequest, err error) {
	sD.AccessRight, err = sD.AccessRight.IsValid()
	if err != nil {
		return d, err
	}

	return sD, nil
}
