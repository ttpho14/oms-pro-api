package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Currency struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef96d9c5954cbf2ea3cf7af"`
	CurrencyId            string             `json:"currency_id" bson:"currency_id" example:"5efc4256bf50a7f7fae2468e"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5efc42563d7352cbfb07572c"`
	Description           string             `json:"description" bson:"description" example:"qui@opticon.com"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"nisi"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"false"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	CreatedDate           time.Time          `json:"created_date" bson:"created_date" example:"2016-07-19T04:53:39 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef96db2dc3410a231844080"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef96db2dc3410a231844080"`
	UpdatedDate           time.Time          `json:"updated_date" bson:"updated_date" example:"2016-07-19T04:53:39 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef96db23c574ffde7644a58"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef96db23c574ffde7644a58"`
}

type CurrencyRequest struct {
	CurrencyId            string             `json:"currency_id" bson:"currency_id" example:"5efc4256bf50a7f7fae2468e"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5efc42563d7352cbfb07572c"`
	Description           string             `json:"description" bson:"description" example:"qui@opticon.com"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"true"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"2019-08-14T12:02:56 -07:00"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef96db2dc3410a231844080"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"2020-05-12T05:33:55 -07:00"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef96db23c574ffde7644a58"`
}
