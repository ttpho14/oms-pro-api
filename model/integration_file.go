package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type IntegrationFile struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5f02aab989bfe6971405ae74"`
	FileName              string             `json:"file_name" bson:"file_name" example:"SOMSDA12319ASDK"`
	Content               string             `json:"content" bson:"content" example:"binary file"`
	InvalidProducts       []interface{}      `json:"invalid_products" bson:"invalid_products"`
	Status                string             `json:"status" bson:"status" example:"In Process"`
	IntegrationType       string             `json:"integration_type" bson:"integration_type" example:"NovoMind"`
	CreatedDate           time.Time          `json:"created_date" bson:"created_date" example:"121321532020"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efc1cfa46e9968f9c6d9d7c"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5efc1cfaaec3d46ad7bf90df"`
	UpdatedDate           time.Time          `json:"updated_date" bson:"updated_date" example:"2020-07-31T09:59:55.347Z"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efc1cfadbb8358614063344"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5efc1cfa023ed7deb7430b8c"`
}

type IntegrationFileRequest struct {
	FileName              string             `json:"file_name" bson:"file_name" example:"SOMSDA12319ASDK"`
	Content               string             `json:"content" bson:"content" example:"binary file"`
	IntegrationType       string             `json:"integration_type" bson:"integration_type" example:"NovoMind"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efc1cfa46e9968f9c6d9d7c"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5efc1cfaaec3d46ad7bf90df"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efc1cfadbb8358614063344"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5efc1cfa023ed7deb7430b8c"`
}

type IntegrationFileResponse struct {
	FileName        string        `json:"file_name" bson:"file_name" example:"SOMSDA12319ASDK"`
	InvalidProducts []interface{} `json:"invalid_products" bson:"invalid_products"`
	Status          string        `json:"status" bson:"status" example:"Success"`
	IntegrationType string        `json:"integration_type" bson:"integration_type" example:"NovoMind"`
	CreatedDate     int64         `json:"created_date" bson:"created_date" example:"1596252644"`
}

type IntegrationFileUpdateInvalidProduct struct {
	FileName       string      `json:"file_name" bson:"file_name"`
	InvalidProducts interface{} `json:"invalid_products" bson:"invalid_products"`
}