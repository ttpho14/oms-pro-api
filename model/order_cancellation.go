package model

import (
	db "../database"
	"../model/enum"
	"../setting"
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type OrderCancellation struct {
	ID                    primitive.ObjectID     `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef96d9c5954cbf2ea3cf7af"`
	DocNum                string                 `json:"doc_num" bson:"doc_num" example:"Doc001"`
	SiteId                string                 `json:"site_id" bson:"site_id" example:"SGWHS" validate:"required"`
	AccountCode           string                 `json:"account_code" bson:"account_code" example:"Account code" validate:"required"`
	SalesChannelKey       primitive.ObjectID     `json:"sales_channel_key" bson:"sales_channel_key" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	SalesChannelId        string                 `json:"sales_channel_id" bson:"sales_channel_id" example:"SALESCHANNEL001" validate:"required"`
	SalesChannelName      string                 `json:"sales_channel_name" bson:"sales_channel_name" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	CountryKey            primitive.ObjectID     `json:"country_key" bson:"country_key" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	CountryId             string                 `json:"country_id" bson:"country_id" example:"COUNTRY0001" validate:"required"`
	RefNo                 string                 `json:"ref_no" bson:"ref_no" example:"123456"`
	RefNo1                string                 `json:"ref_no_1" bson:"ref_no_1" example:"123456"`
	Carrier               string                 `json:"carrier" bson:"carrier" example:"ABC1231"`
	Method                string                 `json:"method" bson:"method" example:"PADVX"`
	ReasonKey             primitive.ObjectID     `json:"reason_key" bson:"reason_key" example:"5ef96d9c5954cbf2ea3cf7af"`
	ReasonId              string                 `json:"reason_id" bson:"reason_id" example:"Reason_ID"`
	ReasonDescription     string                 `json:"reason_description" bson:"reason_description" example:"Reason description"`
	BaseDocNum            string                 `json:"base_doc_num" bson:"base_doc_num" example:"123456"`
	BaseDocKey            primitive.ObjectID     `json:"base_doc_key" bson:"base_doc_key" example:"5efc6f5fa74d733d55d7a390"`
	CustomerKey           primitive.ObjectID     `json:"customer_key" bson:"customer_key" example:"5efc6f5fa74d733d55d7a390"`
	CustomerName          string                 `json:"customer_name" bson:"customer_name" example:"James"`
	OrderDate             time.Time              `json:"order_date" bson:"order_date" example:"2016-07-19T04:53:39 -07:00"`
	DocDate               time.Time              `json:"doc_date" bson:"doc_date" example:"2016-07-19T04:53:39 -07:00"`
	DocDueDate            time.Time              `json:"doc_due_date" bson:"doc_due_date"  example:"2016-07-19T04:53:39 -07:00"`
	TenantKey             primitive.ObjectID     `json:"tenant_key" bson:"tenant_key" example:"5ef2c053d86e577333da9b04"`
	SalesType             string                 `json:"source_type" bson:"source_type" example:"Normal, Staff, Test, Exchange etc."`
	CurrencyKey           primitive.ObjectID     `json:"currency_key" bson:"currency_key" example:"5ef2c053d86e577333da9b04"`
	CurrencyId            string                 `json:"currency_id" bson:"currency_id" example:"CURRENCY0001" validate:"required"`
	CurrencyRate          float64                `json:"currency_rate" bson:"currency_rate" example:"1"`
	TotalBeforeDiscount   float64                `json:"total_before_discount" bson:"total_before_discount" example:"0"`
	DiscountAmount        float64                `json:"discount_amount" bson:"discount_amount" example:"0"`
	DiscountPercent       float64                `json:"discount_percent" bson:"discount_percent" example:"0"`
	TotalAfterDiscount    float64                `json:"total_after_discount" bson:"total_after_discount" example:"0"`
	FreightAmount         float64                `json:"freight_amount" bson:"freight_amount" example:"0"`
	TaxAmount             float64                `json:"tax_amount" bson:"tax_amount" example:"0"`
	TaxRate               float64                `json:"tax_rate" bson:"tax_rate" example:"0"`
	TotalAfterTax         float64                `json:"total_after_tax" bson:"total_after_tax" example:"0"`
	TotalOrderAmount      float64                `json:"total_order_amount" bson:"total_order_amount" example:"0"`
	PayTaxAmount          float64                `json:"pay_tax_amount" bson:"pay_tax_amount" example:"0"`
	PayTaxRate            float64                `json:"pay_tax_rate" bson:"pay_tax_rate" example:"0"`
	PaidAmount            float64                `json:"paid_amount" bson:"paid_amount" example:"0"`
	BalanceAmount         float64                `json:"balance_amount" bson:"balance_amount" example:"0"`
	DocStatus             enum.CancelOrderStatus `json:"doc_status" bson:"doc_status" example:"Normal, Staff, Test, Exchange etc."`
	PaymentStatus         string                 `json:"payment_status" bson:"payment_status" example:"Pending, Partial, Complete,.."`
	Remark                string                 `json:"remark" bson:"remark" example:"Remark"`
	AddValue              string                 `json:"add_value" bson:"add_value" example:"Gift Notes, instruction"`
	BillingAddressKey     primitive.ObjectID     `json:"billing_address_key" bson:"billing_address_key" example:"5ef2c053d86e577333da9b04"`
	BillingAddress        BillingAddress         `json:"billing_address" bson:"billing_address"`
	ShippingAddressKey    primitive.ObjectID     `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053d86e577333da9b04"`
	ShippingAddress       ShippingAddress        `json:"shipping_address" bson:"shipping_address"`
	Email                 string                 `json:"email" bson:"email" example:"abc@pamcs.com" validate:"required"`
	TrackingNo            string                 `json:"tracking_no" bson:"tracking_no" example:"apbca"`
	ObjectType            string                 `json:"object_type" bson:"object_type" example:"type"`
	CreatedDate           time.Time              `json:"created_date" bson:"created_date" example:"2016-07-19T04:53:39 -07:00"`
	CreatedBy             primitive.ObjectID     `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID     `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedDate           time.Time              `json:"updated_date" bson:"updated_date" example:"2016-07-19T04:53:39 -07:00"`
	UpdatedBy             primitive.ObjectID     `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID     `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
}

type OrderCancellationRequest struct {
	BaseDocNum                             string                                   `json:"base_doc_num" bson:"base_doc_num" example:"123456"`
	ReasonKey                              primitive.ObjectID                       `json:"reason_key" bson:"reason_key" example:"5ef96d9c5954cbf2ea3cf7af"`
	ReasonId                               string                                   `json:"reason_id" bson:"reason_id" example:"Reason_ID"`
	ReasonDescription                      string                                   `json:"reason_description" bson:"reason_description" example:"Reason description"`
	FreightAmount                          float64                                  `json:"freight_amount" bson:"freight_amount" example:"0"`
	CancelRequest                          bool                                     `json:"cancel_request" bson:"cancel_request" example:"false"`
	DocStatus                              enum.CancelOrderStatus                   `json:"doc_status" bson:"doc_status" example:"Normal, Staff, Test, Exchange etc."`
	CreatedBy                              primitive.ObjectID                       `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey                  primitive.ObjectID                       `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedBy                              primitive.ObjectID                       `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey                  primitive.ObjectID                       `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
	OrderCancellationDetailRequest         []OrderCancellationDetailRequest         `json:"order_cancellation_details" bson:"order_cancellation_details"`
	OrderCancellationPaymentRequest        []OrderCancellationPaymentRequest        `json:"order_cancellation_payments" bson:"order_cancellation_payments"`
	OrderCancellationCommentRequest        []OrderCancellationCommentRequest        `json:"order_cancellation_comments" bson:"order_cancellation_comments"`
	OrderCancellationShippingDetailRequest []OrderCancellationShippingDetailRequest `json:"order_cancellation_shipping_details" bson:"order_cancellation_shipping_details"`
	//DocDate                         int64                             `json:"doc_date" bson:"doc_date" example:"1597158731"`
	//DocDueDate                      int64                             `json:"doc_due_date" bson:"doc_due_date"  example:"1597158731"`
	//SalesChannelKey                 primitive.ObjectID                `json:"sales_channel_key" bson:"sales_channel_key" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	//SalesChannelId                  string                            `json:"sales_channel_id" bson:"sales_channel_id" example:"SALESCHANNEL001" validate:"required"`
	//SalesChannelName                string                            `json:"sales_channel_name" bson:"sales_channel_name" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	//CountryKey                      primitive.ObjectID                `json:"country_key" bson:"country_key" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	//CountryId                       string                            `json:"country_id" bson:"country_id" example:"COUNTRY0001" validate:"required"`
	//Carrier                         string                            `json:"carrier" bson:"carrier" example:"ABC1231"`
	//Method                          string                            `json:"method" bson:"method" example:"PADVX"`
	//CustomerKey                     primitive.ObjectID                `json:"customer_key" bson:"customer_key"  example:"5efc6f5fa74d733d55d7a390"`
	//CustomerName                    string                            `json:"customer_name" bson:"customer_name" example:"James"`
	//TenantKey                       primitive.ObjectID                `json:"tenant_key" bson:"tenant_key" example:"5ef2c053d86e577333da9b04"`
	//SalesType                       string                            `json:"source_type" bson:"source_type" example:"Normal, Staff, Test, Exchange etc."`
	//CurrencyKey                     primitive.ObjectID                `json:"currency_key" bson:"currency_key" example:"5ef2c053d86e577333da9b04"`
	//CurrencyId                      string                            `json:"currency_id" bson:"currency_id" example:"CURRENCY0001" validate:"required"`
	//CurrencyRate                    float64                           `json:"currency_rate" bson:"currency_rate" example:"1"`
	//TotalBeforeDiscount             float64                           `json:"total_before_discount" bson:"total_before_discount" example:"0"`
	//DiscountAmount                  float64                           `json:"discount_amount" bson:"discount_amount" example:"0"`
	//DiscountPercent                 float64                           `json:"discount_percent" bson:"discount_percent" example:"0"`
	//TotalAfterDiscount              float64                           `json:"total_after_discount" bson:"total_after_discount" example:"0"`
	//FreightAmount                   float64                           `json:"freight_amount" bson:"freight_amount" example:"0"`
	//TaxAmount                       float64                           `json:"tax_amount" bson:"tax_amount" example:"0"`
	//TaxRate                         float64                           `json:"tax_rate" bson:"tax_rate" example:"0"`
	//TotalAfterTax                   float64                           `json:"total_after_tax" bson:"total_after_tax" example:"0"`
	//PayTaxAmount                    float64                           `json:"pay_tax_amount" bson:"pay_tax_amount" example:"0"`
	//PayTaxRate                      float64                           `json:"pay_tax_rate" bson:"pay_tax_rate" example:"0"`
	//PaidAmount                      float64                           `json:"paid_amount" bson:"paid_amount" example:"0"`
	//BalanceAmount                   float64                           `json:"balance_amount" bson:"balance_amount" example:"0"`
	//PaymentStatus                   string                            `json:"payment_status" bson:"payment_status" example:"Pending, Partial, Complete,.."`
	//Remark                          string                            `json:"remark" bson:"remark" example:"Remark"`
	//AddValue                        string                            `json:"add_value" bson:"add_value" example:"Gift Notes, instruction"`
	//BillingAddressKey               primitive.ObjectID                `json:"billing_address_key" bson:"billing_address_key" example:"5ef2c053d86e577333da9b04"`
	//BillingAddress                  BillingAddress                    `json:"billing_address" bson:"billing_address"`
	//ShippingAddressKey              primitive.ObjectID                `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053d86e577333da9b04"`
	//ShippingAddress                 ShippingAddress                   `json:"shipping_address" bson:"shipping_address"`
	//Email                           string                            `json:"email" bson:"email" example:"abc@pamcs.com" validate:"required"`
	//ObjectType                      string                            `json:"object_type" bson:"object_type" example:"type"`
}

type OrderCancellationResponse struct {
	ID                                      primitive.ObjectID                        `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef96d9c5954cbf2ea3cf7af"`
	DocNum                                  string                                    `json:"doc_num" bson:"doc_num" example:"Doc001"`
	SiteId                                  string                                    `json:"site_id" bson:"site_id" example:"SGWHS" validate:"required"`
	AccountCode                             string                                    `json:"account_code" bson:"account_code" example:"Account code" validate:"required"`
	ReasonKey                               primitive.ObjectID                        `json:"reason_key" bson:"reason_key" example:"5ef96d9c5954cbf2ea3cf7af"`
	ReasonId                                string                                    `json:"reason_id" bson:"reason_id" example:"Reason_ID"`
	ReasonDescription                       string                                    `json:"reason_description" bson:"reason_description" example:"Reason description"`
	SalesChannelKey                         primitive.ObjectID                        `json:"sales_channel_key" bson:"sales_channel_key" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	SalesChannelId                          string                                    `json:"sales_channel_id" bson:"sales_channel_id" example:"SALESCHANNEL001" validate:"required"`
	SalesChannelName                        string                                    `json:"sales_channel_name" bson:"sales_channel_name" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	RefNo                                   string                                    `json:"ref_no" bson:"ref_no" example:"123456"`
	RefNo1                                  string                                    `json:"ref_no_1" bson:"ref_no_1" example:"123456"`
	Country                                 Country                                   `json:"country" bson:"country"`
	CountryId                               string                                    `json:"country_id" bson:"country_id" example:"COUNTRY0001" validate:"required"`
	Carrier                                 string                                    `json:"carrier" bson:"carrier" example:"ABC1231"`
	Method                                  string                                    `json:"method" bson:"method" example:"PADVX"`
	BaseDocNum                              string                                    `json:"base_doc_num" bson:"base_doc_num" example:"123456"`
	BaseDocKey                              primitive.ObjectID                        `json:"base_doc_key" bson:"base_doc_key" example:"5efc6f5fa74d733d55d7a390"`
	Customer                                Customer                                  `json:"customer" bson:"customer"`
	CustomerName                            string                                    `json:"customer_name" bson:"customer_name" example:"James"`
	OrderDate                               time.Time                                 `json:"order_date" bson:"order_date" example:"2016-07-19T04:53:39 -07:00"`
	DocDate                                 time.Time                                 `json:"doc_date" bson:"doc_date" example:"1597158731"`
	DocDueDate                              time.Time                                 `json:"doc_due_date" bson:"doc_due_date"  example:"1597158731"`
	Tenant                                  Tenant                                    `json:"tenant" bson:"tenant"`
	SalesType                               string                                    `json:"source_type" bson:"source_type" example:"Normal, Staff, Test, Exchange etc."`
	CurrencyKey                             Currency                                  `json:"currency" bson:"currency"`
	CurrencyId                              string                                    `json:"currency_id" bson:"currency_id" example:"CURRENCY0001" validate:"required"`
	CurrencyRate                            float64                                   `json:"currency_rate" bson:"currency_rate" example:"1"`
	TotalBeforeDiscount                     float64                                   `json:"total_before_discount" bson:"total_before_discount" example:"0"`
	DiscountAmount                          float64                                   `json:"discount_amount" bson:"discount_amount" example:"0"`
	DiscountPercent                         float64                                   `json:"discount_percent" bson:"discount_percent" example:"0"`
	TotalAfterDiscount                      float64                                   `json:"total_after_discount" bson:"total_after_discount" example:"0"`
	FreightAmount                           float64                                   `json:"freight_amount" bson:"freight_amount" example:"0"`
	TaxAmount                               float64                                   `json:"tax_amount" bson:"tax_amount" example:"0"`
	TaxRate                                 float64                                   `json:"tax_rate" bson:"tax_rate" example:"0"`
	TotalAfterTax                           float64                                   `json:"total_after_tax" bson:"total_after_tax" example:"0"`
	TotalOrderAmount                        float64                                   `json:"total_order_amount" bson:"total_order_amount" example:"0"`
	PayTaxAmount                            float64                                   `json:"pay_tax_amount" bson:"pay_tax_amount" example:"0"`
	PayTaxRate                              float64                                   `json:"pay_tax_rate" bson:"pay_tax_rate" example:"0"`
	PaidAmount                              float64                                   `json:"paid_amount" bson:"paid_amount" example:"0"`
	BalanceAmount                           float64                                   `json:"balance_amount" bson:"balance_amount" example:"0"`
	OrderQuantity                           float64                                   `json:"order_quantity" bson:"order_quantity" example:"0"`
	CancelQuantity                          float64                                   `json:"cancel_quantity" bson:"cancel_quantity" example:"0"`
	DocStatus                               enum.CancelOrderStatus                    `json:"doc_status" bson:"doc_status" example:"Normal, Staff, Test, Exchange etc."`
	PaymentStatus                           string                                    `json:"payment_status" bson:"payment_status" example:"Pending, Partial, Complete,.."`
	Remark                                  string                                    `json:"remark" bson:"remark" example:"Remark"`
	AddValue                                string                                    `json:"add_value" bson:"add_value" example:"Gift Notes, instruction"`
	BillingAddressKey                       primitive.ObjectID                        `json:"billing_address_key" bson:"billing_address_key" example:"5ef2c053d86e577333da9b04"`
	BillingAddress                          BillingAddress                            `json:"billing_address" bson:"billing_address"`
	ShippingAddressKey                      primitive.ObjectID                        `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053d86e577333da9b04"`
	ShippingAddress                         ShippingAddress                           `json:"shipping_address" bson:"shipping_address"`
	Email                                   string                                    `json:"email" bson:"email" example:"abc@pamcs.com" validate:"required"`
	TrackingNo                              string                                    `json:"tracking_no" bson:"tracking_no" example:"apbca"`
	ObjectType                              string                                    `json:"object_type" bson:"object_type" example:"type"`
	CreatedDate                             time.Time                                 `json:"created_date" bson:"created_date" example:"11513512132"`
	CreatedBy                               primitive.ObjectID                        `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey                   primitive.ObjectID                        `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedDate                             time.Time                                 `json:"updated_date" bson:"updated_date" example:"2016-07-19T04:53:39 -07:00"`
	UpdatedBy                               primitive.ObjectID                        `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey                   primitive.ObjectID                        `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
	OrderCancellationDetailResponse         []OrderCancellationDetailResponse         `json:"order_cancellation_details" bson:"order_cancellation_details"`
	OrderCancellationPaymentResponse        []OrderCancellationPaymentResponse        `json:"order_cancellation_payments" bson:"order_cancellation_payments"`
	OrderCancellationCommentResponse        []OrderCancellationCommentResponse        `json:"order_cancellation_comments" bson:"order_cancellation_comments"`
	OrderCancellationShippingDetailResponse []OrderCancellationShippingDetailResponse `json:"order_cancellation_shipping_details" bson:"order_cancellation_shipping_details"`
	SalesOrderPaymentCaptures               []SalesOrderPaymentCaptureResponse        `json:"sales_order_payment_captures" bson:"sales_order_payment_captures"`
	SalesOrderPaymentRefunds                []SalesOrderPaymentRefundResponse         `json:"sales_order_payment_refunds" bson:"sales_order_payment_refunds"`
	SalesOrderPaymentReverses               []SalesOrderPaymentReverseResponse        `json:"sales_order_payment_reverses" bson:"sales_order_payment_reverses"`
}

type OrderCancellationExports struct {
	CmaNo          string                     `json:"cma_no" bson:"cma_no" example:"Doc001" validate:"required"`
	OrderNo        string                     `json:"order_no" bson:"order_no" example:"Doc001" validate:"required"`
	CancelDate     time.Time                  `json:"cancel_date" bson:"cancel_date" example:"123124818294" validate:"required"`
	OrderDate      time.Time                  `json:"order_date" bson:"order_date" example:"123124818294" validate:"required"`
	Customer       string                     `json:"customer_name" bson:"customer" validate:"required"`
	Country        string                     `json:"country" bson:"country" validate:"required"`
	SalesChannel   string                     `json:"sales_channel" bson:"sales_channel" validate:"required"`
	CancelStatus    enum.CancelOrderStatus     `json:"cancel_status" bson:"cancel_status" validate:"required"`
	CancelAmount   float64                    `json:"cancel_amount" bson:"cancel_amount" validate:"required"`
	EAN            string                     `json:"ean" bson:"ean"`
	Description    string                     `json:"description" bson:"description"`
	Size           string                     `json:"size" bson:"size"`
	Color          string                     `json:"color" bson:"color"`
	BasePrice      float64                    `json:"base_price" bson:"base_price"`
	LineStatus     enum.CancelOrderLineStatus `json:"line_status" bson:"line_status" validate:"required"`
	OrderQuantity  float64                    `json:"order_quantity" bson:"order_quantity"`
	CancelQuantity float64                    `json:"cancel_quantity" bson:"cancel_quantity"`
	DiscountAmount float64                    `json:"discount_amount" bson:"discount_amount"`
	ShippingFee    float64                    `json:"shipping_fee" bson:"shipping_fee"`
	SubTotal       float64                    `json:"sub_total" bson:"sub_total"`
	//ReturnQuantity float64                `json:"return_quantity" bson:"return_quantity"`
}

//------------- Function -------------
//func (main OrderCancellation) ToResponse() (res OrderCancellationResponse, err error) {
//	err = copier.Copy(&res, &main)
//	if err != nil {
//		return OrderCancellationResponse{}, err
//	}
//	res.DocDate = main.DocDueDate.Unix()
//	res.DocDueDate = main.DocDueDate.Unix()
//	res.CreatedDate = main.CreatedDate.Unix()
//	res.UpdatedDate = main.UpdatedDate.Unix()
//
//	return res, nil
//}

func (req OrderCancellationRequest) Validation() (err error) {
	err = req.CheckBaseHeader()
	if err != nil {
		return errors.New("Check Base base header ERROR - " + err.Error())
	}

	err = req.CheckBaseLine()
	if err != nil {
		return errors.New("Check Base Doc line ERROR - " + err.Error())
	}

	return nil
}

func (req OrderCancellationRequest) CheckBaseHeader() (err error) {
	var salesOrder SalesOrder

	col := db.Database.Collection(setting.SalesOrderTable)
	filter := bson.M{"doc_num": req.BaseDocNum}
	//Check if Sales Order doc num exists
	if err = col.FindOne(context.Background(), filter).Decode(&salesOrder); err != nil {
		return errors.New("Find Sales Order Doc num ERROR -" + err.Error())
	}

	return nil
}

func (req OrderCancellationRequest) CheckBaseLine() (err error) {
	var salesOrderDetail SalesOrderDetail

	col := db.Database.Collection(setting.SalesOrderDetailTable)
	if len(req.OrderCancellationDetailRequest) > 0 {
		for _, line := range req.OrderCancellationDetailRequest {
			filter := bson.M{"doc_num": req.BaseDocNum, "doc_line_num": line.BaseDocLineNum}
			if err = col.FindOne(context.Background(), filter).Decode(&salesOrderDetail); err != nil {
				return errors.New(fmt.Sprintf("Find Sales Order Doc line num '%s' ERROR - %s", line.BaseDocLineNum, err.Error()))
			}

			if line.Quantity == 0 {
				return errors.New(fmt.Sprintf("Cannot cancel '%v' quantity ", line.Quantity))
			}

			if line.Quantity > (salesOrderDetail.Quantity - salesOrderDetail.CancelQuantity) {
				return errors.New(fmt.Sprintf("Cannot cancel '%v' more than remain quantity '%v'", line.Quantity, (salesOrderDetail.Quantity - salesOrderDetail.CancelQuantity)))
			}
		}
	} else {
		return errors.New("Please enter at least one line for cancel detail ")
	}

	return nil
}
