package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Tenant struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef309d05787c3fc462328b0"`
	TenantID              string             `json:"tenant_id" bson:"tenant_id" example:"Lorem"`
	Description           string             `json:"description" bson:"description" example:"quis"`
	LocalCurrencyKey      primitive.ObjectID `json:"local_currency_key" bson:"local_currency_key" example:"5ef309d079dc60cfcd1d1f67"`
	ZipCode               string             `json:"zip_code" bson:"zip_code" example:"ABC"`
	City                  string             `json:"city" bson:"city" example:"ABC"`
	CountryKey            primitive.ObjectID `json:"country_key" bson:"country_key" example:"5ef309d079dc60cfcd1d1f67"`
	CountryId             string             `json:"country_id" bson:"country_id" example:"VN"`
	ContactPerson1        string             `json:"contact_person_1" bson:"contact_person_1" example:"Baldwin Lopez"`
	ContactEmail1         string             `json:"contact_email_1" bson:"contact_email_1" example:"baldwinlopez@geekko.com"`
	ContactNumber1        string             `json:"contact_number_1" bson:"contact_number_1" example:"+1 (813) 448-2585"`
	ContactPerson2        string             `json:"contact_person_2" bson:"contact_person_2" example:"Wise Green"`
	ContactEmail2         string             `json:"contact_email_2" bson:"contact_email_2" example:"wisegreen@geekko.com"`
	ContactNumber2        string             `json:"contact_number_2" bson:"contact_number_2" example:"+1 (831) 472-2704"`
	Address1              string             `json:"address_1" bson:"address_1" example:"281 Division Place, Gulf, Virgin Islands, 2312"`
	Address2              string             `json:"address_2" bson:"address_2" example:"Rogers Avenue, Crayne, Maine, 9028"`
	Address3              string             `json:"address_3" bson:"address_3" example:"Troutman Street, Joppa, Montana, 4269"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"id"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2019-12-28T10:40:48 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef309d0e15a6882e4313243"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef309d076f6ba80bbb65827"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2020-02-28T10:40:48 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef309d0cf592f2a6dbeb21d"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef309d0f585952856ad0d93"`
}

type TenantRequest struct {
	TenantID              string                 `json:"tenant_id" bson:"tenant_id" example:"Lorem"`
	Description           string                 `json:"description" bson:"description" example:"quis"`
	LocalCurrencyKey      primitive.ObjectID     `json:"local_currency_key" bson:"local_currency_key" example:"5ef309d079dc60cfcd1d1f67"`
	ZipCode               string                 `json:"zip_code" bson:"zip_code" example:"ABC"`
	City                  string                 `json:"city" bson:"city" example:"ABC"`
	CountryKey            primitive.ObjectID     `json:"country_key" bson:"country_key" example:"5ef309d079dc60cfcd1d1f67"`
	CountryId             string                 `json:"country_id" bson:"country_id" example:"VN"`
	ContactPerson1        string                 `json:"contact_person_1" bson:"contact_person_1" example:"Baldwin Lopez"`
	ContactEmail1         string                 `json:"contact_email_1" bson:"contact_email_1" example:"baldwinlopez@geekko.com"`
	ContactNumber1        string                 `json:"contact_number_1" bson:"contact_number_1" example:"+1 (813) 448-2585"`
	ContactPerson2        string                 `json:"contact_person_2" bson:"contact_person_2" example:"Wise Green"`
	ContactEmail2         string                 `json:"contact_email_2" bson:"contact_email_2" example:"wisegreen@geekko.com"`
	ContactNumber2        string                 `json:"contact_number_2" bson:"contact_number_2" example:"+1 (831) 472-2704"`
	Address1              string                 `json:"address_1" bson:"address_1" example:"281 Division Place, Gulf, Virgin Islands, 2312"`
	Address2              string                 `json:"address_2" bson:"address_2" example:"Rogers Avenue, Crayne, Maine, 9028"`
	Address3              string                 `json:"address_3" bson:"address_3" example:"Troutman Street, Joppa, Montana, 4269"`
	IsActive              bool                   `json:"is_active" bson:"is_active" example:"true"`
	ObjectType            string                 `json:"object_type" bson:"object_type" example:"id"`
	CreatedBy             primitive.ObjectID     `json:"created_by" bson:"created_by" example:"5ef309d0e15a6882e4313243"`
	CreatedApplicationKey primitive.ObjectID     `json:"created_application_key" bson:"created_application_key" example:"5ef309d076f6ba80bbb65827"`
	UpdatedBy             primitive.ObjectID     `json:"updated_by" bson:"updated_by" example:"5ef309d0cf592f2a6dbeb21d"`
	UpdatedApplicationKey primitive.ObjectID     `json:"updated_application_key" bson:"updated_application_key" example:"5ef309d0f585952856ad0d93"`
	TenantSettingRequest  []TenantSettingRequest `json:"tenant_settings" bson:"tenant_settings"`
}

type TenantResponse struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef309d05787c3fc462328b0"`
	TenantID              string             `json:"tenant_id" bson:"tenant_id" example:"Lorem"`
	Description           string             `json:"description" bson:"description" example:"quis"`
	LocalCurrencyKey      primitive.ObjectID `json:"local_currency_key" bson:"local_currency_key" example:"5ef309d079dc60cfcd1d1f67"`
	ZipCode               string             `json:"zip_code" bson:"zip_code" example:"ABC"`
	City                  string             `json:"city" bson:"city" example:"ABC"`
	CountryKey            primitive.ObjectID `json:"country_key" bson:"country_key" example:"5ef309d079dc60cfcd1d1f67"`
	CountryId             string             `json:"country_id" bson:"country_id" example:"VN"`
	ContactPerson1        string             `json:"contact_person_1" bson:"contact_person_1" example:"Baldwin Lopez"`
	ContactEmail1         string             `json:"contact_email_1" bson:"contact_email_1" example:"baldwinlopez@geekko.com"`
	ContactNumber1        string             `json:"contact_number_1" bson:"contact_number_1" example:"+1 (813) 448-2585"`
	ContactPerson2        string             `json:"contact_person_2" bson:"contact_person_2" example:"Wise Green"`
	ContactEmail2         string             `json:"contact_email_2" bson:"contact_email_2" example:"wisegreen@geekko.com"`
	ContactNumber2        string             `json:"contact_number_2" bson:"contact_number_2" example:"+1 (831) 472-2704"`
	Address1              string             `json:"address_1" bson:"address_1" example:"281 Division Place, Gulf, Virgin Islands, 2312"`
	Address2              string             `json:"address_2" bson:"address_2" example:"Rogers Avenue, Crayne, Maine, 9028"`
	Address3              string             `json:"address_3" bson:"address_3" example:"Troutman Street, Joppa, Montana, 4269"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"id"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2019-12-28T10:40:48 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef309d0e15a6882e4313243"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef309d076f6ba80bbb65827"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2020-02-28T10:40:48 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef309d0cf592f2a6dbeb21d"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef309d0f585952856ad0d93"`
	TenantSetting         []TenantSetting    `json:"tenant_settings" bson:"tenant_settings"`
}

type TenantSettingIds struct {
	TenantSettingIds []string `json:"setting_ids" bson:"setting_ids"`
}
