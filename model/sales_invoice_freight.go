package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// SalesInvoiceFreight struct
type SalesInvoiceFreight struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef2c053f7a34f7669bb8246"`
	DocFreightKey         primitive.ObjectID `json:"comment_key" bson:"comment_key" example:"5ef2c053f7a34f7669bb8246"`
	DocLineKey            primitive.ObjectID `json:"doc_line_key" bson:"doc_line_key" example:"5ef2c053f7a34f7669bb8246"`
	DocKey                primitive.ObjectID `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246"`
	FreightKey            primitive.ObjectID `json:"freight_key" bson:"freight_key" example:"5ef2c053f7a34f7669bb8246"`
	FreightAmount         float64            `json:"freight_amount" bson:"freight_amount" example:"0"`
	TaxCodeKey            primitive.ObjectID `json:"tax_code_key" bson:"tax_code_key" example:"5ef2c053f7a34f7669bb8246"`
	TaxRate               float64            `json:"tax_rate" bson:"tax_rate" example:"0"`
	TaxAmount             float64            `json:"tax_amount" bson:"tax_amount" example:"0"`
	TotalAfterTax         float64            `json:"total_after_tax" bson:"total_after_tax" example:"0"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"type"`
	CreatedDate           time.Time          `json:"created_date" bson:"created_date" example:"2016-07-19T04:53:39 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedDate           time.Time          `json:"updated_date" bson:"updated_date" example:"2016-07-19T04:53:39 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"false"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
}

// AddSalesInvoiceFreight struct
type AddSalesInvoiceFreight struct {
	DocFreightKey         primitive.ObjectID `json:"comment_key" bson:"comment_key" example:"5ef2c053f7a34f7669bb8246"`
	DocLineKey            primitive.ObjectID `json:"doc_line_key" bson:"doc_line_key" example:"5ef2c053f7a34f7669bb8246"`
	DocKey                primitive.ObjectID `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246"`
	FreightKey            primitive.ObjectID `json:"freight_key" bson:"freight_key" example:"5ef2c053f7a34f7669bb8246"`
	FreightAmount         float64            `json:"freight_amount" bson:"freight_amount" example:"0"`
	TaxCodeKey            primitive.ObjectID `json:"tax_code_key" bson:"tax_code_key" example:"5ef2c053f7a34f7669bb8246"`
	TaxRate               float64            `json:"tax_rate" bson:"tax_rate" example:"0"`
	TaxAmount             float64            `json:"tax_amount" bson:"tax_amount" example:"0"`
	TotalAfterTax         float64            `json:"total_after_tax" bson:"total_after_tax" example:"0"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"type"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"false"`
}
