package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type InventoryStatus struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5f02aab989bfe6971405ae74"`
	ProductKey            primitive.ObjectID `json:"product_key" bson:"product_key" example:"5f02aab989bfe6971405ae74"`
	ProductId             string             `json:"product_id" bson:"product_id" example:"eiusmod"`
	ProductName           string             `json:"product_name" bson:"product_name" example:"eiusmod"`
	SiteKey               primitive.ObjectID `json:"site_key" bson:"site_key" example:"5f02aab973afa1a45e4562cc"`
	SiteId                string             `json:"site_id" bson:"site_id" example:"eiusmod"`
	SiteName              string             `json:"site_name" bson:"site_name" example:"eiusmod"`
	OnHandQty             float64            `json:"on_hand_qty" bson:"on_hand_qty" example:"230"`
	YchQty                float64            `json:"ych_qty" bson:"ych_qty" example:"230"`
	OnOrderQty            float64            `json:"on_order_qty" bson:"on_order_qty" example:"230"`
	OnCommitQty           float64            `json:"on_commit_qty" bson:"on_commit_qty" example:"230"`
	OnTsfQty              float64            `json:"on_tsf_qty" bson:"on_tsf_qty" example:"230"`
	ReserveQty            float64            `json:"reserve_qty" bson:"reserve_qty" example:"230"`
	ShippedQty            float64            `json:"shipped_qty" bson:"shipped_qty" example:"230"`
	OnAvailableQty        float64            `json:"on_available_qty" bson:"on_available_qty" example:"230"`
	ProductCost           float64            `json:"product_cost" bson:"product_cost" example:"488.95"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"false"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"eiusmod"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2016-06-13T05:22:29 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efc1cfa46e9968f9c6d9d7c"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5efc1cfaaec3d46ad7bf90df"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2017-08-14T06:19:22 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efc1cfadbb8358614063344"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5efc1cfa023ed7deb7430b8c"`
}

type InventoryStatusResponse struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5f02aab989bfe6971405ae74"`
	ProductKey            primitive.ObjectID `json:"product_key" bson:"product_key" example:"5f02aab989bfe6971405ae74"`
	ProductId             string             `json:"product_id" bson:"product_id" example:"eiusmod"`
	ProductName           string             `json:"product_name" bson:"product_name" example:"eiusmod"`
	SiteKey               primitive.ObjectID `json:"site_key" bson:"site_key" example:"5f02aab973afa1a45e4562cc"`
	SiteId                string             `json:"site_id" bson:"site_id" example:"eiusmod"`
	SiteName              string             `json:"site_name" bson:"site_name" example:"eiusmod"`
	OnHandQty             float64            `json:"on_hand_qty" bson:"on_hand_qty" example:"230"`
	YchQty                float64            `json:"ych_qty" bson:"ych_qty" example:"230"`
	OnOrderQty            float64            `json:"on_order_qty" bson:"on_order_qty" example:"230"`
	OnCommitQty           float64            `json:"on_commit_qty" bson:"on_commit_qty" example:"230"`
	OnTsfQty              float64            `json:"on_tsf_qty" bson:"on_tsf_qty" example:"230"`
	ReserveQty            float64            `json:"reserve_qty" bson:"reserve_qty" example:"230"`
	ShippedQty            float64            `json:"shipped_qty" bson:"shipped_qty" example:"230"`
	OnAvailableQty        float64            `json:"on_available_qty" bson:"on_available_qty" example:"230"`
	NewQty                float64            `json:"new_qty" bson:"new_qty" example:"230"`
	ProcessingQty         float64            `json:"processing_qty" bson:"processing_qty" example:"230"`
	PickedQty             float64            `json:"picked_qty" bson:"picked_qty" example:"230"`
	PackedQty             float64            `json:"packed_qty" bson:"packed_qty" example:"230"`
	ReturnQty             float64            `json:"return_qty" bson:"return_qty" example:"230"`
	CancelQty             float64            `json:"cancel_qty" bson:"cancel_qty" example:"230"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2016-06-13T05:22:29 -07:00"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2017-08-14T06:19:22 -07:00"`
}

type InventoryStatusRequest struct {
	ProductId  string  `json:"product_id" bson:"product_id" example:"eiusmod"`
	SiteId     string  `json:"site_id" bson:"site_id" example:"eiusmod"`
	OnHandQty  float64 `json:"on_hand_qty" bson:"on_hand_qty" example:"230"`
	OnOrderQty float64 `json:"on_order_qty,omitempty" bson:"on_order_qty,omitempty" example:"230"`
}

func (is InventoryStatus) RecalculateQuantity() (InventoryStatus, error) {
	is.OnAvailableQty = is.OnHandQty - is.OnOrderQty
	return is, nil
}
