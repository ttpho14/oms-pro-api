package model

import (
	db "../database"
	"../setting"
	"./enum"
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Return struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef96d9c5954cbf2ea3cf7af"`
	SID                   string             `json:"sid" bson:"sid" example:"Doc001" validate:"required"`
	DocNum                string             `json:"doc_num" bson:"doc_num" example:"Doc001"`
	SiteId                string             `json:"site_id" bson:"site_id" example:"SGWHS" validate:"required"`
	AccountCode           string             `json:"account_code" bson:"account_code" example:"Account code" validate:"required"`
	SalesChannelKey       primitive.ObjectID `json:"sales_channel_key" bson:"sales_channel_key" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	SalesChannelId        string             `json:"sales_channel_id" bson:"sales_channel_id" example:"SALESCHANNEL001" validate:"required"`
	SalesChannelName      string             `json:"sales_channel_name" bson:"sales_channel_name" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	ReasonKey             primitive.ObjectID `json:"reason_key" bson:"reason_key" example:"5ef96d9c5954cbf2ea3cf7af"`
	ReasonId              string             `json:"reason_id" bson:"reason_id" example:"Reason_ID"`
	ReasonDescription     string             `json:"reason_description" bson:"reason_description" example:"Reason description"`
	CountryKey            primitive.ObjectID `json:"country_key" bson:"country_key" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	CountryId             string             `json:"country_id" bson:"country_id" example:"COUNTRY0001" validate:"required"`
	RefNo                 string             `json:"ref_no" bson:"ref_no" example:"123456"`
	RefNo1                string             `json:"ref_no_1" bson:"ref_no_1" example:"123456"`
	CustomerKey           primitive.ObjectID `json:"customer_key" bson:"customer_key" example:"5efc6f5fa74d733d55d7a390"`
	CustomerName          string             `json:"customer_name" bson:"customer_name" example:"James"`
	OrderDate             time.Time          `json:"order_date" bson:"order_date" example:"2016-07-19T04:53:39 -07:00"`
	DocDate               time.Time          `json:"doc_date" bson:"doc_date" example:"2016-07-19T04:53:39 -07:00"`
	DocDueDate            time.Time          `json:"doc_due_date" bson:"doc_due_date"  example:"2016-07-19T04:53:39 -07:00"`
	BaseDocKey            primitive.ObjectID `json:"base_doc_key" bson:"base_doc_key"  example:"5efc6f5fa74d733d55d7a390"`
	BaseDocNum            string             `json:"base_doc_num" bson:"base_doc_num"  example:"abacx"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5ef2c053d86e577333da9b04"`
	CurrencyKey           primitive.ObjectID `json:"currency_key" bson:"currency_key" example:"USD"`
	CurrencyId            string             `json:"currency_id" bson:"currency_id"`
	CurrencyRate          float64            `json:"currency_rate" bson:"currency_rate" example:"1"`
	TotalBeforeDiscount   float64            `json:"total_before_discount" bson:"total_before_discount" example:"0"`
	DiscountAmount        float64            `json:"discount_amount" bson:"discount_amount" example:"0"`
	DiscountPercent       float64            `json:"discount_percent" bson:"discount_percent" example:"0"`
	TotalAfterDiscount    float64            `json:"total_after_discount" bson:"total_after_discount" example:"0"`
	FreightAmount         float64            `json:"freight_amount" bson:"freight_amount" example:"0"`
	TaxAmount             float64            `json:"tax_amount" bson:"tax_amount" example:"0"`
	TaxRate               float64            `json:"tax_rate" bson:"tax_rate" example:"0"`
	TotalAfterTax         float64            `json:"total_after_tax" bson:"total_after_tax" example:"0"`
	PayTaxAmount          float64            `json:"pay_tax_amount" bson:"pay_tax_amount" example:"0"`
	PayTaxRate            float64            `json:"pay_tax_rate" bson:"pay_tax_rate" example:"0"`
	PaidAmount            float64            `json:"paid_amount" bson:"paid_amount" example:"0"`
	BalanceAmount         float64            `json:"balance_amount" bson:"balance_amount" example:"0"`
	DocStatus             enum.ReturnStatus  `json:"doc_status" bson:"doc_status" example:"Normal, Staff, Test, Exchange etc."`
	PaymentStatus         string             `json:"payment_status" bson:"payment_status" example:"Pending, Partial, Complete,.."`
	IsExchange            bool               `json:"is_exchange" bson:"is_exchange" example:"false"`
	Remark                string             `json:"remark" bson:"remark" example:"Remark"`
	Carrier               string             `json:"carrier"bson:"carrier" exmample:"DHL"`
	Method                string             `json:"method" bson:"method" example:"APCSAS"`
	BillingAddressKey     primitive.ObjectID `json:"billing_address_key" bson:"billing_address_key" example:"5ef2c053d86e577333da9b04"`
	BillingAddress        BillingAddress     `json:"billing_address" bson:"billing_address"`
	ShippingAddressKey    primitive.ObjectID `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053d86e577333da9b04"`
	ShippingAddress       ShippingAddress    `json:"shipping_address" bson:"shipping_address"`
	Email                 string             `json:"email" bson:"email" example:"abc@pamcs.com" validate:"required"`
	SAPExport             bool               `json:"sap_export" bson:"sap_export" validate:"false"`
	TrackingNo            string             `json:"tracking_no" bson:"tracking_no" example:"apbca"`
	PickupDate            time.Time          `json:"pickup_date" bson:"pickup_date" example:"apbca"`
	Instruction           string             `json:"instruction" bson:"instruction" example:"apbca"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"type"`
	CreatedDate           time.Time          `json:"created_date" bson:"created_date" example:"2016-07-19T04:53:39 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedDate           time.Time          `json:"updated_date" bson:"updated_date" example:"2016-07-19T04:53:39 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
}

type ReturnResponse struct {
	ID                        primitive.ObjectID                 `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef96d9c5954cbf2ea3cf7af"`
	DocNum                    string                             `json:"doc_num" bson:"doc_num" example:"Doc001"`
	SiteId                    string                             `json:"site_id" bson:"site_id" example:"SGWHS" validate:"required"`
	AccountCode               string                             `json:"account_code" bson:"account_code" example:"Account code" validate:"required"`
	SalesChannelKey           primitive.ObjectID                 `json:"sales_channel_key" bson:"sales_channel_key" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	SalesChannelId            string                             `json:"sales_channel_id" bson:"sales_channel_id" example:"SALESCHANNEL001" validate:"required"`
	SalesChannelName          string                             `json:"sales_channel_name" bson:"sales_channel_name" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	ReasonKey                 primitive.ObjectID                 `json:"reason_key" bson:"reason_key" example:"5ef96d9c5954cbf2ea3cf7af"`
	ReasonId                  string                             `json:"reason_id" bson:"reason_id" example:"Reason_ID"`
	OrderQuantity             float64                            `json:"order_quantity" bson:"order_quantity" example:"0"`
	ReturnQuantity            float64                            `json:"return_quantity" bson:"return_quantity" example:"0"`
	ReasonDescription         string                             `json:"reason_description" bson:"reason_description" example:"Reason description"`
	CountryKey                primitive.ObjectID                 `json:"country_key" bson:"country_key" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	CountryId                 string                             `json:"country_id" bson:"country_id" example:"COUNTRY0001" validate:"required"`
	RefNo                     string                             `json:"ref_no" bson:"ref_no" example:"123456"`
	RefNo1                    string                             `json:"ref_no_1" bson:"ref_no_1" example:"123456"`
	CustomerKey               primitive.ObjectID                 `json:"customer_key" bson:"customer_key" example:"5efc6f5fa74d733d55d7a390"`
	CustomerName              string                             `json:"customer_name" bson:"customer_name" example:"James"`
	OrderDate                 time.Time                          `json:"order_date" bson:"order_date" example:"2016-07-19T04:53:39 -07:00"`
	DocDate                   time.Time                          `json:"doc_date" bson:"doc_date" example:"2016-07-19T04:53:39 -07:00"`
	DocDueDate                time.Time                          `json:"doc_due_date" bson:"doc_due_date"  example:"2016-07-19T04:53:39 -07:00"`
	BaseDocKey                primitive.ObjectID                 `json:"base_doc_key" bson:"base_doc_key"  example:"5efc6f5fa74d733d55d7a390"`
	BaseDocNum                string                             `json:"base_doc_num" bson:"base_doc_num"  example:"abacx"`
	TenantKey                 primitive.ObjectID                 `json:"tenant_key" bson:"tenant_key" example:"5ef2c053d86e577333da9b04"`
	CurrencyKey               primitive.ObjectID                 `json:"currency_key" bson:"currency_key" example:"USD"`
	CurrencyId                string                             `json:"currency_id" bson:"currency_id"`
	CurrencyRate              float64                            `json:"currency_rate" bson:"currency_rate" example:"1"`
	TotalBeforeDiscount       float64                            `json:"total_before_discount" bson:"total_before_discount" example:"0"`
	DiscountAmount            float64                            `json:"discount_amount" bson:"discount_amount" example:"0"`
	DiscountPercent           float64                            `json:"discount_percent" bson:"discount_percent" example:"0"`
	TotalAfterDiscount        float64                            `json:"total_after_discount" bson:"total_after_discount" example:"0"`
	FreightAmount             float64                            `json:"freight_amount" bson:"freight_amount" example:"0"`
	TaxAmount                 float64                            `json:"tax_amount" bson:"tax_amount" example:"0"`
	TaxRate                   float64                            `json:"tax_rate" bson:"tax_rate" example:"0"`
	TotalAfterTax             float64                            `json:"total_after_tax" bson:"total_after_tax" example:"0"`
	TotalOrderAmount          float64                            `json:"total_order_amount" bson:"total_order_amount" example:"0"`
	PayTaxAmount              float64                            `json:"pay_tax_amount" bson:"pay_tax_amount" example:"0"`
	PayTaxRate                float64                            `json:"pay_tax_rate" bson:"pay_tax_rate" example:"0"`
	PaidAmount                float64                            `json:"paid_amount" bson:"paid_amount" example:"0"`
	BalanceAmount             float64                            `json:"balance_amount" bson:"balance_amount" example:"0"`
	DocStatus                 enum.ReturnStatus                  `json:"doc_status" bson:"doc_status" example:"Normal, Staff, Test, Exchange etc."`
	PaymentStatus             string                             `json:"payment_status" bson:"payment_status" example:"Pending, Partial, Complete,.."`
	IsExchange                bool                               `json:"is_exchange" bson:"is_exchange" example:"false"`
	Remark                    string                             `json:"remark" bson:"remark" example:"Remark"`
	Carrier                   string                             `json:"carrier"bson:"carrier" exmample:"DHL"`
	Method                    string                             `json:"method" bson:"method" example:"APCSAS"`
	BillingAddressKey         primitive.ObjectID                 `json:"billing_address_key" bson:"billing_address_key" example:"5ef2c053d86e577333da9b04"`
	BillingAddress            BillingAddress                     `json:"billing_address" bson:"billing_address"`
	ShippingAddressKey        primitive.ObjectID                 `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053d86e577333da9b04"`
	ShippingAddress           ShippingAddress                    `json:"shipping_address" bson:"shipping_address"`
	Email                     string                             `json:"email" bson:"email" example:"abc@pamcs.com" validate:"required"`
	SAPExport                 bool                               `json:"sap_export" bson:"sap_export" validate:"false"`
	TrackingNo                string                             `json:"tracking_no" bson:"tracking_no" example:"apbca"`
	PickupDate                time.Time                          `json:"pickup_date" bson:"pickup_date" example:"apbca"`
	Instruction               string                             `json:"instruction" bson:"instruction" example:"apbca"`
	ObjectType                string                             `json:"object_type" bson:"object_type" example:"type"`
	CreatedDate               time.Time                          `json:"created_date" bson:"created_date" example:"2016-07-19T04:53:39 -07:00"`
	CreatedBy                 primitive.ObjectID                 `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey     primitive.ObjectID                 `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedDate               time.Time                          `json:"updated_date" bson:"updated_date" example:"2016-07-19T04:53:39 -07:00"`
	UpdatedBy                 primitive.ObjectID                 `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey     primitive.ObjectID                 `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
	ReturnDetail              []ReturnDetailResponse             `json:"return_details" bson:"return_details"`
	ReturnPayment             []ReturnPaymentResponse            `json:"return_payments" bson:"return_payments"`
	ReturnComment             []ReturnCommentResponse            `json:"return_comments" bson:"return_comments"`
	ReturnShippingDetail      []ReturnShippingDetailResponse     `json:"return_shipping_details" bson:"return_shipping_details"`
	ReturnStatus              []ReturnStatusResponse             `json:"return_status" bson:"return_status"`
	SalesOrderPaymentCaptures []SalesOrderPaymentCaptureResponse `json:"sales_order_payment_captures" bson:"sales_order_payment_captures"`
	SalesOrderPaymentRefunds  []SalesOrderPaymentRefundResponse  `json:"sales_order_payment_refunds" bson:"sales_order_payment_refunds"`
	SalesOrderPaymentReverses []SalesOrderPaymentReverseResponse `json:"sales_order_payment_reverses" bson:"sales_order_payment_reverses"`
}

type ReturnRequest struct {
	ReasonKey                   primitive.ObjectID            `json:"reason_key" bson:"reason_key" example:"5ef96d9c5954cbf2ea3cf7af"`
	ReasonId                    string                        `json:"reason_id" bson:"reason_id" example:"Reason_ID"`
	ReasonDescription           string                        `json:"reason_description" bson:"reason_description" example:"Reason description"`
	FreightAmount               float64                       `json:"freight_amount" bson:"freight_amount" example:"0"`
	BaseDocNum                  string                        `json:"base_doc_num" bson:"base_doc_num"  example:"abacx"`
	DocStatus                   enum.ReturnStatus             `json:"doc_status" bson:"doc_status" example:"Normal, Staff, Test, Exchange etc."`
	SAPExport                   bool                          `json:"sap_export" bson:"sap_export" validate:"false"`
	PickupDate                  time.Time                     `json:"pickup_date" bson:"pickup_date" example:"apbca"`
	Instruction                 string                        `json:"instruction" bson:"instruction" example:"apbca"`
	CreatedBy                   primitive.ObjectID            `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey       primitive.ObjectID            `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedBy                   primitive.ObjectID            `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey       primitive.ObjectID            `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
	ReturnDetailRequest         []ReturnDetailRequest         `json:"return_details" bson:"return_details"`
	ReturnPaymentRequest        []ReturnPaymentRequest        `json:"return_payments" bson:"return_payments"`
	ReturnCommentRequest        []ReturnCommentRequest        `json:"return_comments" bson:"return_comments"`
	ReturnShippingDetailRequest []ReturnShippingDetailRequest `json:"return_shipping_details" bson:"return_shipping_details"`
	//DocDate               int64                  `json:"doc_date" bson:"doc_date" example:"1598242726"`
	//DocDueDate            int64                  `json:"doc_due_date" bson:"doc_due_date"  example:"1598442726"`

	//PaymentStatus         string                 `json:"payment_status" bson:"payment_status" example:"Pending, Partial, Complete,.."`
	//IsExchange            bool                   `json:"is_exchange" bson:"is_exchange" example:"false"`
	//Remark                string                 `json:"remark" bson:"remark" example:"Remark"`
	//Carrier               string                 `json:"carrier"bson:"carrier" exmample:"DHL"`
	//Method                string                 `json:"method" bson:"method" example:"APCSAS"`
	//BillingAddressKey     primitive.ObjectID     `json:"billing_address_key" bson:"billing_address_key" example:"5ef2c053d86e577333da9b04"`
	//BillingAddress        BillingAddress         `json:"billing_address" bson:"billing_address"`
	//ShippingAddressKey    primitive.ObjectID     `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053d86e577333da9b04"`
	//ShippingAddress       ShippingAddress        `json:"shipping_address" bson:"shipping_address"`
	//ObjectType            string                 `json:"object_type" bson:"object_type" example:"type"`

	//TenantKey             primitive.ObjectID     `json:"tenant_key" bson:"tenant_key" example:"5ef2c053d86e577333da9b04"`
	//CurrencyKey           primitive.ObjectID     `json:"currency_key" bson:"currency_key" example:"USD"`
	//CurrencyId            string                 `json:"currency_id" bson:"currency_id"`
	//CurrencyRate          float64                `json:"currency_rate" bson:"currency_rate" example:"1"`
	//TotalBeforeDiscount   float64                `json:"total_before_discount" bson:"total_before_discount" example:"0"`
	//DiscountAmount        float64                `json:"discount_amount" bson:"discount_amount" example:"0"`
	//DiscountPercent       float64                `json:"discount_percent" bson:"discount_percent" example:"0"`
	//TotalAfterDiscount    float64                `json:"total_after_discount" bson:"total_after_discount" example:"0"`
	//FreightAmount         float64                `json:"freight_amount" bson:"freight_amount" example:"0"`
	//TaxAmount             float64                `json:"tax_amount" bson:"tax_amount" example:"0"`
	//TaxRate               float64                `json:"tax_rate" bson:"tax_rate" example:"0"`
	//TotalAfterTax         float64                `json:"total_after_tax" bson:"total_after_tax" example:"0"`
	//PayTaxAmount          float64                `json:"pay_tax_amount" bson:"pay_tax_amount" example:"0"`
	//PayTaxRate            float64                `json:"pay_tax_rate" bson:"pay_tax_rate" example:"0"`
	//PaidAmount            float64                `json:"paid_amount" bson:"paid_amount" example:"0"`
	//BalanceAmount         float64                `json:"balance_amount" bson:"balance_amount" example:"0"`

	//SalesChannelId        string                 `json:"sales_channel_id" bson:"sales_channel_id" example:"123456"`
	//RefNo                 string                 `json:"ref_no" bson:"ref_no" example:"123456"`

	//CustomerKey           primitive.ObjectID     `json:"customer_key" bson:"customer_key" example:"5efc6f5fa74d733d55d7a390"`
	//CustomerName          string                 `json:"customer_name" bson:"customer_name" example:"James"`

	//ReturnFreightRequest []ReturnFreightRequest `json:"return_freights" bson:"return_freights"`
}

type ReturnUpdateRequest struct {
	BaseDocNum                string                      `json:"base_doc_num,omitempty" bson:"base_doc_num,omitempty" example:"123456"`
	DocStatus                 enum.ReturnStatus           `json:"doc_status,omitempty" bson:"doc_status,omitempty" example:"Normal, Staff, Test, Exchange etc."`
	PaymentStatus             string                      `json:"payment_status,omitempty" bson:"payment_status,omitempty" example:"123456"`
	RefNo                     string                      `json:"ref_no,omitempty" bson:"ref_no,omitempty" example:"123456"`
	ReturnDetailUpdateRequest []ReturnDetailUpdateRequest `json:"return_details,omitempty" bson:"return_details,omitempty"`
	//RefNo1                 string                  `json:"ref_no_1" bson:"ref_no_1" example:"123456"`
}

type ReturnExports struct {
	RmaNo          string                  `json:"rma_no" bson:"rma_no" example:"Doc001" validate:"required"`
	OrderNo        string                  `json:"order_no" bson:"order_no" example:"Doc001" validate:"required"`
	ReturnDate     time.Time               `json:"return_date" bson:"return_date" example:"123124818294" validate:"required"`
	OrderDate      time.Time               `json:"order_date" bson:"order_date" example:"123124818294" validate:"required"`
	Customer       string                  `json:"customer_name" bson:"customer" validate:"required"`
	Country        string                  `json:"country" bson:"country" validate:"required"`
	SalesChannel   string                  `json:"sales_channel" bson:"sales_channel" validate:"required"`
	ReturnStatus   enum.ReturnStatus       `json:"return_status" bson:"return_status" validate:"required"`
	ReturnAmount   float64                 `json:"return_amount" bson:"return_amount" validate:"required"`
	EAN            string                  `json:"ean" bson:"ean"`
	Description    string                  `json:"description" bson:"description"`
	Size           string                  `json:"size" bson:"size"`
	Color          string                  `json:"color" bson:"color"`
	BasePrice      float64                 `json:"base_price" bson:"base_price"`
	LineStatus     enum.ReturnDetailStatus `json:"line_status" bson:"line_status" validate:"required"`
	OrderQuantity  float64                 `json:"order_quantity" bson:"order_quantity"`
	ReturnQuantity float64                 `json:"return_quantity" bson:"return_quantity"`
	DiscountAmount float64                 `json:"discount_amount" bson:"discount_amount"`
	ShippingFee    float64                 `json:"shipping_fee" bson:"shipping_fee"`
	SubTotal       float64                 `json:"sub_total" bson:"sub_total"`
	//CancelQuantity float64          `json:"cancel_quantity" bson:"cancel_quantity"`
}

func (req ReturnRequest) Validation() (err error) {
	err = req.CheckBaseHeader()
	if err != nil {
		return errors.New("Check Base header - " + err.Error())
	}

	err = req.CheckBaseLine()
	if err != nil {
		return errors.New("Check Base line - " + err.Error())
	}

	return nil

}

func (req ReturnRequest) CheckBaseHeader() (err error) {
	var salesOrder SalesOrder

	col := db.Database.Collection(setting.SalesOrderTable)
	filter := bson.M{"doc_num": req.BaseDocNum}
	//Check if Sales Order doc num exists
	if err = col.FindOne(context.Background(), filter).Decode(&salesOrder); err != nil {
		return errors.New("Find Sales Order Doc num ERROR -" + err.Error())
	}

	return nil
}

func (req ReturnRequest) CheckBaseLine() (err error) {
	var salesOrderDetail SalesOrderDetail

	col := db.Database.Collection(setting.SalesOrderDetailTable)
	if len(req.ReturnDetailRequest) > 0 {
		for _, line := range req.ReturnDetailRequest {
			filter := bson.M{"doc_num": req.BaseDocNum, "doc_line_num": line.BaseDocLineNum}
			if err = col.FindOne(context.Background(), filter).Decode(&salesOrderDetail); err != nil {
				return errors.New(fmt.Sprintf("Find Sales Order Doc line num '%s' ERROR - %s", line.BaseDocLineNum, err.Error()))
			}

			if line.Quantity == 0 {
				return errors.New(fmt.Sprintf("Cannot Return '%v' quantity", line.Quantity))
			}

			if line.Quantity > (salesOrderDetail.Quantity - salesOrderDetail.ReturnQuantity) {
				return errors.New(fmt.Sprintf("Cannot Return '%v' more than remain quantity '%v'", line.Quantity, salesOrderDetail.Quantity-salesOrderDetail.ReturnQuantity))
			}
		}
	} else {
		return errors.New("Please enter at least one line for return detail ")
	}

	return nil
}

func (req ReturnUpdateRequest) Validation(isByBase bool) (err error) {
	if err = req.CheckBaseHeader(isByBase); err != nil {
		return errors.New("Validate Base Header ERROR - " + err.Error())
	}

	if err = req.CheckBaseLine(isByBase); err != nil {
		return errors.New("Validate Base Line ERROR - " + err.Error())
	}

	return nil
}

func (req ReturnUpdateRequest) CheckBaseHeader(isByBase bool) (err error) {
	var returnObj Return
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if isByBase {
		if req.BaseDocNum != "" {
			if err = db.Database.Collection(setting.ReturnTable).FindOne(ctx, bson.M{"base_doc_num": req.BaseDocNum}).Decode(&returnObj); err != nil {
				return errors.New(fmt.Sprintf("Find Return Object with base doc num '%s' ERROR - %s", req.BaseDocNum, err.Error()))
			}
		} else {
			return errors.New("Base doc num is missing ")
		}
	}

	if req.DocStatus != "" {
		status, err := req.DocStatus.IsValid()
		if err != nil {
			return errors.New(fmt.Sprintf("Validate new Return status '%s' ERROR - %s", req.DocStatus, err.Error()))
		} else {
			req.DocStatus = status
		}
	}

	return nil
}

func (req ReturnUpdateRequest) CheckBaseLine(isByBase bool) (err error) {
	var returnDetail ReturnDetail

	col := db.Database.Collection(setting.ReturnDetailTable)
	if len(req.ReturnDetailUpdateRequest) > 0 && isByBase {
		for _, line := range req.ReturnDetailUpdateRequest {
			if req.BaseDocNum != "" && line.BaseDocLineNum != "" {
				filter := bson.M{"base_doc_num": req.BaseDocNum, "base_doc_line_num": line.BaseDocLineNum}
				if err = col.FindOne(context.Background(), filter).Decode(&returnDetail); err != nil {
					return errors.New(fmt.Sprintf("Find Return detail with base doc line num '%s' ERROR - %s", line.BaseDocLineNum, err.Error()))
				}
			}
			// Validate the status of line

			//if line.DocLineStatus == "" {
			//	return errors.New("Doc line status can't be empty ")
			//}

			//----------------------------
		}
	} else {
		req.ReturnDetailUpdateRequest = nil
	}

	return nil
}
