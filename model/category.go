package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Category struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef96d9c5954cbf2ea3cf7af"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5efeb035fdc0d9f28a6846c2"`
	CategoryId            string             `json:"category_id" bson:"category_id" example:"mollit"`
	Description           string             `json:"description" bson:"description" example:"nisi"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"anim"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"false"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	CreatedDate           time.Time          `json:"created_date" bson:"created_date" example:"2014-01-14T05:21:24 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efe9f3186e97039d0dc9ef2"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef96db2dc3410a231844080"`
	UpdatedDate           time.Time          `json:"updated_date" bson:"updated_date" example:"2016-06-30T01:42:01 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efe9fc7c85416319b9f067b"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef96db23c574ffde7644a58"`
}

type CategoryRequest struct {
	TenantKey             primitive.ObjectID      `json:"tenant_key" bson:"tenant_key" example:"5efeb035fdc0d9f28a6846c2"`
	CategoryId            string                  `json:"category_id" bson:"category_id" example:"mollit"`
	Description           string                  `json:"description" bson:"description" example:"nisi"`
	AddCategoryDetail     []CategoryDetailRequest `json:"category_detail" bson:"category_detail"`
	ObjectType            string                  `json:"object_type" bson:"object_type" example:"anim"`
	IsActive              bool                    `json:"is_active" bson:"is_active" example:"false"`
	CreatedBy             primitive.ObjectID      `json:"created_by" bson:"created_by" example:"5efe9f3186e97039d0dc9ef2"`
	CreatedApplicationKey primitive.ObjectID      `json:"created_application_key" bson:"created_application_key" example:"5ef96db2dc3410a231844080"`
	UpdatedBy             primitive.ObjectID      `json:"updated_by" bson:"updated_by" example:"5efe9fc7c85416319b9f067b"`
	UpdatedApplicationKey primitive.ObjectID      `json:"updated_application_key" bson:"updated_application_key" example:"5ef96db23c574ffde7644a58"`
}
