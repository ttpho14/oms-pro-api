package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Region struct {
	ID             primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef30898024ec5034669911e"`
	RegionId       string             `json:"region_id" bson:"region_id" example:"aliqua"`
	Description    string             `json:"description" bson:"description" example:"ut"`
	TenantKey      primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5ef308984a55510e9849d98a"`
	ContactPerson1 string             `json:"contact_person_1" bson:"contact_person_1" example:"Ruthie Bradshaw"`
	ContactEmail1  string             `json:"contact_email_1" bson:"contact_email_1" example:"ruthiebradshaw@geekko.com"`
	ContactNumber1 string             `json:"contact_number_1" bson:"contact_number_1" example:"+1 (901) 571-2674"`
	ContactPerson2 string             `json:"contact_person_2" bson:"contact_person_2" example:"Cohen Morris"`
	ContactEmail2  string             `json:"contact_email_2" bson:"contact_email_2" example:"cohenmorris@geekko.com"`
	ContactNumber2 string             `json:"contact_number_2" bson:"contact_number_2" example:"+1 (884) 588-3222"`
	Address1       string             `json:"address_1" bson:"address_1" example:"430 Dumont Avenue, Wacissa, Vermont, 8902"`
	Address2       string             `json:"address_2" bson:"address_2" example:"924 Cypress Avenue, Waterloo, Kansas, 8230"`
	Address3       string             `json:"address_3" bson:"address_3" example:"532 Middagh Street, Greenock, American Samoa, 3223"`

	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"true"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"veniam"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2019-08-07T07:16:21 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef30898cf00e3c6178a7ee0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef308983a001d1cca3dae7c"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2020-06-17T13:13:06.16Z"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef30898fc91158048878fdd"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef3089812312573b264b12d"`
}

type RegionRequest struct {
	RegionId       string             `json:"region_id" bson:"region_id" example:"aliqua"`
	Description    string             `json:"description" bson:"description" example:"ut"`
	TenantKey      primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5ef308984a55510e9849d98a"`
	ContactPerson1 string             `json:"contact_person_1" bson:"contact_person_1" example:"Ruthie Bradshaw"`
	ContactEmail1  string             `json:"contact_email_1" bson:"contact_email_1" example:"ruthiebradshaw@geekko.com"`
	ContactNumber1 string             `json:"contact_number_1" bson:"contact_number_1" example:"+1 (901) 571-2674"`
	ContactPerson2 string             `json:"contact_person_2" bson:"contact_person_2" example:"Cohen Morris"`
	ContactEmail2  string             `json:"contact_email_2" bson:"contact_email_2" example:"cohenmorris@geekko.com"`
	ContactNumber2 string             `json:"contact_number_2" bson:"contact_number_2" example:"+1 (884) 588-3222"`
	Address1       string             `json:"address_1" bson:"address_1" example:"430 Dumont Avenue, Wacissa, Vermont, 8902"`
	Address2       string             `json:"address_2" bson:"address_2" example:"924 Cypress Avenue, Waterloo, Kansas, 8230"`
	Address3       string             `json:"address_3" bson:"address_3" example:"532 Middagh Street, Greenock, American Samoa, 3223"`

	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"true"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"veniam"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef30898cf00e3c6178a7ee0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef308983a001d1cca3dae7c"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef30898fc91158048878fdd"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef3089812312573b264b12d"`
}
