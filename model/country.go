package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Country struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef30c010a6c988ebf981380"`
	CountryId             string             `json:"country_id" bson:"country_id" example:"in"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5ef30c019754f975abe2b21a"`
	RegionKey             primitive.ObjectID `json:"region_key" bson:"region_key" example:"5ef30c01a4d84c899bacf52f"`
	Description           string             `json:"description" bson:"description" example:"aute"`
	ContactPerson1        string             `json:"contact_person_1" bson:"contact_person_1" example:"Melva Booth"`
	ContactEmail1         string             `json:"contact_email_1" bson:"contact_email_1" example:"melvabooth@geekko.com"`
	ContactNumber1        string             `json:"contact_number_1" bson:"contact_number_1" example:"+1 (894) 585-3172"`
	ContactPerson2        string             `json:"contact_person_2" bson:"contact_person_2" example:"Olson Reese"`
	ContactEmail2         string             `json:"contact_email_2" bson:"contact_email_2" example:"olsonreese@geekko.com"`
	ContactNumber2        string             `json:"contact_number_2" bson:"contact_number_2" example:"+1 (865) 502-3998"`
	Address1              string             `json:"address_1" bson:"address_1" example:"101 Jamaica Avenue, Monument, Nebraska, 943"`
	Address2              string             `json:"address_2" bson:"address_2" example:"761 Ridge Boulevard, Crenshaw, New Hampshire, 9767"`
	Address3              string             `json:"address_3" bson:"address_3" example:"502 Dupont Street, Axis, New York, 4283"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"true"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"culpa"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2020-01-10T06:21:06 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef30c012b6bb12311c7159e"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef30c013654b24bba229755"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2020-01-15T06:21:06 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef30c0114ef9f689a99f045"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef30c0128e9e3a679c3e614"`
}

type CountryRequest struct {
	CountryId             string             `json:"country_id" bson:"country_id" example:"in"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5ef30c019754f975abe2b21a"`
	RegionKey             primitive.ObjectID `json:"region_key" bson:"region_key" example:"5ef30c01a4d84c899bacf52f"`
	Description           string             `json:"description" bson:"description" example:"aute"`
	ContactPerson1        string             `json:"contact_person_1" bson:"contact_person_1" example:"Melva Booth"`
	ContactEmail1         string             `json:"contact_email_1" bson:"contact_email_1" example:"melvabooth@geekko.com"`
	ContactNumber1        string             `json:"contact_number_1" bson:"contact_number_1" example:"+1 (894) 585-3172"`
	ContactPerson2        string             `json:"contact_person_2" bson:"contact_person_2" example:"Olson Reese"`
	ContactEmail2         string             `json:"contact_email_2" bson:"contact_email_2" example:"olsonreese@geekko.com"`
	ContactNumber2        string             `json:"contact_number_2" bson:"contact_number_2" example:"+1 (865) 502-3998"`
	Address1              string             `json:"address_1" bson:"address_1" example:"101 Jamaica Avenue, Monument, Nebraska, 943"`
	Address2              string             `json:"address_2" bson:"address_2" example:"761 Ridge Boulevard, Crenshaw, New Hampshire, 9767"`
	Address3              string             `json:"address_3" bson:"address_3" example:"502 Dupont Street, Axis, New York, 4283"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"true"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"culpa"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef30c012b6bb12311c7159e"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef30c013654b24bba229755"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef30c0114ef9f689a99f045"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef30c0128e9e3a679c3e614"`
}
