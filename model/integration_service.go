package model

import "./enum"

type IntegrationService struct {
	ServiceName enum.IntegrationService `json:"service_name" bson:"service_name" example:"OMS.Pro.NovoMind"`
	Description string `json:"description" bson:"description" example:""`
	PID         string `json:"pid" bson:"pid" example:"12521"`
	Status      string `json:"status" bson:"status" example:"active"`
	Memory      string `json:"memory" bson:"memory" example:"0.7"`
	CPU         string `json:"cpu" bson:"cpu" example:"0.5"`
	//ObjectType     string             `json:"object_type" bson:"object_type" example:"false"`
}

type IntegrationServiceRequest struct {
	ServiceName enum.IntegrationService `json:"service_name" bson:"service_name" example:"OMS.Pro.NovoMind"`
	Command     string `json:"command" bson:"command" example:"start,stop,restart"`
}
