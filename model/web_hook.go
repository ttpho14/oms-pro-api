package model

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"time"

	"../database"
	"../model/enum"
	sHelper "../service/helper"
	"../setting"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//--------------------------------------------- 1. Milestone ---------------------------------------------------------------
type Milestone struct {
	RequestID string           `json:"requestid" bson:"request_id" example:"incididunt"`
	Signature string           `json:"signature" bson:"signature" example:"ipsum"`
	Orders    []MilestoneOrder `json:"orders" bson:"orders"`
}

type MilestoneOrder struct {
	Action         string                   `json:"action" bson:"action" example:"UPDATE"`
	ActionRemarks  string                   `json:"action_remarks" bson:"action_remarks" example:"UPDATE"`
	DeliveryOrders []MilestoneDeliveryOrder `json:"deliveryorders" bson:"delivery_orders"`
}

type MilestoneDeliveryOrder struct {
	No             string `json:"no" bson:"no" example:"D1809240025"`
	TrackingNumber string `json:"trackingNumber" bson:"trackingNumber" example:"DEL11201821546"`
	TrackingStatus string `json:"trackingStatus" bson:"trackingStatus" example:"PICKING"`
}

//--------------------- Request -----------------------
type MilestoneRequest struct {
	RequestID string                  `json:"requestid" bson:"request_id" example:"incididunt"`
	Signature string                  `json:"signature" bson:"signature" example:"ipsum"`
	Orders    []MilestoneOrderRequest `json:"orders" bson:"orders"`
}

type MilestoneOrderRequest struct {
	Action         string                          `json:"action" bson:"action" example:"UPDATE"`
	ActionRemarks  string                          `json:"action_remarks" bson:"action_remarks" example:"UPDATE"`
	DeliveryOrders []MilestoneDeliveryOrderRequest `json:"deliveryorders" bson:"delivery_orders"`
}

type MilestoneDeliveryOrderRequest struct {
	No             string `json:"no" bson:"no" example:"D1809240025"`
	TrackingNumber string `json:"trackingNumber" bson:"trackingNumber" example:"DEL11201821546"`
	TrackingStatus string `json:"trackingStatus" bson:"trackingStatus" example:"PICKING"`
}

//--------------------------------------------- 2. Order Pack ---------------------------------------------------------------
type OrderPack struct {
	ID              primitive.ObjectID `json:"_id" bson:"_id" example:"incididunt"`
	AccountCode     string             `json:"account_code" bson:"account_code" example:"incididunt"`
	OwnerCode       string             `json:"owner_code" bson:"owner_code" example:"ut"`
	OrderType       string             `json:"order_type" bson:"order_type" example:"quis"`
	OrderNo         string             `json:"order_no" bson:"order_no" example:"dolor"`
	TransactionType string             `json:"transaction_type" bson:"transaction_type" example:"irure"`
	TransactionNo   string             `json:"transaction_no" bson:"transaction_no" example:"irure"`
	TotalWeight     float64            `json:"total_weight" bson:"total_weight" example:"474.56"`
	TotalVolume     float64            `json:"total_volume" bson:"total_volume" example:"87.05"`
	TotalPackages   float64            `json:"total_packages" bson:"total_packages" example:"429.85"`
	OrderItems      []*OrderItem       `json:"order_items" bson:"order_items,omitempty"`
	PackingPackages []*PackingPackage  `json:"packing_packages" bson:"packing_packages,omitempty"`
}

type OrderItem struct {
	LineNo       string  `json:"line_no" bson:"line_no" example:"qui"`
	Status       string  `json:"status" bson:"status" example:"dolor"`
	ItemRemark   string  `json:"item_remark" bson:"item_remark" example:"dolor"`
	ProductCode  string  `json:"product_code" bson:"product_code" example:"cillum"`
	OrderUom     string  `json:"order_uom" bson:"order_uom" example:"proident"`
	Weight       float64 `json:"weight" bson:"weight" example:"205.33"`
	Volume       float64 `json:"volume" bson:"volume" example:"46.9"`
	BatchNo      string  `json:"batch_no" bson:"batch_no" example:"esse"`
	VarietyCode  string  `json:"variety_code" bson:"variety_code" example:"ea"`
	SupplierCode string  `json:"supplier_code" bson:"supplier_code" example:"occaecat"`
	OrderQty     float64 `json:"order_qty" bson:"order_qty" example:"333"`
	ItemValue    float64 `json:"item_value" bson:"item_value" example:"169.14"`
}

type PackingPackage struct {
	PackageNo     string  `json:"package_no" bson:"package_no" example:"qui"`
	PackageType   string  `json:"package_type" bson:"package_type" example:"dolor"`
	CommodityType string  `json:"commodity_type" bson:"commodity_type" example:"cillum"`
	Length        float64 `json:"length" bson:"length" example:"330.03"`
	Width         float64 `json:"width" bson:"width" example:"200.7"`
	Height        float64 `json:"height" bson:"height" example:"30.04"`
	Weight        float64 `json:"weight" bson:"weight" example:"285.77"`
	Value         float64 `json:"value" bson:"value" example:"455.2"`
	Instruction   string  `json:"instruction" bson:"instruction" example:"laboris"`
}

//-------------------- Request -----------------------
type OrderPackRequest struct {
	AccountCode     string                   `json:"accountCode,omitempty" bson:"account_code,omitempty" example:"incididunt"`
	OwnerCode       string                   `json:"ownerCode,omitempty" bson:"owner_code,omitempty" example:"ut"`
	OrderType       string                   `json:"orderType,omitempty" bson:"order_type,omitempty" example:"quis"`
	OrderNo         string                   `json:"orderNo,omitempty" bson:"order_no,omitempty" example:"dolor"`
	TransactionType string                   `json:"transactionType,omitempty" bson:"transaction_type,omitempty" example:"irure"`
	TransactionNo   string                   `json:"transactionNo,omitempty" bson:"transaction_no,omitempty" example:"irure"`
	TotalWeight     float64                  `json:"totalWeight,omitempty" bson:"total_weight,omitempty" example:"474.56"`
	TotalVolume     float64                  `json:"totalVolume,omitempty" bson:"total_volume,omitempty" example:"87.05"`
	TotalPackages   float64                  `json:"totalPackages,omitempty" bson:"total_packages,omitempty" example:"429.85"`
	OrderItems      []*OrderItemRequest      `json:"orderItems,omitempty" bson:"order_items,omitempty"`
	PackingPackages []*PackingPackageRequest `json:"packingPackages,omitempty" bson:"packing_packages,omitempty"`
}

type OrderItemRequest struct {
	LineNo       string  `json:"lineNo,omitempty" bson:"line_no,omitempty" example:"qui"`
	Status       string  `json:"status,omitempty" bson:"status,omitempty" example:"dolor"`
	ItemRemark   string  `json:"itemRemark" bson:"item_remark" example:"dolor"`
	ProductCode  string  `json:"productCode,omitempty" bson:"product_code,omitempty" example:"cillum"`
	OrderUom     string  `json:"orderUom,omitempty" bson:"order_uom,omitempty" example:"proident"`
	Weight       float64 `json:"weight,omitempty" bson:"weight,omitempty" example:"205.33"`
	Volume       float64 `json:"volume,omitempty" bson:"volume,omitempty" example:"46.9"`
	BatchNo      string  `json:"batchNo,omitempty" bson:"batch_no,omitempty" example:"esse"`
	VarietyCode  string  `json:"varietyCode,omitempty" bson:"variety_code,omitempty" example:"ea"`
	SupplierCode string  `json:"supplierCode,omitempty" bson:"supplier_code,omitempty" example:"occaecat"`
	OrderQty     float64 `json:"orderQty,omitempty" bson:"order_qty,omitempty" example:"333"`
	ItemValue    float64 `json:"itemValue,omitempty" bson:"item_value,omitempty" example:"169.14"`
}

type PackingPackageRequest struct {
	PackageNo     string  `json:"packageNo,omitempty" bson:"package_no,omitempty" example:"qui"`
	PackageType   string  `json:"packageType,omitempty" bson:"package_type,omitempty" example:"dolor"`
	CommodityType string  `json:"commodityType,omitempty" bson:"commodity_type,omitempty" example:"cillum"`
	Length        float64 `json:"length,omitempty" bson:"length,omitempty" example:"330.03"`
	Width         float64 `json:"width,omitempty" bson:"width,omitempty" example:"200.7"`
	Height        float64 `json:"height,omitempty" bson:"height,omitempty" example:"30.04"`
	Weight        float64 `json:"weight,omitempty" bson:"weight,omitempty" example:"285.77"`
	Value         float64 `json:"value,omitempty" bson:"value,omitempty" example:"455.2"`
	Instruction   string  `json:"instruction,omitempty" bson:"instruction,omitempty" example:"laboris"`
}

func (oReq OrderPackRequest) Validation() (OrderPackRequest, error) {
	oReq, err := oReq.CheckHeader()
	if err != nil {
		return oReq, errors.New("Check Order No ERROR - " + err.Error())
	}

	oReq, err = oReq.CheckDetails()
	if err != nil {
		return oReq, errors.New("Check Order Items ERROR - " + err.Error())
	}
	return oReq, nil
}

func (oReq OrderPackRequest) CheckHeader() (OrderPackRequest, error) {
	var (
		salesOrder SalesOrder
		returnObj  Return
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{"doc_num": sHelper.RegexStringID(oReq.OrderNo)}
	if len(oReq.PackingPackages) > 0 {
		if err := database.Database.Collection(setting.SalesOrderTable).FindOne(ctx, filter).Decode(&salesOrder); err != nil {
			return oReq, errors.New(fmt.Sprintf("Find Sales Order with Order Pack Order No '%s' ERROR - %s", oReq.OrderNo, err.Error()))
		}
	}
	if len(oReq.OrderItems) > 0 {
		if err := database.Database.Collection(setting.ReturnTable).FindOne(ctx, filter).Decode(&returnObj); err != nil {
			return oReq, errors.New(fmt.Sprintf("Find Return Order with Order Pack Order No '%s' ERROR - %s", oReq.OrderNo, err.Error()))
		}
	}

	return oReq, nil
}

func (oReq OrderPackRequest) CheckDetails() (OrderPackRequest, error) {
	var (
		returnDetail ReturnDetail
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	for index, _ := range oReq.OrderItems {
		filter := bson.M{
			"doc_num":           sHelper.RegexStringID(oReq.OrderNo),
			"base_doc_line_num": sHelper.RegexStringID(oReq.OrderItems[index].LineNo),
			"product_code":      sHelper.RegexStringID(oReq.OrderItems[index].ProductCode),
		}
		switch strings.ToUpper(oReq.OrderItems[index].Status) {
		case "ACCEPT":
			oReq.OrderItems[index].Status = string(enum.ReturnDetailStatusAccepted)
		case "REJECT":
			oReq.OrderItems[index].Status = string(enum.ReturnDetailStatusRejected)
		}

		returnStatus, err := enum.ReturnDetailStatus(oReq.OrderItems[index].Status).IsValid()
		if err != nil {
			return oReq, errors.New("Check Return Status ERROR - " + err.Error())
		}

		if sHelper.IsItemExistsInArray([]enum.ReturnDetailStatus{enum.ReturnDetailStatusAccepted, enum.ReturnDetailStatusRejected}, returnStatus) {
			if err := database.Database.Collection(setting.ReturnDetailTable).FindOne(ctx, filter).Decode(&returnDetail); err == nil {
				oReq.OrderItems[index].LineNo = returnDetail.DocLineNum
			} else {
				return oReq, errors.New(fmt.Sprintf("Find Return Order Detail with Order Pack Order No [%s] and Base Line No [%s] and Product Code [%s]  ERROR - %s", oReq.OrderNo, oReq.OrderItems[index].LineNo, oReq.OrderItems[index].ProductCode, err.Error()))
			}
		}
		//else {
		//	if err := database.Database.Collection(setting.SalesOrderDetailTable).FindOne(ctx, filter).Decode(&salesOrderDetail); err != nil {
		//		return errors.New(fmt.Sprintf("Find Sales Order Detail with Order Pack Order No '%s' and Line No '%s' ERROR - %s", oReq.OrderNo, oReq.OrderItems[index].LineNo, err.Error()))
		//	}
		//}
	}

	return oReq, nil

}

//--------------------------------------------- 3. Update Sales Order Label URL ---------------------------------------------------------------
type RMALabelUrl struct {
	OrderNo       string `json:"orderNo,omitempty" bson:"order_no,omitempty" example:"ORDER0000001" validate:"required"`
	TransactionNo string `json:"transactionNo,omitempty" bson:"transaction_no,omitempty" example:"YCHSG000000001" validate:"required"`
	LabelUrl      string `json:"labelUrl,omitempty" bson:"label_url,omitempty" example:"https://oms.ych.com/labelPrint?trackingNumber=YCHSG000000001&apiKey=ABCDEF" validate:"required"`
}

type RMALabelUrlRequest struct {
	OrderNo       string `json:"orderNo,omitempty" bson:"order_no,omitempty" example:"ORDER0000001" validate:"required"`
	TransactionNo string `json:"transactionNo,omitempty" bson:"transaction_no,omitempty" example:"YCHSG000000001" validate:"required"`
	LabelUrl      string `json:"labelUrl,omitempty" bson:"label_url,omitempty" example:"https://oms.ych.com/labelPrint?trackingNumber=YCHSG000000001&apiKey=ABCDEF" validate:"required"`
}

//--------------------------------------------- 4. Warehouse Cancel Order ---------------------------------------------------------------
type WarehouseCancelOrder struct {
	OrderNo       string                     `json:"orderNo" bson:"orderNo" example:"ORDER0000001"`
	TransactionNo string                     `json:"transactionNo" bson:"transactionNo" example:"YCHSG000000001"`
	Remarks       string                     `json:"remarks" bson:"remarks" example:"Warehouse Shortage"`
	OrderItems    []WarehouseCancelOrderItem `json:"orderItems" bson:"order_items"`
}
type WarehouseCancelOrderItem struct {
	LineNo      string  `json:"lineNo" bson:"line_no" example:"001"`
	ShortageQty float64 `json:"shortageQty" bson:"shortage_qty" example:"10"`
}

//-------------- Request --------------
type WarehouseCancelOrderRequest struct {
	OrderNo       string                            `json:"orderNo" bson:"orderNo" example:"ORDER0000001"`
	TransactionNo string                            `json:"transactionNo" bson:"transactionNo" example:"YCHSG000000001"`
	Remarks       string                            `json:"remarks" bson:"remarks" example:"Warehouse Shortage"`
	OrderItems    []WarehouseCancelOrderItemRequest `json:"orderItems" bson:"order_items"`
}
type WarehouseCancelOrderItemRequest struct {
	LineNo      string  `json:"lineNo" bson:"order_no" example:"001"`
	ShortageQty float64 `json:"shortageQty" bson:"shortage_qty" example:"10"`
}

//---------------- Function ----------------
func (m MilestoneRequest) Validation() error {
	if err := m.IsValidStatus(); err != nil {
		return errors.New("Validate Sales Order Status ERROR - " + err.Error())
	}
	return nil
}

func (m MilestoneRequest) IsValidStatus() error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var sales SalesOrder

	for _, order := range m.Orders {
		for _, deliOrder := range order.DeliveryOrders {
			var (
				orderStatus  = enum.OrderStatus(deliOrder.TrackingStatus).ToCorrectCase()
				returnStatus = enum.ReturnStatus(deliOrder.TrackingStatus).ToCorrectCase()
			)
			if returnStatus == enum.ReturnStatusReceived {
				if err := database.Database.Collection(setting.ReturnTable).FindOne(ctx, bson.M{"doc_num": deliOrder.No}).Decode(&sales); err != nil {
					return errors.New(fmt.Sprintf("Find Return order doc num '%s' ERROR - %s", deliOrder.No, err.Error()))
				}
			} else if sHelper.IsItemExistsInArray([]enum.OrderStatus{enum.OrderStatusProcessing, enum.OrderStatusPacked, enum.OrderStatusHandover, enum.OrderStatusDelivered, enum.OrderStatusShipped}, orderStatus) {
				if err := database.Database.Collection(setting.SalesOrderTable).FindOne(ctx, bson.M{"doc_num": deliOrder.No}).Decode(&sales); err != nil {
					return errors.New(fmt.Sprintf("Find sales order doc num '%s' ERROR - %s", deliOrder.No, err.Error()))
				}
				if strings.ToUpper(sales.SalesChannelId) != setting.SalesChannelPumaCom && sHelper.IsItemExistsInArray([]enum.OrderStatus{enum.OrderStatusCancelled, enum.OrderStatusShipped, enum.OrderStatusDelivered, enum.OrderStatusReturned}, sales.OrderStatus) {
					return errors.New(fmt.Sprintf("Can't change current order status '%s' to new order status", sales.OrderStatus))
				}
			} else {
				return errors.New("Tracking Status is invalid ")
			}
		}
	}

	return nil
}
