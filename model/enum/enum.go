package enum

import (
	"gopkg.in/oauth2.v4/errors"
	"strings"
)

//func IsValidStatus(s []Status) (err error) {
//	for _, v := range s {
//		if err = v.IsValid(); err != nil {
//			return err
//		}
//	}
//	return nil
//}



//-------------- ORDER STATUS ----------------------
type OrderStatus string

const (
	OrderStatusAll        OrderStatus = "ALL"
	OrderStatusNew        OrderStatus = "NEW"
	OrderStatusOnHold     OrderStatus = "ON_HOLD"
	OrderStatusCancelled  OrderStatus = "CANCELLED"
	OrderStatusProcessing OrderStatus = "PROCESSING"
	OrderStatusPicked     OrderStatus = "PICKED"
	OrderStatusPacked     OrderStatus = "PACKED"
	OrderStatusShipped    OrderStatus = "SHIPPED"
	OrderStatusDelivered  OrderStatus = "DELIVERED"
	OrderStatusReturned   OrderStatus = "RETURNED"
	OrderStatusHandover   OrderStatus = "HANDOVER"
	OrderStatusClosed     OrderStatus = "CLOSED"
)

func (s OrderStatus) IsValid() (OrderStatus, error) {
	newS := s.ToCorrectCase()
	switch newS {
	case OrderStatusNew,
		OrderStatusOnHold,
		OrderStatusCancelled,
		OrderStatusProcessing,
		OrderStatusPicked,
		OrderStatusPacked,
		OrderStatusShipped,
		OrderStatusDelivered,
		OrderStatusReturned,
		OrderStatusHandover,
		OrderStatusClosed:
		return newS, nil
	}
	return "", errors.New("Invalid Order Status")
}
func (s OrderStatus) ToCorrectCase() OrderStatus {
	return OrderStatus(strings.ToUpper(strings.ReplaceAll(strings.Trim(string(s), " "), " ", "_")))
}

func (s OrderStatus) ToUpper() OrderStatus {
	return OrderStatus(strings.ToUpper(string(s)))
}

//-------------- CANCEL ORDER STATUS ----------------------
type CancelOrderStatus string

const (
	CancelOrderStatusAll       CancelOrderStatus = "ALL"
	CancelOrderStatusCancelled CancelOrderStatus = "CANCELLED"
	CancelOrderStatusRefunded  CancelOrderStatus = "REFUNDED"
	CancelOrderStatusClosed    CancelOrderStatus = "CLOSED"
)

func (s CancelOrderStatus) IsValid() (CancelOrderStatus, error) {
	newS := s.ToCorrectCase()
	switch newS {
	case CancelOrderStatusCancelled,
		CancelOrderStatusRefunded,
		CancelOrderStatusClosed:
		return newS, nil
	}
	return "", errors.New("Invalid Cancel Order Status")
}
func (s CancelOrderStatus) ToCorrectCase() CancelOrderStatus {
	return CancelOrderStatus(strings.ToUpper(strings.ReplaceAll(strings.Trim(string(s), " "), " ", "_")))
}

func (s CancelOrderStatus) ToUpper() CancelOrderStatus {
	return CancelOrderStatus(strings.ToUpper(string(s)))
}

//-------------- CANCEL ORDER STATUS ----------------------
type CancelOrderLineStatus string

const (
	CancelOrderStatusLineCancelled CancelOrderLineStatus = "CANCELLED"
	CancelOrderStatusLineRefunded  CancelOrderLineStatus = "REFUNDED"
	CancelOrderStatusLineClosed    CancelOrderLineStatus = "CLOSED"
)

func (s CancelOrderLineStatus) IsValid() (CancelOrderLineStatus, error) {
	newS := s.ToCorrectCase()
	switch newS {
	case CancelOrderStatusLineCancelled,
		CancelOrderStatusLineRefunded,
		CancelOrderStatusLineClosed:
		return newS, nil
	}
	return "", errors.New("Invalid Cancel Order Status")
}
func (s CancelOrderLineStatus) ToCorrectCase() CancelOrderLineStatus {
	return CancelOrderLineStatus(strings.ToUpper(strings.ReplaceAll(strings.Trim(string(s), " "), " ", "_")))
}

func (s CancelOrderLineStatus) ToUpper() CancelOrderLineStatus {
	return CancelOrderLineStatus(strings.ToUpper(string(s)))
}

//-------------------- ORDER LINE STATUS ---------------------
type OrderLineStatus string

const (
	OrderLineStatusNew         OrderLineStatus = "NEW"
	OrderLineStatusOnHold      OrderLineStatus = "ON_HOLD"
	OrderLineStatusCancelled   OrderLineStatus = "CANCELLED"
	OrderLineStatusPicked      OrderLineStatus = "PICKED"
	OrderLineStatusPacked      OrderLineStatus = "PACKED"
	OrderLineStatusProcessing  OrderLineStatus = "PROCESSING"
	OrderLineStatusReadyToShip OrderLineStatus = "READY_TO_SHIP"
	OrderLineStatusShipped     OrderLineStatus = "SHIPPED"
	OrderLineStatusInTransit   OrderLineStatus = "IN_TRANSIT"
	OrderLineStatusHandover    OrderLineStatus = "HANDOVER"
	OrderLineStatusDelivered   OrderLineStatus = "DELIVERED"
	OrderLineStatusReturned    OrderLineStatus = "RETURNED"
	OrderLineStatusReceived    OrderLineStatus = "RECEIVED"
	OrderLineStatusRejected    OrderLineStatus = "REJECTED"
	OrderLineStatusAccepted    OrderLineStatus = "ACCEPTED"
	OrderLineStatusComplete    OrderLineStatus = "COMPLETE"
	OrderLineStatusRefunded    OrderLineStatus = "REFUNDED"
	OrderLineStatusError       OrderLineStatus = "ERROR"
)

func (s OrderLineStatus) IsValid() (OrderLineStatus, error) {
	newOLs := s.ToCorrectCase()
	switch newOLs {
	case OrderLineStatusNew,
		OrderLineStatusOnHold,
		OrderLineStatusCancelled,
		OrderLineStatusPicked,
		OrderLineStatusPacked,
		OrderLineStatusReadyToShip,
		OrderLineStatusShipped,
		OrderLineStatusProcessing,
		OrderLineStatusInTransit,
		OrderLineStatusHandover,
		OrderLineStatusDelivered,
		OrderLineStatusReturned,
		OrderLineStatusReceived,
		OrderLineStatusRejected,
		OrderLineStatusAccepted,
		OrderLineStatusComplete,
		OrderLineStatusRefunded,
		OrderLineStatusError:
		return newOLs, nil
	}
	return "", errors.New("Invalid Order Line Status")
}

func (s OrderLineStatus) ToCorrectCase() OrderLineStatus {
	return OrderLineStatus(strings.ToUpper(strings.ReplaceAll(strings.Trim(string(s), " "), " ", "_")))
}
func (s OrderLineStatus) ToUpper() OrderLineStatus {
	return OrderLineStatus(strings.ToUpper(string(s)))
}

//---------------- SHIPPING STATUS -----------------
type ShippingStatus string

//Shipping Status Constant
const (
	ShippingStatusReadyToShip ShippingStatus = "READY_TO_SHIP"
	ShippingStatusNotShipped  ShippingStatus = "NOT_SHIPPED"
	ShippingStatusShipped     ShippingStatus = "SHIPPED"
	ShippingStatusDelivered   ShippingStatus = "DELIVERED"
)

func (s ShippingStatus) IsValid() (ShippingStatus, error) {
	newSSs := s.ToCorrectCase()
	switch newSSs {
	case ShippingStatusReadyToShip,
		ShippingStatusShipped,
		ShippingStatusNotShipped,
		ShippingStatusDelivered:
		return newSSs, nil
	}
	return "", errors.New("Invalid Shipping Status")
}

func (s ShippingStatus) ToCorrectCase() ShippingStatus {
	return ShippingStatus(strings.ToUpper(strings.ReplaceAll(strings.Trim(string(s), " "), " ", "_")))
}
func (s ShippingStatus) ToUpper() ShippingStatus {
	return ShippingStatus(strings.ToUpper(string(s)))
}

//---------------- PAYMENT STATUS -----------------
type PaymentStatus string

//Payment Status Constant
const (
	PaymentStatusPending        PaymentStatus = "PENDING"
	PaymentStatusAuthorized     PaymentStatus = "AUTHORIZED"
	PaymentStatusCaptureSuccess PaymentStatus = "CAPTURE_SUCCESS"
	PaymentStatusReversed       PaymentStatus = "REVERSED"
	PaymentStatusRefunded       PaymentStatus = "REFUNDED"
)

func (s PaymentStatus) IsValid() (PaymentStatus, error) {
	newPs := s.ToCorrectCase()
	switch newPs {
	case PaymentStatusPending,
		PaymentStatusAuthorized,
		PaymentStatusCaptureSuccess,
		PaymentStatusReversed,
		PaymentStatusRefunded:
		return newPs, nil
	}
	return "", errors.New("Invalid Payment Status")
}

func (s PaymentStatus) ToCorrectCase() PaymentStatus {
	return PaymentStatus(strings.ToUpper(strings.ReplaceAll(strings.Trim(string(s), " "), " ", "_")))
}

func (s PaymentStatus) ToUpper() PaymentStatus {
	return PaymentStatus(strings.ToUpper(string(s)))
}

//-------------- RETURN STATUS ---------------
type ReturnStatus string

const (
	ReturnStatusAll      ReturnStatus = "ALL"
	ReturnStatusRejected ReturnStatus = "REJECTED"
	ReturnStatusReturned ReturnStatus = "RETURNED"
	ReturnStatusRefunded ReturnStatus = "REFUNDED"
	ReturnStatusAccepted ReturnStatus = "ACCEPTED"
	ReturnStatusReceived ReturnStatus = "RECEIVED"
	ReturnStatusClosed   ReturnStatus = "CLOSED"
)

func (s ReturnStatus) IsValid() (ReturnStatus, error) {
	newRs := s.ToCorrectCase()
	switch newRs {
	case ReturnStatusAll,
		ReturnStatusRejected,
		ReturnStatusReturned,
		ReturnStatusRefunded,
		ReturnStatusAccepted,
		ReturnStatusReceived,
		ReturnStatusClosed:
		return newRs, nil
	}
	return "", errors.New("Invalid Return Status")
}
func (s ReturnStatus) ToCorrectCase() ReturnStatus {
	return ReturnStatus(strings.ToUpper(strings.ReplaceAll(strings.Trim(string(s), " "), " ", "_")))
}

func (s ReturnStatus) ToUpper() ReturnStatus {
	return ReturnStatus(strings.ToUpper(string(s)))
}

//-------------- RETURN LINE STATUS ---------------
type ReturnDetailStatus string

const (
	ReturnDetailStatusReceived ReturnDetailStatus = "RECEIVED"
	ReturnDetailStatusReturned ReturnDetailStatus = "RETURNED"
	ReturnDetailStatusAccepted ReturnDetailStatus = "ACCEPTED"
	ReturnDetailStatusRejected ReturnDetailStatus = "REJECTED"
)

func (rl ReturnDetailStatus) IsValid() (ReturnDetailStatus, error) {
	newRs := rl.ToCorrectCase()
	switch newRs {
	case ReturnDetailStatusReceived,
		ReturnDetailStatusReturned,
		ReturnDetailStatusAccepted,
		ReturnDetailStatusRejected:
		return newRs, nil
	}
	return "", errors.New("Invalid Return Status")
}
func (rl ReturnDetailStatus) ToCorrectCase() ReturnDetailStatus {
	return ReturnDetailStatus(strings.ToUpper(strings.ReplaceAll(strings.Trim(string(rl), " "), " ", "_")))
}

func (rl ReturnDetailStatus) ToUpper() ReturnDetailStatus {
	return ReturnDetailStatus(strings.ToUpper(string(rl)))
}

//--------------- APPLICATION ID ------------------
const (
	AppIdOMS         string = "OMS"
	AppIdSAP         string = "SAP"
	AppIdSIA         string = "SIA"
	AppIdSFCC        string = "SFCC"
	AppIdCyberSource string = "CYBERSOURCE"
	AppIdYCH         string = "YCH"
	AppIdEmail       string = "EMAIL"
)

//--------------- DASHBOARD PARAMETERS ----------------
type DashboardPeriod string

const (
	DashboardPeriodToday     DashboardPeriod = "TODAY"
	DashboardPeriodYesterday DashboardPeriod = "YESTERDAY"
	DashboardPeriodThisWeek  DashboardPeriod = "THIS_WEEK"
	DashboardPeriodLastWeek  DashboardPeriod = "LAST_WEEK"
	DashboardPeriodThisMonth DashboardPeriod = "THIS_MONTH"
	DashboardPeriodLastMonth DashboardPeriod = "LAST_MONTH"
	DashboardPeriodThisYear  DashboardPeriod = "THIS_YEAR"
	DashboardPeriodLastYear  DashboardPeriod = "LAST_YEAR"
)

func (s DashboardPeriod) IsValid() (DashboardPeriod, error) {
	newDp := s.ToCorrectCase()
	switch newDp {
	case DashboardPeriodToday,
		DashboardPeriodYesterday,
		DashboardPeriodThisWeek,
		DashboardPeriodLastWeek,
		DashboardPeriodThisMonth,
		DashboardPeriodLastMonth,
		DashboardPeriodThisYear,
		DashboardPeriodLastYear:
		return newDp, nil
	}
	return "", errors.New("Invalid Dashboard Period")
}
func (s DashboardPeriod) ToCorrectCase() DashboardPeriod {
	return DashboardPeriod(strings.ToUpper(strings.ReplaceAll(strings.Trim(string(s), " "), " ", "_")))
}

//--------------- Security Access Right --------------
type AccessRight string

const (
	AccessRightNo       AccessRight = "NO"
	AccessRightReadOnly AccessRight = "READ_ONLY"
	AccessRightFull     AccessRight = "FULL"
)

func (a AccessRight) IsValid() (AccessRight, error) {
	newDp := a.ToCorrectCase()
	switch newDp {
	case AccessRightNo,
		AccessRightReadOnly,
		AccessRightFull:
		return newDp, nil
	}
	return "", errors.New("Invalid Access Right ")
}
func (a AccessRight) ToCorrectCase() AccessRight {
	return AccessRight(strings.ToUpper(strings.ReplaceAll(strings.Trim(string(a), " "), " ", "_")))
}

//------------- Service Status ---------------
type IntegrationService string

const (
	IntegrationServiceSIA            IntegrationService = "OMS.Pro.SIA"
	IntegrationServiceEmail          IntegrationService = "OMS.Pro.Email"
	IntegrationServiceYCHOrder       IntegrationService = "OMS.Pro.YCH.Order"
	IntegrationServiceYCHMY          IntegrationService = "OMS.Pro.YCH.MY"
	IntegrationServiceYCHSG          IntegrationService = "OMS.Pro.YCH.SG"
	IntegrationServiceSAPSG          IntegrationService = "OMS.Pro.SAP.SG"
	IntegrationServiceSAPMY          IntegrationService = "OMS.Pro.SAP.MY"
	IntegrationServiceNovoMind       IntegrationService = "OMS.Pro.NovoMind"
	IntegrationServiceSFCC           IntegrationService = "OMS.Pro.SFCC"
	IntegrationServiceCyberSource    IntegrationService = "OMS.Pro.CyberSource"
	IntegrationServiceAPI            IntegrationService = "OMS.Pro.API"
	IntegrationServiceAPIIntegration IntegrationService = "OMS.Pro.API.Integration"
	IntegrationServiceMongo          IntegrationService = "mongod"
	IntegrationServiceWebApp         IntegrationService = "nginx"
)

func (a IntegrationService) IsValid() (IntegrationService, error) {
	newDp := a.ToCorrectCase()
	switch newDp {
	case IntegrationServiceNovoMind,
		IntegrationServiceSIA,
		IntegrationServiceSAPSG,
		IntegrationServiceSAPMY,
		IntegrationServiceYCHSG,
		IntegrationServiceYCHMY,
		IntegrationServiceYCHOrder,
		IntegrationServiceAPIIntegration,
		IntegrationServiceAPI,
		IntegrationServiceEmail,
		IntegrationServiceSFCC,
		IntegrationServiceCyberSource,
		IntegrationServiceWebApp,
		IntegrationServiceMongo:
		return newDp, nil
	}
	return "", errors.New("Invalid Access Right ")
}
func (a IntegrationService) ToCorrectCase() IntegrationService {
	return IntegrationService(strings.ToUpper(strings.ReplaceAll(strings.Trim(string(a), " "), " ", "_")))
}

