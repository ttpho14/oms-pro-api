package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type PriceListDetail struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef32362ed162a053bc2abc7"`
	PriceListKey          primitive.ObjectID `json:"price_list_key" bson:"price_list_key" example:"5ef3236241f855dfa62623bf"`
	ProductKey            primitive.ObjectID `json:"product_key" bson:"product_key" example:"5ef32362e63434789ba57c48"`
	BasePrice             float64            `json:"base_price" bson:"base_price" example:"251.73"`
	SellingPrice          float64            `json:"selling_price" bson:"selling_price" example:"239.5"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5ef32362cb43d47593850b97"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"true"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"adipisicing"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2017-06-22T03:08:50 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef323627d915eb7e19022bf"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef32362fb8b3696adbc5c85"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2017-07-23T09:10:43 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef323621661906838c2d648"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef323626394868d340550f6"`
}

type PriceListDetailRequest struct {
	PriceListKey          primitive.ObjectID `json:"price_list_key" bson:"price_list_key" example:"5ef3236241f855dfa62623bf"`
	ProductKey            primitive.ObjectID `json:"product_key" bson:"product_key" example:"5ef32362e63434789ba57c48"`
	BasePrice             float64            `json:"base_price" bson:"base_price" example:"251.73"`
	SellingPrice          float64            `json:"selling_price" bson:"selling_price" example:"239.5"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5ef32362cb43d47593850b97"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"true"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"adipisicing"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef323627d915eb7e19022bf"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef32362fb8b3696adbc5c85"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef323621661906838c2d648"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef323626394868d340550f6"`
}
