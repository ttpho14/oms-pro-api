package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type IntegrationLog struct {
	ID           primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef30c010a6c988ebf981380"`
	ServiceName  string             `json:"service_name" bson:"service_name" example:"SAP"`
	FunctionName string             `json:"function_name" bson:"function_name"`
	Log1         string             `json:"log_1" bson:"log_1" example:"request body"`
	Log2         string             `json:"log_2" bson:"log_2" example:"response body"`
	Log3         string             `json:"log_3" bson:"log_3" example:"response body"`
	Date         time.Time          `json:"date" bson:"date" example:"2016-07-19T04:53:39 -07:00"`
}

type IntegrationLogRequest struct {
	ServiceName  string    `json:"service_name" bson:"service_name" example:"GetSalesOrderById"`
	FunctionName string    `json:"function_name" bson:"function_name"`
	Log1         string    `json:"log_1" bson:"log_1" example:"request body"`
	Log2         string    `json:"log_2" bson:"log_2" example:"response body"`
	Log3         string    `json:"log_3" bson:"log_3" example:"response body"`
}

func NewIntegrationLog(serviceName , functionName , log1 , log2 , log3 string) *IntegrationLog {
	return &IntegrationLog{
		ID: primitive.NewObjectID(),
		ServiceName: serviceName,
		FunctionName: functionName,
		Log1: log1,
		Log2: log2,
		Log3: log3,
		Date: time.Now(),
	}
}