package model

import (
	db "../database"
	sHelper "../service/helper"
	"../setting"
	"./enum"
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

// SalesOrder struct
type SalesOrder struct {
	ID                    primitive.ObjectID  `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef96d9c5954cbf2ea3cf7af" validate:"required"`
	SID                   string              `json:"sid" bson:"sid" example:"Doc001" validate:"required"`
	DocNum                string              `json:"doc_num" bson:"doc_num" example:"Doc001" validate:"required"`
	SiteKey               primitive.ObjectID  `json:"site_key" bson:"site_key" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	SiteId                string              `json:"site_id" bson:"site_id" example:"SGWHS" validate:"required"`
	AccountCode           string              `json:"account_code" bson:"account_code" example:"Account code" validate:"required"`
	SalesChannelKey       primitive.ObjectID  `json:"sales_channel_key" bson:"sales_channel_key" example:"5efc6f5fa74d733d55d7a390" validate:"required"`   //
	SalesChannelId        string              `json:"sales_channel_id" bson:"sales_channel_id" example:"SALESCHANNEL001" validate:"required"`              //
	SalesChannelName      string              `json:"sales_channel_name" bson:"sales_channel_name" example:"5efc6f5fa74d733d55d7a390" validate:"required"` //
	CountryKey            primitive.ObjectID  `json:"country_key" bson:"country_key" example:"5efc6f5fa74d733d55d7a390" validate:"required"`               //
	CountryId             string              `json:"country_id" bson:"country_id" example:"COUNTRY0001" validate:"required"`                              //
	RefNo                 string              `json:"ref_no" bson:"ref_no" example:"123456" validate:"required"`                                           //Ref No >>> YCH
	RefNo1                string              `json:"ref_no_1" bson:"ref_no_1" example:"123456" validate:"required"`                                       // Ref No 1 >>> SIA
	CustomerKey           primitive.ObjectID  `json:"customer_key" bson:"customer_key" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	CustomerName          string              `json:"customer_name" bson:"customer_name" example:"James" validate:"required"`
	Email                 string              `json:"email" bson:"email" example:"abc@pamcs.com" validate:"required"`
	DocDate               time.Time           `json:"doc_date" bson:"doc_date" example:"2020-07-11T01:47:04.424Z" validate:"required"`
	DocDueDate            time.Time           `json:"doc_due_date" bson:"doc_due_date"  example:"2020-07-11T01:47:04.424Z" validate:"required"`
	TenantKey             primitive.ObjectID  `json:"tenant_key" bson:"tenant_key" example:"5ef2c053d86e577333da9b04" validate:"required"`
	SalesType             string              `json:"sales_type" bson:"sales_type" example:"Normal, Staff, Test, Exchange etc." validate:"required"`
	CurrencyKey           primitive.ObjectID  `json:"currency_key" bson:"currency_key" example:"5ef2c053d86e577333da9b04" validate:"required"`
	CurrencyId            string              `json:"currency_id" bson:"currency_id" example:"CURRENCY0001" validate:"required"`
	CurrencyRate          float64             `json:"currency_rate" bson:"currency_rate" example:"1" validate:"required"`
	TotalBeforeDiscount   float64             `json:"total_before_discount" bson:"total_before_discount" example:"0" validate:"required"`
	DiscountAmount        float64             `json:"discount_amount" bson:"discount_amount" example:"0" validate:"required"`
	DiscountPercent       float64             `json:"discount_percent" bson:"discount_percent" example:"0" validate:"required"`
	TotalAfterDiscount    float64             `json:"total_after_discount" bson:"total_after_discount" example:"0" validate:"required"`
	TotalWeight           float64             `json:"total_weight,omitempty" bson:"total_weight,omitempty" example:"0" validate:"required"`
	TotalVolume           float64             `json:"total_volume,omitempty" bson:"total_volume,omitempty" example:"0" validate:"required"`
	TotalPackages         float64             `json:"total_packages,omitempty" bson:"total_packages,omitempty" example:"0" validate:"required"`
	FreightAmount         float64             `json:"freight_amount" bson:"freight_amount" example:"0" validate:"required"`
	TaxKey                primitive.ObjectID  `json:"tax_key" bson:"tax_key" example:"5ef2c053d86e577333da9b04" validate:"required"`
	TaxId                 string              `json:"tax_id" bson:"tax_id" example:"TAX00001" validate:"required"`
	TaxAmount             float64             `json:"tax_amount" bson:"tax_amount" example:"0" validate:"required"`
	TaxRate               float64             `json:"tax_rate" bson:"tax_rate" example:"0" validate:"required"`
	TotalAfterTax         float64             `json:"total_after_tax" bson:"total_after_tax" example:"0" validate:"required"`
	PayTaxAmount          float64             `json:"pay_tax_amount" bson:"pay_tax_amount" example:"0" validate:"required"`
	PayTaxRate            float64             `json:"pay_tax_rate" bson:"pay_tax_rate" example:"0" validate:"required"`
	PaidAmount            float64             `json:"paid_amount" bson:"paid_amount" example:"0" validate:"required"`
	BalanceAmount         float64             `json:"balance_amount" bson:"balance_amount" example:"0" validate:"required"`
	DocStatus             string              `json:"doc_status" bson:"doc_status" example:"Normal, Staff, Test, Exchange etc." validate:"required"`
	PaymentStatus         enum.PaymentStatus  `json:"payment_status" bson:"payment_status" example:"Pending, Partial, Complete,.." validate:"required"`
	ShippingStatus        enum.ShippingStatus `json:"shipping_status" bson:"shipping_status" example:"Pending, Partial, Complete,.." validate:"required"`
	OrderStatus           enum.OrderStatus    `json:"order_status" bson:"order_status" example:"Picked, Packed, Shipped, Delivered,.." validate:"required"`
	OrderType             string              `json:"order_type" bson:"order_type" example:"Picked, Packed, Shipped, Delivered,.." validate:"required"`
	EstimateReceiveDate   time.Time           `json:"estimate_receive_date" bson:"estimate_receive_date" example:"2020-07-11T01:47:04.424Z" validate:"required"`
	ShippingNumber        string              `json:"shipping_number" bson:"shipping_number" example:"SHP123" validate:"required"`
	Remark                string              `json:"remark" bson:"remark" example:"Remark" validate:"required"`
	AddValue              string              `json:"add_value" bson:"add_value" example:"Gift Notes, instruction" validate:"required"`
	BillingAddressKey     primitive.ObjectID  `json:"billing_address_key" bson:"billing_address_key" example:"5ef2c053d86e577333da9b04" validate:"required"`
	BillingAddress        BillingAddress      `json:"billing_address" bson:"billing_address"`
	ShippingAddressKey    primitive.ObjectID  `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053d86e577333da9b04" validate:"required"`
	ShippingAddress       ShippingAddress     `json:"shipping_address" bson:"shipping_address"`
	TrackingNo            string              `json:"tracking_no" bson:"tracking_no" example:"apbca"`
	Carrier               string              `json:"carrier" bson:"carrier" example:"ABC1231"`
	Method                string              `json:"method" bson:"method" example:"PADVX"`
	ObjectType            string              `json:"object_type" bson:"object_type" example:"type" validate:"required"`
	LabelUrl              string              `json:"label_url" bson:"label_url" example:"Yes, No, All" validate:"required"`
	SAPExport             bool                `json:"sap_export" bson:"sap_export" validate:"false"`
	CreatedDate           time.Time           `json:"created_date" bson:"created_date" example:"2020-07-11T01:47:04.424Z" validate:"required"`
	CreatedBy             primitive.ObjectID  `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0" validate:"required"`
	CreatedApplicationKey primitive.ObjectID  `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04" validate:"required"`
	UpdatedDate           time.Time           `json:"updated_date" bson:"updated_date" example:"2020-07-11T01:47:04.424Z" validate:"required"`
	UpdatedBy             primitive.ObjectID  `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0" validate:"required"`
	UpdatedApplicationKey primitive.ObjectID  `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04" validate:"required"`
}

type SalesOrderResponse struct {
	ID                       primitive.ObjectID         `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef96d9c5954cbf2ea3cf7af" validate:"required"`
	DocNum                   string                     `json:"doc_num" bson:"doc_num" example:"Doc001" validate:"required"`
	Site                     Site                       `json:"site" bson:"site" validate:"required"`
	SiteKey                  primitive.ObjectID         `json:"site_key" bson:"site_key" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	SiteId                   string                     `json:"site_id" bson:"site_id" example:"SGWHS" validate:"required"`
	AccountCode              string                     `json:"account_code" bson:"account_code" example:"Account code" validate:"required"`
	SalesChannel             SalesChannel               `json:"sales_channel" bson:"sales_channel" validate:"required"`
	SalesChannelId           string                     `json:"sales_channel_id" bson:"sales_channel_id" example:"SALESCHANNEL001" validate:"required"`
	SalesChannelName         string                     `json:"sales_channel_name" bson:"sales_channel_name" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	Country                  Country                    `json:"country" bson:"country" validate:"required"`
	CountryId                string                     `json:"country_id" bson:"country_id" example:"COUNTRY0001" validate:"required"`
	RefNo                    string                     `json:"ref_no" bson:"ref_no" example:"123456" validate:"required"`
	RefNo1                   string                     `json:"ref_no_1" bson:"ref_no_1" example:"123456" validate:"required"`
	Customer                 Customer                   `json:"customer" bson:"customer" validate:"required"`
	CustomerName             string                     `json:"customer_name" bson:"customer_name" example:"James" validate:"required"`
	Email                    string                     `json:"email" bson:"email" example:"abc@pamcs.com" validate:"required"`
	DocDate                  time.Time                  `json:"doc_date" bson:"doc_date" example:"2020-07-11T01:47:04.424Z" validate:"required"`
	DocDueDate               time.Time                  `json:"doc_due_date" bson:"doc_due_date"  example:"2020-07-11T01:47:04.424Z" validate:"required"`
	Tenant                   Tenant                     `json:"tenant" bson:"tenant" validate:"required"`
	SalesType                string                     `json:"sales_type" bson:"sales_type" example:"Normal, Staff, Test, Exchange etc." validate:"required"`
	Currency                 Currency                   `json:"currency" bson:"currency" validate:"required"`
	CurrencyId               string                     `json:"currency_id" bson:"currency_id" example:"CURRENCY0001" validate:"required"`
	CurrencyRate             float64                    `json:"currency_rate" bson:"currency_rate" example:"1" validate:"required"`
	TotalBeforeDiscount      float64                    `json:"total_before_discount" bson:"total_before_discount" example:"0" validate:"required"`
	DiscountAmount           float64                    `json:"discount_amount" bson:"discount_amount" example:"0" validate:"required"`
	DiscountPercent          float64                    `json:"discount_percent" bson:"discount_percent" example:"0" validate:"required"`
	TotalAfterDiscount       float64                    `json:"total_after_discount" bson:"total_after_discount" example:"0" validate:"required"`
	TotalWeight              float64                    `json:"total_weight" bson:"total_weight" example:"0" validate:"required"`
	TotalVolume              float64                    `json:"total_volume" bson:"total_volume" example:"0" validate:"required"`
	TotalPackages            float64                    `json:"total_packages" bson:"total_packages" example:"0" validate:"required"`
	FreightAmount            float64                    `json:"freight_amount" bson:"freight_amount" example:"0" validate:"required"`
	TaxId                    string                     `json:"tax_id" bson:"tax_id" example:"TAX00001" validate:"required"`
	TaxAmount                float64                    `json:"tax_amount" bson:"tax_amount" example:"0" validate:"required"`
	TaxRate                  float64                    `json:"tax_rate" bson:"tax_rate" example:"0" validate:"required"`
	TotalAfterTax            float64                    `json:"total_after_tax" bson:"total_after_tax" example:"0" validate:"required"`
	PayTaxAmount             float64                    `json:"pay_tax_amount" bson:"pay_tax_amount" example:"0" validate:"required"`
	PayTaxRate               float64                    `json:"pay_tax_rate" bson:"pay_tax_rate" example:"0" validate:"required"`
	PaidAmount               float64                    `json:"paid_amount" bson:"paid_amount" example:"0" validate:"required"`
	BalanceAmount            float64                    `json:"balance_amount" bson:"balance_amount" example:"0" validate:"required"`
	DocStatus                string                     `json:"doc_status" bson:"doc_status" example:"Normal, Staff, Test, Exchange etc." validate:"required"`
	PaymentStatus            enum.PaymentStatus         `json:"payment_status" bson:"payment_status" example:"Pending, Partial, Complete,.." validate:"required"`
	ShippingStatus           enum.ShippingStatus        `json:"shipping_status" bson:"shipping_status" example:"Pending, Partial, Complete,.." validate:"required"`
	OrderStatus              enum.OrderStatus           `json:"order_status" bson:"order_status" example:"Picked, Packed, Shipped, Delivered,.." validate:"required"`
	OrderType                string                     `json:"order_type" bson:"order_type" example:"Picked, Packed, Shipped, Delivered,.." validate:"required"`
	OrderQty                 float64                    `json:"order_quantity" bson:"order_quantity" example:"20" validate:"required"`
	ReturnQty                float64                    `json:"return_quantity" bson:"return_quantity" example:"20" validate:"required"`
	CancelQty                float64                    `json:"cancel_quantity" bson:"cancel_quantity" example:"20" validate:"required"`
	EstimateReceiveDate      time.Time                  `json:"estimate_receive_date" bson:"estimate_receive_date" example:"2020-07-11T01:47:04.424Z" validate:"required"`
	ShippingNumber           string                     `json:"shipping_number" bson:"shipping_number" example:"SHP123" validate:"required"`
	Remark                   string                     `json:"remark" bson:"remark" example:"Remark" validate:"required"`
	AddValue                 string                     `json:"add_value" bson:"add_value" example:"Gift Notes, instruction" validate:"required"`
	BillingAddressKey        primitive.ObjectID         `json:"billing_address_key" bson:"billing_address_key" example:"5ef2c053d86e577333da9b04" validate:"required"`
	BillingAddress           BillingAddress             `json:"billing_address" bson:"billing_address"`
	ShippingAddressKey       primitive.ObjectID         `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053d86e577333da9b04" validate:"required"`
	ShippingAddress          ShippingAddress            `json:"shipping_address" bson:"shipping_address"`
	TrackingNo               string                     `json:"tracking_no" bson:"tracking_no" example:"apbca"`
	Carrier                  string                     `json:"carrier" bson:"carrier" example:"ABC1231"`
	Method                   string                     `json:"method" bson:"method" example:"PADVX"`
	LabelUrl                 string                     `json:"label_url" bson:"label_url" example:"Yes, No, All" validate:"required"`
	ObjectType               string                     `json:"object_type" bson:"object_type" example:"type" validate:"required"`
	SAPExport                bool                       `json:"sap_export" bson:"sap_export" validate:"false"`
	CreatedDate              time.Time                  `json:"created_date" bson:"created_date" example:"2020-07-11T01:47:04.424Z" validate:"required"`
	CreatedBy                primitive.ObjectID         `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0" validate:"required"`
	CreatedApplicationKey    primitive.ObjectID         `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04" validate:"required"`
	UpdatedDate              time.Time                  `json:"updated_date" bson:"updated_date" example:"2020-07-11T01:47:04.424Z" validate:"required"`
	UpdatedBy                primitive.ObjectID         `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0" validate:"required"`
	UpdatedApplicationKey    primitive.ObjectID         `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04" validate:"required"`
	OrderPack                OrderPack                  `json:"order_pack" bson:"order_pack" example:"5ef2c053d86e577333da9b04" validate:"required"`
	SalesOrderDetail         []SalesOrderDetail         `json:"sales_order_details" bson:"sales_order_details" validate:"required"`
	SalesOrderComment        []SalesOrderComment        `json:"sales_order_comments" bson:"sales_order_comments" validate:"required"`
	SalesOrderFreight        []SalesOrderFreight        `json:"sales_order_freights" bson:"sales_order_freights" validate:"required"`
	SalesOrderDiscount       []SalesOrderDiscount       `json:"sales_order_discounts" bson:"sales_order_discounts" validate:"required"`
	SalesOrderShippingDetail []SalesOrderShippingDetail `json:"sales_order_shipping_details" bson:"sales_order_shipping_details" validate:"required"`
	SalesOrderPayment        []SalesOrderPayment        `json:"sales_order_payments" bson:"sales_order_payments" validate:"required"`
	SalesOrderPaymentCapture []SalesOrderPaymentCapture `json:"sales_order_payment_captures" bson:"sales_order_payment_captures" validate:"required"`
	SalesOrderPaymentRefund  []SalesOrderPaymentRefund  `json:"sales_order_payment_refunds" bson:"sales_order_payment_refunds" validate:"required"`
	SalesOrderPaymentReverse []SalesOrderPaymentReverse `json:"sales_order_payment_reverses" bson:"sales_order_payment_reverses" validate:"required"`
	SalesOrderStatus         []SalesOrderStatus         `json:"sales_order_status" bson:"sales_order_status" validate:"required"`
}

// SalesOrderRequest struct
type SalesOrderRequest struct {
	DocNum                          string                            `json:"doc_num" bson:"doc_num" example:"Doc001" validate:"required"`
	BrandKey                        primitive.ObjectID                `json:"brand_key" bson:"brand_key" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	SalesChannelId                  string                            `json:"sales_channel_id" bson:"sales_channel_id" example:"SALESCHANNEL0001" validate:"required"`
	SalesChannelName                string                            `json:"sales_channel_name" bson:"sales_channel_name" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	CountryKey                      primitive.ObjectID                `json:"country_key" bson:"country_key" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	CountryId                       string                            `json:"country_id" bson:"country_id" example:"COUNTRY0001" validate:"required"`
	RefNo                           string                            `json:"ref_no" bson:"ref_no" example:"123456" validate:"required"`
	RefNo1                          string                            `json:"ref_no_1" bson:"ref_no_1" example:"123456" validate:"required"`
	CustomerKey                     primitive.ObjectID                `json:"customer_key" bson:"customer_key" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	CustomerName                    string                            `json:"customer_name" bson:"customer_name" example:"James" validate:"required"`
	Email                           string                            `json:"email" bson:"email" example:"abc@pamcs.com" validate:"required"`
	DocDate                         time.Time                         `json:"doc_date" bson:"doc_date" example:"2020-07-11T01:47:04.424Z" validate:"required"`
	DocDueDate                      time.Time                         `json:"doc_due_date" bson:"doc_due_date"  example:"2020-07-11T01:47:04.424Z" validate:"required"`
	TenantKey                       primitive.ObjectID                `json:"tenant_key" bson:"tenant_key" example:"5ef2c053d86e577333da9b04" validate:"required"`
	SalesType                       string                            `json:"sales_type" bson:"sales_type" example:"Normal, Staff, Test, Exchange etc." validate:"required"`
	CurrencyId                      string                            `json:"currency_id" bson:"currency_id" example:"CURRENCY0001" validate:"required"`
	CurrencyRate                    float64                           `json:"currency_rate" bson:"currency_rate" example:"1" validate:"required"`
	TotalBeforeDiscount             float64                           `json:"total_before_discount" bson:"total_before_discount" example:"0" validate:"required"`
	DiscountAmount                  float64                           `json:"discount_amount" bson:"discount_amount" example:"0" validate:"required"`
	DiscountPercent                 float64                           `json:"discount_percent" bson:"discount_percent" example:"0" validate:"required"`
	TotalAfterDiscount              float64                           `json:"total_after_discount" bson:"total_after_discount" example:"0" validate:"required"`
	FreightAmount                   float64                           `json:"freight_amount" bson:"freight_amount" example:"0" validate:"required"`
	TaxId                           string                            `json:"tax_id" bson:"tax_id" example:"Tax00001" validate:"required"`
	TaxAmount                       float64                           `json:"tax_amount" bson:"tax_amount" example:"0" validate:"required"`
	TaxRate                         float64                           `json:"tax_rate" bson:"tax_rate" example:"0" validate:"required"`
	TotalAfterTax                   float64                           `json:"total_after_tax" bson:"total_after_tax" example:"0" validate:"required"`
	PayTaxAmount                    float64                           `json:"pay_tax_amount" bson:"pay_tax_amount" example:"0" validate:"required"`
	PayTaxRate                      float64                           `json:"pay_tax_rate" bson:"pay_tax_rate" example:"0" validate:"required"`
	PaidAmount                      float64                           `json:"paid_amount" bson:"paid_amount" example:"0" validate:"required"`
	BalanceAmount                   float64                           `json:"balance_amount" bson:"balance_amount" example:"0" validate:"required"`
	DocStatus                       string                            `json:"doc_status" bson:"doc_status" example:"Normal, Staff, Test, Exchange etc." validate:"required"`
	PaymentStatus                   enum.PaymentStatus                `json:"payment_status" bson:"payment_status" example:"Pending, Partial, Complete,.." validate:"required"`
	ShippingStatus                  enum.ShippingStatus               `json:"shipping_status" bson:"shipping_status" example:"Pending, Partial, Complete,.." validate:"required"`
	OrderType                       string                            `json:"order_type" bson:"order_type" example:"Picked, Packed, Shipped, Delivered,.." validate:"required"`
	EstimateReceiveDate             time.Time                         `json:"estimate_receive_date" bson:"estimate_receive_date" example:"2020-07-11T01:47:04.424Z" validate:"required"`
	ShippingNumber                  string                            `json:"shipping_number" bson:"shipping_number" example:"SHP123" validate:"required"`
	Remark                          string                            `json:"remark" bson:"remark" example:"Remark" validate:"required"`
	AddValue                        string                            `json:"add_value" bson:"add_value" example:"Gift Notes, instruction" validate:"required"`
	BillingAddressKey               primitive.ObjectID                `json:"billing_address_key" bson:"billing_address_key" example:"5ef2c053d86e577333da9b04" validate:"required"`
	BillingAddress                  BillingAddress                    `json:"billing_address" bson:"billing_address"`
	ShippingAddressKey              primitive.ObjectID                `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053d86e577333da9b04" validate:"required"`
	ShippingAddress                 ShippingAddress                   `json:"shipping_address" bson:"shipping_address"`
	TrackingNo                      string                            `json:"tracking_no" bson:"tracking_no" example:"apbca"`
	Carrier                         string                            `json:"carrier" bson:"carrier" example:"ABC1231"`
	Method                          string                            `json:"method" bson:"method" example:"PADVX"`
	LabelUrl                        string                            `json:"label_url" bson:"label_url" example:"Yes, No, All" validate:"required"`
	SAPExport                       bool                              `json:"sap_export" bson:"sap_export" validate:"false"`
	ObjectType                      string                            `json:"object_type" bson:"object_type" example:"type" validate:"required"`
	CreatedBy                       primitive.ObjectID                `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0" validate:"required"`
	CreatedApplicationKey           primitive.ObjectID                `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04" validate:"required"`
	UpdatedBy                       primitive.ObjectID                `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0" validate:"required"`
	UpdatedApplicationKey           primitive.ObjectID                `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04" validate:"required"`
	SalesOrderDetailRequest         []SalesOrderDetailRequest         `json:"sales_order_details" bson:"sales_order_details" validate:"required"`
	SalesOrderCommentRequest        []SalesOrderCommentRequest        `json:"sales_order_comments" bson:"sales_order_comments" validate:"required"`
	SalesOrderFreightRequest        []SalesOrderFreightRequest        `json:"sales_order_freights" bson:"sales_order_freights" validate:"required"`
	SalesOrderDiscountRequest       []SalesOrderDiscountRequest       `json:"sales_order_discounts" bson:"sales_order_discounts" validate:"required"`
	SalesOrderShippingDetailRequest []SalesOrderShippingDetailRequest `json:"sales_order_shipping_details" bson:"sales_order_shipping_details" validate:"required"`
	SalesOrderPaymentRequest        []SalesOrderPaymentRequest        `json:"sales_order_payments" bson:"sales_order_payments" validate:"required"`
	//OrderStatus                     enum.OrderStatus                  `json:"order_status" bson:"order_status" example:"Picked, Packed, Shipped, Delivered,.." validate:"required"`
}

type SalesOrderExports struct {
	OrderNo        string               `json:"order_no" bson:"order_no" example:"Doc001" validate:"required"`
	OrderDate      time.Time            `json:"order_date" bson:"order_date" example:"123124818294" validate:"required"`
	Customer       string               `json:"customer_name" bson:"customer" validate:"required"`
	Country        string               `json:"country" bson:"country" validate:"required"`
	SalesChannel   string               `json:"sales_channel" bson:"sales_channel" validate:"required"`
	OrderStatus    enum.OrderStatus     `json:"order_status" bson:"order_status" validate:"required"`
	OrderAmount    float64              `json:"order_amount" bson:"order_amount" validate:"required"`
	EAN            string               `json:"ean" bson:"ean"`
	Description    string               `json:"description" bson:"description"`
	Size           string               `json:"size" bson:"size"`
	Color          string               `json:"color" bson:"color"`
	BasePrice      float64              `json:"base_price" bson:"base_price"`
	LineStatus     enum.OrderLineStatus `json:"line_status" bson:"line_status" validate:"required"`
	OrderQuantity  float64              `json:"order_quantity" bson:"order_quantity"`
	CancelQuantity float64              `json:"cancel_quantity" bson:"cancel_quantity"`
	ReturnQuantity float64              `json:"return_quantity" bson:"return_quantity"`
	DiscountAmount float64              `json:"discount_amount" bson:"discount_amount"`
	ShippingFee    float64              `json:"shipping_fee" bson:"shipping_fee"`
	SubTotal       float64              `json:"sub_total" bson:"sub_total"`
}

type BillingAddress struct {
	BFirstName    string `json:"b_first_name" bson:"b_first_name" example:"cupidatat" validate:"required"`
	BLastName     string `json:"b_last_name" bson:"b_last_name" example:"afqe" validate:"required"`
	BAddress1     string `json:"b_address_1" bson:"b_address_1" example:"430 Dumont Avenue, Wacissa, Vermont, 8902" validate:"required"`
	BAddress2     string `json:"b_address_2" bson:"b_address_2" example:"924 Cypress Avenue, Waterloo, Kansas, 8230" validate:"required"`
	BAddress3     string `json:"b_address_3" bson:"b_address_3" example:"532 Middagh Street, Greenock, American Samoa, 3223" validate:"required"`
	BCity         string `json:"b_city" bson:"b_city" example:"Harmon" validate:"required"`
	BZipCode      string `json:"b_zip_code" bson:"b_zip_code" example:"cupidatat" validate:"required"`
	BState        string `json:"b_state" bson:"b_state" example:"Virgin Islands" validate:"required"`
	BCountry      string `json:"b_country" bson:"b_country" example:"El Salvador" validate:"required"`
	BPhone        string `json:"b_phone" bson:"b_phone" example:"(841) 584-2276" validate:"required"`
	BEmailAddress string `json:"b_email_address" bson:"b_email_address" example:"annettecurry@essensia.com" validate:"required"`
}

type ShippingAddress struct {
	SFirstName    string `json:"s_first_name" bson:"s_first_name" example:"cupidatat" validate:"required"`
	SLastName     string `json:"s_last_name" bson:"s_last_name" example:"afqe" validate:"required"`
	SAddress1     string `json:"s_address_1" bson:"s_address_1" example:"430 Dumont Avenue, Wacissa, Vermont, 8902" validate:"required"`
	SAddress2     string `json:"s_address_2" bson:"s_address_2" example:"924 Cypress Avenue, Waterloo, Kansas, 8230" validate:"required"`
	SAddress3     string `json:"s_address_3" bson:"s_address_3" example:"532 Middagh Street, Greenock, American Samoa, 3223" validate:"required"`
	SCity         string `json:"s_city" bson:"s_city" example:"Harmon" validate:"required"`
	SZipCode      string `json:"s_zip_code" bson:"s_zip_code" example:"cupidatat" validate:"required"`
	SState        string `json:"s_state" bson:"s_state" example:"Virgin Islands" validate:"required"`
	SCountry      string `json:"s_country" bson:"s_country" example:"El Salvador" validate:"required"`
	SPhone        string `json:"s_phone" bson:"s_phone" example:"(841) 584-2276" validate:"required"`
	SEmailAddress string `json:"s_email_address" bson:"s_email_address" example:"Dannettecurry@essensia.com" validate:"required"`
}

//------------------- Sub Model ---------------------
type BillingAddressUpdateRequest struct {
	BFirstName    string `json:"b_first_name,omitempty" bson:"b_first_name,omitempty" example:"cupidatat" validate:"required"`
	BLastName     string `json:"b_last_name,omitempty" bson:"b_last_name,omitempty" example:"afqe" validate:"required"`
	BAddress1     string `json:"b_address_1,omitempty" bson:"b_address_1,omitempty" example:"430 Dumont Avenue, Wacissa, Vermont, 8902" validate:"required"`
	BAddress2     string `json:"b_address_2,omitempty" bson:"b_address_2,omitempty" example:"924 Cypress Avenue, Waterloo, Kansas, 8230" validate:"required"`
	BAddress3     string `json:"b_address_3,omitempty" bson:"b_address_3,omitempty" example:"532 Middagh Street, Greenock, American Samoa, 3223" validate:"required"`
	BCity         string `json:"b_city,omitempty" bson:"b_city,omitempty" example:"Harmon" validate:"required"`
	BZipCode      string `json:"b_zip_code,omitempty" bson:"b_zip_code,omitempty" example:"cupidatat" validate:"required"`
	BState        string `json:"b_state,omitempty" bson:"b_state,omitempty" example:"Virgin Islands" validate:"required"`
	BCountry      string `json:"b_country,omitempty" bson:"b_country,omitempty" example:"El Salvador" validate:"required"`
	BPhone        string `json:"b_phone,omitempty" bson:"b_phone,omitempty" example:"(841) 584-2276" validate:"required"`
	BEmailAddress string `json:"b_email_address,omitempty" bson:"b_email_address,omitempty" example:"annettecurry@essensia.com" validate:"required"`
}

type ShippingAddressUpdateRequest struct {
	SFirstName    string `json:"s_first_name,omitempty" bson:"s_first_name,omitempty" example:"cupidatat" validate:"required"`
	SLastName     string `json:"s_last_name,omitempty" bson:"s_last_name,omitempty" example:"afqe" validate:"required"`
	SAddress1     string `json:"s_address_1,omitempty" bson:"s_address_1,omitempty" example:"430 Dumont Avenue, Wacissa, Vermont, 8902" validate:"required"`
	SAddress2     string `json:"s_address_2,omitempty" bson:"s_address_2,omitempty" example:"924 Cypress Avenue, Waterloo, Kansas, 8230" validate:"required"`
	SAddress3     string `json:"s_address_3,omitempty" bson:"s_address_3,omitempty" example:"532 Middagh Street, Greenock, American Samoa, 3223" validate:"required"`
	SCity         string `json:"s_city,omitempty" bson:"s_city,omitempty" example:"Harmon" validate:"required"`
	SZipCode      string `json:"s_zip_code,omitempty" bson:"s_zip_code,omitempty" example:"cupidatat" validate:"required"`
	SState        string `json:"s_state,omitempty" bson:"s_state,omitempty" example:"Virgin Islands" validate:"required"`
	SCountry      string `json:"s_country,omitempty" bson:"s_country,omitempty" example:"El Salvador" validate:"required"`
	SPhone        string `json:"s_phone,omitempty" bson:"s_phone,omitempty" example:"(841) 584-2276" validate:"required"`
	SEmailAddress string `json:"s_email_address,omitempty" bson:"s_email_address,omitempty" example:"Dannettecurry@essensia.com" validate:"required"`
}

type SalesOrderUpdateRequest struct {
	OrderStatus                    enum.OrderStatus                 `json:"order_status,omitempty" bson:"order_status,omitempty" example:"New"`
	ShippingStatus                 enum.ShippingStatus              `json:"shipping_status,omitempty" bson:"shipping_status,omitempty" example:"New"`
	PaymentStatus                  enum.PaymentStatus               `json:"payment_status,omitempty" bson:"payment_status,omitempty" example:"Paid"`
	TrackingNo                     string                           `json:"tracking_no,omitempty" bson:"tracking_no,omitempty" example:"TRACK00012"`
	RefNo                          string                           `json:"ref_no,omitempty" bson:"ref_no,omitempty" example:"TRACK00012"`
	DocDueDate                     int64                            `json:"doc_due_date,omitempty" bson:"doc_due_date,omitempty" example:"10515131521"`
	Carrier                        string                           `json:"carrier,omitempty" bson:"carrier,omitempty" example:"DHL"`
	Method                         string                           `json:"method,omitempty" bson:"method,omitempty" example:"Drop shipping"`
	PhoneNo                        string                           `json:"phone_no,omitempty" bson:"phone_no,omitempty" example:"0813526162215"`
	ShippingLabelUrl               string                           `json:"shipping_label_url,omitempty" bson:"shipping_label_url,omitempty" example:"0"`
	BillingAddress                 *BillingAddressUpdateRequest     `json:"billing_address,omitempty" bson:"billing_address,omitempty"`
	ShippingAddress                *ShippingAddressUpdateRequest    `json:"shipping_address,omitempty" bson:"shipping_address,omitempty"`
	TotalWeight                    float64                          `json:"total_weight,omitempty" bson:"total_weight,omitempty" example:"0" validate:"required"`
	TotalVolume                    float64                          `json:"total_volume,omitempty" bson:"total_volume,omitempty" example:"0" validate:"required"`
	TotalPackages                  float64                          `json:"total_packages,omitempty" bson:"total_packages,omitempty" example:"0" validate:"required"`
	SalesOrderDetailsUpdateRequest []*SalesOrderDetailUpdateRequest `json:"sales_order_details,omitempty" bson:"sales_order_details,omitempty"`
	//RefNo1                           string                            `json:"ref_no_1,omitempty" bson:"ref_no_1,omitempty" example:"TRACK00012"`
}

func (sReq SalesOrderRequest) Validation() (err error) {

	////Check Email
	//if sReq.Email == "" {
	//	return errors.New("Email is required")
	//}
	//
	//if !valid.IsValidEmail(sReq.Email) {
	//	return errors.New("Email is invalid")
	//}
	if len(sReq.SalesOrderDetailRequest) > 0 {
		for _, detail := range sReq.SalesOrderDetailRequest {
			if err = detail.Validation(); err != nil {
				return errors.New("Sales Order Detail validation ERROR - " + err.Error())
			}
		}
	} else {
		return errors.New("Please enter at least one line for sales order detail ")
	}

	if err = sReq.IsValidDocDate();err != nil {
		return errors.New("Check Doc Date ERROR - "+err.Error())
	}

	return nil
}

func (sReq SalesOrderRequest) IsValidDocDate() (err error) {
	if sReq.DocDate.Unix() < 1601510400{ //2020-10-01 GMT+00:00
		return errors.New(fmt.Sprintf("Reject Sales Order has Doc Date [%s] little than [2020-10-01 GMT+00:00] ",sReq.DocDate))
	}

	return nil
}


func (sReq SalesOrderRequest) IsDuplicateDocNum() (err error) {
	col := db.Database.Collection(setting.SalesOrderTable)
	filter := bson.M{"doc_num": sHelper.RegexStringID(sReq.DocNum)}
	count, err := col.CountDocuments(context.Background(), filter)
	if count > 0 {
		return errors.New("Doc Number is already exists ")
	}
	return nil
}

func (sUReq SalesOrderUpdateRequest) CheckSIAUpdateOrder(salesOrder SalesOrder) (err error) {
	if salesOrder.SalesChannelId == setting.SalesChannelLazada || salesOrder.SalesChannelId == setting.SalesChannelZalora {
		if sUReq.OrderStatus == enum.OrderStatusShipped && salesOrder.OrderStatus != enum.OrderStatusPacked {
			return errors.New(fmt.Sprintf("Reject [%s] order from SIA when current status of Sales Order is not [%s] ", enum.OrderStatusShipped, enum.OrderStatusPacked))
		}
		if sUReq.OrderStatus == enum.OrderStatusDelivered && salesOrder.OrderStatus != enum.OrderStatusShipped {
			return errors.New(fmt.Sprintf("Reject [%s] order from SIA when current status of Sales Order is not [%s] ", enum.OrderStatusDelivered, enum.OrderStatusShipped))
		}
	}
	return nil
}

func (sUReq SalesOrderUpdateRequest) Validation(salesOrder SalesOrder) (err error) {
	_, err = sUReq.PaymentStatus.IsValid()
	if err != nil && sUReq.PaymentStatus != "" {
		return err
	}
	_, err = sUReq.ShippingStatus.IsValid()
	if err != nil && sUReq.ShippingStatus != "" {
		return err
	}
	_, err = sUReq.OrderStatus.IsValid()
	if err != nil && sUReq.OrderStatus != "" {
		return err
	}

	if err = sUReq.CheckSIAUpdateOrder(salesOrder); err != nil {
		return errors.New("Check SIA Update order ERROR - "+err.Error())
	}
	return nil
}
