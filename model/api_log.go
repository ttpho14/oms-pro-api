package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type ApiLog struct {
	ID            primitive.ObjectID `json:"_id" bson:"_id" example:"5ef30c010a6c988ebf981380"`
	ApplicationId string             `json:"application_id" bson:"application_id" example:"PUT"`
	Method        string             `json:"method" bson:"method" example:"PUT"`
	Function      string             `json:"function" bson:"function" example:"GetSalesOrderById"`
	RequestBody   string             `json:"request_body" bson:"request_body" example:"request body"`
	ResponseBody  string             `json:"response_body" bson:"response_body" example:"response body"`
	Date          time.Time          `json:"date" bson:"date"`
}

type ApiLogRequest struct {
	ApplicationId string `json:"application_id" bson:"application_id" example:"PUT"`
	Method        string `json:"method" bson:"method" example:"PUT"`
	Function      string `json:"function" bson:"function" example:"GetSalesOrderById"`
	RequestBody   string `json:"request_body" bson:"request_body" example:"request body"`
	ResponseBody  string `json:"response_body" bson:"response_body" example:"response body"`
}
