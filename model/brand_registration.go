package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type BrandRegistration struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef310ad3d7dc23fd278deaa"`
	BrandKey              primitive.ObjectID `json:"brand_key" bson:"brand_key" example:"5ef310ad3d7dc23fd278deaa"`
	SalesChannelKey       primitive.ObjectID `json:"sales_channel_key" bson:"sales_channel_key" example:"5ef310ad4879b341399436e8"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"true"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"esse"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2015-05-15T11:20:58 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef310adfe4156d71a70524c"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef310add2dde9191289b1c9"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2019-12-30T09:05:14 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef301394a64c2e6ca57838c"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef3013963eba4f8a7fa139f"`
}

type BrandRegistrationRequest struct {
	//BrandKey              primitive.ObjectID `json:"brand_key" bson:"brand_key" example:"5ef310ad3d7dc23fd278deaa"`
	SalesChannelKey       primitive.ObjectID `json:"sales_channel_key" bson:"sales_channel_key" example:"5ef310adb7f93e163380ace9"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"true"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"afasb"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef30139b1fe3ab144ba7c2f"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef301392fdd17f6073bdd9c"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef301394a64c2e6ca57838c"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef3013963eba4f8a7fa139f"`
}
