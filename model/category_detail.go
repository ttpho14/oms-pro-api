package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type CategoryDetail struct {
	ID                      primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef96d9c5954cbf2ea3cf7af"`
	CategoryKey             primitive.ObjectID `json:"category_key" bson:"category_key" example:"5efe9f31ece882ccb0230b38"`
	CategoryDetailParentKey primitive.ObjectID `json:"category_detail_parent_key" bson:"category_detail_parent_key" example:"5efe9f31b230daaa3738d9fb"`
	ProductKey              primitive.ObjectID `json:"product_key" bson:"product_key" example:"5efe9f319a55a1bc6ba62b67"`
	Description             string             `json:"description" bson:"description" example:"id"`
	ObjectType              string             `json:"object_type" bson:"object_type" example:"anim"`
	IsActive                bool               `json:"is_active" bson:"is_active" example:"false"`
	IsDeleted               bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	CreatedDate             time.Time          `json:"created_date" bson:"created_date" example:"2014-01-14T05:21:24 -07:00"`
	CreatedBy               primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efe9f3186e97039d0dc9ef2"`
	CreatedApplicationKey   primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef96db2dc3410a231844080"`
	UpdatedDate             time.Time          `json:"updated_date" bson:"updated_date" example:"2016-06-30T01:42:01 -07:00"`
	UpdatedBy               primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efe9fc7c85416319b9f067b"`
	UpdatedApplicationKey   primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef96db23c574ffde7644a58"`
}

type CategoryDetailRequest struct {
	CategoryKey             primitive.ObjectID `json:"category_key" bson:"category_key" example:"5efe9f31ece882ccb0230b38"`
	CategoryDetailParentKey primitive.ObjectID `json:"category_detail_parent_key" bson:"category_detail_parent_key" example:"5efe9f31b230daaa3738d9fb"`
	ProductKey              primitive.ObjectID `json:"product_key" bson:"product_key" example:"5efe9f319a55a1bc6ba62b67"`
	Description             string             `json:"description" bson:"description" example:"id"`
	ObjectType              string             `json:"object_type" bson:"object_type" example:"anim"`
	IsActive                bool               `json:"is_active" bson:"is_active" example:"false"`
	CreatedBy               primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efe9f3186e97039d0dc9ef2"`
	CreatedApplicationKey   primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef96db2dc3410a231844080"`
	UpdatedBy               primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efe9fc7c85416319b9f067b"`
	UpdatedApplicationKey   primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef96db23c574ffde7644a58"`
}
