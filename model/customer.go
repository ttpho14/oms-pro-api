package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Customer struct {
	ID                        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5efc1cfad263971daa76d4bc"`
	CustomerNumber            string             `json:"customer_number" bson:"customer_number" example:"C200123"`
	TenantKey                 primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5efc1cfa8ed2a7041ac307f9"`
	CustomerGroupKey          primitive.ObjectID `json:"customer_group_key" bson:"customer_group_key" example:"5efc1cfa49f15c6e9459b7c5"`
	FirstName                 string             `json:"first_name" bson:"first_name" example:"Hawkins"`
	LastName                  string             `json:"last_name" bson:"last_name" example:"Crawford"`
	PhoneNo                   string             `json:"phone_no" bson:"phone_no" example:"(860) 520-3584"`
	MobileNo                  string             `json:"mobile_no" bson:"mobile_no" example:"(852) 424-2353"`
	Email                     string             `json:"email" bson:"email" example:"hawkinscrawford@anixang.com"`
	Birthday                  string             `json:"birthday" bson:"birthday" example:"1997-10-19T11:09:41 -07:00"`
	Gender                    string             `json:"gender" bson:"gender" example:"female"`
	SalesChannelKey           primitive.ObjectID `json:"sales_channel_key" bson:"sales_channel_key" example:"5efc1cfa0ddde51c51a55b30"`
	Privacy                   bool               `json:"privacy" bson:"privacy" example:"true"`
	CustomerType              string             `json:"customer_type" bson:"customer_type" example:"commodo"`
	DocumentId                string             `json:"document_id" bson:"document_id" example:"ea"`
	DefaultBillingAddressKey  primitive.ObjectID `json:"default_billing_address_key" bson:"default_billing_address_key" example:"5efc1cfa9b209b0e00103c55"`
	DefaultShippingAddressKey primitive.ObjectID `json:"default_shipping_address_key" bson:"default_shipping_address_key" example:"5efc1cfaf32390ae432febe0"`
	IsActive                  bool               `json:"is_active" bson:"is_active" example:"false"`
	IsDeleted                 bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	ObjectType                string             `json:"object_type" bson:"object_type" example:"eiusmod"`
	CreatedDate               time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2016-06-13T05:22:29 -07:00"`
	CreatedBy                 primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efc1cfa46e9968f9c6d9d7c"`
	CreatedApplicationKey     primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5efc1cfaaec3d46ad7bf90df"`
	UpdatedDate               time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2017-08-14T06:19:22 -07:00"`
	UpdatedBy                 primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efc1cfadbb8358614063344"`
	UpdatedApplicationKey     primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5efc1cfa023ed7deb7430b8c"`
	Address                   []Address          `json:"addresses" bson:"addresses"`
}

type CustomerWithDetails struct {
	ID                     primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5efc1cfad263971daa76d4bc"`
	CustomerNumber         string             `json:"customer_number" bson:"customer_number" example:"C200123"`
	TenantKey              primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5efc1cfa8ed2a7041ac307f9"`
	CustomerGroupKey       primitive.ObjectID `json:"customer_group_key" bson:"customer_group_key" example:"5efc1cfa49f15c6e9459b7c5"`
	FirstName              string             `json:"first_name" bson:"first_name" example:"Hawkins"`
	LastName               string             `json:"last_name" bson:"last_name" example:"Crawford"`
	PhoneNo                string             `json:"phone_no" bson:"phone_no" example:"(860) 520-3584"`
	MobileNo               string             `json:"mobile_no" bson:"mobile_no" example:"(852) 424-2353"`
	Email                  string             `json:"email" bson:"email" example:"hawkinscrawford@anixang.com"`
	Birthday               string             `json:"birthday" bson:"birthday" example:"1997-10-19T11:09:41 -07:00"`
	Gender                 string             `json:"gender" bson:"gender" example:"female"`
	SalesChannelKey        primitive.ObjectID `json:"sales_channel_key" bson:"sales_channel_key" example:"5efc1cfa0ddde51c51a55b30"`
	Privacy                bool               `json:"privacy" bson:"privacy" example:"true"`
	CustomerType           string             `json:"customer_type" bson:"customer_type" example:"commodo"`
	DocumentId             string             `json:"document_id" bson:"document_id" example:"ea"`
	DefaultBillingAddress  Address            `json:"default_billing_address" bson:"default_billing_address"`
	DefaultShippingAddress Address            `json:"default_shipping_address" bson:"default_shipping_address"`
	IsActive               bool               `json:"is_active" bson:"is_active" example:"false"`
	IsDeleted              bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	ObjectType             string             `json:"object_type" bson:"object_type" example:"eiusmod"`
	CreatedDate            time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2016-06-13T05:22:29 -07:00"`
	CreatedBy              primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efc1cfa46e9968f9c6d9d7c"`
	CreatedApplicationKey  primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5efc1cfaaec3d46ad7bf90df"`
	UpdatedDate            time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2017-08-14T06:19:22 -07:00"`
	UpdatedBy              primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efc1cfadbb8358614063344"`
	UpdatedApplicationKey  primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5efc1cfa023ed7deb7430b8c"`
	Address                []Address          `json:"addresses" bson:"addresses"`
}

type CustomerRequest struct {
	TenantKey              primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5efc6f5f3803fcbd0caa9799"`
	CustomerGroupKey       primitive.ObjectID `json:"customer_group_key" bson:"customer_group_key" example:"5efc6f5f3803fcbd0caa9799"`
	FirstName              string             `json:"first_name" bson:"first_name" example:"Hawkins"`
	LastName               string             `json:"last_name" bson:"last_name" example:"Crawford"`
	PhoneNo                string             `json:"phone_no" bson:"phone_no" example:"(860) 520-3584"`
	MobileNo               string             `json:"mobile_no" bson:"mobile_no" example:"(852) 424-2353"`
	Email                  string             `json:"email" bson:"email" example:"hawkinscrawford@anixang.com"`
	Birthday               string             `json:"birthday" bson:"birthday" example:"1997-10-19T11:09:41 -07:00"`
	Gender                 string             `json:"gender" bson:"gender" example:"female"`
	SalesChannelKey        primitive.ObjectID `json:"sales_channel_key" bson:"sales_channel_key" example:"5efc6f5f3803fcbd0caa9799"`
	Privacy                bool               `json:"privacy" bson:"privacy" example:"true"`
	CustomerType           string             `json:"customer_type" bson:"customer_type" example:"commodo"`
	DocumentId             string             `json:"document_id" bson:"document_id" example:"ea"`
	DefaultBillingAddress  AddressRequest     `json:"default_billing_address" bson:"default_billing_address"`
	DefaultShippingAddress AddressRequest     `json:"default_shipping_address" bson:"default_shipping_address"`
	IsActive               bool               `json:"is_active" bson:"is_active" example:"false"`
	CreatedBy              primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efc6f5f3803fcbd0caa9799"`
	CreatedApplicationKey  primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5efc6f5f3803fcbd0caa9799"`
	UpdatedBy              primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efc6f5f3803fcbd0caa9799"`
	UpdatedApplicationKey  primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5efc6f5f3803fcbd0caa9799"`
	//Address                   []Address          `json:"addresses" bson:"addresses"`
}

type CustomerAutoIncrement struct {
	ID             string `json:"_id" bson:"_id"`
	SequenceNumber int64  `json:"sequence_number" bson:"sequence_number"`
}
