package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type SalesChannel struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef2c053f7a34f7669bb8246"`
	SalesChannelId        string             `json:"sales_channel_id" bson:"sales_channel_id" example:"laboris"`
	Description           string             `json:"description" bson:"description" example:"dolor"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5ef2c053d86e577333da9b04"`
	RegionKey             primitive.ObjectID `json:"region_key" bson:"region_key" example:"5ef2c0533c2a907f40c5bf17"`
	CountryKey            primitive.ObjectID `json:"country_key" bson:"country_key" example:"5ef2c0539be6e19cab46d5ac"`
	PriceListKey          primitive.ObjectID `json:"price_list_key" bson:"price_list_key" example:"5ef2c0532d28f4b507acff53"`
	ChannelType           string             `json:"channel_type" bson:"channel_type" example:"est"`
	ContactPerson1        string             `json:"contact_person_1" bson:"contact_person_1" example:"Horne Jimenez"`
	ContactEmail1         string             `json:"contact_email_1" bson:"contact_email_1" example:"hornejimenez@geekko.com"`
	ContactNumber1        string             `json:"contact_number_1" bson:"contact_number_1" example:"(826) 506-3022"`
	ContactPerson2        string             `json:"contact_person_2" bson:"contact_person_2" example:"Elnora Guerrero"`
	ContactEmail2         string             `json:"contact_email_2" bson:"contact_email_2" example:"elnoraguerrero@geekko.com"`
	ContactNumber2        string             `json:"contact_number_2" bson:"contact_number_2" example:"(849) 575-3701"`
	Address1              string             `json:"address_1" bson:"address_1" example:"908 Juliana Place, Chautauqua, Montana, 1055"`
	Address2              string             `json:"address_2" bson:"address_2" example:"860 Crystal Street, Florence, Palau, 1774"`
	Address3              string             `json:"address_3" bson:"address_3" example:"657 Jerome Avenue, Downsville, Rhode Island, 5349"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"false"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"true"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"Lorem"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2015-05-24T08:07:59 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c1b825e06580b4f17c17"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2015-05-29T11:50:05 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c1b815c622499daa2be4"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c1b885414ad7861dcef8"`
}

type AddSalesChannel struct {
	SalesChannelId        string             `json:"sales_channel_id" bson:"sales_channel_id" example:"aute"`
	Description           string             `json:"description" bson:"description" example:"magna"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5ef2c23804431095c72f5b1f"`
	RegionKey             primitive.ObjectID `json:"region_key" bson:"region_key" example:"5ef2c238b8742f16e5cc3bfe"`
	CountryKey            primitive.ObjectID `json:"country_key" bson:"country_key" example:"5ef2c23839c0b082a38c5f06"`
	PriceListKey          primitive.ObjectID `json:"price_list_key" bson:"price_list_key" example:"5ef2c2389559dcb55f3fc500"`
	ChannelType           string             `json:"channel_type" bson:"channel_type" example:"dolore"`
	ContactPerson1        string             `json:"contact_person_1" bson:"contact_person_1" example:"Dale Newman"`
	ContactEmail1         string             `json:"contact_email_1" bson:"contact_email_1" example:"dalenewman@geekko.com"`
	ContactNumber1        string             `json:"contact_number_1" bson:"contact_number_1" example:"(972) 487-2113"`
	ContactPerson2        string             `json:"contact_person_2" bson:"contact_person_2" example:"Mcdonald Church"`
	ContactEmail2         string             `json:"contact_email_2" bson:"contact_email_2" example:"mcdonaldchurch@geekko.com"`
	ContactNumber2        string             `json:"contact_number_2" bson:"contact_number_2" example:"(979) 540-2882"`
	Address1              string             `json:"address_1" bson:"address_1" example:"283 Dare Court, Dellview, Federated States Of Micronesia, 2285"`
	Address2              string             `json:"address_2" bson:"address_2" example:"527 Dorchester Road, Idamay, Maine, 3647"`
	Address3              string             `json:"address_3" bson:"address_3" example:"251 Hendrickson Place, Reinerton, Oregon, 7561"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"false"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"true"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"culpa"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c238c01c53e2da163b0f"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c23826cca837b315a991"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c238d65cbdcc88a29043"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c238d04d6ce715a39c69"`
}
