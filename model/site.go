package model

import (
	"../database"
	"../setting"
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Site struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef96d9c5954cbf2ea3cf7af"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5efea83faeeb3942a4940ece"`
	SalesChannelKey       primitive.ObjectID `json:"sales_channel_key" bson:"sales_channel_key" example:"5efea83f41a4bfc5dfdd9e85"`
	SiteId                string             `json:"site_id" bson:"site_id" example:"duis"`
	Description           string             `json:"description" bson:"description" example:"duis"`
	ContactPerson1        string             `json:"contact_person_1" bson:"contact_person_1" example:"Johnson Marquez"`
	ContactEmail1         string             `json:"contact_email_1" bson:"contact_email_1" example:"ruthiebradshaw@geekko.com"`
	ContactNumber1        string             `json:"contact_number_1" bson:"contact_number_1" example:"+1 (901) 571-2674"`
	ContactPerson2        string             `json:"contact_person_2" bson:"contact_person_2" example:"Cohen Morris"`
	ContactEmail2         string             `json:"contact_email_2" bson:"contact_email_2" example:"cohenmorris@geekko.com"`
	ContactNumber2        string             `json:"contact_number_2" bson:"contact_number_2" example:"+1 (884) 588-3222"`
	Address1              string             `json:"address_1" bson:"address_1" example:"430 Dumont Avenue, Wacissa, Vermont, 8902"`
	Address2              string             `json:"address_2" bson:"address_2" example:"924 Cypress Avenue, Waterloo, Kansas, 8230"`
	Address3              string             `json:"address_3" bson:"address_3" example:"532 Middagh Street, Greenock, American Samoa, 3223"`
	Latitude              string             `json:"latitude" bson:"latitude" example:"laboris"`
	Longitude             string             `json:"longitude" bson:"longitude" example:"fugiat"`
	IsVirtual             bool               `json:"is_virtual" bson:"is_virtual" example:"false"`
	CountryKey            primitive.ObjectID `json:"country_key" bson:"country_key" example:"5efea83fc48503644aa95d3a"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"nisi"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"false"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	CreatedDate           time.Time          `json:"created_date" bson:"created_date" example:"2016-07-19T04:53:39 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef96db2dc3410a231844080"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef96db2dc3410a231844080"`
	UpdatedDate           time.Time          `json:"updated_date" bson:"updated_date" example:"2016-07-19T04:53:39 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef96db23c574ffde7644a58"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef96db23c574ffde7644a58"`
}

type SiteRequest struct {
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5efea83faeeb3942a4940ece"`
	SalesChannelKey       primitive.ObjectID `json:"sales_channel_key" bson:"sales_channel_key" example:"5efea83f41a4bfc5dfdd9e85"`
	SiteId                string             `json:"site_id" bson:"site_id" example:"duis"`
	Description           string             `json:"description" bson:"description" example:"duis"`
	ContactPerson1        string             `json:"contact_person_1" bson:"contact_person_1" example:"Johnson Marquez"`
	ContactEmail1         string             `json:"contact_email_1" bson:"contact_email_1" example:"ruthiebradshaw@geekko.com"`
	ContactNumber1        string             `json:"contact_number_1" bson:"contact_number_1" example:"+1 (901) 571-2674"`
	ContactPerson2        string             `json:"contact_person_2" bson:"contact_person_2" example:"Cohen Morris"`
	ContactEmail2         string             `json:"contact_email_2" bson:"contact_email_2" example:"cohenmorris@geekko.com"`
	ContactNumber2        string             `json:"contact_number_2" bson:"contact_number_2" example:"+1 (884) 588-3222"`
	Address1              string             `json:"address_1" bson:"address_1" example:"430 Dumont Avenue, Wacissa, Vermont, 8902"`
	Address2              string             `json:"address_2" bson:"address_2" example:"924 Cypress Avenue, Waterloo, Kansas, 8230"`
	Address3              string             `json:"address_3" bson:"address_3" example:"532 Middagh Street, Greenock, American Samoa, 3223"`
	Latitude              string             `json:"latitude" bson:"latitude" example:"laboris"`
	Longitude             string             `json:"longitude" bson:"longitude" example:"fugiat"`
	IsVirtual             bool               `json:"is_virtual" bson:"is_virtual" example:"false"`
	CountryKey            primitive.ObjectID `json:"country_key" bson:"country_key" example:"5efea83fc48503644aa95d3a"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"nisi"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"false"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef96db2dc3410a231844080"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef96db2dc3410a231844080"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef96db23c574ffde7644a58"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef96db23c574ffde7644a58"`
}

func (s SiteRequest) Validation() (err error) {
	if err = s.CheckSiteId();err != nil {
		return errors.New("Check Site ID ERROR - "+err.Error())
	}

	return nil
}

func (s SiteRequest) CheckSiteId() (err error){
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	count, err := database.Database.Collection(setting.SiteTable).CountDocuments(ctx,bson.M{"site_id":s.SiteId})
	if err != nil  {
		return errors.New("Count Site with Site Id ERROR - "+err.Error())
	}

	if count > 0 {
		return errors.New("Site Id is already exists ")
	}

	return nil
}