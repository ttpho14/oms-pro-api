package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type ProductGroup struct {
	ID             primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5efc529fbbfa67be2f63b486"`
	ProductGroupId string             `json:"product_group_id" bson:"product_group_id" example:"nostrud"`
	Description    string             `json:"description" bson:"description" example:"eiusmod"`
	TenantKey      primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5f02ae4ef996f570ddce7d82"`

	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"true"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"eiusmod"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2019-05-16T03:20:31 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef30139b1fe3ab144ba7c2f"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef301392fdd17f6073bdd9c"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2019-12-30T09:05:14 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef301394a64c2e6ca57838c"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef3013963eba4f8a7fa139f"`
}

type ProductGroupRequest struct {
	ProductGroupId string             `json:"product_group_id" bson:"product_group_id" example:"nostrud"`
	Description    string             `json:"description" bson:"description" example:"eiusmod"`
	TenantKey      primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5f02ae4ef996f570ddce7d82"`

	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"true"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"eiusmod"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef30139b1fe3ab144ba7c2f"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef301392fdd17f6073bdd9c"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef301394a64c2e6ca57838c"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef3013963eba4f8a7fa139f"`
}
