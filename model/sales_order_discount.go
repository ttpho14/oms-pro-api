package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// SalesOrderDiscount struct
type SalesOrderDiscount struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef2c053f7a34f7669bb8246"`
	DocLineKey            primitive.ObjectID `json:"doc_line_key" bson:"doc_line_key" example:"5ef2c053f7a34f7669bb8246"`
	DocKey                primitive.ObjectID `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246"`
	DiscountCode          string             `json:"discount_code" bson:"discount_code" example:"DSC001"`
	Description           string             `json:"description" bson:"description" example:"description"`
	DiscountAmount        float64            `json:"discount_amount" bson:"discount_amount" example:"0"`
	DiscountPercent       float64            `json:"discount_percent" bson:"discount_percent" example:"0"`
	ApplicableQuantity    float64            `json:"applicable_quantity" bson:"applicable_quantity" example:"0"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"type"`
	CreatedDate           time.Time          `json:"created_date" bson:"created_date" example:"2020-07-11T01:47:04.424Z"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedDate           time.Time          `json:"updated_date" bson:"updated_date" example:"2020-07-11T01:47:04.424Z"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
}

// SalesOrderDiscountRequest struct
type SalesOrderDiscountRequest struct {
	DocLineKey            primitive.ObjectID `json:"doc_line_key" bson:"doc_line_key" example:"5ef2c053f7a34f7669bb8246"`
	DocKey                primitive.ObjectID `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246"`
	DiscountCode          string             `json:"discount_code" bson:"discount_code" example:"DSC001"`
	Description           string             `json:"description" bson:"description" example:"description"`
	DiscountAmount        float64            `json:"discount_amount" bson:"discount_amount" example:"0"`
	DiscountPercent       float64            `json:"discount_percent" bson:"discount_percent" example:"0"`
	ApplicableQuantity    float64            `json:"applicable_quantity" bson:"applicable_quantity" example:"0"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"type"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
}
