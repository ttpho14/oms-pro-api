package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

// ReturnStatus struct
type ReturnStatus struct {
	ID         primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef2c053f7a34f7669bb8246"`
	DocNum     string             `json:"doc_num" bson:"doc_num"`
	DocLineNum string             `json:"doc_line_num" bson:"doc_line_num"`
	FromStatus string             `json:"from_status" bson:"from_status"`
	ToStatus   string             `json:"to_status" bson:"to_status"`
	Date       time.Time          `json:"date" bson:"date" example:"2020-07-11T01:47:04.424Z"`
}


type ReturnStatusResponse struct {
	ID         primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef2c053f7a34f7669bb8246"`
	DocNum     string             `json:"doc_num" bson:"doc_num"`
	DocLineNum string             `json:"doc_line_num" bson:"doc_line_num"`
	FromStatus string             `json:"from_status" bson:"from_status"`
	ToStatus   string             `json:"to_status" bson:"to_status"`
	Date       time.Time          `json:"date" bson:"date" example:"2020-07-11T01:47:04.424Z"`
}
// ReturnStatusRequest struct
//type ReturnStatusRequest struct {
//	DocNum     string             `json:"doc_num" bson:"doc_num"`
//	DocLineNum string             `json:"doc_line_num" bson:"doc_line_num"`
//	FromStatus string             `json:"from_status" bson:"from_status"`
//	ToStatus   string             `json:"to_status" bson:"to_status"`
//	Date       time.Time          `json:"date" bson:"date" example:"2020-07-11T01:47:04.424Z"`
//}
