package model

import (
	"./enum"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

// SalesOrderPaymentCapture struct
type SalesOrderPaymentCapture struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef2c053f7a34f7669bb8246"`
	DocKey                primitive.ObjectID `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246"`
	PaymentTypeKey        primitive.ObjectID `json:"payment_type_key" bson:"payment_type_key" example:"5ef2c053f7a34f7669bb8246"`
	PaymentTypeId         string             `json:"payment_type_id" bson:"payment_type_id" example:"Cash"`
	TransactionId         string             `json:"transaction_id" bson:"transaction_id" example:"TRX01253153"`
	AuthorizationNo       string             `json:"authorization_no" bson:"authorization_no" example:"AU01"`
	Amount                float64            `json:"amount" bson:"amount" example:"0"`
	Balance               float64            `json:"balance" bson:"balance" example:"0"`
	Status                enum.PaymentStatus `json:"status" bson:"status" example:"REVERSE"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"type"`
	CreatedDate           time.Time          `json:"created_date" bson:"created_date" example:"2016-07-19T04:53:39 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedDate           time.Time          `json:"updated_date" bson:"updated_date" example:"2016-07-19T04:53:39 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
}

// SalesOrderPaymentCaptureRequest struct
type SalesOrderPaymentCaptureRequest struct {
	DocKey                primitive.ObjectID `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246"`
	PaymentTypeKey        primitive.ObjectID `json:"payment_type_key" bson:"payment_type_key" example:"5ef2c053f7a34f7669bb8246"`
	PaymentTypeId         string             `json:"payment_type_id" bson:"payment_type_id" example:"Cash"`
	TransactionId         string             `json:"transaction_id" bson:"transaction_id" example:"TRX01253153"`
	AuthorizationNo       string             `json:"authorization_no" bson:"authorization_no" example:"AU01"`
	Amount                float64            `json:"amount" bson:"amount" example:"0"`
	Balance               float64            `json:"balance" bson:"balance" example:"0"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"type"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
}

// SalesOrderPaymentCapture struct
type SalesOrderPaymentCaptureResponse struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef2c053f7a34f7669bb8246"`
	DocKey                primitive.ObjectID `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246"`
	PaymentTypeKey        primitive.ObjectID `json:"payment_type_key" bson:"payment_type_key" example:"5ef2c053f7a34f7669bb8246"`
	PaymentTypeId         string             `json:"payment_type_id" bson:"payment_type_id" example:"Cash"`
	TransactionId         string             `json:"transaction_id" bson:"transaction_id" example:"TRX01253153"`
	AuthorizationNo       string             `json:"authorization_no" bson:"authorization_no" example:"AU01"`
	Amount                float64            `json:"amount" bson:"amount" example:"0"`
	Balance               float64            `json:"balance" bson:"balance" example:"0"`
	Status                enum.PaymentStatus `json:"status" bson:"status" example:"REVERSE"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"type"`
	CreatedDate           time.Time          `json:"created_date" bson:"created_date" example:"2016-07-19T04:53:39 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedDate           time.Time          `json:"updated_date" bson:"updated_date" example:"2016-07-19T04:53:39 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
}