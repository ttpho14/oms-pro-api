package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Address struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef96d9c5954cbf2ea3cf7af" validate:"required"`
	SourceObjectType      string             `json:"source_object_type" bson:"source_object_type" example:"5efc6f5f34714ae957380f49" validate:"required"`
	SourceKey             primitive.ObjectID `json:"source_key" bson:"source_key" example:"5efc6f5f3803fcbd0caa9799" validate:"required"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5efc6f5fa74d733d55d7a390" validate:"required"`
	AddressType           string             `json:"address_type" bson:"address_type" example:"ut" validate:"required"`
	StreetNo              string             `json:"street_no" bson:"street_no" example:"Durland Place" validate:"required"`
	Building              string             `json:"building" bson:"building" example:"Lorem" validate:"required"`
	FirstName             string             `json:"first_name" bson:"first_name"`
	LastName             string             `json:"last_name" bson:"last_name"`
	Address1              string             `json:"address_1" bson:"address_1" example:"430 Dumont Avenue, Wacissa, Vermont, 8902" validate:"required"`
	Address2              string             `json:"address_2" bson:"address_2" example:"924 Cypress Avenue, Waterloo, Kansas, 8230" validate:"required"`
	Address3              string             `json:"address_3" bson:"address_3" example:"532 Middagh Street, Greenock, American Samoa, 3223" validate:"required"`
	City                  string             `json:"city" bson:"city" example:"Harmon" validate:"required"`
	ZipCode               string             `json:"zip_code" bson:"zip_code" example:"cupidatat" validate:"required"`
	State                 string             `json:"state" bson:"state" example:"Virgin Islands" validate:"required"`
	County                string             `json:"county" bson:"county" example:"Svalbard and Jan Mayen Islands" validate:"required"`
	Country               string             `json:"country" bson:"country" example:"El Salvador" validate:"required"`
	Phone1                string             `json:"phone_1" bson:"phone_1" example:"(841) 584-2276" validate:"required"`
	Phone2                string             `json:"phone_2" bson:"phone_2" example:"(841) 584-2276" validate:"required"`
	EmailAddress          string             `json:"email_address" bson:"email_address" example:"annettecurry@essensia.com" validate:"required"`
	Company               string             `json:"company" bson:"company" example:"Geekol" validate:"required"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"true" validate:"required"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"false" validate:"required"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"nisi" validate:"required"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false" validate:"required"`
	CreatedDate           time.Time          `json:"created_date" bson:"created_date" example:"2016-07-19T04:53:39 -07:00" validate:"required"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef96db2dc3410a231844080" validate:"required"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef96db2dc3410a231844080" validate:"required"`
	UpdatedDate           time.Time          `json:"updated_date" bson:"updated_date" example:"2016-07-19T04:53:39 -07:00" validate:"required"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef96db23c574ffde7644a58" validate:"required"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef96db23c574ffde7644a58" validate:"required"`
}

type AddressRequest struct {
	SourceObjectType      string             `json:"source_object_type" bson:"source_object_type" example:"awfso" validate:"required"`
	SourceKey             primitive.ObjectID `json:"source_key" bson:"source_key" example:"5efc6f5f3803fcbd0caa9799" validate:"required"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5efc6f5f3803fcbd0caa9799" validate:"required"`
	AddressType           string             `json:"address_type" bson:"address_type" example:"ut" validate:"required"`
	StreetNo              string             `json:"street_no" bson:"street_no" example:"Durland Place" validate:"required"`
	Building              string             `json:"building" bson:"building" example:"Lorem" validate:"required"`
	Address1              string             `json:"address_1" bson:"address_1" example:"430 Dumont Avenue, Wacissa, Vermont, 8902" validate:"required"`
	Address2              string             `json:"address_2" bson:"address_2" example:"924 Cypress Avenue, Waterloo, Kansas, 8230" validate:"required"`
	Address3              string             `json:"address_3" bson:"address_3" example:"532 Middagh Street, Greenock, American Samoa, 3223" validate:"required"`
	City                  string             `json:"city" bson:"city" example:"Harmon" validate:"required"`
	ZipCode               string             `json:"zip_code" bson:"zip_code" example:"cupidatat" validate:"required"`
	State                 string             `json:"state" bson:"state" example:"Virgin Islands" validate:"required"`
	County                string             `json:"county" bson:"county" example:"Svalbard and Jan Mayen Islands" validate:"required"`
	Country               string             `json:"country" bson:"country" example:"El Salvador" validate:"required"`
	Phone1                string             `json:"phone_1" bson:"phone_1" example:"(841) 584-2276" validate:"required"`
	Phone2                string             `json:"phone_2" bson:"phone_2" example:"(841) 584-2276" validate:"required"`
	EmailAddress          string             `json:"email_address" bson:"email_address" example:"annettecurry@essensia.com" validate:"required"`
	Company               string             `json:"company" bson:"company" example:"Geekol" validate:"required"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"true" validate:"required"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"false" validate:"required"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efc6f5f3803fcbd0caa9799" validate:"required"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5efc6f5f3803fcbd0caa9799" validate:"required"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efc6f5f3803fcbd0caa9799" validate:"required"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5efc6f5f3803fcbd0caa9799" validate:"required"`
}

type AddressUpdateRequest struct {
	FirstName             string             `json:"first_name,omitempty" bson:"first_name,omitempty"`
	LastName             string             `json:"last_name,omitempty" bson:"last_name,omitempty"`
	SourceObjectType      string             `json:"source_object_type,omitempty" bson:"source_object_type,omitempty" example:"awfso" validate:"required"`
	SourceKey             primitive.ObjectID `json:"source_key,omitempty" bson:"source_key,omitempty" example:"5efc6f5f3803fcbd0caa9799" validate:"required"`
	TenantKey             primitive.ObjectID `json:"tenant_key,omitempty" bson:"tenant_key,omitempty" example:"5efc6f5f3803fcbd0caa9799" validate:"required"`
	AddressType           string             `json:"address_type,omitempty" bson:"address_type,omitempty" example:"ut" validate:"required"`
	StreetNo              string             `json:"street_no,omitempty" bson:"street_no,omitempty" example:"Durland Place" validate:"required"`
	Building              string             `json:"building,omitempty" bson:"building,omitempty" example:"Lorem" validate:"required"`
	Address1              string             `json:"address_1,omitempty" bson:"address_1,omitempty" example:"430 Dumont Avenue, Wacissa, Vermont, 8902" validate:"required"`
	Address2              string             `json:"address_2,omitempty" bson:"address_2,omitempty" example:"924 Cypress Avenue, Waterloo, Kansas, 8230" validate:"required"`
	Address3              string             `json:"address_3,omitempty" bson:"address_3,omitempty" example:"532 Middagh Street, Greenock, American Samoa, 3223" validate:"required"`
	City                  string             `json:"city,omitempty" bson:"city,omitempty" example:"Harmon" validate:"required"`
	ZipCode               string             `json:"zip_code,omitempty" bson:"zip_code,omitempty" example:"cupidatat" validate:"required"`
	State                 string             `json:"state,omitempty" bson:"state,omitempty" example:"Virgin Islands" validate:"required"`
	County                string             `json:"county,omitempty" bson:"county,omitempty" example:"Svalbard and Jan Mayen Islands" validate:"required"`
	Country               string             `json:"country,omitempty" bson:"country,omitempty" example:"El Salvador" validate:"required"`
	Phone1                string             `json:"phone_1,omitempty" bson:"phone_1,omitempty" example:"(841) 584-2276" validate:"required"`
	Phone2                string             `json:"phone_2,omitempty" bson:"phone_2,omitempty" example:"(841) 584-2276" validate:"required"`
	EmailAddress          string             `json:"email_address,omitempty" bson:"email_address,omitempty" example:"annettecurry@essensia.com" validate:"required"`
	Company               string             `json:"company,omitempty" bson:"company,omitempty" example:"Geekol" validate:"required"`
	IsDefault             bool               `json:"is_default,omitempty" bson:"is_default,omitempty" example:"true" validate:"required"`
	IsActive              bool               `json:"is_active,omitempty" bson:"is_active,omitempty" example:"false" validate:"required"`
	UpdatedBy             primitive.ObjectID `json:"updated_by,omitempty" bson:"updated_by,omitempty" example:"5efc6f5f3803fcbd0caa9799" validate:"required"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key,omitempty" bson:"updated_application_key,omitempty" example:"5efc6f5f3803fcbd0caa9799" validate:"required"`
}
