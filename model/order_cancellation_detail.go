package model

import (
	"../model/enum"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type OrderCancellationDetail struct {
	ID                    primitive.ObjectID         `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef2c053f7a34f7669bb8246"`
	DocNum                string                     `json:"doc_num" bson:"doc_num" example:"Doc num"`
	DocKey                primitive.ObjectID         `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246"`
	DocLineNum            string                     `json:"doc_line_num" bson:"doc_line_num" example:"Doc line num"`
	DocLineStatus         enum.CancelOrderLineStatus `json:"doc_line_status" bson:"doc_line_status" example:"Picking, Packing, Transferring, Transit 1, Transit 2"`
	ProductKey            primitive.ObjectID         `json:"product_key" bson:"product_key" example:"5ef2c053f7a34f7669bb8246"`
	ProductCode           string                     `json:"product_code" bson:"product_code" example:"0"`
	ProductSize           string                     `json:"product_size" bson:"product_size" example:"XL" validate:"required"`
	ProductColor          string                     `json:"product_color" bson:"product_color" example:"red" validate:"required"`
	ProductUom            string                     `json:"product_uom" bson:"product_uom" example:"eiusmod"`
	Description           string                     `json:"description" bson:"description" example:"Description"`
	BaseObjectType        string                     `json:"base_object_type" bson:"base_object_type" example:"type"`
	BaseDocKey            primitive.ObjectID         `json:"base_doc_line" bson:"base_doc_line" example:"5ef2c053f7a34f7669bb8246"`
	BaseDocNum            string                     `json:"base_doc_num" bson:"base_doc_num" example:"123456"`
	BaseDocLineNum        string                     `json:"base_doc_line_num" bson:"base_doc_line_num" example:"Base doc line num"`
	BaseDocLineKey        primitive.ObjectID         `json:"base_doc_line_key" bson:"base_doc_line_key" example:"5ef2c053f7a34f7669bb8246"`
	Quantity              float64                    `json:"quantity" bson:"quantity" example:"0"`
	OrderQuantity         float64                    `json:"order_quantity" bson:"order_quantity" example:"0"`
	Price                 float64                    `json:"price" bson:"price" example:"0"`
	ReturnQuantity        float64                    `json:"return_quantity" bson:"return_quantity" example:"0"`
	LineDiscountAmount    float64                    `json:"line_discount_amount" bson:"line_discount_amount" example:"0"`
	LineDiscountPercent   float64                    `json:"line_discount_percent" bson:"line_discount_percent" example:"0"`
	HeaderDiscountAmount  float64                    `json:"header_discount_amount" bson:"header_discount_amount" example:"0"`
	PriceAfterDiscount    float64                    `json:"price_after_discount" bson:"price_after_discount" example:"0"`
	FreightAmount         float64                    `json:"freight_amount" bson:"freight_amount" example:"0" validate:"required"`
	TaxCodeKey            primitive.ObjectID         `json:"tax_code_key" bson:"tax_code_key" example:"5ef2c053f7a34f7669bb8246"`
	TaxRate               float64                    `json:"tax_rate" bson:"tax_rate" example:"0"`
	PriceAfterTax         float64                    `json:"price_after_tax" bson:"price_after_tax" example:"0"`
	SiteKey               primitive.ObjectID         `json:"origin_site_key" bson:"origin_site_key" example:"5ef2c053f7a34f7669bb8246"`
	Remark                string                     `json:"remark" bson:"remark" example:"Remark"`
	BillingAddressKey     primitive.ObjectID         `json:"billing_address_key" bson:"billing_address_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	BillingAddress        BillingAddress             `json:"billing_address" bson:"billing_address"`
	ShippingAddressKey    primitive.ObjectID         `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053f7a34f7669bb8246"`
	ShippingAddress       ShippingAddress            `json:"shipping_address" bson:"shipping_address"`
	ImageUrl              string                     `json:"image_url" bson:"image_url" example:""`
	ObjectType            string                     `json:"object_type" bson:"object_type" example:"type"`
	CreatedDate           time.Time                  `json:"created_date" bson:"created_date" example:"2016-07-19T04:53:39 -07:00"`
	CreatedBy             primitive.ObjectID         `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID         `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedDate           time.Time                  `json:"updated_date" bson:"updated_date" example:"2016-07-19T04:53:39 -07:00"`
	UpdatedBy             primitive.ObjectID         `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID         `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
}

type OrderCancellationDetailRequest struct {
	DocLineStatus         enum.CancelOrderLineStatus `json:"doc_line_status" bson:"doc_line_status" example:"Picking, Packing, Transferring, Transit 1, Transit 2"`
	BaseDocLineNum        string                     `json:"base_doc_line_num" bson:"base_doc_line_num" example:"Base doc line num"`
	Quantity              float64                    `json:"quantity" bson:"quantity" example:"0"`
	CreatedBy             primitive.ObjectID         `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID         `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedBy             primitive.ObjectID         `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID         `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`

	//DocNum                string             `json:"doc_num" bson:"doc_num" example:"Doc num"`
	//DocLineNum            string             `json:"doc_line_num" bson:"doc_line_num" example:"Doc line num"`
	//DocKey                primitive.ObjectID `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246"`

	//ProductKey            primitive.ObjectID `json:"product_key" bson:"product_key" example:"5ef2c053f7a34f7669bb8246"`
	//ProductCode           string             `json:"product_code" bson:"product_code" example:"0"`
	//ProductSize           string             `json:"product_size" bson:"product_size" example:"XL" validate:"required"`
	//ProductColor          string             `json:"product_color" bson:"product_color" example:"red" validate:"required"`
	//Description           string             `json:"description" bson:"description" example:"Description"`
	//BaseObjectType        string             `json:"base_object_type" bson:"base_object_type" example:"type"`
	//BaseDocNum            string             `json:"base_doc_num" bson:"base_doc_num" example:"123456"`

	//OrderQuantity         float64            `json:"order_quantity" bson:"order_quantity" example:"0"`
	//Price                 float64            `json:"price" bson:"price" example:"0"`
	//ReturnQuantity        float64            `json:"return_quantity" bson:"return_quantity" example:"0"`
	//LineDiscountAmount    float64            `json:"line_discount_amount" bson:"line_discount_amount" example:"0"`
	//LineDiscountPercent   float64            `json:"line_discount_percent" bson:"line_discount_percent" example:"0"`
	//HeaderDiscountAmount  float64            `json:"header_discount_amount" bson:"header_discount_amount" example:"0"`
	//PriceAfterDiscount    float64            `json:"price_after_discount" bson:"price_after_discount" example:"0"`
	//TaxCodeKey            primitive.ObjectID `json:"tax_code_key" bson:"tax_code_key" example:"5ef2c053f7a34f7669bb8246"`
	//TaxRate               float64            `json:"tax_rate" bson:"tax_rate" example:"0"`
	//PriceAfterTax         float64            `json:"price_after_rate" bson:"price_after_rate" example:"0"`
	//SiteKey               primitive.ObjectID `json:"origin_site_key" bson:"origin_site_key" example:"5ef2c053f7a34f7669bb8246"`
	//Remark                string             `json:"remark" bson:"remark" example:"Remark"`
	//BillingAddressKey     primitive.ObjectID `json:"billing_address_key" bson:"billing_address_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	//BillingAddress        BillingAddress     `json:"billing_address" bson:"billing_address"`
	//ShippingAddressKey    primitive.ObjectID `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053f7a34f7669bb8246"`
	//ShippingAddress       ShippingAddress    `json:"shipping_address" bson:"shipping_address"`

}

type OrderCancellationDetailResponse struct {
	ID                    primitive.ObjectID         `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef2c053f7a34f7669bb8246"`
	DocNum                string                     `json:"doc_num" bson:"doc_num" example:"Doc num"`
	DocKey                primitive.ObjectID         `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246"`
	DocLineNum            string                     `json:"doc_line_num" bson:"doc_line_num" example:"Doc line num"`
	DocLineStatus         enum.CancelOrderLineStatus `json:"doc_line_status" bson:"doc_line_status" example:"Picking, Packing, Transferring, Transit 1, Transit 2"`
	ProductKey            primitive.ObjectID         `json:"product_key" bson:"product_key" example:"5ef2c053f7a34f7669bb8246"`
	ProductCode           string                     `json:"product_code" bson:"product_code" example:"0"`
	ProductSize           string                     `json:"product_size" bson:"product_size" example:"XL" validate:"required"`
	ProductColor          string                     `json:"product_color" bson:"product_color" example:"red" validate:"required"`
	ProductUom            string                     `json:"product_uom" bson:"product_uom" example:"eiusmod"`
	Description           string                     `json:"description" bson:"description" example:"Description"`
	BaseObjectType        string                     `json:"base_object_type" bson:"base_object_type" example:"type"`
	BaseDocKey            primitive.ObjectID         `json:"base_doc_line" bson:"base_doc_line" example:"5ef2c053f7a34f7669bb8246"`
	BaseDocNum            string                     `json:"base_doc_num" bson:"base_doc_num" example:"123456"`
	BaseDocLineNum        string                     `json:"base_doc_line_num" bson:"base_doc_line_num" example:"Base doc line num"`
	BaseDocLineKey        primitive.ObjectID         `json:"base_doc_line_key" bson:"base_doc_line_key" example:"5ef2c053f7a34f7669bb8246"`
	Quantity              float64                    `json:"quantity" bson:"quantity" example:"0"`
	OrderQuantity         float64                    `json:"order_quantity" bson:"order_quantity" example:"0"`
	Price                 float64                    `json:"price" bson:"price" example:"0"`
	ReturnQuantity        float64                    `json:"return_quantity" bson:"return_quantity" example:"0"`
	LineDiscountAmount    float64                    `json:"line_discount_amount" bson:"line_discount_amount" example:"0"`
	LineDiscountPercent   float64                    `json:"line_discount_percent" bson:"line_discount_percent" example:"0"`
	HeaderDiscountAmount  float64                    `json:"header_discount_amount" bson:"header_discount_amount" example:"0"`
	PriceAfterDiscount    float64                    `json:"price_after_discount" bson:"price_after_discount" example:"0"`
	FreightAmount         float64                    `json:"freight_amount" bson:"freight_amount" example:"0" validate:"required"`
	TaxCodeKey            primitive.ObjectID         `json:"tax_code_key" bson:"tax_code_key" example:"5ef2c053f7a34f7669bb8246"`
	TaxRate               float64                    `json:"tax_rate" bson:"tax_rate" example:"0"`
	PriceAfterTax         float64                    `json:"price_after_tax" bson:"price_after_tax" example:"0"`
	SiteKey               primitive.ObjectID         `json:"origin_site_key" bson:"origin_site_key" example:"5ef2c053f7a34f7669bb8246"`
	Remark                string                     `json:"remark" bson:"remark" example:"Remark"`
	BillingAddressKey     primitive.ObjectID         `json:"billing_address_key" bson:"billing_address_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	BillingAddress        BillingAddress             `json:"billing_address" bson:"billing_address"`
	ShippingAddressKey    primitive.ObjectID         `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053f7a34f7669bb8246"`
	ShippingAddress       ShippingAddress            `json:"shipping_address" bson:"shipping_address"`
	ImageUrl              string                     `json:"image_url" bson:"image_url" example:""`
	ObjectType            string                     `json:"object_type" bson:"object_type" example:"type"`
	CreatedDate           time.Time                  `json:"created_date" bson:"created_date" example:"2016-07-19T04:53:39 -07:00"`
	CreatedBy             primitive.ObjectID         `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID         `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedDate           time.Time                  `json:"updated_date" bson:"updated_date" example:"2016-07-19T04:53:39 -07:00"`
	UpdatedBy             primitive.ObjectID         `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID         `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
}

//func (main OrderCancellationDetail) ToResponse() (res OrderCancellationDetailResponse, err error) {
//	err = copier.Copy(&res, &main)
//	if err != nil {
//		return OrderCancellationDetailResponse{}, err
//	}
//	res.CreatedDate = main.CreatedDate.Unix()
//	res.UpdatedDate = main.UpdatedDate.Unix()
//
//	return res, nil
//}
