package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Vendor struct {
	ID                        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5efc529fbbfa67be2f63b486"`
	TenantKey                 primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5efc529f6832dab3cca767fd"`
	VendorGroupKey            primitive.ObjectID `json:"vendor_group_key" bson:"vendor_group_key" example:"5efc529f6d3c966b4d59d5b8"`
	CompanyName               string             `json:"company_name" bson:"company_name" example:"Blackwell"`
	VendorID                  string             `json:"vendor_id" bson:"vendor_id" example:"Crawford"`
	VendorType                string             `json:"vendor_type" bson:"vendor_type" example:"(860) 520-3584"`
	Description               string             `json:"description" bson:"description" example:"(852) 424-2353"`
	ContactPerson             string             `json:"contact_person" bson:"contact_person" example:"hawkinscrawford@anixang.com"`
	PhoneNo                   string             `json:"phone_no" bson:"phone_no" example:"1997-10-19T11:09:41 -07:00"`
	MobileNo                  string             `json:"mobile_no" bson:"mobile_no" example:"female"`
	Email                     string             `json:"email" bson:"email" example:"5efc1cfa0ddde51c51a55b30"`
	TaxId                     string             `json:"tax_id" bson:"tax_id" example:"true"`
	DefaultBillingAddressKey  primitive.ObjectID `json:"default_billing_address_key" bson:"df_billing_address_key" example:"5efc1cfa9b209b0e00103c55"`
	DefaultShippingAddressKey primitive.ObjectID `json:"default_shipping_address_key" bson:"df_shipping_address_key" example:"5efc1cfaf32390ae432febe0"`
	IsActive                  bool               `json:"is_active" bson:"is_active" example:"false"`
	IsDeleted                 bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	ObjectType                string             `json:"object_type" bson:"object_type" example:"eiusmod"`
	CreatedDate               time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2016-06-13T05:22:29 -07:00"`
	CreatedBy                 primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efc1cfa46e9968f9c6d9d7c"`
	CreatedApplicationKey     primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5efc1cfaaec3d46ad7bf90df"`
	UpdatedDate               time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2017-08-14T06:19:22 -07:00"`
	UpdatedBy                 primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efc1cfadbb8358614063344"`
	UpdatedApplicationKey     primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5efc1cfa023ed7deb7430b8c"`
}

type AddVendor struct {
	TenantKey                 primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5efc529f6832dab3cca767fd"`
	VendorGroupKey            primitive.ObjectID `json:"vendor_group_key" bson:"vendor_group_key" example:"5efc529f6d3c966b4d59d5b8"`
	CompanyName               string             `json:"company_name" bson:"company_name" example:"Blackwell"`
	VendorID                  string             `json:"vendor_id" bson:"vendor_id" example:"Crawford"`
	VendorType                string             `json:"vendor_type" bson:"vendor_type" example:"(860) 520-3584"`
	Description               string             `json:"description" bson:"description" example:"(852) 424-2353"`
	ContactPerson             string             `json:"contact_person" bson:"contact_person" example:"hawkinscrawford@anixang.com"`
	PhoneNo                   string             `json:"phone_no" bson:"phone_no" example:"1997-10-19T11:09:41 -07:00"`
	MobileNo                  string             `json:"mobile_no" bson:"mobile_no" example:"female"`
	Email                     string             `json:"email" bson:"email" example:"5efc1cfa0ddde51c51a55b30"`
	TaxId                     string             `json:"tax_id" bson:"tax_id" example:"true"`
	DefaultBillingAddressKey  primitive.ObjectID `json:"default_billing_address_key" bson:"df_billing_address_key" example:"5efc1cfa9b209b0e00103c55"`
	DefaultShippingAddressKey primitive.ObjectID `json:"default_shipping_address_key" bson:"df_shipping_address_key" example:"5efc1cfaf32390ae432febe0"`
	IsActive                  bool               `json:"is_active" bson:"is_active" example:"false"`
	ObjectType                string             `json:"object_type" bson:"object_type" example:"eiusmod"`
	CreatedBy                 primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efc1cfa46e9968f9c6d9d7c"`
	CreatedApplicationKey     primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5efc1cfaaec3d46ad7bf90df"`
	UpdatedBy                 primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efc1cfadbb8358614063344"`
	UpdatedApplicationKey     primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5efc1cfa023ed7deb7430b8c"`
}
