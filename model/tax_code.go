package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type TaxCode struct {
	ID          primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5efc529fbbfa67be2f63b486"`
	TenantKey   primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5f029693a926f8d356588e7f"`
	TaxCodeId   string             `json:"tax_code_id" bson:"tax_code_id" example:"dolor"`
	Description string             `json:"description" bson:"description" example:"velit"`
	TaxCodeType string             `json:"tax_code_type" bson:"tax_code_type" example:"consequat"`
	TaxRate     float64            `json:"tax_rate" bson:"tax_rate" example:"68.569246"`

	IsActive              bool               `json:"is_active" bson:"is_active" example:"false"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"eiusmod"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2016-06-13T05:22:29 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efc1cfa46e9968f9c6d9d7c"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5efc1cfaaec3d46ad7bf90df"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2017-08-14T06:19:22 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efc1cfadbb8358614063344"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5efc1cfa023ed7deb7430b8c"`
}

type AddTaxCode struct {
	TenantKey   primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5f029693a926f8d356588e7f"`
	TaxCodeId   string             `json:"tax_code_id" bson:"tax_code_id" example:"dolor"`
	Description string             `json:"description" bson:"description" example:"velit"`
	TaxCodeType string             `json:"tax_code_type" bson:"tax_code_type" example:"consequat"`
	TaxRate     float64            `json:"tax_rate" bson:"tax_rate" example:"68.569246"`

	IsActive              bool               `json:"is_active" bson:"is_active" example:"false"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"eiusmod"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efc1cfa46e9968f9c6d9d7c"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5efc1cfaaec3d46ad7bf90df"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efc1cfadbb8358614063344"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5efc1cfa023ed7deb7430b8c"`
}
