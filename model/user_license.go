package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type UserLicense struct {
	ID       primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef96d9c5954cbf2ea3cf7af"`
	Email    string             `json:"email" bson:"email" example:"bookerchaney@opticon.com"`
	DateTime time.Time          `json:"date_time" bson:"date_time"`
}

type UserLicenseRequest struct {
	ID       primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef96d9c5954cbf2ea3cf7af"`
	Email    string             `json:"email" bson:"email" example:"bookerchaney@opticon.com"`
}
