package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type QueueIncoming struct {
	ID             primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef30898024ec5034669911e"`
	ApplicationKey primitive.ObjectID `json:"application_key" bson:"application_key" example:"5ef308984a55510e9849d98a"`
	FileName       string             `json:"file_name" bson:"file_name" example:"GetSalesOrders"`
	Status         bool               `json:"status" bson:"status" example:"true"`
	Content        string             `json:"content" bson:"content" example:"true"`
	ErrMsg         string             `json:"err_msg" bson:"ErrMsg" example:"qwera"`
	CreatedDate    time.Time          `json:"created_date" bson:"created_date" example:"true"`
	UpdatedDate    time.Time          `json:"updated_date" bson:"updated_date" example:"true"`
}

type AddQueueIncoming struct {
	ApplicationKey primitive.ObjectID `json:"application_key" bson:"application_key" example:"5ef308984a55510e9849d98a"`
	FileName       string             `json:"file_name" bson:"file_name" example:"GetSalesOrders"`
	Status         bool               `json:"status" bson:"status" example:"true"`
	Content        string             `json:"content" bson:"content" example:"true"`
	ErrMsg         string             `json:"err_msg" bson:"ErrMsg" example:"asdaq"`
}
