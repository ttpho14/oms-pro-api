package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// SalesOrderShippingDetail struct
type SalesOrderShippingDetail struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef2c053f7a34f7669bb8246"`
	DocLineKey            primitive.ObjectID `json:"doc_line_key" bson:"doc_line_key" example:"5ef2c053f7a34f7669bb8246"`
	DocKey                primitive.ObjectID `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246"`
	DocShipStatus         string             `json:"doc_ship_status" bson:"doc_ship_status" example:"Picking, Packing, Transferring, Transit 1, Transit 2"`
	DocShipNumber         string             `json:"doc_ship_number" bson:"doc_ship_number" example:"DOC001"`
	ShipBy                string             `json:"ship_by" bson:"ship_by" example:"0"`
	DeliverSiteKey        primitive.ObjectID `json:"deliver_site_key" bson:"deliver_site_key" example:"5ef2c053f7a34f7669bb8246"`
	Vendor                primitive.ObjectID `json:"vendor" bson:"vendor" example:"5ef2c053f7a34f7669bb8246"`
	DocShipDate           time.Time          `json:"doc_ship_date" bson:"doc_ship_date"  example:"2020-07-11T01:47:04.424Z"`
	EstimateReceiveDate   time.Time          `json:"estimate_receive_date" bson:"estimate_receive_date"  example:"2020-07-11T01:47:04.424Z"`
	ProductKey            primitive.ObjectID `json:"product_key" bson:"product_key" example:"5ef2c053f7a34f7669bb8246"`
	ProductCode           string             `json:"product_code" bson:"product_code" example:"0"`
	Description           string             `json:"description" bson:"description" example:"Description"`
	Quantity              float64            `json:"quantity" bson:"quantity" example:"0"`
	ShippingLabelUrl      string             `json:"shipping_label_url" bson:"shipping_label_url" example:"0"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"type"`
	CreatedDate           time.Time          `json:"created_date" bson:"created_date" example:"2020-07-11T01:47:04.424Z"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedDate           time.Time          `json:"updated_date" bson:"updated_date" example:"2020-07-11T01:47:04.424Z"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
}

// SalesOrderShippingDetailRequest struct
type SalesOrderShippingDetailRequest struct {
	DocLineKey            primitive.ObjectID `json:"doc_line_key" bson:"doc_line_key" example:"5ef2c053f7a34f7669bb8246"`
	DocKey                primitive.ObjectID `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246"`
	DocShipStatus         string             `json:"doc_ship_status" bson:"doc_ship_status" example:"Picking, Packing, Transferring, Transit 1, Transit 2"`
	DocShipNumber         string             `json:"doc_ship_number" bson:"doc_ship_number" example:"DOC001"`
	ShipBy                string             `json:"ship_by" bson:"ship_by" example:"0"`
	DeliverSiteKey        primitive.ObjectID `json:"deliver_site_key" bson:"deliver_site_key" example:"5ef2c053f7a34f7669bb8246"`
	Vendor                primitive.ObjectID `json:"vendor" bson:"vendor" example:"5ef2c053f7a34f7669bb8246"`
	DocShipDate           time.Time          `json:"doc_ship_date" bson:"doc_ship_date"  example:"2020-07-11T01:47:04.424Z"`
	EstimateReceiveDate   time.Time          `json:"estimate_receive_date" bson:"estimate_receive_date"  example:"2020-07-11T01:47:04.424Z"`
	ProductKey            primitive.ObjectID `json:"product_key" bson:"product_key" example:"5ef2c053f7a34f7669bb8246"`
	ProductCode           string             `json:"product_code" bson:"product_code" example:"0"`
	Description           string             `json:"description" bson:"description" example:"Description"`
	Quantity              float64            `json:"quantity" bson:"quantity" example:"0"`
	ShippingLabelUrl      string             `json:"shipping_label_url" bson:"shipping_label_url" example:"0"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"type"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
}
