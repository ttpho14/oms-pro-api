package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type InventoryLog struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5f02aab989bfe6971405ae74"`
	LogDate               time.Time          `json:"log_date" bson:"log_date" example:"2020-07-07T03:22:23.044+00:00"`
	ProductKey            primitive.ObjectID `json:"product_key" bson:"product_key" example:"5f02aab989bfe6971405ae74"`
	ProductId             string             `json:"product_id" bson:"product_id" example:"eiusmod"`
	ProductName           string             `json:"product_name" bson:"product_name" example:"eiusmod"`
	SiteKey               primitive.ObjectID `json:"site_key" bson:"site_key" example:"5f02aab973afa1a45e4562cc"`
	SiteId                string             `json:"site_id" bson:"site_id" example:"eiusmod"`
	SiteName              string             `json:"site_name" bson:"site_name" example:"eiusmod"`
	OnHandQty             int64              `json:"on_hand_qty" bson:"on_hand_qty" example:"230"`
	OnOrderQty            int64              `json:"on_order_qty" bson:"on_order_qty" example:"230"`
	OnCommitQty           int64              `json:"on_commit_qty" bson:"on_commit_qty" example:"230"`
	OnAvailableQty        int64              `json:"on_available_qty" bson:"on_available_qty" example:"230"`
	SourceObjectType      primitive.ObjectID `json:"source_object_type" bson:"source_object_type" example:"5f02aab989bfe6971405ae74"`
	SourceDocKey          primitive.ObjectID `json:"source_doc_key" bson:"source_doc_key" example:"5f02aab989bfe6971405ae74"`
	SourceDocLineKey      primitive.ObjectID `json:"source_doc_line_key" bson:"source_doc_line_key" example:"5f02aab989bfe6971405ae74"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"eiusmod"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2016-06-13T05:22:29 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efc1cfa46e9968f9c6d9d7c"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5efc1cfaaec3d46ad7bf90df"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2017-08-14T06:19:22 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efc1cfadbb8358614063344"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5efc1cfa023ed7deb7430b8c"`
}

type AddInventoryLog struct {
	ProductKey            primitive.ObjectID `json:"product_key" bson:"product_key" example:"5f02aab989bfe6971405ae74"`
	ProductId             string             `json:"product_id" bson:"product_id" example:"eiusmod"`
	ProductName           string             `json:"product_name" bson:"product_name" example:"eiusmod"`
	SiteKey               primitive.ObjectID `json:"site_key" bson:"site_key" example:"5f02aab973afa1a45e4562cc"`
	SiteId                string             `json:"site_id" bson:"site_id" example:"eiusmod"`
	SiteName              string             `json:"site_name" bson:"site_name" example:"eiusmod"`
	OnHandQty             int64              `json:"on_hand_qty" bson:"on_hand_qty" example:"230"`
	OnOrderQty            int64              `json:"on_order_qty" bson:"on_order_qty" example:"230"`
	OnCommitQty           int64              `json:"on_commit_qty" bson:"on_commit_qty" example:"230"`
	OnAvailableQty        int64              `json:"on_available_qty" bson:"on_available_qty" example:"230"`
	SourceObjectType      primitive.ObjectID `json:"source_object_type" bson:"source_object_type" example:"5f02aab989bfe6971405ae74"`
	SourceDocKey          primitive.ObjectID `json:"source_doc_key" bson:"source_doc_key" example:"5f02aab989bfe6971405ae74"`
	SourceDocLineKey      primitive.ObjectID `json:"source_doc_line_key" bson:"source_doc_line_key" example:"5f02aab989bfe6971405ae74"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"eiusmod"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5efc1cfa46e9968f9c6d9d7c"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5efc1cfaaec3d46ad7bf90df"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5efc1cfadbb8358614063344"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5efc1cfa023ed7deb7430b8c"`
}
