package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// SalesInvoiceDetail struct
type SalesInvoiceDetail struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef2c053f7a34f7669bb8246"`
	DocLineKey            primitive.ObjectID `json:"doc_line_key" bson:"doc_line_key" example:"5ef2c053f7a34f7669bb8246"`
	DocKey                primitive.ObjectID `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246"`
	DocLineStatus         string             `json:"doc_ship_status" bson:"doc_ship_status" example:"Picking, Packing, Transferring, Transit 1, Transit 2"`
	ProductKey            primitive.ObjectID `json:"product_key" bson:"product_key" example:"5ef2c053f7a34f7669bb8246"`
	ProductCode           string             `json:"product_code" bson:"product_code" example:"0"`
	Description           string             `json:"description" bson:"description" example:"Description"`
	BaseObjectType        string             `json:"base_object_type" bson:"base_object_type" example:"type"`
	BaseDocLineKey        primitive.ObjectID `json:"base_doc_line_key" bson:"base_doc_line_key" example:"type"`
	BaseDocKey            primitive.ObjectID `json:"base_doc_line" bson:"base_doc_line" example:"type"`
	Quantity              float64            `json:"quantity" bson:"quantity" example:"0"`
	Price                 float64            `json:"price" bson:"price" example:"0"`
	LineDiscountAmount    float64            `json:"line_discount_amount" bson:"line_discount_amount" example:"0"`
	LineDiscountPercent   float64            `json:"line_discount_percent" bson:"line_discount_percent" example:"0"`
	HeaderDiscountAmount  float64            `json:"header_discount_amount" bson:"header_discount_amount" example:"0"`
	PriceAfterDiscount    float64            `json:"price_after_discount" bson:"price_after_discount" example:"0"`
	TaxCodeKey            primitive.ObjectID `json:"tax_code_key" bson:"tax_code_key" example:"5ef2c053f7a34f7669bb8246"`
	TaxRate               float64            `json:"tax_rate" bson:"tax_rate" example:"0"`
	PriceAfterTax         float64            `json:"price_after_rate" bson:"price_after_rate" example:"0"`
	SiteKey               primitive.ObjectID `json:"origin_site_key" bson:"origin_site_key" example:"5ef2c053f7a34f7669bb8246"`
	Remark                string             `json:"remark" bson:"remark" example:"Remark"`
	ShippingAddressKey    primitive.ObjectID `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053f7a34f7669bb8246"`
	ShippingAddress       string             `json:"shipping_address" bson:"shipping_address" example:"908 Juliana Place, Chautauqua, Montana, 1055"`
	BillingAddressKey     primitive.ObjectID `json:"bill_address_key" bson:"bill_address_key" example:"5ef2c053f7a34f7669bb8246"`
	BillingAddress        string             `json:"bill_address" bson:"bill_address" example:"908 Juliana Place, Chautauqua, Montana, 1055"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"type"`
	CreatedDate           time.Time          `json:"created_date" bson:"created_date" example:"2016-07-19T04:53:39 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedDate           time.Time          `json:"updated_date" bson:"updated_date" example:"2016-07-19T04:53:39 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"false"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
}

// AddSalesInvoiceDetail struct
type AddSalesInvoiceDetail struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef2c053f7a34f7669bb8246"`
	DocLineKey            primitive.ObjectID `json:"doc_line_key" bson:"doc_line_key" example:"5ef2c053f7a34f7669bb8246"`
	DocKey                primitive.ObjectID `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246"`
	DocLineStatus         string             `json:"doc_ship_status" bson:"doc_ship_status" example:"Picking, Packing, Transferring, Transit 1, Transit 2"`
	ProductKey            primitive.ObjectID `json:"product_key" bson:"product_key" example:"5ef2c053f7a34f7669bb8246"`
	ProductCode           string             `json:"product_code" bson:"product_code" example:"0"`
	Description           string             `json:"description" bson:"description" example:"Description"`
	BaseObjectType        string             `json:"base_object_type" bson:"base_object_type" example:"type"`
	BaseDocLineKey        primitive.ObjectID `json:"base_doc_line_key" bson:"base_doc_line_key" example:"type"`
	BaseDocKey            primitive.ObjectID `json:"base_doc_line" bson:"base_doc_line" example:"type"`
	Quantity              float64            `json:"quantity" bson:"quantity" example:"0"`
	Price                 float64            `json:"price" bson:"price" example:"0"`
	LineDiscountAmount    float64            `json:"line_discount_amount" bson:"line_discount_amount" example:"0"`
	LineDiscountPercent   float64            `json:"line_discount_percent" bson:"line_discount_percent" example:"0"`
	HeaderDiscountAmount  float64            `json:"header_discount_amount" bson:"header_discount_amount" example:"0"`
	PriceAfterDiscount    float64            `json:"price_after_discount" bson:"price_after_discount" example:"0"`
	TaxCodeKey            primitive.ObjectID `json:"tax_code_key" bson:"tax_code_key" example:"5ef2c053f7a34f7669bb8246"`
	TaxRate               float64            `json:"tax_rate" bson:"tax_rate" example:"0"`
	PriceAfterTax         float64            `json:"price_after_rate" bson:"price_after_rate" example:"0"`
	SiteKey               primitive.ObjectID `json:"origin_site_key" bson:"origin_site_key" example:"5ef2c053f7a34f7669bb8246"`
	Remark                string             `json:"remark" bson:"remark" example:"Remark"`
	ShippingAddressKey    primitive.ObjectID `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053f7a34f7669bb8246"`
	ShippingAddress       string             `json:"shipping_address" bson:"shipping_address" example:"908 Juliana Place, Chautauqua, Montana, 1055"`
	BillingAddressKey     primitive.ObjectID `json:"bill_address_key" bson:"bill_address_key" example:"5ef2c053f7a34f7669bb8246"`
	BillingAddress        string             `json:"bill_address" bson:"bill_address" example:"908 Juliana Place, Chautauqua, Montana, 1055"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"type"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"false"`
}
