package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type PriceList struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef31b7066d8cb5b0de85a36"`
	PriceListId           string             `json:"price_list_id" bson:"price_list_id" example:"est eiusmod"`
	Description           string             `json:"description" bson:"description" example:"elit deserunt"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5ef31b70fa815890c2e14b24"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"true"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"esse"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2014-11-02T10:37:29 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef31b70a4c50e67fb0a973d"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef31b70fd38ad08761121f4"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2017-07-23T09:10:43 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef31b70a7cd2af74d63b050"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef31b705391e02cb8965012"`
}

type PriceListRequest struct {
	PriceListId           string             `json:"price_list_id" bson:"price_list_id" example:"est eiusmod"`
	Description           string             `json:"description" bson:"description" example:"elit deserunt"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5ef31b70fa815890c2e14b24"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"true"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"esse"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef31b70a4c50e67fb0a973d"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef31b70fd38ad08761121f4"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef31b70a7cd2af74d63b050"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef31b705391e02cb8965012"`
}
