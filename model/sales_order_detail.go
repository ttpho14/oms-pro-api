package model

import (
	db "../database"
	"../model/enum"
	"../setting"
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// SalesOrderDetail struct
type SalesOrderDetail struct {
	ID                    primitive.ObjectID   `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	DocKey                primitive.ObjectID   `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	DocNum                string               `json:"doc_num" bson:"doc_num" example:"DocNum" validate:"required"`
	DocLineNum            string               `json:"doc_line_num" bson:"doc_line_num" example:"DocLineNum" validate:"required"`
	DocLineStatus         enum.OrderLineStatus `json:"doc_line_status" bson:"doc_line_status" example:"Picking, Packing, Transferring, Transit 1, Transit 2" validate:"required"`
	ProductKey            primitive.ObjectID   `json:"product_key" bson:"product_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	ProductCode           string               `json:"product_code" bson:"product_code" example:"0" validate:"required"`
	ProductSize           string               `json:"product_size" bson:"product_size" example:"XL" validate:"required"`
	ProductColor          string               `json:"product_color" bson:"product_color" example:"red" validate:"required"`
	ProductUom            string               `json:"product_uom" bson:"product_uom" example:"eiusmod"`
	Description           string               `json:"description" bson:"description" example:"Description" validate:"required"`
	BrandKey              primitive.ObjectID   `json:"brand_key" bson:"brand_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	BrandId               string               `json:"brand_id" bson:"brand_id" example:"ABC" validate:"required"`
	BaseObjectType        string               `json:"base_object_type" bson:"base_object_type" example:"type" validate:"required"`
	BaseDocLineKey        primitive.ObjectID   `json:"base_doc_line_key" bson:"base_doc_line_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	BaseDocKey            primitive.ObjectID   `json:"base_doc_key" bson:"base_doc_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	Quantity              float64              `json:"quantity" bson:"quantity" example:"0" validate:"required"`
	Price                 float64              `json:"price" bson:"price" example:"0" validate:"required"`
	CancelQuantity        float64              `json:"cancel_quantity" bson:"cancel_quantity" example:"0" validate:"required"`
	ReturnQuantity        float64              `json:"return_quantity" bson:"return_quantity" example:"0" validate:"required"`
	OpenQuantity          float64              `json:"open_quantity" bson:"open_quantity" example:"0" validate:"required"`
	ShippedQuantity       float64              `json:"shipped_quantity" bson:"shipped_quantity" example:"0" validate:"required"`
	FreightAmount         float64              `json:"freight_amount" bson:"freight_amount" example:"0" validate:"required"`
	LineDiscountAmount    float64              `json:"line_discount_amount" bson:"line_discount_amount" example:"0" validate:"required"`
	LineDiscountPercent   float64              `json:"line_discount_percent" bson:"line_discount_percent" example:"0" validate:"required"`
	HeaderDiscountAmount  float64              `json:"header_discount_amount" bson:"header_discount_amount" example:"0" validate:"required"`
	PriceAfterDiscount    float64              `json:"price_after_discount" bson:"price_after_discount" example:"0" validate:"required"`
	PaymentStatus         string               `json:"payment_status" bson:"payment_status" example:"Pending, Partial, Complete,.." validate:"required"`
	ShippingStatus        string               `json:"shipping_status" bson:"shipping_status" example:"Pending, Partial, Complete,.." validate:"required"`
	TaxCodeKey            primitive.ObjectID   `json:"tax_code_key" bson:"tax_code_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	TaxCodeId             string               `json:"tax_code_id" bson:"tax_code_id" example:"Tax001" validate:"required"`
	TaxRate               float64              `json:"tax_rate" bson:"tax_rate" example:"0" validate:"required"`
	PriceAfterTax         float64              `json:"price_after_tax" bson:"price_after_tax" example:"0" validate:"required"`
	OriginSiteKey         primitive.ObjectID   `json:"origin_site_key" bson:"origin_site_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	IsItemExchange        bool                 `json:"is_item_exchange" bson:"is_item_exchange" example:"false" validate:"required"`
	Remark                string               `json:"remark" bson:"remark" example:"Remark" validate:"required"`
	AddValue              string               `json:"add_value" bson:"add_value" example:"type" validate:"required"`
	ShippingAddressKey    primitive.ObjectID   `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	ShippingAddress       ShippingAddress      `json:"shipping_address" bson:"shipping_address"`
	BillingAddressKey     primitive.ObjectID   `json:"billing_address_key" bson:"billing_address_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	BillingAddress        BillingAddress       `json:"billing_address" bson:"billing_address"`
	Weight                float64              `json:"weight" bson:"weight" example:"35674155180528"`
	Volume                float64              `json:"volume" bson:"volume" example:"35674155180528"`
	BatchNo               string               `json:"batch_no" bson:"batch_no" example:"35674155180528"`
	Reason                string               `json:"reason" bson:"reason" example:"35674155180528"`
	ImageUrl              string               `json:"image_url" bson:"image_url" example:""`
	ObjectType            string               `json:"object_type" bson:"object_type" example:"type" validate:"required"`
	CreatedDate           time.Time            `json:"created_date" bson:"created_date" example:"2020-07-11T01:47:04.424Z" validate:"required"`
	CreatedBy             primitive.ObjectID   `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0" validate:"required"`
	CreatedApplicationKey primitive.ObjectID   `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04" validate:"required"`
	UpdatedDate           time.Time            `json:"updated_date" bson:"updated_date" example:"2020-07-11T01:47:04.424Z" validate:"required"`
	UpdatedBy             primitive.ObjectID   `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0" validate:"required"`
	UpdatedApplicationKey primitive.ObjectID   `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04" validate:"required"`
}

// SalesOrderDetailRequest struct
type SalesOrderDetailRequest struct {
	DocKey                primitive.ObjectID `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	DocNum                string             `json:"doc_num" bson:"doc_num" example:"DocNum" validate:"required"`
	DocLineNum            string             `json:"doc_line_num" bson:"doc_line_num" example:"DocLineNum" validate:"required"`
	ProductCode           string             `json:"product_code" bson:"product_code" example:"0" validate:"required"`
	Description           string             `json:"description" bson:"description" example:"Description" validate:"required"`
	BrandId               string             `json:"brand_id" bson:"brand_id" example:"ABC" validate:"required"`
	BaseObjectType        string             `json:"base_object_type" bson:"base_object_type" example:"type" validate:"required"`
	BaseDocLineKey        primitive.ObjectID `json:"base_doc_line_key" bson:"base_doc_line_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	BaseDocKey            primitive.ObjectID `json:"base_doc_key" bson:"base_doc_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	Quantity              float64            `json:"quantity" bson:"quantity" example:"0" validate:"required"`
	Price                 float64            `json:"price" bson:"price" example:"0" validate:"required"`
	CancelQuantity        float64            `json:"cancel_quantity" bson:"cancel_quantity" example:"0" validate:"required"`
	ReturnQuantity        float64            `json:"return_quantity" bson:"return_quantity" example:"0" validate:"required"`
	OpenQuantity          float64            `json:"open_quantity" bson:"open_quantity" example:"0" validate:"required"`
	ShippedQuantity       float64            `json:"shipped_quantity" bson:"shipped_quantity" example:"0" validate:"required"`
	FreightAmount         float64            `json:"freight_amount" bson:"freight_amount" example:"0" validate:"required"`
	LineDiscountAmount    float64            `json:"line_discount_amount" bson:"line_discount_amount" example:"0" validate:"required"`
	LineDiscountPercent   float64            `json:"line_discount_percent" bson:"line_discount_percent" example:"0" validate:"required"`
	HeaderDiscountAmount  float64            `json:"header_discount_amount" bson:"header_discount_amount" example:"0" validate:"required"`
	PriceAfterDiscount    float64            `json:"price_after_discount" bson:"price_after_discount" example:"0" validate:"required"`
	PaymentStatus         string             `json:"payment_status" bson:"payment_status" example:"Pending, Partial, Complete,.." validate:"required"`
	ShippingStatus        string             `json:"shipping_status" bson:"shipping_status" example:"Pending, Partial, Complete,.." validate:"required"`
	TaxCodeId             string             `json:"tax_code_id" bson:"tax_code_id" example:"Tax0001" validate:"required"`
	TaxRate               float64            `json:"tax_rate" bson:"tax_rate" example:"0" validate:"required"`
	PriceAfterTax         float64            `json:"price_after_tax" bson:"price_after_tax" example:"0" validate:"required"`
	OriginSiteKey         primitive.ObjectID `json:"origin_site_key" bson:"origin_site_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	IsItemExchange        bool               `json:"is_item_exchange" bson:"is_item_exchange" example:"false" validate:"required"`
	Remark                string             `json:"remark" bson:"remark" example:"Remark" validate:"required"`
	AddValue              string             `json:"add_value" bson:"add_value" example:"type" validate:"required"`
	ShippingAddressKey    primitive.ObjectID `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	ShippingAddress       ShippingAddress    `json:"shipping_address" bson:"shipping_address"`
	BillingAddressKey     primitive.ObjectID `json:"billing_address_key" bson:"billing_address_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	BillingAddress        BillingAddress     `json:"billing_address" bson:"billing_address"`
	ImageUrl              string             `json:"image_url" bson:"image_url" example:""`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"type" validate:"required"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0" validate:"required"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0" validate:"required"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053f7a34f7669bb8246" validate:"required"`
	//DocLineStatus         enum.OrderLineStatus `json:"doc_line_status" bson:"doc_line_status" example:"Picking, Packing, Transferring, Transit 1, Transit 2" validate:"required"`
}

type SalesOrderDetailUpdateRequest struct {
	Weight          float64              `json:"weight,omitempty" bson:"weight,omitempty" example:"35674155180528"`
	Volume          float64              `json:"volume,omitempty" bson:"volume,omitempty" example:"35674155180528"`
	BatchNo         string               `json:"batch_no,omitempty" bson:"batch_no,omitempty" example:"35674155180528"`
	Reason          string               `json:"reason,omitempty" bson:"reason,omitempty" example:"35674155180528"`
	DocLineNum      string               `json:"doc_line_num,omitempty" bson:"doc_line_num,omitempty" example:"35674155180528"`
	DocLineStatus   enum.OrderLineStatus `json:"doc_line_status,omitempty" bson:"doc_line_status,omitempty" example:"NEW"`
	PaymentStatus   enum.PaymentStatus   `json:"payment_status,omitempty" bson:"payment_status,omitempty" example:"NEW"`
	ShippingStatus  enum.ShippingStatus  `json:"shipping_status,omitempty" bson:"shipping_status,omitempty" example:"NEW"`
	CancelQuantity  float64              `json:"cancel_quantity,omitempty" bson:"cancel_quantity,omitempty"`
	ReturnQuantity  float64              `json:"return_quantity,omitempty" bson:"return_quantity,omitempty"`
	ShippedQuantity float64              `json:"shipped_quantity,omitempty" bson:"shipped_quantity,omitempty"`
}

func (sD SalesOrderDetailRequest) Validation() (err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	count, err := db.Database.Collection(setting.ProductTable).CountDocuments(ctx, bson.M{"product_id": sD.ProductCode})
	if err != nil {
		return errors.New("Count Product errors " + err.Error())
	}
	if count == 0 {
		return errors.New("There is no Product Code in Database !! ")
	}

	countInventory, err := db.Database.Collection(setting.InventoryStatusTable).CountDocuments(ctx, bson.M{"product_id": sD.ProductCode})
	if err != nil {
		return errors.New("Count Product errors " + err.Error())
	}
	if countInventory == 0 {
		return errors.New("There is no Product Code in Inventory Status !! ")
	}

	return nil
}
