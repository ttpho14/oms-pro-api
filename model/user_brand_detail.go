package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type UserBrandDetail struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef96d9c5954cbf2ea3cf7af"`
	UserKey               primitive.ObjectID `json:"user_key" bson:"user_key" example:"5efc529f6d3c966b4d59d5b8"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5efc529f6d3c966b4d59d5b8"`
	BrandKey              primitive.ObjectID `json:"brand_key" bson:"brand_key" example:"5efc529f6d3c966b4d59d5b8"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"est"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2019-08-14T12:02:56 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef96db2dc3410a231844080"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef96db2088ec88b6b05e1d5"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2020-05-12T05:33:55 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef96db23c574ffde7644a58"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef96db231451247fd91958e"`
	IsSystem              bool               `json:"is_system" bson:"is_system" example:"false"`
	IsTenantAdmin         bool               `json:"is_tenant_admin" bson:"is_tenant_admin" example:"true"`
}

type AddUserBrandDetail struct {
	UserKey               primitive.ObjectID `json:"user_key" bson:"user_key" example:"5efc529f6d3c966b4d59d5b8"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5efc529f6d3c966b4d59d5b8"`
	BrandKey              primitive.ObjectID `json:"brand_key" bson:"brand_key" example:"5efc529f6d3c966b4d59d5b8"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"est"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2019-08-14T12:02:56 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef96db2dc3410a231844080"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef96db2088ec88b6b05e1d5"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef96db23c574ffde7644a58"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef96db231451247fd91958e"`
	IsSystem              bool               `json:"is_system" bson:"is_system" example:"false"`
	IsTenantAdmin         bool               `json:"is_tenant_admin" bson:"is_tenant_admin" example:"true"`
}
