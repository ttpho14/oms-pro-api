package model

import (
	"../database"
	"../setting"
	"./enum"
	"context"
	"errors"
	"fmt"
	"github.com/go-openapi/strfmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type User struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef96d9c5954cbf2ea3cf7af"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5efeb035fdc0d9f28a6846c2"`
	UserId                string             `json:"user_id" bson:"user_id" example:"nisi"`
	UserName              string             `json:"user_name" bson:"user_name" example:"ipsum"`
	Password              string             `json:"password" bson:"password" example:"culpa"`
	Email                 string             `json:"email" bson:"email" example:"bookerchaney@opticon.com"`
	Photo                 string             `json:"photo" bson:"photo" example:"5ef301393137b76125b31f9c"`
	SecurityRoleKey       primitive.ObjectID `json:"security_role_key" bson:"security_role_key" example:"5ef96db21ba8581c09597642"`
	ChangePassword        bool               `json:"change_password" bson:"change_password" example:"true"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"est"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2019-08-14T12:02:56 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef96db2dc3410a231844080"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef96db2088ec88b6b05e1d5"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2020-05-12T05:33:55 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef96db23c574ffde7644a58"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef96db231451247fd91958e"`
	IsSystem              bool               `json:"is_system" bson:"is_system" example:"false"`
	IsTenantAdmin         bool               `json:"is_tenant_admin" bson:"is_tenant_admin" example:"true"`
}

type UserRequest struct {
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5efeb035fdc0d9f28a6846c2"`
	UserId                string             `json:"user_id" bson:"user_id" example:"nisi"`
	UserName              string             `json:"user_name" bson:"user_name" example:"ipsum"`
	Password              string             `json:"password" bson:"password" example:"culpa"`
	Email                 string             `json:"email" bson:"email" example:"bookerchaney@opticon.com"`
	Photo                 string             `json:"photo" bson:"photo" example:"5ef301393137b76125b31f9c"`
	SecurityRoleKey       primitive.ObjectID `json:"security_role_key" bson:"security_role_key" example:"5ef96db21ba8581c09597642"`
	ChangePassword        bool               `json:"change_password" bson:"change_password" example:"true"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"est"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef96db2dc3410a231844080"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef96db2088ec88b6b05e1d5"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef96db23c574ffde7644a58"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef96db231451247fd91958e"`
	IsSystem              bool               `json:"is_system" bson:"is_system" example:"false"`
	IsTenantAdmin         bool               `json:"is_tenant_admin" bson:"is_tenant_admin" example:"true"`
}

type UserChangePassword struct {
	Email           string `json:"email" bson:"email"`
	CurrentPassword string `json:"current_password" bson:"current_password"`
	NewPassword     string `json:"new_password" bson:"new_password"`
}

type UserResetPassword struct {
	Email string `json:"email" bson:"email"`
}

type UserLogin struct {
	Email    string `json:"email" bson:"email"`
	Password string `json:"password" bson:"password"`
}

type UserLogout struct {
	Email    string `json:"email" bson:"email"`
}

func OmsUserInitialize() *User {
	currentTime := time.Now()

	return &User{
		ID:                    primitive.NewObjectID(),
		TenantKey:             primitive.NilObjectID,
		UserId:                enum.AppIdOMS,
		UserName:              enum.AppIdOMS,
		Password:              "FGHIJK",
		Email:                 "oms@productaddon.com",
		Photo:                 "",
		SecurityRoleKey:       primitive.NilObjectID,
		IsActive:              true,
		IsDeleted:             false,
		ObjectType:            "user",
		CreatedDate:           currentTime,
		CreatedBy:             primitive.NilObjectID,
		CreatedApplicationKey: primitive.NilObjectID,
		UpdatedDate:           currentTime,
		UpdatedBy:             primitive.NilObjectID,
		UpdatedApplicationKey: primitive.NilObjectID,
		IsSystem:              true,
		IsTenantAdmin:         true,
	}
}

func (uReq UserRequest) Validation() (err error) {
	//Handle Register
	if !strfmt.IsEmail(uReq.Email) {
		return errors.New("Email is invalid ")
	}

	_, err = uReq.IsEmailExists()
	if err != nil {
		return errors.New("Check Email ERROR - " + err.Error())
	}

	return nil
}

func (uReq UserRequest) IsEmailExists() (b bool, err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	count, err := database.Database.Collection(setting.UserTable).CountDocuments(ctx, bson.M{"email": uReq.Email, "is_deleted": false})
	if err != nil {
		return false, errors.New("Count document ERROR - " + err.Error())
	}

	if count == 0 {
		return false, errors.New(fmt.Sprintf("Email '%s' doesn't exists ", uReq.Email))
	}

	return true, nil
}
