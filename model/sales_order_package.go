package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// SalesOrderPackage struct
type SalesOrderPackage struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef2c053f7a34f7669bb8246"`
	DocKey                primitive.ObjectID `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246"`
	DocNum                string             `json:"doc_num" bson:"doc_num" example:"doc num"`
	PackageNo             string             `json:"package_no" bson:"package_no" example:"laboris"`
	PackageType           string             `json:"package_type" bson:"package_type" example:"laboris"`
	CommodityType         string             `json:"commodity_type" bson:"commodity_type" example:"laboris"`
	Length                float64            `json:"length" bson:"length" example:"1"`
	Width                 float64            `json:"width" bson:"width" example:"1"`
	Height                float64            `json:"height" bson:"height" example:"1"`
	Weight                float64            `json:"weight" bson:"weight" example:"2"`
	Value                 float64            `json:"value" bson:"value" example:"3"`
	Instruction           string             `json:"instruction" bson:"instruction" example:"laboris"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"type"`
	CreatedDate           time.Time          `json:"created_date" bson:"created_date" example:"2016-07-19T04:53:39 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedDate           time.Time          `json:"updated_date" bson:"updated_date" example:"2016-07-19T04:53:39 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
}

// SalesOrderPackageRequest struct
type SalesOrderPackageRequest struct {
	DocKey                primitive.ObjectID `json:"doc_key" bson:"doc_key" example:"5ef2c053f7a34f7669bb8246"`
	DocNum                string             `json:"doc_num" bson:"doc_num" example:"5ef2c053f7a34f7669bb8246"`
	PackageNo             string             `json:"package_no" bson:"package_no" example:"laboris"`
	PackageType           string             `json:"package_type" bson:"package_type" example:"laboris"`
	CommodityType         string             `json:"commodity_type" bson:"commodity_type" example:"laboris"`
	Length                float64            `json:"length" bson:"length" example:"1"`
	Width                 float64            `json:"width" bson:"width" example:"1"`
	Height                float64            `json:"height" bson:"height" example:"1"`
	Weight                float64            `json:"weight" bson:"weight" example:"2"`
	Value                 float64            `json:"value" bson:"value" example:"3"`
	Instruction           string             `json:"instruction" bson:"instruction" example:"laboris"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
}

// SalesOrderPackageRequest struct
type SalesOrderPackageUpdateRequest struct {
	PackageType   string  `json:"package_type,omitempty" bson:"package_type,omitempty" example:"laboris"`
	CommodityType string  `json:"commodity_type,omitempty" bson:"commodity_type,omitempty" example:"laboris"`
	Length        float64 `json:"length,omitempty" bson:"length,omitempty" example:"1"`
	Width         float64 `json:"width,omitempty" bson:"width,omitempty" example:"2"`
	Height        float64 `json:"height,omitempty" bson:"height,omitempty" example:"3"`
	Weight        float64 `json:"weight,omitempty" bson:"weight,omitempty" example:"4"`
	Value         float64 `json:"value,omitempty" bson:"value,omitempty" example:"2"`
	Instruction   string  `json:"instruction,omitempty" bson:"instruction,omitempty" example:"laboris"`
}
