package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type TenantSetting struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef309d05787c3fc462328b0"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5ef309d05787c3fc462328b0"`
	SettingId             string             `json:"setting_id" bson:"setting_id" example:"quis"`
	SettingValue          string             `json:"setting_value" bson:"setting_value" example:"adagz"`
	Description           string             `json:"description" bson:"description" example:"quis"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"id"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2019-12-28T10:40:48 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef309d0e15a6882e4313243"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef309d076f6ba80bbb65827"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2020-02-28T10:40:48 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef309d0cf592f2a6dbeb21d"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef309d0f585952856ad0d93"`
}

type TenantSettingRequest struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef309d05787c3fc462328b0"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5ef309d05787c3fc462328b0"`
	SettingId             string             `json:"setting_id" bson:"setting_id" example:"quis"`
	SettingValue          string             `json:"setting_value" bson:"setting_value" example:"adagz"`
	Description           string             `json:"description" bson:"description" example:"quis"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"id"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef309d0e15a6882e4313243"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef309d076f6ba80bbb65827"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef309d0cf592f2a6dbeb21d"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef309d0f585952856ad0d93"`
}
