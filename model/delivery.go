package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Delivery struct
type Delivery struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef96d9c5954cbf2ea3cf7af"`
	DocKey                primitive.ObjectID `json:"doc_key" bson:"doc_key" example:"5efc6f5f34714ae957380f49"`
	DocNum                string             `json:"doc_num" bson:"doc_num" example:"Doc001"`
	RefNo                 string             `json:"ref_no" bson:"ref_no" example:"123456"`
	CustomerKey           primitive.ObjectID `json:"customer_key" bson:"customer_key" example:"5efc6f5fa74d733d55d7a390"`
	CustomerName          string             `json:"customer_name" bson:"customer_name" example:"James"`
	DocDate               time.Time          `json:"doc_date" bson:"address_2" example:"2016-07-19T04:53:39 -07:00"`
	DocDueDate            time.Time          `json:"doc_due_date" bson:"doc_due_date"  example:"2016-07-19T04:53:39 -07:00"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5ef2c053d86e577333da9b04"`
	CurrencyKey           primitive.ObjectID `json:"currency_key" bson:"currency_key" example:"USD"`
	CurrencyRate          float64            `json:"currency_rate" bson:"currency_rate" example:"1"`
	TotalBeforeDiscount   float64            `json:"total_before_discount" bson:"total_before_discount" example:"0"`
	DiscountAmount        float64            `json:"discount_amount" bson:"discount_amount" example:"0"`
	DiscountPercent       float64            `json:"discount_percent" bson:"discount_percent" example:"0"`
	TotalAfterDiscount    float64            `json:"total_after_discount" bson:"total_after_discount" example:"0"`
	FreightAmount         float64            `json:"freight_amount" bson:"freight_amount" example:"0"`
	TaxAmount             float64            `json:"tax_amount" bson:"tax_amount" example:"0"`
	TaxRate               float64            `json:"tax_rate" bson:"tax_rate" example:"0"`
	TotalAfterTax         float64            `json:"total_after_tax" bson:"total_after_tax" example:"0"`
	PayTaxAmount          float64            `json:"pay_tax_amount" bson:"pay_tax_amount" example:"0"`
	PayTaxRate            float64            `json:"pay_tax_rate" bson:"pay_tax_rate" example:"0"`
	PaidAmount            float64            `json:"paid_amount" bson:"paid_amount" example:"0"`
	BalanceAmount         float64            `json:"balance_amount" bson:"balance_amount" example:"0"`
	DocStatus             string             `json:"doc_status" bson:"doc_status" example:"Normal, Staff, Test, Exchange etc."`
	PaymentStatus         string             `json:"payment_status" bson:"payment_status" example:"Pending, Partial, Complete,.."`
	Remark                string             `json:"remark" bson:"remark" example:"Remark"`
	AddValue              string             `json:"add_value" bson:"add_value" example:"Gift Notes, instruction"`
	BillingAddressKey     primitive.ObjectID `json:"billing_address_key" bson:"billing_address_key" example:"5ef2c053d86e577333da9b04"`
	BillingAddress        string             `json:"billing_address" bson:"billing_address" example:"908 Juliana Place, Chautauqua, Montana, 1055"`
	ShippingAddressKey    primitive.ObjectID `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053d86e577333da9b04"`
	ShippingAddress       string             `json:"shipping_address" bson:"shipping_address" example:"908 Juliana Place, Chautauqua, Montana, 1055"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"type"`
	CreatedDate           time.Time          `json:"created_date" bson:"created_date" example:"2016-07-19T04:53:39 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedDate           time.Time          `json:"updated_date" bson:"updated_date" example:"2016-07-19T04:53:39 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicateKey   primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"false"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
}

// AddDelivery struct
type AddDelivery struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef96d9c5954cbf2ea3cf7af"`
	DocKey                primitive.ObjectID `json:"doc_key" bson:"doc_key" example:"5efc6f5f34714ae957380f49"`
	DocNum                string             `json:"doc_num" bson:"doc_num" example:"Doc001"`
	RefNo                 string             `json:"ref_no" bson:"ref_no" example:"123456"`
	CustomerKey           primitive.ObjectID `json:"customer_key" bson:"customer_key" example:"5efc6f5fa74d733d55d7a390"`
	CustomerName          string             `json:"customer_name" bson:"customer_name" example:"James"`
	DocDate               time.Time          `json:"doc_date" bson:"address_2" example:"2016-07-19T04:53:39 -07:00"`
	DocDueDate            time.Time          `json:"doc_due_date" bson:"doc_due_date"  example:"2016-07-19T04:53:39 -07:00"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5ef2c053d86e577333da9b04"`
	CurrencyKey           primitive.ObjectID `json:"currency_key" bson:"currency_key" example:"USD"`
	CurrencyRate          float64            `json:"currency_rate" bson:"currency_rate" example:"1"`
	TotalBeforeDiscount   float64            `json:"total_before_discount" bson:"total_before_discount" example:"0"`
	DiscountAmount        float64            `json:"discount_amount" bson:"discount_amount" example:"0"`
	DiscountPercent       float64            `json:"discount_percent" bson:"discount_percent" example:"0"`
	TotalAfterDiscount    float64            `json:"total_after_discount" bson:"total_after_discount" example:"0"`
	FreightAmount         float64            `json:"freight_amount" bson:"freight_amount" example:"0"`
	TaxAmount             float64            `json:"tax_amount" bson:"tax_amount" example:"0"`
	TaxRate               float64            `json:"tax_rate" bson:"tax_rate" example:"0"`
	TotalAfterTax         float64            `json:"total_after_tax" bson:"total_after_tax" example:"0"`
	PayTaxAmount          float64            `json:"pay_tax_amount" bson:"pay_tax_amount" example:"0"`
	PayTaxRate            float64            `json:"pay_tax_rate" bson:"pay_tax_rate" example:"0"`
	PaidAmount            float64            `json:"paid_amount" bson:"paid_amount" example:"0"`
	BalanceAmount         float64            `json:"balance_amount" bson:"balance_amount" example:"0"`
	DocStatus             string             `json:"doc_status" bson:"doc_status" example:"Normal, Staff, Test, Exchange etc."`
	PaymentStatus         string             `json:"payment_status" bson:"payment_status" example:"Pending, Partial, Complete,.."`
	Remark                string             `json:"remark" bson:"remark" example:"Remark"`
	AddValue              string             `json:"add_value" bson:"add_value" example:"Gift Notes, instruction"`
	BillingAddressKey     primitive.ObjectID `json:"billing_address_key" bson:"billing_address_key" example:"5ef2c053d86e577333da9b04"`
	BillingAddress        string             `json:"billing_address" bson:"billing_address" example:"908 Juliana Place, Chautauqua, Montana, 1055"`
	ShippingAddressKey    primitive.ObjectID `json:"shipping_address_key" bson:"shipping_address_key" example:"5ef2c053d86e577333da9b04"`
	ShippingAddress       string             `json:"shipping_address" bson:"shipping_address" example:"908 Juliana Place, Chautauqua, Montana, 1055"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"type"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef2c053d75b3ce26f8a50c0"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef2c053d86e577333da9b04"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef2c053d75b3ce26f8a50c0"`
	UpdatedApplicateKey   primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef2c053d86e577333da9b04"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"false"`

	AddDeliveryDetail  []AddDeliveryDetail  `json:"delivery_details" bson:"delivery_details"`
	AddDeliveryFreight []AddDeliveryFreight `json:"delivery_freights" bson:"delivery_freights"`
}
