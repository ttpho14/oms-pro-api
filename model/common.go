package model

type UpdatedResponse struct {
	MatchedCount  int    `json:"MathchedCount" bson:"MathchedCount" example:"1"`
	ModifiedCount int    `json:"ModifiedCount" bson:"ModifiedCount" example:"1"`
	UpsertedCount int    `json:"UpsertedCount" bson:"UpsertedCount" example:"0"`
	UpsertedID    string `json:"UpsertedID" bson:"UpsertedID" example: "null"`
}

type ResponseResult struct {
	Success   bool        `json:"success"`
	Message   string      `json:"message"`
	ErrorCode int         `json:"error_code"`
	Data      interface{} `json:"data"`
}

type Field struct {
	FieldName string `json:"field_name"`
	DataType  string `json:"data_type"`
}

type Count struct {
	Count int64 `json:"count" bson:"count" example:"12"`
}
