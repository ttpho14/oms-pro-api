package model

import (
	db "../database"
	"../setting"
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Queue struct {
	ID                primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef30898024ec5034669911e"`
	FromApplicationId string             `json:"from_application_id" bson:"from_application_id" example:"YCH"`
	ToApplicationId   string             `json:"to_application_id" bson:"to_application_id" example:"OMS"`
	SourceObjectType  string             `json:"source_object_type" bson:"source_object_type" example:"sales_order"`
	SourceObjectId    string             `json:"source_object_id" bson:"source_object_id" example:"TRX000121"`
	JsonData          string             `json:"json_data" bson:"json_data"`
	SendData          string             `json:"send_data" bson:"send_data"`
	ResponseData      string             `json:"response_data" bson:"response_data"`
	RetryTimes        int64              `json:"retry_times" bson:"retry_times"`
	RetryDays         int64              `json:"retry_days" bson:"retry_days"`
	Status            bool               `json:"status" bson:"status" example:"false"`
	ErrorMessage      string             `json:"error_message" bson:"error_message" example:"uqdoca"`
	CreatedDate       time.Time          `json:"created_date" bson:"created_date" example:"true"`
	UpdatedDate       time.Time          `json:"updated_date" bson:"updated_date" example:"true"`
}

type QueueRequest struct {
	FromApplicationId string `json:"from_application_id,omitempty" bson:"from_application_id,omitempty" example:"5ef308984a55510e9849d98a"`
	ToApplicationId   string `json:"to_application_id,omitempty" bson:"to_application_id,omitempty" example:"5ef308984a55510e9849d98a"`
	SourceObjectType  string `json:"source_object_type,omitempty" bson:"source_object_type,omitempty" example:"aliqua"`
	SourceObjectId    string `json:"source_object_id,omitempty" bson:"source_object_id,omitempty" example:"5ef308984a55510e9849d98a"`
	JsonData          string `json:"json_data,omitempty" bson:"json_data,omitempty"`
	SendData          string `json:"send_data,omitempty" bson:"send_data,omitempty"`
	RetryTimes        int64  `json:"retry_times,omitempty" bson:"retry_times,omitempty"`
	RetryDays         int64  `json:"retry_days,omitempty" bson:"retry_days,omitempty"`
	ResponseData      string `json:"response_data,omitempty" bson:"response_data,omitempty"`
	Status            bool   `json:"status,omitempty" bson:"status,omitempty" example:"false"`
	ErrorMessage      string `json:"error_message,omitempty" bson:"error_message,omitempty" example:"uqdoca"`
}

func (qReq QueueRequest) IsExistsData() (err error) {
	filter := bson.M{
		"from_application_id": qReq.FromApplicationId,
		"to_application_id":   qReq.ToApplicationId,
		"source_object_type":  qReq.SourceObjectType,
		"source_object_id":    qReq.SourceObjectId,
		"json_data":           qReq.JsonData,
	}

	count, _ := db.Database.Collection(setting.QueueTable).CountDocuments(context.Background(), filter)

	if count > 0 {
		return errors.New("Data Already exists ")
	}
	return nil
}
