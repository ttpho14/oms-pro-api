package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Brand struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef3013931fcf41ad46deca7"`
	BrandId               string             `json:"brand_id" bson:"brand_id" example:"excepteur"`
	Description           string             `json:"description" bson:"description" example:"culpa"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5ef301393137b76125b31f9c"`
	Division              string             `json:"division" bson:"division" example:"commodo"`
	Segmentation          string             `json:"segmentation" bson:"segmentation" example:"voluptate"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"true"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"false"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2019-05-16T03:20:31 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef30139b1fe3ab144ba7c2f"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef301392fdd17f6073bdd9c"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2019-12-30T09:05:14 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef301394a64c2e6ca57838c"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef3013963eba4f8a7fa139f"`
}

type BrandWithDetails struct {
	ID                    primitive.ObjectID  `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef3013931fcf41ad46deca7"`
	BrandId               string              `json:"brand_id" bson:"brand_id" example:"excepteur"`
	Description           string              `json:"description" bson:"description" example:"culpa"`
	TenantKey             primitive.ObjectID  `json:"tenant_key" bson:"tenant_key" example:"5ef301393137b76125b31f9c"`
	Division              string              `json:"division" bson:"division" example:"commodo"`
	Segmentation          string              `json:"segmentation" bson:"segmentation" example:"voluptate"`
	BrandRegistration     []BrandRegistration `json:"brand_registrations" bson:"brand_registrations"`
	IsActive              bool                `json:"is_active" bson:"is_active" example:"true"`
	IsDeleted             bool                `json:"is_deleted" bson:"is_deleted" example:"false"`
	IsDefault             bool                `json:"is_default" bson:"is_default" example:"true"`
	ObjectType            string              `json:"object_type" bson:"object_type" example:"false"`
	CreatedDate           time.Time           `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2019-05-16T03:20:31 -07:00"`
	CreatedBy             primitive.ObjectID  `json:"created_by" bson:"created_by" example:"5ef30139b1fe3ab144ba7c2f"`
	CreatedApplicationKey primitive.ObjectID  `json:"created_application_key" bson:"created_application_key" example:"5ef301392fdd17f6073bdd9c"`
	UpdatedDate           time.Time           `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2019-12-30T09:05:14 -07:00"`
	UpdatedBy             primitive.ObjectID  `json:"updated_by" bson:"updated_by" example:"5ef301394a64c2e6ca57838c"`
	UpdatedApplicationKey primitive.ObjectID  `json:"updated_application_key" bson:"updated_application_key" example:"5ef3013963eba4f8a7fa139f"`
}

type BrandRequest struct {
	BrandId                  string                     `json:"brand_id" bson:"brand_id" example:"excepteur"`
	Description              string                     `json:"description" bson:"description" example:"culpa"`
	TenantKey                primitive.ObjectID         `json:"tenant_key" bson:"tenant_key" example:"5ef301393137b76125b31f9c"`
	Division                 string                     `json:"division" bson:"division" example:"commodo"`
	Segmentation             string                     `json:"segmentation" bson:"segmentation" example:"voluptate"`
	BrandRegistrationRequest []BrandRegistrationRequest `json:"brand_registrations" bson:"brand_registrations"`
	IsActive                 bool                       `json:"is_active" bson:"is_active" example:"true"`
	IsDefault                bool                       `json:"is_default" bson:"is_default" example:"true"`
	ObjectType               string                     `json:"object_type" bson:"object_type" example:"false"`
	CreatedBy                primitive.ObjectID         `json:"created_by" bson:"created_by" example:"5ef30139b1fe3ab144ba7c2f"`
	CreatedApplicationKey    primitive.ObjectID         `json:"created_application_key" bson:"created_application_key" example:"5ef301392fdd17f6073bdd9c"`
	UpdatedBy                primitive.ObjectID         `json:"updated_by" bson:"updated_by" example:"5ef301394a64c2e6ca57838c"`
	UpdatedApplicationKey    primitive.ObjectID         `json:"updated_application_key" bson:"updated_application_key" example:"5ef3013963eba4f8a7fa139f"`
}
