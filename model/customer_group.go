package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type CustomerGroup struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5efc056c7288fa0f7616bd89"`
	CustomerGroupId       string             `json:"customer_group_id" bson:"customer_group_id" example:"anim"`
	Description           string             `json:"description" bson:"description" example:"deserunt"`
	TenantKey             string             `json:"tenant_key" bson:"application_id" example:"5efc056cb5d1f792a5f90e42"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"false"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"duis"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2015-07-12T05:00:34 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef955aabe7dcb42670633fb"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef955aa4c9a02bc46fa0614"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2017-10-14T11:15:09 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef955aa0708b6f7ee36b4c0"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef955aad64e77d0ee9427f1"`
}

type CustomerGroupRequest struct {
	CustomerGroupId       string             `json:"customer_group_id" bson:"customer_group_id" example:"anim"`
	Description           string             `json:"description" bson:"description" example:"deserunt"`
	TenantKey             string             `json:"tenant_key" bson:"application_id" example:"5efc056cb5d1f792a5f90e42"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDefault             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"duis"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef955aabe7dcb42670633fb"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef955aa4c9a02bc46fa0614"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef955aa0708b6f7ee36b4c0"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef955aad64e77d0ee9427f1"`
}
