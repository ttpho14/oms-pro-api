package model

import (
	"../setting"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Application struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef955aa38382ae8f746f065"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5ef955aa3eb50d33a9c86909"`
	SalesChannelKey       primitive.ObjectID `json:"sales_channel_key" bson:"sales_channel_key" example:"5ef955aa8ca953065abe3956"`
	ApplicationId         string             `json:"application_id" bson:"application_id" example:"consectetur"`
	Description           string             `json:"description" bson:"description" example:"dolore"`
	ClientId              string             `json:"client_id" bson:"client_id" example:"8662515b-bf1f-4f39-8afc-8fb4aea55b63"`
	SecretKey             string             `json:"secret_key" bson:"secret_key" example:"3087b8bf-fae5-4818-97a7-24329473705e"`
	RedirectURI           string             `json:"redirect_uri" bson:"redirect_uri" example:"http://localhost:8080"`
	GrantType             string             `json:"grant_type" bson:"grant_type" example:"deserunt"`
	Scope                 string             `json:"scope" bson:"scope" example:"esse"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"duis"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2015-07-12T05:00:34 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef955aabe7dcb42670633fb"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef955aa4c9a02bc46fa0614"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2017-10-14T11:15:09 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef955aa0708b6f7ee36b4c0"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef955aad64e77d0ee9427f1"`
}

type ApplicationWithDetails struct {
	ID                       primitive.ObjectID         `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef955aa38382ae8f746f065"`
	TenantKey                primitive.ObjectID         `json:"tenant_key" bson:"tenant_key" example:"5ef955aa3eb50d33a9c86909"`
	SalesChannelKey          primitive.ObjectID         `json:"sales_channel_key" bson:"sales_channel_key" example:"5ef955aa8ca953065abe3956"`
	ApplicationId            string                     `json:"application_id" bson:"application_id" example:"consectetur"`
	Description              string                     `json:"description" bson:"description" example:"dolore"`
	ClientId                 string                     `json:"client_id" bson:"client_id" example:"8662515b-bf1f-4f39-8afc-8fb4aea55b63"`
	SecretKey                string                     `json:"secret_key" bson:"secret_key" example:"3087b8bf-fae5-4818-97a7-24329473705e"`
	RedirectURI              string                     `json:"redirect_uri" bson:"redirect_uri" example:"http://localhost:8080"`
	GrantType                string                     `json:"grant_type" bson:"grant_type" example:"deserunt"`
	Scope                    string                     `json:"scope" bson:"scope" example:"esse"`
	IsActive                 bool                       `json:"is_active" bson:"is_active" example:"true"`
	IsDeleted                bool                       `json:"is_deleted" bson:"is_deleted" example:"false"`
	ObjectType               string                     `json:"object_type" bson:"object_type" example:"duis"`
	CreatedDate              time.Time                  `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2015-07-12T05:00:34 -07:00"`
	CreatedBy                primitive.ObjectID         `json:"created_by" bson:"created_by" example:"5ef955aabe7dcb42670633fb"`
	CreatedApplicationKey    primitive.ObjectID         `json:"created_application_key" bson:"created_application_key" example:"5ef955aa4c9a02bc46fa0614"`
	UpdatedDate              time.Time                  `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2017-10-14T11:15:09 -07:00"`
	UpdatedBy                primitive.ObjectID         `json:"updated_by" bson:"updated_by" example:"5ef955aa0708b6f7ee36b4c0"`
	UpdatedApplicationKey    primitive.ObjectID         `json:"updated_application_key" bson:"updated_application_key" example:"5ef955aad64e77d0ee9427f1"`
	ApplicationConfiguration []ApplicationConfiguration `json:"application_configurations" bson:"application_configurations"`
}

type ApplicationRequest struct {
	TenantKey                       primitive.ObjectID                `json:"tenant_key" bson:"tenant_key" example:"5ef955aa3eb50d33a9c86909"`
	SalesChannelKey                 primitive.ObjectID                `json:"sales_channel_key" bson:"sales_channel_key" example:"5ef955aa8ca953065abe3956"`
	ApplicationId                   string                            `json:"application_id" bson:"application_id" example:"consectetur"`
	Description                     string                            `json:"description" bson:"description" example:"dolore"`
	RedirectURI                     string                            `json:"redirect_uri" bson:"redirect_uri" example:"http://localhost:8080"`
	GrantType                       string                            `json:"grant_type" bson:"grant_type" example:"deserunt"`
	Scope                           string                            `json:"scope" bson:"scope" example:"esse"`
	IsActive                        bool                              `json:"is_active" bson:"is_active" example:"true"`
	ObjectType                      string                            `json:"object_type" bson:"object_type" example:"duis"`
	CreatedBy                       primitive.ObjectID                `json:"created_by" bson:"created_by" example:"5ef955aabe7dcb42670633fb"`
	CreatedApplicationKey           primitive.ObjectID                `json:"created_application_key" bson:"created_application_key" example:"5ef955aa4c9a02bc46fa0614"`
	UpdatedBy                       primitive.ObjectID                `json:"updated_by" bson:"updated_by" example:"5ef955aa0708b6f7ee36b4c0"`
	UpdatedApplicationKey           primitive.ObjectID                `json:"updated_application_key" bson:"updated_application_key" example:"5ef955aad64e77d0ee9427f1"`
	ApplicationConfigurationRequest []ApplicationConfigurationRequest `json:"application_configurations" bson:"application_configurations"`
}

func OmsApplicationInitialize() *Application {
	currentTime := time.Now()

	return &Application{
		ID:                    primitive.NewObjectID(),
		TenantKey:             primitive.NilObjectID,
		SalesChannelKey:       primitive.NilObjectID,
		ApplicationId:         "OMS",
		Description:           "OMS",
		ClientId:              setting.OmsClientId,
		SecretKey:             setting.OmsSecretKey,
		RedirectURI:           "",
		GrantType:             "",
		Scope:                 "",
		IsActive:              true,
		IsDeleted:             false,
		ObjectType:            "application",
		CreatedDate:           currentTime,
		CreatedBy:             primitive.NilObjectID,
		CreatedApplicationKey: primitive.NilObjectID,
		UpdatedDate:           currentTime,
		UpdatedBy:             primitive.NilObjectID,
		UpdatedApplicationKey: primitive.NilObjectID,
	}
}
