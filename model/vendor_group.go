package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type VendorGroup struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5efc1cfad263971daa76d4bc"`
	VendorGroupId         string             `json:"vendor_group_id" bson:"vendor_group_id" example:"in"`
	Description           string             `json:"description" bson:"description" example:"in"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5ef96db2088ec88b6b05e1d5"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDeleted             bool               `json:"is_deleted" bson:"is_deleted" example:"false"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"false"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"est"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2019-08-14T12:02:56 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef96db2dc3410a231844080"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef96db2088ec88b6b05e1d5"`
	UpdatedDate           time.Time          `json:"updated_date,omitempty" bson:"updated_date,omitempty" example:"2020-05-12T05:33:55 -07:00"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef96db23c574ffde7644a58"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef96db231451247fd91958e"`
}

type AddVendorGroup struct {
	VendorGroupId         string             `json:"vendor_group_id" bson:"vendor_group_id" example:"in"`
	Description           string             `json:"description" bson:"description" example:"in"`
	TenantKey             primitive.ObjectID `json:"tenant_key" bson:"tenant_key" example:"5ef96db2088ec88b6b05e1d5"`
	IsActive              bool               `json:"is_active" bson:"is_active" example:"true"`
	IsDefault             bool               `json:"is_default" bson:"is_default" example:"false"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"est"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef96db2dc3410a231844080"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef96db2088ec88b6b05e1d5"`
	UpdatedBy             primitive.ObjectID `json:"updated_by" bson:"updated_by" example:"5ef96db23c574ffde7644a58"`
	UpdatedApplicationKey primitive.ObjectID `json:"updated_application_key" bson:"updated_application_key" example:"5ef96db231451247fd91958e"`
}
