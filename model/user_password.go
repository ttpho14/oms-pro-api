package model

import (
	"../setting"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type UserPassword struct {
	ID                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty" example:"5ef96d9c5954cbf2ea3cf7af"`
	Email                 string             `json:"email" bson:"email" example:"bookerchaney@opticon.com"`
	Password              string             `json:"password" bson:"password"`
	ObjectType            string             `json:"object_type" bson:"object_type" example:"user_password"`
	CreatedDate           time.Time          `json:"created_date,omitempty" bson:"created_date,omitempty" example:"2019-08-14T12:02:56 -07:00"`
	CreatedBy             primitive.ObjectID `json:"created_by" bson:"created_by" example:"5ef96db2dc3410a231844080"`
	CreatedApplicationKey primitive.ObjectID `json:"created_application_key" bson:"created_application_key" example:"5ef96db2088ec88b6b05e1d5"`
}

func NewUserPassword(email string, password string) *UserPassword {
	return &UserPassword{
		ID: primitive.NewObjectID(),
		Email: email,
		Password: password,
		ObjectType: setting.UserPasswordTable,
		CreatedDate: time.Now(),
		CreatedBy: primitive.NilObjectID,
		CreatedApplicationKey: primitive.NilObjectID,

	}
}
