package api

import (
	"../setting"
	"../database"
	"./api_helper"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"net/http"
	"os"
	"reflect"
	"time"

	"../model"
)

// Add TaxCode godoc
// @Tags TaxCode
// @Summary Add TaxCode
// @Description Add TaxCode
// @Accept  json
// @Produce  json
// @Param body body []model.AddTaxCode true "Add TaxCode"
// @Success 200 {object} []model.TaxCode
// @Header 200 {string} Token "qwerty"
// @Router /TaxCode [POST]
func CreateTaxCode(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateTaxCode")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.TaxCodeTable)
	response.Header().Set("content-type", "application/json")
	var taxCode model.TaxCode
	err = json.NewDecoder(request.Body).Decode(&taxCode)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	//Set Date = time.Now()
	taxCode.CreatedDate = time.Now()
	taxCode.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	result, err := collection.InsertOne(ctx, taxCode)
	if err != nil {
		json.NewEncoder(response).Encode(err.Error())
		return
	}
	id := result.InsertedID
	taxCode.ID = id.(primitive.ObjectID)
	json.NewEncoder(response).Encode(taxCode)
}

func CreateManyTaxCode(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyTaxCode")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.TaxCodeTable)
	response.Header().Set("content-type", "application/json")
	var manyTaxCode []model.TaxCode
	err = json.NewDecoder(request.Body).Decode(&manyTaxCode)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, taxCode := range manyTaxCode {
		taxCode.ID = primitive.NewObjectID()
		taxCode.CreatedDate = time.Now()
		taxCode.UpdatedDate = time.Now()
		ui = append(ui, taxCode)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// Get TaxCode By Id godoc
// @Tags TaxCode
// @Summary Get TaxCode by Id
// @Description Get TaxCode by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of TaxCode need to be found"
// @Success 200 {object} model.TaxCode
// @Header 200 {string} Token "qwerty"
// @Router /TaxCode/{id} [get]
func GetTaxCodeById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetTaxCodeById")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.TaxCodeTable)
	response.Header().Set("content-type", "application/json")
	var taxCode model.TaxCode
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//_id = id and is_deleted = false
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}, "is_deleted": bson.M{"$eq": false}}
	err := collection.FindOne(ctx, filter).Decode(&taxCode)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(taxCode)
}

// Get number of TaxCodes godoc
// @Tags TaxCode
// @Summary Get number of TaxCodes sort by created_date field descending
// @Description Get number of TaxCodes depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.TaxCode
// @Header 200 {string} Token "qwerty"
// @Router /TaxCode [get]
func GetTaxCodes(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetTaxCodes")
	//Get Limit and Page
	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	collection := database.Database.Collection(setting.TaxCodeTable)
	response.Header().Set("content-type", "application/json")
	var allTaxCode []model.TaxCode
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"is_deleted": bson.M{"$eq": false}}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var taxCode model.TaxCode
		cursor.Decode(&taxCode)
		allTaxCode = append(allTaxCode, taxCode)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(allTaxCode)
}

// Update TaxCode godoc
// @Tags TaxCode
// @Summary Update TaxCode
// @Description Update TaxCode
// @Accept  json
// @Produce  json
// @Param id path string true "Id of TaxCode need to be found"
// @Param body body model.AddTaxCode true "Update TaxCode"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /TaxCode/{id} [put]
func UpdateTaxCode(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "UpdateTaxCode")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.TaxCodeTable)
	response.Header().Set("content-type", "application/json")
	var taxCode model.TaxCode
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	err = json.NewDecoder(request.Body).Decode(&taxCode)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	//Set lai thoi gian luc update
	taxCode.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	fmt.Println("filter TaxCode", filter)
	update := bson.M{"$set": taxCode}
	result, err := collection.UpdateOne(ctx, filter, update)
	fmt.Println("Result TaxCode", result)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

// Soft Delete TaxCode godoc
// @Tags TaxCode
// @Summary Soft Delete TaxCode
// @Description Soft Delete TaxCode
// @Accept  json
// @Produce  json
// @Param id path string true "Id of TaxCode need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /TaxCode/{id} [delete]
func DeleteSoftTaxCode(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteSoftTaxCode")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.TaxCodeTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("UpdateOne() result:", result)
		fmt.Println("UpdateOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("UpdateOne() result MatchedCount:", result.MatchedCount)
		fmt.Println("UpdateOne() result ModifiedCount:", result.ModifiedCount)
		fmt.Println("UpdateOne() result UpsertedCount:", result.UpsertedCount)
		fmt.Println("UpdateOne() result UpsertedID:", result.UpsertedID)
	}
	json.NewEncoder(response).Encode(result)
}

//DeleteHardTaxCode
/*
func DeleteHardTaxCode(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardTaxCode")
	var collection *mongo.Collection
	collection = database.Database.Collection("taxCode")
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
//-------------------- REGION API - END -----------------------
