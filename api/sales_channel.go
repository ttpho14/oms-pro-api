package api

import (
	"../database"
	"../setting"
	"./api_helper"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"net/http"
	"time"

	"os"
	"reflect"

	"../model"
)

// Add Sales Channel godoc
// @Tags Sales Channel
// @Summary Add Sales Channel
// @Description Add Sales Channel
// @Accept  json
// @Produce  json
// @Param body body []model.AddSalesChannel true "Add Sales Channel"
// @Success 200 {object} []model.SalesChannel
// @Header 200 {string} Token "qwerty"
// @Router /SalesChannel [POST]
func CreateSalesChannel(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateSalesChannel")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.SalesChannelTable)
	response.Header().Set("content-type", "application/json")
	var salesChannel model.SalesChannel
	err = json.NewDecoder(request.Body).Decode(&salesChannel)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	//Set Date = time.Now()
	salesChannel.CreatedDate = time.Now()
	salesChannel.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	result, _ := collection.InsertOne(ctx, salesChannel)
	id := result.InsertedID
	salesChannel.ID = id.(primitive.ObjectID)
	json.NewEncoder(response).Encode(salesChannel)
}

func CreateManySalesChannel(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManySalesChannel")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.SalesChannelTable)
	response.Header().Set("content-type", "application/json")
	var manySalesChannel []model.SalesChannel
	err = json.NewDecoder(request.Body).Decode(&manySalesChannel)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, salesChannel := range manySalesChannel {
		salesChannel.ID = primitive.NewObjectID()
		salesChannel.CreatedDate = time.Now()
		salesChannel.UpdatedDate = time.Now()
		ui = append(ui, salesChannel)
	}
	_, err = collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// Get Sales Channel By Id godoc
// @Tags Sales Channel
// @Summary Get Sales Channel by Id
// @Description Get Sales Channel by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Channel need to be found"
// @Success 200 {object} model.SalesChannel
// @Header 200 {string} Token "qwerty"
// @Router /SalesChannel/{id} [get]
func GetSalesChannelById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetSalesChannelById")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.SalesChannelTable)
	response.Header().Set("content-type", "application/json")
	var salesChannel model.SalesChannel
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//_id = id and is_deleted = false
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}, "is_deleted": bson.M{"$eq": false}}
	err := collection.FindOne(ctx, filter).Decode(&salesChannel)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesChannel)
}

// Get number of Sales Channels godoc
// @Tags Sales Channel
// @Summary Get number of Sales Channels sort by created_date field descending
// @Description Get number of Sales Channels depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.SalesChannel
// @Header 200 {string} Token "qwerty"
// @Router /SalesChannel [get]
func GetSalesChannels(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetSalesChannels")
	//Get Limit and Page
	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	collection := database.Database.Collection(setting.SalesChannelTable)
	response.Header().Set("content-type", "application/json")
	var allSalesChannel []model.SalesChannel
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"is_deleted": bson.M{"$eq": false}}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var salesChannel model.SalesChannel
		cursor.Decode(&salesChannel)
		allSalesChannel = append(allSalesChannel, salesChannel)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(allSalesChannel)
}

// Update Sales Channel godoc
// @Tags Sales Channel
// @Summary Update Sales Channel
// @Description Update Sales Channel
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Channel need to be found"
// @Param body body model.SalesChannel true "Update Sales Channel"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /SalesChannel/{id} [put]
func UpdateSalesChannel(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "UpdateSalesChannel")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.SalesChannelTable)
	response.Header().Set("content-type", "application/json")
	var salesChannel model.SalesChannel
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	err = json.NewDecoder(request.Body).Decode(&salesChannel)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	//Set lai thoi gian luc update
	salesChannel.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": salesChannel}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

// Soft Delete Sales Channel godoc
// @Tags Sales Channel
// @Summary Soft Delete Sales Channel
// @Description Soft Delete Sales Channel
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Channel need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /SalesChannel/{id} [delete]
func DeleteSoftSalesChannel(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteSoftSalesChannel")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.SalesChannelTable)
	response.Header().Set("content-type", "application/json")
	//var salesChannel model.SalesChannel
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	//json.NewDecoder(request.Body).Decode(&salesChannel)
	//Set lai thoi gian luc update
	//salesChannel.UpdatedDate = time.Now()
	//salesChannel.IsDeleted = true
	//ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	//defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	// Check for error, else print the UpdateOne() API call resultss
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("UpdateOne() result:", result)
		fmt.Println("UpdateOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("UpdateOne() result MatchedCount:", result.MatchedCount)
		fmt.Println("UpdateOne() result ModifiedCount:", result.ModifiedCount)
		fmt.Println("UpdateOne() result UpsertedCount:", result.UpsertedCount)
		fmt.Println("UpdateOne() result UpsertedID:", result.UpsertedID)
	}
	json.NewEncoder(response).Encode(result)
}

//DeleteHardSales Channel
/*
func DeleteHardSalesChannel(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardSalesChannel")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.SalesChannelTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
//-------------------- Country API - END -----------------------
