package api

import (
	"context"
	"encoding/json"
	"net/http"
	"os"
	"strconv"
	"time"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

var DELIVERY_FREIGHT_TABLE_NAME = "delivery_freight"

// CreateDeliveryFreight godoc
// @Tags Delivery Freight
// @Summary Add Delivery Freight
// @Description Add Delivery Freight
// @Accept  json
// @Produce  json
// @Param body body []model.AddDeliveryFreight true "Add Delivery Freight"
// @Success 200 {object} []model.DeliveryFreight
// @Header 200 {string} Token "qwerty"
// @Router /DeliveryFreight [POST]
func CreateDeliveryFreight(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateDeliveryFreight")
	var (
		collection      *mongo.Collection
		DeliveryFreight model.DeliveryFreight
	)

	collection = database.Database.Collection(DELIVERY_FREIGHT_TABLE_NAME)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&DeliveryFreight)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	currentTime := time.Now()
	DeliveryFreight.CreatedDate = currentTime
	DeliveryFreight.UpdatedDate = currentTime

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	result, err := collection.InsertOne(ctx, DeliveryFreight)
	if err != nil {
		json.NewEncoder(response).Encode(err.Error())
		return
	}
	id := result.InsertedID
	DeliveryFreight.ID = id.(primitive.ObjectID)
	json.NewEncoder(response).Encode(DeliveryFreight)
}

// CreateManyDeliveryFreight func
func CreateManyDeliveryFreight(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManyDeliveryFreight")
	var (
		collection       *mongo.Collection
		DeliveryFreights []model.DeliveryFreight
		ui               []interface{}
	)
	collection = database.Database.Collection(DELIVERY_FREIGHT_TABLE_NAME)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&DeliveryFreights)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	currentTime := time.Now()
	for _, DeliveryFreight := range DeliveryFreights {
		DeliveryFreight.ID = primitive.NewObjectID()
		DeliveryFreight.CreatedDate = currentTime
		DeliveryFreight.UpdatedDate = currentTime
		ui = append(ui, DeliveryFreight)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// GetDeliveryFreightByID godoc
// @Tags Delivery Freight
// @Summary Get Delivery Freight by Id
// @Description Get Delivery Freight by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Delivery Freight need to be found"
// @Success 200 {object} model.DeliveryFreight
// @Header 200 {string} Token "qwerty"
// @Router /DeliveryFreight/{id} [get]
func GetDeliveryFreightByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetDeliveryFreightByID")
	var (
		collection      *mongo.Collection
		DeliveryFreight model.DeliveryFreight
	)
	collection = database.Database.Collection(DELIVERY_FREIGHT_TABLE_NAME)

	response.Header().Set("content-type", "application/json")

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	err := collection.FindOne(ctx, filter).Decode(&DeliveryFreight)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(DeliveryFreight)
}

// GetDeliveryFreights godoc
// @Tags Delivery Freight
// @Summary Get all Delivery Freight
// @Description Get all Delivery Freight
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.DeliveryFreight
// @Header 200 {string} Token "qwerty"
// @Router /DeliveryFreight [get]
func GetDeliveryFreights(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetDeliveryFreights")
	var (
		collection       *mongo.Collection
		deliveryFreights []model.DeliveryFreight
		query            = request.URL.Query()
		options          = options.Find()
	)
	collection = database.Database.Collection(DELIVERY_FREIGHT_TABLE_NAME)

	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, err := strconv.ParseInt(query.Get("limit"), 10, 64)
	if limit <= 0 {
		limit = 100
	} else if limit >= 500 {
		limit = 500
	}
	options.SetLimit(limit)
	cursor, err := collection.Find(ctx, options)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var deliveryFreight model.DeliveryFreight
		cursor.Decode(&deliveryFreight)
		deliveryFreights = append(deliveryFreights, deliveryFreight)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(deliveryFreights)
}

// UpdateDeliveryFreight godoc
// @Tags Delivery Freight
// @Summary Update Delivery Freight
// @Description Update Delivery Freight
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Delivery need to be found"
// @Param body body model.AddDelivery true "Update Delivery Freight"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /DeliveryFreight/{id} [put]
func UpdateDeliveryFreight(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateDeliveryFreight")
	var (
		collection      *mongo.Collection
		deliveryFreight model.DeliveryFreight
	)
	collection = database.Database.Collection(DELIVERY_FREIGHT_TABLE_NAME)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&deliveryFreight)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	deliveryFreight.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": deliveryFreight}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

func DeleteSoftDeliveryFreight(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftDeliveryFreight")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(DELIVERY_FREIGHT_TABLE_NAME)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
