package api

import (
	"context"
	"net/http"
	"time"

	"../database"
	"../model"
	"../setting"
	"./api_helper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

func GetUserPasswords(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetUserPasswords")
	var (
		allUserPassword []model.UserPassword
		filter          bson.D
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.UserPasswordTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	email := request.URL.Query().Get("email")
	if email != "" {
		filter = append(filter, bson.E{Key: "email", Value: email})
	}

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter = append(filter, bson.E{Key: "_id", Value: bson.M{"$ne": bsonx.ObjectID(primitive.NilObjectID)}})
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))

	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var user model.UserPassword
		_ = cursor.Decode(&user)
		allUserPassword = append(allUserPassword, user)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"user_password": allUserPassword}
	api_helper.SuccessResponse(response, data, "Get User Password Successfully")
}
