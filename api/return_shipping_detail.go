package api

import (
	"context"
	"encoding/json"
	"net/http"
	"os"
	"time"

	"../setting"
	"github.com/jinzhu/copier"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateReturnShippingDetail godoc
// @Tags Return Order ShippingDetail
// @Summary Add Return Order ShippingDetail
// @Description Add Return Order ShippingDetail
// @Accept  json
// @Produce  json
// @Param body body model.ReturnShippingDetailRequest true "Add Return Order ShippingDetail"
// @Success 200 {object} model.ReturnShippingDetail
// @Header 200 {string} Token "qwerty"
// @Router /ReturnShippingDetail [POST]
func CreateReturnShippingDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateReturnShippingDetail")
	var (
		collection              *mongo.Collection
		returnShippingDetail    model.ReturnShippingDetail
		returnShippingDetailReq model.ReturnShippingDetailRequest
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReturnShippingDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&returnShippingDetailReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Return Order ShippingDetail Input ERROR - "+err.Error())
		return
	}

	_ = copier.Copy(&returnShippingDetail, &returnShippingDetailReq)

	currentTime := time.Now()
	returnShippingDetail.ID = primitive.NewObjectIDFromTimestamp(currentTime)
	returnShippingDetail.ObjectType = setting.ReturnShippingDetailTable
	returnShippingDetail.CreatedDate = currentTime
	returnShippingDetail.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, returnShippingDetail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Return Order ShippingDetail ERROR - "+err.Error())
		return
	}

	data := bson.M{"return_comment": returnShippingDetail}
	api_helper.SuccessResponse(response, data, "Create Return Order ShippingDetail successfully")
}

// CreateManyReturnShippingDetail func
func CreateManyReturnShippingDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManyReturnShippingDetail")
	var (
		collection            *mongo.Collection
		returnShippingDetails []model.ReturnShippingDetail
	)
	collection = database.Database.Collection(setting.ReturnShippingDetailTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&returnShippingDetails)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	currentTime := time.Now()
	for _, returnShippingDetail := range returnShippingDetails {
		returnShippingDetail.ID = primitive.NewObjectID()
		returnShippingDetail.CreatedDate = currentTime
		returnShippingDetail.UpdatedDate = currentTime
		ui = append(ui, returnShippingDetail)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// GetReturnShippingDetailByID godoc
// @Tags Return Order ShippingDetail
// @Summary Get Return Order ShippingDetail by Id
// @Description Get Return Order ShippingDetail by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Return Order ShippingDetail need to be found"
// @Success 200 {object} model.ReturnShippingDetail
// @Header 200 {string} Token "qwerty"
// @Router /ReturnShippingDetail/{id} [get]
func GetReturnShippingDetailByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSaleOrderShippingDetailByID")
	var (
		collection           *mongo.Collection
		returnShippingDetail model.ReturnShippingDetail
	)
	collection = database.Database.Collection(setting.ReturnShippingDetailTable)

	response.Header().Set("content-type", "application/json")

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	err := collection.FindOne(ctx, filter).Decode(&returnShippingDetail)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(returnShippingDetail)
}

// GetReturnShippingDetails godoc
// @Tags Return Order ShippingDetail
// @Summary Get all Return Order ShippingDetail
// @Description Get all Return Order ShippingDetail
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.ReturnShippingDetail
// @Header 200 {string} Token "qwerty"
// @Router /ReturnShippingDetail [get]
func GetReturnShippingDetails(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetReturnShippingDetails")
	var (
		collection            *mongo.Collection
		returnShippingDetails []model.ReturnShippingDetail
		findOptions           = options.Find()
		filter                = bson.M{}
	)
	collection = database.Database.Collection(setting.ReturnShippingDetailTable)

	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions) //findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var returnShippingDetail model.ReturnShippingDetail
		cursor.Decode(&returnShippingDetail)
		returnShippingDetails = append(returnShippingDetails, returnShippingDetail)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(returnShippingDetails)
}

// UpdateReturnShippingDetail godoc
// @Tags Return Order ShippingDetail
// @Summary Update Return Order ShippingDetail
// @Description Update Return Order ShippingDetail
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Return need to be found"
// @Param body body model.ReturnRequest true "Update Return Order ShippingDetail"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /ReturnShippingDetail/{id} [put]
func UpdateReturnShippingDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateSaleOrderShippingDetail")
	var (
		collection           *mongo.Collection
		returnShippingDetail model.ReturnShippingDetail
	)
	collection = database.Database.Collection(setting.ReturnShippingDetailTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&returnShippingDetail)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	returnShippingDetail.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": returnShippingDetail}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

func DeleteSoftReturnShippingDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftReturnShippingDetail")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.ReturnShippingDetailTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
