package api

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"time"

	"../database"
	"../model"
	sHelper "../service/helper"
	"../setting"
	help "./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// Create Queue godoc
// @Tags Queue
// @Summary Create Queue
// @Description Create Queue
// @Accept  json
// @Produce  json
// @Param body body model.QueueRequest true "Create Queue"
// @Success 200 {object} model.Queue
// @Header 200 {string} Token "qwerty"
// @Router /Queue [POST]
func CreateQueue(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "CreateQueue")
	var (
		collection *mongo.Collection
		queue      model.Queue
		queueReq   model.QueueRequest
	)

	response.Header().Set("content-type", "application/json")

	err = json.NewDecoder(request.Body).Decode(&queueReq)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}
	request.Body.Close()

	err := queueReq.IsExistsData()
	if err != nil {
		var res model.ResponseResult
		res.Success = true
		res.Message = err.Error()
		res.ErrorCode = 999
		json.NewEncoder(response).Encode(res)
		return
	}

	err = copier.Copy(&queue, &queueReq)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy input to main model ERROR - "+err.Error())
		return
	}
	//Set Date = time.Now()
	currentTime := time.Now()
	queue.ID = primitive.NewObjectID()
	queue.Status = false
	queue.CreatedDate = currentTime
	queue.UpdatedDate = currentTime

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	collection = database.Database.Collection(setting.QueueTable)
	_, err = collection.InsertOne(ctx, queue)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert to Database ERROR - "+err.Error())
		return
	}

	data := bson.M{"queue": queue}
	help.SuccessResponse(response, data, "Insert Queue to Database successfully")
}

// Update Queue godoc
// @Tags Queue
// @Summary Update Queue
// @Description Update Queue
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Application need to be found"
// @Param body body model.QueueRequest true "Update Queue"
// @Success 200 {object} model.Queue
// @Header 200 {string} Token "qwerty"
// @Router /Queue [PUT]
func UpdateQueue(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "CreateQueue")
	var (
		collection  *mongo.Collection
		queue       model.Queue
		queueReq    model.QueueRequest
		queueReqInt interface{}
	)

	response.Header().Set("content-type", "application/json")

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&queueReq)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}
	request.Body.Close()

	collection = database.Database.Collection(setting.QueueTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{"_id": bsonx.ObjectID(id)}

	err = collection.FindOne(ctx, filter).Decode(&queue)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Find queue ERROR - "+err.Error())
		return
	}

	queue.UpdatedDate = time.Now()

	if queueReq.Status == false {
		queue.Status = false
	}

	// To Byte
	qrb, _ := json.Marshal(queueReq)
	// To Interface{}
	_ = json.Unmarshal(qrb, &queueReqInt)

	update := bson.A{
		bson.M{"$set": queue}, // Set Main model first
		//bson.M{"$set": queueReqInt}, // Set Request Interface second
	}

	if string(qrb) != "{}" {
		update = append(update, bson.M{"$set": queueReqInt}) // Set Request Interface second
	}

	//Update
	_, err = collection.UpdateOne(ctx, filter, update)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Update queue ERROR - "+err.Error())
		return
	}

	//Find Queue after update
	err = collection.FindOne(ctx, filter).Decode(&queue)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Find queue after Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"queue": queue}
	help.SuccessResponse(response, data, "Update Queue to Database successfully")
}

// Get number of Queues godoc
// @Tags Queue
// @Summary Get number of Queues sort by created_date field descending
// @Description Get number of Queues depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Param keyword path string false "Keyword to search. By source object ID"
// @Param from_application_id path string false "From Application ID"
// @Param to_application_id path string false "To Application ID"
// @Param source_object_type path string false "Source Object Type"
// @Param from_date path int false "From Date ( epoch date )"
// @Param to_date path int false "To date ( epoch date )"
// @Success 200 {object} []model.Queue
// @Header 200 {string} Token "qwerty"
// @Router /Queue [get]
func GetQueues(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "GetQueues")
	var (
		queue    model.Queue
		allQueue []model.Queue
		andArray bson.A
	)
	response.Header().Set("content-type", "application/json")
	//Get Limit and Page
	limit, page, err := help.GetLimitAndPage(request)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit/Page ERROR - "+err.Error())
		return
	}

	fromAppId := request.URL.Query().Get("from_application_id")
	toAppId := request.URL.Query().Get("to_application_id")
	sourceObjType := request.URL.Query().Get("source_object_type")
	paramStatus := request.URL.Query().Get("status")
	webStatus := request.URL.Query().Get("web_status")
	keyword := request.URL.Query().Get("keyword")
	if keyword != "" {
		or := bson.D{{"$or", bson.A{
			bson.D{{"source_object_id", primitive.Regex{Pattern: keyword, Options: "i"}}},
		}}}
		andArray = append(andArray, or)
	}

	if fromAppId != "" {
		andArray = append(andArray, bson.M{"from_application_id": sHelper.RegexStringID(fromAppId)})
	}
	if toAppId != "" {
		andArray = append(andArray, bson.M{"to_application_id": sHelper.RegexStringID(toAppId)})
	}
	if sourceObjType != "" {
		andArray = append(andArray, bson.M{"source_object_type": sourceObjType})
	}

	if paramStatus != "" {
		status, err := strconv.ParseBool(paramStatus)
		if err != nil {
			help.ErrorResponse(response, http.StatusInternalServerError, 0, "Status must be true or false")
			return
		}
		andArray = append(andArray, bson.M{"status": status})
	}

	//For status on Web.
	if webStatus != "" {
		switch strings.ToUpper(webStatus) {
		case "PROCESSING":
			andArray = append(andArray, bson.M{
				"error_message": "",
				"status":        false,
			})
		case "SUCCESS":
			andArray = append(andArray, bson.M{"status": true})
		case "FAIL":
			andArray = append(andArray, bson.M{
				"error_message": bson.M{"$ne": ""},
				"status":        false,
			})
		}
	}

	fromDate, toDate, err := help.GetFromAndToDate(request)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Get From date/To Date ERROR - "+err.Error())
		return
	}

	andArray = append(andArray, bson.M{"created_date": bson.M{
		"$gte": time.Unix(fromDate, 0),
		"$lte": time.Unix(toDate, 0),
	},
	})

	collection := database.Database.Collection(setting.QueueTable)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))

	filter := bson.M{
		"$and": andArray,
	}

	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Find queue ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		_ = cursor.Decode(&queue)
		allQueue = append(allQueue, queue)
	}
	if err := cursor.Err(); err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"queue": allQueue}
	help.SuccessResponse(response, data, "Get Queue successfully")
}

// Get Count of Queues godoc
// @Tags Queue
// @Summary Get Count of  Queues
// @Description Get Count of Queues
// @Accept  json
// @Produce  json
// @Param keyword path string false "Keyword to search. By source object ID"
// @Param from_application_id path string false "From Application ID"
// @Param to_application_id path string false "To Application ID"
// @Param source_object_type path string false "Source Object Type"
// @Param from_date path int false "From Date ( epoch date )"
// @Param to_date path int false "To date ( epoch date )"
// @Success 200 {object} model.Count
// @Header 200 {string} Token "qwerty"
// @Router /Queue/count [get]
func GetCountQueue(response http.ResponseWriter, request *http.Request) {
	//handle error
	var (
		funcName = "GetCountQueue"
		andArray = bson.A{
			bson.M{"_id": bson.M{"$ne": bsonx.ObjectID(primitive.NilObjectID)}},
		}
	)
	defer help.RecoverError(response, funcName)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.QueueTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//Get Tenant Key fromm Token
	//_, err = api_helper.GetApplicationFromToken(setting.Token.GetClientID())
	//if err != nil {
	//	logger.Logger.Errorf(" `%s` ERROR : `%s`", funcName, err.Error())
	//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Get Tenant from Token ERROR - "+err.Error())
	//	return
	//}

	fromAppId := request.URL.Query().Get("from_application_id")
	toAppId := request.URL.Query().Get("to_application_id")
	sourceObjType := request.URL.Query().Get("source_object_type")
	paramStatus := request.URL.Query().Get("status")
	webStatus := request.URL.Query().Get("web_status")
	keyword := request.URL.Query().Get("keyword")
	if keyword != "" {
		or := bson.D{{"$or", bson.A{
			bson.D{{"source_object_id", primitive.Regex{Pattern: keyword, Options: "i"}}},
		}}}
		andArray = append(andArray, or)
	}

	if fromAppId != "" {
		andArray = append(andArray, bson.M{"from_application_id": fromAppId})
	}
	if toAppId != "" {
		andArray = append(andArray, bson.M{"to_application_id": toAppId})
	}
	if sourceObjType != "" {
		andArray = append(andArray, bson.M{"source_object_type": sourceObjType})
	}
	if paramStatus != "" {
		status, err := strconv.ParseBool(paramStatus)
		if err != nil {
			help.ErrorResponse(response, http.StatusInternalServerError, 0, "Status must be true or false")
			return
		}
		andArray = append(andArray, bson.M{"status": status})
	}

	//For status on Web.
	if webStatus != "" {
		switch strings.ToUpper(webStatus) {
		case "PROCESSING":
			andArray = append(andArray, bson.M{
				"error_message": "",
				"status":        false,
			})
		case "SUCCESS":
			andArray = append(andArray, bson.M{"status": true})
		case "FAIL":
			andArray = append(andArray, bson.M{
				"error_message": bson.M{"$ne": ""},
				"status":        false,
			})
		}
	}

	fromDate, toDate, err := help.GetFromAndToDate(request)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Get From date/To Date ERROR - "+err.Error())
		return
	}

	andArray = append(andArray, bson.M{"created_date": bson.M{
		"$gte": time.Unix(fromDate, 0),
		"$lte": time.Unix(toDate, 0),
	},
	})

	filter := bson.M{
		"$and": andArray,
		//{"tenant_key", tenantKey},
	}

	count, err := collection.CountDocuments(ctx, filter)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Product count ERROR - "+err.Error())
		return
	}

	data := bson.M{"count": count}
	help.SuccessResponse(response, data, "Get Count Queue successfully")
}

// Get Application By Id godoc
// @Tags Application
// @Summary Get Application by Id
// @Description Get Application by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Application need to be found"
// @Success 200 {object} model.Application
// @Header 200 {string} Token "qwerty"
// @Router /Application/{id} [get]

//-------------------- Service ---------------------
