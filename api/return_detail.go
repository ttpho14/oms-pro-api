package api

import (
	"context"
	"encoding/json"
	"net/http"
	"os"
	"time"

	"../setting"
	"github.com/jinzhu/copier"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateReturnDetail godoc
// @Tags Return Detail
// @Summary Add Return Detail
// @Description Add Return Detail
// @Accept  json
// @Produce  json
// @Param body body model.ReturnDetailRequest true "Add Return Detail"
// @Success 200 {object} model.ReturnDetail
// @Header 200 {string} Token "qwerty"
// @Router /ReturnDetail [POST]
func CreateReturnDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateReturnDetail")
	var (
		collection   *mongo.Collection
		returnDetail model.ReturnDetail
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReturnDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&returnDetail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Input to Request Model ERROR - "+err.Error())
		return
	}
	//Set Date = time.Now()
	currentTime := time.Now()
	returnDetail.ID = primitive.NewObjectID()
	returnDetail.CreatedDate = currentTime
	returnDetail.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, returnDetail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	data := bson.M{"return_detail": returnDetail}
	api_helper.SuccessResponse(response, data, "Return Detail successfully")
}

// GetReturnDetailByID godoc
// @Tags Return Detail
// @Summary Get Return Detail by Id
// @Description Get Return Detail by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Return Detail need to be found"
// @Success 200 {object} model.ReturnDetail
// @Header 200 {string} Token "qwerty"
// @Router /ReturnDetail/{id} [get]
func GetReturnDetailByID(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetReturnDetailById")
	var (
		collection   *mongo.Collection
		returnDetail model.ReturnDetail
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReturnDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	err := collection.FindOne(ctx, filter).Decode(&returnDetail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	data := bson.M{"bundle_detail": returnDetail}
	api_helper.SuccessResponse(response, data, "Get Return Detail successfully")
}

// GetReturnDetails godoc
// @Tags Return Detail
// @Summary Get all Return Detail
// @Description Get all Return Detail
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.ReturnDetail
// @Header 200 {string} Token "qwerty"
// @Router /ReturnDetail [get]
func GetReturnDetails(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetReturnDetails")
	var (
		returnDetail    model.ReturnDetail
		allReturnDetail []model.ReturnDetail
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.ReturnDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{"is_deleted": false}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		_ = cursor.Decode(&returnDetail)
		allReturnDetail = append(allReturnDetail, returnDetail)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"return_detail": allReturnDetail}
	api_helper.SuccessResponse(response, data, "Get Return Detail successfully")
}

// UpdateReturnDetail godoc
// @Tags Return Detail
// @Summary Update Return Detail
// @Description Update Return Detail
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Return need to be found"
// @Param body body model.ReturnRequest true "Update Return Detail"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /ReturnDetail/{id} [put]
func UpdateReturnDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var (
		collection      *mongo.Collection
		returnDetail    model.ReturnDetail
		returnDetailReq model.ReturnDetailRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReturnDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err = collection.FindOne(ctx, filter).Decode(&returnDetail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = json.NewDecoder(request.Body).Decode(&returnDetailReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&returnDetail, &returnDetailReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set lai thoi gian luc update
	returnDetail.UpdatedDate = time.Now()

	update := bson.M{"$set": returnDetail}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"return_detail": returnDetail}
	api_helper.SuccessResponse(response, data, "Update Return Detail Successfully")
}

//------------------- Unused --------------------------
func CreateManyReturnDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManyReturnDetail")
	var (
		collection    *mongo.Collection
		returnDetails []model.ReturnDetail
		ui            []interface{}
	)
	collection = database.Database.Collection(setting.ReturnDetailTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&returnDetails)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	currentTime := time.Now()
	for _, ReturnDetail := range returnDetails {
		ReturnDetail.ID = primitive.NewObjectID()
		ReturnDetail.CreatedDate = currentTime
		ReturnDetail.UpdatedDate = currentTime
		ui = append(ui, ReturnDetail)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// DeleteSoftReturnDetail func
func DeleteSoftReturnDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftReturnDetail")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.ReturnDetailTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
