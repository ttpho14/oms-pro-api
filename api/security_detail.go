package api

import (
	"../database"
	"../setting"
	"./api_helper"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"net/http"
	"os"
	"reflect"
	"time"

	"../model"
)

// Add SecurityDetail godoc
// @Tags SecurityDetail
// @Summary Add SecurityDetail
// @Description Add SecurityDetail
// @Accept  json
// @Produce  json
// @Param body body []model.SecurityDetailRequest true "Add SecurityDetail"
// @Success 200 {object} []model.SecurityDetail
// @Header 200 {string} Token "qwerty"
// @Router /SecurityDetail [POST]
func CreateSecurityDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateSecurityDetail")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.SecurityDetailTable)
	response.Header().Set("content-type", "application/json")
	var securityDetail model.SecurityDetail
	err = json.NewDecoder(request.Body).Decode(&securityDetail)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	//Set Date = time.Now()
	securityDetail.CreatedDate = time.Now()
	securityDetail.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	result, err := collection.InsertOne(ctx, securityDetail)
	if err != nil {
		json.NewEncoder(response).Encode(err.Error())
		return
	}
	id := result.InsertedID
	securityDetail.ID = id.(primitive.ObjectID)
	json.NewEncoder(response).Encode(securityDetail)
}

func CreateManySecurityDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManySecurityDetail")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.SecurityDetailTable)
	response.Header().Set("content-type", "application/json")
	var manySecurityDetail []model.SecurityDetail
	err = json.NewDecoder(request.Body).Decode(&manySecurityDetail)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, securityDetail := range manySecurityDetail {
		securityDetail.ID = primitive.NewObjectID()
		securityDetail.CreatedDate = time.Now()
		securityDetail.UpdatedDate = time.Now()
		ui = append(ui, securityDetail)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// Get SecurityDetail By Id godoc
// @Tags SecurityDetail
// @Summary Get SecurityDetail by Id
// @Description Get SecurityDetail by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of SecurityDetail need to be found"
// @Success 200 {object} model.SecurityDetail
// @Header 200 {string} Token "qwerty"
// @Router /SecurityDetail/{id} [get]
func GetSecurityDetailById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetSecurityDetailById")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.SecurityDetailTable)
	response.Header().Set("content-type", "application/json")
	var securityDetail model.SecurityDetail
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//_id = id and is_deleted = false
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	err := collection.FindOne(ctx, filter).Decode(&securityDetail)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(securityDetail)
}

// Get number of SecurityDetails godoc
// @Tags SecurityDetail
// @Summary Get number of SecurityDetails sort by created_date field descending
// @Description Get number of SecurityDetails depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.SecurityDetail
// @Header 200 {string} Token "qwerty"
// @Router /SecurityDetail [get]
func GetSecurityDetails(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetSecurityDetails")
	//Get Limit and Page
	//limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300)
	//if err != nil {
	//	response.WriteHeader(http.StatusInternalServerError)
	//	response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
	//	return
	//}
	//page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1)
	//if err != nil {
	//	response.WriteHeader(http.StatusInternalServerError)
	//	response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
	//	return
	//}
	collection := database.Database.Collection(setting.SecurityDetailTable)
	response.Header().Set("content-type", "application/json")
	var allSecurityDetail []model.SecurityDetail
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//filter := bson.M{"is_deleted": bson.M{" $eq": false}}
	filter := bson.M{}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	//findOptions.SetLimit(limit)
	// Set page
	//findOptions.SetSkip(limit*(page-1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var securityDetail model.SecurityDetail
		cursor.Decode(&securityDetail)
		allSecurityDetail = append(allSecurityDetail, securityDetail)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(allSecurityDetail)
}

// Update SecurityDetail godoc
// @Tags SecurityDetail
// @Summary Update SecurityDetail
// @Description Update SecurityDetail
// @Accept  json
// @Produce  json
// @Param id path string true "Id of SecurityDetail need to be found"
// @Param body body model.SecurityDetailRequest true "Update SecurityDetail"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /SecurityDetail/{id} [put]
func UpdateSecurityDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "UpdateSecurityDetail")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.SecurityDetailTable)
	response.Header().Set("content-type", "application/json")
	var securityDetail model.SecurityDetail
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	err = json.NewDecoder(request.Body).Decode(&securityDetail)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	//Set lai thoi gian luc update
	securityDetail.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	fmt.Println("filter SecurityDetail", filter)
	update := bson.M{"$set": securityDetail}
	result, err := collection.UpdateOne(ctx, filter, update)
	fmt.Println("Result SecurityDetail", result)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

func DeleteSoftSecurityDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteSoftSecurityDetail")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.SecurityDetailTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("UpdateOne() result:", result)
		fmt.Println("UpdateOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("UpdateOne() result MatchedCount:", result.MatchedCount)
		fmt.Println("UpdateOne() result ModifiedCount:", result.ModifiedCount)
		fmt.Println("UpdateOne() result UpsertedCount:", result.UpsertedCount)
		fmt.Println("UpdateOne() result UpsertedID:", result.UpsertedID)
	}
	json.NewEncoder(response).Encode(result)
}

//DeleteHardSecurityDetail
/*
func DeleteHardSecurityDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardSecurityDetail")
	var collection *mongo.Collection
	collection = database.Database.Collection("securityDetail")
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
//-------------------- REGION API - END -----------------------
