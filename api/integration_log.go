package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../database"
	"../model"
	sHelper "../service/helper"
	"../setting"
	help "./api_helper"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Create Integration Log godoc
// @Tags Integration Log
// @Summary Create Integration Log
// @Description Create Integration Log
// @Accept  json
// @Produce  json
// @Param body body model.IntegrationLogRequest true "Create Integration Log"
// @Success 200 {object} model.IntegrationLog
// @Header 200 {string} Token "qwerty"
// @Router /IntegrationLog [POST]
func CreateIntegrationLog(response http.ResponseWriter, request *http.Request) {
	defer help.RecoverError(response, "GetServiceStatus")

	var (
		log    model.IntegrationLog
		logReq model.IntegrationLogRequest
	)

	response.Header().Set("content-type", "application/json")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&logReq)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}
	request.Body.Close()

	_ = copier.Copy(&log, &logReq)
	log.ID = primitive.NewObjectID()
	log.Date = time.Now()

	if _, err = database.Database.Collection(setting.IntegrationLogTable).InsertOne(ctx, log); err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Integration Log ERROR - "+err.Error())
		return
	}

	filter := bson.M{
		"date": bson.M{"$lte": time.Now().AddDate(0, 0, -10)},
	}

	_, err = database.Database.Collection(setting.IntegrationLogTable).DeleteMany(ctx, filter)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete Old Integration Log ERROR - "+err.Error())
		return
	}

	data := bson.M{"integration_log": log}
	help.SuccessResponse(response, data, "Create Integration Log successfully")
}

// Get List of Integration Log  godoc
// @Tags Integration Log
// @Summary Get List of Integration Log
// @Description Get List of Integration Log
// @Accept  json
// @Produce  json
// @Param service_name path string false "Service Name"
// @Param function_name path string false "Function Name"
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Param from_date path int false "From date Unix time."
// @Param to_date path int false "To date Unix time."
// @Success 200 {object} model.IntegrationLog
// @Header 200 {string} Token "qwerty"
// @Router /IntegrationLog [GET]
func GetIntegrationLog(response http.ResponseWriter, request *http.Request) {
	defer help.RecoverError(response, "GetServiceStatus")

	var (
		logs     []model.IntegrationLog
		andArray bson.A
	)

	response.Header().Set("content-type", "application/json")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, page, err := help.GetLimitAndPage(request)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	serviceName := request.URL.Query().Get("service_name")
	if serviceName != "" {
		andArray = append(andArray, bson.M{"service_name": sHelper.RegexStringID(serviceName)})
	}

	functionName := request.URL.Query().Get("function_name")
	if functionName != "" {
		andArray = append(andArray, bson.M{"function_name": sHelper.RegexStringID(functionName)})
	}

	log1 := request.URL.Query().Get("log_1")
	if log1 != "" {
		andArray = append(andArray, bson.D{{"log_1", primitive.Regex{Pattern: log1, Options: "i"}}})
	}

	log2 := request.URL.Query().Get("log_2")
	if log2 != "" {
		andArray = append(andArray, bson.D{{"log_2", primitive.Regex{Pattern: log2, Options: "i"}}})
	}

	log3 := request.URL.Query().Get("log_3")
	if log3 != "" {
		andArray = append(andArray, bson.D{{"log_3", primitive.Regex{Pattern: log3, Options: "i"}}})
	}

	fromDate, toDate, err := help.GetFromAndToDate(request)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Get From and To Date ERROR - "+err.Error())
		return
	}

	andArray = append(andArray, bson.M{
		"date": bson.M{
			"$gte": time.Unix(fromDate, 0),
			"$lte": time.Unix(toDate, 0),
		},
	})

	filter := bson.M{"$and": andArray}

	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))

	cursor, err := database.Database.Collection(setting.IntegrationLogTable).Find(ctx, filter, findOptions)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Integration Log ERROR - "+err.Error())
		return
	}

	if err = cursor.All(context.TODO(), &logs); err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch cursor for Integration Log ERROR - "+err.Error())
		return
	}

	data := bson.M{"integration_log": logs}
	help.SuccessResponse(response, data, "Get Services status successfully")
}

// Get Count of Integration Log  godoc
// @Tags Integration Log
// @Summary Get count of Integration Log
// @Description Get count of Integration Log
// @Accept  json
// @Produce  json
// @Param service_name path string false "Service Name"
// @Param function_name path string false "Function Name"
// @Param from_date path int false "From date Unix time."
// @Param to_date path int false "To date Unix time."
// @Success 200 {object} []model.IntegrationLog
// @Header 200 {string} Token "qwerty"
// @Router /IntegrationLog/count [GET]
func GetCountIntegrationLog(response http.ResponseWriter, request *http.Request) {
	defer help.RecoverError(response, "GetServiceStatus")

	var (
		andArray bson.A
	)

	response.Header().Set("content-type", "application/json")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	serviceName := request.URL.Query().Get("service_name")
	if serviceName != "" {
		andArray = append(andArray, bson.M{"service_name": primitive.Regex{Pattern: serviceName, Options: "i"}})
	}

	functionName := request.URL.Query().Get("function_name")
	if functionName != "" {
		andArray = append(andArray, bson.M{"function_name": primitive.Regex{Pattern: functionName, Options: "i"}})
	}

	log1 := request.URL.Query().Get("log_1")
	if log1 != "" {
		andArray = append(andArray, bson.D{{"log_1", primitive.Regex{Pattern: log1, Options: "i"}}})
	}

	log2 := request.URL.Query().Get("log_2")
	if log2 != "" {
		andArray = append(andArray, bson.D{{"log_2", primitive.Regex{Pattern: log2, Options: "i"}}})
	}

	log3 := request.URL.Query().Get("log_3")
	if log3 != "" {
		andArray = append(andArray, bson.D{{"log_3", primitive.Regex{Pattern: log3, Options: "i"}}})
	}

	fromDate, toDate, err := help.GetFromAndToDate(request)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Get From and To Date ERROR - "+err.Error())
		return
	}

	andArray = append(andArray, bson.M{
		"date": bson.M{
			"$gte": time.Unix(fromDate, 0),
			"$lte": time.Unix(toDate, 0),
		},
	})

	filter := bson.M{"$and": andArray}

	count, err := database.Database.Collection(setting.IntegrationLogTable).CountDocuments(ctx, filter)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Count Integration Log ERROR - "+err.Error())
		return
	}

	data := bson.M{"count": count}
	help.SuccessResponse(response, data, "Get Count Integration Log successfully")
}
