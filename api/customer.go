package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"../database"
	"../service"
	"../setting"
	helper "./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

// Add Customer godoc
// @Tags Customer
// @Summary Add Customer
// @Description Add Customer
// @Accept  json
// @Produce  json
// @Param body body model.CustomerRequest true "Add Customer"
// @Success 200 {object} model.Customer
// @Header 200 {string} Token "qwerty"
// @Router /Customer [POST]
func CreateCustomer(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "CreateCustomer")
	var (
		collection     *mongo.Collection
		customer       model.Customer
		customerReq    model.CustomerRequest
		billAddress    model.Address
		shipAddress    model.Address
		sequenceNumber model.CustomerAutoIncrement
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.CustomerTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&customerReq)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	//Set current time
	currentTime := time.Now()

	_ = copier.Copy(&customer, &customerReq)
	customer.ID = primitive.NewObjectID()
	customer.ObjectType = setting.CustomerTable

	_ = copier.Copy(&billAddress, &customerReq.DefaultBillingAddress)

	_ = copier.Copy(&shipAddress, &customerReq.DefaultShippingAddress)

	// Billing Address
	billAddress.ID = primitive.NewObjectID()
	billAddress.SourceObjectType = setting.CustomerTable
	billAddress.SourceKey = customer.ID
	billAddress.AddressType = "Default Billing"
	billAddress.IsDeleted = false
	billAddress.CreatedDate = currentTime
	billAddress.UpdatedDate = currentTime

	//Shipping Address
	shipAddress.ID = primitive.NewObjectID()
	shipAddress.SourceObjectType = setting.CustomerTable
	shipAddress.SourceKey = customer.ID
	shipAddress.AddressType = "Default Shipping"
	shipAddress.IsDeleted = false
	shipAddress.CreatedDate = currentTime
	shipAddress.UpdatedDate = currentTime

	//Customer
	customer.DefaultBillingAddressKey = billAddress.ID
	customer.DefaultShippingAddressKey = shipAddress.ID
	customer.IsDeleted = false
	customer.CreatedDate = currentTime
	customer.UpdatedDate = currentTime

	if err = collection.FindOne(ctx, bson.M{"_id": "customer_number"}).Decode(&sequenceNumber); err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Sequence number of Customer ERROR - "+err.Error())
		return
	}

	customer.CustomerNumber, err = service.GetCustomerSequenceNumber()

	// Insert customer
	_, err := collection.InsertOne(ctx, customer)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Customer ERROR - "+err.Error())
		return
	}

	collection = database.Database.Collection(setting.AddressTable)
	_, err = collection.InsertOne(ctx, billAddress)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Billing Address ERROR - "+err.Error())
		return
	}

	_, err = collection.InsertOne(ctx, shipAddress)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Shipping Address ERROR - "+err.Error())
		return
	}

	if err = service.UpdateCustomerSequenceNumber(); err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Customer Sequence Number ERROR - "+err.Error())
		return
	}

	data := bson.M{"customer": customer}
	helper.SuccessResponse(response, data, "Create Customer successfully")
}

// Get Customer By Id godoc
// @Tags Customer
// @Summary Get Customer by Id
// @Description Get Customer by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Customer need to be found"
// @Success 200 {object} model.Customer
// @Header 200 {string} Token "qwerty"
// @Router /Customer/{id} [get]
func GetCustomerById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "GetCustomers")
	//Get Limit and Page
	var (
		collection    = database.Database.Collection(setting.CustomerTable)
		customer      model.CustomerWithDetails
		lookupAddress bson.D
	)

	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	b := []byte(fmt.Sprintf(`{"$lookup":{"from":"%s","let":{"id":"$_id"},"pipeline":[{"$match":{"$expr":{"$and":[{"$eq":["$source_object_type","customer"]},{"$eq":["$source_key","$$id"]}]}}}],"as":"addresses"}}
		`, setting.AddressTable))
	err = bson.UnmarshalExtJSON(b, true, &lookupAddress)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Unmarshal JSON ERROR - "+err.Error())
		return
		return
	}
	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.D{{"is_deleted", false}},
				bson.D{{"_id", bsonx.ObjectID(id)}},
			},
			},
		},
	}}

	lookupDefaultBillingAddress := bson.D{
		{"$lookup", bson.D{
			{"from", setting.AddressTable},
			{"localField", "default_billing_address_key"},
			{"foreignField", "_id"},
			{"as", "default_billing_address"},
		},
		},
	}

	lookupDefaultShippingAddress := bson.D{
		{"$lookup", bson.D{
			{"from", setting.AddressTable},
			{"localField", "default_shipping_address_key"},
			{"foreignField", "_id"},
			{"as", "default_shipping_address"},
		},
		},
	}

	unwindBillingAddress := bson.D{
		{"$unwind", bson.D{
			{"path", "$default_billing_address"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindShippingAddress := bson.D{
		{"$unwind", bson.D{
			{"path", "$default_shipping_address"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	sort := bson.D{{"$sort", bson.D{{"created_date", -1}}}}

	pipeline := mongo.Pipeline{
		match,
		lookupAddress,
		lookupDefaultBillingAddress,
		lookupDefaultShippingAddress,
		unwindBillingAddress,
		unwindShippingAddress,
		sort,
	}
	//cursor, err := collection.Find(ctx, filter, findOptions)
	//if err != nil {
	//	response.WriteHeader(http.StatusInternalServerError)
	//	response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
	//	return
	//}
	cursor, err := collection.Aggregate(ctx, pipeline)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		_ = cursor.Decode(&customer)
	}
	if err := cursor.Err(); err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"customer": customer}
	helper.SuccessResponse(response, data, "Get Customer successfully")
}

// Get number of Customers godoc
// @Tags Customer
// @Summary Get number of Customers sort by created_date field descending
// @Description Get number of Customers depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.Customer
// @Header 200 {string} Token "qwerty"
// @Router /Customer [get]
func GetCustomers(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "GetCustomers")
	//Get Limit and Page
	var (
		collection    = database.Database.Collection(setting.CustomerTable)
		customer      model.CustomerWithDetails
		allCustomer   []model.CustomerWithDetails
		lookupAddress bson.D
	)

	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, page, err := helper.GetLimitAndPage(request)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	// Test
	b := []byte(fmt.Sprintf(`{"$lookup":{"from":"%s","let":{"id":"$_id"},"pipeline":[{"$match":{"$expr":{"$and":[{"$eq":["$source_object_type","customer"]},{"$eq":["$source_key","$$id"]}]}}}],"as":"addresses"}}
		`, setting.AddressTable))
	err = bson.UnmarshalExtJSON(b, true, &lookupAddress)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Unmarshal JSON ERROR - "+err.Error())
		return
	}
	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.D{{"is_deleted", false}},
			},
			},
		},
	}}

	lookupDefaultBillingAddress := bson.D{
		{"$lookup", bson.D{
			{"from", setting.AddressTable},
			{"localField", "default_billing_address_key"},
			{"foreignField", "_id"},
			{"as", "default_billing_address"},
		},
		},
	}

	lookupDefaultShippingAddress := bson.D{
		{"$lookup", bson.D{
			{"from", setting.AddressTable},
			{"localField", "default_shipping_address_key"},
			{"foreignField", "_id"},
			{"as", "default_shipping_address"},
		},
		},
	}
	unwindBillingAddress := bson.D{
		{"$unwind", bson.D{
			{"path", "$default_billing_address"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindShippingAddress := bson.D{
		{"$unwind", bson.D{
			{"path", "$default_shipping_address"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	sort := bson.D{{"$sort", bson.D{{"created_date", -1}}}}
	skip := bson.D{{"$skip", limit * (page - 1)}}
	lim := bson.D{{"$limit", limit}}

	pipeline := mongo.Pipeline{
		match,
		lookupDefaultBillingAddress,
		lookupDefaultShippingAddress,
		unwindShippingAddress,
		unwindBillingAddress,
		lookupAddress,
		sort,
		skip,
		lim,
	}

	cursor, err := collection.Aggregate(ctx, pipeline)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		cursor.Decode(&customer)
		allCustomer = append(allCustomer, customer)
	}
	if err := cursor.Err(); err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"customer": allCustomer}
	helper.SuccessResponse(response, data, "Get Customer successfully")
}

// Update Customer godoc
// @Tags Customer
// @Summary Update Customer
// @Description Update Customer
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Customer need to be found"
// @Param body body model.CustomerRequest true "Update Customer"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Customer/{id} [put]
func UpdateCustomer(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "UpdateCustomer")
	var (
		customerCollection *mongo.Collection
		addressCollection  *mongo.Collection
		customer           model.Customer
		customerReq        model.CustomerRequest
		billAddress        model.Address
		shipAddress        model.Address
		currentTime        = time.Now()
	)
	response.Header().Set("content-type", "application/json")

	customerCollection = database.Database.Collection(setting.CustomerTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//Decode Body to Customer Request.
	err = json.NewDecoder(request.Body).Decode(&customerReq)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	customerFilter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err = customerCollection.FindOne(ctx, customerFilter).Decode(&customer)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Customer ERROR - "+err.Error())
		return
	}

	_ = copier.Copy(&customer, &customerReq)
	customer.UpdatedDate = time.Now()

	//Get Bill and Ship Address
	addressCollection = database.Database.Collection(setting.AddressTable)

	billFilter := bson.M{"_id": bsonx.ObjectID(customer.DefaultBillingAddressKey)}
	shipFilter := bson.M{"_id": bsonx.ObjectID(customer.DefaultShippingAddressKey)}

	err = addressCollection.FindOne(ctx, billFilter).Decode(&billAddress)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Bill Address ERROR - "+err.Error())
		return
	}

	err = addressCollection.FindOne(ctx, shipFilter).Decode(&shipAddress)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Ship Address ERROR - "+err.Error())
		return
	}

	// Copy from customer Request to Bill Address
	_ = copier.Copy(&billAddress, &customerReq.DefaultBillingAddress)
	billAddress.AddressType = "Default Billing"
	billAddress.SourceObjectType = setting.CustomerTable
	billAddress.SourceKey = customer.ID
	billAddress.UpdatedDate = currentTime

	_ = copier.Copy(&shipAddress, &customerReq.DefaultShippingAddress)
	shipAddress.AddressType = "Default Shipping"
	shipAddress.SourceObjectType = setting.CustomerTable
	shipAddress.SourceKey = customer.ID
	shipAddress.UpdatedDate = currentTime

	customerUpdate := bson.M{"$set": customer}
	billUpdate := bson.M{"$set": billAddress}
	shipUpdate := bson.M{"$set": shipAddress}

	_, err = customerCollection.UpdateOne(ctx, customerFilter, customerUpdate)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Customer ERROR - "+err.Error())
		return
	}

	_, err = addressCollection.UpdateOne(ctx, billFilter, billUpdate)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Billing Address ERROR - "+err.Error())
		return
	}

	_, err = addressCollection.UpdateOne(ctx, shipFilter, shipUpdate)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Shipping Address ERROR - "+err.Error())
		return
	}

	data := bson.M{"customer": customer}
	helper.SuccessResponse(response, data, "Update Customer successfully")
}

// Soft Delete Customer godoc
// @Tags Customer
// @Summary Soft Delete Customer
// @Description Soft Delete Customer
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Customer need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Customer/{id} [delete]
func DeleteSoftCustomer(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "DeleteSoftCustomer")
	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.CustomerTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete Customer ERROR - "+err.Error())
		return
	}

	helper.SuccessResponse(response, id, "Delete Customer successfully")
}

//DeleteHardCustomer
/*
func DeleteHardCustomer(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "DeleteHardCustomer")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.CustomerTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
//-------------------- Unused -----------------------
func CreateManyCustomer(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "CreateManyCustomer")
	var (
		collection   *mongo.Collection
		manyCustomer []model.Customer
		ui           []interface{}
	)

	response.Header().Set("content-type", "application/json")
	collection = database.Database.Collection(setting.CustomerTable)

	err = json.NewDecoder(request.Body).Decode(&manyCustomer)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
		return
	}
	for _, customer := range manyCustomer {
		currentTime := time.Now()
		customer.ID = primitive.NewObjectID()
		customer.CreatedDate = currentTime
		customer.UpdatedDate = currentTime
		ui = append(ui, customer)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
		return
	}
	data := bson.M{"customer": ui}
	helper.SuccessResponse(response, data, "Get Customer successfully")
}

func GetCustomerByIdOld(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "GetCustomerById")
	var (
		collection *mongo.Collection
		customer   model.Customer
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.CustomerTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	err := collection.FindOne(ctx, filter).Decode(&customer)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	data := bson.M{"customer": customer}
	helper.SuccessResponse(response, data, "Get Customer successfully")
}
