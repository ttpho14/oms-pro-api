package api

import (
	"context"
	"net/http"
	"time"

	aHelper "../api/api_helper"
	"../database"
	"../model/enum"
	"../service"
	sHelper "../service/helper"
	"../setting"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/bsontype"
	"go.mongodb.org/mongo-driver/mongo"
)

func GetDashBoard(response http.ResponseWriter, request *http.Request) {
	var (
		sales             bson.M
		orderCancellation bson.M
		returnObj         bson.M
		period            enum.DashboardPeriod
	)
	response.Header().Set("content-type", "application/json")
	coll := database.Database.Collection(setting.SalesOrderTable)

	/*
		facetSalesOrder: {
		      "categorizedByTags": [
		        { $unwind: "$tags" },
		        { $sortByCount: "$tags" }
		      ],
	*/

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	p := request.URL.Query().Get("period")
	if p != "" {
		period, err = enum.DashboardPeriod(p).IsValid()
		if err != nil {
			aHelper.ErrorResponse(response, http.StatusInternalServerError, 0, "Period is invalid - "+err.Error())
			return
		}
	}
	countryId := request.URL.Query().Get("country_id")
	salesChannelId := request.URL.Query().Get("sales_channel_id")

	matchFilter, orderDetailGroup, dateDesc, err := service.DashboardMakeFilter(period, countryId, salesChannelId)
	if err != nil {
		aHelper.ErrorResponse(response, http.StatusInternalServerError, 0, "Make filter for Dashboard ERROR - "+err.Error())
		return
	}
	//Look up for foreign key

	lookupSite := bson.D{
		{"$lookup", bson.M{
			"from":         setting.SiteTable,
			"localField":   "site_id",
			"foreignField": "site_id",
			"as":           setting.SiteTable,
		},
		},
	}
	unwindSite := bson.D{
		{"$unwind", bson.M{
			"path":                       "$" + setting.SiteTable,
			"preserveNullAndEmptyArrays": true,
		},
		},
	}
	lookupCountry := bson.D{
		{"$lookup", bson.M{
			"from":         setting.CountryTable,
			"localField":   "site.country_key",
			"foreignField": "_id",
			"as":           setting.CountryTable + "_from_site",
		},
		},
	}
	unwindCountry := bson.D{
		{"$unwind", bson.M{
			"path":                       "$" + setting.CountryTable + "_from_site",
			"preserveNullAndEmptyArrays": true,
		},
		},
	}
	country := bson.A{
		//bson.M{
		//	"$lookup": bson.M{
		//		"from":         setting.SiteTable,
		//		"localField":   "site_id",
		//		"foreignField": "site_id",
		//		"as":           setting.SiteTable,
		//	},
		//},
		//bson.M{
		//	"$unwind": bson.M{
		//		"path":                       "$" + setting.SiteTable,
		//		"preserveNullAndEmptyArrays": true,
		//	},
		//},
		//bson.M{
		//	"$lookup": bson.M{
		//		"from":         setting.CountryTable,
		//		"localField":   "site.country_key",
		//		"foreignField": "_id",
		//		"as":           setting.CountryTable,
		//	},
		//},
		//bson.M{
		//	"$unwind": bson.M{
		//		"path":                       "$" + setting.CountryTable,
		//		"preserveNullAndEmptyArrays": true,
		//	},
		//},
		bson.M{
			"$group": bson.M{
				"_id":   "$" + setting.CountryTable + "_from_site.country_id",
				"count": bson.M{"$sum": 1},
			},
		},
		bson.M{
			"$project": bson.M{
				"_id":   0,
				"name":  "$_id",
				"count": 1,
			},
		},
	}
	order := bson.A{
		bson.M{
			"$group": bson.M{
				"_id":    bsontype.Null,
				"amount": bson.M{"$sum": "$total_after_tax"},
				"count":  bson.M{"$sum": 1},
			},
		},
		bson.M{
			"$project": bson.M{
				"_id":    0,
				"amount": 1,
				"count":  1,
			},
		},
	}
	otherOrder := bson.A{
		bson.M{"$match": bson.M{
			"order_status": bson.M{
				"$in": bson.A{
					enum.OrderStatusNew,
					enum.OrderStatusOnHold,
					enum.OrderStatusProcessing,
					enum.OrderStatusPicked,
					enum.OrderStatusPacked,
					enum.OrderStatusShipped,
					enum.OrderStatusDelivered,
					enum.OrderStatusClosed,
				},
			},
		},
		},
		bson.M{
			"$group": bson.M{
				"_id":    "$order_status",
				"amount": bson.M{"$sum": "$total_after_tax"},
				"count":  bson.M{"$sum": 1},
			},
		},
		bson.M{
			"$project": bson.M{
				"order_status": "$_id",
				"_id":          0,
				"amount":       1,
				"count":        1,
			},
		},
	}
	//returnOrder := bson.A{
	//	bson.M{
	//		"$lookup": bson.M{
	//			"from":         setting.SiteTable,
	//			"localField":   "site_id",
	//			"foreignField": "site_id",
	//			"as":           setting.SiteTable,
	//		},
	//	},
	//	bson.M{
	//		"$unwind": bson.M{
	//			"path":                       "$" + setting.SiteTable,
	//			"preserveNullAndEmptyArrays": true,
	//		},
	//	},
	//}
	salesChannel := bson.A{
		bson.M{
			"$group": bson.M{
				"_id":   bson.M{"$toUpper": "$sales_channel_id"},
				"count": bson.M{"$sum": 1},
			},
		},
		bson.M{
			"$project": bson.M{
				"_id":   0,
				"name":  "$_id",
				"count": 1,
			},
		},
	}
	orderDetail := bson.A{
		//	{ $group: { _id: { $dayOfYear: "$date"},
		//		click: { $sum: 1 } } }
		//)
		bson.M{
			"$group": bson.M{
				"_id": orderDetailGroup,
				//	bson.M{
				//	"month": bson.M{"$month": "$created_date"},
				//	"day":   bson.M{"$dayOfMonth": "$created_date"},
				//	"year":  bson.M{"$year": "$created_date"},
				//},
				"amount": bson.M{"$sum": "$total_after_tax"},
				"count":  bson.M{"$sum": 1},
			},
		},
		bson.M{
			"$project": bson.M{
				"date": dateDesc,
				//	bson.M{
				//	"$concat": bson.A{
				//		bson.M{"$toString": "$_id.month"},
				//		"/",
				//		bson.M{"$toString": "$_id.day"},
				//		"/",
				//		bson.M{"$toString": "$_id.year"},
				//	},
				//},
				"_id":    0,
				"amount": 1,
				"count":  1,
			}},
	}
	facetSalesOrder := bson.D{
		{"$facet", bson.M{
			"order":         order,
			"other_order":   otherOrder,
			"country":       country,
			"sales_channel": salesChannel,
			"order_detail":  orderDetail,
		}},
	}
	unwindOrder := bson.D{
		{"$unwind", bson.D{
			{"path", "$order"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	//unwindNewOrder := bson.D{
	//	{"$unwind", bson.D{
	//		{"path", "$other_order"},
	//		{"preserveNullAndEmptyArrays", true}},
	//	},
	//}
	pipeLineSalesOrder := mongo.Pipeline{
		lookupSite,
		unwindSite,
		lookupCountry,
		unwindCountry,
		matchFilter,
		facetSalesOrder,
		unwindOrder,
	}
	cursor, err := coll.Aggregate(ctx, pipeLineSalesOrder) //findOptions)
	if err != nil {
		aHelper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Sales Order ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	if cursor.Next(ctx) {
		err := cursor.Decode(&sales)
		if err != nil {
			aHelper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Sales Order with Details ERROR - "+err.Error())
			return
		}
	}
	if err := cursor.Err(); err != nil {
		aHelper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor for Sales Order ERROR - "+err.Error())
		return
	}

	//--------------- CANCEL ORDER ----------------
	count := bson.D{
		{"$count", "count"},
	}
	pipeLineCancelAndReturn := mongo.Pipeline{
		lookupSite,
		unwindSite,
		lookupCountry,
		unwindCountry,
		matchFilter,
		count,
	}

	cursor, err = database.Database.Collection(setting.OrderCancellationTable).Aggregate(ctx, pipeLineCancelAndReturn)
	if err != nil {
		aHelper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Order Cancellation ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	if cursor.Next(ctx) {
		err := cursor.Decode(&orderCancellation)
		if err != nil {
			aHelper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Order Cancellation with Details ERROR - "+err.Error())
			return
		}
	}
	if err := cursor.Err(); err != nil {
		aHelper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor Order Cancellation ERROR - "+err.Error())
		return
	}

	// -------------- RETURN ORDER -----------------

	cursor, err = database.Database.Collection(setting.ReturnTable).Aggregate(ctx, pipeLineCancelAndReturn)
	if err != nil {
		aHelper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Return Order ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	if cursor.Next(ctx) {
		err := cursor.Decode(&returnObj)
		if err != nil {
			aHelper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Return Order with Details ERROR - "+err.Error())
			return
		}
	}
	if err := cursor.Err(); err != nil {
		aHelper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor Return Order ERROR - "+err.Error())
		return
	}

	data := bson.M{
		"sales_order":        sales,
		"order_cancellation": orderCancellation,
		"return":             returnObj,
	}
	aHelper.SuccessResponse(response, data, "Get Dashboard successfully")
}

func GetGlobalSearch(response http.ResponseWriter, request *http.Request) {
	var (
		globalSearch []bson.M
	)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	response.Header().Set("content-type", "application/json")

	keyword := request.URL.Query().Get("keyword")

	limit := bson.D{
		{"$limit", 5},
	}

	tmpLimit := bson.D{
		{"$limit", 1},
	}

	tmpProject := bson.D{
		{"$project", bson.M{
			"_id": 1,
		}},
	}

	lookupSalesOrder := bson.D{
		{"$lookup", bson.M{
			"from": setting.SalesOrderTable,
			"pipeline": bson.A{
				bson.M{
					"$match": bson.M{
						"doc_num": sHelper.RegexStringLike(keyword),
					},
				},
				limit,
				bson.M{
					"$project": bson.M{
						"_id":                0,
						"source_object_type": setting.SalesOrderTable,
						"source_object_id":   "$doc_num",
						"description":        "$doc_num",
					},
				},
			},
			"as": setting.SalesOrderTable,
		}},
	}

	lookupReturn := bson.D{
		{"$lookup", bson.M{
			"from": setting.ReturnTable,
			"pipeline": bson.A{
				bson.M{
					"$match": bson.M{
						"doc_num": sHelper.RegexStringLike(keyword),
					},
				},
				limit,
				bson.M{
					"$project": bson.M{
						"_id":                0,
						"source_object_type": setting.ReturnTable,
						"source_object_id":   "$doc_num",
						"description":        "$doc_num",
					},
				},
			},
			"as": setting.ReturnTable,
		}},
	}

	lookupCancel := bson.D{
		{"$lookup", bson.M{
			"from": setting.OrderCancellationTable,
			"pipeline": bson.A{
				bson.M{
					"$match": bson.M{
						"doc_num": sHelper.RegexStringLike(keyword),
					},
				},
				limit,
				bson.M{
					"$project": bson.M{
						"_id":                0,
						"source_object_type": setting.OrderCancellationTable,
						"source_object_id":   "$doc_num",
						"description":        "$doc_num",
					},
				},
			},
			"as": setting.OrderCancellationTable,
		}},
	}

	lookupCustomer := bson.D{
		{"$lookup", bson.M{
			"from": setting.CustomerTable,
			"pipeline": bson.A{
				bson.M{
					"$match": bson.M{
						"$or": bson.A{
							bson.M{"phone_no": sHelper.RegexStringLike(keyword)},
							bson.M{"email": sHelper.RegexStringLike(keyword)},
						},
					},
				},
				limit,
				bson.M{
					"$project": bson.M{
						"_id":                0,
						"source_object_type": setting.CustomerTable,
						"source_object_id":   "$email",
						"description":        "$phone_no",
					},
				},
			},
			"as": setting.CustomerTable,
		}},
	}

	lookupProduct := bson.D{
		{"$lookup", bson.M{
			"from": setting.ProductTable,
			"pipeline": bson.A{
				bson.M{
					"$match": bson.M{
						"barcode": sHelper.RegexStringLike(keyword),
					},
				},
				limit,
				bson.M{
					"$project": bson.M{
						"_id":                0,
						"source_object_type": setting.ProductTable,
						"source_object_id":   "$product_id",
						"description":        "$short_description",
					},
				},
			},
			"as": setting.ProductTable,
		}},
	}

	project := bson.D{
		{"$project", bson.M{
			"union_all": bson.M{
				"$concatArrays": bson.A{
					"$" + setting.SalesOrderTable,
					"$" + setting.ReturnTable,
					"$" + setting.OrderCancellationTable,
					"$" + setting.ProductTable,
					"$" + setting.CustomerTable,
				},
			},
		}},
	}

	unwind := bson.D{
		{"$unwind", "$union_all"},
	}

	replaceRoot := bson.D{
		{"$replaceRoot", bson.M{
			"newRoot": "$union_all",
		}},
	}

	pipeline := mongo.Pipeline{
		tmpLimit,
		tmpProject,
		lookupSalesOrder,
		lookupReturn,
		lookupCancel,
		lookupCustomer,
		lookupProduct,
		project,
		unwind,
		replaceRoot,
	}

	cursor, err := database.Database.Collection(setting.ApplicationTable).Aggregate(ctx, pipeline)
	if err != nil {
		aHelper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Global Search ERROR - "+err.Error())
		return
	}

	if err = cursor.All(ctx, &globalSearch); err != nil {
		aHelper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Global Search ERROR - "+err.Error())
		return
	}

	data := bson.M{
		"result": globalSearch,
	}
	aHelper.SuccessResponse(response, data, "Get Global Search successfully")
}
