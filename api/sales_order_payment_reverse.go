package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../model/enum"
	"../setting"
	"github.com/jinzhu/copier"

	"../database"
	"../model"
	"./api_helper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateSalesOrderPaymentReverse godoc
// @Tags Sales Order PaymentReverse
// @Summary Add Sales Order PaymentReverse
// @Description Add Sales Order PaymentReverse
// @Accept  json
// @Produce  json
// @Param body body model.SalesOrderPaymentReverseRequest true "Add Sales Order PaymentReverse"
// @Success 200 {object} model.SalesOrderPaymentReverse
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderPaymentReverse [POST]
func CreateSalesOrderPaymentReverse(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateSalesOrderPaymentReverse")
	var (
		collection                  *mongo.Collection
		salesOrderPaymentReverse    model.SalesOrderPaymentReverse
		salesOrderPaymentReverseReq model.SalesOrderPaymentReverseRequest
	)

	collection = database.Database.Collection(setting.SalesOrderPaymentReverseTable)
	response.Header().Set("content-type", "application/json")
	if err = json.NewDecoder(request.Body).Decode(&salesOrderPaymentReverseReq); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Sales Order Payment Capture ERROR - "+err.Error())
		return
	}

	if err = copier.Copy(&salesOrderPaymentReverse, &salesOrderPaymentReverseReq); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	currentTime := time.Now()
	salesOrderPaymentReverse.ID = primitive.NewObjectID()
	salesOrderPaymentReverse.Status = enum.PaymentStatusReversed
	salesOrderPaymentReverse.CreatedDate = currentTime
	salesOrderPaymentReverse.UpdatedDate = currentTime

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err = collection.InsertOne(ctx, salesOrderPaymentReverse)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Sales Order Payment Capture ERROR - "+err.Error())
		return
	}

	filterSalesOrder := bson.M{"_id": bsonx.ObjectID(salesOrderPaymentReverse.DocKey)}
	updateSalesOrder := bson.M{"$set": bson.M{"payment_status": salesOrderPaymentReverse.Status}}
	if _, err = database.Database.Collection(setting.SalesOrderTable).UpdateOne(ctx, filterSalesOrder, updateSalesOrder); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Sales Order Payment Status ERROR - "+err.Error())
		return
	}

	if _, err = database.Database.Collection(setting.OrderCancellationTable).UpdateOne(ctx, bson.M{"_id": bsonx.ObjectID(salesOrderPaymentReverseReq.OrderCancellationKey)}, bson.M{"$set": bson.M{"doc_status": enum.CancelOrderStatusCancelled}}); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Order Cancellation status to [CANCELLED] ERROR - "+err.Error())
		return
	}

	data := bson.M{"sales_order_payment_capture": salesOrderPaymentReverse}
	api_helper.SuccessResponse(response, data, "Create Sales Order Payment Capture successfully")
}
