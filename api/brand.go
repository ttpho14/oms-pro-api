package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"reflect"
	_ "strconv"
	"time"

	"../database"
	"../setting"
	help "./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

// Add Brand godoc
// @Tags Brand
// @Summary Add Brand
// @Description Add Brand
// @Accept  json
// @Produce  json
// @Param body body model.BrandRequest true "Add Brand"
// @Success 200 {object} model.Brand
// @Header 200 {string} Token "qwerty"
// @Router /Brand [POST]
func CreateBrand(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "CreateBrand")
	var (
		collection           *mongo.Collection
		brand                model.Brand
		brandReq             model.BrandRequest
		brandRegistration    model.BrandRegistration
		uiBrandRegistrations []interface{}
	)

	response.Header().Set("content-type", "application/json")
	//Decode to Brand Input
	collection = database.Database.Collection(setting.BrandTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&brandReq)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	filter := bson.D{{"brand_id", brandReq.BrandId}, {"is_deleted", false}}

	// Validate unique Id
	count, err := collection.CountDocuments(ctx, filter)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	if count > 0 {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Brand Id is already exists")
		return
	}

	// Copy Brand Input to Brand
	err = copier.Copy(&brand, &brandReq)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	// Assign others value to Brand
	currentDate := time.Now()
	// If BrandID is null, it means no record in database ==> Assign Default value to Brand
	brand.ID = primitive.NewObjectID()
	brand.CreatedDate = currentDate
	brand.UpdatedDate = currentDate

	_, err = collection.InsertOne(ctx, brand)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	//Brand Registration from Brand Input
	for _, brandRegistrationInput := range brandReq.BrandRegistrationRequest {
		//Copy fields value from Brand Registration Input model to main model - copy( mainModel, inputModel)
		err = copier.Copy(&brandRegistration, &brandRegistrationInput)
		if err != nil {
			help.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
			return
		}
		// Assign others value to main model.
		currentTime := time.Now()
		brandRegistration.BrandKey = brand.ID
		brandRegistration.ID = primitive.NewObjectID()
		brandRegistration.CreatedDate = currentTime
		brandRegistration.UpdatedDate = currentTime
		brandRegistration.IsDeleted = false

		uiBrandRegistrations = append(uiBrandRegistrations, brandRegistration)
	}

	if len(uiBrandRegistrations) > 0 {
		collection = database.Database.Collection(setting.BrandRegistrationTable)
		_, err = collection.InsertMany(ctx, uiBrandRegistrations)
		if err != nil {
			help.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Brand Registration ERROR - "+err.Error())
			return
		}
	}

	data := bson.M{
		"brand":               brand,
		"brand_registrations": uiBrandRegistrations,
	}
	help.SuccessResponse(response, data, "Insert Brand successfully")
}

// Get Brand By Id godoc
// @Tags Brand
// @Summary Get Brand by Id
// @Description Get Brand by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Brand need to be found"
// @Success 200 {object} model.Brand
// @Header 200 {string} Token "qwerty"
// @Router /Brand/{id} [get]
func GetBrandById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "GetBrandById")
	var (
		collection *mongo.Collection
		brand      model.BrandWithDetails
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.BrandTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//configurationId = id and is_deleted = false
	//filter := bson.D{{"_id", bsonx.ObjectID(id)}, {"is_deleted", false}}

	//----------------------------
	//_id = id and is_deleted = false
	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.D{{"is_deleted", false}},
				bson.D{{"_id", bsonx.ObjectID(id)}},
			},
			},
		},
	},
	}
	// lookup = join table on ....
	lookupBrandRegistration := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.BrandRegistrationTable},
				{"localField", "_id"},
				{"foreignField", "brand_key"},
				{"as", "brand_registrations"},
			},
		},
	}
	pipeline := mongo.Pipeline{
		match,
		lookupBrandRegistration,
	}

	cursor, err := collection.Aggregate(ctx, pipeline) //findOptions)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	cursor.Next(ctx)
	err = cursor.Decode(&brand)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"brand": brand}
	help.SuccessResponse(response, data, "Get Brand By Id successfully")
}

// Get number of Brands godoc
// @Tags Brand
// @Summary Get number of Brands sort by created_date field descending
// @Description Get number of Brands depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.Brand
// @Header 200 {string} Token "qwerty"
// @Router /Brand [get]
func GetBrands(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "GetBrands")
	var (
		brand    model.BrandWithDetails
		allBrand []model.BrandWithDetails
	)

	response.Header().Set("content-type", "application/json")
	collection := database.Database.Collection(setting.BrandTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := help.GetLimitAndPage(request)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	// match = Where
	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.D{
					{"is_deleted", false},
				},
			},
			},
		},
	}}
	// lookup = join table on ....
	lookupBrandRegistration := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.BrandRegistrationTable},
				{"localField", "_id"},
				{"foreignField", "brand_key"},
				{"as", "brand_registrations"},
			},
		},
	}
	// Sort = Order by , "1" = asc , "-1" = desc
	sort := bson.D{{"$sort", bson.D{{"created_date", -1}}}}
	// Skip = Skip number of record
	skip := bson.D{{"$skip", limit * (page - 1)}}
	// lim = Limit number of record in 1 page.
	lim := bson.D{{"$limit", limit}}
	pipeline := mongo.Pipeline{
		match,
		lookupBrandRegistration,
		sort,
		skip,
		lim,
	}

	cursor, err := collection.Aggregate(ctx, pipeline) //findOptions)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		cursor.Decode(&brand)
		allBrand = append(allBrand, brand)
	}
	if err := cursor.Err(); err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch ERROR - "+err.Error())
		return
	}

	data := bson.M{"brand": allBrand}
	help.SuccessResponse(response, data, "Get Brand successfully")
}

// Update Brand godoc
// @Tags Brand
// @Summary Update Brand
// @Description Update Brand
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Brand need to be found"
// @Param body body model.BrandRequest true "Update Brand"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Brand/{id} [put]
func UpdateBrand(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "UpdateBrand")
	// Initial Values.
	var (
		collection *mongo.Collection
		brand      model.Brand
		brandReq   model.BrandRequest
		filter     bson.D
	)

	// Set response header
	response.Header().Set("content-type", "application/json")

	//Collection
	collection = database.Database.Collection(setting.BrandTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Get Id Param from request URL
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	// Filter to find Brand by Id, is_deleted = false
	filter = bson.D{
		{"_id", bsonx.ObjectID(id)},
		{"is_deleted", false},
	}

	//Find Brand By Id
	err = collection.FindOne(ctx, filter).Decode(&brand)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError,0, "Find ERROR - "+err.Error())
		return
	}

	// Decode Request.Body to Input Model
	err = json.NewDecoder(request.Body).Decode(&brandReq)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError,0, "Decode ERROR - "+err.Error())
		return
	}

	// Copy fields Value from Input Model to Main Model
	err = copier.Copy(&brand, &brandReq)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError,0, "Copy ERROR - "+err.Error())
		return
	}
	// Assign Main Model's values
	brand.UpdatedDate = time.Now()

	//Update Brand
	update := bson.D{{"$set", brand}}
	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError,0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"brand": brand}
	help.SuccessResponse(response, data, "Update Brand successfully")
}

// Soft Delete Brand godoc
// @Tags Brand
// @Summary Soft Delete Brand
// @Description Soft Delete Brand
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Brand need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Brand/{id} [delete]
func DeleteSoftBrand(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "DeleteSoftBrand")
	var collection *mongo.Collection
	response.Header().Set("content-type", "application/json")
	collection = database.Database.Collection(setting.BrandTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	help.SuccessResponse(response, id, "Delete Brand successfully")
}

//-------------------- Un use-----------------------
func ImportBrand(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "CreateBrand")
	response.Header().Set("content-type", "application/json")
	var (
		collection           *mongo.Collection
		brand                model.Brand
		brandReq             model.BrandRequest
		brandRegistration    model.BrandRegistration
		uiBrandRegistrations []interface{}
	)
	//Decode to Brand Input
	err = json.NewDecoder(request.Body).Decode(&brandReq)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	opts := options.Update().SetUpsert(true)
	filter := bson.D{{"brand_id", brandReq.BrandId}, {"is_deleted", false}}

	collection = database.Database.Collection(setting.BrandTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Get value from DB and copy to Brand
	_ = collection.FindOne(ctx, filter).Decode(&brand)

	// Copy Brand Input to Brand
	err = copier.Copy(&brand, &brandReq)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	// Assign others value to Brand
	currentDate := time.Now()
	// If BrandID is null, it means no record in database ==> Assign Default value to Brand
	if primitive.ObjectID.IsZero(brand.ID) {
		brand.ID = primitive.NewObjectID()
		brand.CreatedDate = currentDate
	}
	brand.UpdatedDate = currentDate

	update := bson.D{{"$set", brand}}

	result, err := collection.UpdateOne(ctx, filter, update, opts)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	// If Match document => Update and result
	var msg string
	if result.MatchedCount != 0 {
		msg = "Match 1 document. Updated Brand successfully."
	} else { // else if MatchedCount = 0 => UpsertedCount != 0
		msg = "Match 0 document. Inserted Brand successfully"
	}

	//Brand Registration from Brand Input
	for _, brandRegistrationInput := range brandReq.BrandRegistrationRequest {
		//Copy fields value from Brand Registration Input model to main model - copy( mainModel, inputModel)
		err = copier.Copy(&brandRegistration, &brandRegistrationInput)
		if err != nil {
			help.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
			return
		}
		// Assign others value to main model.
		currentTime := time.Now()
		brandRegistration.BrandKey = brand.ID
		brandRegistration.ID = primitive.NewObjectID()
		brandRegistration.CreatedDate = currentTime
		brandRegistration.UpdatedDate = currentTime
		brandRegistration.IsDeleted = false

		uiBrandRegistrations = append(uiBrandRegistrations, brandRegistration)
	}

	collection = database.Database.Collection(setting.BrandRegistrationTable)
	// Sales Order Detail
	_, err = collection.InsertMany(ctx, uiBrandRegistrations)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Many ERROR - "+err.Error())
		return
	}

	//res.Data = bson.M{
	//	"brand":               brand,
	//	"brand_registrations": uiBrandRegistrations,
	//}d

	data := bson.M{
		"brand":               brand,
		"brand_registrations": uiBrandRegistrations,
	}
	help.SuccessResponse(response, data, msg)
	//// Insert
	//if count == 0 {
	//	_, err := collection.InsertOne(ctx, brand)
	//	if err != nil {
	//		response.WriteHeader(http.StatusBadRequest)
	//		json.NewEncoder(response).Encode(bson.M{"message": err.Error()})
	//		return
	//	}
	//
	//	json.NewEncoder(response).Encode(brand)
	//} else { // Update
	//	update := bson.D{{"$set", brand}}
	//
	//	result, err := collection.UpdateOne(ctx, filter, update)
	//	if err != nil {
	//		response.WriteHeader(http.StatusBadRequest)
	//		json.NewEncoder(response).Encode(bson.M{"message": err.Error()})
	//		return
	//	}
	//
	//	json.NewEncoder(response).Encode(result)
	//}

}

func CreateManyBrand(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "CreateManyBrand")
	var (
		collection     *mongo.Collection
		manyBrandInput []model.BrandRequest
		brand          model.Brand
	)
	collection = database.Database.Collection(setting.BrandTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&manyBrandInput)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	var ui []interface{}
	for _, brandReq := range manyBrandInput {
		//help.ValidateBrand(brandReq)
		err = copier.Copy(&brand, &brandReq)
		if err != nil {
			response.WriteHeader(http.StatusBadRequest)
			response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
			return
		}
		currentTime := time.Now()

		brand.ID = primitive.NewObjectID()
		brand.CreatedDate = currentTime
		brand.UpdatedDate = currentTime
		brand.IsActive = true
		brand.IsDeleted = false

		ui = append(ui, brand)
	}

	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

func GetBrandsTest(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "GetBrands")
	//Get Id if exists
	var (
		collection  *mongo.Collection
		limit       int64
		page        int64
		filter      bson.D
		findOptions = options.Find()
	)
	fields := help.GetFields(model.Brand{})
	for _, field := range fields {
		value := request.URL.Query().Get(field.FieldName)
		if value != "" {
			// Add filter brand_id to value
			filter, err = help.ParseValueToFilter(filter, field.FieldName, value, field.DataType)
		}
	}
	filter = append(filter, bson.E{Key: "is_deleted", Value: false})
	//Get Limit and Page
	limit, err = help.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err = help.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})

	collection = database.Database.Collection(setting.BrandTable)
	response.Header().Set("content-type", "application/json")
	var allBrand []model.Brand
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var brand model.Brand
		cursor.Decode(&brand)
		allBrand = append(allBrand, brand)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(allBrand)
}

func DeleteHardBrand(response http.ResponseWriter, request *http.Request) {
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.BrandTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
