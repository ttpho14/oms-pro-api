package api

import (
	"context"
	"encoding/json"
	"net/http"
	"os"
	"strconv"
	"time"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

var DELIVERY_TABLE_NAME = "delivery"

// CreateDelivery godoc
// @Tags Delivery
// @Summary Add Delivery
// @Description Add Delivery
// @Accept  json
// @Produce  json
// @Param body body []model.AddDelivery true "Add Delivery"
// @Success 200 {object} []model.Delivery
// @Header 200 {string} Token "qwerty"
// @Router /Delivery [POST]
func CreateDelivery(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateDelivery")
	var (
		collection        *mongo.Collection
		deliveryRequest   model.AddDelivery
		delivery          model.Delivery
		uiDeliveryDetail  []interface{}
		uiDeliveryFreight []interface{}
	)

	collection = database.Database.Collection(DELIVERY_TABLE_NAME)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&deliveryRequest)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	copier.Copy(&delivery, &deliveryRequest)

	currentTime := time.Now()
	delivery.CreatedDate = currentTime
	delivery.UpdatedDate = currentTime
	delivery.ID = primitive.NewObjectID()
	delivery.DocKey = delivery.ID
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	result, err := collection.InsertOne(ctx, delivery)
	if err != nil {
		json.NewEncoder(response).Encode(err.Error())
		return
	}

	id := result.InsertedID
	delivery.ID = id.(primitive.ObjectID)

	// Delivery Detail
	for _, detail := range deliveryRequest.AddDeliveryDetail {
		var (
			deliveryDetail model.DeliveryDetail
		)
		copier.Copy(&deliveryDetail, &detail)
		deliveryDetail.DocKey = delivery.ID
		deliveryDetail.ID = primitive.NewObjectID()
		deliveryDetail.CreatedDate = currentTime
		deliveryDetail.UpdatedDate = currentTime
		uiDeliveryDetail = append(uiDeliveryDetail, deliveryDetail)
	}

	// Delivery Freight
	for _, freight := range deliveryRequest.AddDeliveryFreight {
		var (
			deliveryFreight model.DeliveryFreight
		)
		copier.Copy(&deliveryFreight, &freight)
		deliveryFreight.DocKey = delivery.ID
		deliveryFreight.ID = primitive.NewObjectID()
		deliveryFreight.CreatedDate = currentTime
		deliveryFreight.UpdatedDate = currentTime
		uiDeliveryFreight = append(uiDeliveryFreight, deliveryFreight)
	}

	if len(uiDeliveryDetail) > 0 {
		collection = database.Database.Collection(DELIVERY_DETAIL_TABLE_NAME)
		// Delivery Detail
		_, err = collection.InsertMany(context.TODO(), uiDeliveryDetail)
		if err != nil {
			response.WriteHeader(http.StatusInternalServerError)
			response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
			return
		}
	} else {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + "Please enter at least one line for detail." + `" }`))
		return
	}

	if len(uiDeliveryFreight) > 0 {
		collection = database.Database.Collection(DELIVERY_FREIGHT_TABLE_NAME)
		// Delivery Freight
		_, err = collection.InsertMany(context.TODO(), uiDeliveryFreight)
		if err != nil {
			response.WriteHeader(http.StatusInternalServerError)
			response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
			return
		}
	}

	json.NewEncoder(response).Encode(delivery)
}

// CreateManyDelivery func
func CreateManyDelivery(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManyDelivery")
	var (
		collection        *mongo.Collection
		deliveriesRequest []model.AddDelivery
		uiDelivery        []interface{}
		uiDeliveryDetail  []interface{}
		uiDeliveryFreight []interface{}
	)

	collection = database.Database.Collection(DELIVERY_TABLE_NAME)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&deliveriesRequest)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	currentTime := time.Now()
	for _, deliveryRequest := range deliveriesRequest {
		var (
			delivery model.Delivery
		)
		copier.Copy(&delivery, &deliveryRequest)

		delivery.ID = primitive.NewObjectID()
		delivery.DocKey = delivery.ID
		delivery.CreatedDate = currentTime
		delivery.UpdatedDate = currentTime
		uiDelivery = append(uiDelivery, delivery)

		// Delivery Detail
		for _, detail := range deliveryRequest.AddDeliveryDetail {
			var (
				deliveryDetail model.DeliveryDetail
			)
			copier.Copy(&deliveryDetail, &detail)
			deliveryDetail.DocKey = delivery.ID
			deliveryDetail.ID = primitive.NewObjectID()
			deliveryDetail.CreatedDate = currentTime
			deliveryDetail.UpdatedDate = currentTime
			uiDeliveryDetail = append(uiDeliveryDetail, deliveryDetail)
		}

		// Delivery Freight
		for _, freight := range deliveryRequest.AddDeliveryFreight {
			var (
				deliveryFreight model.DeliveryFreight
			)
			copier.Copy(&deliveryFreight, &freight)
			deliveryFreight.DocKey = delivery.ID
			deliveryFreight.ID = primitive.NewObjectID()
			deliveryFreight.CreatedDate = currentTime
			deliveryFreight.UpdatedDate = currentTime
			uiDeliveryFreight = append(uiDeliveryFreight, deliveryFreight)
		}
	}

	// Delivery
	_, err := collection.InsertMany(context.TODO(), uiDelivery)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	if len(uiDeliveryDetail) > 0 {
		collection = database.Database.Collection(DELIVERY_DETAIL_TABLE_NAME)
		// Delivery Detail
		_, err = collection.InsertMany(context.TODO(), uiDeliveryDetail)
		if err != nil {
			response.WriteHeader(http.StatusInternalServerError)
			response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
			return
		}
	} else {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + "Please enter at least one line for delivery detail." + `" }`))
		return
	}

	if len(uiDeliveryFreight) > 0 {
		collection = database.Database.Collection(DELIVERY_FREIGHT_TABLE_NAME)
		// Delivery Freight
		_, err = collection.InsertMany(context.TODO(), uiDeliveryFreight)
		if err != nil {
			response.WriteHeader(http.StatusInternalServerError)
			response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
			return
		}
	}
	json.NewEncoder(response).Encode(map[string]interface{}{
		"deliverys":         uiDelivery,
		"delivery_details":  uiDeliveryDetail,
		"delivery_freights": uiDeliveryFreight,
	})
}

// GetDeliveryByID godoc
// @Tags Delivery
// @Summary Get Delivery by Id
// @Description Get Delivery by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Delivery need to be found"
// @Success 200 {object} model.Delivery
// @Header 200 {string} Token "qwerty"
// @Router /Delivery/{id} [get]
func GetDeliveryByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetDeliveryByID")
	var (
		collection *mongo.Collection
		delivery   model.AddDelivery
	)
	collection = database.Database.Collection(DELIVERY_TABLE_NAME)

	response.Header().Set("content-type", "application/json")

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}

	err := collection.FindOne(ctx, filter).Decode(&delivery)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	filterChild := bson.M{"doc_key": bson.M{"$eq": bsonx.ObjectID(id)}}

	// Find Detail
	collection = database.Database.Collection(DELIVERY_DETAIL_TABLE_NAME)
	var details []model.AddDeliveryDetail

	cursor, err := collection.Find(ctx, filterChild)
	err = cursor.All(ctx, &details)
	delivery.AddDeliveryDetail = details

	// Find Freight
	collection = database.Database.Collection(DELIVERY_FREIGHT_TABLE_NAME)
	var freight []model.AddDeliveryFreight

	cursor, err = collection.Find(ctx, filterChild)
	err = cursor.All(ctx, &freight)
	delivery.AddDeliveryFreight = freight

	json.NewEncoder(response).Encode(delivery)
}

// GetDeliveries godoc
// @Tags Delivery
// @Summary Get all Delivery
// @Description Get all Delivery
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.Delivery
// @Header 200 {string} Token "qwerty"
// @Router /Delivery [get]
func GetDeliveries(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetDeliveries")
	var (
		collection *mongo.Collection
		deliveries []model.Delivery
		query      = request.URL.Query()
		options    = options.Find()
	)
	collection = database.Database.Collection(DELIVERY_TABLE_NAME)

	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, err := strconv.ParseInt(query.Get("limit"), 10, 64)
	if limit <= 0 {
		limit = 100
	} else if limit >= 500 {
		limit = 500
	}
	options.SetLimit(limit)
	cursor, err := collection.Find(ctx, options)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var delivery model.Delivery
		cursor.Decode(&delivery)
		deliveries = append(deliveries, delivery)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(deliveries)
}

// UpdateDelivery godoc
// @Tags Delivery
// @Summary Update Delivery
// @Description Update Delivery
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Delivery need to be found"
// @Param body body model.AddDelivery true "Update Delivery"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Delivery/{id} [put]
func UpdateDelivery(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateDelivery")
	var (
		collection        *mongo.Collection
		deliveryRequest   model.AddDelivery
		delivery          model.Delivery
		uiDeliveryDetail  []interface{}
		uiDeliveryFreight []interface{}
	)
	collection = database.Database.Collection(DELIVERY_TABLE_NAME)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&deliveryRequest)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	copier.Copy(&delivery, &deliveryRequest)
	currentTime := time.Now()
	delivery.UpdatedDate = currentTime
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": delivery}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	// Delivery Detail
	if len(deliveryRequest.AddDeliveryDetail) > 0 {
		collection = database.Database.Collection(DELIVERY_DETAIL_TABLE_NAME)

		for _, detail := range deliveryRequest.AddDeliveryDetail {
			var (
				deliveryDetail model.DeliveryDetail
			)
			copier.Copy(&deliveryDetail, &detail)
			deliveryDetail.UpdatedDate = currentTime

			if deliveryDetail.ID != primitive.NilObjectID {
				filter = bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(deliveryDetail.ID)}}
				update = bson.M{"$set": deliveryDetail}
				result, err = collection.UpdateOne(ctx, filter, update)
				if err != nil {
					response.WriteHeader(http.StatusInternalServerError)
					response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
					return
				}
			} else {
				uiDeliveryDetail = append(uiDeliveryDetail, deliveryDetail)
			}
		}

		// Insert New
		if len(uiDeliveryDetail) > 0 {
			collection = database.Database.Collection(DELIVERY_DETAIL_TABLE_NAME)
			// Delivery Detail
			_, err = collection.InsertMany(context.TODO(), uiDeliveryDetail)
			if err != nil {
				response.WriteHeader(http.StatusInternalServerError)
				response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
				return
			}
		}
	}

	// Delivery Freight
	if len(deliveryRequest.AddDeliveryFreight) > 0 {
		collection = database.Database.Collection(DELIVERY_FREIGHT_TABLE_NAME)

		for _, freight := range deliveryRequest.AddDeliveryFreight {
			var (
				deliveryFreight model.DeliveryFreight
			)
			copier.Copy(&deliveryFreight, &freight)
			deliveryFreight.UpdatedDate = currentTime

			if deliveryFreight.ID != primitive.NilObjectID {
				filter = bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(deliveryFreight.ID)}}
				update = bson.M{"$set": deliveryFreight}
				result, err = collection.UpdateOne(ctx, filter, update)
				if err != nil {
					response.WriteHeader(http.StatusInternalServerError)
					response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
					return
				}
			} else {
				uiDeliveryFreight = append(uiDeliveryFreight, deliveryFreight)
			}
		}

		// Insert New
		if len(uiDeliveryFreight) > 0 {
			collection = database.Database.Collection(DELIVERY_FREIGHT_TABLE_NAME)
			_, err = collection.InsertMany(context.TODO(), uiDeliveryFreight)
			if err != nil {
				response.WriteHeader(http.StatusInternalServerError)
				response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
				return
			}
		}
	}

	json.NewEncoder(response).Encode(result)
}

func DeleteSoftDelivery(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftDelivery")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(DELIVERY_TABLE_NAME)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
