package api

import (
	"context"
	"encoding/json"
	"net/http"
	"os"
	"time"

	"../setting"
	"github.com/jinzhu/copier"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateOrderCancellationShippingDetail godoc
// @Tags Order Cancellation ShippingDetail
// @Summary Add Order Cancellation ShippingDetail
// @Description Add Order Cancellation ShippingDetail
// @Accept  json
// @Produce  json
// @Param body body model.OrderCancellationShippingDetailRequest true "Add Order Cancellation ShippingDetail"
// @Success 200 {object} model.OrderCancellationShippingDetail
// @Header 200 {string} Token "qwerty"
// @Router /OrderCancellationShippingDetail [POST]
func CreateOrderCancellationShippingDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateOrderCancellationShippingDetail")
	var (
		collection                         *mongo.Collection
		orderCancellationShippingDetail    model.OrderCancellationShippingDetail
		orderCancellationShippingDetailReq model.OrderCancellationShippingDetailRequest
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.OrderCancellationShippingDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&orderCancellationShippingDetailReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Order Cancellation ShippingDetail Input ERROR - "+err.Error())
		return
	}

	_ = copier.Copy(&orderCancellationShippingDetail, &orderCancellationShippingDetailReq)

	currentTime := time.Now()
	orderCancellationShippingDetail.ID = primitive.NewObjectIDFromTimestamp(currentTime)
	orderCancellationShippingDetail.ObjectType = setting.OrderCancellationShippingDetailTable
	orderCancellationShippingDetail.CreatedDate = currentTime
	orderCancellationShippingDetail.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, orderCancellationShippingDetail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Order Cancellation ShippingDetail ERROR - "+err.Error())
		return
	}

	data := bson.M{"order_cancellation_shippingDetail": orderCancellationShippingDetail}
	api_helper.SuccessResponse(response, data, "Create Order Cancellation ShippingDetail successfully")
}

// CreateManyOrderCancellationShippingDetail func
func CreateManyOrderCancellationShippingDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManyOrderCancellationShippingDetail")
	var (
		collection                       *mongo.Collection
		orderCancellationShippingDetails []model.OrderCancellationShippingDetail
	)
	collection = database.Database.Collection(setting.OrderCancellationShippingDetailTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&orderCancellationShippingDetails)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	currentTime := time.Now()
	for _, orderCancellationShippingDetail := range orderCancellationShippingDetails {
		orderCancellationShippingDetail.ID = primitive.NewObjectID()
		orderCancellationShippingDetail.CreatedDate = currentTime
		orderCancellationShippingDetail.UpdatedDate = currentTime
		ui = append(ui, orderCancellationShippingDetail)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// GetOrderCancellationShippingDetailByID godoc
// @Tags Order Cancellation ShippingDetail
// @Summary Get Order Cancellation ShippingDetail by Id
// @Description Get Order Cancellation ShippingDetail by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Order Cancellation ShippingDetail need to be found"
// @Success 200 {object} model.OrderCancellationShippingDetail
// @Header 200 {string} Token "qwerty"
// @Router /OrderCancellationShippingDetail/{id} [get]
func GetOrderCancellationShippingDetailByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSaleOrderShippingDetailByID")
	var (
		collection                      *mongo.Collection
		orderCancellationShippingDetail model.OrderCancellationShippingDetail
	)
	collection = database.Database.Collection(setting.OrderCancellationShippingDetailTable)

	response.Header().Set("content-type", "application/json")

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	err := collection.FindOne(ctx, filter).Decode(&orderCancellationShippingDetail)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(orderCancellationShippingDetail)
}

// GetOrderCancellationShippingDetails godoc
// @Tags Order Cancellation ShippingDetail
// @Summary Get all Order Cancellation ShippingDetail
// @Description Get all Order Cancellation ShippingDetail
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.OrderCancellationShippingDetail
// @Header 200 {string} Token "qwerty"
// @Router /OrderCancellationShippingDetail [get]
func GetOrderCancellationShippingDetails(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetOrderCancellationShippingDetails")
	var (
		collection                       *mongo.Collection
		orderCancellationShippingDetails []model.OrderCancellationShippingDetail
		findOptions                      = options.Find()
		filter                           = bson.M{}
	)
	collection = database.Database.Collection(setting.OrderCancellationShippingDetailTable)

	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions) //findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var orderCancellationShippingDetail model.OrderCancellationShippingDetail
		cursor.Decode(&orderCancellationShippingDetail)
		orderCancellationShippingDetails = append(orderCancellationShippingDetails, orderCancellationShippingDetail)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(orderCancellationShippingDetails)
}

// UpdateOrderCancellationShippingDetail godoc
// @Tags Order Cancellation ShippingDetail
// @Summary Update Order Cancellation ShippingDetail
// @Description Update Order Cancellation ShippingDetail
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Order Cancellation need to be found"
// @Param body body model.OrderCancellationRequest true "Update Order Cancellation ShippingDetail"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /OrderCancellationShippingDetail/{id} [put]
func UpdateOrderCancellationShippingDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateSaleOrderShippingDetail")
	var (
		collection                      *mongo.Collection
		orderCancellationShippingDetail model.OrderCancellationShippingDetail
	)
	collection = database.Database.Collection(setting.OrderCancellationShippingDetailTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&orderCancellationShippingDetail)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	orderCancellationShippingDetail.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": orderCancellationShippingDetail}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

func DeleteSoftOrderCancellationShippingDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftOrderCancellationShippingDetail")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.OrderCancellationShippingDetailTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
