package api

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"reflect"
	_ "strconv"
	"time"

	"../database"
	"../model/enum"
	"../service"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

func CreateIntegrationFile(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateIntegrationFile")
	var (
		collection      *mongo.Collection
		integrationFile model.IntegrationFile
		result          model.IntegrationFile
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	collection = database.Database.Collection(setting.IntegrationFileTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&integrationFile)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Error - "+err.Error())
		return
	}

	_ = collection.FindOne(ctx, bson.M{"file_name": integrationFile.FileName}).Decode(&result)

	if result.ID != primitive.NilObjectID {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "File is already exists in database !!")
		return
	}

	//Set Date = time.Now()
	currentTime := time.Now()
	integrationFile.ID = primitive.NewObjectID()
	integrationFile.Status = "In Process"
	integrationFile.InvalidProducts = []interface{}{}
	integrationFile.CreatedDate = currentTime
	integrationFile.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, integrationFile)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert to Database Error - "+err.Error())
		return
	}

	//b, _ := json.Marshal(integrationFile.InvalidProducts)
	err = service.InsertQueue(integrationFile.IntegrationType, enum.AppIdOMS, setting.IntegrationFileTable, integrationFile.FileName, integrationFile.Content, "", "", "", false)
	if err != nil {
		fmt.Println("INSERT QUEUE FAIL - " + err.Error())
	}

	data := bson.M{"integration_file": integrationFile}
	api_helper.SuccessResponse(response, data, "Create Integration File successfully ")
}

// Get number of IntegrationFiles godoc
// @Tags IntegrationFile
// @Summary Get number of IntegrationFiles sort by created_date field descending
// @Description Get number of IntegrationFiles depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Param from_date path int false "From date with Unix Time format. If missing, default value is 0. Maximum value will be current time with Unix Time format."
// @Param to_date path int false "To date with Unix Time format. If missing, Default value will be current time with Unix Time format. Maximum value will be current time with Unix Time format."
// @Success 200 {object} []model.IntegrationFileResponse
// @Header 200 {string} Token "qwerty"
// @Router /IntegrationFile [get]
func GetIntegrationFiles(response http.ResponseWriter, request *http.Request) {
	//handle error
	const funcName string = "GetIntegrationFiles"

	defer api_helper.RecoverError(response, funcName)
	var (
		integrationFile            model.IntegrationFile
		integrationFileResponse    model.IntegrationFileResponse
		allIntegrationFileResponse []model.IntegrationFileResponse
	)

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit/Page ERROR - "+err.Error())
		return
	}

	currentTime := time.Now().Unix()

	fromDate, err := api_helper.ParseParamStringToIntFromURLQuery(request, "from_date", 0, currentTime)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get From Date ERROR - "+err.Error())
		return
	}
	toDate, err := api_helper.ParseParamStringToIntFromURLQuery(request, "to_date", currentTime, currentTime)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get To Date ERROR - "+err.Error())
		return
	}

	if fromDate > toDate {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, fmt.Sprintf("ERROR : From Date '%s' CANNOT greater than To Date '%s'", time.Unix(fromDate, 0), time.Unix(toDate, 0)))
		return
	}

	collection := database.Database.Collection(setting.IntegrationFileTable)
	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"created_date": bson.M{"$gte": time.Unix(fromDate, 0), "$lte": time.Unix(toDate, 0)}}

	findOptions := options.Find()
	//Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	//Set limit record of 1 page
	findOptions.SetLimit(limit)
	//Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		//Decode cursor to model
		cursor.Decode(&integrationFile)
		//Copy model to response model
		_ = copier.Copy(&integrationFileResponse, &integrationFile)
		//Assign value to property of response model
		integrationFileResponse.CreatedDate = integrationFile.CreatedDate.Unix()

		allIntegrationFileResponse = append(allIntegrationFileResponse, integrationFileResponse)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"integration_file": allIntegrationFileResponse}
	api_helper.SuccessResponse(response, data, "Get Integration File successfully")
}

func UpdateIntegrationFileStatus(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "UpdateIntegrationFile")
	// Initial Values.
	var (
		collection              *mongo.Collection
		integrationFile         model.IntegrationFile
		integrationFileResponse model.IntegrationFileResponse
		queue                   model.Queue
		currentTime             = time.Now()
	)

	// Set response header
	response.Header().Set("content-type", "application/json")

	//Collection
	collection = database.Database.Collection(setting.IntegrationFileTable)
	queueCollection := database.Database.Collection(setting.QueueTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Get Id Param from request URL
	read, err := ioutil.ReadAll(request.Body)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusBadRequest,0, "Read request body error - "+err.Error())
		return
	}
	err = json.Unmarshal(read, &integrationFile)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusBadRequest,0, "Unmarshal error - "+err.Error())
		return
	}
	// Filter to find IntegrationFile File name, is_deleted = false
	filter := bson.M{"file_name": integrationFile.FileName}
	//Find IntegrationFile By File name
	err = collection.FindOne(ctx, filter).Decode(&integrationFile)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusBadRequest,0, "Find file error - "+err.Error())
		return
	}

	// Find Queue
	queueFilter := bson.M{
		"from_application_id": integrationFile.IntegrationType,
		"to_application_id":   enum.AppIdOMS,
		"source_object_type":  setting.IntegrationFileTable,
		"source_object_id":    integrationFile.FileName,
	}
	if err = queueCollection.FindOne(ctx, queueFilter).Decode(&queue); err != nil {
		api_helper.ErrorResponse(response, http.StatusBadRequest,0, "Find queue ERROR - "+err.Error())
		return
	}

	//check invalid products.
	if len(integrationFile.InvalidProducts) == 0 {
		integrationFile.Status = "Success"
		queue.Status = true
	} else {
		integrationFile.Status = "Fail"
		bytes, _ := json.Marshal(integrationFile.InvalidProducts)
		queue.ErrorMessage = string(bytes)
		queue.Status = false
	}

	integrationFile.UpdatedDate = currentTime
	queue.UpdatedDate = currentTime

	updateFile := bson.M{"$set": integrationFile}
	_, err = collection.UpdateOne(ctx, filter, updateFile)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusBadRequest,0, "Update File error - "+err.Error())
		return
	}

	if _, err = queueCollection.UpdateOne(ctx, queueFilter, bson.M{"$set": queue}); err != nil {
		api_helper.ErrorResponse(response, http.StatusBadRequest,0, "Update queue error - "+err.Error())
		return
	}

	_ = copier.Copy(&integrationFileResponse, &integrationFile)
	integrationFileResponse.CreatedDate = integrationFile.CreatedDate.Unix()

	data := bson.M{"integration_file": integrationFileResponse}
	api_helper.SuccessResponse(response, data, "Update Integration File status successfully")
}

func UpdateIntegrationFileInvalidProducts(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "UpdateIntegrationFile")
	// Initial Values.
	var (
		collection              *mongo.Collection
		integrationFile         model.IntegrationFile
		updateInvalidProduct    model.IntegrationFile
		integrationFileResponse model.IntegrationFileResponse
	)

	// Set response header
	response.Header().Set("content-type", "application/json")

	//Collection
	collection = database.Database.Collection(setting.IntegrationFileTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Get Id Param from request URL
	//read, err := ioutil.ReadAll(request.Body)
	//if err != nil {
	//	api_helper.ErrorResponse(response, http.StatusBadRequest,0, "Read request body error - "+err.Error())
	//	return
	//}
	//err = json.Unmarshal(read, &updateInvalidProduct)
	//if err != nil {
	//	api_helper.ErrorResponse(response, http.StatusBadRequest,0, "Unmarshal error - "+err.Error())
	//	return
	//}

	if err = json.NewDecoder(request.Body).Decode(&updateInvalidProduct); err != nil {
		api_helper.ErrorResponse(response, http.StatusBadRequest,0, "Decode ERROR - "+err.Error())
		return
	}

	// Filter to find IntegrationFile File name, is_deleted = false
	filter := bson.D{
		{"file_name", updateInvalidProduct.FileName},
	}

	//Find IntegrationFile By File name
	err = collection.FindOne(ctx, filter).Decode(&integrationFile)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusBadRequest,0, "Find file error - "+err.Error())
		return
	}

	//if len(integrationFile.InvalidProducts) == 0 {
	//	integrationFile.Status = "Success"
	//} else {
	//	integrationFile.Status = "Fail"
	//}
	for _, prod := range updateInvalidProduct.InvalidProducts {
		var prodBson bson.M
		b, _ := json.Marshal(prod)
		if err = json.Unmarshal(b, &prodBson); err != nil {
			api_helper.ErrorResponse(response, http.StatusBadRequest,0, "Unmarshal Invalid Product ERROR - "+err.Error())
			return
		}
		integrationFile.InvalidProducts = append(integrationFile.InvalidProducts, prodBson)
	}

	integrationFile.UpdatedDate = time.Now()

	update := bson.M{"$set": integrationFile}
	_, err = collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusBadRequest,0, "Update Invalid Products of File error - "+err.Error())
		return
	}

	_ = copier.Copy(&integrationFileResponse, &integrationFile)
	integrationFileResponse.CreatedDate = integrationFile.CreatedDate.Unix()

	data := bson.M{"integration_file": integrationFileResponse}
	api_helper.SuccessResponse(response, data, "Update Invalid Products of Integration File status successfully")
}

//-------------------- Unused ------------------------

func CreateManyIntegrationFile(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyIntegrationFile")
	var (
		collection               *mongo.Collection
		manyIntegrationFileInput []model.IntegrationFileRequest
		integrationFile          model.IntegrationFile
	)
	collection = database.Database.Collection(setting.IntegrationFileTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&manyIntegrationFileInput)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	var ui []interface{}
	for _, integrationFileInput := range manyIntegrationFileInput {
		//api_helper.ValidateIntegrationFile(integrationFileInput)
		err = copier.Copy(&integrationFile, &integrationFileInput)
		if err != nil {
			response.WriteHeader(http.StatusBadRequest)
			response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
			return
		}

		currentTime := time.Now()
		integrationFile.ID = primitive.NewObjectID()
		integrationFile.CreatedDate = currentTime
		integrationFile.UpdatedDate = currentTime

		ui = append(ui, integrationFile)
	}

	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

func GetIntegrationFileById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetIntegrationFileById")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.IntegrationFileTable)
	response.Header().Set("content-type", "application/json")
	var integrationFile model.IntegrationFile
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//_id = id and is_deleted = false
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}, "is_deleted": bson.M{"$eq": false}}
	err := collection.FindOne(ctx, filter).Decode(&integrationFile)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(integrationFile)
}

func DeleteSoftIntegrationFile(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteSoftIntegrationFile")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.IntegrationFileTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("UpdateOne() result:", result)
		fmt.Println("UpdateOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("UpdateOne() result MatchedCount:", result.MatchedCount)
		fmt.Println("UpdateOne() result ModifiedCount:", result.ModifiedCount)
		fmt.Println("UpdateOne() result UpsertedCount:", result.UpsertedCount)
		fmt.Println("UpdateOne() result UpsertedID:", result.UpsertedID)
	}
	json.NewEncoder(response).Encode(result)
}

//DeleteHardIntegrationFile
/*
func DeleteHardIntegrationFile(response http.ResponseWriter, request *http.Request) {
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.IntegrationFileTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
