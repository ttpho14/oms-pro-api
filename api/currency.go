package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../database"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

// Add Currency godoc
// @Tags Currency
// @Summary Add Currency
// @Description Add Currency
// @Accept  json
// @Produce  json
// @Param body body model.CurrencyRequest true "Add Currency"
// @Success 200 {object} model.Currency
// @Header 200 {string} Token "qwerty"
// @Router /Currency [POST]
func CreateCurrency(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateCurrency")
	var (
		collection  *mongo.Collection
		currency    model.Currency
		currencyReq model.CurrencyRequest
	)

	response.Header().Set("content-type", "application/json")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	collection = database.Database.Collection(setting.CurrencyTable)
	err = json.NewDecoder(request.Body).Decode(&currencyReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&currency, &currencyReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set Date = time.Now()
	currentTime := time.Now()
	currency.ID = primitive.NewObjectID()
	currency.CreatedDate = currentTime
	currency.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, currency)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	data := bson.M{"currency": currency}
	api_helper.SuccessResponse(response, data, "Create Currency successfully")
}

// Get Currency By Id godoc
// @Tags Currency
// @Summary Get Currency by Id
// @Description Get Currency by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Currency need to be found"
// @Success 200 {object} model.Currency
// @Header 200 {string} Token "qwerty"
// @Router /Currency/{id} [get]
func GetCurrencyById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetCurrencyById")
	var (
		collection *mongo.Collection
		currency   model.Currency
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.CurrencyTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err := collection.FindOne(ctx, filter).Decode(&currency)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	data := bson.M{"currency": currency}
	api_helper.SuccessResponse(response, data, "Get Currency Successfully")
}

// Get number of Currencies godoc
// @Tags Currency
// @Summary Get number of Currencies sort by created_date field descending
// @Description Get number of Currencies depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.Currency
// @Header 200 {string} Token "qwerty"
// @Router /Currency [get]
func GetCurrencies(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetCurrencies")

	var (
		allCurrency []model.Currency
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.CurrencyTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{"is_deleted": bson.M{"$eq": false}}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))

	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var currency model.Currency
		_ = cursor.Decode(&currency)
		allCurrency = append(allCurrency, currency)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"currency": allCurrency}
	api_helper.SuccessResponse(response, data, "Get Currency Successfully")
}

// Update Currency godoc
// @Tags Currency
// @Summary Update Currency
// @Description Update Currency
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Currency need to be found"
// @Param body body []model.CurrencyRequest true "Update Currency"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Currency/{id} [put]
func UpdateCurrency(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var (
		collection  *mongo.Collection
		currency    model.Currency
		currencyReq model.CurrencyRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.CurrencyTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err = collection.FindOne(ctx, filter).Decode(&currency)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = json.NewDecoder(request.Body).Decode(&currencyReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&currency, &currencyReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set lai thoi gian luc update
	currency.UpdatedDate = time.Now()

	update := bson.M{"$set": currency}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"currency": currency}
	api_helper.SuccessResponse(response, data, "Update Currency Successfully")
}

// Soft Delete Currency godoc
// @Tags Currency
// @Summary Soft Delete Currency
// @Description Soft Delete Currency
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Currency need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Currency/{id} [delete]
func DeleteSoftCurrency(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.CurrencyTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	api_helper.SuccessResponse(response, id, "Delete Currency successfully")
}

//-------------------- Unused ----------------------
func CreateManyCurrency(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyCurrency")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.CurrencyTable)
	response.Header().Set("content-type", "application/json")
	var manyCurrency []model.Currency
	err = json.NewDecoder(request.Body).Decode(&manyCurrency)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, currency := range manyCurrency {
		currency.ID = primitive.NewObjectID()
		currency.CreatedDate = time.Now()
		currency.UpdatedDate = time.Now()
		ui = append(ui, currency)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

//DeleteHardCurrency
/*
func DeleteHardCurrency(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardCurrency")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.CurrencyTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
