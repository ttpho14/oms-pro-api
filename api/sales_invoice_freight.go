package api

import (
	"../setting"
	"context"
	"encoding/json"
	"net/http"
	"os"
	"time"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateSalesInvoiceFreight godoc
// @Tags Sales Invoice Freight
// @Summary Add Sales Invoice Freight
// @Description Add Sales Invoice Freight
// @Accept  json
// @Produce  json
// @Param body body []model.AddSalesInvoiceFreight true "Add Sales Invoice Freight"
// @Success 200 {object} []model.SalesInvoiceFreight
// @Header 200 {string} Token "qwerty"
// @Router /SalesInvoiceFreight [POST]
func CreateSalesInvoiceFreight(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateSalesInvoiceFreight")
	var (
		collection          *mongo.Collection
		salesInvoiceFreight model.SalesInvoiceFreight
	)

	collection = database.Database.Collection(setting.SalesInvoiceFreightTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&salesInvoiceFreight)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	currentTime := time.Now()
	salesInvoiceFreight.CreatedDate = currentTime
	salesInvoiceFreight.UpdatedDate = currentTime

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	result, err := collection.InsertOne(ctx, salesInvoiceFreight)
	if err != nil {
		json.NewEncoder(response).Encode(err.Error())
		return
	}
	id := result.InsertedID
	salesInvoiceFreight.ID = id.(primitive.ObjectID)
	json.NewEncoder(response).Encode(salesInvoiceFreight)
}

// CreateManySalesInvoiceFreight func
func CreateManySalesInvoiceFreight(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManySalesInvoiceFreight")
	var (
		collection           *mongo.Collection
		salesInvoiceFreights []model.SalesInvoiceFreight
	)
	collection = database.Database.Collection(setting.SalesInvoiceFreightTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&salesInvoiceFreights)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	currentTime := time.Now()
	for _, salesInvoiceFreight := range salesInvoiceFreights {
		salesInvoiceFreight.ID = primitive.NewObjectID()
		salesInvoiceFreight.CreatedDate = currentTime
		salesInvoiceFreight.UpdatedDate = currentTime
		ui = append(ui, salesInvoiceFreight)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// GetSalesInvoiceFreightByID godoc
// @Tags Sales Invoice Freight
// @Summary Get Sales Invoice Freight by Id
// @Description Get Sales Invoice Freight by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Invoice Freight need to be found"
// @Success 200 {object} model.SalesInvoiceFreight
// @Header 200 {string} Token "qwerty"
// @Router /SalesInvoiceFreight/{id} [get]
func GetSalesInvoiceFreightByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSaleOrderFreightByID")
	var (
		collection          *mongo.Collection
		salesInvoiceFreight model.SalesInvoiceFreight
	)
	collection = database.Database.Collection(setting.SalesInvoiceFreightTable)

	response.Header().Set("content-type", "application/json")

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	err := collection.FindOne(ctx, filter).Decode(&salesInvoiceFreight)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesInvoiceFreight)
}

// GetSalesInvoiceFreights godoc
// @Tags Sales Invoice Freight
// @Summary Get all Sales Invoice Freight
// @Description Get all Sales Invoice Freight
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.SalesInvoiceFreight
// @Header 200 {string} Token "qwerty"
// @Router /SalesInvoiceFreight [get]
func GetSalesInvoiceFreights(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesInvoiceFreights")
	var (
		collection           *mongo.Collection
		salesInvoiceFreights []model.SalesInvoiceFreight
		findOptions          = options.Find()
		filter               = bson.M{}
	)
	collection = database.Database.Collection(setting.SalesInvoiceFreightTable)

	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions) //findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var saleInvoiceFreight model.SalesInvoiceFreight
		cursor.Decode(&saleInvoiceFreight)
		salesInvoiceFreights = append(salesInvoiceFreights, saleInvoiceFreight)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesInvoiceFreights)
}

// UpdateSalesInvoiceFreight godoc
// @Tags Sales Invoice Freight
// @Summary Update Sales Invoice Freight
// @Description Update Sales Invoice Freight
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Invoice Freight need to be found"
// @Param body body model.AddSalesInvoiceFreight true "Update Sales Invoice Freight"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /SalesInvoiceFreight/{id} [put]
func UpdateSalesInvoiceFreight(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateSaleOrderFreight")
	var (
		collection       *mongo.Collection
		saleOrderFreight model.SalesInvoiceFreight
	)
	collection = database.Database.Collection(setting.SalesInvoiceFreightTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&saleOrderFreight)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	saleOrderFreight.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": saleOrderFreight}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

func DeleteSoftSalesInvoiceFreight(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftSalesInvoiceFreight")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.SalesInvoiceFreightTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
