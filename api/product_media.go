package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../database"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

// Add ProductMedia godoc
// @Tags ProductMedia
// @Summary Add ProductMedia
// @Description Add ProductMedia
// @Accept  json
// @Produce  json
// @Param body body model.ProductMediaRequest true "Add ProductMedia"
// @Success 200 {object} model.ProductMedia
// @Header 200 {string} Token "qwerty"
// @Router /ProductMedia [POST]
func CreateProductMedia(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateProductMedia")
	var (
		collection   *mongo.Collection
		productMedia model.ProductMedia
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ProductMediaTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&productMedia)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Input to Request Model ERROR - "+err.Error())
		return
	}
	//Set Date = time.Now()
	currentTime := time.Now()
	productMedia.ID = primitive.NewObjectID()
	productMedia.CreatedDate = currentTime
	productMedia.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, productMedia)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	data := bson.M{"product_media": productMedia}
	api_helper.SuccessResponse(response, data, "Product Media successfully")
}

// Get ProductMedia By Id godoc
// @Tags ProductMedia
// @Summary Get ProductMedia by Id
// @Description Get ProductMedia by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of ProductMedia need to be found"
// @Success 200 {object} model.ProductMedia
// @Header 200 {string} Token "qwerty"
// @Router /ProductMedia/{id} [get]
func GetProductMediaById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetProductMediaById")
	var (
		collection   *mongo.Collection
		productMedia model.ProductMedia
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ProductMediaTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	err := collection.FindOne(ctx, filter).Decode(&productMedia)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	data := bson.M{"bundle_detail": productMedia}
	api_helper.SuccessResponse(response, data, "Get Product Media successfully")
}

// Get number of ProductMedias godoc
// @Tags ProductMedia
// @Summary Get number of ProductMedias sort by created_date field descending
// @Description Get number of ProductMedias depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.ProductMedia
// @Header 200 {string} Token "qwerty"
// @Router /ProductMedia [get]
func GetProductMedias(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetProductMedias")
	var (
		productMedia    model.ProductMedia
		allProductMedia []model.ProductMedia
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.ProductMediaTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{"is_deleted": false}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		_ = cursor.Decode(&productMedia)
		allProductMedia = append(allProductMedia, productMedia)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"product_media": allProductMedia}
	api_helper.SuccessResponse(response, data, "Get Product Media successfully")
}

// Update ProductMedia godoc
// @Tags ProductMedia
// @Summary Update ProductMedia
// @Description Update ProductMedia
// @Accept  json
// @Produce  json
// @Param id path string true "Id of ProductMedia need to be found"
// @Param body body model.ProductMediaRequest true "Update ProductMedia"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /ProductMedia/{id} [put]
func UpdateProductMedia(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var (
		collection      *mongo.Collection
		productMedia    model.ProductMedia
		productMediaReq model.ProductMediaRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ProductMediaTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err = collection.FindOne(ctx, filter).Decode(&productMedia)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = json.NewDecoder(request.Body).Decode(&productMediaReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&productMedia, &productMediaReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set lai thoi gian luc update
	productMedia.UpdatedDate = time.Now()

	update := bson.M{"$set": productMedia}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"product_media": productMedia}
	api_helper.SuccessResponse(response, data, "Update Product Media Successfully")
}

// Soft Delete ProductMedia godoc
// @Tags ProductMedia
// @Summary Soft Delete ProductMedia
// @Description Soft Delete ProductMedia
// @Accept  json
// @Produce  json
// @Param id path string true "Id of ProductMedia need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /ProductMedia/{id} [delete]
func DeleteSoftProductMedia(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteSoftProductMedia")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ProductMediaTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	api_helper.SuccessResponse(response, id, "Delete Product Media Successfully")
}

//DeleteHardProductMedia
/*
func DeleteHardProductMedia(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardProductMedia")
	var collection *mongo.Collection
	collection = database.Database.Collection("productMedia")
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
//-------------------- Unused  -----------------------

func CreateManyProductMedia(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyProductMedia")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.ProductMediaTable)
	response.Header().Set("content-type", "application/json")
	var manyProductMedia []model.ProductMedia
	err = json.NewDecoder(request.Body).Decode(&manyProductMedia)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, productMedia := range manyProductMedia {
		productMedia.ID = primitive.NewObjectID()
		productMedia.CreatedDate = time.Now()
		productMedia.UpdatedDate = time.Now()
		ui = append(ui, productMedia)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}
