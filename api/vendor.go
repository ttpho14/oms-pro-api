package api

import (
	"../database"
	"../setting"
	"./api_helper"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"net/http"
	"os"
	"reflect"
	"time"

	"../model"
)

// Add Vendor godoc
// @Tags Vendor
// @Summary Add Vendor
// @Description Add Vendor
// @Accept  json
// @Produce  json
// @Param body body []model.AddVendor true "Add Vendor"
// @Success 200 {object} []model.Vendor
// @Header 200 {string} Token "qwerty"
// @Router /Vendor [POST]
func CreateVendor(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateVendor")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.VendorTable)
	response.Header().Set("content-type", "application/json")
	var vendor model.Vendor
	err = json.NewDecoder(request.Body).Decode(&vendor)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	//Set Date = time.Now()
	vendor.CreatedDate = time.Now()
	vendor.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	result, err := collection.InsertOne(ctx, vendor)
	if err != nil {
		json.NewEncoder(response).Encode(err.Error())
		return
	}
	id := result.InsertedID
	vendor.ID = id.(primitive.ObjectID)
	json.NewEncoder(response).Encode(vendor)
}

func CreateManyVendor(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyVendor")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.VendorTable)
	response.Header().Set("content-type", "application/json")
	var manyVendor []model.Vendor
	err = json.NewDecoder(request.Body).Decode(&manyVendor)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, vendor := range manyVendor {
		vendor.ID = primitive.NewObjectID()
		vendor.CreatedDate = time.Now()
		vendor.UpdatedDate = time.Now()
		ui = append(ui, vendor)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// Get Vendor By Id godoc
// @Tags Vendor
// @Summary Get Vendor by Id
// @Description Get Vendor by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Vendor need to be found"
// @Success 200 {object} model.Vendor
// @Header 200 {string} Token "qwerty"
// @Router /Vendor/{id} [get]
func GetVendorById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetVendorById")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.VendorTable)
	response.Header().Set("content-type", "application/json")
	var vendor model.Vendor
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//_id = id and is_deleted = false
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}, "is_deleted": bson.M{"$eq": false}}
	err := collection.FindOne(ctx, filter).Decode(&vendor)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(vendor)
}

// Get number of Vendors godoc
// @Tags Vendor
// @Summary Get number of Vendors sort by created_date field descending
// @Description Get number of Vendors depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.Vendor
// @Header 200 {string} Token "qwerty"
// @Router /Vendor [get]
func GetVendors(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetVendors")
	//Get Limit and Page
	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	collection := database.Database.Collection(setting.VendorTable)
	response.Header().Set("content-type", "application/json")
	var allVendor []model.Vendor
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"is_deleted": bson.M{"$eq": false}}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var vendor model.Vendor
		cursor.Decode(&vendor)
		allVendor = append(allVendor, vendor)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(allVendor)
}

// Update Vendor godoc
// @Tags Vendor
// @Summary Update Vendor
// @Description Update Vendor
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Vendor need to be found"
// @Param body body model.AddVendor true "Update Vendor"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Vendor/{id} [put]
func UpdateVendor(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "UpdateVendor")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.VendorTable)
	response.Header().Set("content-type", "application/json")
	var vendor model.Vendor
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	err = json.NewDecoder(request.Body).Decode(&vendor)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	//Set lai thoi gian luc update
	vendor.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	fmt.Println("filter Vendor", filter)
	update := bson.M{"$set": vendor}
	result, err := collection.UpdateOne(ctx, filter, update)
	fmt.Println("Result Vendor", result)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

// Soft Delete Vendor godoc
// @Tags Vendor
// @Summary Soft Delete Vendor
// @Description Soft Delete Vendor
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Vendor need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Vendor/{id} [delete]
func DeleteSoftVendor(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteSoftVendor")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.VendorTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("UpdateOne() result:", result)
		fmt.Println("UpdateOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("UpdateOne() result MatchedCount:", result.MatchedCount)
		fmt.Println("UpdateOne() result ModifiedCount:", result.ModifiedCount)
		fmt.Println("UpdateOne() result UpsertedCount:", result.UpsertedCount)
		fmt.Println("UpdateOne() result UpsertedID:", result.UpsertedID)
	}
	json.NewEncoder(response).Encode(result)
}

//DeleteHardVendor
/*
func DeleteHardVendor(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardVendor")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.VendorTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
//-------------------- REGION API - END -----------------------
