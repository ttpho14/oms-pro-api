package api

import (
	"context"
	"encoding/json"
	"net/http"
	"os"
	"time"

	"../setting"
	"github.com/jinzhu/copier"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateOrderCancellationDetail godoc
// @Tags Order Cancellation Detail
// @Summary Add Order Cancellation Detail
// @Description Add Order Cancellation Detail
// @Accept  json
// @Produce  json
// @Param body body model.OrderCancellationDetailRequest true "Add Order Cancellation Detail"
// @Success 200 {object} model.OrderCancellationDetail
// @Header 200 {string} Token "qwerty"
// @Router /OrderCancellationDetail [POST]
func CreateOrderCancellationDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateOrderCancellationDetail")
	var (
		collection                 *mongo.Collection
		orderCancellationDetail    model.OrderCancellationDetail
		orderCancellationDetailReq model.OrderCancellationDetailRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.OrderCancellationDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&orderCancellationDetailReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Input to Request Model ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&orderCancellationDetail, &orderCancellationDetailReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy Request Model to Main Model ERROR - "+err.Error())
		return
	}

	currentTime := time.Now()
	orderCancellationDetail.ID = primitive.NewObjectID()
	orderCancellationDetail.CreatedDate = currentTime
	orderCancellationDetail.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, orderCancellationDetail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	//orderCancellationDetailRes, err := orderCancellationDetail.ToResponse()
	//if err != nil {
	//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Parse from main model to response model ERROR - "+err.Error())
	//	return
	//}

	data := bson.M{"order_cancellation_detail": orderCancellationDetail}
	api_helper.SuccessResponse(response, data, "Create Order Cancellation Detail successfully")
}

// GetOrderCancellationDetailByID godoc
// @Tags Order Cancellation Detail
// @Summary Get Order Cancellation Detail by Id
// @Description Get Order Cancellation Detail by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Order Cancellation Detail need to be found"
// @Success 200 {object} model.OrderCancellationDetail
// @Header 200 {string} Token "qwerty"
// @Router /OrderCancellationDetail/{id} [get]
func GetOrderCancellationDetailByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetOrderCancellationDetailByID")
	var (
		collection              *mongo.Collection
		orderCancellationDetail model.OrderCancellationDetail
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.OrderCancellationDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id)}
	err := collection.FindOne(ctx, filter).Decode(&orderCancellationDetail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode result to Main Model ERROR - "+err.Error())
		return
	}

	//orderCancellationDetailResponse, err := orderCancellationDetail.ToResponse()
	//if err != nil {
	//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Parse from main model to response model ERROR - "+err.Error())
	//	return
	//}

	data := bson.M{"order_cancellation_detail": orderCancellationDetail}
	api_helper.SuccessResponse(response, data, "Get Order Cancellation Detail successfully")
}

// GetOrderCancellationDetails godoc
// @Tags Order Cancellation Detail
// @Summary Get all Order Cancellation Detail
// @Description Get all Order Cancellation Detail
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.OrderCancellationDetail
// @Header 200 {string} Token "qwerty"
// @Router /OrderCancellationDetail [get]
func GetOrderCancellationDetails(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetOrderCancellationDetails")
	var (
		collection                         *mongo.Collection
		orderCancellationResponse          model.OrderCancellationDetailResponse
		allOrderCancellationDetailResponse []model.OrderCancellationDetailResponse
		findOptions                        = options.Find()
		filter                             = bson.M{}
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.OrderCancellationDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))

	cursor, err := collection.Find(ctx, filter, findOptions) //findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		_ = cursor.Decode(&orderCancellationResponse)

		//orderCancellationResponse, err := orderCancellation.ToResponse()
		//if err != nil {
		//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Parse from main model to response model ERROR - "+err.Error())
		//	return
		//}

		allOrderCancellationDetailResponse = append(allOrderCancellationDetailResponse, orderCancellationResponse)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}
	data := bson.M{"order_cancellation_detail": allOrderCancellationDetailResponse}
	api_helper.SuccessResponse(response, data, "Get Order Cancellation Detail successfully")
}

// UpdateOrderCancellationDetail godoc
// @Tags Order Cancellation Detail
// @Summary Update Order Cancellation Detail
// @Description Update Order Cancellation Detail
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Order Cancellation Detail need to be found"
// @Param body body model.OrderCancellationDetailRequest true "Update Order Cancellation Detail"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /OrderCancellationDetail/{id} [put]
func UpdateOrderCancellationDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateOrderCancellationDetail")
	var (
		collection                 *mongo.Collection
		orderCancellationDetail    model.OrderCancellationDetail
		orderCancellationDetailReq model.OrderCancellationDetailRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.OrderCancellationDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&orderCancellationDetailReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	filter := bson.M{"_id": bsonx.ObjectID(id)}

	err = collection.FindOne(ctx, filter).Decode(&orderCancellationDetail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&orderCancellationDetail, &orderCancellationDetailReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	orderCancellationDetail.UpdatedDate = time.Now()

	filter = bson.M{"_id": bsonx.ObjectID(id)}
	update := bson.M{"$set": orderCancellationDetail}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	//orderCancellationDetailRes,err :=  orderCancellationDetail.ToResponse()
	//if err != nil {
	//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Parse from main model to response model ERROR - "+err.Error())
	//	return
	//}

	data := bson.M{"order_cancellation_detail": orderCancellationDetail}
	api_helper.SuccessResponse(response, data, "Update Order Cancellation Detail successfully")
}

//-------------------- Unused -----------------------
// CreateManyOrderCancellationDetail func
func CreateManyOrderCancellationDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManyOrderCancellationDetail")
	var (
		collection                  *mongo.Collection
		orderCancellationDetails    []model.OrderCancellationDetail
		orderCancellationDetailsReq []model.OrderCancellationDetailRequest
		ui                          []interface{}
		currentTime                 = time.Now()
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.OrderCancellationDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&orderCancellationDetailsReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Input to Request Model ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&orderCancellationDetails, &orderCancellationDetailsReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy Request Model to Main Model ERROR - "+err.Error())
		return
	}

	for _, orderCancellationDetail := range orderCancellationDetails {
		orderCancellationDetail.ID = primitive.NewObjectID()
		orderCancellationDetail.CreatedDate = currentTime
		orderCancellationDetail.UpdatedDate = currentTime
		ui = append(ui, orderCancellationDetail)
	}

	_, err = collection.InsertMany(ctx, ui)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	data := bson.M{"order_cancellation_detail": ui}
	api_helper.SuccessResponse(response, data, "Create Order Cancellation Detail successfully")
}

// DeleteSoftOrderCancellationDetail func
func DeleteSoftOrderCancellationDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftOrderCancellationDetail")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.OrderCancellationDetailTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
