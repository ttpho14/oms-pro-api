package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../database"
	"../model"
	"../setting"
	helper "./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// Add Address godoc
// @Tags Address
// @Summary Add Address
// @Description Add Address
// @Accept  json
// @Produce  json
// @Param body body model.AddressRequest true "Add Address"
// @Success 200 {object} model.Address
// @Header 200 {string} Token "qwerty"
// @Router /Address [POST]
func CreateAddress(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "CreateAddress")
	var (
		collection *mongo.Collection
		address    model.Address
		addressReq model.AddressRequest
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.AddressTable)

	//Decode request.Body into Input model
	err = json.NewDecoder(request.Body).Decode(&addressReq)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&address, &addressReq)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	currentTime := time.Now()
	address.ID = primitive.NewObjectID()
	address.CreatedDate = currentTime
	address.UpdatedDate = currentTime
	//address.IsActive = true
	address.IsDeleted = false

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	_, err := collection.InsertOne(ctx, address)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	data := bson.M{"address": address}
	helper.SuccessResponse(response, data, "Create Address successfully")
}

// Get Address By Id godoc
// @Tags Address
// @Summary Get Address by Id
// @Description Get Address by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Address need to be found"
// @Success 200 {object} model.Address
// @Header 200 {string} Token "qwerty"
// @Router /Address/{id} [get]
func GetAddressById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "GetAddressById")
	var (
		collection *mongo.Collection
		address    model.Address
		filter     bson.D
	)
	collection = database.Database.Collection(setting.AddressTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//_id = id and is_deleted = false
	filter = bson.D{{"_id", bsonx.ObjectID(id)}, {"is_deleted", false}}
	err := collection.FindOne(ctx, filter).Decode(&address)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
		return
	}

	data := bson.M{"address": address}
	helper.SuccessResponse(response, data, "Get Address successfully")
}

// Get number of Address godoc
// @Tags Address
// @Summary Get number of Address
// @Description Get number of Address depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.Address
// @Header 200 {string} Token "qwerty"
// @Router /Address [get]
func GetAddresses(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "GetAddresses")
	response.Header().Set("content-type", "application/json")
	//Get Limit and Page
	var (
		address    model.Address
		allAddress []model.Address
		collection *mongo.Collection
	)

	limit, page, err := helper.GetLimitAndPage(request)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
		return
	}

	collection = database.Database.Collection(setting.AddressTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.D{{"is_deleted", false}}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		cursor.Decode(&address)
		allAddress = append(allAddress, address)
	}
	if err := cursor.Err(); err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
		return
	}

	data := bson.M{"address": allAddress}
	helper.SuccessResponse(response, data, "Get Addresses successfully")

}

// Update Address godoc
// @Tags Address
// @Summary Update Address
// @Description Update Address
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Address need to be found"
// @Param body body model.AddressUpdateRequest true "Update Address"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Address/{id} [put]
func UpdateAddress(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "UpdateAddress")
	// Initial Values.
	var (
		collection          *mongo.Collection
		addressUpdateReq    model.AddressUpdateRequest
		addressUpdateReqInt interface{}
		address             model.Address
		filter              bson.D
	)
	// Set response header
	response.Header().Set("content-type", "application/json")
	//Collection
	collection = database.Database.Collection(setting.AddressTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Get Id Param from request URL
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	// Filter to find Address by Id, is_deleted = false
	filter = bson.D{
		{"_id", bsonx.ObjectID(id)},
		{"is_deleted", false},
	}

	//Find Address By Id
	err = collection.FindOne(ctx, filter).Decode(&address)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Address ERROR - "+err.Error())
		return
	}

	// Decode Request.Body to Input Model
	err = json.NewDecoder(request.Body).Decode(&addressUpdateReq)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Address Input ERROR - "+err.Error())
		return
	}

	//// Copy fields Value from Input Model to Main Model
	//err = copier.Copy(&address, &addressUpdateReq)
	//if err != nil {
	//	helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Address Input ERROR - "+err.Error())
	//	return
	//}

	b, err := bson.Marshal(addressUpdateReq)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Marshal Address Update Input ERROR - "+err.Error())
		return
	}

	if err = bson.Unmarshal(b, &addressUpdateReqInt); err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Unmarshal Address Update Input to interface ERROR - "+err.Error())
		return
	}

	// Assign Main Model's values
	address.UpdatedDate = time.Now()

	//Update Sales Order.
	update := bson.A{
		bson.M{"$set": address}, // Set Main model first
	}

	//check sales order request is empty.
	if string(b) != "{}" {
		update = append(update, bson.M{"$set": addressUpdateReqInt}) // Set Request Interface second
	}

	//Update Address
	_, err = collection.UpdateOne(ctx, filter, update)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update address ERROR - "+err.Error())
		return
	}

	err = collection.FindOne(ctx, filter).Decode(&address)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Address ERROR - "+err.Error())
		return
	}

	data := bson.M{"address": address}
	helper.SuccessResponse(response, data, "Update Addresses successfully")
}

// Soft Delete Address godoc
// @Tags Address
// @Summary Soft Delete Address
// @Description Soft Delete Address
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Address need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Address/{id} [delete]
func DeleteSoftAddress(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "DeleteSoftAddress")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.AddressTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.D{{"_id", id}}
	update := bson.D{
		{"$set", bson.D{
			{"is_deleted", true},
			{"updated_date", time.Now()},
		},
		},
	}
	_, err := collection.UpdateOne(context.Background(), filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
		return
	}

	data := bson.M{"_id": bsonx.ObjectID(id)}
	helper.SuccessResponse(response, data, "Delete Addresses successfully")
}

//-------------------- Un use -----------------------

func CreateManyAddress(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "CreateManyAddress")
	var (
		collection     *mongo.Collection
		manyAddAddress []model.AddressRequest
		address        model.Address
		res            model.ResponseResult
	)
	collection = database.Database.Collection(setting.AddressTable)

	response.Header().Set("content-type", "application/json")

	err = json.NewDecoder(request.Body).Decode(&manyAddAddress)
	if err != nil {
		res.Message = err.Error()
		response.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(response).Encode(res)
		return
	}

	var ui []interface{}
	for _, addAddress := range manyAddAddress {
		//helper.ValidateAddress(addAddress)
		err = copier.Copy(&address, &addAddress)
		if err != nil {
			res.Message = err.Error()
			response.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(response).Encode(res)
			return
		}
		currentTime := time.Now()

		address.ID = primitive.NewObjectID()
		address.CreatedDate = currentTime
		address.UpdatedDate = currentTime
		//address.IsActive = true
		address.IsDeleted = false

		ui = append(ui, address)
	}

	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		res.Message = err.Error()
		response.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(response).Encode(res)
		return
	}

	res.Success = true
	res.Message = "Create Many Address successfully"
	res.Data = bson.M{"address": ui}
	json.NewEncoder(response).Encode(res)
}

//DeleteHardAddress
/*
func DeleteHardAddress(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "DeleteHardAddress")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.ADDRESS_TABLE_NAME)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
