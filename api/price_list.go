package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../database"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

// Add PriceList godoc
// @Tags PriceList
// @Summary Add PriceList
// @Description Add PriceList
// @Accept  json
// @Produce  json
// @Param body body model.PriceListRequest true "Add PriceList"
// @Success 200 {object} model.PriceList
// @Header 200 {string} Token "qwerty"
// @Router /PriceList [POST]
func CreatePriceList(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreatePriceList")
	var (
		collection *mongo.Collection
		priceList  model.PriceList
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.PriceListTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&priceList)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Input to Request Model ERROR - "+err.Error())
		return
	}
	//Set Date = time.Now()
	currentTime := time.Now()
	priceList.ID = primitive.NewObjectID()
	priceList.CreatedDate = currentTime
	priceList.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, priceList)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	data := bson.M{"price_list": priceList}
	api_helper.SuccessResponse(response, data, "Price List successfully")
}

// Get PriceList By Id godoc
// @Tags PriceList
// @Summary Get PriceList by Id
// @Description Get PriceList by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of PriceList need to be found"
// @Success 200 {object} model.PriceList
// @Header 200 {string} Token "qwerty"
// @Router /PriceList/{id} [get]
func GetPriceListById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetPriceListById")
	var (
		collection *mongo.Collection
		priceList  model.PriceList
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.PriceListTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	err := collection.FindOne(ctx, filter).Decode(&priceList)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	data := bson.M{"bundle_detail": priceList}
	api_helper.SuccessResponse(response, data, "Get Price List successfully")
}

// Get number of PriceLists godoc
// @Tags PriceList
// @Summary Get number of PriceLists sort by created_date field descending
// @Description Get number of PriceLists depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.PriceList
// @Header 200 {string} Token "qwerty"
// @Router /PriceList [get]
func GetPriceLists(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetPriceLists")
	var (
		priceList    model.PriceList
		allPriceList []model.PriceList
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.PriceListTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{"is_deleted": false}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		_ = cursor.Decode(&priceList)
		allPriceList = append(allPriceList, priceList)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"price_list": allPriceList}
	api_helper.SuccessResponse(response, data, "Get Price List successfully")
}

// Update PriceList godoc
// @Tags PriceList
// @Summary Update PriceList
// @Description Update PriceList
// @Accept  json
// @Produce  json
// @Param id path string true "Id of PriceList need to be found"
// @Param body body model.PriceListRequest true "Update PriceList"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /PriceList/{id} [put]
func UpdatePriceList(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var (
		collection   *mongo.Collection
		priceList    model.PriceList
		priceListReq model.PriceListRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.PriceListTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err = collection.FindOne(ctx, filter).Decode(&priceList)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = json.NewDecoder(request.Body).Decode(&priceListReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&priceList, &priceListReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set lai thoi gian luc update
	priceList.UpdatedDate = time.Now()

	update := bson.M{"$set": priceList}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"price_list": priceList}
	api_helper.SuccessResponse(response, data, "Update Price List Successfully")
}

// Soft Delete PriceList godoc
// @Tags PriceList
// @Summary Soft Delete PriceList
// @Description Soft Delete PriceList
// @Accept  json
// @Produce  json
// @Param id path string true "Id of PriceList need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /PriceList/{id} [delete]
func DeleteSoftPriceList(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteSoftPriceList")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.PriceListTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	api_helper.SuccessResponse(response, id, "Delete Price List Successfully")
}

//DeleteHardPriceList
/*
func DeleteHardPriceList(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardPriceList")
	var collection *mongo.Collection
	collection = database.Database.Collection("priceList")
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
//-------------------- Unused  -----------------------

func CreateManyPriceList(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyPriceList")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.PriceListTable)
	response.Header().Set("content-type", "application/json")
	var manyPriceList []model.PriceList
	err = json.NewDecoder(request.Body).Decode(&manyPriceList)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, priceList := range manyPriceList {
		priceList.ID = primitive.NewObjectID()
		priceList.CreatedDate = time.Now()
		priceList.UpdatedDate = time.Now()
		ui = append(ui, priceList)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}
