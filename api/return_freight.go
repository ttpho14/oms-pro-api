package api

import (
	"context"
	"encoding/json"
	"net/http"
	"os"
	"time"

	"../setting"
	"github.com/jinzhu/copier"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateReturnFreight godoc
// @Tags Return Freight
// @Summary Add Return Freight
// @Description Add Return Freight
// @Accept  json
// @Produce  json
// @Param body body model.ReturnFreightRequest true "Add Return Freight"
// @Success 200 {object} model.ReturnFreight
// @Header 200 {string} Token "qwerty"
// @Router /ReturnFreight [POST]
func CreateReturnFreight(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateReturnFreight")
	var (
		collection    *mongo.Collection
		returnFreight model.ReturnFreight
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReturnFreightTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&returnFreight)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Input to Request Model ERROR - "+err.Error())
		return
	}
	//Set Date = time.Now()
	currentTime := time.Now()
	returnFreight.ID = primitive.NewObjectID()
	returnFreight.CreatedDate = currentTime
	returnFreight.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, returnFreight)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	data := bson.M{"return_freight": returnFreight}
	api_helper.SuccessResponse(response, data, "Return Freight successfully")
}

// GetReturnFreightByID godoc
// @Tags Return Freight
// @Summary Get Return Freight by Id
// @Description Get Return Freight by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Return Freight need to be found"
// @Success 200 {object} model.ReturnFreight
// @Header 200 {string} Token "qwerty"
// @Router /ReturnFreight/{id} [get]
func GetReturnFreightByID(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetReturnFreightById")
	var (
		collection    *mongo.Collection
		returnFreight model.ReturnFreight
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReturnFreightTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	err := collection.FindOne(ctx, filter).Decode(&returnFreight)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	data := bson.M{"bundle_freight": returnFreight}
	api_helper.SuccessResponse(response, data, "Get Return Freight successfully")
}

// GetReturnFreights godoc
// @Tags Return Freight
// @Summary Get all Return Freight
// @Description Get all Return Freight
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.ReturnFreight
// @Header 200 {string} Token "qwerty"
// @Router /ReturnFreight [get]
func GetReturnFreights(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetReturnFreights")
	var (
		returnFreight    model.ReturnFreight
		allReturnFreight []model.ReturnFreight
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.ReturnFreightTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{"is_deleted": false}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		_ = cursor.Decode(&returnFreight)
		allReturnFreight = append(allReturnFreight, returnFreight)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"return_freight": allReturnFreight}
	api_helper.SuccessResponse(response, data, "Get Return Freight successfully")
}

// UpdateReturnFreight godoc
// @Tags Return Freight
// @Summary Update Return Freight
// @Description Update Return Freight
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Return need to be found"
// @Param body body model.ReturnRequest true "Update Return Freight"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /ReturnFreight/{id} [put]
func UpdateReturnFreight(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var (
		collection       *mongo.Collection
		returnFreight    model.ReturnFreight
		returnFreightReq model.ReturnFreightRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReturnFreightTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err = collection.FindOne(ctx, filter).Decode(&returnFreight)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = json.NewDecoder(request.Body).Decode(&returnFreightReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&returnFreight, &returnFreightReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set lai thoi gian luc update
	returnFreight.UpdatedDate = time.Now()

	update := bson.M{"$set": returnFreight}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"return_freight": returnFreight}
	api_helper.SuccessResponse(response, data, "Update Return Freight Successfully")
}

//------------------- Unused --------------------------
func CreateManyReturnFreight(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManyReturnFreight")
	var (
		collection     *mongo.Collection
		returnFreights []model.ReturnFreight
		ui             []interface{}
	)
	collection = database.Database.Collection(setting.ReturnFreightTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&returnFreights)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	currentTime := time.Now()
	for _, ReturnFreight := range returnFreights {
		ReturnFreight.ID = primitive.NewObjectID()
		ReturnFreight.CreatedDate = currentTime
		ReturnFreight.UpdatedDate = currentTime
		ui = append(ui, ReturnFreight)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// DeleteSoftReturnFreight func
func DeleteSoftReturnFreight(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftReturnFreight")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.ReturnFreightTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
