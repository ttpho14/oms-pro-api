package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../model/enum"
	"../setting"
	"github.com/jinzhu/copier"

	"../database"
	"../model"
	"./api_helper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateSalesOrderPaymentCapture godoc
// @Tags Sales Order PaymentCapture
// @Summary Add Sales Order PaymentCapture
// @Description Add Sales Order PaymentCapture
// @Accept  json
// @Produce  json
// @Param body body model.SalesOrderPaymentCaptureRequest true "Add Sales Order PaymentCapture"
// @Success 200 {object} model.SalesOrderPaymentCapture
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderPaymentCapture [POST]
func CreateSalesOrderPaymentCapture(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateSalesOrderPaymentCapture")
	var (
		collection                  *mongo.Collection
		salesOrderPaymentCapture    model.SalesOrderPaymentCapture
		salesOrderPaymentCaptureReq model.SalesOrderPaymentCaptureRequest
	)

	collection = database.Database.Collection(setting.SalesOrderPaymentCaptureTable)
	response.Header().Set("content-type", "application/json")
	if err = json.NewDecoder(request.Body).Decode(&salesOrderPaymentCaptureReq); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Sales Order Payment Capture ERROR - "+err.Error())
		return
	}

	if err = copier.Copy(&salesOrderPaymentCapture, &salesOrderPaymentCaptureReq); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	currentTime := time.Now()
	salesOrderPaymentCapture.ID = primitive.NewObjectID()
	salesOrderPaymentCapture.Status = enum.PaymentStatusCaptureSuccess
	salesOrderPaymentCapture.CreatedDate = currentTime
	salesOrderPaymentCapture.UpdatedDate = currentTime

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err = collection.InsertOne(ctx, salesOrderPaymentCapture)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Sales Order Payment Capture ERROR - "+err.Error())
		return
	}

	filterSalesOrder := bson.M{"_id": bsonx.ObjectID(salesOrderPaymentCapture.DocKey)}
	updateSalesOrder := bson.M{"$set": bson.M{"payment_status": salesOrderPaymentCapture.Status}}
	if _, err = database.Database.Collection(setting.SalesOrderTable).UpdateOne(ctx, filterSalesOrder, updateSalesOrder); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Sales Order Payment Status ERROR - "+err.Error())
		return
	}

	data := bson.M{"sales_order_payment_capture": salesOrderPaymentCapture}
	api_helper.SuccessResponse(response, data, "Create Sales Order Payment Capture successfully")
}
