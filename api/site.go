package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../database"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

// Add Site godoc
// @Tags Site
// @Summary Add Site
// @Description Add Site
// @Accept  json
// @Produce  json
// @Param body body model.SiteRequest true "Add Site"
// @Success 200 {object} model.Site
// @Header 200 {string} Token "qwerty"
// @Router /Site [POST]
func CreateSite(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateSite")
	var (
		collection *mongo.Collection
		site       model.Site
		siteReq    model.SiteRequest
	)

	response.Header().Set("content-type", "application/json")

	_, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	collection = database.Database.Collection(setting.SiteTable)
	err = json.NewDecoder(request.Body).Decode(&siteReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	if err = siteReq.Validation(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Site Validation ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&site, &siteReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set Date = time.Now()
	currentTime := time.Now()
	site.ID = primitive.NewObjectID()
	site.CreatedDate = currentTime
	site.UpdatedDate = currentTime

	_, err := collection.InsertOne(context.Background(), site)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	_, _, err = CreateInventoryStatusBySite(site)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Create Inventory Status/Log ERROR - "+err.Error())
		return
	}

	data := bson.M{
		"site": site,
	}
	api_helper.SuccessResponse(response, data, "Create Site successfully")
}

// Get Site By Id godoc
// @Tags Site
// @Summary Get Site by Id
// @Description Get Site by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Site need to be found"
// @Success 200 {object} model.Site
// @Header 200 {string} Token "qwerty"
// @Router /Site/{id} [get]
func GetSiteById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetSiteById")
	var (
		collection *mongo.Collection
		site       model.Site
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.SiteTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err := collection.FindOne(ctx, filter).Decode(&site)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	data := bson.M{"site": site}
	api_helper.SuccessResponse(response, data, "Get Site Successfully")
}

// Get number of Sites godoc
// @Tags Site
// @Summary Get number of Sites sort by created_date field descending
// @Description Get number of Sites depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.Site
// @Header 200 {string} Token "qwerty"
// @Router /Site [get]
func GetSites(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetSites")

	var (
		allSite []model.Site
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.SiteTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{"is_deleted": bson.M{"$eq": false}}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))

	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var site model.Site
		_ = cursor.Decode(&site)
		allSite = append(allSite, site)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"site": allSite}
	api_helper.SuccessResponse(response, data, "Get Site Successfully")
}

// Update Site godoc
// @Tags Site
// @Summary Update Site
// @Description Update Site
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Site need to be found"
// @Param body body model.SiteRequest true "Update Site"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Site/{id} [put]
func UpdateSite(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var (
		collection *mongo.Collection
		site       model.Site
		siteReq    model.SiteRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.SiteTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id)}

	err = collection.FindOne(ctx, filter).Decode(&site)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = json.NewDecoder(request.Body).Decode(&siteReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&site, &siteReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set lai thoi gian luc update
	site.UpdatedDate = time.Now()

	update := bson.M{"$set": site}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"site": site}
	api_helper.SuccessResponse(response, data, "Update Site Successfully")
}

// Soft Delete Site godoc
// @Tags Site
// @Summary Soft Delete Site
// @Description Soft Delete Site
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Site need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Site/{id} [delete]
func DeleteSoftSite(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.SiteTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id)}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	api_helper.SuccessResponse(response, id, "Delete Site successfully")
}

//-------------------- Unused -----------------------
func CreateManySite(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManySite")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.SiteTable)
	response.Header().Set("content-type", "application/json")
	var manySite []model.Site
	err = json.NewDecoder(request.Body).Decode(&manySite)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, site := range manySite {
		site.ID = primitive.NewObjectID()
		site.CreatedDate = time.Now()
		site.UpdatedDate = time.Now()
		ui = append(ui, site)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

//DeleteHardSite
/*
func DeleteHardSite(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.SiteTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
