package api

import (
	"context"
	"net/http"
	"time"

	"../database"
	"../model"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// Get number of UserLicenses godoc
// @Tags UserLicense
// @Summary Get number of UserLicenses sort by created_date field descending
// @Description Get number of UserLicenses depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.UserLicense
// @Header 200 {string} Token "qwerty"
// @Router /UserLicense [get]
func GetUserLicenses(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetCurrencies")

	var (
		allUserLicense []model.UserLicense
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.UserLicenseTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))

	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	defer cursor.Close(ctx)
	if err = cursor.All(ctx, &allUserLicense); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch All to User License ERROR - "+err.Error())
		return
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"user": allUserLicense}
	api_helper.SuccessResponse(response, data, "Get User License Successfully")
}

// Get number of UserLicenses godoc
// @Tags UserLicense
// @Summary Get number of UserLicenses
// @Description Get number of UserLicenses
// @Accept  json
// @Produce  json
// @Success 200 {object} model.UserLicense
// @Header 200 {string} Token "qwerty"
// @Router /UserLicense/count [get]
func GetCountUserLicenses(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetCurrencies")

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.UserLicenseTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	count, err := collection.CountDocuments(ctx, bson.M{})
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Count User License ERROR - "+err.Error())
		return
	}

	data := bson.M{"count": count}
	api_helper.SuccessResponse(response, data, "Count User License Successfully")
}

// Delete User License godoc
// @Tags UserLicense
// @Summary Delete User License
// @Description Delete User License
// @Accept  json
// @Produce  json
// @Param id path string true "Id of UserLicense need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /UserLicense/{id} [delete]
func DeleteUserLicense(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.UserLicenseTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id)}

	_, err := collection.DeleteOne(ctx, filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	api_helper.SuccessResponse(response, id, "Delete User License successfully")
}
