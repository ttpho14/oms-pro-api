package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../database"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

// Add ProductVariant godoc
// @Tags ProductVariant
// @Summary Add ProductVariant
// @Description Add ProductVariant
// @Accept  json
// @Produce  json
// @Param body body model.ProductVariantRequest true "Add ProductVariant"
// @Success 200 {object} model.ProductVariant
// @Header 200 {string} Token "qwerty"
// @Router /ProductVariant [POST]
func CreateProductVariant(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateProductVariant")
	var (
		collection     *mongo.Collection
		productVariant model.ProductVariant
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ProductVariantTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&productVariant)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Input to Request Model ERROR - "+err.Error())
		return
	}
	//Set Date = time.Now()
	currentTime := time.Now()
	productVariant.ID = primitive.NewObjectID()
	productVariant.CreatedDate = currentTime
	productVariant.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, productVariant)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	data := bson.M{"product_variant": productVariant}
	api_helper.SuccessResponse(response, data, "Product Variant successfully")
}

// Get ProductVariant By Id godoc
// @Tags ProductVariant
// @Summary Get ProductVariant by Id
// @Description Get ProductVariant by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of ProductVariant need to be found"
// @Success 200 {object} model.ProductVariant
// @Header 200 {string} Token "qwerty"
// @Router /ProductVariant/{id} [get]
func GetProductVariantById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetProductVariantById")
	var (
		collection     *mongo.Collection
		productVariant model.ProductVariant
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ProductVariantTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	err := collection.FindOne(ctx, filter).Decode(&productVariant)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	data := bson.M{"bundle_detail": productVariant}
	api_helper.SuccessResponse(response, data, "Get Product Variant successfully")
}

// Get number of ProductVariants godoc
// @Tags ProductVariant
// @Summary Get number of ProductVariants sort by created_date field descending
// @Description Get number of ProductVariants depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.ProductVariant
// @Header 200 {string} Token "qwerty"
// @Router /ProductVariant [get]
func GetProductVariants(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetProductVariants")
	var (
		productVariant    model.ProductVariant
		allProductVariant []model.ProductVariant
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.ProductVariantTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{"is_deleted": false}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		_ = cursor.Decode(&productVariant)
		allProductVariant = append(allProductVariant, productVariant)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"product_variant": allProductVariant}
	api_helper.SuccessResponse(response, data, "Get Product Variant successfully")
}

// Update ProductVariant godoc
// @Tags ProductVariant
// @Summary Update ProductVariant
// @Description Update ProductVariant
// @Accept  json
// @Produce  json
// @Param id path string true "Id of ProductVariant need to be found"
// @Param body body model.ProductVariantRequest true "Update ProductVariant"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /ProductVariant/{id} [put]
func UpdateProductVariant(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var (
		collection        *mongo.Collection
		productVariant    model.ProductVariant
		productVariantReq model.ProductVariantRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ProductVariantTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err = collection.FindOne(ctx, filter).Decode(&productVariant)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = json.NewDecoder(request.Body).Decode(&productVariantReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&productVariant, &productVariantReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set lai thoi gian luc update
	productVariant.UpdatedDate = time.Now()

	update := bson.M{"$set": productVariant}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"product_variant": productVariant}
	api_helper.SuccessResponse(response, data, "Update Product Variant Successfully")
}

// Soft Delete ProductVariant godoc
// @Tags ProductVariant
// @Summary Soft Delete ProductVariant
// @Description Soft Delete ProductVariant
// @Accept  json
// @Produce  json
// @Param id path string true "Id of ProductVariant need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /ProductVariant/{id} [delete]
func DeleteSoftProductVariant(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteSoftProductVariant")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ProductVariantTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	api_helper.SuccessResponse(response, id, "Delete Product Variant Successfully")
}

//DeleteHardProductVariant
/*
func DeleteHardProductVariant(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardProductVariant")
	var collection *mongo.Collection
	collection = database.Database.Collection("productVariant")
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
//-------------------- Unused  -----------------------

func CreateManyProductVariant(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyProductVariant")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.ProductVariantTable)
	response.Header().Set("content-type", "application/json")
	var manyProductVariant []model.ProductVariant
	err = json.NewDecoder(request.Body).Decode(&manyProductVariant)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, productVariant := range manyProductVariant {
		productVariant.ID = primitive.NewObjectID()
		productVariant.CreatedDate = time.Now()
		productVariant.UpdatedDate = time.Now()
		ui = append(ui, productVariant)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}
