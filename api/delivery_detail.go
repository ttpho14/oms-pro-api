package api

import (
	"context"
	"encoding/json"
	"net/http"
	"os"
	"strconv"
	"time"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

var DELIVERY_DETAIL_TABLE_NAME = "delivery_detail"

// CreateDeliveryDetail godoc
// @Tags Delivery Detail
// @Summary Add Delivery Detail
// @Description Add Delivery Detail
// @Accept  json
// @Produce  json
// @Param body body []model.AddDeliveryDetail true "Add Delivery Detail"
// @Success 200 {object} []model.DeliveryDetail
// @Header 200 {string} Token "qwerty"
// @Router /DeliveryDetail [POST]
func CreateDeliveryDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateDeliveryDetail")
	var (
		collection     *mongo.Collection
		DeliveryDetail model.DeliveryDetail
	)

	collection = database.Database.Collection(DELIVERY_DETAIL_TABLE_NAME)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&DeliveryDetail)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	currentTime := time.Now()
	DeliveryDetail.CreatedDate = currentTime
	DeliveryDetail.UpdatedDate = currentTime

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	result, err := collection.InsertOne(ctx, DeliveryDetail)
	if err != nil {
		json.NewEncoder(response).Encode(err.Error())
		return
	}
	id := result.InsertedID
	DeliveryDetail.ID = id.(primitive.ObjectID)
	json.NewEncoder(response).Encode(DeliveryDetail)
}

// CreateManyDeliveryDetail func
func CreateManyDeliveryDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManyDeliveryDetail")
	var (
		collection      *mongo.Collection
		deliveryDetails []model.DeliveryDetail
		ui              []interface{}
	)
	collection = database.Database.Collection(DELIVERY_DETAIL_TABLE_NAME)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&deliveryDetails)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	currentTime := time.Now()
	for _, DeliveryDetail := range deliveryDetails {
		DeliveryDetail.ID = primitive.NewObjectID()
		DeliveryDetail.CreatedDate = currentTime
		DeliveryDetail.UpdatedDate = currentTime
		ui = append(ui, DeliveryDetail)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// GetDeliveryDetailByID godoc
// @Tags Delivery Detail
// @Summary Get Delivery Detail by Id
// @Description Get Delivery Detail by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Delivery Detail need to be found"
// @Success 200 {object} model.DeliveryDetail
// @Header 200 {string} Token "qwerty"
// @Router /DeliveryDetail/{id} [get]
func GetDeliveryDetailByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetDeliveryDetailByID")
	var (
		collection     *mongo.Collection
		DeliveryDetail model.DeliveryDetail
	)
	collection = database.Database.Collection(DELIVERY_DETAIL_TABLE_NAME)

	response.Header().Set("content-type", "application/json")

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	err := collection.FindOne(ctx, filter).Decode(&DeliveryDetail)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(DeliveryDetail)
}

// GetDeliveryDetails godoc
// @Tags Delivery Detail
// @Summary Get all Delivery Detail
// @Description Get all Delivery Detail
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.DeliveryDetail
// @Header 200 {string} Token "qwerty"
// @Router /DeliveryDetail [get]
func GetDeliveryDetails(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetDeliveryDetails")
	var (
		collection      *mongo.Collection
		deliveryDetails []model.DeliveryDetail
		query           = request.URL.Query()
		options         = options.Find()
	)
	collection = database.Database.Collection(DELIVERY_DETAIL_TABLE_NAME)

	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, err := strconv.ParseInt(query.Get("limit"), 10, 64)
	if limit <= 0 {
		limit = 100
	} else if limit >= 500 {
		limit = 500
	}
	options.SetLimit(limit)
	cursor, err := collection.Find(ctx, options)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var deliveryDetail model.DeliveryDetail
		cursor.Decode(&deliveryDetail)
		deliveryDetails = append(deliveryDetails, deliveryDetail)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(deliveryDetails)
}

// UpdateDeliveryDetail godoc
// @Tags Delivery Detail
// @Summary Update Delivery Detail
// @Description Update Delivery Detail
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Delivery need to be found"
// @Param body body model.AddDelivery true "Update Delivery Detail"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /DeliveryDetail/{id} [put]
func UpdateDeliveryDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateDeliveryDetail")
	var (
		collection     *mongo.Collection
		deliveryDetail model.DeliveryDetail
	)
	collection = database.Database.Collection(DELIVERY_DETAIL_TABLE_NAME)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&deliveryDetail)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	deliveryDetail.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": deliveryDetail}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

func DeleteSoftDeliveryDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftDeliveryDetail")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(DELIVERY_DETAIL_TABLE_NAME)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
