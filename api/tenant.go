package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"../database"
	"../model"
	"../service"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// Add Tenant godoc
// @Tags Tenant
// @Summary Add Tenant
// @Description Add Tenant
// @Accept  json
// @Produce  json
// @Param body body model.TenantRequest true "Tenant Request Input"
// @Success 200 {object} model.ResponseResult
// @Header 200 {string} Token "qwerty"
// @Router /Tenant [post]
func CreateTenant(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateTenant")
	var (
		collection       *mongo.Collection
		tenant           model.Tenant
		tenantDetails    model.TenantResponse
		tenantReq        model.TenantRequest
		tenantSetting    model.TenantSetting
		uiTenantSettings []interface{}
		currentTime      = time.Now()
	)

	response.Header().Set("content-type", "tenant/json")

	collection = database.Database.Collection(setting.TenantTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&tenantReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&tenant, &tenantReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set Date = time.Now()
	tenant.ID = primitive.NewObjectID()
	tenant.CreatedDate = currentTime
	tenant.UpdatedDate = currentTime

	//Tenant Configuration.
	for _, addTenantSetting := range tenantReq.TenantSettingRequest {
		//Copy fields value from Input model to main model - copy( mainModel, inputModel)
		err = copier.Copy(&tenantSetting, &addTenantSetting)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy Tenant Setting Request into Main Model ERROR - "+err.Error())
			return
		}
		// Assign others value to main model.
		tenantSetting.TenantKey = tenant.ID
		tenantSetting.ID = primitive.NewObjectID()
		tenantSetting.CreatedDate = currentTime
		tenantSetting.UpdatedDate = currentTime
		tenantSetting.IsDeleted = false

		uiTenantSettings = append(uiTenantSettings, tenantSetting)
	}

	if len(uiTenantSettings) > 0 {
		collection = database.Database.Collection(setting.TenantSettingTable)
		_, err = collection.InsertMany(context.TODO(), uiTenantSettings)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Tenant Settings ERROR - "+err.Error())
			return
		}
	}

	collection = database.Database.Collection(setting.TenantTable)
	_, err := collection.InsertOne(ctx, tenant)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Tenant ERROR - "+err.Error())
		return
	}

	if err = service.CheckDefaultTenantSetting(tenant.ID); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Check Default Tenant Setting ERROR - "+err.Error())
		return
	}

	_ = copier.Copy(&tenantDetails, &tenant)
	byte, _ := json.Marshal(uiTenantSettings)
	json.Unmarshal(byte, &tenantDetails.TenantSetting)

	data := bson.M{"tenant": tenantDetails}
	api_helper.SuccessResponse(response, data, "Create Tenant successfully")
}

/*
func CreateDefaultTenantSettings(tenantKey primitive.ObjectID){
	var (
		settings = []string{"smtp_server","smtp_user","smtp_password","smtp_port","smtp_ssl","decimal_quantity","smtp_port","smtp_ssl","decimal_quantity"}
		existsSettings []string
		collection *mongo.Collection
		filter bson.D
		findOptions = options.Find()

	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	collection = database.Database.Collection(setting.TenantSettingTable)
	filter = bson.D{{"tenant_key", bsonx.ObjectID(tenantKey)}}
	projection := bson.D{{"setting_id",1},{"_id",0}}
	findOptions.SetProjection(projection)

	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		logger.Logger.Error(err.Error())
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {

	}
}
*/
// Get Tenant By Id godoc
// @Tags Tenant
// @Summary Get Tenant by Id
// @Description Get Tenant by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Tenant need to be found"
// @Success 200 {object} model.Tenant
// @Header 200 {string} Token "qwerty"
// @Router /Tenant/{id} [get]
func GetTenantById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetTenantById")
	response.Header().Set("content-type", "application/json")

	var (
		collection *mongo.Collection
		tenant     model.TenantResponse
	)

	collection = database.Database.Collection(setting.TenantTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.D{{"is_deleted", false}},
				bson.D{{"_id", bsonx.ObjectID(id)}},
			},
			},
		},
	},
	}
	// lookup = join table on ....
	lookupTenantSettings := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.TenantSettingTable},
				{"localField", "_id"},
				{"foreignField", "tenant_key"},
				{"as", "tenant_settings"},
			},
		},
	}
	pipeline := mongo.Pipeline{
		match,
		lookupTenantSettings,
	}

	cursor, err := collection.Aggregate(ctx, pipeline) //findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	cursor.Next(ctx)
	err = cursor.Decode(&tenant)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"tenant": tenant}
	api_helper.SuccessResponse(response, data, "Get Tenant by ID successfully")
}

// Get number of Tenants godoc
// @Tags Tenant
// @Summary Get number of Tenants sort by created_date field descending
// @Description Get number of Tenants depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.Tenant
// @Header 200 {string} Token "qwerty"
// @Router /Tenant [get]
func GetTenants(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetTenants")
	//Get Limit and Page
	var (
		tenantWithDetails    model.TenantResponse
		allTenantWithDetails []model.TenantResponse
	)

	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	response.Header().Set("content-type", "tenant/json")

	collection := database.Database.Collection(setting.TenantTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// match = Where
	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.D{
					{"is_deleted", false},
				},
			},
			},
		},
	}}
	// lookup = join table on ....
	lookupTenantSetting := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.TenantSettingTable},
				{"localField", "_id"},
				{"foreignField", "tenant_key"},
				{"as", "tenant_settings"},
			},
		},
	}
	// Sort = Order by , "1" = asc , "-1" = desc
	sort := bson.D{{"$sort", bson.D{{"created_date", -1}}}}
	// Skip = Skip number of record
	skip := bson.D{{"$skip", limit * (page - 1)}}
	// lim = Limit number of record in 1 page.
	lim := bson.D{{"$limit", limit}}
	pipeline := mongo.Pipeline{
		match,
		lookupTenantSetting,
		sort,
		skip,
		lim,
	}

	cursor, err := collection.Aggregate(ctx, pipeline) //findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		_ = cursor.Decode(&tenantWithDetails)
		allTenantWithDetails = append(allTenantWithDetails, tenantWithDetails)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"tenant": allTenantWithDetails}
	api_helper.SuccessResponse(response, data, "Get Tenant with details successfully")
}

func UpdateTenant(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "UpdateTenant")
	var (
		collection    *mongo.Collection
		tenant        model.Tenant
		tenantReq     model.TenantRequest
		tenantSetting model.TenantSetting
		currentTime   = time.Now()
		opts          = options.Update().SetUpsert(true)
	)

	response.Header().Set("content-type", "tenant/json")

	collection = database.Database.Collection(setting.TenantTable)
	tenantSettingCollection := database.Database.Collection(setting.TenantSettingTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	// Filter to find Application by Id, is_deleted = false
	tenantFilter := bson.M{
		"_id":        bsonx.ObjectID(id),
		"is_deleted": false,
	}

	//Find Application By Id
	err = collection.FindOne(ctx, tenantFilter).Decode(&tenant)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find or Decode ERROR - "+err.Error())
		return
	}

	// Decode Request.body to Input Model
	err = json.NewDecoder(request.Body).Decode(&tenantReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	// Copy fields Value from Input Model to Main Model
	err = copier.Copy(&tenant, &tenantReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	for _, tenantSettingReq := range tenantReq.TenantSettingRequest {
		//check if new Configuration ID exists in DB
		tenantFilter := bson.M{
			"_id":        bson.M{"$ne": bsonx.ObjectID(tenantSettingReq.ID)},
			"tenant_key": bsonx.ObjectID(tenant.ID),
			"setting_id": tenantSettingReq.SettingId,
			"is_deleted": false,
		}

		count, err := tenantSettingCollection.CountDocuments(ctx, tenantFilter)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ID Exists ERROR - "+err.Error())
			return
		}
		if count > 0 {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, fmt.Sprintf("Setting ID '%s' already exists", tenantSettingReq.SettingId))
			return
		}

		//Find Tenant setting in DB.
		tenantFilter = bson.M{"_id": bsonx.ObjectID(tenantSettingReq.ID)}
		_ = tenantSettingCollection.FindOne(ctx, tenantFilter).Decode(&tenantSetting)

		err = copier.Copy(&tenantSetting, &tenantSettingReq)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
			return
		}

		// applicationConfig.ID Is Zero ===>>> This is a new record.
		if primitive.ObjectID.IsZero(tenantSetting.ID) {
			tenantSetting.ID = primitive.NewObjectID()
			tenantSetting.TenantKey = tenant.ID
			tenantSetting.IsDeleted = false
			tenantSetting.ObjectType = setting.ApplicationConfigurationTable
			tenantSetting.CreatedDate = currentTime
		}
		tenantSetting.UpdatedDate = currentTime

		tenantSettingFilter := bson.M{"_id": bsonx.ObjectID(tenantSetting.ID)}
		tenantSettingUpdate := bson.M{"$set": tenantSetting}

		// When Update one, upsert config is turn on
		// If result match with filter ==> It will Update
		// Else ==> Insert new Record.
		_, err = tenantSettingCollection.UpdateOne(ctx, tenantSettingFilter, tenantSettingUpdate, opts)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, fmt.Sprintf("Insert/Update Tenant Setting ID '%s' ERROR - %s", tenantSetting.SettingId, err.Error()))
			return
		}

	}

	//Set lai thoi gian luc update
	tenant.UpdatedDate = currentTime

	tenantUpdate := bson.M{"$set": tenant}
	_, err := collection.UpdateOne(ctx, tenantFilter, tenantUpdate)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Tenant ERROR - "+err.Error())
		return
	}

	//if err = service.CheckDefaultTenantSetting(tenant.ID); err != nil {
	//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Check Tenant Setting ERROR - "+err.Error())
	//	return
	//}

	data := bson.M{"tenant": tenant}
	api_helper.SuccessResponse(response, data, "Update Tenant successfully")
}

// Soft Delete Tenant godoc
// @Tags Tenant
// @Summary Soft Delete Tenant
// @Description Soft Delete Tenant
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Tenant need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Tenant/{id} [delete]
func DeleteSoftTenant(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteSoftTenant")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.TenantTable)
	response.Header().Set("content-type", "tenant/json")
	//var tenant model.Tenant
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	//json.NewDecoder(request.Body).Decode(&tenant)
	//Set lai thoi gian luc update
	//tenant.UpdatedDate = time.Now()
	//tenant.IsDeleted = true
	//ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	//defer cancel()
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}
	_, err := collection.UpdateOne(context.Background(), filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}
	api_helper.SuccessResponse(response, id, "Update Tenant successfully")
}

//DeleteHardTenant
/*
func DeleteHardTenant(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardTenant")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.TenantTable)
	response.Header().Set("content-type", "tenant/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("DeleteOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
//-------------------- Unused -----------------------

func CreateManyTenant(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyTenant")
	var (
		collection       *mongo.Collection
		manyAddTenant    []model.TenantRequest
		tenant           model.Tenant
		tenantSetting    model.TenantSetting
		uiTenant         []interface{}
		uiTenantSettings []interface{}
		currentTime      = time.Now()
	)
	collection = database.Database.Collection(setting.TenantTable)
	response.Header().Set("content-type", "tenant/json")
	err = json.NewDecoder(request.Body).Decode(&manyAddTenant)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	for _, addTenant := range manyAddTenant {
		//api_helper.ValidateTenant(addTenant)
		err = copier.Copy(&tenant, &addTenant)
		if err != nil {
			response.WriteHeader(http.StatusBadRequest)
			response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
			return
		}

		tenant.ID = primitive.NewObjectID()
		tenant.CreatedDate = currentTime
		tenant.UpdatedDate = currentTime
		tenant.IsActive = true
		tenant.IsDeleted = false

		uiTenant = append(uiTenant, tenant)

		//Tenant Configuration.
		for _, addTenantSetting := range addTenant.TenantSettingRequest {
			//Copy fields value from Input model to main model - copy( mainModel, inputModel)
			err = copier.Copy(&tenantSetting, &addTenantSetting)
			if err != nil {
				response.WriteHeader(http.StatusBadRequest)
				response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
				return
			}
			// Assign others value to main model.
			tenantSetting.TenantKey = tenant.ID
			tenantSetting.ID = primitive.NewObjectID()
			tenantSetting.CreatedDate = currentTime
			tenantSetting.UpdatedDate = currentTime
			tenantSetting.IsDeleted = false
			uiTenantSettings = append(uiTenantSettings, tenantSetting)
		}
	}

	_, err := collection.InsertMany(context.TODO(), uiTenant)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	collection = database.Database.Collection(setting.TenantSettingTable)
	_, err = collection.InsertMany(context.TODO(), uiTenantSettings)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(map[string]interface{}{
		"tenant":          uiTenant,
		"tenant_settings": uiTenantSettings,
	})
}

// Update Tenant godoc
// @Tags Tenant
// @Summary Update Tenant
// @Description Update Tenant
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Tenant need to be found"
// @Param body body model.TenantRequest true "Update Tenant"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Tenant/{id} [put]
func UpdateTenantOld(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "UpdateTenant")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.TenantTable)
	response.Header().Set("content-type", "tenant/json")
	var tenant model.Tenant
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	json.NewDecoder(request.Body).Decode(&tenant)
	//Set lai thoi gian luc update
	tenant.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": tenant}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}
