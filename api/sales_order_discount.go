package api

import (
	"../setting"
	"context"
	"encoding/json"
	"net/http"
	"os"
	"time"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateSalesOrderDiscount godoc
// @Tags Sales Order Discount
// @Summary Add Sales Order Discount
// @Description Add Sales Order Discount
// @Accept  json
// @Produce  json
// @Param body body []model.SalesOrderDiscountRequest true "Add Sales Order Discount"
// @Success 200 {object} []model.SalesOrderDiscount
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderDiscount [POST]
func CreateSalesOrderDiscount(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateSalesOrderDiscount")
	var (
		collection         *mongo.Collection
		salesOrderDiscount model.SalesOrderDiscount
	)

	collection = database.Database.Collection(setting.SalesOrderDiscountTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&salesOrderDiscount)

	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	currentTime := time.Now()
	salesOrderDiscount.CreatedDate = currentTime
	salesOrderDiscount.UpdatedDate = currentTime

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	result, err := collection.InsertOne(ctx, salesOrderDiscount)
	if err != nil {
		json.NewEncoder(response).Encode(err.Error())
		return
	}
	id := result.InsertedID
	salesOrderDiscount.ID = id.(primitive.ObjectID)
	json.NewEncoder(response).Encode(salesOrderDiscount)
}

// CreateManySalesOrderDiscount func
func CreateManySalesOrderDiscount(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManySalesOrderDiscount")
	var (
		collection          *mongo.Collection
		salesOrderDiscounts []model.SalesOrderDiscount
	)
	collection = database.Database.Collection(setting.SalesOrderDiscountTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&salesOrderDiscounts)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	currentTime := time.Now()
	for _, salesOrderDiscount := range salesOrderDiscounts {
		salesOrderDiscount.ID = primitive.NewObjectID()
		salesOrderDiscount.CreatedDate = currentTime
		salesOrderDiscount.UpdatedDate = currentTime
		ui = append(ui, salesOrderDiscount)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// GetSalesOrderDiscountByID godoc
// @Tags Sales Order Discount
// @Summary Get Sales Order Discount by Id
// @Description Get Sales Order Discount by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Order Discount need to be found"
// @Success 200 {object} model.SalesOrderDiscount
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderDiscount/{id} [get]
func GetSalesOrderDiscountByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSaleOrderDiscountByID")
	var (
		collection         *mongo.Collection
		salesOrderDiscount model.SalesOrderDiscount
	)
	collection = database.Database.Collection(setting.SalesOrderDiscountTable)

	response.Header().Set("content-type", "application/json")

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	err := collection.FindOne(ctx, filter).Decode(&salesOrderDiscount)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesOrderDiscount)
}

// GetSalesOrderDiscounts godoc
// @Tags Sales Order Discount
// @Summary Get all Sales Order Discount
// @Description Get all Sales Order Discount
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.SalesOrderDiscount
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderDiscount [get]
func GetSalesOrderDiscounts(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesOrderDiscounts")
	var (
		collection          *mongo.Collection
		salesOrderDiscounts []model.SalesOrderDiscount
		findOptions         = options.Find()
		filter              = bson.M{}
	)
	collection = database.Database.Collection(setting.SalesOrderDiscountTable)

	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions) //findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var saleOrderDiscount model.SalesOrderDiscount
		cursor.Decode(&saleOrderDiscount)
		salesOrderDiscounts = append(salesOrderDiscounts, saleOrderDiscount)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesOrderDiscounts)
}

// UpdateSalesOrderDiscount godoc
// @Tags Sales Order Discount
// @Summary Update Sales Order Discount
// @Description Update Sales Order Discount
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Order need to be found"
// @Param body body model.SalesOrderRequest true "Update Sales Order Discount"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderDiscount/{id} [put]
func UpdateSalesOrderDiscount(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateSaleOrderDiscount")
	var (
		collection        *mongo.Collection
		saleOrderDiscount model.SalesOrderDiscount
	)
	collection = database.Database.Collection(setting.SalesOrderDiscountTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&saleOrderDiscount)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	saleOrderDiscount.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": saleOrderDiscount}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

func DeleteSoftSalesOrderDiscount(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftSalesOrderDiscount")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.SalesOrderDiscountTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
