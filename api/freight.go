package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../database"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

// Add Freight godoc
// @Tags Freight
// @Summary Add Freight
// @Description Add Freight
// @Accept  json
// @Produce  json
// @Param body body model.FreightRequest true "Add Freight"
// @Success 200 {object} model.Freight
// @Header 200 {string} Token "qwerty"
// @Router /Freight [POST]
func CreateFreight(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateFreight")
	var (
		collection *mongo.Collection
		freight    model.Freight
		freightReq model.FreightRequest
	)

	response.Header().Set("content-type", "application/json")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	collection = database.Database.Collection(setting.FreightTable)
	err = json.NewDecoder(request.Body).Decode(&freightReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&freight, &freightReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set Date = time.Now()
	currentTime := time.Now()
	freight.ID = primitive.NewObjectID()
	freight.CreatedDate = currentTime
	freight.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, freight)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	data := bson.M{"freight": freight}
	api_helper.SuccessResponse(response, data, "Create Freight successfully")
}

// Get Freight By Id godoc
// @Tags Freight
// @Summary Get Freight by Id
// @Description Get Freight by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Freight need to be found"
// @Success 200 {object} model.Freight
// @Header 200 {string} Token "qwerty"
// @Router /Freight/{id} [get]
func GetFreightById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetFreightById")
	var (
		collection *mongo.Collection
		freight    model.Freight
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.FreightTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err := collection.FindOne(ctx, filter).Decode(&freight)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	data := bson.M{"freight": freight}
	api_helper.SuccessResponse(response, data, "Get Freight Successfully")
}

// Get number of Freights godoc
// @Tags Freight
// @Summary Get number of Freights sort by created_date field descending
// @Description Get number of Freights depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.Freight
// @Header 200 {string} Token "qwerty"
// @Router /Freight [get]
func GetFreights(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetFreights")

	var (
		allFreight []model.Freight
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.FreightTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{"is_deleted": bson.M{"$eq": false}}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))

	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var freight model.Freight
		_ = cursor.Decode(&freight)
		allFreight = append(allFreight, freight)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"freight": allFreight}
	api_helper.SuccessResponse(response, data, "Get Freight Successfully")
}

// Update Freight godoc
// @Tags Freight
// @Summary Update Freight
// @Description Update Freight
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Freight need to be found"
// @Param body body model.FreightRequest true "Update Freight"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Freight/{id} [put]
func UpdateFreight(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var (
		collection *mongo.Collection
		freight    model.Freight
		freightReq model.FreightRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.FreightTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id)}

	err = collection.FindOne(ctx, filter).Decode(&freight)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = json.NewDecoder(request.Body).Decode(&freightReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&freight, &freightReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set lai thoi gian luc update
	freight.UpdatedDate = time.Now()

	update := bson.M{"$set": freight}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"freight": freight}
	api_helper.SuccessResponse(response, data, "Update Freight Successfully")
}

// Soft Delete Freight godoc
// @Tags Freight
// @Summary Soft Delete Freight
// @Description Soft Delete Freight
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Freight need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Freight/{id} [delete]
func DeleteSoftFreight(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.FreightTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id)}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	api_helper.SuccessResponse(response, id, "Delete Freight successfully")
}

//-------------------- Unused -----------------------
func CreateManyFreight(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyFreight")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.FreightTable)
	response.Header().Set("content-type", "application/json")
	var manyFreight []model.Freight
	err = json.NewDecoder(request.Body).Decode(&manyFreight)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, freight := range manyFreight {
		freight.ID = primitive.NewObjectID()
		freight.CreatedDate = time.Now()
		freight.UpdatedDate = time.Now()
		ui = append(ui, freight)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

//DeleteHardFreight
/*
func DeleteHardFreight(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.FreightTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
