package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"reflect"
	"strings"
	"time"

	"../setting"
	"go.mongodb.org/mongo-driver/mongo/options"

	"../database"
	"../model"
	"../model/enum"
	"../service"
	sHelper "../service/helper"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateSalesOrder godoc
// @Tags Sales Order
// @Summary Add Sales Order
// @Description Add Sales Order
// @Accept  json
// @Produce  json
// @Param body body model.SalesOrderRequest true "Add Sales Order"
// @Success 200 {object} model.SalesOrder
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrder [POST]
func CreateSalesOrder(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateSalesOrder")
	var (
		salesCollection            *mongo.Collection
		collection                 *mongo.Collection
		salesOrderRequest          model.SalesOrderRequest
		salesOrder                 model.SalesOrder
		salesOrderWithDetail       model.SalesOrderResponse
		customer                   model.Customer
		appToLookupSite            string
		uiSalesOrderDetail         []interface{}
		uiSalesOrderComment        []interface{}
		uiSalesOrderDiscount       []interface{}
		uiSalesOrderFreight        []interface{}
		uiSalesOrderShippingDetail []interface{}
		uiSalesOrderPayment        []interface{}
	)
	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	salesCollection = database.Database.Collection(setting.SalesOrderTable)
	err = json.NewDecoder(request.Body).Decode(&salesOrderRequest)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Sales Order Input ERROR - "+err.Error())
		return
	}

	err = salesOrderRequest.Validation()
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Input is invalid - "+err.Error())
		return
	}
	err = salesOrderRequest.IsDuplicateDocNum()
	if err != nil {
		var res model.ResponseResult
		res.Success = true
		res.Message = "Duplicate Doc num"
		res.ErrorCode = 999
		json.NewEncoder(response).Encode(res)
		return
	}

	err = copier.Copy(&salesOrder, &salesOrderRequest)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy Sales Order Input ERROR - "+err.Error())
		return
	}

	salesOrder.SalesChannelKey, _ = sHelper.GetKeyFromId(setting.SalesChannelTable, "sales_channel_id", salesOrderRequest.SalesChannelId)

	salesOrder.CountryKey, _ = sHelper.GetKeyFromId(setting.CountryTable, "country_id", salesOrderRequest.CountryId)

	if strings.ToUpper(salesOrder.SalesChannelId) == setting.SalesChannelPumaCom {
		appToLookupSite = enum.AppIdSFCC
	} else {
		appToLookupSite = enum.AppIdSIA
	}

	salesOrder.SiteId, err = service.GetApplicationConfigValue(appToLookupSite, salesOrder.SalesChannelName)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, fmt.Sprintf("Get OMS Site From [%s] Application ERROR - %s", appToLookupSite, err.Error()))
		return
	}
	salesOrder.AccountCode, err = service.GetApplicationConfigValue(enum.AppIdYCH, salesOrder.SiteId)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get OMS Site  From SIA Application ERROR - "+err.Error())
		return
	}

	salesOrder.SiteKey, _ = sHelper.GetKeyFromId(setting.SiteTable, "site_id", salesOrder.SiteId)

	salesOrder.CurrencyKey, _ = sHelper.GetKeyFromId(setting.CurrencyTable, "currency_id", salesOrder.CurrencyId)

	currentTime := time.Now()
	salesOrder.ID = primitive.NewObjectIDFromTimestamp(currentTime)
	//Create SID for SAP
	time.Sleep(time.Second * 2)
	salesOrder.SID = fmt.Sprintf("%v", time.Now().Unix())
	//salesOrder.OrderStatus = enum.OrderStatusNew
	salesOrder.ObjectType = setting.SalesOrderTable
	salesOrder.CreatedDate = currentTime
	salesOrder.UpdatedDate = currentTime

	// Sales Order Detail
	if len(salesOrderRequest.SalesOrderDetailRequest) > 0 {
		for _, detail := range salesOrderRequest.SalesOrderDetailRequest {
			var (
				salesOrderDetail model.SalesOrderDetail
				product          model.Product
			)
			err = copier.Copy(&salesOrderDetail, &detail)
			if err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy Sales Order Detail Input ERROR - "+err.Error())
				return
			}
			salesOrderDetail.ID = primitive.NewObjectID()
			salesOrderDetail.DocKey = salesOrder.ID
			salesOrderDetail.DocNum = salesOrder.DocNum
			//salesOrderDetail.DocLineStatus = enum.OrderLineStatusNew
			salesOrderDetail.CreatedDate = currentTime
			salesOrderDetail.UpdatedDate = currentTime

			if err = database.Database.Collection(setting.ProductTable).FindOne(ctx, bson.M{"product_id": salesOrderDetail.ProductCode}).Decode(&product); err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Product '"+salesOrderDetail.ProductCode+"' ERROR - "+err.Error())
				return
			}
			salesOrderDetail.ProductKey = product.ID
			salesOrderDetail.ProductColor = product.Color
			salesOrderDetail.ProductSize = product.Size
			salesOrderDetail.ProductUom = product.Uom
			salesOrderDetail.ImageUrl = product.ImageUrl

			collection = database.Database.Collection(setting.SalesOrderDetailTable)
			// Sales Order Detail
			_, err = collection.InsertOne(ctx, salesOrderDetail)
			if err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Sales Order Detail '"+detail.DocLineNum+"' ERROR - "+err.Error())
				return
			}

			err = service.HandleSalesOrderDetailStatusChange(salesOrderDetail.DocNum, salesOrderDetail.DocLineNum, enum.OrderLineStatusNew)
			if err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Handle Sales Order Detail '"+detail.DocLineNum+" status change' ERROR - "+err.Error())
				return
			}

			salesOrderDetail.DocLineStatus = enum.OrderLineStatusNew
			uiSalesOrderDetail = append(uiSalesOrderDetail, salesOrderDetail)
			salesOrderWithDetail.SalesOrderDetail = append(salesOrderWithDetail.SalesOrderDetail, salesOrderDetail)
		}
	} else {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Please enter at least one line for order detail.")
		return
	}

	// Sales Order Comment
	for _, comment := range salesOrderRequest.SalesOrderCommentRequest {
		var (
			salesOrderComment model.SalesOrderComment
		)
		err = copier.Copy(&salesOrderComment, &comment)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy Sales Order Comment Input ERROR - "+err.Error())
			return
		}
		salesOrderComment.DocKey = salesOrder.ID
		salesOrderComment.ID = primitive.NewObjectID()
		//salesOrderComment.CommentKey = salesOrderComment.ID
		salesOrderComment.CreatedDate = currentTime
		salesOrderComment.UpdatedDate = currentTime
		uiSalesOrderComment = append(uiSalesOrderComment, salesOrderComment)
		salesOrderWithDetail.SalesOrderComment = append(salesOrderWithDetail.SalesOrderComment, salesOrderComment)
	}

	// Sales Order Freight
	for _, freight := range salesOrderRequest.SalesOrderFreightRequest {
		var (
			salesOrderFreight model.SalesOrderFreight
		)
		err = copier.Copy(&salesOrderFreight, &freight)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy Sales Order Freight Input ERROR - "+err.Error())
			return
		}
		salesOrderFreight.DocKey = salesOrder.ID
		salesOrderFreight.ID = primitive.NewObjectID()
		//salesOrderFreight.DocFreightKey = salesOrderFreight.ID
		salesOrderFreight.CreatedDate = currentTime
		salesOrderFreight.UpdatedDate = currentTime
		uiSalesOrderFreight = append(uiSalesOrderFreight, salesOrderFreight)
		salesOrderWithDetail.SalesOrderFreight = append(salesOrderWithDetail.SalesOrderFreight, salesOrderFreight)

	}

	// Sales Order Discount
	for _, discount := range salesOrderRequest.SalesOrderDiscountRequest {
		var (
			salesOrderDiscount model.SalesOrderDiscount
		)
		err = copier.Copy(&salesOrderDiscount, &discount)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy Sales Order Discount Input ERROR - "+err.Error())
			return
		}
		salesOrderDiscount.DocKey = salesOrder.ID
		salesOrderDiscount.ID = primitive.NewObjectID()
		//salesOrderDiscount.DocDiscountKey = salesOrderDiscount.ID
		salesOrderDiscount.CreatedDate = currentTime
		salesOrderDiscount.UpdatedDate = currentTime
		uiSalesOrderDiscount = append(uiSalesOrderDiscount, salesOrderDiscount)
		salesOrderWithDetail.SalesOrderDiscount = append(salesOrderWithDetail.SalesOrderDiscount, salesOrderDiscount)
	}

	// Sales Order Shipping Detail
	for _, shippingDetail := range salesOrderRequest.SalesOrderShippingDetailRequest {
		var (
			salesOrderShippingDetail model.SalesOrderShippingDetail
		)
		err = copier.Copy(&salesOrderShippingDetail, &shippingDetail)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy Sales Order Shipping Detail Input ERROR - "+err.Error())
			return
		}
		salesOrderShippingDetail.DocKey = salesOrder.ID
		salesOrderShippingDetail.ID = primitive.NewObjectID()
		//salesOrderShippingDetail.DocShipKey = salesOrderShippingDetail.ID
		salesOrderShippingDetail.CreatedDate = currentTime
		salesOrderShippingDetail.UpdatedDate = currentTime
		uiSalesOrderShippingDetail = append(uiSalesOrderShippingDetail, salesOrderShippingDetail)
		salesOrderWithDetail.SalesOrderShippingDetail = append(salesOrderWithDetail.SalesOrderShippingDetail, salesOrderShippingDetail)
	}

	// Sales Order Payment
	for _, payment := range salesOrderRequest.SalesOrderPaymentRequest {
		var (
			salesOrderPayment model.SalesOrderPayment
		)
		err = copier.Copy(&salesOrderPayment, &payment)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy Sales Order Payment Input ERROR - "+err.Error())
			return
		}
		salesOrderPayment.DocKey = salesOrder.ID
		salesOrderPayment.ID = primitive.NewObjectID()
		salesOrderPayment.PaymentTypeKey = salesOrderPayment.ID
		salesOrderPayment.CreatedDate = currentTime
		salesOrderPayment.UpdatedDate = currentTime
		uiSalesOrderPayment = append(uiSalesOrderPayment, salesOrderPayment)
		salesOrderWithDetail.SalesOrderPayment = append(salesOrderWithDetail.SalesOrderPayment, salesOrderPayment)
	}

	/*
		// Sales Order Package
		for _, pack := range salesOrderRequest.SalesOrderPackageRequest {
			var (
				orderPackage model.SalesOrderPackage
			)
			err = copier.Copy(&orderPackage, &pack)
			if err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Copy Sales Order Payment Input ERROR - "+err.Error())
				return
			}
			orderPackage.DocKey = salesOrder.ID
			orderPackage.ID = primitive.NewObjectID()
			orderPackage.ObjectType = setting.SalesOrderPackageTable
			orderPackage.CreatedDate = currentTime
			orderPackage.UpdatedDate = currentTime

			if _, err = database.Database.Collection(setting.SalesOrderPackageTable).InsertOne(ctx,orderPackage); err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Insert Sales Order Comment ERROR - "+err.Error())
				return
			}

			salesOrderWithDetail.SalesOrderPackage = append(salesOrderWithDetail.SalesOrderPackage, orderPackage)
		}
	*/
	if len(uiSalesOrderComment) > 0 {
		collection = database.Database.Collection(setting.SalesOrderCommentTable)
		// Sales Order Comment
		_, err = collection.InsertMany(context.TODO(), uiSalesOrderComment)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Sales Order Comment ERROR - "+err.Error())
			return
		}
	}
	if len(uiSalesOrderFreight) > 0 {
		collection = database.Database.Collection(setting.SalesOrderFreightTable)
		// Sales Order Freight
		_, err = collection.InsertMany(context.TODO(), uiSalesOrderFreight)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Sales Order Freight ERROR - "+err.Error())
			return
		}
	}
	if len(uiSalesOrderDiscount) > 0 {
		collection = database.Database.Collection(setting.SalesOrderDiscountTable)
		// Sales Order Discount
		_, err = collection.InsertMany(context.TODO(), uiSalesOrderDiscount)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Sales Order Discount ERROR - "+err.Error())
			return
		}
	}
	if len(uiSalesOrderShippingDetail) > 0 {
		collection = database.Database.Collection(setting.SalesOrderShippingDetailTable)
		// Sales Order Shipping Detail
		_, err = collection.InsertMany(context.TODO(), uiSalesOrderShippingDetail)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Sales Order Shipping Detail ERROR - "+err.Error())
			return
		}
	}
	if len(uiSalesOrderPayment) > 0 {
		collection = database.Database.Collection(setting.SalesOrderPaymentTable)
		// Sales Order Payment
		_, err = collection.InsertMany(context.TODO(), uiSalesOrderPayment)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Sales Order Payment ERROR - "+err.Error())
			return
		}
	}

	//err = HandleOrderStatusChange(salesOrder.DocNum, enum.OrderStatusNew)
	//if err != nil {
	//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Handle Order Status Change ERROR - "+err.Error())
	//	return
	//}

	if salesOrder.Email == "noreply@rciapac.com" && salesOrder.ShippingAddress.SPhone != "" {
		phoneNo, _ := sHelper.RegexOnlyNumber(salesOrder.ShippingAddress.SPhone)
		salesOrder.Email = phoneNo + "@puma.com"
		_ = database.Database.Collection(setting.CustomerTable).FindOne(ctx, bson.M{"email": salesOrder.Email}).Decode(&customer)
	} else {
		_ = database.Database.Collection(setting.CustomerTable).FindOne(ctx, bson.M{"email": salesOrder.Email}).Decode(&customer)
	}

	salesOrder.ShippingAddress.SPhone = sHelper.IsEmptyString(salesOrder.ShippingAddress.SPhone, "N/A")
	salesOrder.CustomerName = sHelper.IsEmptyString(salesOrder.CustomerName, "N/A")

	if customer.ID != primitive.NilObjectID {
		salesOrder.CustomerKey = customer.ID
	} else {
		customerAfter, _ := service.CreateCustomerFromSalesOrder(salesOrder)
		salesOrder.CustomerKey = customerAfter.ID
	}

	_, err := salesCollection.InsertOne(ctx, salesOrder)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Sales Order ERROR - "+err.Error())
		return
	}

	salesOrder, err = service.HandleOrderStatusChange(salesOrder.DocNum, enum.OrderStatusNew, false)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Handle Sales Order Status update ERROR - "+err.Error())
		return
	}

	_ = copier.Copy(&salesOrderWithDetail, &salesOrder)

	data := bson.M{
		"sales_order": salesOrderWithDetail,
	}
	api_helper.SuccessResponse(response, data, "Create Sales Order successfully")
}

// GetSalesOrderByID godoc
// @Tags Sales Order
// @Summary Get Sales Order by Id
// @Description Get Sales Order by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Order need to be found"
// @Success 200 {object} model.SalesOrder
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrder/{id} [get]
func GetSalesOrderByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesOrders")

	var (
		collection *mongo.Collection
		salesOrder model.SalesOrderResponse
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.SalesOrderTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//Match with Id
	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.D{{"_id", bsonx.ObjectID(id)}},
			},
			},
		},
	}}
	//Look up for foreign key
	lookupCountry := bson.D{
		{"$lookup", bson.D{
			{"from", "country"},
			{"localField", "country_key"},
			{"foreignField", "_id"},
			{"as", "country"},
		},
		}}
	lookupSite := bson.D{
		{"$lookup", bson.D{
			{"from", "site"},
			{"localField", "site_key"},
			{"foreignField", "_id"},
			{"as", "site"},
		},
		}}
	lookupSalesChannel := bson.D{
		{"$lookup", bson.D{
			{"from", "sales_channel"},
			{"localField", "sales_channel_key"},
			{"foreignField", "_id"},
			{"as", "sales_channel"},
		},
		}}
	lookupTenant := bson.D{
		{"$lookup", bson.D{
			{"from", "tenant"},
			{"localField", "tenant_key"},
			{"foreignField", "_id"},
			{"as", "tenant"},
		},
		}}
	lookupCustomer := bson.D{
		{"$lookup", bson.D{
			{"from", "customer"},
			{"localField", "customer_key"},
			{"foreignField", "_id"},
			{"as", "customer"},
		},
		}}
	lookupCurrency := bson.D{
		{"$lookup", bson.D{
			{"from", "currency"},
			{"localField", "currency_key"},
			{"foreignField", "_id"},
			{"as", "currency"},
		},
		}}
	lookupSalesOrderDetails := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", "sales_order_details"},
		},
		}}
	lookupSalesOrderComments := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderCommentTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", "sales_order_comments"},
		},
		}}
	lookupSalesOrderFreights := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderFreightTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", "sales_order_freights"},
		},
		}}
	lookupSalesOrderDiscounts := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderDiscountTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", "sales_order_discounts"},
		},
		}}
	lookupSalesOrderShippingDetails := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderShippingDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", "sales_order_shipping_details"},
		},
		}}
	lookupSalesOrderPayments := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderPaymentTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", setting.SalesOrderPaymentTable + "s"},
			},
		}}
	lookupSalesOrderPaymentCaptures := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderPaymentCaptureTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", setting.SalesOrderPaymentCaptureTable + "s"},
			},
		}}
	lookupSalesOrderPaymentRefunds := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderPaymentRefundTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", setting.SalesOrderPaymentRefundTable + "s"},
			},
		}}
	lookupSalesOrderPaymentReverses := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderPaymentReverseTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", setting.SalesOrderPaymentReverseTable + "s"},
			},
		}}
	lookupSalesOrderStatus := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderStatusTable},
			{"let", bson.M{"dn": "$doc_num"}},
			{"pipeline", bson.A{
				bson.M{"$match": bson.M{
					"$expr": bson.M{
						"$and": bson.A{
							bson.M{"$eq": bson.A{"$doc_num", "$$dn"}},
							bson.M{"$eq": bson.A{"$doc_line_num", ""}},
						},
					},
				}},
			}},
			{"as", setting.SalesOrderStatusTable},
		},
		}}
	lookupOrderPack := bson.D{
		{"$lookup", bson.D{
			{"from", setting.OrderPackTable},
			{"localField", "doc_num"},
			{"foreignField", "order_no"},
			{"as", setting.OrderPackTable},
		},
		}}

	//Unwind array to object
	unwindOrderPack := bson.D{
		{"$unwind", bson.D{
			{"path", "$" + setting.OrderPackTable},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindCountry := bson.D{
		{"$unwind", bson.D{
			{"path", "$country"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindSite := bson.D{
		{"$unwind", bson.D{
			{"path", "$site"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindSalesChannel := bson.D{
		{"$unwind", bson.D{
			{"path", "$sales_channel"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindTenant := bson.D{
		{"$unwind", bson.D{
			{"path", "$tenant"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindCustomer := bson.D{
		{"$unwind", bson.D{
			{"path", "$customer"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindCurrency := bson.D{
		{"$unwind", bson.D{
			{"path", "$currency"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	sumSalesOrderDetailQuantity := bson.D{
		{"$addFields", bson.D{
			{"order_quantity", bson.D{
				{"$sum", "$sales_order_details.quantity"},
			}},
		}},
	}
	sumSalesOrderDetailReturnQuantity := bson.D{
		{"$addFields", bson.D{
			{"return_quantity", bson.D{
				{"$sum", "$sales_order_details.return_quantity"},
			}},
		}},
	}
	sumSalesOrderDetailCancelQuantity := bson.D{
		{"$addFields", bson.D{
			{"cancel_quantity", bson.D{
				{"$sum", "$sales_order_details.cancel_quantity"},
			}},
		}},
	}

	pipeline := mongo.Pipeline{
		match,
		lookupCountry,
		lookupSalesChannel,
		lookupSite,
		lookupTenant,
		lookupCustomer,
		lookupCurrency,
		lookupSalesOrderDetails,
		lookupSalesOrderComments,
		lookupSalesOrderFreights,
		lookupSalesOrderShippingDetails,
		lookupSalesOrderPayments,
		lookupSalesOrderPaymentCaptures,
		lookupSalesOrderPaymentRefunds,
		lookupSalesOrderPaymentReverses,
		lookupSalesOrderDiscounts,
		lookupSalesOrderStatus,
		lookupOrderPack,
		unwindOrderPack,
		unwindCountry,
		unwindSite,
		unwindSalesChannel,
		unwindTenant,
		unwindCustomer,
		unwindCurrency,
		sumSalesOrderDetailQuantity,
		sumSalesOrderDetailReturnQuantity,
		sumSalesOrderDetailCancelQuantity,
	}

	cursor, err := collection.Aggregate(ctx, pipeline) //findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR"+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		err := cursor.Decode(&salesOrder)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Sales Order with Details ERROR"+err.Error())
			return
		}
		//salesOrders = append(salesOrders, salesOrder)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR"+err.Error())
		return
	}

	if salesOrder.ID == primitive.NilObjectID {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "No document in Database")
		return
	}

	data := bson.M{"sales_order": salesOrder}
	api_helper.SuccessResponse(response, data, "Get Sales By ID successfully")
}

// GetSalesOrderByID godoc
// @Tags Sales Order
// @Summary Get Sales Order by Id
// @Description Get Sales Order by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Doc Number of Sales Order need to be found"
// @Success 200 {object} model.SalesOrder
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrder/doc_num/{doc_num} [get]v
func GetSalesOrderByDocNum(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesOrders")
	var (
		collection *mongo.Collection
		salesOrder model.SalesOrderResponse
		//salesOrders []model.SalesOrderResponse
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.SalesOrderTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	docNum := mux.Vars(request)["doc_num"]

	//Match with Id
	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.D{{"doc_num", docNum}},
			},
			},
		},
	}}
	//Look up for foreign key
	lookupCountry := bson.D{
		{"$lookup", bson.D{
			{"from", "country"},
			{"localField", "country_key"},
			{"foreignField", "_id"},
			{"as", "country"},
		},
		}}
	lookupSite := bson.D{
		{"$lookup", bson.D{
			{"from", "site"},
			{"localField", "site_key"},
			{"foreignField", "_id"},
			{"as", "site"},
		},
		}}
	lookupSalesChannel := bson.D{
		{"$lookup", bson.D{
			{"from", "sales_channel"},
			{"localField", "sales_channel_key"},
			{"foreignField", "_id"},
			{"as", "sales_channel"},
		},
		}}
	lookupTenant := bson.D{
		{"$lookup", bson.D{
			{"from", "tenant"},
			{"localField", "tenant_key"},
			{"foreignField", "_id"},
			{"as", "tenant"},
		},
		}}
	lookupCustomer := bson.D{
		{"$lookup", bson.D{
			{"from", "customer"},
			{"localField", "customer_key"},
			{"foreignField", "_id"},
			{"as", "customer"},
		},
		}}
	lookupCurrency := bson.D{
		{"$lookup", bson.D{
			{"from", "currency"},
			{"localField", "currency_key"},
			{"foreignField", "_id"},
			{"as", "currency"},
		},
		}}
	lookupSalesOrderDetails := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", "sales_order_details"},
		},
		}}
	lookupSalesOrderComments := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderCommentTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", "sales_order_comments"},
		},
		}}
	lookupSalesOrderFreights := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderFreightTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", "sales_order_freights"},
		},
		}}
	lookupSalesOrderDiscounts := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderDiscountTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", "sales_order_discounts"},
		},
		}}
	lookupSalesOrderShippingDetails := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderShippingDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", "sales_order_shipping_details"},
		},
		}}
	lookupSalesOrderPayments := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderPaymentTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", "sales_order_payments"},
			},
		}}
	lookupSalesOrderPaymentCaptures := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderPaymentCaptureTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", setting.SalesOrderPaymentCaptureTable + "s"},
			},
		}}
	lookupSalesOrderPaymentRefunds := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderPaymentRefundTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", setting.SalesOrderPaymentRefundTable + "s"},
			},
		}}
	lookupSalesOrderPaymentReverses := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderPaymentReverseTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", setting.SalesOrderPaymentReverseTable + "s"},
			},
		}}
	lookupSalesOrderStatus := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderStatusTable},
			{"let", bson.M{"dn": "$doc_num"}},
			{"pipeline", bson.A{
				bson.M{"$match": bson.M{
					"$expr": bson.M{
						"$and": bson.A{
							bson.M{"$eq": bson.A{"$doc_num", "$$dn"}},
							bson.M{"$eq": bson.A{"$doc_line_num", ""}},
						},
					},
				}},
			}},
			{"as", setting.SalesOrderStatusTable},
		},
		}}
	lookupOrderPack := bson.D{
		{"$lookup", bson.D{
			{"from", setting.OrderPackTable},
			{"localField", "doc_num"},
			{"foreignField", "order_no"},
			{"as", setting.OrderPackTable},
		},
		}}

	//Unwind array to object
	unwindOrderPack := bson.D{
		{"$unwind", bson.D{
			{"path", "$" + setting.OrderPackTable},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindCountry := bson.D{
		{"$unwind", bson.D{
			{"path", "$country"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindSite := bson.D{
		{"$unwind", bson.D{
			{"path", "$site"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindSalesChannel := bson.D{
		{"$unwind", bson.D{
			{"path", "$sales_channel"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindTenant := bson.D{
		{"$unwind", bson.D{
			{"path", "$tenant"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindCustomer := bson.D{
		{"$unwind", bson.D{
			{"path", "$customer"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindCurrency := bson.D{
		{"$unwind", bson.D{
			{"path", "$currency"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	sumSalesOrderDetailQuantity := bson.D{
		{"$addFields", bson.D{
			{"order_quantity", bson.D{
				{"$sum", "$sales_order_details.quantity"},
			}},
		}},
	}
	sumSalesOrderDetailReturnQuantity := bson.D{
		{"$addFields", bson.D{
			{"return_quantity", bson.D{
				{"$sum", "$sales_order_details.return_quantity"},
			}},
		}},
	}
	sumSalesOrderDetailCancelQuantity := bson.D{
		{"$addFields", bson.D{
			{"cancel_quantity", bson.D{
				{"$sum", "$sales_order_details.cancel_quantity"},
			}},
		}},
	}

	pipeline := mongo.Pipeline{
		match,
		lookupCountry,
		lookupSalesChannel,
		lookupSite,
		lookupTenant,
		lookupCustomer,
		lookupCurrency,
		lookupSalesOrderDetails,
		lookupSalesOrderComments,
		lookupSalesOrderFreights,
		lookupSalesOrderShippingDetails,
		lookupSalesOrderPayments,
		lookupSalesOrderPaymentCaptures,
		lookupSalesOrderPaymentRefunds,
		lookupSalesOrderPaymentReverses,
		lookupSalesOrderDiscounts,
		lookupSalesOrderStatus,
		lookupOrderPack,
		unwindOrderPack,
		unwindCountry,
		unwindSite,
		unwindSalesChannel,
		unwindTenant,
		unwindCustomer,
		unwindCurrency,
		sumSalesOrderDetailQuantity,
		sumSalesOrderDetailReturnQuantity,
		sumSalesOrderDetailCancelQuantity,
	}

	cursor, err := collection.Aggregate(ctx, pipeline) //findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR "+err.Error())
		return
	}

	for cursor.Next(ctx) {
		err = cursor.Decode(&salesOrder)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Sales Order with Details ERROR "+err.Error())
			return
		}
		//salesOrders = append(salesOrders, salesOrder)
		if err := cursor.Err(); err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch cursor ERROR "+err.Error())
			return
		}
		cursor.Close(ctx)
	}

	if salesOrder.ID == primitive.NilObjectID {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "No document in Database")
		return
	}

	data := bson.M{"sales_order": salesOrder}
	api_helper.SuccessResponse(response, data, "Get Sales Order by Doc Number successfully")
}

// GetSalesOrders godoc
// @Tags Sales Order
// @Summary Get all Sales Order
// @Description Get all Sales Order
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Param status path string false "Status of order . Awaiting Payment, Awaiting Fulfillment, Awaiting Shipment, Shipped, Delivered, Cancelled, Return, Closed"
// @Param country_id path string false "Country Id. of Sales Order"
// @Param sales_channel_id path string false "Sales Channel Id of Sales Order."
// @Param keyword path string false "Keyword for Doc Number, Customer name and Email searching."
// @Success 200 {object} []model.SalesOrder
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrder [get]
func GetSalesOrders(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesOrders")
	var (
		collection *mongo.Collection
		//salesOrder       model.SalesOrderResponse
		allSalesOrders   []bson.M
		matchFilter      bson.A
		matchProductCode bson.D
		//brand          model.Brand
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.SalesOrderTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	status := request.URL.Query().Get("status")
	if status != "" {
		orderStatus := enum.OrderStatus(status)
		if orderStatus, err = orderStatus.IsValid(); err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
			return
		} else {
			if orderStatus != enum.OrderStatusAll {
				matchFilter = append(matchFilter, bson.D{{"order_status", orderStatus.ToCorrectCase()}})
			}
		}
	}

	countryId := request.URL.Query().Get("country_id")
	if countryId != "" {
		//filter := bson.M{"country_id": countryId}
		//
		//collection = database.Database.Collection(setting.CountryTable)
		//err = collection.FindOne(ctx, filter).Decode(&country)
		//if err != nil {
		//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Country object ERROR - "+err.Error())
		//	return
		//}
		matchFilter = append(matchFilter, bson.D{{"country_id", countryId}})
	}

	salesChannelId := request.URL.Query().Get("sales_channel_id")
	if salesChannelId != "" {
		//filter := bson.M{"sales_channel_id": salesChannelId}
		//
		//collection = database.Database.Collection(setting.SalesChannelTable)
		//err = collection.FindOne(ctx, filter).Decode(&salesChannel)
		//if err != nil {
		//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Sales Channel object ERROR - "+err.Error())
		//	return
		//}
		matchFilter = append(matchFilter, bson.D{{"sales_channel_id", salesChannelId}})
	}

	keyword := request.URL.Query().Get("keyword")
	if keyword != "" {
		or := bson.D{{Key: "$or", Value: bson.A{
			bson.D{{"doc_num", primitive.Regex{Pattern: keyword, Options: "i"}}},
			bson.D{{"customer_name", primitive.Regex{Pattern: keyword, Options: "i"}}},
			bson.D{{"email", primitive.Regex{Pattern: keyword, Options: "i"}}},
		}}}
		matchFilter = append(matchFilter, or)
	}

	email := request.URL.Query().Get("email")
	if email != "" {
		//filter := bson.M{"sales_channel_id": salesChannelId}
		//
		//collection = database.Database.Collection(setting.SalesChannelTable)
		//err = collection.FindOne(ctx, filter).Decode(&salesChannel)
		//if err != nil {
		//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Sales Channel object ERROR - "+err.Error())
		//	return
		//}
		matchFilter = append(matchFilter, bson.D{{"email", email}})
	}

	ean := request.URL.Query().Get("ean")
	if ean != "" {
		matchProductCode = bson.D{
			{"$match", bson.M{
				setting.SalesOrderDetailTable + "s": bson.M{
					"$elemMatch": bson.M{
						"product_code": ean,
					},
				},
			}},
		}
	} else {
		matchProductCode = bson.D{
			{"$match", bson.M{
				"_id": bson.M{"$ne": bsonx.ObjectID(primitive.NilObjectID)},
			}},
		}
	}

	fromDate, toDate, err := api_helper.GetFromAndToDate(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get From and To Date ERROR - "+err.Error())
		return
	}

	matchFilter = append(matchFilter, bson.M{
		"doc_date": bson.M{
			"$gte": time.Unix(fromDate, 0),
			"$lte": time.Unix(toDate, 0),
		},
	})
	//brandId := request.URL.Query().Get("brand_id")
	//if brandId != "" {
	//	collection = database.Database.Collection(setting.SalesChannelTable)
	//	err = collection.FindOne(ctx, bson.M{"brand_id": brandId}).Decode(&brand)
	//	if err != nil {
	//		api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Sales Channel object ERROR - "+err.Error())
	//		return
	//	}
	//	matchFilter = append(matchFilter, bson.D{{"brand_key",brand.ID}})
	//}

	if len(matchFilter) == 0 {
		matchFilter = bson.A{bson.D{{"_id", bson.D{{"$ne", bsonx.ObjectID(primitive.NilObjectID)}}}}}
	}

	//Match with Id matchFilter
	match := bson.D{{
		"$match", bson.D{
			{"$and", matchFilter},
		},
	}}
	//sort, skip and limit
	sort := bson.D{{"$sort", bson.D{{"doc_date", -1}}}}
	skip := bson.D{{"$skip", limit * (page - 1)}}
	lim := bson.D{{"$limit", limit}}

	//Look up for foreign key
	//lookupCountry := bson.D{
	//	{"$lookup", bson.D{
	//		{"from", "country"},
	//		{"localField", "country_key"},
	//		{"foreignField", "_id"},
	//		{"as", "country"},
	//	},
	//	}}
	//lookupSite := bson.D{
	//	{"$lookup", bson.D{
	//		{"from", "site"},
	//		{"localField", "site_key"},
	//		{"foreignField", "_id"},
	//		{"as", "site"},
	//	},
	//	}}
	//lookupSalesChannel := bson.D{
	//	{"$lookup", bson.D{
	//		{"from", "sales_channel"},
	//		{"localField", "sales_channel_key"},
	//		{"foreignField", "_id"},
	//		{"as", "sales_channel"},
	//	},
	//	}}
	//lookupTenant := bson.D{
	//	{"$lookup", bson.D{
	//		{"from", "tenant"},
	//		{"localField", "tenant_key"},
	//		{"foreignField", "_id"},
	//		{"as", "tenant"},
	//	},
	//	}}
	//lookupCustomer := bson.D{
	//	{"$lookup", bson.D{
	//		{"from", "customer"},
	//		{"localField", "customer_key"},
	//		{"foreignField", "_id"},
	//		{"as", "customer"},
	//	},
	//	}}
	//lookupCurrency := bson.D{
	//	{"$lookup", bson.D{
	//		{"from", "currency"},
	//		{"localField", "currency_key"},
	//		{"foreignField", "_id"},
	//		{"as", "currency"},
	//	},
	//	}}
	lookupSalesOrderDetails := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderDetailTable + "s"},
		},
		}}

	//lookupSalesOrderComments := bson.D{
	//	{"$lookup", bson.D{
	//		{"from", setting.SalesOrderCommentTable},
	//		{"localField", "_id"},
	//		{"foreignField", "doc_key"},
	//		{"as", "sales_order_comments"},
	//	},
	//	}}
	//lookupSalesOrderFreights := bson.D{
	//	{"$lookup", bson.D{
	//		{"from", setting.SalesOrderFreightTable},
	//		{"localField", "_id"},
	//		{"foreignField", "doc_key"},
	//		{"as", "sales_order_freights"},
	//	},
	//	}}
	//lookupSalesOrderDiscounts := bson.D{
	//	{"$lookup", bson.D{
	//		{"from", setting.SalesOrderDiscountTable},
	//		{"localField", "_id"},
	//		{"foreignField", "doc_key"},
	//		{"as", "sales_order_discounts"},
	//	},
	//	}}
	//lookupSalesOrderShippingDetails := bson.D{
	//	{"$lookup", bson.D{
	//		{"from", setting.SalesOrderShippingDetailTable},
	//		{"localField", "_id"},
	//		{"foreignField", "doc_key"},
	//		{"as", "sales_order_shipping_details"},
	//	},
	//	}}
	//lookupSalesOrderPayments := bson.D{
	//	{
	//		"$lookup", bson.D{
	//		{"from", setting.SalesOrderPaymentTable},
	//		{"localField", "_id"},
	//		{"foreignField", "doc_key"},
	//		{"as", "sales_order_payments"},
	//	},
	//	}}
	//lookupSalesOrderStatus := bson.D{
	//	{"$lookup", bson.D{
	//		{"from", setting.SalesOrderStatusTable},
	//		{"let", bson.M{"dn": "$doc_num"}},
	//		{"pipeline", bson.A{
	//			bson.M{"$match": bson.M{
	//				"$expr": bson.M{
	//					"$and": bson.A{
	//						bson.M{"$eq": bson.A{"$doc_num", "$$dn"}},
	//						bson.M{"$eq": bson.A{"$doc_line_num", ""}},
	//					},
	//				},
	//			}},
	//		}},
	//		{"as", setting.SalesOrderStatusTable},
	//	},
	//	}}
	//lookupOrderPack := bson.D{
	//	{"$lookup", bson.D{
	//		{"from", setting.OrderPackTable},
	//		{"localField", "doc_num"},
	//		{"foreignField", "order_no"},
	//		{"as", setting.OrderPackTable},
	//	},
	//	}}
	//
	////Unwind array to object
	//unwindOrderPack := bson.D{
	//	{"$unwind", bson.D{
	//		{"path", "$" + setting.OrderPackTable},
	//		{"preserveNullAndEmptyArrays", true}},
	//	},
	//}
	//unwindCountry := bson.D{
	//	{"$unwind", bson.D{
	//		{"path", "$country"},
	//		{"preserveNullAndEmptyArrays", true}},
	//	},
	//}
	//unwindSite := bson.D{
	//	{"$unwind", bson.D{
	//		{"path", "$site"},
	//		{"preserveNullAndEmptyArrays", true}},
	//	},
	//}
	//unwindSalesChannel := bson.D{
	//	{"$unwind", bson.D{
	//		{"path", "$sales_channel"},
	//		{"preserveNullAndEmptyArrays", true}},
	//	},
	//}
	//unwindTenant := bson.D{
	//	{"$unwind", bson.D{
	//		{"path", "$tenant"},
	//		{"preserveNullAndEmptyArrays", true}},
	//	},
	//}
	//unwindCustomer := bson.D{
	//	{"$unwind", bson.D{
	//		{"path", "$customer"},
	//		{"preserveNullAndEmptyArrays", true}},
	//	},
	//}
	//unwindCurrency := bson.D{
	//	{"$unwind", bson.D{
	//		{"path", "$currency"},
	//		{"preserveNullAndEmptyArrays", true}},
	//	},
	//}
	sumSalesOrderDetailQuantity := bson.D{
		{"$addFields", bson.D{
			{"order_quantity", bson.D{
				{"$sum", "$sales_order_details.quantity"},
			}},
		}},
	}
	sumSalesOrderDetailReturnQuantity := bson.D{
		{"$addFields", bson.D{
			{"return_quantity", bson.D{
				{"$sum", "$sales_order_details.return_quantity"},
			}},
		}},
	}
	sumSalesOrderDetailCancelQuantity := bson.D{
		{"$addFields", bson.D{
			{"cancel_quantity", bson.D{
				{"$sum", "$sales_order_details.cancel_quantity"},
			}},
		}},
	}

	projection := bson.D{
		{
			"$project", bson.M{
				"doc_num":          1,
				"doc_date":         1,
				"customer_name":    1,
				"country_id":       1,
				"sales_channel_id": 1,
				"order_status":     1,
				"order_quantity":   1,
				"cancel_quantity":  1,
				"return_quantity":  1,
				"total_after_tax":  1,
				"currency_id":      1,
			},
		},
	}

	pipeline := mongo.Pipeline{
		match,
		lookupSalesOrderDetails,
		matchProductCode,
		sort,
		skip,
		lim,
		//lookupCountry,
		//lookupSalesChannel,
		//lookupSite,
		//lookupTenant,
		//lookupCustomer,
		//lookupCurrency,
		//lookupSalesOrderStatus,
		//lookupSalesOrderComments,
		//lookupSalesOrderFreights,
		//lookupSalesOrderShippingDetails,
		//lookupSalesOrderPayments,
		//lookupSalesOrderDiscounts,
		//lookupOrderPack,
		//unwindOrderPack,
		//unwindCountry,
		//unwindSite,
		//unwindSalesChannel,
		//unwindTenant,
		//unwindCustomer,
		//unwindCurrency,
		sumSalesOrderDetailQuantity,
		sumSalesOrderDetailReturnQuantity,
		sumSalesOrderDetailCancelQuantity,
		projection,
	}

	cursor, err := collection.Aggregate(ctx, pipeline) //findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	//for cursor.Next(ctx) {
	//	err := cursor.Decode(&salesOrder)
	//	if err != nil {
	//		api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Sales Order with Details ERROR - "+err.Error())
	//		return
	//	}
	//	allSalesOrders = append(allSalesOrders, salesOrder)
	//}
	cursor.All(ctx, &allSalesOrders)
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"sales_order": allSalesOrders}
	api_helper.SuccessResponse(response, data, "Get Sales Order with Details successfully")
}

// Export Sales Orders godoc
// @Tags Sales Order
// @Summary Export Sales Order
// @Description Export Sales Order
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Param status path string false "Status of order . Awaiting Payment, Awaiting Fulfillment, Awaiting Shipment, Shipped, Delivered, Cancelled, Return, Closed"
// @Param country_id path string false "Country Id. of Sales Order"
// @Param sales_channel_id path string false "Sales Channel Id of Sales Order."
// @Param keyword path string false "Keyword for Doc Number, Customer name and Email searching."
// @Success 200 {object} []model.SalesOrder
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrder [get]
func ExportSalesOrders(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesOrders")
	var (
		collection       *mongo.Collection
		allSalesOrders   []model.SalesOrderExports
		matchFilter      bson.A
		matchProductCode bson.D
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.SalesOrderTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	status := request.URL.Query().Get("status")
	if status != "" {
		orderStatus := enum.OrderStatus(status)
		if orderStatus, err = orderStatus.IsValid(); err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
			return
		} else {
			if orderStatus != enum.OrderStatusAll {
				matchFilter = append(matchFilter, bson.D{{"order_status", orderStatus.ToCorrectCase()}})
			}
		}
	}

	countryId := request.URL.Query().Get("country_id")
	if countryId != "" {
		matchFilter = append(matchFilter, bson.D{{"country_id", sHelper.RegexStringID(countryId)}})
	}

	salesChannelId := request.URL.Query().Get("sales_channel_id")
	if salesChannelId != "" {
		matchFilter = append(matchFilter, bson.D{{"sales_channel_id", sHelper.RegexStringID(salesChannelId)}})
	}

	email := request.URL.Query().Get("email")
	if email != "" {
		matchFilter = append(matchFilter, bson.D{{"email", sHelper.RegexStringID(email)}})
	}

	ean := request.URL.Query().Get("ean")
	if ean != "" {
		matchProductCode = bson.D{
			{"$match", bson.M{
				setting.SalesOrderDetailTable + "s": bson.M{
					"$elemMatch": bson.M{
						"product_code": sHelper.RegexStringID(ean) ,
					},
				},
			}},
		}
	} else {
		matchProductCode = bson.D{
			{"$match", bson.M{
				"_id": bson.M{"$ne": bsonx.ObjectID(primitive.NilObjectID)},
			}},
		}
	}

	keyword := request.URL.Query().Get("keyword")
	if keyword != "" {
		or := bson.D{{Key: "$or", Value: bson.A{
			bson.D{{"doc_num", sHelper.RegexStringID(keyword)}},
			bson.D{{"customer_name", sHelper.RegexStringID(keyword)}},
			bson.D{{"email", sHelper.RegexStringID(keyword)}},
		}}}
		matchFilter = append(matchFilter, or)
	}

	fromDate, toDate, err := api_helper.GetFromAndToDate(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get From and To Date ERROR - "+err.Error())
		return
	}

	matchFilter = append(matchFilter, bson.M{
		"doc_date": bson.M{
			"$gte": time.Unix(fromDate, 0),
			"$lte": time.Unix(toDate, 0),
		},
	})

	if len(matchFilter) == 0 {
		matchFilter = bson.A{bson.D{{"_id", bson.D{{"$ne", bsonx.ObjectID(primitive.NilObjectID)}}}}}
	}

	//Match with Id matchFilter
	match := bson.D{{
		"$match", bson.D{
			{"$and", matchFilter},
		},
	}}
	//sort, skip and limit
	sort := bson.D{{"$sort", bson.D{{"doc_date", -1}}}}
	//Look up for foreign key
	lookupSalesOrderDetails := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderDetailTable + "s"},
		},
		}}

	detailStr := "$" + setting.SalesOrderDetailTable + "s"
	//Unwind array to object
	unwindSalesOrderDetail := bson.D{
		{"$unwind", bson.D{
			{"path", detailStr},
			{"preserveNullAndEmptyArrays", true}},
		},
	}

	projection := bson.D{
		{"$project", bson.D{
			{"_id", 0},
			{"order_no", "$doc_num"},
			{"order_date", "$doc_date"},
			{"customer", "$customer_name"},
			{"country", "$country_id"},
			{"sales_channel", "$sales_channel_id"},
			{"order_status", "$order_status"},
			{"order_amount", "$total_after_tax"},
			{"ean", detailStr + ".product_code"},
			{"description", detailStr + ".description"},
			{"size", detailStr + ".product_size"},
			{"color", detailStr + ".product_color"},
			{"base_price", detailStr + ".price"},
			{"line_status", detailStr + ".doc_line_status"},
			{"order_quantity", detailStr + ".quantity"},
			{"cancel_quantity", detailStr + ".cancel_quantity"},
			{"return_quantity", detailStr + ".return_quantity"},
			{"discount_amount", detailStr + ".line_discount_amount"},
			{"shipping_fee", detailStr + ".freight_amount"},
			{"sub_total", bson.M{
				"$subtract": bson.A{
					bson.M{
						"$multiply": bson.A{
							detailStr + ".quantity",
							detailStr + ".price",
						}},
					detailStr + ".line_discount_amount",
				},
			}},
		}},
	}

	pipeline := mongo.Pipeline{
		match,
		lookupSalesOrderDetails,
		matchProductCode,
		sort,
		unwindSalesOrderDetail,
		projection,
	}

	cursor, err := collection.Aggregate(ctx, pipeline)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	if err = cursor.All(ctx, &allSalesOrders); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"sales_order": allSalesOrders}
	api_helper.SuccessResponse(response, data, "Export Sales Order successfully")
}

func GetSalesOrdersByDate(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesOrders")
	var (
		collection  *mongo.Collection
		salesOrders []model.SalesOrderResponse
		currentTime = time.Now().Unix()
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.SalesOrderTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	date, err := api_helper.ParseParamStringToIntFromURL(request, "date", currentTime, currentTime)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Date ERROR - "+err.Error())
		return
	}

	beginDate := (date / setting.DaySecond) * setting.DaySecond
	endDate := beginDate + setting.DaySecond - 1

	//Filter with date
	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.M{"created_date": bson.M{"$gte": time.Unix(beginDate, 0), "$lte": time.Unix(endDate, 0)}},
			},
			},
		},
	},
	}
	//Look up
	lookupCountry := bson.D{
		{
			"$lookup", bson.D{
				{"from", "country"},
				{"localField", "country_key"},
				{"foreignField", "_id"},
				{"as", "country"},
			},
		}}
	lookupSite := bson.D{
		{
			"$lookup", bson.D{
				{"from", "site"},
				{"localField", "site_key"},
				{"foreignField", "_id"},
				{"as", "site"},
			},
		}}
	lookupSalesChannel := bson.D{
		{
			"$lookup", bson.D{
				{"from", "sales_channel"},
				{"localField", "sales_channel_key"},
				{"foreignField", "_id"},
				{"as", "sales_channel"},
			},
		}}
	lookupTenant := bson.D{
		{
			"$lookup", bson.D{
				{"from", "tenant"},
				{"localField", "tenant_key"},
				{"foreignField", "_id"},
				{"as", "tenant"},
			},
		}}
	lookupCustomer := bson.D{
		{
			"$lookup", bson.D{
				{"from", "customer"},
				{"localField", "customer_key"},
				{"foreignField", "_id"},
				{"as", "customer"},
			},
		}}
	lookupCurrency := bson.D{
		{
			"$lookup", bson.D{
				{"from", "currency"},
				{"localField", "currency_key"},
				{"foreignField", "_id"},
				{"as", "currency"},
			},
		}}
	lookupSalesOrderDetails := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderDetailTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", "sales_order_details"},
			},
		}}
	lookupSalesOrderComments := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderCommentTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", "sales_order_comments"},
			},
		}}
	lookupSalesOrderFreights := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderFreightTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", "sales_order_freights"},
			},
		}}
	lookupSalesOrderDiscounts := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderDiscountTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", "sales_order_discounts"},
			},
		}}
	lookupSalesOrderShippingDetails := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderShippingDetailTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", "sales_order_shipping_details"},
			},
		}}
	lookupSalesOrderPayments := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderPaymentTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", "sales_order_payments"},
			},
		}}
	lookupSalesOrderPaymentCaptures := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderPaymentCaptureTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", setting.SalesOrderPaymentCaptureTable + "s"},
			},
		}}
	lookupSalesOrderPaymentRefunds := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderPaymentRefundTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", setting.SalesOrderPaymentRefundTable + "s"},
			},
		}}
	lookupSalesOrderPaymentReverses := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.SalesOrderPaymentReverseTable},
				{"localField", "_id"},
				{"foreignField", "doc_key"},
				{"as", setting.SalesOrderPaymentReverseTable + "s"},
			},
		}}
	lookupSalesOrderStatus := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderStatusTable},
			{"let", bson.M{"dn": "$doc_num"}},
			{"pipeline", bson.A{
				bson.M{"$match": bson.M{
					"$expr": bson.M{
						"$and": bson.A{
							bson.M{"$eq": bson.A{"$doc_num", "$$dn"}},
							bson.M{"$eq": bson.A{"$doc_line_num", ""}},
						},
					},
				}},
			}},
			{"as", setting.SalesOrderStatusTable},
		},
		}}
	lookupOrderPack := bson.D{
		{"$lookup", bson.D{
			{"from", setting.OrderPackTable},
			{"localField", "doc_num"},
			{"foreignField", "order_no"},
			{"as", setting.OrderPackTable},
		},
		}}

	//Unwind array to object
	unwindOrderPack := bson.D{
		{"$unwind", bson.D{
			{"path", "$" + setting.OrderPackTable},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindCountry := bson.D{
		{"$unwind", bson.D{
			{"path", "$country"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindSite := bson.D{
		{"$unwind", bson.D{
			{"path", "$site"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindSalesChannel := bson.D{
		{"$unwind", bson.D{
			{"path", "$sales_channel"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindTenant := bson.D{
		{"$unwind", bson.D{
			{"path", "$tenant"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindCustomer := bson.D{
		{"$unwind", bson.D{
			{"path", "$customer"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindCurrency := bson.D{
		{"$unwind", bson.D{
			{"path", "$currency"},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	sumSalesOrderDetailQuantity := bson.D{
		{"$addFields", bson.D{
			{"order_quantity", bson.D{
				{"$sum", "$sales_order_details.quantity"},
			}},
		}},
	}
	sumSalesOrderDetailReturnQuantity := bson.D{
		{"$addFields", bson.D{
			{"return_quantity", bson.D{
				{"$sum", "$sales_order_details.return_quantity"},
			}},
		}},
	}
	sumSalesOrderDetailCancelQuantity := bson.D{
		{"$addFields", bson.D{
			{"cancel_quantity", bson.D{
				{"$sum", "$sales_order_details.cancel_quantity"},
			}},
		}},
	}

	//sort, skip and limit
	//sort := bson.D{{"$sort", bson.D{{"doc_date", -1}}}}
	//skip := bson.D{{"$skip", limit * (page - 1)}}
	//lim := bson.D{{"$limit", limit}}
	pipeline := mongo.Pipeline{
		match,
		lookupCountry,
		lookupSalesChannel,
		lookupSite,
		lookupTenant,
		lookupCustomer,
		lookupCurrency,
		lookupSalesOrderDetails,
		lookupSalesOrderComments,
		lookupSalesOrderFreights,
		lookupSalesOrderShippingDetails,
		lookupSalesOrderPayments,
		lookupSalesOrderPaymentCaptures,
		lookupSalesOrderPaymentRefunds,
		lookupSalesOrderPaymentReverses,
		lookupSalesOrderDiscounts,
		lookupSalesOrderStatus,
		lookupOrderPack,
		unwindOrderPack,
		unwindCountry,
		unwindSite,
		unwindSalesChannel,
		unwindTenant,
		unwindCustomer,
		unwindCurrency,
		sumSalesOrderDetailQuantity,
		sumSalesOrderDetailReturnQuantity,
		sumSalesOrderDetailCancelQuantity,
		//sort,
		//skip,
		//lim,
	}

	cursor, err := collection.Aggregate(ctx, pipeline) //findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var saleOrder model.SalesOrderResponse
		err := cursor.Decode(&saleOrder)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
			return
		}
		salesOrders = append(salesOrders, saleOrder)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"sales_order": salesOrders}
	api_helper.SuccessResponse(response, data, fmt.Sprintf("Get Sales order by '%s' successfully", time.Unix(beginDate, 0)))
}

// Update SalesOrder Order godoc
// @Tags Sales Order
// @Summary Update Sales Order
// @Description Update Sales Order
// @Accept  json
// @Produce  json
// @Param doc_num path string true "doc_num of Sales Order need to be found"
// @Param body body model.SalesOrderUpdateRequest string "Update Sales Order"
// @Success 200 {object} model.SalesOrder
// @Header 200 {string} Token "qwerty"
// @Router /UpdateSalesOrder/{doc_num} [put]
func UpdateSalesOrderStatus(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateSaleOrder")
	var (
		collection          *mongo.Collection
		salesOrder          model.SalesOrder
		salesOrderUpdateReq model.SalesOrderUpdateRequest
		salesOrderReqInt    interface{}
		detail              model.SalesOrderDetail
		detailUpdateReqInt  interface{}
		currentTime         = time.Now()
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.SalesOrderTable)
	detailCollection := database.Database.Collection(setting.SalesOrderDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	docNum, _ := mux.Vars(request)["doc_num"]

	err = json.NewDecoder(request.Body).Decode(&salesOrderUpdateReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Sales Order Status Input ERROR - "+err.Error())
		return
	}

	filter := bson.M{"doc_num": docNum}

	err = collection.FindOne(ctx, filter).Decode(&salesOrder)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Sales Order ERROR - "+err.Error())
		return
	}

	// Check if Sales Order Update Request  is valid
	err = salesOrderUpdateReq.Validation(salesOrder)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Request Input ERROR - "+err.Error())
		return
	}

	// Update status for detail.
	for _, detailUpdateReq := range salesOrderUpdateReq.SalesOrderDetailsUpdateRequest {

		newDocLineStatus := detailUpdateReq.DocLineStatus
		filterChild := bson.M{"doc_num": docNum, "doc_line_num": detailUpdateReq.DocLineNum}
		err = detailCollection.FindOne(ctx, filterChild).Decode(&detail)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Sales Order Detail ERROR - "+err.Error())
			return
		}

		detailUpdateReq.DocLineStatus = ""
		// To Byte
		dByte, _ := json.Marshal(detailUpdateReq)
		// To Interface{} with fields which is not empty
		_ = json.Unmarshal(dByte, &detailUpdateReqInt)

		detail.UpdatedDate = currentTime
		//Update
		update := bson.A{
			bson.M{"$set": detail},             // Set Main model first
			bson.M{"$set": detailUpdateReqInt}, // Set Request Interface second
		}
		//Update Sales Order Detail
		_, err = detailCollection.UpdateOne(ctx, filterChild, update)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Sales Order Detail ERROR - "+err.Error())
			return
		}

		// Check if new status no equal to NEW and empty
		if newDocLineStatus != "" && newDocLineStatus != enum.OrderLineStatusNew {
			if err = service.HandleSalesOrderDetailStatusChange(docNum, detailUpdateReq.DocLineNum, newDocLineStatus); err != nil {
				fmt.Println(fmt.Sprintf("Sales Order Detail '%s' change ERROR - %s", detailUpdateReq.DocLineNum, err.Error()))
			}
		}

	}

	//Update for Package
	//for _, _ = range salesOrderUpdateReq.SalesOrderPackageUpdateRequest {
	//
	//}



	// Handle Update Sales Order Status.
	// Check new status same with old one
	// Check if new status no equal to NEW and empty
	if salesOrderUpdateReq.OrderStatus != "" && salesOrderUpdateReq.OrderStatus != enum.OrderStatusNew {
		//oldStatus := salesOrder.OrderStatus
		//newStatus := salesOrderUpdateReq.OrderStatus
		//
		//if oldStatus == newStatus {
		//	api_helper.SuccessResponse(response, nil, "New status is same with Old status - NO UPDATE")
		//	return
		//}
		//if !IsValidOrderStatusChange(oldStatus, newStatus.ToUpper()) {
		//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, fmt.Sprintf("Order Status '%s' is not allowed to change to '%s'", oldStatus, newStatus))
		//	return
		//}
		salesOrder, err = service.HandleOrderStatusChange(salesOrder.DocNum, salesOrderUpdateReq.OrderStatus, false)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Handle Order Status change ERROR - "+err.Error())
			return
		}
	} else if salesOrderUpdateReq.OrderStatus == enum.OrderStatusNew {
		salesOrderUpdateReq.OrderStatus = ""
	}

	if salesOrderUpdateReq.DocDueDate != 0 {
		salesOrder.DocDueDate = time.Unix(salesOrderUpdateReq.DocDueDate, 0)
	}

	//salesOrderUpdateReq.OrderStatus = ""
	// To Byte
	//salesOrderUpdateReq.SalesOrderPackageUpdateRequest = nil
	salesOrderUpdateReq.SalesOrderDetailsUpdateRequest = nil
	salesOrderUpdateReq.DocDueDate = 0

	salesOrder.UpdatedDate = time.Now()

	//Update Sales Order.
	update := bson.A{
		bson.M{"$set": salesOrder}, // Set Main model first
	}

	if salesOrderUpdateReq.PhoneNo != "" {
		update = append(update, bson.M{
			"$set": bson.M{
				"billing_address.b_phone":  salesOrderUpdateReq.PhoneNo,
				"shipping_address.s_phone": salesOrderUpdateReq.PhoneNo,
			},
		})
		salesOrderUpdateReq.PhoneNo = ""
	}

	if salesOrderUpdateReq.ShippingLabelUrl != "" {
		filterLabel := bson.M{"doc_key": bsonx.ObjectID(salesOrder.ID)}
		updateLabel := bson.M{"$set": bson.M{"shipping_label_url": salesOrderUpdateReq.ShippingLabelUrl}}
		result, err := database.Database.Collection(setting.SalesOrderShippingDetailTable).UpdateMany(ctx, filterLabel, updateLabel)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Shipping Label URL for Shipping Details ERROR - "+err.Error())
			return
		}

		if result.MatchedCount == 0 {
			var shippingDetail model.SalesOrderShippingDetail

			shippingDetail.ID = primitive.NewObjectID()
			shippingDetail.DocKey = salesOrder.ID
			shippingDetail.ShippingLabelUrl = salesOrderUpdateReq.ShippingLabelUrl
			shippingDetail.CreatedDate = currentTime
			shippingDetail.UpdatedDate = currentTime

			_, err := database.Database.Collection(setting.SalesOrderShippingDetailTable).InsertOne(ctx, shippingDetail)
			if err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Shipping Details with label url ERROR - "+err.Error())
				return
			}
		}
	}

	sb, _ := json.Marshal(salesOrderUpdateReq)
	// To Interface{} with fields which is not empty
	_ = json.Unmarshal(sb, &salesOrderReqInt)

	//check sales order request is empty.
	if string(sb) != "{}" {
		update = append(update, bson.M{"$set": salesOrderReqInt}) // Set Request Interface second
	}

	_, err = collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Sales Order ERROR - "+err.Error())
		return
	}

	err = collection.FindOne(ctx, filter).Decode(&salesOrder)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Sales Order ERROR - "+err.Error())
		return
	}

	data := bson.M{"sales_order": salesOrder}
	api_helper.SuccessResponse(response, data, "Update Sales Order successfully")
}

// Get count Sales Order godoc
// @Tags Sales Order
// @Summary Get count Sales Order
// @Description Get count Sales Order
// @Accept  json
// @Produce  json
// @Param status path string false "Status of order . Awaiting Payment, Awaiting Fulfillment, Awaiting Shipment, Shipped, Delivered, Cancelled, Return, Closed"
// @Param country_id path string false "Country Id. of Sales Order"
// @Param sales_channel_id path string false "Sales Channel Id of Sales Order."
// @Param keyword path string false "Keyword for Doc Number, Customer name and Email searching."
// @Success 200 {object} model.Count
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrder/count [get]
func GetCountSalesOrder(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesOrders")
	var (
		collection       *mongo.Collection
		matchFilter      bson.A
		matchProductCode bson.D
		countBson        = bson.M{"count": 0}
		//brand          model.Brand
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.SalesOrderTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	status := request.URL.Query().Get("status")
	if status != "" {
		orderStatus := enum.OrderStatus(status)
		if orderStatus, err = orderStatus.IsValid(); err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
			return
		} else {
			if orderStatus != enum.OrderStatusAll {
				matchFilter = append(matchFilter, bson.D{{"order_status", orderStatus.ToCorrectCase()}})
			}
		}
	}

	countryId := request.URL.Query().Get("country_id")
	if countryId != "" {
		//filter := bson.M{"country_id": countryId}
		//
		//collection = database.Database.Collection(setting.CountryTable)
		//err = collection.FindOne(ctx, filter).Decode(&country)
		//if err != nil {
		//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Country object ERROR - "+err.Error())
		//	return
		//}
		matchFilter = append(matchFilter, bson.D{{"country_id", countryId}})
	}

	salesChannelId := request.URL.Query().Get("sales_channel_id")
	if salesChannelId != "" {
		//filter := bson.M{"sales_channel_id": salesChannelId}
		//
		//collection = database.Database.Collection(setting.SalesChannelTable)
		//err = collection.FindOne(ctx, filter).Decode(&salesChannel)
		//if err != nil {
		//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Sales Channel object ERROR - "+err.Error())
		//	return
		//}
		matchFilter = append(matchFilter, bson.D{{"sales_channel_id", salesChannelId}})
	}

	keyword := request.URL.Query().Get("keyword")
	if keyword != "" {
		or := bson.D{{Key: "$or", Value: bson.A{
			bson.D{{"doc_num", primitive.Regex{Pattern: keyword, Options: "i"}}},
			bson.D{{"customer_name", primitive.Regex{Pattern: keyword, Options: "i"}}},
			bson.D{{"email", primitive.Regex{Pattern: keyword, Options: "i"}}},
		}}}
		matchFilter = append(matchFilter, or)
	}

	email := request.URL.Query().Get("email")
	if email != "" {
		//filter := bson.M{"sales_channel_id": salesChannelId}
		//
		//collection = database.Database.Collection(setting.SalesChannelTable)
		//err = collection.FindOne(ctx, filter).Decode(&salesChannel)
		//if err != nil {
		//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Sales Channel object ERROR - "+err.Error())
		//	return
		//}
		matchFilter = append(matchFilter, bson.D{{"email", email}})
	}

	ean := request.URL.Query().Get("ean")
	if ean != "" {
		matchProductCode = bson.D{
			{"$match", bson.M{
				setting.SalesOrderDetailTable + "s": bson.M{
					"$elemMatch": bson.M{
						"product_code": ean,
					},
				},
			}},
		}
	} else {
		matchProductCode = bson.D{
			{"$match", bson.M{
				"_id": bson.M{"$ne": bsonx.ObjectID(primitive.NilObjectID)},
			}},
		}
	}

	fromDate, toDate, err := api_helper.GetFromAndToDate(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get From and To Date ERROR - "+err.Error())
		return
	}

	matchFilter = append(matchFilter, bson.M{
		"doc_date": bson.M{
			"$gte": time.Unix(fromDate, 0),
			"$lte": time.Unix(toDate, 0),
		},
	})
	//brandId := request.URL.Query().Get("brand_id")
	//if brandId != "" {
	//	collection = database.Database.Collection(setting.SalesChannelTable)
	//	err = collection.FindOne(ctx, bson.M{"brand_id": brandId}).Decode(&brand)
	//	if err != nil {
	//		api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Sales Channel object ERROR - "+err.Error())
	//		return
	//	}
	//	matchFilter = append(matchFilter, bson.D{{"brand_key",brand.ID}})
	//}

	if len(matchFilter) == 0 {
		matchFilter = bson.A{bson.D{{"_id", bson.D{{"$ne", bsonx.ObjectID(primitive.NilObjectID)}}}}}
	}

	//Match with Id matchFilter
	match := bson.D{{
		"$match", bson.D{
			{"$and", matchFilter},
		},
	}}

	lookupSalesOrderDetails := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderDetailTable + "s"},
		},
		}}

	count := bson.D{
		{"$count", "count"},
	}

	pipeline := mongo.Pipeline{
		match,
		lookupSalesOrderDetails,
		matchProductCode,
		count,
	}

	cursor, err := collection.Aggregate(ctx, pipeline) //findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	if cursor.Next(ctx) {
		if err := cursor.Decode(&countBson); err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
			return
		}
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	api_helper.SuccessResponse(response, countBson, "Get count Sales Order successfully")
}

//---------------------- Unused ----------------------
func GetCountSalesOrderOld(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesOrders")

	var (
		collection  *mongo.Collection
		matchFilter bson.M
		andArray    = bson.A{
			bson.M{"_id": bson.M{"$ne": bsonx.ObjectID(primitive.NilObjectID)}},
		}
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.SalesOrderTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	andArray = append(andArray, bson.D{
		{"_id", bson.M{"$ne": bsonx.ObjectID(primitive.NilObjectID)}},
	})

	status := request.URL.Query().Get("status")
	keyword := request.URL.Query().Get("keyword")
	if keyword != "" {
		or := bson.D{{Key: "$or", Value: bson.A{
			bson.D{{"doc_num", primitive.Regex{Pattern: keyword, Options: "i"}}},
			bson.D{{"customer_name", primitive.Regex{Pattern: keyword, Options: "i"}}},
			bson.D{{"email", primitive.Regex{Pattern: keyword, Options: "i"}}},
		}}}
		andArray = append(andArray, or)
	}
	if status != "" {
		orderStatus := enum.OrderStatus(status)
		if orderStatus, err = orderStatus.IsValid(); err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
			return
		} else {
			if orderStatus != enum.OrderStatusAll {
				andArray = append(andArray, bson.D{{"order_status", orderStatus.ToCorrectCase()}})
			}
		}
	}

	countryId := request.URL.Query().Get("country_id")
	if countryId != "" {
		//filter := bson.M{"country_id": countryId}
		//
		//collection = database.Database.Collection(setting.CountryTable)
		//err = collection.FindOne(ctx, filter).Decode(&country)
		//if err != nil {
		//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Country object ERROR - "+err.Error())
		//	return
		//}
		andArray = append(andArray, bson.D{{"country_id", countryId}})
	}

	salesChannelId := request.URL.Query().Get("sales_channel_id")
	if salesChannelId != "" {
		//filter := bson.M{"sales_channel_id": salesChannelId}
		//
		//collection = database.Database.Collection(setting.SalesChannelTable)
		//err = collection.FindOne(ctx, filter).Decode(&salesChannel)
		//if err != nil {
		//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Sales Channel object ERROR - "+err.Error())
		//	return
		//}
		andArray = append(andArray, bson.D{{"sales_channel_id", salesChannelId}})
	}

	fromDate, toDate, err := api_helper.GetFromAndToDate(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get From and To Date ERROR - "+err.Error())
		return
	}

	andArray = append(andArray, bson.M{
		"doc_date": bson.M{
			"$gte": time.Unix(fromDate, 0),
			"$lte": time.Unix(toDate, 0),
		},
	})

	//brandId := request.URL.Query().Get("brand_id")
	//if brandId != "" {
	//	collection = database.Database.Collection(setting.SalesChannelTable)
	//	err = collection.FindOne(ctx, bson.M{"brand_id": brandId}).Decode(&brand)
	//	if err != nil {
	//		api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Sales Channel object ERROR - "+err.Error())
	//		return
	//	}
	//	matchFilter = append(matchFilter, bson.D{{"brand_key",brand.ID}})
	//}

	matchFilter = bson.M{
		"$and": andArray,
	}

	count, err := collection.CountDocuments(ctx, matchFilter)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Count document ERROR - "+err.Error())
		return
	}

	data := bson.M{"count": count}
	api_helper.SuccessResponse(response, data, "Get count Sales Order successfully")
}

func GetSalesOrdersTestView(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesOrders")
	var (
		collection     *mongo.Collection
		salesOrder     model.SalesOrderResponse
		allSalesOrders []model.SalesOrderResponse
		filter         bson.D
		//brand          model.Brand
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection("sales_order_with_details")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	countryId := request.URL.Query().Get("country_id")
	if countryId != "" {
		filter = append(filter, bson.E{Key: "country.country_id", Value: countryId})
	}
	salesChannelId := request.URL.Query().Get("sales_channel_id")
	if salesChannelId != "" {
		filter = append(filter, bson.E{Key: "sales_channel.sales_channel_id", Value: salesChannelId})
	}
	//brandId := request.URL.Query().Get("brand_id")
	//if brandId != "" {
	//	collection = database.Database.Collection(setting.SalesChannelTable)
	//	err = collection.FindOne(ctx, bson.M{"brand_id": brandId}).Decode(&brand)
	//	if err != nil {
	//		api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Sales Channel object ERROR - "+err.Error())
	//		return
	//	}
	//	matchFilter = append(matchFilter, bson.D{{"brand_key",brand.ID}})
	//}

	keyword := request.URL.Query().Get("keyword")
	if keyword != "" {
		filter = append(filter, bson.E{Key: "$text", Value: bson.M{"$search": keyword}})
	}

	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))

	models := []mongo.IndexModel{
		{
			Keys: bsonx.Doc{{Key: "country_id", Value: bsonx.String("text")}},
		},
	}

	ind, err := database.Database.Collection(setting.CountryTable).Indexes().CreateMany(ctx, models)
	if err != nil {
		fmt.Println("Indexes().CreateOne() ERROR:", err)
		os.Exit(1) // exit in case of error
	} else {
		// API call returns string of the index name
		fmt.Println("CreateOne() index:", ind)
		fmt.Println("CreateOne() type:", reflect.TypeOf(ind), "\n")
	}

	cursor, err := collection.Find(ctx, filter, findOptions) //findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		err := cursor.Decode(&salesOrder)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Sales Order with Details ERROR - "+err.Error())
			return
		}
		allSalesOrders = append(allSalesOrders, salesOrder)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"sales_order": allSalesOrders}
	api_helper.SuccessResponse(response, data, "Get Sales Order with Details successfully")
}

func GetSalesOrdersOld(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesOrders")
	var (
		collection  *mongo.Collection
		salesOrders []model.SalesOrder
		findOptions = options.Find()
		filter      = bson.M{}
	)
	collection = database.Database.Collection(setting.SalesOrderTable)
	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	//cursor, err := collection.Find(ctx, filter, findOptions )//findOptions)

	cursor, err := collection.Find(ctx, filter, findOptions) //findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var saleOrder model.SalesOrder
		cursor.Decode(&saleOrder)
		salesOrders = append(salesOrders, saleOrder)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesOrders)
}

func UpdateSalesOrder(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateSaleOrder")
	var (
		collection        *mongo.Collection
		salesOrderRequest model.SalesOrderRequest
		salesOrder        model.SalesOrder
	)
	collection = database.Database.Collection(setting.SalesOrderTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&salesOrderRequest)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	copier.Copy(&salesOrder, &salesOrderRequest)
	currentTime := time.Now()
	salesOrder.UpdatedDate = currentTime

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": salesOrder}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	var uiSalesOrderDetail []interface{}
	var uiSalesOrderComment []interface{}
	var uiSalesOrderDiscount []interface{}
	var uiSalesOrderFreight []interface{}
	var uiSalesOrderShippingDetail []interface{}
	var uiSalesOrderPayment []interface{}

	// Sales Order Detail
	if len(salesOrderRequest.SalesOrderDetailRequest) > 0 {
		collection = database.Database.Collection(setting.SalesOrderDetailTable)

		for _, detail := range salesOrderRequest.SalesOrderDetailRequest {
			var (
				salesOrderDetail model.SalesOrderDetail
			)
			copier.Copy(&salesOrderDetail, &detail)
			salesOrderDetail.UpdatedDate = currentTime

			if salesOrderDetail.ID != primitive.NilObjectID {
				filter = bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(salesOrderDetail.ID)}}
				update = bson.M{"$set": salesOrderDetail}
				result, err = collection.UpdateOne(ctx, filter, update)
				if err != nil {
					response.WriteHeader(http.StatusInternalServerError)
					response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
					return
				}
			} else {
				uiSalesOrderDetail = append(uiSalesOrderDetail, salesOrderDetail)
			}
		}

		// Insert New
		if len(uiSalesOrderDetail) > 0 {
			collection = database.Database.Collection(setting.SalesOrderDetailTable)
			// Sales Order Detail
			_, err = collection.InsertMany(context.TODO(), uiSalesOrderDetail)
			if err != nil {
				response.WriteHeader(http.StatusInternalServerError)
				response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
				return
			}
		}
	}

	// Sales Order Comment
	if len(salesOrderRequest.SalesOrderCommentRequest) > 0 {
		collection = database.Database.Collection(setting.SalesOrderCommentTable)

		for _, comment := range salesOrderRequest.SalesOrderCommentRequest {
			var (
				salesOrderComment model.SalesOrderComment
			)
			copier.Copy(&salesOrderComment, &comment)
			salesOrderComment.UpdatedDate = currentTime

			if salesOrderComment.ID != primitive.NilObjectID {
				filter = bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(salesOrderComment.ID)}}
				update = bson.M{"$set": salesOrderComment}
				result, err = collection.UpdateOne(ctx, filter, update)
				if err != nil {
					response.WriteHeader(http.StatusInternalServerError)
					response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
					return
				}
			} else {
				uiSalesOrderComment = append(uiSalesOrderComment, salesOrderComment)
			}
		}

		// Insert New
		if len(uiSalesOrderComment) > 0 {
			collection = database.Database.Collection(setting.SalesOrderCommentTable)
			_, err = collection.InsertMany(context.TODO(), uiSalesOrderComment)
			if err != nil {
				response.WriteHeader(http.StatusInternalServerError)
				response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
				return
			}
		}
	}

	// Sales Order Freight
	if len(salesOrderRequest.SalesOrderFreightRequest) > 0 {
		collection = database.Database.Collection(setting.SalesOrderFreightTable)

		for _, freight := range salesOrderRequest.SalesOrderFreightRequest {
			var (
				salesOrderFreight model.SalesOrderFreight
			)
			copier.Copy(&salesOrderFreight, &freight)
			salesOrderFreight.UpdatedDate = currentTime

			if salesOrderFreight.ID != primitive.NilObjectID {
				filter = bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(salesOrderFreight.ID)}}
				update = bson.M{"$set": salesOrderFreight}
				result, err = collection.UpdateOne(ctx, filter, update)
				if err != nil {
					response.WriteHeader(http.StatusInternalServerError)
					response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
					return
				}
			} else {
				uiSalesOrderFreight = append(uiSalesOrderFreight, salesOrderFreight)
			}
		}

		// Insert New
		if len(uiSalesOrderFreight) > 0 {
			collection = database.Database.Collection(setting.SalesOrderFreightTable)
			_, err = collection.InsertMany(context.TODO(), uiSalesOrderFreight)
			if err != nil {
				response.WriteHeader(http.StatusInternalServerError)
				response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
				return
			}
		}
	}

	// Sales Order Discount
	if len(salesOrderRequest.SalesOrderDiscountRequest) > 0 {
		collection = database.Database.Collection(setting.SalesOrderDiscountTable)

		for _, discount := range salesOrderRequest.SalesOrderDiscountRequest {
			var (
				salesOrderDiscount model.SalesOrderDiscount
			)
			copier.Copy(&salesOrderDiscount, &discount)
			salesOrderDiscount.UpdatedDate = currentTime

			if salesOrderDiscount.ID != primitive.NilObjectID {
				filter = bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(salesOrderDiscount.ID)}}
				update = bson.M{"$set": salesOrderDiscount}
				result, err = collection.UpdateOne(ctx, filter, update)
				if err != nil {
					response.WriteHeader(http.StatusInternalServerError)
					response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
					return
				}
			} else {
				uiSalesOrderDiscount = append(uiSalesOrderDiscount, salesOrderDiscount)
			}
		}

		// Insert New
		if len(uiSalesOrderDiscount) > 0 {
			collection = database.Database.Collection(setting.SalesOrderDiscountTable)
			_, err = collection.InsertMany(context.TODO(), uiSalesOrderDiscount)
			if err != nil {
				response.WriteHeader(http.StatusInternalServerError)
				response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
				return
			}
		}
	}

	// Sales Order Shipping Detail
	if len(salesOrderRequest.SalesOrderShippingDetailRequest) > 0 {
		collection = database.Database.Collection(setting.SalesOrderShippingDetailTable)

		for _, shippingDetail := range salesOrderRequest.SalesOrderShippingDetailRequest {
			var (
				salesOrderShippingDetail model.SalesOrderDiscount
			)
			copier.Copy(&salesOrderShippingDetail, &shippingDetail)
			salesOrderShippingDetail.UpdatedDate = currentTime

			if salesOrderShippingDetail.ID != primitive.NilObjectID {
				filter = bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(salesOrderShippingDetail.ID)}}
				update = bson.M{"$set": salesOrderShippingDetail}
				result, err = collection.UpdateOne(ctx, filter, update)
				if err != nil {
					response.WriteHeader(http.StatusInternalServerError)
					response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
					return
				}
			} else {
				uiSalesOrderShippingDetail = append(uiSalesOrderShippingDetail, salesOrderShippingDetail)
			}
		}

		// Insert New
		if len(uiSalesOrderShippingDetail) > 0 {
			collection = database.Database.Collection(setting.SalesOrderShippingDetailTable)
			_, err = collection.InsertMany(context.TODO(), uiSalesOrderShippingDetail)
			if err != nil {
				response.WriteHeader(http.StatusInternalServerError)
				response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
				return
			}
		}
	}

	// Sales Order Payment
	if len(salesOrderRequest.SalesOrderPaymentRequest) > 0 {
		collection = database.Database.Collection(setting.SalesOrderPaymentTable)

		for _, payment := range salesOrderRequest.SalesOrderPaymentRequest {
			var (
				salesOrderPayment model.SalesOrderPayment
			)
			copier.Copy(&salesOrderPayment, &payment)
			salesOrderPayment.UpdatedDate = currentTime

			if salesOrderPayment.ID != primitive.NilObjectID {
				filter = bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(salesOrderPayment.ID)}}
				update = bson.M{"$set": salesOrderPayment}
				result, err = collection.UpdateOne(ctx, filter, update)
				if err != nil {
					response.WriteHeader(http.StatusInternalServerError)
					response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
					return
				}
			} else {
				uiSalesOrderPayment = append(uiSalesOrderPayment, salesOrderPayment)
			}
		}

		// Insert New
		if len(uiSalesOrderPayment) > 0 {
			collection = database.Database.Collection(setting.SalesOrderPaymentTable)
			_, err = collection.InsertMany(context.TODO(), uiSalesOrderPayment)
			if err != nil {
				response.WriteHeader(http.StatusInternalServerError)
				response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
				return
			}
		}
	}

	json.NewEncoder(response).Encode(result)
}

func DeleteSoftSalesOrder(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftSalesOrder")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.SalesOrderTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}

func Test(response http.ResponseWriter, request *http.Request) {
	const shortForm = "2006-Jan-02"
	t, err := time.Parse(shortForm, "2013-Feb-03")
	if err != nil {
		fmt.Println("err: ", err.Error())
	}
	fmt.Println(t)

	var t1 int64
	t1 = time.Now().Unix()
	date := time.Unix(t1, 0)

	dateAdd := date.AddDate(0, 0, 1)
	fmt.Println("date: ", dateAdd)

	dateFormat := date.Format("2006-Jan-02")
	fmt.Println("date: ", dateFormat)

	dateParse, err := time.Parse(shortForm, dateFormat)
	if err != nil {
		fmt.Println("err: ", err.Error())
	}
	fmt.Println("dateParse : ", dateParse)

}

// CreateManySalesOrder func
func CreateManySalesOrder(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManySalesOrder")
	var (
		collection                 *mongo.Collection
		salesOrdersRequest         []model.SalesOrderRequest
		uiSalesOrder               []interface{}
		uiSalesOrderDetail         []interface{}
		uiSalesOrderComment        []interface{}
		uiSalesOrderDiscount       []interface{}
		uiSalesOrderFreight        []interface{}
		uiSalesOrderShippingDetail []interface{}
		uiSalesOrderPayment        []interface{}
	)

	collection = database.Database.Collection(setting.SalesOrderTable)
	response.Header().Set("content-type", "application/json")

	err = json.NewDecoder(request.Body).Decode(&salesOrdersRequest)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode input ERROR - "+err.Error())
		return
	}
	currentTime := time.Now()
	for _, salesOrderRequest := range salesOrdersRequest {
		var (
			salesOrder model.SalesOrder
		)
		err = copier.Copy(&salesOrder, &salesOrderRequest)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy input ERROR - "+err.Error())
			return
		}
		salesOrder.ID = primitive.NewObjectID()
		//salesOrder.DocKey = salesOrder.ID
		salesOrder.CreatedDate = currentTime
		salesOrder.UpdatedDate = currentTime
		uiSalesOrder = append(uiSalesOrder, salesOrder)

		// Sales Order Detail
		for _, detail := range salesOrderRequest.SalesOrderDetailRequest {
			var (
				salesOrderDetail model.SalesOrderDetail
			)
			err = copier.Copy(&salesOrderDetail, &detail)
			if err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy Sales Order Detail ERROR - "+err.Error())
				return
			}
			salesOrderDetail.DocKey = salesOrder.ID
			salesOrderDetail.ID = primitive.NewObjectID()
			//salesOrderDetail.DocLineKey = salesOrderDetail.ID
			salesOrderDetail.CreatedDate = currentTime
			salesOrderDetail.UpdatedDate = currentTime
			uiSalesOrderDetail = append(uiSalesOrderDetail, salesOrderDetail)
		}

		// Sales Order Comment
		for _, comment := range salesOrderRequest.SalesOrderCommentRequest {
			var (
				salesOrderComment model.SalesOrderComment
			)
			err = copier.Copy(&salesOrderComment, &comment)
			if err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy Sales Order Comment ERROR - "+err.Error())
				return
			}
			salesOrderComment.DocKey = salesOrder.ID
			salesOrderComment.ID = primitive.NewObjectID()
			//salesOrderComment.CommentKey = salesOrderComment.ID
			salesOrderComment.CreatedDate = currentTime
			salesOrderComment.UpdatedDate = currentTime
			uiSalesOrderComment = append(uiSalesOrderComment, salesOrderComment)
		}

		// Sales Order Freight
		for _, freight := range salesOrderRequest.SalesOrderFreightRequest {
			var (
				salesOrderFreight model.SalesOrderFreight
			)
			err = copier.Copy(&salesOrderFreight, &freight)
			if err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy Sales Order Freight ERROR - "+err.Error())
				return
			}
			salesOrderFreight.DocKey = salesOrder.ID
			salesOrderFreight.ID = primitive.NewObjectID()
			//salesOrderFreight.DocFreightKey = salesOrderFreight.ID
			salesOrderFreight.CreatedDate = currentTime
			salesOrderFreight.UpdatedDate = currentTime
			uiSalesOrderFreight = append(uiSalesOrderFreight, salesOrderFreight)
		}

		// Sales Order Discount
		for _, discount := range salesOrderRequest.SalesOrderDiscountRequest {
			var (
				salesOrderDiscount model.SalesOrderDiscount
			)
			err = copier.Copy(&salesOrderDiscount, &discount)
			if err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy Sales Order Discount ERROR - "+err.Error())
				return
			}
			salesOrderDiscount.DocKey = salesOrder.ID
			salesOrderDiscount.ID = primitive.NewObjectID()
			//salesOrderDiscount.DocDiscountKey = salesOrderDiscount.ID
			salesOrderDiscount.CreatedDate = currentTime
			salesOrderDiscount.UpdatedDate = currentTime
			uiSalesOrderDiscount = append(uiSalesOrderDiscount, salesOrderDiscount)
		}

		// Sales Order Shipping Detail
		for _, shippingDetail := range salesOrderRequest.SalesOrderShippingDetailRequest {
			var (
				salesOrderShippingDetail model.SalesOrderShippingDetail
			)
			err = copier.Copy(&salesOrderShippingDetail, &shippingDetail)
			if err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy Sales Order Shipping Detail ERROR - "+err.Error())
				return
			}
			salesOrderShippingDetail.DocKey = salesOrder.ID
			salesOrderShippingDetail.ID = primitive.NewObjectID()
			//salesOrderShippingDetail.DocShipKey = salesOrderShippingDetail.ID
			salesOrderShippingDetail.CreatedDate = currentTime
			salesOrderShippingDetail.UpdatedDate = currentTime
			uiSalesOrderShippingDetail = append(uiSalesOrderShippingDetail, salesOrderShippingDetail)
		}

		// Sales Order Payment
		for _, payment := range salesOrderRequest.SalesOrderPaymentRequest {
			var (
				salesOrderPayment model.SalesOrderPayment
			)
			err = copier.Copy(&salesOrderPayment, &payment)
			if err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy Sales Order Payment ERROR - "+err.Error())
				return
			}
			salesOrderPayment.DocKey = salesOrder.ID
			salesOrderPayment.ID = primitive.NewObjectID()
			salesOrderPayment.PaymentTypeKey = salesOrderPayment.ID
			salesOrderPayment.CreatedDate = currentTime
			salesOrderPayment.UpdatedDate = currentTime
			uiSalesOrderPayment = append(uiSalesOrderPayment, salesOrderPayment)
		}
	}

	// Sales Order
	_, err := collection.InsertMany(context.TODO(), uiSalesOrder)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Sales Order ERROR - "+err.Error())
		return
	}

	if len(uiSalesOrderDetail) > 0 {
		collection = database.Database.Collection(setting.SalesOrderDetailTable)
		// Sales Order Detail
		_, err = collection.InsertMany(context.TODO(), uiSalesOrderDetail)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Sales Order Detail ERROR - "+err.Error())
			return
		}
	} else {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Please enter at least one line for order detail.")
		return
	}
	if len(uiSalesOrderComment) > 0 {
		collection = database.Database.Collection(setting.SalesOrderCommentTable)
		// Sales Order Comment
		_, err = collection.InsertMany(context.TODO(), uiSalesOrderComment)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Sales Order Comment ERROR - "+err.Error())
			return
		}
	}
	if len(uiSalesOrderFreight) > 0 {
		collection = database.Database.Collection(setting.SalesOrderFreightTable)
		// Sales Order Freight
		_, err = collection.InsertMany(context.TODO(), uiSalesOrderFreight)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Sales Order Freight ERROR - "+err.Error())
			return
		}
	}
	if len(uiSalesOrderDiscount) > 0 {
		collection = database.Database.Collection(setting.SalesOrderDiscountTable)
		// Sales Order Discount
		_, err = collection.InsertMany(context.TODO(), uiSalesOrderDiscount)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Sales Order Discount ERROR - "+err.Error())
			return
		}
	}
	if len(uiSalesOrderShippingDetail) > 0 {
		collection = database.Database.Collection(setting.SalesOrderShippingDetailTable)
		// Sales Order Shipping Detail
		_, err = collection.InsertMany(context.TODO(), uiSalesOrderShippingDetail)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Sales Order Shipping Detail ERROR - "+err.Error())
			return
		}
	}
	if len(uiSalesOrderPayment) > 0 {
		collection = database.Database.Collection(setting.SalesOrderPaymentTable)
		// Sales Order Payment
		_, err = collection.InsertMany(context.TODO(), uiSalesOrderPayment)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Sales Order Payment ERROR - "+err.Error())
			return
		}
	}

	//json.NewEncoder(response).Encode(salesOrdersRequest)
	data := bson.M{
		"sales_order":                 uiSalesOrder,
		"sales_order_detail":          uiSalesOrderDetail,
		"sales_order_comment":         uiSalesOrderComment,
		"sales_order_freight":         uiSalesOrderFreight,
		"sales_order_discount":        uiSalesOrderDiscount,
		"sales_order_shipping_detail": uiSalesOrderShippingDetail,
		"sales_order_payment":         uiSalesOrderPayment,
	}
	api_helper.SuccessResponse(response, data, "Create Sales Order successfully")
}
