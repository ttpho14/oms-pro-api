package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../database"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

// Add Reason godoc
// @Tags Reason
// @Summary Add Reason
// @Description Add Reason
// @Accept  json
// @Produce  json
// @Param body body model.ReasonRequest true "Add Reason"
// @Success 200 {object} model.Reason
// @Header 200 {string} Token "qwerty"
// @Router /Reason [POST]
func CreateReason(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateReason")
	var (
		collection *mongo.Collection
		reason     model.Reason
		reasonReq  model.ReasonRequest
	)

	response.Header().Set("content-type", "application/json")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	collection = database.Database.Collection(setting.ReasonTable)
	err = json.NewDecoder(request.Body).Decode(&reasonReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&reason, &reasonReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set Date = time.Now()
	currentTime := time.Now()
	reason.ID = primitive.NewObjectID()
	reason.ObjectType = setting.ReasonTable
	reason.CreatedDate = currentTime
	reason.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, reason)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	data := bson.M{"reason": reason}
	api_helper.SuccessResponse(response, data, "Create Reason successfully")
}

// Get Reason By Id godoc
// @Tags Reason
// @Summary Get Reason by Id
// @Description Get Reason by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Reason need to be found"
// @Success 200 {object} model.Reason
// @Header 200 {string} Token "qwerty"
// @Router /Reason/{id} [get]
func GetReasonById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetReasonById")
	var (
		collection *mongo.Collection
		reason     model.Reason
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReasonTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err := collection.FindOne(ctx, filter).Decode(&reason)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	data := bson.M{"reason": reason}
	api_helper.SuccessResponse(response, data, "Get Reason Successfully")
}

// Get number of Reasons godoc
// @Tags Reason
// @Summary Get number of Reasons sort by created_date field descending
// @Description Get number of Reasons depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.Reason
// @Header 200 {string} Token "qwerty"
// @Router /Reason [get]
func GetReasons(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetReasons")

	var (
		allReason []model.Reason
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.ReasonTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{"is_deleted": bson.M{"$eq": false}}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))

	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var reason model.Reason
		_ = cursor.Decode(&reason)
		allReason = append(allReason, reason)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"reason": allReason}
	api_helper.SuccessResponse(response, data, "Get Reason Successfully")
}

// Update Reason godoc
// @Tags Reason
// @Summary Update Reason
// @Description Update Reason
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Reason need to be found"
// @Param body body model.ReasonRequest true "Update Reason"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Reason/{id} [put]
func UpdateReason(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var (
		collection *mongo.Collection
		reason     model.Reason
		reasonReq  model.ReasonRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReasonTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err = collection.FindOne(ctx, filter).Decode(&reason)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = json.NewDecoder(request.Body).Decode(&reasonReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&reason, &reasonReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set lai thoi gian luc update
	reason.UpdatedDate = time.Now()

	update := bson.M{"$set": reason}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"reason": reason}
	api_helper.SuccessResponse(response, data, "Update Reason Successfully")
}

// Soft Delete Reason godoc
// @Tags Reason
// @Summary Soft Delete Reason
// @Description Soft Delete Reason
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Reason need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Reason/{id} [delete]
func DeleteSoftReason(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReasonTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	api_helper.SuccessResponse(response, id, "Delete Reason successfully")
}

//-------------------- Unused -----------------------
func CreateManyReason(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyReason")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.ReasonTable)
	response.Header().Set("content-type", "application/json")
	var manyReason []model.Reason
	err = json.NewDecoder(request.Body).Decode(&manyReason)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, reason := range manyReason {
		reason.ID = primitive.NewObjectID()
		reason.CreatedDate = time.Now()
		reason.UpdatedDate = time.Now()
		ui = append(ui, reason)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

//DeleteHardReason
/*
func DeleteHardReason(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.ReasonTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
