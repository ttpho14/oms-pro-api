package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"../database"
	"../model"
	"../model/enum"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// Add Product godoc
// @Tags Product
// @Summary Add Product
// @Description Add Product
// @Accept  json
// @Produce  json
// @Param body body model.ProductRequest true "Add Product"
// @Success 200 {object} model.Product
// @Header 200 {string} Token "qwerty"
// @Router /Product [POST]
func CreateProduct(response http.ResponseWriter, request *http.Request) {
	//handle error
	funcName := "CreateProduct"

	defer api_helper.RecoverError(response, funcName)
	var (
		collection *mongo.Collection
		addProduct model.ProductRequest
		product    model.Product
		data       bson.M
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ProductTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&addProduct)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	// Get Tenant Key
	// 1. Get from Body Json
	// 2. Get from User => Tenant Key
	// 2. Get from Token.GetClientId() => Application => TenantKey
	//if api_helper.IsFieldExistsInModel(addProduct, "TenantKey") {
	//	tenantKey = addProduct.TenantKey

	//if primitive.ObjectID.IsZero(addProduct.TenantKey) {
	//	key, err := api_helper.GetApplicationFromToken(setting.Token.GetClientID())
	//	if err != nil {
	//		logger.Logger.Errorf(" `%s` ERROR : `%s`", funcName, err.Error())
	//		api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Get Tenant Key From Token ERROR - "+err.Error())
	//		return
	//	}
	//	addProduct.TenantKey = key
	//}

	_ = options.Update().SetUpsert(true)
	filter := bson.D{
		{"product_id", addProduct.ProductId},
		//{"tenant_key", addProduct.TenantKey},
	}

	collection = database.Database.Collection(setting.ProductTable)
	_ = collection.FindOne(ctx, filter).Decode(&product)

	//Set Date = time.Now()
	err = copier.Copy(&product, &addProduct)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy Request model to Main model ERROR - "+err.Error())
		return
	}
	currentTime := time.Now()

	if primitive.ObjectID.IsZero(product.ID) {
		product.ID = primitive.NewObjectID()
		product.CreatedDate = currentTime
		product.CreatedBy = primitive.NewObjectID()
		product.CreatedApplicationKey = primitive.NewObjectID()
		product.ObjectType = setting.ProductTable
	}

	product.UpdatedDate = currentTime
	product.UpdatedBy = primitive.NewObjectIDFromTimestamp(currentTime)
	product.UpdatedApplicationKey = primitive.NewObjectIDFromTimestamp(currentTime)

	//set Upsert = true, update ==> set product
	upsert := options.Update().SetUpsert(true)
	update := bson.D{{"$set", product}}

	//Update, if match with filter ==> update
	//If don't, it will insert 1 record to DB.
	result, err := collection.UpdateOne(ctx, filter, update, upsert)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert/Update ERROR - "+err.Error())
		return
	}

	// Upserted Count == 0 =======>>>> Update
	if result.UpsertedCount == 0 {
		data = bson.M{"product": product}
		api_helper.SuccessResponse(response, data, "Update Product successfully")
		return
	} else { // Upserted count > 0 =======>>>> Insert
		//Create Inventory Status
		_, _, err := CreateInventoryStatusByProduct(product)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Inserted Product but Inventory Status / Inventory Log occurs error : "+err.Error())
			return
		}

		data = bson.M{
			"product": product,
		}
		api_helper.SuccessResponse(response, data, "Insert Product and create Inventory Status, Inventory Log successfully")
		return
	}
}

// Get Product By Id godoc
// @Tags Product
// @Summary Get Product by Id
// @Description Get Product by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Product need to be found"
// @Success 200 {object} model.Product
// @Header 200 {string} Token "qwerty"
// @Router /Product/{id} [get]
func GetProductByProductIdOld(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetProductByProductId")
	var (
		collection  *mongo.Collection
		productRes  model.ProductResponse
		matchFilter bson.A
	)
	response.Header().Set("content-type", "application/json")
	collection = database.Database.Collection(setting.ProductTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := mux.Vars(request)["prodId"]

	matchFilter = append(matchFilter, bson.D{{"product_id", id}}, bson.D{{"is_deleted", false}}) //bson.D{{"tenant_key",tenantKey})

	//Match with Id matchFilter
	match := bson.D{{
		"$match", bson.D{
			{"$and", matchFilter},
		},
	}}

	//Look up
	lookupInventoryStatus := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.InventoryStatusTable},
				{"localField", "product_id"},
				{"foreignField", "product_id"},
				{"as", "inventory_status"},
			},
		}}

	sumInventoryQuantity := bson.D{
		{"$addFields", bson.D{
			{"on_hand_qty", bson.D{
				{"$sum", fmt.Sprintf("$%s.on_hand_qty", setting.InventoryStatusTable)},
			}},
			{"on_available_qty", bson.D{
				{"$sum", fmt.Sprintf("$%s.on_available_qty", setting.InventoryStatusTable)},
			}},
		}},
	}
	//sort, skip and limit
	pipeline := mongo.Pipeline{
		match,
		lookupInventoryStatus,
		sumInventoryQuantity,
	}

	cursor, err := collection.Aggregate(ctx, pipeline)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Product ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		cursor.Decode(&productRes)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch cursor ERROR - "+err.Error())
		return
	}

	if productRes.ID == primitive.NilObjectID {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "No document in Database")
		return
	}

	data := bson.M{"product": productRes}
	api_helper.SuccessResponse(response, data, "Get Product by ID successfully")
}

func GetProductByProductId(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetProductByProductId")
	var (
		collection *mongo.Collection
		productRes model.ProductResponse
		//productRes  bson.M
		matchFilter bson.A
	)
	response.Header().Set("content-type", "application/json")
	collection = database.Database.Collection(setting.ProductTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	productId, _ := mux.Vars(request)["prodId"]

	matchFilter = append(matchFilter, bson.D{{"product_id", productId}}, bson.D{{"is_deleted", false}}) //bson.D{{"tenant_key",tenantKey})

	//Match with Id matchFilter
	match := bson.D{{
		"$match", bson.D{
			{"$and", matchFilter},
		},
	}}

	//lookupInventoryStatus := bson.D{
	//	{
	//		"$lookup", bson.D{
	//		{"from", setting.InventoryStatusTable},
	//		{"localField", "product_id"},
	//		{"foreignField", "product_id"},
	//		{"as", "inventory_status"},
	//	},
	//	}}

	//Look up
	lookupInventoryStatus := bson.D{
		{Key: "$lookup", Value: bson.M{
			"from": setting.InventoryStatusTable,
			"let":  bson.M{},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{bson.M{"$eq": bson.A{"$product_id", productId}}}}}},
				bson.M{
					"$lookup": bson.M{
						"from": setting.SalesOrderTable,
						"let":  bson.M{"siteId": "$site_id"},
						"pipeline": bson.A{
							bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{bson.M{"$eq": bson.A{"$site_id", "$$siteId"}}}}}},
							bson.M{
								"$lookup": bson.M{
									"from": setting.SalesOrderDetailTable,
									"let":  bson.M{"key": "$_id"},
									"pipeline": bson.A{
										bson.M{
											"$match": bson.M{
												"$expr": bson.M{
													"$and": bson.A{
														bson.M{"$eq": bson.A{"$doc_key", "$$key"}},
														bson.M{"$eq": bson.A{"$product_code", productId}},
														bson.M{"$in": bson.A{"$doc_line_status", bson.A{enum.OrderLineStatusNew, enum.OrderLineStatusProcessing, enum.OrderLineStatusPicked, enum.OrderLineStatusPacked}}},
													},
												},
											},
										},
									},
									"as": setting.SalesOrderDetailTable,
								},
							},
							bson.M{"$unwind": bson.M{"path": "$" + setting.SalesOrderDetailTable}},
							bson.M{
								"$group": bson.M{
									"_id": bsonx.Null(),
									"new_qty": bson.M{
										"$sum": bson.M{
											"$cond": bson.A{
												bson.M{
													"$eq": bson.A{
														"$" + setting.SalesOrderDetailTable + ".doc_line_status",
														enum.OrderLineStatusNew,
													},
												},
												"$" + setting.SalesOrderDetailTable + ".quantity",
												0,
											},
										},
									},
									"processing_qty": bson.M{
										"$sum": bson.M{
											"$cond": bson.A{
												bson.M{
													"$eq": bson.A{
														"$" + setting.SalesOrderDetailTable + ".doc_line_status",
														enum.OrderLineStatusProcessing,
													},
												},
												"$" + setting.SalesOrderDetailTable + ".quantity",
												0,
											},
										},
									},
									"picked_qty": bson.M{
										"$sum": bson.M{
											"$cond": bson.A{
												bson.M{
													"$eq": bson.A{
														"$" + setting.SalesOrderDetailTable + ".doc_line_status",
														enum.OrderLineStatusPicked,
													},
												},
												"$" + setting.SalesOrderDetailTable + ".quantity",
												0,
											},
										},
									},
									"packed_qty": bson.M{
										"$sum": bson.M{
											"$cond": bson.A{
												bson.M{
													"$eq": bson.A{
														"$" + setting.SalesOrderDetailTable + ".doc_line_status",
														enum.OrderLineStatusPacked,
													},
												},
												"$" + setting.SalesOrderDetailTable + ".quantity",
												0,
											},
										},
									},
									"return_qty": bson.M{
										"$sum": "$" + setting.SalesOrderDetailTable + ".return_quantity",
									},
									"cancel_qty": bson.M{
										"$sum": "$" + setting.SalesOrderDetailTable + ".cancel_quantity",
									},
								},
							},
						},
						"as": setting.SalesOrderDetailTable + "s",
					},
				},
				bson.M{"$unwind": bson.M{"path": "$" + setting.SalesOrderDetailTable + "s", "preserveNullAndEmptyArrays": true}},

				bson.M{
					"$project": bson.M{
						"_id":              1,
						"product_key":      1,
						"product_id":       1,
						"product_name":     1,
						"site_key":         1,
						"site_id":          1,
						"site_name":        1,
						"ych_qty":          1,
						"on_hand_qty":      1,
						"on_order_qty":     1,
						"on_available_qty": 1,
						"on_commit_qty":    1,
						"on_tsf_qty":       1,
						"reverse_qty":      1,
						"new_qty":          "$" + setting.SalesOrderDetailTable + "s.new_qty",
						"processing_qty":   "$" + setting.SalesOrderDetailTable + "s.processing_qty",
						"picked_qty":       "$" + setting.SalesOrderDetailTable + "s.picked_qty",
						"packed_qty":       "$" + setting.SalesOrderDetailTable + "s.packed_qty",
						"return_qty":       "$" + setting.SalesOrderDetailTable + "s.return_qty",
						"cancel_qty":       "$" + setting.SalesOrderDetailTable + "s.cancel_qty",
						"created_date":     1,
						"updated_date":     1,
					},
				},
			},
			"as": setting.InventoryStatusTable,
		},
		}}
	sumInventoryQuantity := bson.D{
		{"$addFields", bson.D{
			{"on_hand_qty", bson.D{
				{"$sum", fmt.Sprintf("$%s.on_hand_qty", setting.InventoryStatusTable)},
			}},
			{"on_available_qty", bson.D{
				{"$sum", fmt.Sprintf("$%s.on_available_qty", setting.InventoryStatusTable)},
			}},
			{"return_qty", bson.D{
				{"$sum", fmt.Sprintf("$%s.return_qty", setting.InventoryStatusTable)},
			}},
			{"cancel_qty", bson.D{
				{"$sum", fmt.Sprintf("$%s.cancel_qty", setting.InventoryStatusTable)},
			}},
		}},
	}
	//sort, skip and limit
	pipeline := mongo.Pipeline{
		match,
		lookupInventoryStatus,
		sumInventoryQuantity,
	}

	cursor, err := collection.Aggregate(ctx, pipeline)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Product ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	if cursor.Next(ctx) {
		cursor.Decode(&productRes)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch cursor ERROR - "+err.Error())
		return
	}

	if productRes.ID == primitive.NilObjectID {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "No document in Database")
		return
	}

	data := bson.M{"product": productRes}
	api_helper.SuccessResponse(response, data, "Get Product by Product ID successfully")
}

func GetProductByKey(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetProductByProductId")
	var (
		collection  *mongo.Collection
		productRes  model.ProductResponse
		product     model.Product
		matchFilter bson.A
	)
	response.Header().Set("content-type", "application/json")
	collection = database.Database.Collection(setting.ProductTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	key, _ := primitive.ObjectIDFromHex(mux.Vars(request)["key"])

	if err = collection.FindOne(ctx, bson.M{"_id": bsonx.ObjectID(key)}).Decode(&product); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Product by Key ERROR - "+err.Error())
		return
	}

	matchFilter = append(matchFilter, bson.D{{"product_id", product.ProductId}}, bson.D{{"is_deleted", false}}) //bson.D{{"tenant_key",tenantKey})

	//Match with Id matchFilter
	match := bson.D{{
		"$match", bson.D{
			{"$and", matchFilter},
		},
	}}

	//Look up
	//lookupInventoryStatus := bson.D{
	//	{
	//		"$lookup", bson.D{
	//		{"from", setting.InventoryStatusTable},
	//		{"localField", "product_id"},
	//		{"foreignField", "product_id"},
	//		{"as", "inventory_status"},
	//	},
	//	}}

	lookupInventoryStatus := bson.D{
		{Key: "$lookup", Value: bson.M{
			"from": setting.InventoryStatusTable,
			"let":  bson.M{},
			"pipeline": bson.A{
				bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{bson.M{"$eq": bson.A{"$product_id", product.ProductId}}}}}},
				bson.M{
					"$lookup": bson.M{
						"from": setting.SalesOrderTable,
						"let":  bson.M{"siteId": "$site_id"},
						"pipeline": bson.A{
							bson.M{"$match": bson.M{"$expr": bson.M{"$and": bson.A{bson.M{"$eq": bson.A{"$site_id", "$$siteId"}}}}}},
							bson.M{
								"$lookup": bson.M{
									"from": setting.SalesOrderDetailTable,
									"let":  bson.M{"key": "$_id"},
									"pipeline": bson.A{
										bson.M{
											"$match": bson.M{
												"$expr": bson.M{
													"$and": bson.A{
														bson.M{"$eq": bson.A{"$doc_key", "$$key"}},
														bson.M{"$eq": bson.A{"$product_code", product.ProductId}},
														bson.M{"$in": bson.A{"$doc_line_status", bson.A{enum.OrderLineStatusNew, enum.OrderLineStatusProcessing, enum.OrderLineStatusPicked, enum.OrderLineStatusPacked}}},
													},
												},
											},
										},
									},
									"as": setting.SalesOrderDetailTable,
								},
							},
							bson.M{"$unwind": bson.M{"path": "$" + setting.SalesOrderDetailTable}},
							bson.M{
								"$group": bson.M{
									"_id": bsonx.Null(),
									"new_qty": bson.M{
										"$sum": bson.M{
											"$cond": bson.A{
												bson.M{
													"$eq": bson.A{
														"$" + setting.SalesOrderDetailTable + ".doc_line_status",
														enum.OrderLineStatusNew,
													},
												},
												"$" + setting.SalesOrderDetailTable + ".quantity",
												0,
											},
										},
									},
									"processing_qty": bson.M{
										"$sum": bson.M{
											"$cond": bson.A{
												bson.M{
													"$eq": bson.A{
														"$" + setting.SalesOrderDetailTable + ".doc_line_status",
														enum.OrderLineStatusProcessing,
													},
												},
												"$" + setting.SalesOrderDetailTable + ".quantity",
												0,
											},
										},
									},
									"picked_qty": bson.M{
										"$sum": bson.M{
											"$cond": bson.A{
												bson.M{
													"$eq": bson.A{
														"$" + setting.SalesOrderDetailTable + ".doc_line_status",
														enum.OrderLineStatusPicked,
													},
												},
												"$" + setting.SalesOrderDetailTable + ".quantity",
												0,
											},
										},
									},
									"packed_qty": bson.M{
										"$sum": bson.M{
											"$cond": bson.A{
												bson.M{
													"$eq": bson.A{
														"$" + setting.SalesOrderDetailTable + ".doc_line_status",
														enum.OrderLineStatusPacked,
													},
												},
												"$" + setting.SalesOrderDetailTable + ".quantity",
												0,
											},
										},
									},
									"return_qty": bson.M{
										"$sum": "$" + setting.SalesOrderDetailTable + ".return_quantity",
									},
									"cancel_qty": bson.M{
										"$sum": "$" + setting.SalesOrderDetailTable + ".cancel_quantity",
									},
								},
							},
						},
						"as": setting.SalesOrderDetailTable + "s",
					},
				},
				bson.M{"$unwind": bson.M{"path": "$" + setting.SalesOrderDetailTable + "s", "preserveNullAndEmptyArrays": true}},
				bson.M{
					"$project": bson.M{
						"_id":              1,
						"product_key":      1,
						"product_id":       1,
						"product_name":     1,
						"site_key":         1,
						"site_id":          1,
						"site_name":        1,
						"ych_qty":          1,
						"on_hand_qty":      1,
						"on_order_qty":     1,
						"on_available_qty": 1,
						"on_commit_qty":    1,
						"on_tsf_qty":       1,
						"reverse_qty":      1,
						"new_qty":          "$" + setting.SalesOrderDetailTable + "s.new_qty",
						"processing_qty":   "$" + setting.SalesOrderDetailTable + "s.processing_qty",
						"picked_qty":       "$" + setting.SalesOrderDetailTable + "s.picked_qty",
						"packed_qty":       "$" + setting.SalesOrderDetailTable + "s.packed_qty",
						"return_qty":       "$" + setting.SalesOrderDetailTable + "s.return_qty",
						"cancel_qty":       "$" + setting.SalesOrderDetailTable + "s.cancel_qty",
						"created_date":     1,
						"updated_date":     1,
					},
				},
			},
			"as": setting.InventoryStatusTable,
		},
		}}

	sumInventoryQuantity := bson.D{
		{"$addFields", bson.D{
			{"on_hand_qty", bson.D{
				{"$sum", fmt.Sprintf("$%s.on_hand_qty", setting.InventoryStatusTable)},
			}},
			{"on_available_qty", bson.D{
				{"$sum", fmt.Sprintf("$%s.on_available_qty", setting.InventoryStatusTable)},
			}},
		}},
	}
	//sort, skip and limit
	pipeline := mongo.Pipeline{
		match,
		lookupInventoryStatus,
		sumInventoryQuantity,
	}

	cursor, err := collection.Aggregate(ctx, pipeline)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Product ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	if cursor.Next(ctx) {
		cursor.Decode(&productRes)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch cursor ERROR - "+err.Error())
		return
	}

	if productRes.ID == primitive.NilObjectID {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "No document in Database")
		return
	}

	data := bson.M{"product": productRes}
	api_helper.SuccessResponse(response, data, "Get Product by Key successfully")
}

// Get Products godoc
// @Tags Product
// @Summary Get number of Products sort by created_date field descending
// @Description Get number of Products depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Param keyword path string false "Keyword search. For barcode, short & long description, color, size of Product"
// @Success 200 {object} []model.Product
// @Header 200 {string} Token "qwerty"
// @Router /Product [get]
func GetProducts(response http.ResponseWriter, request *http.Request) {
	//handle error
	var (
		productRes    model.ProductResponse
		allProductRes []model.ProductResponse
		funcName      = "GetProducts"
		matchFilter   bson.A
	)
	defer api_helper.RecoverError(response, funcName)
	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.ProductTable)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	//Get Tenant Key fromm Token
	//_, err = api_helper.GetApplicationFromToken(setting.Token.GetClientID())
	//if err != nil {
	//	logger.Logger.Errorf(" `%s` ERROR : `%s`", funcName, err.Error())
	//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Get Tenant Key from Token ERROR - "+err.Error())
	//	return
	//}

	keyword := request.URL.Query().Get("keyword")
	if keyword != "" {
		or := bson.D{{Key: "$or", Value: bson.A{
			bson.D{{"product_id", primitive.Regex{Pattern: keyword, Options: "i"}}},
			bson.D{{"barcode", primitive.Regex{Pattern: keyword, Options: "i"}}},
			bson.D{{"short_description", primitive.Regex{Pattern: keyword, Options: "i"}}},
			bson.D{{"long_description", primitive.Regex{Pattern: keyword, Options: "i"}}},
			bson.D{{"color", primitive.Regex{Pattern: keyword, Options: "i"}}},
			bson.D{{"size", primitive.Regex{Pattern: keyword, Options: "i"}}},
		}}}
		matchFilter = append(matchFilter, or)
	}

	//if len(matchFilter) == 0 {
	//	matchFilter = bson.A{bson.D{{"_id", bson.D{{"$ne", bsonx.ObjectID(primitive.NilObjectID)}}}}}
	//}
	matchFilter = append(matchFilter, bson.D{{"is_deleted", false}}) //bson.D{{"tenant_key",tenantKey})

	//Match with Id matchFilter
	match := bson.D{{
		"$match", bson.D{
			{"$and", matchFilter},
		},
	}}

	//Look up
	lookupInventoryStatus := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.InventoryStatusTable},
				{"localField", "product_id"},
				{"foreignField", "product_id"},
				{"as", setting.InventoryStatusTable},
			},
		}}

	sumInventoryQuantity := bson.D{
		{"$addFields", bson.D{
			{"on_hand_qty", bson.D{
				{"$sum", fmt.Sprintf("$%s.on_hand_qty", setting.InventoryStatusTable)},
			}},
			{"on_available_qty", bson.D{
				{"$sum", fmt.Sprintf("$%s.on_available_qty", setting.InventoryStatusTable)},
			}},
		}},
	}
	//sort, skip and limit
	sort := bson.D{{"$sort", bson.D{{"created_date", -1}}}}
	skip := bson.D{{"$skip", limit * (page - 1)}}
	lim := bson.D{{"$limit", limit}}
	pipeline := mongo.Pipeline{
		match,
		sort,
		skip,
		lim,
		lookupInventoryStatus,
		sumInventoryQuantity,
	}

	cursor, err := collection.Aggregate(ctx, pipeline)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Product ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		cursor.Decode(&productRes)
		allProductRes = append(allProductRes, productRes)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch cursor ERROR - "+err.Error())
		return
	}
	data := bson.M{"product": allProductRes}
	api_helper.SuccessResponse(response, data, "Get Product successfully")
}

// Get count of Products godoc
// @Tags Product
// @Summary Get count of Products
// @Description Get number of Products depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param keyword path string false "Keyword search. For barcode, short & long description, color, size of Product"
// @Success 200 {object} model.Count
// @Header 200 {string} Token "qwerty"
// @Router /Product/count [get]
func GetProductsCount(response http.ResponseWriter, request *http.Request) {
	//handle error
	var (
		funcName = "GetProductsCount"
		andArray = bson.A{
			bson.M{"_id": bson.M{"$ne": bsonx.ObjectID(primitive.NilObjectID)}},
			bson.M{"is_deleted": false},
		}
	)
	defer api_helper.RecoverError(response, funcName)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.ProductTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//Get Tenant Key fromm Token
	//_, err = api_helper.GetApplicationFromToken(setting.Token.GetClientID())
	//if err != nil {
	//	logger.Logger.Errorf(" `%s` ERROR : `%s`", funcName, err.Error())
	//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Get Tenant from Token ERROR - "+err.Error())
	//	return
	//}

	keyword := request.URL.Query().Get("keyword")
	if keyword != "" {
		or := bson.D{{Key: "$or", Value: bson.A{
			bson.D{{"barcode", primitive.Regex{Pattern: keyword, Options: "i"}}},
			bson.D{{"product_id", primitive.Regex{Pattern: keyword, Options: "i"}}},
			bson.D{{"short_description", primitive.Regex{Pattern: keyword, Options: "i"}}},
			bson.D{{"long_description", primitive.Regex{Pattern: keyword, Options: "i"}}},
			bson.D{{"color", primitive.Regex{Pattern: keyword, Options: "i"}}},
			bson.D{{"size", primitive.Regex{Pattern: keyword, Options: "i"}}},
		}}}
		andArray = append(andArray, or)
	}

	filter := bson.M{
		"$and": andArray,
		//{"tenant_key", tenantKey},
	}

	count, err := collection.CountDocuments(ctx, filter)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Product count ERROR - "+err.Error())
		return
	}

	data := bson.M{"count": count}
	api_helper.SuccessResponse(response, data, "Get Count Product successfully")
}

// Update Product godoc
// @Tags Product
// @Summary Update Product
// @Description Update Product
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Product need to be found"
// @Param body body model.ProductRequest true "Update Product"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Product/{id} [put]
func UpdateProduct(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var (
		collection *mongo.Collection
		product    model.Product
		productReq model.ProductRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ProductTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err = collection.FindOne(ctx, filter).Decode(&product)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = json.NewDecoder(request.Body).Decode(&productReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&product, &productReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set lai thoi gian luc update
	product.UpdatedDate = time.Now()

	update := bson.M{"$set": product}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"product": product}
	api_helper.SuccessResponse(response, data, "Update Product Successfully")
}

// Soft Delete Product godoc
// @Tags Product
// @Summary Soft Delete Product
// @Description Soft Delete Product
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Product need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Product/{id} [delete]
func DeleteSoftProduct(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteSoftProduct")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ProductTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	api_helper.SuccessResponse(response, id, "Delete Product Successfully")
}

//DeleteHardProduct
/*
func DeleteHardProduct(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardProduct")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.ProductTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
//-------------------- Unused -----------------------

func CreateManyProduct(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyProduct")
	var (
		collection    *mongo.Collection
		productInputs []model.ProductRequest
		product       model.Product
		ui            []interface{}
	)
	collection = database.Database.Collection(setting.ProductTable)
	response.Header().Set("content-type", "application/json")

	json.NewDecoder(request.Body).Decode(&productInputs)
	for _, productInput := range productInputs {
		api_helper.ValidateProduct(productInput)
		err = copier.Copy(&product, &productInput)
		if err != nil {
			json.NewEncoder(response).Encode(err.Error())
			return
		}
		product.ID = primitive.NewObjectID()
		product.CreatedDate = time.Now()
		product.UpdatedDate = time.Now()
		ui = append(ui, product)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

func CreateManyProductTest(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyProduct")
	var (
		collection    *mongo.Collection
		productInputs []model.ProductRequest
		product       model.Product
		ui            []interface{}
	)
	collection = database.Database.Collection(setting.ProductTable)
	response.Header().Set("content-type", "application/json")

	json.NewDecoder(request.Body).Decode(&productInputs)
	for _, productInput := range productInputs {
		api_helper.ValidateProduct(productInput)
		err = copier.Copy(&product, &productInput)
		if err != nil {
			json.NewEncoder(response).Encode(err.Error())
			return
		}
		product.ID = primitive.NewObjectID()
		product.CreatedDate = time.Now()
		product.UpdatedDate = time.Now()
		ui = append(ui, product)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}
