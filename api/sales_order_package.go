package api

import (
	"context"
	"encoding/json"
	"net/http"
	"os"
	"time"

	"../setting"
	"github.com/jinzhu/copier"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateSalesOrderPackage godoc
// @Tags Sales Order Package
// @Summary Add Sales Order Package
// @Description Add Sales Order Package
// @Accept  json
// @Produce  json
// @Param body body model.SalesOrderPackageRequest true "Add Sales Order Package"
// @Success 200 {object} model.SalesOrderPackage
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderPackage [POST]
func CreateSalesOrderPackage(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateSalesOrderPackage")
	var (
		collection           *mongo.Collection
		salesOrderPackage    model.SalesOrderPackage
		salesOrderPackageReq model.SalesOrderPackageRequest
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.SalesOrderPackageTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&salesOrderPackageReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Sales Order Package Input ERROR - "+err.Error())
		return
	}

	_ = copier.Copy(&salesOrderPackage, &salesOrderPackageReq)

	currentTime := time.Now()
	salesOrderPackage.ID = primitive.NewObjectIDFromTimestamp(currentTime)
	salesOrderPackage.ObjectType = setting.SalesOrderPackageTable
	salesOrderPackage.CreatedDate = currentTime
	salesOrderPackage.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, salesOrderPackage)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Sales Order Package ERROR - "+err.Error())
		return
	}

	data := bson.M{"sales_order_package": salesOrderPackage}
	api_helper.SuccessResponse(response, data, "Create Sales Order Package successfully")
}

// GetSalesOrderPackageByID godoc
// @Tags Sales Order Package
// @Summary Get Sales Order Package by Id
// @Description Get Sales Order Package by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Order Package need to be found"
// @Success 200 {object} model.SalesOrderPackage
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderPackage/{id} [get]
func GetSalesOrderPackageByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSaleOrderPackageByID")
	var (
		collection        *mongo.Collection
		salesOrderPackage model.SalesOrderPackage
	)
	collection = database.Database.Collection(setting.SalesOrderPackageTable)

	response.Header().Set("content-type", "application/json")

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	err := collection.FindOne(ctx, filter).Decode(&salesOrderPackage)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesOrderPackage)
}

// GetSalesOrderPackages godoc
// @Tags Sales Order Package
// @Summary Get all Sales Order Package
// @Description Get all Sales Order Package
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.SalesOrderPackage
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderPackage [get]
func GetSalesOrderPackages(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesOrderPackages")
	var (
		collection         *mongo.Collection
		salesOrderPackages []model.SalesOrderPackage
		findOptions        = options.Find()
		filter             = bson.M{}
	)
	collection = database.Database.Collection(setting.SalesOrderPackageTable)

	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions) //findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var saleOrderPackage model.SalesOrderPackage
		cursor.Decode(&saleOrderPackage)
		salesOrderPackages = append(salesOrderPackages, saleOrderPackage)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesOrderPackages)
}

// UpdateSalesOrderPackage godoc
// @Tags Sales Order Package
// @Summary Update Sales Order Package
// @Description Update Sales Order Package
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Order need to be found"
// @Param body body model.SalesOrderRequest true "Update Sales Order Package"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderPackage/{id} [put]
func UpdateSalesOrderPackage(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateSaleOrderPackage")
	var (
		collection       *mongo.Collection
		saleOrderPackage model.SalesOrderPackage
	)
	collection = database.Database.Collection(setting.SalesOrderPackageTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&saleOrderPackage)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	saleOrderPackage.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": saleOrderPackage}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

func DeleteSoftSalesOrderPackage(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftSalesOrderPackage")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.SalesOrderPackageTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
