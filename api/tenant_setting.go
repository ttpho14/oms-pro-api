package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"reflect"
	"time"

	"../database"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

// Add TenantSetting godoc
// @Tags TenantSetting
// @Summary Add TenantSetting
// @Description Add TenantSetting
// @Accept  json
// @Produce  json
// @Param body body model.TenantSettingRequest true "Add TenantSetting"
// @Success 200 {object} model.TenantSetting
// @Header 200 {string} Token "qwerty"
// @Router /TenantSetting [POST]
func CreateTenantSetting(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateTenantSetting")
	var (
		collection       *mongo.Collection
		tenantSetting    model.TenantSetting
		tenantSettingReq model.TenantSettingRequest
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.TenantSettingTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&tenantSettingReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&tenantSetting, &tenantSettingReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	currentTime := time.Now()
	tenantSetting.ID = primitive.NewObjectID()
	tenantSetting.CreatedDate = currentTime
	tenantSetting.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, tenantSetting)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	data := bson.M{"tenant_setting": tenantSetting}
	api_helper.SuccessResponse(response, data, "Insert Tenant Setting successfully")
}

func CreateManyTenantSetting(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyTenantSetting")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.TenantSettingTable)
	response.Header().Set("content-type", "application/json")
	var manyTenantSetting []model.TenantSetting
	err = json.NewDecoder(request.Body).Decode(&manyTenantSetting)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, tenantSetting := range manyTenantSetting {
		tenantSetting.ID = primitive.NewObjectID()
		tenantSetting.CreatedDate = time.Now()
		tenantSetting.UpdatedDate = time.Now()
		ui = append(ui, tenantSetting)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// Get TenantSetting By Id godoc
// @Tags TenantSetting
// @Summary Get TenantSetting by Id
// @Description Get TenantSetting by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of TenantSetting need to be found"
// @Success 200 {object} model.TenantSetting
// @Header 200 {string} Token "qwerty"
// @Router /TenantSetting/{id} [get]
func GetTenantSettingById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetTenantSettingById")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.TenantSettingTable)
	response.Header().Set("content-type", "application/json")
	var tenantSetting model.TenantSetting
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//_id = id and is_deleted = false
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}, "is_deleted": bson.M{"$eq": false}}
	err := collection.FindOne(ctx, filter).Decode(&tenantSetting)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(tenantSetting)
}

// Get number of TenantSettings godoc
// @Tags TenantSetting
// @Summary Get number of TenantSettings sort by created_date field descending
// @Description Get number of TenantSettings depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.TenantSetting
// @Header 200 {string} Token "qwerty"
// @Router /TenantSetting [get]
func GetTenantSettings(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetTenantSettings")
	//Get Limit and Page
	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	collection := database.Database.Collection(setting.TenantSettingTable)
	response.Header().Set("content-type", "application/json")
	var allTenantSetting []model.TenantSetting
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"is_deleted": bson.M{"$eq": false}}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var tenantSetting model.TenantSetting
		cursor.Decode(&tenantSetting)
		allTenantSetting = append(allTenantSetting, tenantSetting)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(allTenantSetting)
}

// Update TenantSetting godoc
// @Tags TenantSetting
// @Summary Update TenantSetting
// @Description Update TenantSetting
// @Accept  json
// @Produce  json
// @Param id path string true "Id of TenantSetting need to be found"
// @Param body body model.TenantSettingRequest true "Update TenantSetting"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /TenantSetting/{id} [put]
func UpdateTenantSetting(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "UpdateTenantSetting")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.TenantSettingTable)
	response.Header().Set("content-type", "application/json")
	var tenantSetting model.TenantSetting
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	err = json.NewDecoder(request.Body).Decode(&tenantSetting)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	//Set lai thoi gian luc update
	tenantSetting.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	fmt.Println("filter TenantSetting", filter)
	update := bson.M{"$set": tenantSetting}
	result, err := collection.UpdateOne(ctx, filter, update)
	fmt.Println("Result TenantSetting", result)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

func DeleteSoftTenantSetting(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteSoftTenantSetting")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.TenantSettingTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("UpdateOne() result:", result)
		fmt.Println("UpdateOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("UpdateOne() result MatchedCount:", result.MatchedCount)
		fmt.Println("UpdateOne() result ModifiedCount:", result.ModifiedCount)
		fmt.Println("UpdateOne() result UpsertedCount:", result.UpsertedCount)
		fmt.Println("UpdateOne() result UpsertedID:", result.UpsertedID)
	}
	json.NewEncoder(response).Encode(result)
}

//DeleteHardTenantSetting
/*
func DeleteHardTenantSetting(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardTenantSetting")
	var collection *mongo.Collection
	collection = database.Database.Collection("tenantSetting")
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
//-------------------- REGION API - END -----------------------
