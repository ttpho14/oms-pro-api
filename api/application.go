package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"../database"
	"../model"
	"../setting"
	helper "./api_helper"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// Add Application godoc
// @Tags Application
// @Summary Add Application
// @Description Add Application
// @Accept  json
// @Produce  json
// @Param body body model.ApplicationRequest true "Add Application"
// @Success 200 {object} model.Application
// @Header 200 {string} Token "qwerty"
// @Router /Application [POST]
func CreateApplication(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "CreateApplication")
	response.Header().Set("content-type", "application/json")
	// Create Client ID, Secret.
	createClientId()
	createSecretKey()
	var (
		collection                  *mongo.Collection
		application                 model.Application
		applicationDetails          model.ApplicationWithDetails
		addApplication              model.ApplicationRequest
		uiApplicationConfigurations []interface{}
		applicationConfiguration    model.ApplicationConfiguration
		currentTime                 = time.Now()
	)
	collection = database.Database.Collection(setting.ApplicationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// Decode from Body to Input model
	err = json.NewDecoder(request.Body).Decode(&addApplication)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	//Copy
	err = copier.Copy(&application, &addApplication)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Assign others value to main model.
	application.ID = primitive.NewObjectID()
	application.CreatedDate = currentTime
	application.UpdatedDate = currentTime
	application.IsDeleted = false
	application.ClientId = GetClientId()
	application.SecretKey = GetSecretKey()

	//Application Configuration.
	for _, addApplicationConfiguration := range addApplication.ApplicationConfigurationRequest {
		//Copy fields value from Input model to main model - copy( mainModel, inputModel)
		err = copier.Copy(&applicationConfiguration, &addApplicationConfiguration)
		if err != nil {
			helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
			return
		}
		// Assign others value to main model.
		applicationConfiguration.ApplicationKey = application.ID
		applicationConfiguration.ID = primitive.NewObjectID()
		applicationConfiguration.CreatedDate = currentTime
		applicationConfiguration.UpdatedDate = currentTime
		applicationConfiguration.IsDeleted = false
		uiApplicationConfigurations = append(uiApplicationConfigurations, applicationConfiguration)
	}

	if len(uiApplicationConfigurations) > 0 {
		collection = database.Database.Collection(setting.ApplicationConfigurationTable)
		// Sales Order Detail
		_, err = collection.InsertMany(ctx, uiApplicationConfigurations)
		if err != nil {
			helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Many ERROR - "+err.Error())
			return
		}
	}

	collection = database.Database.Collection(setting.ApplicationTable)
	_, err := collection.InsertOne(ctx, application)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	_ = copier.Copy(&applicationDetails, &application)
	bytes, _ := json.Marshal(uiApplicationConfigurations)
	_ = json.Unmarshal(bytes, &applicationDetails.ApplicationConfiguration)

	data := bson.M{
		"application": applicationDetails,
	}
	helper.SuccessResponse(response, data, "Create Application successfully")
}

// Get Application By Id godoc
// @Tags Application
// @Summary Get Application by Id
// @Description Get Application by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Application need to be found"
// @Success 200 {object} model.Application
// @Header 200 {string} Token "qwerty"
// @Router /Application/{id} [get]
func GetApplicationByKey(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "GetApplicationByKey")
	response.Header().Set("content-type", "application/json")

	var (
		collection  *mongo.Collection
		application model.ApplicationWithDetails
	)

	collection = database.Database.Collection(setting.ApplicationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	key, _ := primitive.ObjectIDFromHex(mux.Vars(request)["key"])

	//_id = key and is_deleted = false
	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.D{{"is_deleted", false}},
				bson.D{{"_id", bsonx.ObjectID(key)}},
			},
			},
		},
	},
	}
	// lookup = join table on ....
	lookupAppConfig := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.ApplicationConfigurationTable},
				{"localField", "_id"},
				{"foreignField", "application_key"},
				{"as", "application_configurations"},
			},
		},
	}
	pipeline := mongo.Pipeline{
		match,
		lookupAppConfig,
	}

	cursor, err := collection.Aggregate(ctx, pipeline) //findOptions)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	cursor.Next(ctx)
	err = cursor.Decode(&application)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"application": application}
	helper.SuccessResponse(response, data, "Get Application by key successfully")
}

// Get Application By Id godoc
// @Tags Application
// @Summary Get Application by Id
// @Description Get Application by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Application need to be found"
// @Success 200 {object} model.Application
// @Header 200 {string} Token "qwerty"
// @Router /Application/{id} [get]
func GetApplicationById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "GetApplicationByKey")
	response.Header().Set("content-type", "application/json")

	var (
		collection  *mongo.Collection
		application model.ApplicationWithDetails
	)

	collection = database.Database.Collection(setting.ApplicationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := mux.Vars(request)["id"]

	//_id = id and is_deleted = false
	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.D{{"is_deleted", false}},
				bson.D{{"application_id", id}},
			},
			},
		},
	},
	}
	// lookup = join table on ....
	lookupAppConfig := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.ApplicationConfigurationTable},
				{"localField", "_id"},
				{"foreignField", "application_key"},
				{"as", "application_configurations"},
			},
		},
	}
	pipeline := mongo.Pipeline{
		match,
		lookupAppConfig,
	}

	cursor, err := collection.Aggregate(ctx, pipeline) //findOptions)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	cursor.Next(ctx)
	err = cursor.Decode(&application)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"application": application}
	helper.SuccessResponse(response, data, "Get Application by Application Id successfully")
}

// Get number of Applications godoc
// @Tags Application
// @Summary Get number of Applications sort by created_date field descending
// @Description Get number of Applications depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.Application
// @Header 200 {string} Token "qwerty"
// @Router /Application [get]
func GetApplications(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "GetApplications")

	var (
		allApplicationWithDetails []model.ApplicationWithDetails
		applicationWithDetails    model.ApplicationWithDetails
		collection                *mongo.Collection
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ApplicationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := helper.GetLimitAndPage(request)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	// match = Where
	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.D{
					{"is_deleted", false},
				},
			},
			},
		},
	}}
	// lookup = join table on ....
	lookupAppConfig := bson.D{
		{
			"$lookup", bson.D{
				{"from", setting.ApplicationConfigurationTable},
				{"localField", "_id"},
				{"foreignField", "application_key"},
				{"as", "application_configurations"},
			},
		},
	}
	// Sort = Order by , "1" = asc , "-1" = desc
	sort := bson.D{{"$sort", bson.D{{"created_date", -1}}}}
	// Skip = Skip number of record
	skip := bson.D{{"$skip", limit * (page - 1)}}
	// lim = Limit number of record in 1 page.
	lim := bson.D{{"$limit", limit}}
	pipeline := mongo.Pipeline{
		match,
		lookupAppConfig,
		sort,
		skip,
		lim,
	}

	cursor, err := collection.Aggregate(ctx, pipeline) //findOptions)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		cursor.Decode(&applicationWithDetails)
		allApplicationWithDetails = append(allApplicationWithDetails, applicationWithDetails)
	}
	if err := cursor.Err(); err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch ERROR - "+err.Error())
		return
	}

	data := bson.M{"application": allApplicationWithDetails}
	helper.SuccessResponse(response, data, "Get Application with details successfully")
}

// Update Application godoc
// @Tags Application
// @Summary Update Application
// @Description Update Application
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Application need to be found"
// @Param body body model.ApplicationRequest true "Update Application"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Application/{id} [put]
// Find ID from DB
//=> If exists
//	=> Decode to Main Model.
//		Decode Body.Request to Input Model
//		Copy fields value from Input Model to Main Model.
//		Assign other fields value that don't exists in Input Model
func UpdateApplication(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "UpdateApplication")
	// Initial Values.
	var (
		collection          *mongo.Collection
		appConfigCollection *mongo.Collection
		application         model.Application
		applicationReq      model.ApplicationRequest
		applicationConfig   model.ApplicationConfiguration
		currentTime         = time.Now()
		appFilter           bson.M
		appConfigFilter     bson.M
		appUpdate           bson.M
		appConfigUpdate     bson.M
		opts                = options.Update().SetUpsert(true)
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ApplicationTable)
	appConfigCollection = database.Database.Collection(setting.ApplicationConfigurationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Get Id Param from request URL
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	// Filter to find Application by Id, is_deleted = false
	appFilter = bson.M{
		"_id":        bsonx.ObjectID(id),
		"is_deleted": false,
	}

	//Find Application By Id
	err = collection.FindOne(ctx, appFilter).Decode(&application)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find or Decode ERROR - "+err.Error())
		return
	}

	// Decode Request.Body to Input Model
	err = json.NewDecoder(request.Body).Decode(&applicationReq)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	// Copy fields Value from Input Model to Main Model
	err = copier.Copy(&application, &applicationReq)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	for _, applicationConfigReq := range applicationReq.ApplicationConfigurationRequest {
		//check if new Configuration ID exists in DB
		appConfigFilter = bson.M{
			"_id":              bson.M{"$ne": bsonx.ObjectID(applicationConfigReq.ID)},
			"application_key":  bsonx.ObjectID(application.ID),
			"configuration_id": applicationConfigReq.ConfigurationId,
			"is_deleted":       false,
		}

		count, err := appConfigCollection.CountDocuments(ctx, appConfigFilter)
		if err != nil {
			helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ID Exists ERROR - "+err.Error())
			return
		}
		if count > 0 {
			helper.ErrorResponse(response, http.StatusInternalServerError, 0, fmt.Sprintf("Configuration ID '%s' already exists", applicationConfigReq.ConfigurationId))
			return
		}

		//Find Application config in DB.
		appConfigFilter = bson.M{"_id": bsonx.ObjectID(applicationConfigReq.ID)}
		_ = appConfigCollection.FindOne(ctx, appConfigFilter).Decode(&applicationConfig)

		err = copier.Copy(&applicationConfig, &applicationConfigReq)
		if err != nil {
			helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
			return
		}

		// applicationConfig.ID Is Zero ===>>> This is a new record.
		if primitive.ObjectID.IsZero(applicationConfig.ID) {
			applicationConfig.ID = primitive.NewObjectID()
			applicationConfig.ApplicationKey = application.ID
			applicationConfig.IsDeleted = false
			applicationConfig.ObjectType = setting.ApplicationConfigurationTable
			applicationConfig.CreatedDate = currentTime
		}
		applicationConfig.UpdatedDate = currentTime

		appConfigFilter = bson.M{"_id": bsonx.ObjectID(applicationConfig.ID)}
		appConfigUpdate = bson.M{"$set": applicationConfig}

		// When Update one, upsert config is turn on
		// If result match with filter ==> It will Update
		// Else ==> Insert new Record.
		_, err = appConfigCollection.UpdateOne(ctx, appConfigFilter, appConfigUpdate, opts)
		if err != nil {
			helper.ErrorResponse(response, http.StatusInternalServerError, 0, fmt.Sprintf("Insert/Update Application Configuration ID '%s' ERROR - %s", applicationConfig.ConfigurationId, err.Error()))
			return
		}

	}
	// Assign Main Model's values
	application.UpdatedDate = currentTime

	//Update Application
	appUpdate = bson.M{"$set": application}
	_, err := collection.UpdateOne(ctx, appFilter, appUpdate)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Application ERROR - "+err.Error())
		return
	}

	data := bson.M{"application": application}
	helper.SuccessResponse(response, data, "Update Application successfully")
}

// Soft Delete Application godoc
// @Tags Application
// @Summary Soft Delete Application
// @Description Soft Delete Application
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Application need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Application/{id} [delete]
func DeleteSoftApplication(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "DeleteSoftApplication")

	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ApplicationTable)
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{
		"$set": bson.M{
			"is_deleted":   true,
			"updated_date": time.Now(),
		},
	}

	_, err := collection.UpdateOne(context.Background(), filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	helper.SuccessResponse(response, id, "Delete Application successfully")
}

//--------------------  END -----------------------

func createClientId() {
	clientId = uuid.New().String()[:8]
}

func createSecretKey() {
	secretKey = uuid.New().String()[:8]
}

func GetClientId() string {
	return clientId
}

func GetSecretKey() string {
	return secretKey
}

// ----------------- Unused -----------------------------

func CreateManyApplication(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "CreateManyApplication")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.ApplicationTable)
	response.Header().Set("content-type", "application/json")
	var manyApplication []model.Application
	err = json.NewDecoder(request.Body).Decode(&manyApplication)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(response).Encode(bson.M{"message": err.Error()})
		return
	}
	var ui []interface{}
	for _, application := range manyApplication {
		//createClientId()
		//createSecretKey()

		application.ID = primitive.NewObjectID()
		application.CreatedDate = time.Now()
		application.UpdatedDate = time.Now()
		//application.ClientId= GetClientId()
		//application.SecretKey= GetSecretKey()

		//validation.CheckNul(application)

		ui = append(ui, application)

	}

	_, err = collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(response).Encode(bson.M{"message": err.Error()})
		return
	}
	json.NewEncoder(response).Encode(ui)
}

//DeleteHardApplication
/*
func DeleteHardApplication(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "DeleteHardApplication")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.ApplicationTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
