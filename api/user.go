package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"../database"
	"../model"
	"../model/enum"
	"../service"
	sHelper "../service/helper"
	"../setting"
	"./api_helper"
	"github.com/go-openapi/strfmt"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// Add User godoc
// @Tags User
// @Summary Add User
// @Description Add User
// @Accept  json
// @Produce  json
// @Param body body []model.UserRequest true "Add User"
// @Success 200 {object} []model.User
// @Header 200 {string} Token "qwerty"
// @Router /User [POST]
func CreateUser(response http.ResponseWriter, request *http.Request) {
	//handle error
	var (
		user       model.User
		userReq    model.UserRequest
		res        model.User
		collection *mongo.Collection
		funcName   = "CreateUser"
	)
	defer api_helper.RecoverError(response, funcName)
	response.Header().Set("content-type", "application/json")
	// Collection
	collection = database.Database.Collection(setting.UserTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&userReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&user, &userReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Handle Register
	if !strfmt.IsEmail(user.Email) {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Validate ERROR - "+"Email: '"+user.Email+"' is invalid")
		return
	}

	filter := bson.M{"email": user.Email, "is_deleted": false}
	err := collection.FindOne(ctx, filter).Decode(&res)
	if err != nil {
		// If user name doesn't exists
		if err.Error() == "mongo: no documents in result" {
			//Hash Password
			//hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), 5)
			//if err != nil {
			//	response.WriteHeader(http.StatusInternalServerError)
			//	response.Write([]byte(`{ "message": "Error While Hashing Password, Try Again" }`))
			//	return
			//}
			//user.Password = string(hash)
			//Set Date = time.Now()
			currentTime := time.Now()
			user.ID = primitive.NewObjectID()
			user.IsDeleted = false
			user.CreatedDate = currentTime
			user.UpdatedDate = currentTime
			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()
			_, err = collection.InsertOne(ctx, user)
			if err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
				return
			}

			if err = service.InsertUserPassword(user.Email, user.Password); err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert User Password ERROR - "+err.Error())
				return
			}

			data := bson.M{"user": user}
			api_helper.SuccessResponse(response, data, "Create user success")
			return
		}
		// User exists
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Email already exists")
}

// Get User By Id godoc
// @Tags User
// @Summary Get User by Id
// @Description Get User by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of User need to be found"
// @Success 200 {object} model.User
// @Header 200 {string} Token "qwerty"
// @Router /User/{id} [get]
func GetUserById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetUserById")
	var (
		collection *mongo.Collection
		user       model.User
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.UserTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err := collection.FindOne(ctx, filter).Decode(&user)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	data := bson.M{"user": user}
	api_helper.SuccessResponse(response, data, "Get User Successfully")
}

// Get number of Users godoc
// @Tags User
// @Summary Get number of Users sort by created_date field descending
// @Description Get number of Users depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.User
// @Header 200 {string} Token "qwerty"
// @Router /User [get]
func GetUsers(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetCurrencies")

	var (
		allUser []model.User
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.UserTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{"is_deleted": bson.M{"$eq": false}}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))

	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var user model.User
		_ = cursor.Decode(&user)
		allUser = append(allUser, user)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"user": allUser}
	api_helper.SuccessResponse(response, data, "Get User Successfully")
}

// Update User godoc
// @Tags User
// @Summary Update User
// @Description Update User
// @Accept  json
// @Produce  json
// @Param id path string true "Id of User need to be found"
// @Param body body model.UserRequest true "Update User"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /User/{id} [put]
func UpdateUser(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var (
		collection *mongo.Collection
		user       model.User
		userReq    model.UserRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.UserTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err = collection.FindOne(ctx, filter).Decode(&user)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = json.NewDecoder(request.Body).Decode(&userReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	if !strfmt.IsEmail(userReq.Email) {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Validate ERROR - "+"Email: '"+userReq.Email+"' is invalid")
		return
	}

	filterEmail := bson.M{
		"email":      bson.M{"$ne": user.Email, "$eq": userReq.Email},
		"is_deleted": false,
	}

	count, err := collection.CountDocuments(ctx, filterEmail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Count Email exists ERROR - "+err.Error())
		return
	}

	if count > 0 {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Cannot update Email. Because Email is already exists")
		return
	}

	err = copier.Copy(&user, &userReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set lai thoi gian luc update
	user.UpdatedDate = time.Now()

	update := bson.M{"$set": user}

	_, err = collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"user": user}
	api_helper.SuccessResponse(response, data, "Update User Successfully")
}

// Soft Delete User godoc
// @Tags User
// @Summary Soft Delete User
// @Description Soft Delete User
// @Accept  json
// @Produce  json
// @Param id path string true "Id of User need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /User/{id} [delete]
func DeleteSoftUser(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.UserTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	api_helper.SuccessResponse(response, id, "Delete User successfully")
}

// User login godoc
// @Tags User
// @Summary User login
// @Description User Login
// @Accept  json
// @Produce  json
// @Param body body model.UserLogin true "Reset User Password"
// @Success 200 {object} model.User
// @Header 200 {string} Token "qwerty"
// @Router /User/login [POST]
func UserLogin(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UserLogin")
	response.Header().Set("content-type", "application/json")
	var (
		user   model.User
		result model.User
	)

	collection := database.Database.Collection(setting.UserTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&user)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}
	if !strfmt.IsEmail(user.Email) {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Email '"+user.Email+"' is invalid")
		return
	}

	filter := bson.D{{"email", user.Email}, {"password", user.Password}, {"is_deleted", false}}
	err := collection.FindOne(ctx, filter).Decode(&result)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Email or Password is invalid")
		return
	}

	//check if user is inactive
	if !result.IsActive {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "User '"+user.Email+"' is inactive now!!!")
		return
	}

	//err = bcrypt.CompareHashAndPassword([]byte(result.Password), []byte(user.Password))
	//if err != nil {
	//	response.WriteHeader(http.StatusInternalServerError)
	//	response.Write([]byte(`{ "message": "Invalid Password" }`))
	//	return
	//}

	if result.Email != "oms@productaddon.com" {
		if err = service.CreateUserLicense(result.Email); err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Create User License ERROR - "+err.Error())
			return
		}
	}

	api_helper.SuccessResponse(response, bson.M{"user": result}, "Login Success")

}

// User login godoc
// @Tags User
// @Summary User logout
// @Description User Logout
// @Accept  json
// @Produce  json
// @Param body body model.UserLogout true "User logout"
// @Success 200 {object} model.User
// @Header 200 {string} Token "qwerty"
// @Router /User/logout [POST]
func UserLogout(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("content-type", "application/json")
	var (
		user   model.UserLogout
		result model.User
	)

	collection := database.Database.Collection(setting.UserTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&user)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}
	if !strfmt.IsEmail(user.Email) {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Email '"+user.Email+"' is invalid")
		return
	}

	filter := bson.D{{"email", user.Email}, {"is_deleted", false}}
	err := collection.FindOne(ctx, filter).Decode(&result)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Email is invalid")
		return
	}

	//check if user is inactive
	//if !result.IsActive {
	//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "User '"+user.Email+"' is inactive now!!!")
	//	return
	//}

	//err = bcrypt.CompareHashAndPassword([]byte(result.Password), []byte(user.Password))
	//if err != nil {
	//	response.WriteHeader(http.StatusInternalServerError)
	//	response.Write([]byte(`{ "message": "Invalid Password" }`))
	//	return
	//}

	if result.Email != "oms@productaddon.com" {
		if err = service.DeleteUserLicense(user.Email); err != nil {
			fmt.Println("Delete User License ERROR - " + err.Error())
		}
	}

	api_helper.SuccessResponse(response, nil, "Logout Successfully")

}

// User Reset password godoc
// @Tags User
// @Summary User reset password
// @Description User reset password
// @Accept  json
// @Produce  json
// @Param body body model.UserResetPassword true "Reset User Password"
// @Success 200 {object} model.User
// @Header 200 {string} Token "qwerty"
// @Router /User/reset_password [POST]
func UserResetPassword(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("content-type", "application/json")
	var (
		user        model.User
		result      model.User
		currentTime = time.Now()
	)

	collection := database.Database.Collection(setting.UserTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&user)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}
	if !strfmt.IsEmail(user.Email) {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Email '"+user.Email+"' is invalid")
		return
	}

	filter := bson.D{{"email", user.Email}, {"is_deleted", false}}
	err := collection.FindOne(ctx, filter).Decode(&result)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Email does not exists")
		return
	}

	//check if user is inactive
	if !result.IsActive {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "User '"+user.Email+"' is inactive now!!!")
		return
	}

	rawPassword := uuid.New().String()[:8]

	newPassword := sHelper.WebEncrypt(rawPassword)

	encryptedRawPassword, err := sHelper.Encrypt(rawPassword, setting.Key)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Encrypted New Password ERROR - "+err.Error())
		return
	}

	update := bson.M{"$set": bson.M{
		"password":        newPassword,
		"change_password": true,
		"updated_date":    currentTime,
	}}

	if _, err = collection.UpdateOne(ctx, filter, update); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Password ERROR - "+err.Error())
		return
	}

	if err = service.InsertUserPassword(user.Email, newPassword); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert User Password ERROR - "+err.Error())
		return
	}

	if err = service.InsertQueue(enum.AppIdOMS, enum.AppIdEmail, "reset_password", user.Email, encryptedRawPassword, "", "", "", false); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert queue for reset password ERROR - "+err.Error())
		return
	}

	//err = bcrypt.CompareHashAndPassword([]byte(result.Password), []byte(user.Password))
	//if err != nil {
	//	response.WriteHeader(http.StatusInternalServerError)
	//	response.Write([]byte(`{ "message": "Invalid Password" }`))
	//	return
	//}

	api_helper.SuccessResponse(response, nil, "Reset Password Successfully")

}

// User Change password godoc
// @Tags User
// @Summary User change password
// @Description User change password
// @Accept  json
// @Produce  json
// @Param body body model.UserChangePassword true "Change User Password"
// @Success 200 {object} model.User
// @Header 200 {string} Token "qwerty"
// @Router /User/change_password [POST]
func UserChangePassword(response http.ResponseWriter, request *http.Request) {
	var (
		user        model.UserChangePassword
		result      model.User
		currentTime = time.Now()
	)
	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.UserTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&user)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}
	if !strfmt.IsEmail(user.Email) {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Email '"+user.Email+"' is invalid")
		return
	}

	filter := bson.D{{"email", user.Email}, {"password", user.CurrentPassword}, {"is_deleted", false}}
	err := collection.FindOne(ctx, filter).Decode(&result)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Email or current Password is invalid")
		return
	}

	//check if user is inactive
	if !result.IsActive {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "User '"+user.Email+"' is inactive now!!!")
		return
	}

	update := bson.M{
		"$set": bson.M{
			"password":        user.NewPassword,
			"change_password": false,
			"updated_date":    currentTime,
		},
	}

	_, err = collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Change password ERROR")
		return
	}

	if err = service.InsertUserPassword(user.Email, user.NewPassword); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert User Password ERROR - "+err.Error())
		return
	}

	result.Password = user.NewPassword
	result.ChangePassword = false
	result.UpdatedDate = currentTime

	//err = bcrypt.CompareHashAndPassword([]byte(result.Password), []byte(user.Password))
	//if err != nil {
	//	response.WriteHeader(http.StatusInternalServerError)
	//	response.Write([]byte(`{ "message": "Invalid Password" }`))
	//	return
	//}

	api_helper.SuccessResponse(response, bson.M{"user": result}, "Change password Success")

}

//--------------------------- Unused -----------------------------------------------------------------------------------
func CreateManyUser(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyUser")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.UserTable)
	response.Header().Set("content-type", "application/json")
	var manyUser []model.User
	err = json.NewDecoder(request.Body).Decode(&manyUser)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, user := range manyUser {
		user.ID = primitive.NewObjectID()
		user.CreatedDate = time.Now()
		user.UpdatedDate = time.Now()
		ui = append(ui, user)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

//DeleteHardUser
/*
func DeleteHardUser(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardUser")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.UserTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
