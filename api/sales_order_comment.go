package api

import (
	"context"
	"encoding/json"
	"net/http"
	"os"
	"time"

	"../setting"
	"github.com/jinzhu/copier"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateSalesOrderComment godoc
// @Tags Sales Order Comment
// @Summary Add Sales Order Comment
// @Description Add Sales Order Comment
// @Accept  json
// @Produce  json
// @Param body body model.SalesOrderCommentRequest true "Add Sales Order Comment"
// @Success 200 {object} model.SalesOrderComment
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderComment [POST]
func CreateSalesOrderComment(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateSalesOrderComment")
	var (
		collection           *mongo.Collection
		salesOrderComment    model.SalesOrderComment
		salesOrderCommentReq model.SalesOrderCommentRequest
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.SalesOrderCommentTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&salesOrderCommentReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Sales Order Comment Input ERROR - "+err.Error())
		return
	}

	_ = copier.Copy(&salesOrderComment, &salesOrderCommentReq)

	currentTime := time.Now()
	salesOrderComment.ID = primitive.NewObjectIDFromTimestamp(currentTime)
	salesOrderComment.ObjectType = setting.SalesOrderCommentTable
	salesOrderComment.CreatedDate = currentTime
	salesOrderComment.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, salesOrderComment)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Sales Order Comment ERROR - "+err.Error())
		return
	}

	data := bson.M{"sales_order_comment": salesOrderComment}
	api_helper.SuccessResponse(response, data, "Create Sales Order Comment successfully")
}

// CreateManySalesOrderComment func
func CreateManySalesOrderComment(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManySalesOrderComment")
	var (
		collection         *mongo.Collection
		salesOrderComments []model.SalesOrderComment
	)
	collection = database.Database.Collection(setting.SalesOrderCommentTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&salesOrderComments)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	currentTime := time.Now()
	for _, salesOrderComment := range salesOrderComments {
		salesOrderComment.ID = primitive.NewObjectID()
		salesOrderComment.CreatedDate = currentTime
		salesOrderComment.UpdatedDate = currentTime
		ui = append(ui, salesOrderComment)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// GetSalesOrderCommentByID godoc
// @Tags Sales Order Comment
// @Summary Get Sales Order Comment by Id
// @Description Get Sales Order Comment by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Order Comment need to be found"
// @Success 200 {object} model.SalesOrderComment
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderComment/{id} [get]
func GetSalesOrderCommentByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSaleOrderCommentByID")
	var (
		collection        *mongo.Collection
		salesOrderComment model.SalesOrderComment
	)
	collection = database.Database.Collection(setting.SalesOrderCommentTable)

	response.Header().Set("content-type", "application/json")

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	err := collection.FindOne(ctx, filter).Decode(&salesOrderComment)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesOrderComment)
}

// GetSalesOrderComments godoc
// @Tags Sales Order Comment
// @Summary Get all Sales Order Comment
// @Description Get all Sales Order Comment
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.SalesOrderComment
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderComment [get]
func GetSalesOrderComments(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesOrderComments")
	var (
		collection         *mongo.Collection
		salesOrderComments []model.SalesOrderComment
		findOptions        = options.Find()
		filter             = bson.M{}
	)
	collection = database.Database.Collection(setting.SalesOrderCommentTable)

	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions) //findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var saleOrderComment model.SalesOrderComment
		cursor.Decode(&saleOrderComment)
		salesOrderComments = append(salesOrderComments, saleOrderComment)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesOrderComments)
}

// UpdateSalesOrderComment godoc
// @Tags Sales Order Comment
// @Summary Update Sales Order Comment
// @Description Update Sales Order Comment
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Order need to be found"
// @Param body body model.SalesOrderRequest true "Update Sales Order Comment"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderComment/{id} [put]
func UpdateSalesOrderComment(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateSaleOrderComment")
	var (
		collection       *mongo.Collection
		saleOrderComment model.SalesOrderComment
	)
	collection = database.Database.Collection(setting.SalesOrderCommentTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&saleOrderComment)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	saleOrderComment.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": saleOrderComment}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

func DeleteSoftSalesOrderComment(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftSalesOrderComment")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.SalesOrderCommentTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
