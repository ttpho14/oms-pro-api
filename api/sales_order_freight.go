package api

import (
	"../setting"
	"context"
	"encoding/json"
	"net/http"
	"os"
	"time"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateSalesOrderFreight godoc
// @Tags Sales Order Freight
// @Summary Add Sales Order Freight
// @Description Add Sales Order Freight
// @Accept  json
// @Produce  json
// @Param body body []model.SalesOrderFreightRequest true "Add Sales Order Freight"
// @Success 200 {object} []model.SalesOrderFreight
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderFreight [POST]
func CreateSalesOrderFreight(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateSalesOrderFreight")
	var (
		collection        *mongo.Collection
		salesOrderFreight model.SalesOrderFreight
	)

	collection = database.Database.Collection(setting.SalesOrderFreightTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&salesOrderFreight)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	currentTime := time.Now()
	salesOrderFreight.CreatedDate = currentTime
	salesOrderFreight.UpdatedDate = currentTime

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	result, err := collection.InsertOne(ctx, salesOrderFreight)
	if err != nil {
		json.NewEncoder(response).Encode(err.Error())
		return
	}
	id := result.InsertedID
	salesOrderFreight.ID = id.(primitive.ObjectID)
	json.NewEncoder(response).Encode(salesOrderFreight)
}

// CreateManySalesOrderFreight func
func CreateManySalesOrderFreight(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManySalesOrderFreight")
	var (
		collection         *mongo.Collection
		salesOrderFreights []model.SalesOrderFreight
	)
	collection = database.Database.Collection(setting.SalesOrderFreightTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&salesOrderFreights)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	currentTime := time.Now()
	for _, salesOrderFreight := range salesOrderFreights {
		salesOrderFreight.ID = primitive.NewObjectID()
		salesOrderFreight.CreatedDate = currentTime
		salesOrderFreight.UpdatedDate = currentTime
		ui = append(ui, salesOrderFreight)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// GetSalesOrderFreightByID godoc
// @Tags Sales Order Freight
// @Summary Get Sales Order Freight by Id
// @Description Get Sales Order Freight by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Order Freight need to be found"
// @Success 200 {object} model.SalesOrderFreight
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderFreight/{id} [get]
func GetSalesOrderFreightByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSaleOrderFreightByID")
	var (
		collection        *mongo.Collection
		salesOrderFreight model.SalesOrderFreight
	)
	collection = database.Database.Collection(setting.SalesOrderFreightTable)

	response.Header().Set("content-type", "application/json")

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	err := collection.FindOne(ctx, filter).Decode(&salesOrderFreight)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesOrderFreight)
}

// GetSalesOrderFreights godoc
// @Tags Sales Order Freight
// @Summary Get all Sales Order Freight
// @Description Get all Sales Order Freight
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.SalesOrderFreight
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderFreight [get]
func GetSalesOrderFreights(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesOrderFreights")
	var (
		collection         *mongo.Collection
		salesOrderFreights []model.SalesOrderFreight
		findOptions        = options.Find()
		filter             = bson.M{}
	)
	collection = database.Database.Collection(setting.SalesOrderFreightTable)

	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions) //findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var saleOrderFreight model.SalesOrderFreight
		cursor.Decode(&saleOrderFreight)
		salesOrderFreights = append(salesOrderFreights, saleOrderFreight)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesOrderFreights)
}

// UpdateSalesOrderFreight godoc
// @Tags Sales Order Freight
// @Summary Update Sales Order Freight
// @Description Update Sales Order Freight
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Order need to be found"
// @Param body body model.SalesOrderFreightRequest true "Update Sales Order Freight"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderFreight/{id} [put]
func UpdateSalesOrderFreight(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateSaleOrderFreight")
	var (
		collection       *mongo.Collection
		saleOrderFreight model.SalesOrderFreight
	)
	collection = database.Database.Collection(setting.SalesOrderFreightTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&saleOrderFreight)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	saleOrderFreight.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": saleOrderFreight}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

func DeleteSoftSalesOrderFreight(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftSalesOrderFreight")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.SalesOrderFreightTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
