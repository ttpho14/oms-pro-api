package api

import (
	"../database"
	"../model"
	"../setting"
	"./api_helper"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"net/http"
	"os"
	"reflect"
	"time"
)

// Add UserBrandDetail godoc
// @Tags User Brand Detail
// @Summary Add UserBrandDetail
// @Description Add UserBrandDetail
// @Accept  json
// @Produce  json
// @Param body body []model.AddUserBrandDetail true "Add UserBrandDetail"
// @Success 200 {object} []model.UserBrandDetail
// @Header 200 {string} Token "qwerty"
// @Router /UserBrandDetail [POST]
func CreateUserBrandDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateUserBrandDetail")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.UserBrandDetailTable)
	response.Header().Set("content-type", "application/json")
	var userBrandDetail model.UserBrandDetail
	err = json.NewDecoder(request.Body).Decode(&userBrandDetail)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	//Set Date = time.Now()
	userBrandDetail.CreatedDate = time.Now()
	userBrandDetail.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	result, err := collection.InsertOne(ctx, userBrandDetail)
	if err != nil {
		json.NewEncoder(response).Encode(err.Error())
		return
	}
	id := result.InsertedID
	userBrandDetail.ID = id.(primitive.ObjectID)
	json.NewEncoder(response).Encode(userBrandDetail)
}

func CreateManyUserBrandDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyUserBrandDetail")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.UserBrandDetailTable)
	response.Header().Set("content-type", "application/json")
	var manyUserBrandDetail []model.UserBrandDetail
	err = json.NewDecoder(request.Body).Decode(&manyUserBrandDetail)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, userBrandDetail := range manyUserBrandDetail {
		userBrandDetail.ID = primitive.NewObjectID()
		userBrandDetail.CreatedDate = time.Now()
		userBrandDetail.UpdatedDate = time.Now()
		ui = append(ui, userBrandDetail)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// Get UserBrandDetail By Id godoc
// @Tags User Brand Detail
// @Summary Get UserBrandDetail by Id
// @Description Get UserBrandDetail by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of UserBrandDetail need to be found"
// @Success 200 {object} model.UserBrandDetail
// @Header 200 {string} Token "qwerty"
// @Router /UserBrandDetail/{id} [get]
func GetUserBrandDetailById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetUserBrandDetailById")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.UserBrandDetailTable)
	response.Header().Set("content-type", "application/json")
	var userBrandDetail model.UserBrandDetail
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//_id = id and is_deleted = false
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}, "is_deleted": bson.M{"$eq": false}}
	err := collection.FindOne(ctx, filter).Decode(&userBrandDetail)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(userBrandDetail)
}

// Get number of User Brand Details godoc
// @Tags User Brand Detail
// @Summary Get number of User Brand Details sort by created_date field descending
// @Description Get number of User Brand Details depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.UserBrandDetail
// @Header 200 {string} Token "qwerty"
// @Router /UserBrandDetail [get]
func GetUserBrandDetails(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetUserBrandDetails")
	//Get Limit and Page
	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	collection := database.Database.Collection(setting.UserBrandDetailTable)
	response.Header().Set("content-type", "application/json")
	var allUserBrandDetail []model.UserBrandDetail
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"is_deleted": bson.M{"$eq": false}}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var userBrandDetail model.UserBrandDetail
		cursor.Decode(&userBrandDetail)
		allUserBrandDetail = append(allUserBrandDetail, userBrandDetail)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(allUserBrandDetail)
}

// Update UserBrandDetail godoc
// @Tags User Brand Detail
// @Summary Update UserBrandDetail
// @Description Update UserBrandDetail
// @Accept  json
// @Produce  json
// @Param id path string true "Id of UserBrandDetail need to be found"
// @Param body body model.AddUserBrandDetail true "Update UserBrandDetail"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /UserBrandDetail/{id} [put]
func UpdateUserBrandDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "UpdateUserBrandDetail")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.UserBrandDetailTable)
	response.Header().Set("content-type", "application/json")
	var userBrandDetail model.UserBrandDetail
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	err = json.NewDecoder(request.Body).Decode(&userBrandDetail)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	//Set lai thoi gian luc update
	userBrandDetail.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	fmt.Println("filter UserBrandDetail", filter)
	update := bson.M{"$set": userBrandDetail}
	result, err := collection.UpdateOne(ctx, filter, update)
	fmt.Println("Result UserBrandDetail", result)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

// Soft Delete UserBrandDetail godoc
// @Tags User Brand Detail
// @Summary Soft Delete UserBrandDetail
// @Description Soft Delete UserBrandDetail
// @Accept  json
// @Produce  json
// @Param id path string true "Id of UserBrandDetail need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /UserBrandDetail/{id} [delete]
func DeleteSoftUserBrandDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteSoftUserBrandDetail")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.UserBrandDetailTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("UpdateOne() result:", result)
		fmt.Println("UpdateOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("UpdateOne() result MatchedCount:", result.MatchedCount)
		fmt.Println("UpdateOne() result ModifiedCount:", result.ModifiedCount)
		fmt.Println("UpdateOne() result UpsertedCount:", result.UpsertedCount)
		fmt.Println("UpdateOne() result UpsertedID:", result.UpsertedID)
	}
	json.NewEncoder(response).Encode(result)
}

//DeleteHardUserBrandDetail
/*
func DeleteHardUserBrandDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardUserBrandDetail")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.UserBrandDetailTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
//-------------------- REGION API - END -----------------------
