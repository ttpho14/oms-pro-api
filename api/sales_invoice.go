package api

import (
	"../setting"
	"context"
	"encoding/json"
	"net/http"
	"os"
	"time"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateSalesInvoice godoc
// @Tags Sales Invoice
// @Summary Add Sales Invoice
// @Description Add Sales Invoice
// @Accept  json
// @Produce  json
// @Param body body []model.AddSalesInvoice true "Add Sales Invoice"
// @Success 200 {object} []model.SalesInvoice
// @Header 200 {string} Token "qwerty"
// @Router /SalesInvoice [POST]
func CreateSalesInvoice(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateSalesInvoice")
	var (
		collection            *mongo.Collection
		salesInvoiceRequest   model.AddSalesInvoice
		salesInvoice          model.SalesInvoice
		uiSalesInvoiceDetail  []interface{}
		uiSalesInvoiceFreight []interface{}
	)

	collection = database.Database.Collection(setting.SalesInvoiceTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&salesInvoice)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	copier.Copy(&salesInvoice, &salesInvoiceRequest)
	currentTime := time.Now()
	salesInvoice.CreatedDate = currentTime
	salesInvoice.UpdatedDate = currentTime
	salesInvoice.ID = primitive.NewObjectID()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	result, err := collection.InsertOne(ctx, salesInvoice)
	if err != nil {
		json.NewEncoder(response).Encode(err.Error())
		return
	}

	id := result.InsertedID
	salesInvoice.ID = id.(primitive.ObjectID)
	// Sales Invoice Detail
	for _, detail := range salesInvoiceRequest.AddSalesInvoiceDetail {
		var (
			salesInvoiceDetail model.SalesInvoiceDetail
		)
		copier.Copy(&salesInvoiceDetail, &detail)
		salesInvoiceDetail.DocKey = salesInvoice.ID
		salesInvoiceDetail.ID = primitive.NewObjectID()
		salesInvoiceDetail.CreatedDate = currentTime
		salesInvoiceDetail.UpdatedDate = currentTime
		uiSalesInvoiceDetail = append(uiSalesInvoiceDetail, salesInvoiceDetail)
	}

	// Sales Invoice Freight
	for _, freight := range salesInvoiceRequest.AddSalesInvoiceFreight {
		var (
			salesInvoiceFreight model.SalesInvoiceFreight
		)
		copier.Copy(&salesInvoiceFreight, &freight)
		salesInvoiceFreight.DocKey = salesInvoice.ID
		salesInvoiceFreight.ID = primitive.NewObjectID()
		salesInvoiceFreight.CreatedDate = currentTime
		salesInvoiceFreight.UpdatedDate = currentTime
		uiSalesInvoiceFreight = append(uiSalesInvoiceFreight, salesInvoiceFreight)
	}

	json.NewEncoder(response).Encode(salesInvoice)
}

// CreateManySalesInvoice func
func CreateManySalesInvoice(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManySalesInvoice")
	var (
		collection            *mongo.Collection
		salesInvoicesRequest  []model.AddSalesInvoice
		uiSalesInvoice        []interface{}
		uiSalesInvoiceDetail  []interface{}
		uiSalesInvoiceFreight []interface{}
	)

	collection = database.Database.Collection(setting.SalesInvoiceTable)
	response.Header().Set("content-type", "application/json")

	err = json.NewDecoder(request.Body).Decode(&salesInvoicesRequest)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	currentTime := time.Now()
	for _, salesInvoiceRequest := range salesInvoicesRequest {
		var (
			salesInvoice model.SalesInvoice
		)
		copier.Copy(&salesInvoice, &salesInvoiceRequest)

		salesInvoice.ID = primitive.NewObjectID()
		salesInvoice.CreatedDate = currentTime
		salesInvoice.UpdatedDate = currentTime
		uiSalesInvoice = append(uiSalesInvoice, salesInvoice)

		// Sales Invoice Detail
		for _, detail := range salesInvoiceRequest.AddSalesInvoiceDetail {
			var (
				salesInvoiceDetail model.SalesInvoiceDetail
			)
			copier.Copy(&salesInvoiceDetail, &detail)
			salesInvoiceDetail.DocKey = salesInvoice.ID
			salesInvoiceDetail.ID = primitive.NewObjectID()
			salesInvoiceDetail.CreatedDate = currentTime
			salesInvoiceDetail.UpdatedDate = currentTime
			uiSalesInvoiceDetail = append(uiSalesInvoiceDetail, salesInvoiceDetail)
		}

		// Sales Invoice Freight
		for _, freight := range salesInvoiceRequest.AddSalesInvoiceFreight {
			var (
				salesInvoiceFreight model.SalesInvoiceFreight
			)
			copier.Copy(&salesInvoiceFreight, &freight)
			salesInvoiceFreight.DocKey = salesInvoice.ID
			salesInvoiceFreight.ID = primitive.NewObjectID()
			salesInvoiceFreight.CreatedDate = currentTime
			salesInvoiceFreight.UpdatedDate = currentTime
			uiSalesInvoiceFreight = append(uiSalesInvoiceFreight, salesInvoiceFreight)
		}
	}

	// Sales Invoice
	_, err = collection.InsertMany(context.TODO(), uiSalesInvoice)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	if len(uiSalesInvoiceDetail) > 0 {
		collection = database.Database.Collection(setting.SalesInvoiceDetailTable)
		// Sales Invoice Detail
		_, err = collection.InsertMany(context.TODO(), uiSalesInvoiceDetail)
		if err != nil {
			response.WriteHeader(http.StatusInternalServerError)
			response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
			return
		}
	} else {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + "Please enter at least one line for order detail." + `" }`))
		return
	}

	if len(uiSalesInvoiceFreight) > 0 {
		collection = database.Database.Collection(setting.SalesInvoiceFreightTable)
		// Sales Invoice Freight
		_, err = collection.InsertMany(context.TODO(), uiSalesInvoiceFreight)
		if err != nil {
			response.WriteHeader(http.StatusInternalServerError)
			response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
			return
		}
	}
	json.NewEncoder(response).Encode(map[string]interface{}{
		"sales_invoice":         uiSalesInvoice,
		"sales_invoice_detail":  uiSalesInvoiceDetail,
		"sales_invoice_freight": uiSalesInvoiceFreight,
	})
}

// GetSalesInvoiceByID godoc
// @Tags Sales Invoice
// @Summary Get Sales Invoice by Id
// @Description Get Sales Invoice by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Invoice need to be found"
// @Success 200 {object} model.SalesInvoice
// @Header 200 {string} Token "qwerty"
// @Router /SalesInvoice/{id} [get]
func GetSalesInvoiceByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSaleOrderByID")
	var (
		collection   *mongo.Collection
		SalesInvoice model.AddSalesInvoice
	)
	collection = database.Database.Collection(setting.SalesInvoiceTable)

	response.Header().Set("content-type", "application/json")

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}

	err := collection.FindOne(ctx, filter).Decode(&SalesInvoice)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	filterChild := bson.M{"doc_key": bson.M{"$eq": bsonx.ObjectID(id)}}

	// Find Detail
	collection = database.Database.Collection(setting.SalesInvoiceDetailTable)
	var details []model.AddSalesInvoiceDetail

	cursor, err := collection.Find(ctx, filterChild)
	err = cursor.All(ctx, &details)
	SalesInvoice.AddSalesInvoiceDetail = details

	// Find Freight
	collection = database.Database.Collection(setting.SalesInvoiceFreightTable)
	var freight []model.AddSalesInvoiceFreight

	cursor, err = collection.Find(ctx, filterChild)
	err = cursor.All(ctx, &freight)
	SalesInvoice.AddSalesInvoiceFreight = freight

	json.NewEncoder(response).Encode(SalesInvoice)
}

// GetSalesInvoices godoc
// @Tags Sales Invoice
// @Summary Get all Sales Invoice
// @Description Get all Sales Invoice
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.SalesInvoice
// @Header 200 {string} Token "qwerty"
// @Router /SalesInvoice [get]
func GetSalesInvoices(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesInvoices")
	var (
		collection    *mongo.Collection
		SalesInvoices []model.SalesInvoice
		findOptions   = options.Find()
		filter        = bson.M{}
	)
	collection = database.Database.Collection(setting.SalesInvoiceTable)

	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))

	cursor, err := collection.Find(ctx, filter, findOptions) //findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var saleOrder model.SalesInvoice
		cursor.Decode(&saleOrder)
		SalesInvoices = append(SalesInvoices, saleOrder)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(SalesInvoices)
}

// UpdateSalesInvoice godoc
// @Tags Sales Invoice
// @Summary Update Sales Invoice
// @Description Update Sales Invoice
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Invoice need to be found"
// @Param body body model.AddSalesInvoice true "Update Sales Invoice"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /SalesInvoice/{id} [put]
func UpdateSalesInvoice(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateSaleOrder")
	var (
		collection            *mongo.Collection
		salesInvoiceRequest   model.AddSalesInvoice
		salesInvoice          model.SalesInvoice
		uiSalesInvoiceDetail  []interface{}
		uiSalesInvoiceFreight []interface{}
	)
	collection = database.Database.Collection(setting.SalesInvoiceTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&salesInvoiceRequest)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	copier.Copy(&salesInvoice, &salesInvoiceRequest)
	currentTime := time.Now()
	salesInvoice.UpdatedDate = currentTime

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": salesInvoice}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	// Sales Invoice Detail
	if len(salesInvoiceRequest.AddSalesInvoiceDetail) > 0 {
		collection = database.Database.Collection(setting.SalesOrderDetailTable)

		for _, detail := range salesInvoiceRequest.AddSalesInvoiceDetail {
			var (
				salesInvoiceDetail model.SalesInvoiceDetail
			)
			copier.Copy(&salesInvoiceDetail, &detail)
			salesInvoiceDetail.UpdatedDate = currentTime

			if salesInvoiceDetail.ID != primitive.NilObjectID {
				filter = bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(salesInvoiceDetail.ID)}}
				update = bson.M{"$set": salesInvoiceDetail}
				result, err = collection.UpdateOne(ctx, filter, update)
				if err != nil {
					response.WriteHeader(http.StatusInternalServerError)
					response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
					return
				}
			} else {
				uiSalesInvoiceDetail = append(uiSalesInvoiceDetail, salesInvoiceDetail)
			}
		}

		// Insert New
		if len(uiSalesInvoiceDetail) > 0 {
			collection = database.Database.Collection(setting.SalesOrderDetailTable)
			// Sales Invoice Detail
			_, err = collection.InsertMany(context.TODO(), uiSalesInvoiceDetail)
			if err != nil {
				response.WriteHeader(http.StatusInternalServerError)
				response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
				return
			}
		}
	}

	// Sales Invoice Freight
	if len(salesInvoiceRequest.AddSalesInvoiceFreight) > 0 {
		collection = database.Database.Collection(setting.SalesOrderFreightTable)

		for _, freight := range salesInvoiceRequest.AddSalesInvoiceFreight {
			var (
				salesInvoiceFreight model.SalesInvoiceFreight
			)
			copier.Copy(&salesInvoiceFreight, &freight)
			salesInvoiceFreight.UpdatedDate = currentTime

			if salesInvoiceFreight.ID != primitive.NilObjectID {
				filter = bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(salesInvoiceFreight.ID)}}
				update = bson.M{"$set": salesInvoiceFreight}
				result, err = collection.UpdateOne(ctx, filter, update)
				if err != nil {
					response.WriteHeader(http.StatusInternalServerError)
					response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
					return
				}
			} else {
				uiSalesInvoiceFreight = append(uiSalesInvoiceFreight, salesInvoiceFreight)
			}
		}

		// Insert New
		if len(uiSalesInvoiceFreight) > 0 {
			collection = database.Database.Collection(setting.SalesOrderFreightTable)
			_, err = collection.InsertMany(context.TODO(), uiSalesInvoiceFreight)
			if err != nil {
				response.WriteHeader(http.StatusInternalServerError)
				response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
				return
			}
		}
	}

	json.NewEncoder(response).Encode(result)
}

func DeleteSoftSalesInvoice(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftSalesInvoice")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.SalesInvoiceTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
