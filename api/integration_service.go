package api

import (
	"encoding/json"
	"fmt"
	"net/http"

	"../model"
	"../model/enum"
	"../service"
	help "./api_helper"
	"go.mongodb.org/mongo-driver/bson"
)

// Make command for Integration Service godoc
// @Tags IntegrationService
// @Summary Make command for Integration Service
// @Description Make command for Integration Service
// @Accept  json
// @Produce  json
// @Param body body model.IntegrationServiceRequest true "Update Queue"
// @Success 200 {object} model.IntegrationService
// @Header 200 {string} Token "qwerty"
// @Router /IntegrationService/command [POST]
func IntegrationServiceCommand(response http.ResponseWriter, request *http.Request) {
	defer help.RecoverError(response, "GetServiceStatus")

	var serviceReq model.IntegrationServiceRequest

	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&serviceReq)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	serviceName := string(serviceReq.ServiceName)

	if serviceReq.ServiceName == enum.IntegrationServiceMongo {
		if err = service.IntegrationServiceCommand("service", serviceName, serviceReq.Command); err != nil {
			help.ErrorResponse(response, http.StatusInternalServerError, 0, fmt.Sprintf("Execute command [%s] for [%s] service ERROR - %s", serviceReq.Command, serviceName, err.Error()))
			return
		}
	} else {
		if err = service.IntegrationServiceCommand("systemctl", serviceReq.Command, serviceName); err != nil {
			help.ErrorResponse(response, http.StatusInternalServerError, 0, fmt.Sprintf("Execute command [%s] for [%s] service ERROR - %s", serviceReq.Command, serviceName, err.Error()))
			return
		}
	}

	help.SuccessResponse(response, nil, "Make System command success")
}

// Get List of Integration Service status  godoc
// @Tags IntegrationService
// @Summary Get List of Integration Service
// @Description Get List of Integration Service
// @Accept  json
// @Produce  json
// @Success 200 {object} model.IntegrationService
// @Header 200 {string} Token "qwerty"
// @Router /IntegrationService [GET]
func GetIntegrationServiceInfo(response http.ResponseWriter, request *http.Request) {
	defer help.RecoverError(response, "GetServiceStatus")

	response.Header().Set("content-type", "application/json")

	services, err := service.GetAllIntegrationServiceInfo()
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Get All Service Info ERROR - "+err.Error())
		return
	}

	data := bson.M{"integration_service": services}
	help.SuccessResponse(response, data, "Get Services status successfully")
}
