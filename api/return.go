package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"../database"
	"../model"
	"../model/enum"
	"../service"
	"../service/helper"
	sHelper "../service/helper"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateReturn godoc
// @Tags Return
// @Summary Add Return
// @Description Add Return
// @Accept  json
// @Produce  json
// @Param body body model.ReturnRequest true "Add Return"
// @Success 200 {object} model.Return
// @Header 200 {string} Token "qwerty"
// @Router /Return [POST]
func CreateReturn(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateReturn")
	var (
		collection           *mongo.Collection
		returnObj            model.Return
		returnResponse       model.ReturnResponse
		returnRequest        model.ReturnRequest
		salesOrder           model.SalesOrder
		currentTime          = time.Now()
		salesOrderDetailColl = database.Database.Collection(setting.SalesOrderDetailTable)
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReturnTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&returnRequest)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = returnRequest.Validation()
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Input Validation ERROR - "+err.Error())
		return
	}

	_ = database.Database.Collection(setting.SalesOrderTable).FindOne(ctx, bson.M{"doc_num": returnRequest.BaseDocNum}).Decode(&salesOrder)

	_ = copier.Copy(&returnObj, &salesOrder)

	_ = copier.Copy(&returnObj, &returnRequest)

	count, err := database.Database.Collection(setting.ReturnTable).CountDocuments(ctx, bson.M{"base_doc_num": returnObj.BaseDocNum})
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Count Return document ERROR - "+err.Error())
		return
	}

	returnObj.ID = primitive.NewObjectID()
	//Create SID for SAP Integration
	time.Sleep(time.Second * 2)
	salesOrder.SID = fmt.Sprintf("%v", time.Now().Unix())
	returnObj.DocNum = fmt.Sprintf("%s_RC%02d", returnRequest.BaseDocNum, count+1)
	returnObj.ObjectType = setting.ReturnTable
	returnObj.BaseDocKey = salesOrder.ID
	returnObj.OrderDate = salesOrder.DocDate
	returnObj.DocDate = currentTime
	if returnObj.ReasonKey == primitive.NilObjectID {
		returnObj.ReasonKey, _ = helper.GetKeyFromId(setting.ReasonTable, "reason_id", returnObj.ReasonId)
	}
	//returnObj.SiteId = salesOrder.SiteId
	//returnObj.AccountCode = salesOrder.AccountCode
	returnObj.DocStatus = enum.ReturnStatusReturned
	//returnObj.BillingAddress = salesOrder.BillingAddress
	//returnObj.BillingAddressKey = salesOrder.BillingAddressKey
	//returnObj.ShippingAddress = salesOrder.ShippingAddress
	//returnObj.ShippingAddressKey = salesOrder.ShippingAddressKey
	//returnObj.Email = salesOrder.Email
	returnObj.CreatedDate = currentTime
	returnObj.UpdatedDate = currentTime

	_, err = collection.InsertOne(ctx, returnObj)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Return Order ERROR - "+err.Error())
		return
	}

	returnObj.TotalAfterTax = 0

	// Return Detail
	if len(returnRequest.ReturnDetailRequest) > 0 {
		for _, detail := range returnRequest.ReturnDetailRequest {
			var (
				returnDetail         model.ReturnDetail
				returnDetailResponse model.ReturnDetailResponse
				inventoryStatus      model.InventoryStatus
				salesOrderDetail     model.SalesOrderDetail
			)
			returnDetailColl := database.Database.Collection(setting.ReturnDetailTable)
			inventoryStatusColl := database.Database.Collection(setting.InventoryStatusTable)

			salesOrderDetailFilter := bson.M{"doc_num": returnRequest.BaseDocNum, "doc_line_num": detail.BaseDocLineNum}
			_ = salesOrderDetailColl.FindOne(ctx, salesOrderDetailFilter).Decode(&salesOrderDetail)

			detailCount, err := returnDetailColl.CountDocuments(ctx, bson.M{"base_doc_num": returnObj.BaseDocNum, "base_doc_line_num": salesOrderDetail.DocLineNum})
			if err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Count Return Detail ERROR - "+err.Error())
				return
			}

			_ = copier.Copy(&returnDetail, &salesOrderDetail)

			_ = copier.Copy(&returnDetail, &detail)

			returnDetail.ID = primitive.NewObjectID()
			returnDetail.DocKey = returnObj.ID
			returnDetail.DocNum = returnObj.DocNum
			returnDetail.DocLineNum = fmt.Sprintf("%s_RC%02d", salesOrderDetail.DocLineNum, detailCount+1)
			returnDetail.BaseDocKey = salesOrder.ID
			returnDetail.DocLineStatus = enum.ReturnDetailStatusReturned
			returnDetail.BaseDocNum = returnObj.BaseDocNum
			returnDetail.BaseDocLineKey = salesOrderDetail.ID
			returnDetail.BaseDocLineNum = salesOrderDetail.DocLineNum
			returnDetail.ObjectType = setting.ReturnDetailTable
			returnDetail.OrderQuantity = salesOrderDetail.Quantity
			//returnDetail.ProductKey = salesOrderDetail.ProductKey
			//returnDetail.ProductCode = salesOrderDetail.ProductCode
			//returnDetail.ProductColor = salesOrderDetail.ProductColor
			//returnDetail.ProductSize = salesOrderDetail.ProductSize
			returnDetail.CreatedDate = currentTime
			returnDetail.UpdatedDate = currentTime

			// Insert Order Cancellation Detail
			if _, err := returnDetailColl.InsertOne(ctx, returnDetail); err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Return Order Detail ERROR - "+err.Error())
				return
			}

			returnObj.TotalAfterTax += returnDetail.PriceAfterTax*returnDetail.Quantity + returnDetail.FreightAmount - returnDetail.LineDiscountAmount

			// Update Sales Order detail
			salesOrderDetail.ReturnQuantity = salesOrderDetail.ReturnQuantity + returnDetail.Quantity
			salesOrderDetail.UpdatedDate = currentTime
			salesOrderDetailUpdate := bson.M{"$set": salesOrderDetail}
			if _, err := salesOrderDetailColl.UpdateOne(ctx, salesOrderDetailFilter, salesOrderDetailUpdate); err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Sales Order Detail ERROR - "+err.Error())
				return
			}

			// Get Inventory Status and Update OnOrder >>>>> Update On Available Quantity
			inventoryFilter := bson.M{"product_id": returnDetail.ProductCode, "site_id": salesOrder.SiteId}
			if err = inventoryStatusColl.FindOne(ctx, inventoryFilter).Decode(&inventoryStatus); err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Product Code and Site Id in Inventory Status ERROR - "+err.Error())
				return
			}

			inventoryStatus.OnOrderQty = inventoryStatus.OnOrderQty - returnDetail.Quantity
			inventoryStatus.OnAvailableQty = inventoryStatus.OnHandQty - inventoryStatus.OnOrderQty
			inventoryStatus.UpdatedDate = currentTime

			inventoryStatusUpdate := bson.M{"$set": inventoryStatus}
			if _, err = inventoryStatusColl.UpdateOne(ctx, inventoryFilter, inventoryStatusUpdate); err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Inventory Status ERROR - "+err.Error())
				return
			}

			if salesOrderDetail.ReturnQuantity == salesOrderDetail.Quantity {
				if err = service.HandleSalesOrderDetailStatusChange(salesOrderDetail.DocNum, salesOrderDetail.DocLineNum, enum.OrderLineStatusReturned); err != nil {
					api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Handle Sales Order Detail Status change ERROR - "+err.Error())
					return
				}
			}

			_ = copier.Copy(&returnDetailResponse, &returnDetail)

			returnResponse.ReturnDetail = append(returnResponse.ReturnDetail, returnDetailResponse)
		}
	} else {
		//Do roll back here
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Please enter at least one line for detail.")
		return
	}

	//returnObj.TotalAfterTax = returnObj.TotalAfterTax + returnObj.FreightAmount + returnObj.TaxAmount - returnObj.DiscountAmount
	returnObj.TotalAfterTax = returnObj.TotalAfterTax - returnObj.DiscountAmount

	_ = copier.Copy(&returnResponse, &returnObj)

	if _, err = collection.UpdateOne(ctx, bson.M{"doc_num": returnObj.DocNum}, bson.M{"$set": bson.M{"total_after_tax": returnObj.TotalAfterTax}}); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Order Cancellation Total after tax ERROR - "+err.Error())
		return
	}

	countLine, _ := salesOrderDetailColl.CountDocuments(ctx, bson.M{"doc_num": salesOrder.DocNum})
	countLineStatus, _ := salesOrderDetailColl.CountDocuments(ctx, bson.M{"doc_num": salesOrder.DocNum, "doc_line_status": enum.OrderLineStatusReturned})

	if countLine == countLineStatus {
		_, err = service.HandleOrderStatusChange(salesOrder.DocNum, enum.OrderStatusReturned, false)
	}

	// Copy Sales Order Payment to Return Payment
	var salesOrderPayments []model.SalesOrderPayment
	salesOrderPaymentCursor, err := database.Database.Collection(setting.SalesOrderPaymentTable).Find(ctx, bson.M{"doc_key": bsonx.ObjectID(salesOrder.ID)})
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Base Sales Order Payment ERROR - "+err.Error())
		return
	}

	if err = salesOrderPaymentCursor.All(ctx, &salesOrderPayments); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Base Sales Order Payment ERROR - "+err.Error())
		return
	}

	for _, salesOrderPayment := range salesOrderPayments {
		var (
			returnPayment         model.ReturnPayment
			returnPaymentResponse model.ReturnPaymentResponse
			returnPaymentColl     = database.Database.Collection(setting.ReturnPaymentTable)
		)
		err = copier.Copy(&returnPayment, &salesOrderPayment)
		if err != nil {
			fmt.Println("abc", err.Error())
		} else {
			fmt.Println("xyz", returnPayment.Amount)
			fmt.Println("abasd", salesOrderPayment.Amount)

		}
		returnPayment.ID = primitive.NewObjectID()
		returnPayment.DocKey = returnObj.ID
		returnPayment.CreatedDate = currentTime
		returnPayment.UpdatedDate = currentTime

		//if enum.PaymentStatus(returnObj.PaymentStatus).ToCorrectCase() == enum.PaymentStatusRefunded {
		//	returnPayment.Amount = returnObj.TotalAfterTax
		//} else {
		//	returnPayment.Amount = 0
		//}

		if _, err := returnPaymentColl.InsertOne(ctx, returnPayment); err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Return Order Payment ERROR - "+err.Error())
			return
		}

		_ = copier.Copy(&returnPaymentResponse, &returnPayment)
		returnResponse.ReturnPayment = append(returnResponse.ReturnPayment, returnPaymentResponse)
	}

	// Return Payment
	/*
		for _, payment := range returnRequest.ReturnPaymentRequest {
			var (
				returnPayment         model.ReturnPayment
				returnPaymentResponse model.ReturnPaymentResponse
				returnPaymentColl     = database.Database.Collection(setting.ReturnPaymentTable)
			)
			copier.Copy(&returnPayment, &payment)
			returnPayment.ID = primitive.NewObjectID()
			returnPayment.DocKey = returnObj.ID
			returnPayment.CreatedDate = currentTime
			returnPayment.UpdatedDate = currentTime

			if enum.PaymentStatus(returnObj.PaymentStatus).ToCorrectCase() == enum.PaymentStatusRefunded {
				returnPayment.Amount = returnObj.TotalAfterTax
			} else {
				returnPayment.Amount = 0
			}

			if _, err := returnPaymentColl.InsertOne(ctx, returnPayment); err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Insert Return Order Payment ERROR - "+err.Error())
				return
			}

			_ = copier.Copy(&returnPaymentResponse, &returnPayment)
			returnResponse.ReturnPayment = append(returnResponse.ReturnPayment, returnPaymentResponse)
		}
	*/

	for _, comment := range returnRequest.ReturnCommentRequest {
		var (
			returnComment         model.ReturnComment
			returnCommentResponse model.ReturnCommentResponse
			returnCommentColl     = database.Database.Collection(setting.ReturnCommentTable)
		)
		copier.Copy(&returnComment, &comment)
		returnComment.ID = primitive.NewObjectID()
		returnComment.DocKey = returnObj.ID
		returnComment.CreatedDate = currentTime
		returnComment.UpdatedDate = currentTime

		if _, err := returnCommentColl.InsertOne(ctx, returnComment); err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Return Order Comment ERROR - "+err.Error())
			return
		}

		_ = copier.Copy(&returnCommentResponse, &returnComment)
		returnResponse.ReturnComment = append(returnResponse.ReturnComment, returnCommentResponse)
	}

	for _, shipping := range returnRequest.ReturnShippingDetailRequest {
		var (
			returnShippingDetail         model.ReturnShippingDetail
			returnShippingDetailResponse model.ReturnShippingDetailResponse
			returnShippingDetailColl     = database.Database.Collection(setting.ReturnShippingDetailTable)
		)
		_ = copier.Copy(&returnShippingDetail, &shipping)
		returnShippingDetail.ID = primitive.NewObjectID()
		returnShippingDetail.DocKey = returnObj.ID
		returnShippingDetail.CreatedDate = currentTime
		returnShippingDetail.UpdatedDate = currentTime

		if _, err := returnShippingDetailColl.InsertOne(ctx, returnShippingDetail); err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Return Order Shipping Detail ERROR - "+err.Error())
			return
		}

		_ = copier.Copy(&returnShippingDetailResponse, &returnShippingDetail)
		returnResponse.ReturnShippingDetail = append(returnResponse.ReturnShippingDetail, returnShippingDetailResponse)
	}

	// Return Freight
	/*
		for _, freight := range returnRequest.ReturnFreightRequest {
			var (
				returnFreight model.ReturnFreight
			)
			copier.Copy(&returnFreight, &freight)
			returnFreight.DocKey = returnObj.ID
			returnFreight.ID = primitive.NewObjectID()
			returnFreight.CreatedDate = currentTime
			returnFreight.UpdatedDate = currentTime
			uiReturnFreight = append(uiReturnFreight, returnFreight)
		}
	*/

	//if len(uiReturnDetail) > 0 {
	//	collection = database.Database.Collection(setting.ReturnDetailTable)
	//	// Return Detail
	//	_, err = collection.InsertMany(context.TODO(), uiReturnDetail)
	//	if err != nil {
	//		api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Insert Return Detail ERROR - "+err.Error())
	//		return
	//	}
	//} else {
	//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Please enter at least one line for detail.")
	//	return
	//}

	/*
		if len(uiReturnFreight) > 0 {
			collection = database.Database.Collection(setting.ReturnFreightTable)
			// Return Freight
			_, err = collection.InsertMany(context.TODO(), uiReturnFreight)
			if err != nil {
				response.WriteHeader(http.StatusInternalServerError)
				response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
				return
			}
		}
	*/

	//if len(uiReturnPayment) > 0 {
	//	collection = database.Database.Collection(setting.ReturnPaymentTable)
	//	// Return Detail
	//	_, err = collection.InsertMany(context.TODO(), uiReturnPayment)
	//	if err != nil {
	//		api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Insert Return Detail ERROR - "+err.Error())
	//		return
	//	}
	//}

	returnObj, err = service.HandleReturnStatusChange(returnObj.DocNum, enum.ReturnStatusReturned)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Handle Return status change ERROR - "+err.Error())
		return
	}

	returnObj, err = service.HandleReturnPaymentStatusChange(returnObj.DocNum, returnObj.PaymentStatus)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Handle Return salesOrderPayment status change ERROR - "+err.Error())
		return
	}

	returnDetails, err := service.GetReturnWithDetails(returnObj.DocNum)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Return with details ERROR - "+err.Error())
		return
	}

	if strings.ToUpper(returnDetails.SalesChannelId) == setting.SalesChannelPumaCom {
		b, _ := json.Marshal(returnDetails)

		//if err = service.InsertQueue(enum.AppIdOMS, enum.AppIdEmail, setting.ReturnTable, returnDetails.DocNum, string(b), "", "", "", false); err != nil {
		//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, err.Error())
		//	return
		//}
		//
		//if err = service.InsertQueue(enum.AppIdOMS, enum.AppIdSFCC, setting.ReturnTable, returnDetails.DocNum, string(b), "", "", "", false); err != nil {
		//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, err.Error())
		//	return
		//}

		if err = service.InsertQueue(enum.AppIdOMS, enum.AppIdYCH, setting.ReturnTable, returnDetails.DocNum, string(b), "", "", "", false); err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
			return
		}
	}

	data := bson.M{"return": returnDetails}
	api_helper.SuccessResponse(response, data, "Create Return successfully")
}

// GetReturnByID godoc
// @Tags Return
// @Summary Get Return by Id
// @Description Get Return by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Return need to be found"
// @Success 200 {object} model.Return
// @Header 200 {string} Token "qwerty"
// @Router /Return/{id} [get]
func GetReturnByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetReturnByID")
	var (
		collection *mongo.Collection
		returnObj  model.ReturnResponse
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReturnTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//Match with Id
	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.D{{"_id", bsonx.ObjectID(id)}},
			},
			},
		},
	}}
	lookupDetail := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.ReturnDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.ReturnDetailTable + "s"},
		},
		}}

	lookupPayment := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.ReturnPaymentTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.ReturnPaymentTable + "s"},
		},
		}}
	lookupComment := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.ReturnCommentTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.ReturnCommentTable + "s"},
		},
		}}
	lookupShippingDetail := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.ReturnShippingDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.ReturnShippingDetailTable + "s"},
		},
		}}
	lookupStatus := bson.D{
		{"$lookup", bson.D{
			{"from", setting.ReturnStatusTable},
			{"let", bson.M{"dn": "$doc_num"}},
			{"pipeline", bson.A{
				bson.M{"$match": bson.M{
					"$expr": bson.M{
						"$and": bson.A{
							bson.M{"$eq": bson.A{"$doc_num", "$$dn"}},
							bson.M{"$eq": bson.A{"$doc_line_num", ""}},
						},
					},
				}},
			}},
			{"as", setting.ReturnStatusTable},
		},
		}}

	lookupSalesOrder := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderTable},
			{"localField", "base_doc_key"},
			{"foreignField", "_id"},
			{"as", setting.SalesOrderTable},
		},
		}}
	unwindSalesOrder := bson.D{
		{"$unwind", bson.D{
			{"path", "$" + setting.SalesOrderTable},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	addTotalOrderAmount := bson.D{
		{"$addFields", bson.D{
			{"total_order_amount", "$" + setting.SalesOrderTable + ".total_after_tax"},
		}},
	}
	lookupSalesOrderDetails := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderDetailTable},
			{"localField", "base_doc_key"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderDetailTable},
		},
		}}
	lookupSalesOrderCaptures := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderPaymentCaptureTable},
			{"localField", "base_doc_key"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderPaymentCaptureTable + "s"},
		},
		}}
	lookupSalesOrderRefunds := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderPaymentRefundTable},
			{"localField", "base_doc_key"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderPaymentRefundTable + "s"},
		},
		}}
	lookupSalesOrderReverses := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderPaymentReverseTable},
			{"localField", "base_doc_key"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderPaymentReverseTable + "s"},
		},
		}}

	addFields := bson.D{
		{"$addFields", bson.D{
			{"order_quantity", bson.D{
				{"$sum", "$"+setting.SalesOrderDetailTable+"s.quantity"},
			}},
			{"cancel_quantity", bson.D{
				{"$sum", "$"+setting.SalesOrderDetailTable+"s.cancel_quantity"},
			}},
			{"total_order_amount", "$" + setting.SalesOrderTable + ".total_after_tax"},
		}},
	}

	pipeLine := mongo.Pipeline{
		match,
		lookupDetail,
		lookupPayment,
		lookupComment,
		lookupShippingDetail,
		lookupStatus,
		lookupSalesOrder,
		unwindSalesOrder,
		addTotalOrderAmount,
		lookupSalesOrderDetails,
		lookupSalesOrderCaptures,
		lookupSalesOrderRefunds,
		lookupSalesOrderReverses,
		addFields,
	}

	cursor, err := collection.Aggregate(ctx, pipeLine) //findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR"+err.Error())
		return
	}
	defer cursor.Close(ctx)
	if cursor.Next(ctx) {
		err := cursor.Decode(&returnObj)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Order Return with Details ERROR"+err.Error())
			return
		}
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR"+err.Error())
		return
	}

	data := bson.M{"return": returnObj}
	api_helper.SuccessResponse(response, data, "Get Return by ID successfully")
}

// Get Return By Doc Num godoc
// @Tags Return
// @Summary Get Return by Doc num
// @Description Get Return by Doc num
// @Accept  json
// @Produce  json
// @Param id path string true "Doc number of Return need to be found"
// @Success 200 {object} model.Return
// @Header 200 {string} Token "qwerty"
// @Router /Return/doc_num/{doc_num} [get]
func GetReturnByDocNum(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetReturnByID")
	var (
		collection *mongo.Collection
		returnObj  model.ReturnResponse
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReturnTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	docNum, _ := mux.Vars(request)["doc_num"]

	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.D{{"doc_num", docNum}},
			},
			},
		},
	}}
	lookupDetail := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.ReturnDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.ReturnDetailTable + "s"},
		},
		}}

	lookupPayment := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.ReturnPaymentTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.ReturnPaymentTable + "s"},
		},
		}}

	lookupComment := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.ReturnCommentTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.ReturnCommentTable + "s"},
		},
		}}

	lookupShippingDetail := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.ReturnShippingDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.ReturnShippingDetailTable + "s"},
		},
		}}
	lookupStatus := bson.D{
		{"$lookup", bson.D{
			{"from", setting.ReturnStatusTable},
			{"let", bson.M{"dn": "$doc_num"}},
			{"pipeline", bson.A{
				bson.M{"$match": bson.M{
					"$expr": bson.M{
						"$and": bson.A{
							bson.M{"$eq": bson.A{"$doc_num", "$$dn"}},
							bson.M{"$eq": bson.A{"$doc_line_num", ""}},
						},
					},
				}},
			}},
			{"as", setting.ReturnStatusTable},
		},
		}}

	lookupSalesOrder := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderTable},
			{"localField", "base_doc_key"},
			{"foreignField", "_id"},
			{"as", setting.SalesOrderTable},
		},
		}}
	unwindSalesOrder := bson.D{
		{"$unwind", bson.D{
			{"path", "$" + setting.SalesOrderTable},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	addTotalOrderAmount := bson.D{
		{"$addFields", bson.D{
			{"total_order_amount", "$" + setting.SalesOrderTable + ".total_after_tax"},
		}},
	}

	lookupSalesOrderDetails := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderDetailTable},
			{"localField", "base_doc_key"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderDetailTable},
		},
		}}
	lookupSalesOrderCaptures := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderPaymentCaptureTable},
			{"localField", "base_doc_key"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderPaymentCaptureTable + "s"},
		},
		}}
	lookupSalesOrderRefunds := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderPaymentRefundTable},
			{"localField", "base_doc_key"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderPaymentRefundTable + "s"},
		},
		}}
	lookupSalesOrderReverses := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderPaymentReverseTable},
			{"localField", "base_doc_key"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderPaymentReverseTable + "s"},
		},
		}}

	addFields := bson.D{
		{"$addFields", bson.D{
			{"order_quantity", bson.D{
				{"$sum", "$"+setting.SalesOrderDetailTable+"s.quantity"},
			}},
			{"cancel_quantity", bson.D{
				{"$sum", "$"+setting.SalesOrderDetailTable+"s.cancel_quantity"},
			}},
			{"total_order_amount", "$" + setting.SalesOrderTable + ".total_after_tax"},
		}},
	}


	pipeLine := mongo.Pipeline{
		match,
		lookupDetail,
		lookupPayment,
		lookupComment,
		lookupShippingDetail,
		lookupStatus,
		lookupSalesOrder,
		unwindSalesOrder,
		addTotalOrderAmount,
		lookupSalesOrderDetails,
		lookupSalesOrderCaptures,
		lookupSalesOrderRefunds,
		lookupSalesOrderReverses,
		addFields,
	}

	cursor, err := collection.Aggregate(ctx, pipeLine) //findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR"+err.Error())
		return
	}
	defer cursor.Close(ctx)
	if cursor.Next(ctx) {
		err := cursor.Decode(&returnObj)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Order Return with Details ERROR"+err.Error())
			return
		}
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR"+err.Error())
		return
	}

	data := bson.M{"return": returnObj}
	api_helper.SuccessResponse(response, data, "Get Return by ID successfully")
}

// GetReturns godoc
// @Tags Return
// @Summary Get all Return
// @Description Get all Return
// @Accept  json
// @Produce  json
// @Param status path string false "Status of order . RETURNED, REFUNDED, ACCEPTED, CLOSED"
// @Param country_id path string false "Country Id. of Return Order"
// @Param sales_channel_id path string false "Sales Channel Id of Return Order."
// @Param keyword path string false "Keyword for Doc Number, Customer name and Email searching."
// @Success 200 {object} []model.Return
// @Header 200 {string} Token "qwerty"
// @Router /Return [get]
func GetReturns(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetReturns")
	var (
		collection   *mongo.Collection
		allReturnObj []bson.M //[]model.ReturnResponse
		matchFilter  bson.A
	)
	collection = database.Database.Collection(setting.ReturnTable)

	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	status := request.URL.Query().Get("status")
	if status != "" {
		returnStatus := enum.ReturnStatus(status)
		if returnStatus, err = returnStatus.IsValid(); err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
			return
		} else {
			if returnStatus != enum.ReturnStatusAll {
				matchFilter = append(matchFilter, bson.D{{"doc_status", returnStatus}})
			}
		}
	}

	countryId := request.URL.Query().Get("country_id")
	if countryId != "" {

		matchFilter = append(matchFilter, bson.D{{"country_id", sHelper.RegexStringID(countryId)}})
	}

	salesChannelId := request.URL.Query().Get("sales_channel_id")
	if salesChannelId != "" {

		matchFilter = append(matchFilter, bson.D{{"sales_channel_id", sHelper.RegexStringID(salesChannelId)}})
	}

	keyword := request.URL.Query().Get("keyword")
	if keyword != "" {
		or := bson.D{{Key: "$or", Value: bson.A{
			bson.D{{"doc_num", sHelper.RegexStringID(keyword)}},
			bson.D{{"customer_name", sHelper.RegexStringID(keyword)}},
			bson.D{{"email", sHelper.RegexStringID(keyword)}},
		}}}
		matchFilter = append(matchFilter, or)
	}

	if len(matchFilter) == 0 {
		matchFilter = bson.A{bson.D{{"_id", bson.D{{"$ne", bsonx.ObjectID(primitive.NilObjectID)}}}}}
	}

	andArray := bson.M{
		"$and": matchFilter,
	}

	//Match with Id matchFilter
	match := bson.D{{
		"$match", andArray,
	}}

	sort := bson.D{{"$sort", bson.D{{"doc_date", -1}}}}
	skip := bson.D{{"$skip", limit * (page - 1)}}
	lim := bson.D{{"$limit", limit}}

	lookupDetail := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.ReturnDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.ReturnDetailTable + "s"},
		},
		}}

	lookupPayment := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.ReturnPaymentTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.ReturnPaymentTable + "s"},
		},
		}}

	lookupComment := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.ReturnCommentTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.ReturnCommentTable + "s"},
		},
		}}

	lookupShippingDetail := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.ReturnShippingDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.ReturnShippingDetailTable + "s"},
		},
		}}

	lookupStatus := bson.D{
		{"$lookup", bson.D{
			{"from", setting.ReturnStatusTable},
			{"let", bson.M{"dn": "$doc_num"}},
			{"pipeline", bson.A{
				bson.M{"$match": bson.M{
					"$expr": bson.M{
						"$and": bson.A{
							bson.M{"$eq": bson.A{"$doc_num", "$$dn"}},
							bson.M{"$eq": bson.A{"$doc_line_num", ""}},
						},
					},
				}},
			}},
			{"as", setting.ReturnStatusTable},
		},
		}}

	lookupSalesOrderDetails := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderDetailTable},
			{"localField", "base_doc_key"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderDetailTable},
		},
		}}

	sumSalesOrderDetailQuantity := bson.D{
		{"$addFields", bson.D{
			{"order_quantity", bson.D{
				{"$sum", "$" + setting.SalesOrderDetailTable + ".quantity"},
			}},
		}},
	}
	sumReturnQuantity := bson.D{
		{"$addFields", bson.D{
			{"return_quantity", bson.D{
				{"$sum", "$" + setting.ReturnDetailTable + "s.quantity"},
			}},
		}},
	}
	projection := bson.D{
		{
			"$project", bson.M{
			"doc_num":          1,
			"base_doc_num":     1,
			"order_date":       1,
			"doc_date":         1,
			"doc_status":       1,
			"customer_name":    1,
			"country_id":       1,
			"sales_channel_id": 1,
			"order_quantity":   1,
			"return_quantity":  1,
			"total_after_tax":  1,
			"currency_id":      1,
		},
		},
	}

	pipeLine := mongo.Pipeline{
		match,
		sort,
		skip,
		lim,
		lookupDetail,
		lookupPayment,
		lookupComment,
		lookupShippingDetail,
		lookupStatus,
		lookupSalesOrderDetails,
		sumSalesOrderDetailQuantity,
		sumReturnQuantity,
		projection,
	}

	cursor, err := collection.Aggregate(ctx, pipeLine) //findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR"+err.Error())
		return
	}
	defer cursor.Close(ctx)
	if err = cursor.All(ctx, &allReturnObj); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Order Return with Details ERROR"+err.Error())
		return
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR"+err.Error())
		return
	}

	count, err := collection.CountDocuments(ctx, andArray)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Count ERROR"+err.Error())
		return
	}

	data := bson.M{
		"return": allReturnObj,
		"count":  count,
	}
	api_helper.SuccessResponse(response, data, "Get Return successfully")
}

// Get count Return Order godoc
// @Tags Return
// @Summary Get count Return Order
// @Description Get count Return Order
// @Accept  json
// @Produce  json
// @Param status path string false "Status of order . RETURNED, REFUNDED, ACCEPTED, CLOSED"
// @Param country_id path string false "Country Id. of Return Order"
// @Param sales_channel_id path string false "Sales Channel Id of Return Order."
// @Param keyword path string false "Keyword for Doc Number, Customer name and Email searching."
// @Success 200 {object} model.Count
// @Header 200 {string} Token "qwerty"
// @Router /Return/count [get]
func GetCountReturn(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesOrders")

	var (
		collection  *mongo.Collection
		matchFilter = bson.D{}
		andArray    = bson.A{
			bson.M{"_id": bson.M{"$ne": bsonx.ObjectID(primitive.NilObjectID)}},
		}
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReturnTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	status := request.URL.Query().Get("status")
	keyword := request.URL.Query().Get("keyword")
	if keyword != "" {
		or := bson.D{{Key: "$or", Value: bson.A{
			bson.D{{"doc_num", primitive.Regex{Pattern: keyword, Options: "i"}}},
			bson.D{{"customer_name", primitive.Regex{Pattern: keyword, Options: "i"}}},
			bson.D{{"email", primitive.Regex{Pattern: keyword, Options: "i"}}},
		}}}
		andArray = append(andArray, or)
	}
	if status != "" {
		orderStatus := enum.OrderStatus(status)
		if orderStatus, err = orderStatus.IsValid(); err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
			return
		} else {
			if orderStatus != enum.OrderStatusAll {
				andArray = append(andArray, bson.D{{"order_status", orderStatus.ToCorrectCase()}})
			}
		}
	}

	countryId := request.URL.Query().Get("country_id")
	if countryId != "" {
		//filter := bson.M{"country_id": countryId}
		//
		//collection = database.Database.Collection(setting.CountryTable)
		//err = collection.FindOne(ctx, filter).Decode(&country)
		//if err != nil {
		//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Country object ERROR - "+err.Error())
		//	return
		//}
		andArray = append(andArray, bson.D{{"country_id", countryId}})
	}

	salesChannelId := request.URL.Query().Get("sales_channel_id")
	if salesChannelId != "" {
		//filter := bson.M{"sales_channel_id": salesChannelId}
		//
		//collection = database.Database.Collection(setting.SalesChannelTable)
		//err = collection.FindOne(ctx, filter).Decode(&salesChannel)
		//if err != nil {
		//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Sales Channel object ERROR - "+err.Error())
		//	return
		//}
		andArray = append(andArray, bson.D{{"sales_channel_id", salesChannelId}})
	}

	//brandId := request.URL.Query().Get("brand_id")
	//if brandId != "" {
	//	collection = database.Database.Collection(setting.SalesChannelTable)
	//	err = collection.FindOne(ctx, bson.M{"brand_id": brandId}).Decode(&brand)
	//	if err != nil {
	//		api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Sales Channel object ERROR - "+err.Error())
	//		return
	//	}
	//	matchFilter = append(matchFilter, bson.D{{"brand_key",brand.ID}})
	//}

	count, err := collection.CountDocuments(ctx, matchFilter)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Count document ERROR - "+err.Error())
		return
	}

	data := bson.M{"count": count}
	api_helper.SuccessResponse(response, data, "Get count Return Order successfully")
}

func ExportReturns(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetReturns")
	var (
		collection       *mongo.Collection
		allReturns       []model.ReturnExports
		matchFilter      bson.A
		matchProductCode bson.D
		//brand          model.Brand
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReturnTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	status := request.URL.Query().Get("status")
	if status != "" {
		orderStatus := enum.OrderStatus(status)
		if orderStatus, err = orderStatus.IsValid(); err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
			return
		} else {
			if orderStatus != enum.OrderStatusAll {
				matchFilter = append(matchFilter, bson.D{{"order_status", orderStatus.ToCorrectCase()}})
			}
		}
	}

	countryId := request.URL.Query().Get("country_id")
	if countryId != "" {
		matchFilter = append(matchFilter, bson.D{{"country_id", sHelper.RegexStringID(countryId)}})
	}

	salesChannelId := request.URL.Query().Get("sales_channel_id")
	if salesChannelId != "" {
		matchFilter = append(matchFilter, bson.D{{"sales_channel_id", sHelper.RegexStringID(salesChannelId)}})
	}

	email := request.URL.Query().Get("email")
	if email != "" {
		matchFilter = append(matchFilter, bson.D{{"email", sHelper.RegexStringID(email)}})
	}

	ean := request.URL.Query().Get("ean")
	if ean != "" {
		matchProductCode = bson.D{
			{"$match", bson.M{
				setting.ReturnDetailTable + "s": bson.M{
					"$elemMatch": bson.M{
						"product_code": sHelper.RegexStringID(ean),
					},
				},
			}},
		}
	} else {
		matchProductCode = bson.D{
			{"$match", bson.M{
				"_id": bson.M{"$ne": bsonx.ObjectID(primitive.NilObjectID)},
			}},
		}
	}

	keyword := request.URL.Query().Get("keyword")
	if keyword != "" {
		or := bson.D{{Key: "$or", Value: bson.A{
			bson.D{{"doc_num", sHelper.RegexStringID(keyword)}},
			bson.D{{"customer_name", sHelper.RegexStringID(keyword)}},
			bson.D{{"email", sHelper.RegexStringID(keyword)}},
		}}}
		matchFilter = append(matchFilter, or)
	}

	fromDate, toDate, err := api_helper.GetFromAndToDate(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get From and To Date ERROR - "+err.Error())
		return
	}

	matchFilter = append(matchFilter, bson.M{
		"doc_date": bson.M{
			"$gte": time.Unix(fromDate, 0),
			"$lte": time.Unix(toDate, 0),
		},
	})

	if len(matchFilter) == 0 {
		matchFilter = bson.A{bson.D{{"_id", bson.D{{"$ne", bsonx.ObjectID(primitive.NilObjectID)}}}}}
	}

	//Match with Id matchFilter
	match := bson.D{{
		"$match", bson.D{
			{"$and", matchFilter},
		},
	}}
	//sort, skip and limit
	sort := bson.D{{"$sort", bson.D{{"doc_date", -1}}}}
	//Look up for foreign key
	lookupReturnDetails := bson.D{
		{"$lookup", bson.D{
			{"from", setting.ReturnDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.ReturnDetailTable + "s"},
		},
		}}

	detailStr := "$" + setting.ReturnDetailTable + "s"
	//Unwind array to object
	unwindReturnDetail := bson.D{
		{"$unwind", bson.D{
			{"path", detailStr},
			{"preserveNullAndEmptyArrays", true}},
		},
	}

	projection := bson.D{
		{"$project", bson.D{
			{"_id", 0},
			{"rma_no", "$doc_num"},
			{"order_no", "$base_doc_num"},
			{"return_date", "$doc_date"},
			{"order_date", "$order_date"},
			{"customer", "$customer_name"},
			{"country", "$country_id"},
			{"sales_channel", "$sales_channel_id"},
			{"return_status", "$doc_status"},
			{"return_amount", "$total_after_tax"},
			{"ean", detailStr + ".product_code"},
			{"description", detailStr + ".description"},
			{"size", detailStr + ".product_size"},
			{"color", detailStr + ".product_color"},
			{"base_price", detailStr + ".price"},
			{"line_status", detailStr + ".doc_line_status"},
			{"order_quantity", detailStr + ".order_quantity"},
			{"return_quantity", detailStr + ".quantity"},
			{"discount_amount", detailStr + ".line_discount_amount"},
			{"shipping_fee", detailStr + ".freight_amount"},
			{"sub_total", bson.M{
				"$subtract": bson.A{
					bson.M{
						"$multiply": bson.A{
							detailStr + ".quantity",
							detailStr + ".price",
						}},
					detailStr + ".line_discount_amount",
				},
			}},
		}},
	}

	pipeline := mongo.Pipeline{
		match,
		lookupReturnDetails,
		matchProductCode,
		sort,
		unwindReturnDetail,
		projection,
	}

	cursor, err := collection.Aggregate(ctx, pipeline) //findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	if err = cursor.All(ctx, &allReturns); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"sales_order": allReturns}
	api_helper.SuccessResponse(response, data, "Export Return Order successfully")
}

// UpdateReturn godoc
// @Tags Return
// @Summary Update Return
// @Description Update Return
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Return need to be found"
// @Param body body model.ReturnRequest true "Update Return"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Return/{id} [put]
func UpdateReturn(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateReturn")
	var (
		collection     *mongo.Collection
		returnRequest  model.ReturnRequest
		returnObj      model.Return
		returnDetail   model.ReturnDetail
		uiReturnDetail []interface{}
		//uiReturnFreight []interface{}
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReturnTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&returnRequest)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	filter := bson.M{"_id": bsonx.ObjectID(id)}
	err = collection.FindOne(ctx, filter).Decode(&returnObj)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&returnObj, &returnRequest)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	currentTime := time.Now()
	returnObj.UpdatedDate = currentTime

	update := bson.M{"$set": returnObj}
	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update return ERROR - "+err.Error())
		return
	}

	// Return Detail
	if len(returnRequest.ReturnDetailRequest) > 0 {
		collection = database.Database.Collection(setting.ReturnDetailTable)

		for _, detail := range returnRequest.ReturnDetailRequest {
			_ = copier.Copy(&returnDetail, &detail)
			returnDetail.UpdatedDate = currentTime

			if returnDetail.ID != primitive.NilObjectID {
				filter = bson.M{"_id": bsonx.ObjectID(returnDetail.ID)}
				update = bson.M{"$set": returnDetail}
				_, err = collection.UpdateOne(ctx, filter, update)
				if err != nil {
					api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Return Detail ERROR - "+err.Error())
					return
				}
			} else {
				uiReturnDetail = append(uiReturnDetail, returnDetail)
			}
		}

		// Insert New
		if len(uiReturnDetail) > 0 {
			collection = database.Database.Collection(setting.ReturnDetailTable)
			// Return Detail
			_, err = collection.InsertMany(context.TODO(), uiReturnDetail)
			if err != nil {
				api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Return Detail ERROR - "+err.Error())
				return
			}
		}
	}

	// Return Freight
	/*
		if len(returnRequest.ReturnFreightRequest) > 0 {
			collection = database.Database.Collection(setting.ReturnFreightTable)

			for _, freight := range returnRequest.ReturnFreightRequest {
				var (
					returnFreight model.ReturnFreight
				)
				copier.Copy(&returnFreight, &freight)
				returnFreight.UpdatedDate = currentTime

				if returnFreight.ID != primitive.NilObjectID {
					filter = bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(returnFreight.ID)}}
					update = bson.M{"$set": returnFreight}
					result, err = collection.UpdateOne(ctx, filter, update)
					if err != nil {
						response.WriteHeader(http.StatusInternalServerError)
						response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
						return
					}
				} else {
					uiReturnFreight = append(uiReturnFreight, returnFreight)
				}
			}

			// Insert New
			if len(uiReturnFreight) > 0 {
				collection = database.Database.Collection(setting.ReturnFreightTable)
				_, err = collection.InsertMany(context.TODO(), uiReturnFreight)
				if err != nil {
					response.WriteHeader(http.StatusInternalServerError)
					response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
					return
				}
			}
		}*/

	returnResponse, err := service.GetReturnWithDetails(returnObj.DocNum)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Return with details ERROR - "+err.Error())
		return
	}

	data := bson.M{"return": returnResponse}
	api_helper.SuccessResponse(response, data, "Update Return Order successfully")
}

// UpdateReturn godoc
// @Tags Return
// @Summary Update Return
// @Description Update Return
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Return need to be found"
// @Param body body model.ReturnUpdateRequest true "Update Return"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Return/doc_num/{docNum} [put]
func UpdateReturnStatus(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateReturn")
	var (
		collection          *mongo.Collection
		returnObj           model.Return
		returnUpdateReqInt  interface{}
		returnUpdateRequest model.ReturnUpdateRequest
		currentTime         = time.Now()
		//uiReturnFreight []interface{}
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReturnTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	docNum, _ := mux.Vars(request)["docNum"]

	err = json.NewDecoder(request.Body).Decode(&returnUpdateRequest)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	if err = returnUpdateRequest.Validation(false); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Validate Return Update request ERROR - "+err.Error())
		return
	}

	filter := bson.M{"doc_num": docNum}
	err = collection.FindOne(ctx, filter).Decode(&returnObj)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	// DOC STATUS //

	if returnUpdateRequest.DocStatus != "" {
		//oldStatus := salesOrder.OrderStatus
		//newStatus := salesOrderUpdateReq.OrderStatus
		//
		//if oldStatus == newStatus {
		//	api_helper.SuccessResponse(response, nil, "New status is same with Old status - NO UPDATE")
		//	return
		//}
		//if !IsValidOrderStatusChange(oldStatus, newStatus.ToUpper()) {
		//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, fmt.Sprintf("Order Status '%s' is not allowed to change to '%s'", oldStatus, newStatus))
		//	return
		//}

		returnObj, err = service.HandleReturnStatusChange(returnObj.DocNum, returnUpdateRequest.DocStatus)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Handle Return Status change ERROR - "+err.Error())
			return
		}

		returnUpdateRequest.DocStatus = ""
	}

	if returnUpdateRequest.PaymentStatus != "" {
		returnObj, err = service.HandleReturnPaymentStatusChange(returnObj.DocNum, returnUpdateRequest.PaymentStatus)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Handle Return Payment Status change ERROR - "+err.Error())
			return
		}
		returnUpdateRequest.PaymentStatus = ""
	}

	//No update for Detail
	returnUpdateRequest.BaseDocNum = ""
	returnUpdateRequest.ReturnDetailUpdateRequest = nil

	rb, err := bson.Marshal(returnUpdateRequest)
	if err != nil {
		fmt.Println("Marshal FAILED - " + err.Error())
		err = nil
	}
	// To Interface{} with fields which is not empty
	_ = bson.Unmarshal(rb, &returnUpdateReqInt)
	// ----------//
	returnObj.UpdatedDate = currentTime

	//Update Sales Order.
	update := bson.A{
		bson.M{"$set": returnObj}, // Set Main model first
	}

	//check sales order request is empty.
	if len(rb) != 5 {
		update = append(update, bson.M{"$set": returnUpdateReqInt}) // Set Request Interface second
	}

	_, err = collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Sales Order ERROR - "+err.Error())
		return
	}

	returnResponse, err := service.GetReturnWithDetails(returnObj.DocNum)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Return with details ERROR - "+err.Error())
		return
	}

	data := bson.M{"return": returnResponse}
	api_helper.SuccessResponse(response, data, "Update Return Order successfully")
}

func UpdateReturnStatusByBaseDocNum(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateReturn")
	var (
		collection          *mongo.Collection
		returnObj           model.Return
		returnUpdateReqInt  interface{}
		returnUpdateRequest model.ReturnUpdateRequest
		currentTime         = time.Now()
		//uiReturnFreight []interface{}
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReturnTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&returnUpdateRequest)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	if err = returnUpdateRequest.Validation(true); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Validate Return Update request ERROR - "+err.Error())
		return
	}

	filter := bson.M{"base_doc_num": returnUpdateRequest.BaseDocNum}
	err = collection.FindOne(ctx, filter).Decode(&returnObj)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	// DOC STATUS //

	if returnUpdateRequest.DocStatus != "" {
		//oldStatus := salesOrder.OrderStatus
		//newStatus := salesOrderUpdateReq.OrderStatus
		//
		//if oldStatus == newStatus {
		//	api_helper.SuccessResponse(response, nil, "New status is same with Old status - NO UPDATE")
		//	return
		//}
		//if !IsValidOrderStatusChange(oldStatus, newStatus.ToUpper()) {
		//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, fmt.Sprintf("Order Status '%s' is not allowed to change to '%s'", oldStatus, newStatus))
		//	return
		//}

		returnObj, err = service.HandleReturnStatusChange(returnObj.DocNum, returnUpdateRequest.DocStatus)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Handle Return Status change ERROR - "+err.Error())
			return
		}

		returnUpdateRequest.DocStatus = ""
	}

	if returnUpdateRequest.PaymentStatus != "" {
		returnObj, err = service.HandleReturnPaymentStatusChange(returnObj.DocNum, returnUpdateRequest.PaymentStatus)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Handle Return Payment Status change ERROR - "+err.Error())
			return
		}
		returnUpdateRequest.PaymentStatus = ""
	}

	if len(returnUpdateRequest.ReturnDetailUpdateRequest) > 0 {
		for _, detail := range returnUpdateRequest.ReturnDetailUpdateRequest {
			if detail.DocLineStatus != "" {
				filter := bson.M{
					"base_doc_num":      returnUpdateRequest.BaseDocNum,
					"base_doc_line_num": detail.BaseDocLineNum,
				}
				update := bson.M{"$set": bson.M{"doc_line_status": detail.DocLineStatus, "updated_date": currentTime}}

				if _, err = database.Database.Collection(setting.ReturnDetailTable).UpdateOne(ctx, filter, update); err != nil {
					api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Return detail status ERROR - "+err.Error())
					return
				}
			}
		}

		returnUpdateRequest.ReturnDetailUpdateRequest = nil
	}

	rb, _ := bson.Marshal(returnUpdateRequest)
	// To Interface{} with fields which is not empty
	_ = bson.Unmarshal(rb, &returnUpdateReqInt)
	// ----------//
	returnObj.UpdatedDate = currentTime

	//Update Sales Order.
	update := bson.A{
		bson.M{"$set": returnObj}, // Set Main model first
	}

	//check sales order request is empty.
	if len(rb) != 5 {
		update = append(update, bson.M{"$set": returnUpdateReqInt}) // Set Request Interface second
	}

	_, err = collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Return order ERROR - "+err.Error())
		return
	}

	returnResponse, err := service.GetReturnWithDetails(returnObj.DocNum)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Return with details ERROR - "+err.Error())
		return
	}

	data := bson.M{"return": returnResponse}
	api_helper.SuccessResponse(response, data, "Update Return Order successfully")
}

func UpdateReturnStatusByDocNum(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateReturn")
	var (
		collection    *mongo.Collection
		returnObj     model.Return
		returnRequest model.ReturnRequest
		//uiReturnFreight []interface{}
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReturnTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	docNum, _ := mux.Vars(request)["doc_num"]

	err = json.NewDecoder(request.Body).Decode(&returnRequest)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	filter := bson.M{"doc_num": docNum}
	err = collection.FindOne(ctx, filter).Decode(&returnObj)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	returnObj, err = service.HandleReturnStatusChange(returnObj.DocNum, returnRequest.DocStatus.ToCorrectCase())
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Handle Return status change ERROR - "+err.Error())
		return
	}

	currentTime := time.Now()

	returnObj.UpdatedDate = currentTime

	data := bson.M{"return": returnObj}
	api_helper.SuccessResponse(response, data, "Update Return status successfully")
}

//------------------------- Service ------------------------

//------------------------- Unused ------------------------
func CreateManyReturn(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManyReturn")
	var (
		collection      *mongo.Collection
		returnsRequest  []model.ReturnRequest
		uiReturn        []interface{}
		uiReturnDetail  []interface{}
		uiReturnFreight []interface{}
	)

	collection = database.Database.Collection(setting.ReturnTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&returnsRequest)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	currentTime := time.Now()
	for _, returnRequest := range returnsRequest {
		var (
			returnObj model.Return
		)
		copier.Copy(&returnObj, &returnRequest)

		returnObj.ID = primitive.NewObjectID()
		returnObj.CreatedDate = currentTime
		returnObj.UpdatedDate = currentTime
		uiReturn = append(uiReturn, returnObj)

		// Return Detail
		for _, detail := range returnRequest.ReturnDetailRequest {
			var (
				returnDetail model.ReturnDetail
			)
			copier.Copy(&returnDetail, &detail)
			returnDetail.DocKey = returnObj.ID
			returnDetail.ID = primitive.NewObjectID()
			returnDetail.CreatedDate = currentTime
			returnDetail.UpdatedDate = currentTime
			uiReturnDetail = append(uiReturnDetail, returnDetail)
		}

		// Return Freight
		/*
			for _, freight := range returnRequest.ReturnFreightRequest {
				var (
					returnFreight model.ReturnFreight
				)
				copier.Copy(&returnFreight, &freight)
				returnFreight.DocKey = returnObj.ID
				returnFreight.ID = primitive.NewObjectID()
				returnFreight.CreatedDate = currentTime
				returnFreight.UpdatedDate = currentTime
				uiReturnFreight = append(uiReturnFreight, returnFreight)
			}
		*/
	}

	// Return
	_, err := collection.InsertMany(context.TODO(), uiReturn)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	if len(uiReturnDetail) > 0 {
		collection = database.Database.Collection(setting.ReturnDetailTable)
		// Return Detail
		_, err = collection.InsertMany(context.TODO(), uiReturnDetail)
		if err != nil {
			response.WriteHeader(http.StatusInternalServerError)
			response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
			return
		}
	} else {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + "Please enter at least one line for Return detail." + `" }`))
		return
	}

	if len(uiReturnFreight) > 0 {
		collection = database.Database.Collection(setting.ReturnFreightTable)
		// Return Freight
		_, err = collection.InsertMany(context.TODO(), uiReturnFreight)
		if err != nil {
			response.WriteHeader(http.StatusInternalServerError)
			response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
			return
		}
	}
	json.NewEncoder(response).Encode(map[string]interface{}{
		"returns":         uiReturn,
		"return_details":  uiReturnDetail,
		"return_freights": uiReturnFreight,
	})
}

func DeleteSoftReturn(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftReturn")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.ReturnTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
