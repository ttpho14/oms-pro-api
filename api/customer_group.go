package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../database"
	"../model"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// Add CustomerGroup godoc
// @Tags CustomerGroup
// @Summary Add CustomerGroup
// @Description Add CustomerGroup
// @Accept  json
// @Produce  json
// @Param body body model.CustomerGroupRequest true "Add CustomerGroup"
// @Success 200 {object} model.CustomerGroup
// @Header 200 {string} Token "qwerty"
// @Router /CustomerGroup [POST]
func CreateCustomerGroup(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateCustomerGroup")
	var (
		collection       *mongo.Collection
		customerGroup    model.CustomerGroup
		customerGroupReq model.CustomerGroupRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.CustomerGroupTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&customerGroupReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&customerGroup, &customerGroupReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set Date = time.Now()
	currentTime := time.Now()
	customerGroup.ID = primitive.NewObjectID()
	customerGroup.CreatedDate = currentTime
	customerGroup.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, customerGroup)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	data := bson.M{"customer_group": customerGroup}
	api_helper.SuccessResponse(response, data, "Create Customer Group successfully")
}

// Get CustomerGroup By Id godoc
// @Tags CustomerGroup
// @Summary Get CustomerGroup by Id
// @Description Get CustomerGroup by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of CustomerGroup need to be found"
// @Success 200 {object} model.CustomerGroup
// @Header 200 {string} Token "qwerty"
// @Router /CustomerGroup/{id} [get]
func GetCustomerGroupById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetCustomerGroupById")
	var (
		collection    *mongo.Collection
		customerGroup model.CustomerGroup
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.CustomerGroupTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	err := collection.FindOne(ctx, filter).Decode(&customerGroup)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	data := bson.M{"customer_group": customerGroup}
	api_helper.SuccessResponse(response, data, "Get Customer Group successfully")
}

// Get number of CustomerGroups godoc
// @Tags CustomerGroup
// @Summary Get number of CustomerGroups sort by created_date field descending
// @Description Get number of CustomerGroups depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.CustomerGroup
// @Header 200 {string} Token "qwerty"
// @Router /CustomerGroup [get]
func GetCustomerGroups(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetCustomerGroups")
	var (
		customerGroup    model.CustomerGroup
		allCustomerGroup []model.CustomerGroup
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.CustomerGroupTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{"is_deleted": false}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		_ = cursor.Decode(&customerGroup)
		allCustomerGroup = append(allCustomerGroup, customerGroup)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"customer_group": allCustomerGroup}
	api_helper.SuccessResponse(response, data, "Get Customer Group successfully")
}

// Update CustomerGroup godoc
// @Tags CustomerGroup
// @Summary Update CustomerGroup
// @Description Update CustomerGroup
// @Accept  json
// @Produce  json
// @Param id path string true "Id of CustomerGroup need to be found"
// @Param body body model.CustomerGroupRequest true "Update CustomerGroup"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /CustomerGroup/{id} [put]
func UpdateCustomerGroup(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var (
		collection       *mongo.Collection
		customerGroup    model.CustomerGroup
		customerGroupReq model.CustomerGroupRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.CustomerGroupTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err = collection.FindOne(ctx, filter).Decode(&customerGroup)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = json.NewDecoder(request.Body).Decode(&customerGroupReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&customerGroup, &customerGroupReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set lai thoi gian luc update
	customerGroup.UpdatedDate = time.Now()

	update := bson.M{"$set": customerGroup}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"customer_group": customerGroup}
	api_helper.SuccessResponse(response, data, "Update Customer Group Successfully")
}

// Soft Delete CustomerGroup godoc
// @Tags CustomerGroup
// @Summary Soft Delete CustomerGroup
// @Description Soft Delete CustomerGroup
// @Accept  json
// @Produce  json
// @Param id path string true "Id of CustomerGroup need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /CustomerGroup/{id} [delete]
func DeleteSoftCustomerGroup(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteSoftCustomerGroup")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.CustomerGroupTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	api_helper.SuccessResponse(response, id, "Delete Customer Group Successfully")
}

//-------------------- Unused -----------------------
func CreateManyCustomerGroup(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyCustomerGroup")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.CustomerGroupTable)
	response.Header().Set("content-type", "application/json")
	var manyCustomerGroup []model.CustomerGroup
	err = json.NewDecoder(request.Body).Decode(&manyCustomerGroup)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, customerGroup := range manyCustomerGroup {
		customerGroup.ID = primitive.NewObjectID()
		customerGroup.CreatedDate = time.Now()
		customerGroup.UpdatedDate = time.Now()
		ui = append(ui, customerGroup)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

//DeleteHardCustomerGroup
/*
func DeleteHardCustomerGroup(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardCustomerGroup")
	var collection *mongo.Collection
	collection = database.Database.Collection("customerGroup")
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
