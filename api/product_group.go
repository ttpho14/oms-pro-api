package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../database"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

// Add ProductGroup godoc
// @Tags ProductGroup
// @Summary Add ProductGroup
// @Description Add ProductGroup
// @Accept  json
// @Produce  json
// @Param body body model.ProductGroupRequest true "Add ProductGroup"
// @Success 200 {object} model.ProductGroup
// @Header 200 {string} Token "qwerty"
// @Router /ProductGroup [POST]
func CreateProductGroup(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateProductGroup")
	var (
		collection   *mongo.Collection
		productGroup model.ProductGroup
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ProductGroupTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&productGroup)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Input to Request Model ERROR - "+err.Error())
		return
	}
	//Set Date = time.Now()
	currentTime := time.Now()
	productGroup.ID = primitive.NewObjectID()
	productGroup.CreatedDate = currentTime
	productGroup.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, productGroup)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	data := bson.M{"product_group": productGroup}
	api_helper.SuccessResponse(response, data, "Product Group successfully")
}

// Get ProductGroup By Id godoc
// @Tags ProductGroup
// @Summary Get ProductGroup by Id
// @Description Get ProductGroup by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of ProductGroup need to be found"
// @Success 200 {object} model.ProductGroup
// @Header 200 {string} Token "qwerty"
// @Router /ProductGroup/{id} [get]
func GetProductGroupById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetProductGroupById")
	var (
		collection   *mongo.Collection
		productGroup model.ProductGroup
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ProductGroupTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	err := collection.FindOne(ctx, filter).Decode(&productGroup)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	data := bson.M{"bundle_detail": productGroup}
	api_helper.SuccessResponse(response, data, "Get Product Group successfully")
}

// Get number of ProductGroups godoc
// @Tags ProductGroup
// @Summary Get number of ProductGroups sort by created_date field descending
// @Description Get number of ProductGroups depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.ProductGroup
// @Header 200 {string} Token "qwerty"
// @Router /ProductGroup [get]
func GetProductGroups(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetProductGroups")
	var (
		productGroup    model.ProductGroup
		allProductGroup []model.ProductGroup
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.ProductGroupTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{"is_deleted": false}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		_ = cursor.Decode(&productGroup)
		allProductGroup = append(allProductGroup, productGroup)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"product_group": allProductGroup}
	api_helper.SuccessResponse(response, data, "Get Product Group successfully")
}

// Update ProductGroup godoc
// @Tags ProductGroup
// @Summary Update ProductGroup
// @Description Update ProductGroup
// @Accept  json
// @Produce  json
// @Param id path string true "Id of ProductGroup need to be found"
// @Param body body model.ProductGroupRequest true "Update ProductGroup"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /ProductGroup/{id} [put]
func UpdateProductGroup(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var (
		collection      *mongo.Collection
		productGroup    model.ProductGroup
		productGroupReq model.ProductGroupRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ProductGroupTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err = collection.FindOne(ctx, filter).Decode(&productGroup)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = json.NewDecoder(request.Body).Decode(&productGroupReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&productGroup, &productGroupReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set lai thoi gian luc update
	productGroup.UpdatedDate = time.Now()

	update := bson.M{"$set": productGroup}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"product_group": productGroup}
	api_helper.SuccessResponse(response, data, "Update Product Group Successfully")
}

// Soft Delete ProductGroup godoc
// @Tags ProductGroup
// @Summary Soft Delete ProductGroup
// @Description Soft Delete ProductGroup
// @Accept  json
// @Produce  json
// @Param id path string true "Id of ProductGroup need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /ProductGroup/{id} [delete]
func DeleteSoftProductGroup(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteSoftProductGroup")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ProductGroupTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	api_helper.SuccessResponse(response, id, "Delete Product Group Successfully")
}

//DeleteHardProductGroup
/*
func DeleteHardProductGroup(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardProductGroup")
	var collection *mongo.Collection
	collection = database.Database.Collection("productGroup")
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
//-------------------- Unused  -----------------------

func CreateManyProductGroup(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyProductGroup")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.ProductGroupTable)
	response.Header().Set("content-type", "application/json")
	var manyProductGroup []model.ProductGroup
	err = json.NewDecoder(request.Body).Decode(&manyProductGroup)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, productGroup := range manyProductGroup {
		productGroup.ID = primitive.NewObjectID()
		productGroup.CreatedDate = time.Now()
		productGroup.UpdatedDate = time.Now()
		ui = append(ui, productGroup)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}
