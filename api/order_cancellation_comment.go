package api

import (
	"context"
	"encoding/json"
	"net/http"
	"os"
	"time"

	"../setting"
	"github.com/jinzhu/copier"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateOrderCancellationComment godoc
// @Tags Order Cancellation Comment
// @Summary Add Order Cancellation Comment
// @Description Add Order Cancellation Comment
// @Accept  json
// @Produce  json
// @Param body body model.OrderCancellationCommentRequest true "Add Order Cancellation Comment"
// @Success 200 {object} model.OrderCancellationComment
// @Header 200 {string} Token "qwerty"
// @Router /OrderCancellationComment [POST]
func CreateOrderCancellationComment(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateOrderCancellationComment")
	var (
		collection                  *mongo.Collection
		orderCancellationComment    model.OrderCancellationComment
		orderCancellationCommentReq model.OrderCancellationCommentRequest
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.OrderCancellationCommentTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&orderCancellationCommentReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Order Cancellation Comment Input ERROR - "+err.Error())
		return
	}

	_ = copier.Copy(&orderCancellationComment, &orderCancellationCommentReq)

	currentTime := time.Now()
	orderCancellationComment.ID = primitive.NewObjectIDFromTimestamp(currentTime)
	orderCancellationComment.ObjectType = setting.OrderCancellationCommentTable
	orderCancellationComment.CreatedDate = currentTime
	orderCancellationComment.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, orderCancellationComment)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Order Cancellation Comment ERROR - "+err.Error())
		return
	}

	data := bson.M{"order_cancellation_comment": orderCancellationComment}
	api_helper.SuccessResponse(response, data, "Create Order Cancellation Comment successfully")
}

// CreateManyOrderCancellationComment func
func CreateManyOrderCancellationComment(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManyOrderCancellationComment")
	var (
		collection                *mongo.Collection
		orderCancellationComments []model.OrderCancellationComment
	)
	collection = database.Database.Collection(setting.OrderCancellationCommentTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&orderCancellationComments)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	currentTime := time.Now()
	for _, orderCancellationComment := range orderCancellationComments {
		orderCancellationComment.ID = primitive.NewObjectID()
		orderCancellationComment.CreatedDate = currentTime
		orderCancellationComment.UpdatedDate = currentTime
		ui = append(ui, orderCancellationComment)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// GetOrderCancellationCommentByID godoc
// @Tags Order Cancellation Comment
// @Summary Get Order Cancellation Comment by Id
// @Description Get Order Cancellation Comment by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Order Cancellation Comment need to be found"
// @Success 200 {object} model.OrderCancellationComment
// @Header 200 {string} Token "qwerty"
// @Router /OrderCancellationComment/{id} [get]
func GetOrderCancellationCommentByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSaleOrderCommentByID")
	var (
		collection               *mongo.Collection
		orderCancellationComment model.OrderCancellationComment
	)
	collection = database.Database.Collection(setting.OrderCancellationCommentTable)

	response.Header().Set("content-type", "application/json")

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	err := collection.FindOne(ctx, filter).Decode(&orderCancellationComment)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(orderCancellationComment)
}

// GetOrderCancellationComments godoc
// @Tags Order Cancellation Comment
// @Summary Get all Order Cancellation Comment
// @Description Get all Order Cancellation Comment
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.OrderCancellationComment
// @Header 200 {string} Token "qwerty"
// @Router /OrderCancellationComment [get]
func GetOrderCancellationComments(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetOrderCancellationComments")
	var (
		collection                *mongo.Collection
		orderCancellationComments []model.OrderCancellationComment
		findOptions               = options.Find()
		filter                    = bson.M{}
	)
	collection = database.Database.Collection(setting.OrderCancellationCommentTable)

	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions) //findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var orderCancellationComment model.OrderCancellationComment
		cursor.Decode(&orderCancellationComment)
		orderCancellationComments = append(orderCancellationComments, orderCancellationComment)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(orderCancellationComments)
}

// UpdateOrderCancellationComment godoc
// @Tags Order Cancellation Comment
// @Summary Update Order Cancellation Comment
// @Description Update Order Cancellation Comment
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Order Cancellation need to be found"
// @Param body body model.OrderCancellationRequest true "Update Order Cancellation Comment"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /OrderCancellationComment/{id} [put]
func UpdateOrderCancellationComment(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateSaleOrderComment")
	var (
		collection               *mongo.Collection
		orderCancellationComment model.OrderCancellationComment
	)
	collection = database.Database.Collection(setting.OrderCancellationCommentTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&orderCancellationComment)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	orderCancellationComment.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": orderCancellationComment}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

func DeleteSoftOrderCancellationComment(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftOrderCancellationComment")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.OrderCancellationCommentTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
