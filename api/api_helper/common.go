package api_helper

import (
	"../../database"
	"../../logger"
	"../../model"
	"../../setting"
	"../../validation"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"time"
)

func ParseParamStringToIntFromURLQuery(r *http.Request, param string, defaultValue int64, max int64) (int64, error) {
	paramInt := r.URL.Query().Get(param)
	if paramInt == "" {
		return defaultValue, nil
	} else {
		i, err := strconv.ParseInt(paramInt, 10, 64)
		if err != nil {
			return 0, err
		}
		if max != 0 && i > max {
			i = max
		}
		return i, nil
	}
}

func ParseParamStringToIntFromURL(r *http.Request, param string, defaultValue int64, max int64) (int64, error) {
	paramInt := r.URL.Query().Get(param)
	if paramInt == "" {
		return defaultValue, nil
	} else {
		i, err := strconv.ParseInt(paramInt, 10, 64)
		if err != nil {
			return 0, err
		}
		if max != 0 && i > max {
			i = max
		}
		return i, nil
	}
}

func RecoverError(response http.ResponseWriter, funcName string) {
	if r := recover(); r != nil {
		responseData := bson.M{
			"status":   500,
			"msg":      r,
			"api_name": funcName,
		}
		logger.Logger.Error(funcName + ": " + fmt.Sprintf("%v", r))
		response.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(response).Encode(responseData)
		return
	}
}

func ValidateAddress(add model.AddressRequest) {
	validation.CheckNul(add)
	validation.IsValidEmail(add.EmailAddress)
	validation.CheckPhone(add.Phone1)
	validation.CheckPhone(add.Phone2)
	validation.ValidateKey(add.TenantKey, "tenant")
	validation.ValidateKey(add.SourceKey, add.SourceObjectType)
	//validation.ValidateKey(add.UpdatedApplicationKey,"application")
	//validation.ValidateKey(add.CreatedApplicationKey,"application")
}

func ValidateSaleOrder(addSaleOrder model.SalesOrderRequest) {
	validation.CheckNul(addSaleOrder)
	validation.ValidateKey(addSaleOrder.TenantKey, "tenant")
}

func GetFields(b interface{}) []model.Field {
	var fields []model.Field
	val := reflect.ValueOf(b)
	for i := 0; i < val.Type().NumField(); i++ {
		var field model.Field
		fieldName := val.Type().Field(i).Tag.Get("json")
		split := strings.Split(fieldName, ",")
		fieldName = split[0]

		field.FieldName = fieldName
		field.DataType = val.Type().Field(i).Type.String()

		fields = append(fields, field)
	}
	return fields
}

func ParseValueToFilter(filter bson.D, field string, valStr string, dataType string) (bson.D, error) {
	switch dataType {
	case "bool":
		value, err := strconv.ParseBool(valStr)
		if err != nil {
			return nil, err
		}
		filter = append(filter, bson.E{Key: field, Value: value})
	case "int":
		value, err := strconv.ParseInt(valStr, 10, 64)
		if err != nil {
			return nil, err
		}
		filter = append(filter, bson.E{Key: field, Value: value})
	case "primitive.ObjectID":
		objectId, err := primitive.ObjectIDFromHex(valStr)
		if err != nil {
			return nil, err
		}
		value := bsonx.ObjectID(objectId)
		filter = append(filter, bson.E{Key: field, Value: value})
	case "string":
		filter = append(filter, bson.E{Key: field, Value: valStr})
	default:
		return filter, nil
	}
	return filter, nil
}

func ValidateProduct(addProduct model.ProductRequest) {
	validation.CheckNul(addProduct)
}

func GetApplicationFromToken(clientId string) (model.Application, error) {
	var (
		collection *mongo.Collection
		//user       model.User
		application model.Application
		//funcName    = "GetApplicationFromToken"
	)
	//if userId != "" {
	//	collection = database.Database.Collection(setting.USER_TABLE_NAME)
	//
	//	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	//	defer cancel()
	//	//_id = id and is_deleted = false
	//	filter := bson.D{{"user_id", userId}}
	//
	//	err := collection.FindOne(ctx, filter).Decode(&user)
	//	if err != nil {
	//		logger.Logger.Error(funcName + "ERROR : " + err.Error())
	//		return
	//	}
	//}
	collection = database.Database.Collection(setting.ApplicationTable)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//_id = id and is_deleted = false
	filter := bson.D{{"client_id", clientId}}

	if err := collection.FindOne(ctx, filter).Decode(&application); err != nil {
		return application, errors.New("Find Application ERROR - " + err.Error())
	}

	return application, nil
}

func IsFieldExistsInModel(i interface{}, fieldName string) bool {
	ValueIface := reflect.ValueOf(i)

	// Check if the passed interface is a pointer
	if ValueIface.Type().Kind() != reflect.Ptr {
		// Create a new type of Iface's Type, so we have a pointer to work with
		ValueIface = reflect.New(reflect.TypeOf(i))
	}

	// 'dereference' with Elem() and get the field by name
	Field := ValueIface.Elem().FieldByName(fieldName)
	if !Field.IsValid() {
		logger.Logger.Errorf("Interface `%s` does not have the field `%s`", ValueIface.Type(), fieldName)
		return false
	}
	return true
}

func GetLimitAndPage(r *http.Request) (limit int64, page int64, err error) {
	limit, err = ParseParamStringToIntFromURLQuery(r, "limit", 300, 500)
	if err != nil {
		return 0, 0, err
	}

	page, err = ParseParamStringToIntFromURLQuery(r, "page", 1, 0)
	if err != nil {
		return 0, 0, err
	}

	return limit, page, nil
}

func GetFromAndToDate(r *http.Request) (fromDate int64, toDate int64, err error) {
	currentTime := time.Now().Unix()

	fromDate, err = ParseParamStringToIntFromURL(r, "from_date", setting.DefaultFromDateUnix, currentTime)
	if err != nil {
		return 0, 0, err
	}

	toDate, err = ParseParamStringToIntFromURL(r, "to_date", setting.DefaultToDateUnix, setting.DefaultToDateUnix)
	if err != nil {
		return 0, 0, err
	}

	if fromDate > toDate {
		return 0, 0, errors.New(fmt.Sprintf("ERROR : From Date '%s' CAN NOT greater than To Date '%s'", time.Unix(fromDate, 0), time.Unix(toDate, 0)))
	}

	return fromDate, toDate, nil
}

func ErrorResponse(response http.ResponseWriter, statusCode, errorCode int, errMsg string) {
	var res model.ResponseResult
	res.Message = errMsg
	res.ErrorCode = errorCode
	response.WriteHeader(statusCode)

	//resBytes,_ := json.Marshal(res)
	//service.UpdateAPILog()

	json.NewEncoder(response).Encode(res)
}

func SuccessResponse(response http.ResponseWriter, data interface{}, msg string) {
	var res model.ResponseResult
	res.Success = true
	res.Message = msg
	res.Data = data
	json.NewEncoder(response).Encode(res)
}
