package api

import (
	"../database"
	"../model"
	"../setting"
	"./api_helper"
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"net/http"
	"os"
	"time"
)

// CreateSalesOrderDetail godoc
// @Tags Sales Order Detail
// @Summary Add Sales Order Detail
// @Description Add Sales Order Detail
// @Accept  json
// @Produce  json
// @Param body body []model.SalesOrderDetailRequest true "Add Sales Order Detail"
// @Success 200 {object} []model.SalesOrderDetail
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderDetail [POST]
func CreateSalesOrderDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateSalesOrderDetail")
	var (
		collection       *mongo.Collection
		salesOrderDetail model.SalesOrderDetail
	)

	collection = database.Database.Collection(setting.SalesOrderDetailTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&salesOrderDetail)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	currentTime := time.Now()
	salesOrderDetail.CreatedDate = currentTime
	salesOrderDetail.UpdatedDate = currentTime

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	result, err := collection.InsertOne(ctx, salesOrderDetail)
	if err != nil {
		json.NewEncoder(response).Encode(err.Error())
		return
	}
	id := result.InsertedID
	salesOrderDetail.ID = id.(primitive.ObjectID)
	json.NewEncoder(response).Encode(salesOrderDetail)
}

// CreateManySalesOrderDetail func
func CreateManySalesOrderDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManySalesOrderDetail")
	var (
		collection               *mongo.Collection
		salesOrderDetail         model.SalesOrderDetail
		salesOrderDetailsRequest []model.SalesOrderDetailRequest
		uiSalesOrderDetails      []interface{}
	)
	collection = database.Database.Collection(setting.SalesOrderDetailTable)
	response.Header().Set("content-type", "application/json")

	err = json.NewDecoder(request.Body).Decode(&salesOrderDetailsRequest)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	currentTime := time.Now()
	for _, salesRequest := range salesOrderDetailsRequest {
		copier.Copy(&salesOrderDetail, salesRequest)

		salesOrderDetail.ID = primitive.NewObjectID()
		salesOrderDetail.CreatedDate = currentTime
		salesOrderDetail.UpdatedDate = currentTime
		uiSalesOrderDetails = append(uiSalesOrderDetails, salesOrderDetail)
	}
	_, err = collection.InsertMany(context.TODO(), uiSalesOrderDetails)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(uiSalesOrderDetails)
}

// GetSalesOrderDetailByID godoc
// @Tags Sales Order Detail
// @Summary Get Sales Order Detail by Id
// @Description Get Sales Order Detail by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Order Detail need to be found"
// @Success 200 {object} model.SalesOrderDetail
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderDetail/{id} [get]
func GetSalesOrderDetailByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSaleOrderDetailByID")
	var (
		collection       *mongo.Collection
		salesOrderDetail model.SalesOrderDetail
	)
	collection = database.Database.Collection(setting.SalesOrderDetailTable)

	response.Header().Set("content-type", "application/json")

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	err := collection.FindOne(ctx, filter).Decode(&salesOrderDetail)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesOrderDetail)
}

// GetSalesOrderDetails godoc
// @Tags Sales Order Detail
// @Summary Get all Sales Order Detail
// @Description Get all Sales Order Detail
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.SalesOrderDetail
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderDetail [get]
func GetSalesOrderDetails(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesOrderDetails")
	var (
		collection        *mongo.Collection
		salesOrderDetails []model.SalesOrderDetail
		findOptions       = options.Find()
		filter            = bson.M{}
	)
	collection = database.Database.Collection(setting.SalesOrderDetailTable)
	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions) //findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var saleOrderDetail model.SalesOrderDetail
		cursor.Decode(&saleOrderDetail)
		salesOrderDetails = append(salesOrderDetails, saleOrderDetail)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesOrderDetails)
}

// UpdateSalesOrderDetail godoc
// @Tags Sales Order Detail
// @Summary Update Sales Order Detail
// @Description Update Sales Order Detail
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Order need to be found"
// @Param body body model.SalesOrderDetailRequest true "Update Sales Order Detail"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderDetail/{id} [put]
func UpdateSalesOrderDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateSaleOrderDetail")
	var (
		collection      *mongo.Collection
		saleOrderDetail model.SalesOrderDetail
	)
	collection = database.Database.Collection(setting.SalesOrderDetailTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&saleOrderDetail)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	saleOrderDetail.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": saleOrderDetail}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

//--------------------------- Service ---------------------------------



func DeleteSoftSalesOrderDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftSalesOrderDetail")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.SalesOrderDetailTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
