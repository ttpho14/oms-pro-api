package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../database"
	"../model"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// Add Category godoc
// @Tags Category
// @Summary Add Category
// @Description Add Category
// @Accept  json
// @Produce  json
// @Param body body model.CategoryRequest true "Add Category"
// @Success 200 {object} model.Category
// @Header 200 {string} Token "qwerty"
// @Router /Category [POST]
func CreateCategory(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateCategory")
	var (
		collection  *mongo.Collection
		category    model.Category
		categoryReq model.CategoryRequest
	)

	response.Header().Set("content-type", "application/json")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	collection = database.Database.Collection(setting.CategoryTable)
	err = json.NewDecoder(request.Body).Decode(&categoryReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&category, &categoryReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set Date = time.Now()
	currentTime := time.Now()
	category.ID = primitive.NewObjectID()
	category.CreatedDate = currentTime
	category.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, category)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	data := bson.M{"category": category}
	api_helper.SuccessResponse(response, data, "Create Category successfully")
}

// Get Category By Id godoc
// @Tags Category
// @Summary Get Category by Id
// @Description Get Category by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Category need to be found"
// @Success 200 {object} model.Category
// @Header 200 {string} Token "qwerty"
// @Router /Category/{id} [get]
func GetCategoryById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetCategoryById")
	var (
		collection *mongo.Collection
		category   model.Category
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.CategoryTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err := collection.FindOne(ctx, filter).Decode(&category)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	data := bson.M{"category": category}
	api_helper.SuccessResponse(response, data, "Get Category Successfully")
}

// Get number of Categories godoc
// @Tags Category
// @Summary Get number of Categories sort by created_date field descending
// @Description Get number of Categories depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.Category
// @Header 200 {string} Token "qwerty"
// @Router /Category [get]
func GetCategories(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetCategories")

	var (
		allCategory []model.Category
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.CategoryTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{"is_deleted": bson.M{"$eq": false}}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))

	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var category model.Category
		_ = cursor.Decode(&category)
		allCategory = append(allCategory, category)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"category": allCategory}
	api_helper.SuccessResponse(response, data, "Get Category Successfully")
}

// Update Category godoc
// @Tags Category
// @Summary Update Category
// @Description Update Category
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Category need to be found"
// @Param body body model.CategoryRequest true "Update Category"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Category/{id} [put]
func UpdateCategory(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var (
		collection  *mongo.Collection
		category    model.Category
		categoryReq model.CategoryRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.CategoryTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err = collection.FindOne(ctx, filter).Decode(&category)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = json.NewDecoder(request.Body).Decode(&categoryReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&category, &categoryReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set lai thoi gian luc update
	category.UpdatedDate = time.Now()

	update := bson.M{"$set": category}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"category": category}
	api_helper.SuccessResponse(response, data, "Update Category Successfully")
}

// Soft Delete Category godoc
// @Tags Category
// @Summary Soft Delete Category
// @Description Soft Delete Category
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Category need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Category/{id} [delete]
func DeleteSoftCategory(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.CategoryTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	api_helper.SuccessResponse(response, id, "Delete Category successfully")
}

//-------------------- Unused -----------------------
func CreateCategoryOld(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateCategory")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.CategoryTable)
	response.Header().Set("content-type", "application/json")
	var category model.Category
	err = json.NewDecoder(request.Body).Decode(&category)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	//Set Date = time.Now()
	category.CreatedDate = time.Now()
	category.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	result, _ := collection.InsertOne(ctx, category)
	id := result.InsertedID
	category.ID = id.(primitive.ObjectID)
	json.NewEncoder(response).Encode(category)
}

func CreateManyCategory(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyCategory")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.CategoryTable)
	response.Header().Set("content-type", "application/json")
	var manyCategory []model.Category
	err = json.NewDecoder(request.Body).Decode(&manyCategory)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, category := range manyCategory {
		category.ID = primitive.NewObjectID()
		category.CreatedDate = time.Now()
		category.UpdatedDate = time.Now()
		ui = append(ui, category)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

func CreateManyCategoryTest(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyCategory")
	var (
		manyAddCategory []model.CategoryRequest
		uiHeader        []interface{}
		uiDetail        []interface{}
	)
	collectionHeader := database.Database.Collection(setting.CategoryTable)
	collectionDetail := database.Database.Collection("category_detail")
	response.Header().Set("content-type", "application/json")
	//var manyCategory []model.Category
	err = json.NewDecoder(request.Body).Decode(&manyAddCategory)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	// Loop through Input Category
	for _, addCategory := range manyAddCategory {
		var category model.Category
		currentTime := time.Now()
		// Assign input to category
		category.ID = primitive.NewObjectID()
		category.TenantKey = addCategory.TenantKey
		category.CategoryId = addCategory.CategoryId
		category.Description = addCategory.Description
		category.ObjectType = addCategory.ObjectType
		category.IsActive = addCategory.IsActive
		category.IsDeleted = false
		category.CreatedDate = currentTime
		category.CreatedBy = addCategory.CreatedBy
		category.CreatedApplicationKey = addCategory.CreatedApplicationKey
		category.UpdatedDate = currentTime
		category.UpdatedBy = addCategory.UpdatedBy
		category.UpdatedApplicationKey = addCategory.UpdatedApplicationKey

		uiHeader = append(uiHeader, category)
		//Assign input to category detail
		for _, addCategoryDetail := range addCategory.AddCategoryDetail {
			var categoryDetail model.CategoryDetail
			categoryDetail.ID = primitive.NewObjectID()
			// Get Id from header
			categoryDetail.CategoryKey = category.ID
			categoryDetail.CategoryDetailParentKey = addCategoryDetail.CategoryDetailParentKey
			categoryDetail.Description = addCategoryDetail.Description
			categoryDetail.ProductKey = addCategoryDetail.ProductKey
			categoryDetail.ObjectType = addCategoryDetail.ObjectType
			categoryDetail.IsActive = addCategoryDetail.IsActive
			categoryDetail.IsDeleted = false
			categoryDetail.CreatedDate = currentTime
			categoryDetail.CreatedBy = addCategoryDetail.CreatedBy
			categoryDetail.CreatedApplicationKey = addCategoryDetail.CreatedApplicationKey
			categoryDetail.UpdatedDate = currentTime
			categoryDetail.UpdatedBy = addCategoryDetail.UpdatedBy
			categoryDetail.UpdatedApplicationKey = addCategoryDetail.UpdatedApplicationKey
			uiDetail = append(uiDetail, categoryDetail)
		}

	}
	//Insert into HEADER - Category
	_, err := collectionHeader.InsertMany(context.TODO(), uiHeader)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	//Insert into Detail - Category Detail
	_, err = collectionDetail.InsertMany(context.TODO(), uiDetail)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(map[string]interface{}{"category": uiHeader, "category_detail": uiDetail})
}

func GetCategoryByIdOld(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetCategoryById")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.CategoryTable)
	response.Header().Set("content-type", "application/json")
	var category model.Category
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//_id = id and is_deleted = false
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}, "is_deleted": bson.M{"$eq": false}}
	err := collection.FindOne(ctx, filter).Decode(&category)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(category)
}

func GetCategoriesOld(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetCategories")
	//Get Limit and Page
	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	collection := database.Database.Collection(setting.CategoryTable)
	response.Header().Set("content-type", "application/json")
	var allCategory []model.Category
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"is_deleted": bson.M{"$eq": false}}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var category model.Category
		cursor.Decode(&category)
		allCategory = append(allCategory, category)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(allCategory)
}

//DeleteHardCategory
/*
func DeleteHardCategory(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardCategory")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.CategoryTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
