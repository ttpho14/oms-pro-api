package api

import (
	"context"
	"net/http"
	"time"

	"../database"
	"../setting"
	"./api_helper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"

	"../model"
)

// Add Currency godoc
// @Tags Currency
// @Summary Add Currency
// @Description Add Currency
// @Accept  json
// @Produce  json
// @Param body body []model.CurrencyRequest true "Add Currency"
// @Success 200 {object} []model.Currency
// @Header 200 {string} Token "qwerty"
// @Router /Currency [POST]
func OmsInitialization(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateCurrency")

	var (
		userCollection        *mongo.Collection
		applicationCollection *mongo.Collection
		user                  = model.OmsUserInitialize()
		application           = model.OmsApplicationInitialize()
	)

	clientId = setting.OmsClientId
	secretKey = setting.OmsSecretKey

	response.Header().Set("content-type", "application/json")

	userCollection = database.Database.Collection(setting.UserTable)
	applicationCollection = database.Database.Collection(setting.ApplicationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	userFilter := bson.M{"user_id": user.UserId}

	userCount, err := userCollection.CountDocuments(ctx, userFilter)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Count User ERROR - "+err.Error())
		return
	}

	if userCount == 0 {
		_, err := userCollection.InsertOne(ctx, user)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert OMS User ERROR - "+err.Error())
			return
		}
	} else {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "OMS User is already exists")
		return
	}

	applicationFilter := bson.M{"application_id": application.ApplicationId}

	applicationCount, err := applicationCollection.CountDocuments(ctx, applicationFilter)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Count ERROR - "+err.Error())
		return
	}

	if applicationCount == 0 {
		_, err = applicationCollection.InsertOne(ctx, application)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert OMS Application ERROR - "+err.Error())
			return
		}
	} else {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "OMS Application is already exists")
		return
	}

	data := bson.M{
		"user":        user,
		"application": application,
	}
	api_helper.SuccessResponse(response, data, "Create OMS User and Application successfully")
}
