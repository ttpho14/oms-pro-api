package api

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"../database"
	"../model"
	"../model/enum"
	"../service"
	sHelper "../service/helper"
	"../setting"
	aHelper "./api_helper"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// Update MileStone godoc
// @Tags Web Hook
// @Summary Update MileStone
// @Description Update MileStone
// @Accept  json
// @Produce  json
// @Param body body model.MilestoneRequest true "Update MileStone"
// @Success 200 {object} model.Milestone
// @Header 200 {string} Token "qwerty"
// @Router /MileStone [put]
func UpdateMilestone(response http.ResponseWriter, request *http.Request) {
	defer aHelper.RecoverError(response, "UpdateMilestone")
	response.Header().Set("content-type", "application/json")
	var (
		mileStone        model.Milestone
		mileStoneRequest model.MilestoneRequest
		res              model.ResponseResult
		byteBody         interface{}
		//mileStoneOrders         []model.MilestoneOrder
		//mileStoneDeliveryOrders []model.MilestoneDeliveryOrder
	)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	bodyReader, _ := ioutil.ReadAll(request.Body)
	_ = json.Unmarshal(bodyReader, &byteBody)
	body, _ := json.Marshal(byteBody)

	_ = service.CreateIntegrationLog("OMS.Pro."+enum.AppIdYCH, "UpdateMileStone", string(body), "", "")

	err = json.Unmarshal(bodyReader, &mileStoneRequest)
	if err != nil {
		errMsg := "Decode ERROR - " + err.Error()
		service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "milestone", mileStoneRequest.Orders[0].DeliveryOrders[0].No, string(body), errMsg, response)
		return
	}

	if err = mileStoneRequest.Validation(); err != nil {
		errMsg := "Validate Milestone request ERROR - " + err.Error()
		service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "milestone", mileStoneRequest.Orders[0].DeliveryOrders[0].No, string(body), errMsg, response)
		return
	}

	//body, _ = json.Marshal(mileStoneRequest)
	//_ = json.Unmarshal(body, &byteBody)

	err = copier.Copy(&mileStone, &mileStoneRequest)
	if err != nil {
		errMsg := "Copy ERROR - " + err.Error()
		service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "milestone", mileStoneRequest.Orders[0].DeliveryOrders[0].No, string(body), errMsg, response)
		return
	}

	for _, order := range mileStone.Orders {
		for _, deliOrder := range order.DeliveryOrders {
			var (
				orderStatus  = enum.OrderStatus(deliOrder.TrackingStatus).ToCorrectCase()
				returnStatus = enum.ReturnStatus(deliOrder.TrackingStatus).ToCorrectCase()
			)

			if returnStatus == enum.ReturnStatusReceived {
				_, err = service.HandleReturnStatusChange(deliOrder.No, enum.ReturnStatusReceived)
				if err != nil {
					errMsg := "Handle Return Status change ERROR - " + err.Error()
					service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "milestone", mileStone.Orders[0].DeliveryOrders[0].No, string(body), errMsg, response)
					return
				}
			} else {
				switch orderStatus {
				case enum.OrderStatusProcessing:
					orderStatus = enum.OrderStatusPicked
				case enum.OrderStatusHandover:
					orderStatus = enum.OrderStatusShipped
				}
				_, err = service.HandleOrderStatusChange(deliOrder.No, orderStatus, true)
				if err != nil {
					errMsg := "Handle Order Status change ERROR - " + err.Error()
					service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "milestone", mileStone.Orders[0].DeliveryOrders[0].No, string(body), errMsg, response)
					return
				}

				count, err := database.Database.Collection(setting.SalesOrderTable).CountDocuments(ctx, bson.M{"doc_num": deliOrder.No, "sales_channel_id": sHelper.RegexStringID("puma.com")})
				if err != nil {
					errMsg := "Count Sales Order with sales channel id equal to " + setting.SalesChannelPumaCom + " ERROR - " + err.Error()
					service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "milestone", mileStone.Orders[0].DeliveryOrders[0].No, string(body), errMsg, response)
					return
				}

				if count > 0 {
					_, err = database.Database.Collection(setting.SalesOrderTable).UpdateOne(ctx, bson.M{"doc_num": deliOrder.No}, bson.M{"$set": bson.M{"tracking_no": deliOrder.TrackingNumber, "updated_date": time.Now()}})
					if err != nil {
						errMsg := "Update Sales Order ERROR - " + err.Error()
						service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "milestone", mileStone.Orders[0].DeliveryOrders[0].No, string(body), errMsg, response)
						return
					}

					/*
						if orderStatus == enum.OrderStatusPacked{
							salesOrder ,err := service.GetSalesOrderWithDetails(deliOrder.No)
							if err != nil {
								errMsg := "Get Sales Order ERROR - " + err.Error()
								service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "milestone", mileStone.Orders[0].DeliveryOrders[0].No, string(body), errMsg, response)
								return
							}

							salesBytes , err := json.Marshal(salesOrder)
							if err != nil {
								errMsg := "Marshal Sales Order ERROR - " + err.Error()
								service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "milestone", mileStone.Orders[0].DeliveryOrders[0].No, string(body), errMsg, response)
								return
							}

							_ = service.InsertQueue(enum.AppIdOMS,enum.AppIdCyberSource,"captures", mileStone.Orders[0].DeliveryOrders[0].No, string(salesBytes), "","", "", false)
						}
					*/
				}

			}
		}
	}
	//mileStone.Action = "UPDATE"
	//mileStone.ActionRemarks = "YCH OMS updated status"

	data := bson.M{"milestone": mileStone}
	msg := "Update Milestone successfully!"

	res.Success = true
	res.Message = msg
	res.Data = data

	byteData, _ := json.Marshal(res)

	//byteMilestone, _ := json.Marshal(mileStoneRequest)

	_ = service.InsertQueue(enum.AppIdYCH, enum.AppIdOMS, "milestone", mileStone.Orders[0].DeliveryOrders[0].No, string(body), string(byteData), string(body), "", true)

	aHelper.SuccessResponse(response, data, msg)
}

// Update Order Pack godoc
// @Tags Web Hook
// @Summary Update Order Pack
// @Description Update Order Pack
// @Accept  json
// @Produce  json
// @Param body body model.OrderPackRequest true "Update Order Pack"
// @Success 200 {object} model.OrderPack
// @Header 200 {string} Token "qwerty"
// @Router /OrderPack [put]
func UpdateOrderPack(response http.ResponseWriter, request *http.Request) {
	defer aHelper.RecoverError(response, "UpdateOrderPack")
	response.Header().Set("content-type", "application/json")
	collection := database.Database.Collection(setting.OrderPackTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Minute)
	defer cancel()
	var (
		tmpOrderPack     model.OrderPack
		orderPack        model.OrderPack
		orderPackRequest model.OrderPackRequest
		res              model.ResponseResult
		byteBody         interface{}
	)
	bodyReader, _ := ioutil.ReadAll(request.Body)
	_ = json.Unmarshal(bodyReader, &byteBody)
	body, _ := json.Marshal(byteBody)

	_ = service.CreateIntegrationLog("OMS.Pro."+enum.AppIdYCH, "UpdateOrderPack", string(body), "", "")

	err = json.Unmarshal(bodyReader, &orderPackRequest)
	if err != nil {
		errMsg := "Decode ERROR - " + err.Error()
		service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "order_pack", orderPackRequest.OrderNo, string(body), errMsg, response)
		return
	}

	orderPackRequest, err = orderPackRequest.Validation()
	if err != nil {
		errMsg := "Validate Order Pack Request ERROR - " + err.Error()
		service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "order_pack", orderPackRequest.OrderNo, string(body), errMsg, response)
		return
	}

	//salesUpdateReq.TotalWeight = orderPackRequest.TotalWeight
	//salesUpdateReq.TotalVolume = orderPackRequest.TotalVolume
	//salesUpdateReq.TotalPackages = orderPackRequest.TotalPackages
	//
	//for _, packItemReq := range orderPackRequest.OrderItems {
	//	var orderDetailUpdateReq model.SalesOrderDetailUpdateRequest
	//
	//	orderDetailUpdateReq.Weight = packItemReq.Weight
	//	orderDetailUpdateReq.Volume = packItemReq.Volume
	//	orderDetailUpdateReq.BatchNo = packItemReq.BatchNo
	//	orderDetailUpdateReq.Reason = packItemReq.Reason // In original pack item doesn't have this field
	//
	//	salesUpdateReq.SalesOrderDetailsUpdateRequest = append(salesUpdateReq.SalesOrderDetailsUpdateRequest, &orderDetailUpdateReq)
	//}
	//
	//for _, packageReq := range orderPackRequest.PackingPackages {
	//	var (
	//		orderPackageUpdateReq    model.SalesOrderPackageUpdateRequest
	//		orderPackageUpdateReqInt interface{}
	//	)
	//
	//	orderPackageUpdateReq.PackageType = packageReq.PackageType
	//	orderPackageUpdateReq.CommodityType = packageReq.CommodityType
	//	orderPackageUpdateReq.Length = packageReq.Length
	//	orderPackageUpdateReq.Width = packageReq.Width
	//	orderPackageUpdateReq.Height = packageReq.Height
	//	orderPackageUpdateReq.Weight = packageReq.Weight
	//	orderPackageUpdateReq.Value = packageReq.Value
	//	orderPackageUpdateReq.Instruction = packageReq.Instruction
	//
	//	bytesOrderPackageUpdateReq, err := json.Marshal(orderPackageUpdateReq)
	//	if err != nil {
	//		res.Message = "Marshal ERROR - " + err.Error()
	//		byteRes, _ := json.Marshal(res)
	//		_ = service.InsertQueue(enum.AppIdYCH, enum.AppIdOMS, "order_pack", orderPackRequest.OrderNo, string(body), string(byteRes), string(body), "", true)
	//		aHelper.ErrorResponse(response, http.StatusInternalServerError,0, "Copy ERROR - "+err.Error())
	//		return
	//	}
	//
	//	err = json.Unmarshal(bytesOrderPackageUpdateReq, &orderPackageUpdateReqInt)
	//	if err != nil {
	//		res.Message = "Unmarshal ERROR - " + err.Error()
	//		byteRes, _ := json.Marshal(res)
	//		_ = service.InsertQueue(enum.AppIdYCH, enum.AppIdOMS, "order_pack", orderPackRequest.OrderNo, string(body), string(byteRes), string(body), "", true)
	//		aHelper.ErrorResponse(response, http.StatusInternalServerError,0, "Copy ERROR - "+err.Error())
	//		return
	//	}
	//	filter := bson.M{
	//		"doc_num":      orderPack.OrderNo,
	//		"doc_line_num": packageReq.PackageNo,
	//	}
	//	update := bson.M{"$set": orderPackageUpdateReqInt}
	//
	//	database.Database.Collection(setting.SalesOrderPackageTable).UpdateOne(ctx, filter,update)
	//
	//	salesUpdateReq.SalesOrderPackagesUpdateRequest = append(salesUpdateReq.SalesOrderPackagesUpdateRequest, &orderPackageUpdateReq)
	//}
	filterOrderPack := bson.M{"order_no": sHelper.RegexStringID(orderPackRequest.OrderNo)}

	_ = collection.FindOne(ctx, filterOrderPack).Decode(&orderPack)

	if orderPack.ID == primitive.NilObjectID {
		orderPack.ID = primitive.NewObjectID()

		err = copier.Copy(&orderPack, &orderPackRequest)
		if err != nil {
			errMsg := "Copy ERROR - " + err.Error()
			service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "order_pack", orderPackRequest.OrderNo, string(body), errMsg, response)
			return
		}

		if _, err = collection.InsertOne(ctx, orderPack); err != nil {
			errMsg := "Insert Order Pack ERROR - " + err.Error()
			service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "order_pack", orderPackRequest.OrderNo, string(body), errMsg, response)
			return
		}

		err = copier.Copy(&tmpOrderPack, &orderPack)
		if err != nil {
			errMsg := "Copy to temp Order Pack ERROR - " + err.Error()
			service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "order_pack", orderPackRequest.OrderNo, string(body), errMsg, response)
			return
		}

	} else {
		var (
			byteOrderPackInt    interface{}
			byteOrderPackReqInt interface{}
		)
		for _, pack := range orderPackRequest.PackingPackages {
			var (
				filter  bson.M
				update  bson.M
				packInt map[string]interface{}
			)
			filter = bson.M{
				"order_no": orderPackRequest.OrderNo,
				"packing_packages": bson.M{
					"$elemMatch": bson.M{
						"package_no": sHelper.RegexStringID(pack.PackageNo),
					},
				}}
			count, err := collection.CountDocuments(ctx, filter)
			if err != nil {
				errMsg := "Count Packing Packages ERROR - " + err.Error()
				service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "order_pack", orderPackRequest.OrderNo, string(body), errMsg, response)
				return
			}
			if count > 0 {
				packBytes, _ := bson.Marshal(pack)
				_ = bson.Unmarshal(packBytes, &packInt)
				packInt = sHelper.AddStringToKeyOfMap("packing_packages.$.", packInt)

				update = bson.M{
					"$set": packInt,
				}
			} else {
				update = bson.M{
					"$push": bson.M{
						"packing_packages": pack,
					},
				}
				filter = bson.M{
					"order_no": sHelper.RegexStringID(orderPackRequest.OrderNo),
				}
			}
			if _, err = collection.UpdateOne(ctx, filter, update); err != nil {
				errMsg := "Update element into Packing Packages array ERROR - " + err.Error()
				service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "order_pack", orderPackRequest.OrderNo, string(body), errMsg, response)
				return
			}

		}
		for _, item := range orderPackRequest.OrderItems {
			var (
				filter  bson.M
				update  bson.M
				itemInt map[string]interface{}
			)
			filter = bson.M{
				"order_no": orderPackRequest.OrderNo,
				"order_items": bson.M{
					"$elemMatch": bson.M{
						"line_no": sHelper.RegexStringID(item.LineNo),
					},
				}}
			count, err := collection.CountDocuments(ctx, filter)
			if err != nil {
				errMsg := "Count Order Item ERROR - " + err.Error()
				service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "order_pack", orderPackRequest.OrderNo, string(body), errMsg, response)
				return
			}
			if count > 0 {
				itemBytes, _ := bson.Marshal(item)
				_ = bson.Unmarshal(itemBytes, &itemInt)

				itemInt = sHelper.AddStringToKeyOfMap("order_items.$.", itemInt)

				update = bson.M{
					"$set": itemInt,
				}
			} else {
				update = bson.M{
					"$push": bson.M{
						"order_items": item,
					},
				}
				filter = bson.M{
					"order_no": sHelper.RegexStringID(orderPackRequest.OrderNo),
				}
			}
			if _, err = collection.UpdateOne(ctx, filter, update); err != nil {
				errMsg := "Update element into Order Item array ERROR - " + err.Error()
				service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "order_pack", orderPackRequest.OrderNo, string(body), errMsg, response)
				return
			}

		}

		_ = copier.Copy(&tmpOrderPack, &orderPack)

		_ = copier.Copy(&tmpOrderPack, &orderPackRequest)

		orderPackRequest.OrderItems = nil
		orderPackRequest.PackingPackages = nil
		orderPack.OrderItems = nil
		orderPack.PackingPackages = nil

		byteOrderPack, _ := bson.Marshal(orderPack)
		_ = bson.Unmarshal(byteOrderPack, &byteOrderPackInt)

		byteOrderPackReq, _ := bson.Marshal(orderPackRequest)
		_ = bson.Unmarshal(byteOrderPackReq, &byteOrderPackReqInt)

		//Update Sales Order.
		filter := bson.M{"order_no": sHelper.RegexStringID(orderPack.OrderNo)}

		update := bson.A{
			bson.M{"$set": byteOrderPackInt}, // Set Main model first
		}

		//check sales order request is empty.
		if string(byteOrderPackReq) != "{}" {
			update = append(update, bson.M{"$set": byteOrderPackReqInt}) // Set Request Interface second
		}

		if _, err = collection.UpdateOne(ctx, filter, update); err != nil {
			errMsg := "Update order pack ERROR - " + err.Error()
			service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "order_pack", orderPackRequest.OrderNo, string(body), errMsg, response)
			return
		}

	}

	var willUpdateReturnStatus = false
	for _, item := range tmpOrderPack.OrderItems {
		returnDetailStatus := enum.ReturnDetailStatus(item.Status).ToCorrectCase()

		if enum.ReturnStatus(returnDetailStatus) == enum.ReturnStatusAccepted {
			willUpdateReturnStatus = true
		}

		if err = service.HandleReturnDetailStatusChange(orderPackRequest.OrderNo, item.LineNo, returnDetailStatus); err != nil {
			errMsg := "Update Return order detail status ERROR - " + err.Error()
			service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "order_pack", orderPackRequest.OrderNo, string(body), errMsg, response)
			return
		}
	}

	if willUpdateReturnStatus {
		if _, err = service.HandleReturnStatusChange(orderPackRequest.OrderNo, enum.ReturnStatusAccepted); err != nil {
			errMsg := "Update Return Order status to [ACCEPTED] ERROR - " + err.Error()
			service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "order_pack", orderPackRequest.OrderNo, string(body), errMsg, response)
			return
		}
	}

	filter := bson.M{"order_no": sHelper.RegexStringID(orderPack.OrderNo)}

	if err = collection.FindOne(ctx, filter).Decode(&orderPack); err != nil {
		errMsg := "Find order pack ERROR - " + err.Error()
		service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "order_pack", orderPackRequest.OrderNo, string(body), errMsg, response)
		return
	}

	data := bson.M{
		"order_pack": orderPack,
	}

	msg := "Update Order Pack Info successfully!"

	res.Success = true
	res.Message = msg
	res.Data = data

	byteData, _ := json.Marshal(res)

	//byteMilestone, _ := json.Marshal(mileStoneRequest)

	_ = service.InsertQueue(enum.AppIdYCH, enum.AppIdOMS, "order_pack", orderPackRequest.OrderNo, string(body), string(byteData), string(body), "", true)
	aHelper.SuccessResponse(response, data, msg)
}

// Update SalesOrder Order Label URL godoc
// @Tags Web Hook
// @Summary Update Sales Order Label URL
// @Description Update Sales Order Label URL
// @Accept  json
// @Produce  json
// @Param body body model.RMALabelUrlRequest string "Update Sales Order Label URL"
// @Success 200 {object} model.SalesOrder
// @Header 200 {string} Token "qwerty"
// @Router /UpdateSalesOrderLabelUrl [put]
func UpdateSalesOrderLabelUrl(response http.ResponseWriter, request *http.Request) {
	defer aHelper.RecoverError(response, "UpdateSalesOrderLabelUrl")
	var (
		collection     *mongo.Collection
		salesOrder     model.SalesOrder
		rmaLabelUrlReq model.RMALabelUrlRequest
		res            model.ResponseResult
		byteBody       interface{}
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.SalesOrderTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	bodyReader, _ := ioutil.ReadAll(request.Body)
	_ = json.Unmarshal(bodyReader, &byteBody)
	body, _ := json.Marshal(byteBody)

	_ = service.CreateIntegrationLog("OMS.Pro."+enum.AppIdYCH, "UpdateSalesOrderLabelUrl", string(body), "", "")

	err = json.Unmarshal(bodyReader, &rmaLabelUrlReq)
	if err != nil {
		errMsg := "Decode Sales Order Label URL Input ERROR - " + err.Error()
		service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "label_url", rmaLabelUrlReq.OrderNo, string(body), errMsg, response)
		return
	}

	filter := bson.M{
		"doc_num": sHelper.RegexStringID(rmaLabelUrlReq.OrderNo),
		//"ref_no":  rmaLabelUrlReq.TransactionNo,
	}

	err = collection.FindOne(ctx, filter).Decode(&salesOrder)
	if err != nil {
		errMsg := "Find Sales Order ERROR - " + err.Error()
		service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "label_url", rmaLabelUrlReq.OrderNo, string(body), errMsg, response)
		return
	}

	//salesOrder.RefNo = rmaLabelUrlReq.TransactionNo
	salesOrder.LabelUrl = rmaLabelUrlReq.LabelUrl
	salesOrder.UpdatedDate = time.Now()

	update := bson.M{"$set": salesOrder}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		errMsg := "Update Label URL ERROR - " + err.Error()
		service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "label_url", rmaLabelUrlReq.OrderNo, string(body), errMsg, response)
		return
	}

	data := bson.M{"sales_order": salesOrder}

	msg := "Update Order Label URL successfully"

	res.Success = true
	res.Message = msg
	res.Data = data

	byteData, _ := json.Marshal(res)

	//byteMilestone, _ := json.Marshal(mileStoneRequest)

	_ = service.InsertQueue(enum.AppIdYCH, enum.AppIdOMS, "warehouse_cancel_order", rmaLabelUrlReq.OrderNo, string(body), string(byteData), string(body), "", true)
	aHelper.SuccessResponse(response, data, "Update Order Label URL successfully")
}

// Update Warehouse Cancel Order godoc
// @Tags Web Hook
// @Summary Update Warehouse Cancel Order
// @Description Update WareHouse Cancel Order
// @Accept  json
// @Produce  json
// @Param body body model.WarehouseCancelOrderRequest true "Update Warehouse Cancel Order"
// @Success 200 {object} model.WarehouseCancelOrder
// @Header 200 {string} Token "qwerty"
// @Router /WarehouseCancelOrder [PUT]
func UpdateWarehouseCancelOrder(response http.ResponseWriter, request *http.Request) {
	defer aHelper.RecoverError(response, "UpdateWarehouseCancelOrder")
	var (
		warehouseCancelOrder     model.WarehouseCancelOrder
		warehouseCancelOrderReq  model.WarehouseCancelOrderRequest
		orderCancellationRequest model.OrderCancellationRequest
		res                      model.ResponseResult
		byteBody                 interface{}
	)

	response.Header().Set("content-type", "application/json")

	_, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	bodyReader, _ := ioutil.ReadAll(request.Body)
	_ = json.Unmarshal(bodyReader, &byteBody)
	body, _ := json.Marshal(byteBody)

	_ = service.CreateIntegrationLog("OMS.Pro."+enum.AppIdYCH, "UpdateWarehouseCancelOrder", string(body), "", "")

	err = json.Unmarshal(bodyReader, &warehouseCancelOrderReq)
	if err != nil {
		errMsg := "Decode ERROR - " + err.Error()
		service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "warehouse_cancel_order", warehouseCancelOrderReq.OrderNo, string(body), errMsg, response)
		return
	}

	err = copier.Copy(&warehouseCancelOrder, &warehouseCancelOrderReq)

	orderCancellationRequest.BaseDocNum = warehouseCancelOrder.OrderNo

	orderCancellationRequest.ReasonId = "200"

	orderCancellationRequest.ReasonKey, _ = sHelper.GetKeyFromId(setting.ReasonTable, "reason_id", orderCancellationRequest.ReasonId)

	orderCancellationRequest.ReasonDescription = "Warehouse Shortage"

	for _, item := range warehouseCancelOrder.OrderItems {
		var orderCancellationDetail model.OrderCancellationDetailRequest

		orderCancellationDetail.BaseDocLineNum = item.LineNo
		orderCancellationDetail.Quantity = item.ShortageQty

		orderCancellationRequest.OrderCancellationDetailRequest = append(orderCancellationRequest.OrderCancellationDetailRequest, orderCancellationDetail)
	}

	orderCancellationResponse, err := service.CreateOrderCancellation(orderCancellationRequest, body)
	if err != nil {
		errMsg := "Create Cancellation ERROR - " + err.Error()
		service.WebHookErrorResponse(enum.AppIdYCH, enum.AppIdOMS, "warehouse_cancel_order", warehouseCancelOrderReq.OrderNo, string(body), errMsg, response)
		return
	}

	data := bson.M{"warehouse_cancel_order": warehouseCancelOrder, "order_cancellation": orderCancellationResponse}
	msg := "Warehouse Cancel Order"

	res.Success = true
	res.Message = msg
	res.Data = data

	byteRes, _ := json.Marshal(res)

	//byteMilestone, _ := json.Marshal(mileStoneRequest)

	//_ = service.InsertQueue(enum.AppIdOMS, enum.AppIdSIA, setting.OrderCancellationTable, orderCancellationResponse.DocNum, string(body),string(byteRes), string(body),"",false)

	_ = service.InsertQueue(enum.AppIdYCH, enum.AppIdOMS, "warehouse_cancel_order", warehouseCancelOrder.OrderNo, string(body), string(byteRes), string(body), "", true)
	aHelper.SuccessResponse(response, data, msg)
	return
}
