package api

import (
	"../setting"
	"context"
	"encoding/json"
	"net/http"
	"os"
	"strconv"
	"time"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateSalesOrderPayment godoc
// @Tags Sales Order Payment
// @Summary Add Sales Order Payment
// @Description Add Sales Order Payment
// @Accept  json
// @Produce  json
// @Param body body []model.SalesOrderPaymentRequest true "Add Sales Order Payment"
// @Success 200 {object} []model.SalesOrderPayment
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderPayment [POST]
func CreateSalesOrderPayment(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateSalesOrderPayment")
	var (
		collection        *mongo.Collection
		salesOrderPayment model.SalesOrderPayment
	)

	collection = database.Database.Collection(setting.SalesOrderPaymentTable)
	response.Header().Set("content-type", "application/json")
	json.NewDecoder(request.Body).Decode(&salesOrderPayment)

	currentTime := time.Now()
	salesOrderPayment.CreatedDate = currentTime
	salesOrderPayment.UpdatedDate = currentTime

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	result, err := collection.InsertOne(ctx, salesOrderPayment)
	if err != nil {
		json.NewEncoder(response).Encode(err.Error())
		return
	}
	id := result.InsertedID
	salesOrderPayment.ID = id.(primitive.ObjectID)
	json.NewEncoder(response).Encode(salesOrderPayment)
}

// CreateManySalesOrderPayment func
func CreateManySalesOrderPayment(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManySalesOrderPayment")
	var (
		collection         *mongo.Collection
		salesOrderPayments []model.SalesOrderPayment
	)
	collection = database.Database.Collection(setting.SalesOrderPaymentTable)
	response.Header().Set("content-type", "application/json")
	json.NewDecoder(request.Body).Decode(&salesOrderPayments)
	var ui []interface{}
	currentTime := time.Now()
	for _, salesOrderPayment := range salesOrderPayments {
		salesOrderPayment.ID = primitive.NewObjectID()
		salesOrderPayment.CreatedDate = currentTime
		salesOrderPayment.UpdatedDate = currentTime
		ui = append(ui, salesOrderPayment)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// GetSalesOrderPaymentByID godoc
// @Tags Sales Order Payment
// @Summary Get Sales Order Payment by Id
// @Description Get Sales Order Payment by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Order Payment need to be found"
// @Success 200 {object} model.SalesOrderPayment
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderPayment/{id} [get]
func GetSalesOrderPaymentByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSaleOrderPaymentByID")
	var (
		collection        *mongo.Collection
		salesOrderPayment model.SalesOrderPayment
	)
	collection = database.Database.Collection(setting.SalesOrderPaymentTable)

	response.Header().Set("content-type", "application/json")

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	err := collection.FindOne(ctx, filter).Decode(&salesOrderPayment)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesOrderPayment)
}

// GetSalesOrderPayments godoc
// @Tags Sales Order Payment
// @Summary Get all Sales Order Payment
// @Description Get all Sales Order Payment
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.SalesOrderPayment
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderPayment [get]
func GetSalesOrderPayments(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesOrderPayments")
	var (
		collection         *mongo.Collection
		salesOrderPayments []model.SalesOrderPayment
		query              = request.URL.Query()
		options            = options.Find()
	)
	collection = database.Database.Collection(setting.SalesOrderPaymentTable)

	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, err := strconv.ParseInt(query.Get("limit"), 10, 64)
	if limit <= 0 {
		limit = 100
	} else if limit >= 500 {
		limit = 500
	}
	options.SetLimit(limit)
	cursor, err := collection.Find(ctx, options)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var saleOrderPayment model.SalesOrderPayment
		cursor.Decode(&saleOrderPayment)
		salesOrderPayments = append(salesOrderPayments, saleOrderPayment)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesOrderPayments)
}

// UpdateSalesOrderPayment godoc
// @Tags Sales Order Payment
// @Summary Update Sales Order Payment
// @Description Update Sales Order Payment
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Order need to be found"
// @Param body body model.SalesOrderRequest true "Update Sales Order Payment"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderPayment/{id} [put]
func UpdateSalesOrderPayment(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateSaleOrderPayment")
	var (
		collection       *mongo.Collection
		saleOrderPayment model.SalesOrderPayment
	)
	collection = database.Database.Collection(setting.SalesOrderPaymentTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	json.NewDecoder(request.Body).Decode(&saleOrderPayment)
	saleOrderPayment.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": saleOrderPayment}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

func DeleteSoftSalesOrderPayment(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftSalesOrderPayment")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.SalesOrderPaymentTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
