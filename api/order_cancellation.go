package api

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"../model/enum"
	"../service"
	sHelper "../service/helper"
	"../setting"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateOrderCancellation godoc
// @Tags Order Cancellation
// @Summary Add Order Cancellation
// @Description Add Order Cancellation
// @Accept  json
// @Produce  json
// @Param body body model.OrderCancellationRequest true "Add Order Cancellation"
// @Success 200 {object} model.OrderCancellation
// @Header 200 {string} Token "qwerty"
// @Router /OrderCancellation [POST]
func CreateOrderCancellation(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateOrderCancellation")
	var (
		orderCancellationRequest model.OrderCancellationRequest
		byteBody                 interface{}
	)

	response.Header().Set("content-type", "application/json")

	bodyReader, _ := ioutil.ReadAll(request.Body)
	_ = json.Unmarshal(bodyReader, &byteBody)
	body, _ := json.Marshal(byteBody)

	//err = json.NewDecoder(request.Body).Decode(&orderCancellationRequest)

	err = json.Unmarshal(bodyReader, &orderCancellationRequest)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Input to Request Model ERROR - "+err.Error())
		return
	}

	err = orderCancellationRequest.Validation()
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Validation ERROR - "+err.Error())
		return
	}

	orderCancellationResponse, err := service.CreateOrderCancellation(orderCancellationRequest, body)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Create Cancellation ERROR - "+err.Error())
		return
	}

	data := bson.M{"order_cancellation": orderCancellationResponse}

	api_helper.SuccessResponse(response, data, "Create Order Cancellation successfully")
}

// GetOrderCancellationByID godoc
// @Tags Order Cancellation
// @Summary Get Order Cancellation by Id
// @Description Get Order Cancellation by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Order Cancellation need to be found"
// @Success 200 {object} model.OrderCancellation
// @Header 200 {string} Token "qwerty"
// @Router /OrderCancellation/{id} [get]
func GetOrderCancellationByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetOrderCancellationByID")
	var (
		collection                *mongo.Collection
		orderCancellationResponse model.OrderCancellationResponse
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.OrderCancellationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id)}
	//Get record from DB
	err := collection.FindOne(ctx, filter).Decode(&orderCancellationResponse)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Input to Model ERROR - "+err.Error())
		return
	}

	//Match with Id
	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.D{{"_id", bsonx.ObjectID(id)}},
			},
			},
		},
	}}
	//Look up for foreign key
	lookupCustomer := bson.D{
		{"$lookup", bson.D{
			{"from", setting.CustomerTable},
			{"localField", "customer_key"},
			{"foreignField", "_id"},
			{"as", setting.CustomerTable},
		},
		}}
	lookupCountry := bson.D{
		{"$lookup", bson.D{
			{"from", setting.CountryTable},
			{"localField", "country_key"},
			{"foreignField", "_id"},
			{"as", setting.CountryTable},
		},
		}}
	lookupCurrency := bson.D{
		{"$lookup", bson.D{
			{"from", setting.CurrencyTable},
			{"localField", "currency_key"},
			{"foreignField", "_id"},
			{"as", setting.CurrencyTable},
		},
		}}
	lookupTenant := bson.D{
		{"$lookup", bson.D{
			{"from", setting.TenantTable},
			{"localField", "tenant_key"},
			{"foreignField", "_id"},
			{"as", setting.TenantTable},
		},
		}}
	unwindCountry := bson.D{
		{"$unwind", bson.D{
			{"path", "$" + setting.CountryTable},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindCustomer := bson.D{
		{"$unwind", bson.D{
			{"path", "$" + setting.CustomerTable},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindCurrency := bson.D{
		{"$unwind", bson.D{
			{"path", "$" + setting.CurrencyTable},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindTenant := bson.D{
		{"$unwind", bson.D{
			{"path", "$" + setting.TenantTable},
			{"preserveNullAndEmptyArrays", true}},
		},
	}

	lookupDetail := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.OrderCancellationDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.OrderCancellationDetailTable + "s"},
		},
		}}

	lookupPayment := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.OrderCancellationPaymentTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.OrderCancellationPaymentTable + "s"},
		},
		}}
	lookupComment := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.OrderCancellationCommentTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.OrderCancellationCommentTable + "s"},
		},
		}}
	lookupShippingDetail := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.OrderCancellationShippingDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.OrderCancellationShippingDetailTable + "s"},
		},
		}}

	lookupSalesOrder := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderTable},
			{"localField", "base_doc_key"},
			{"foreignField", "_id"},
			{"as", setting.SalesOrderTable},
		},
		}}
	unwindSalesOrder := bson.D{
		{"$unwind", bson.D{
			{"path", "$" + setting.SalesOrderTable},
			{"preserveNullAndEmptyArrays", true}},
		},
	}

	lookupSalesOrderDetails := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderDetailTable},
			{"localField", "base_doc_key"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderDetailTable+"s"},
		},
		}}
	lookupSalesOrderCaptures := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderPaymentCaptureTable},
			{"localField", "base_doc_key"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderPaymentCaptureTable + "s"},
		},
		}}
	lookupSalesOrderRefunds := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderPaymentRefundTable},
			{"localField", "base_doc_key"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderPaymentRefundTable + "s"},
		},
		}}
	lookupSalesOrderReverses := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderPaymentReverseTable},
			{"localField", "base_doc_key"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderPaymentReverseTable + "s"},
		},
		}}

	addFields := bson.D{
		{"$addFields", bson.D{
			{"order_quantity", bson.D{
				{"$sum", "$"+setting.SalesOrderDetailTable+"s.quantity"},
			}},
			{"cancel_quantity", bson.D{
				{"$sum", "$"+setting.SalesOrderDetailTable+"s.cancel_quantity"},
			}},
			{"total_order_amount", "$" + setting.SalesOrderTable + ".total_after_tax"},
		}},
	}

	pipeLine := mongo.Pipeline{
		match,
		lookupCustomer,
		lookupCountry,
		lookupCurrency,
		lookupTenant,
		lookupDetail,
		lookupPayment,
		lookupComment,
		lookupSalesOrder,
		lookupSalesOrderDetails,
		lookupShippingDetail,
		lookupSalesOrderCaptures,
		lookupSalesOrderRefunds,
		lookupSalesOrderReverses,
		unwindSalesOrder,
		unwindCountry,
		unwindCustomer,
		unwindCurrency,
		unwindTenant,
		addFields,
	}

	cursor, err := collection.Aggregate(ctx, pipeLine) //findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR"+err.Error())
		return
	}
	defer cursor.Close(ctx)
	if cursor.Next(ctx) {
		err := cursor.Decode(&orderCancellationResponse)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Order Canncellation with Details ERROR"+err.Error())
			return
		}
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR"+err.Error())
		return
	}
	data := bson.M{
		"order_cancellation": orderCancellationResponse,
	}
	api_helper.SuccessResponse(response, data, "Get Order Cancellation by ID successfully")
}

// GetOrderCancellationBy Doc Num godoc
// @Tags Order Cancellation
// @Summary Get Order Cancellation by Doc num
// @Description Get Order Cancellation by Doc num
// @Accept  json
// @Produce  json
// @Param doc_num path string true "Doc Number of Order Cancellation need to be found"
// @Success 200 {object} model.OrderCancellation
// @Header 200 {string} Token "qwerty"
// @Router /OrderCancellation/doc_num/{doc_num} [get]
func GetOrderCancellationByDocNum(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetOrderCancellationByID")
	var (
		collection                *mongo.Collection
		orderCancellationResponse model.OrderCancellationResponse
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.OrderCancellationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	docNum, _ := mux.Vars(request)["doc_num"]

	//filter := bson.M{"doc_num": docNum}
	////Get record from DB
	//err := collection.FindOne(ctx, filter).Decode(&orderCancellationResponse)
	//if err != nil {
	//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Input to Model ERROR - "+err.Error())
	//	return
	//}

	//Match with Id
	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.D{{"doc_num", docNum}},
			},
			},
		},
	}}
	//Look up for foreign key
	lookupCustomer := bson.D{
		{"$lookup", bson.D{
			{"from", setting.CustomerTable},
			{"localField", "customer_key"},
			{"foreignField", "_id"},
			{"as", setting.CustomerTable},
		},
		}}
	lookupCountry := bson.D{
		{"$lookup", bson.D{
			{"from", setting.CountryTable},
			{"localField", "country_key"},
			{"foreignField", "_id"},
			{"as", setting.CountryTable},
		},
		}}
	lookupCurrency := bson.D{
		{"$lookup", bson.D{
			{"from", setting.CurrencyTable},
			{"localField", "currency_key"},
			{"foreignField", "_id"},
			{"as", setting.CurrencyTable},
		},
		}}
	lookupTenant := bson.D{
		{"$lookup", bson.D{
			{"from", setting.TenantTable},
			{"localField", "tenant_key"},
			{"foreignField", "_id"},
			{"as", setting.TenantTable},
		},
		}}
	unwindCountry := bson.D{
		{"$unwind", bson.D{
			{"path", "$" + setting.CountryTable},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindCustomer := bson.D{
		{"$unwind", bson.D{
			{"path", "$" + setting.CustomerTable},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindCurrency := bson.D{
		{"$unwind", bson.D{
			{"path", "$" + setting.CurrencyTable},
			{"preserveNullAndEmptyArrays", true}},
		},
	}
	unwindTenant := bson.D{
		{"$unwind", bson.D{
			{"path", "$" + setting.TenantTable},
			{"preserveNullAndEmptyArrays", true}},
		},
	}

	lookupDetail := bson.D{
		{
			"$lookup", bson.D{
			{"from", "order_cancellation_detail"},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", "order_cancellation_details"},
		},
		}}

	lookupPayment := bson.D{
		{
			"$lookup", bson.D{
			{"from", "order_cancellation_payment"},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", "order_cancellation_payments"},
		},
		}}

	lookupComment := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.OrderCancellationCommentTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.OrderCancellationCommentTable + "s"},
		},
		}}

	lookupShippingDetail := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.OrderCancellationShippingDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.OrderCancellationShippingDetailTable + "s"},
		},
		}}
	lookupSalesOrderCaptures := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderPaymentCaptureTable},
			{"localField", "base_doc_key"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderPaymentCaptureTable + "s"},
		},
		}}
	lookupSalesOrderRefunds := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderPaymentRefundTable},
			{"localField", "base_doc_key"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderPaymentRefundTable + "s"},
		},
		}}
	lookupSalesOrderReverses := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderPaymentReverseTable},
			{"localField", "base_doc_key"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderPaymentReverseTable + "s"},
		},
		}}

	lookupSalesOrder := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderTable},
			{"localField", "base_doc_key"},
			{"foreignField", "_id"},
			{"as", setting.SalesOrderTable},
		},
		}}
	unwindSalesOrder := bson.D{
		{"$unwind", bson.D{
			{"path", "$" + setting.SalesOrderTable},
			{"preserveNullAndEmptyArrays", true}},
		},
	}

	lookupSalesOrderDetails := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderDetailTable},
			{"localField", "base_doc_key"},
			{"foreignField", "doc_key"},
			{"as", setting.SalesOrderDetailTable + "s"},
		},
		}}

	addFields := bson.D{
		{"$addFields", bson.D{
			{"order_quantity", bson.D{
				{"$sum", "$"+setting.SalesOrderDetailTable+"s.quantity"},
			}},
			{"cancel_quantity", bson.D{
				{"$sum", "$"+setting.SalesOrderDetailTable+"s.cancel_quantity"},
			}},
			{"total_order_amount", "$" + setting.SalesOrderTable + ".total_after_tax"},
		}},
	}

	pipeLine := mongo.Pipeline{
		match,
		lookupCustomer,
		lookupCountry,
		lookupCurrency,
		lookupTenant,
		lookupDetail,
		lookupPayment,
		lookupComment,
		lookupSalesOrder,
		lookupSalesOrderDetails,
		lookupSalesOrderCaptures,
		lookupSalesOrderRefunds,
		lookupSalesOrderReverses,
		lookupShippingDetail,
		unwindSalesOrder,
		unwindCountry,
		unwindCustomer,
		unwindCurrency,
		unwindTenant,
		addFields,
	}

	cursor, err := collection.Aggregate(ctx, pipeLine) //findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR"+err.Error())
		return
	}
	defer cursor.Close(ctx)
	if cursor.Next(ctx) {

		err := cursor.Decode(&orderCancellationResponse)
		if err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Order Cancellation with Details ERROR"+err.Error())
			return
		}
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR"+err.Error())
		return
	}
	data := bson.M{
		"order_cancellation": orderCancellationResponse,
	}
	api_helper.SuccessResponse(response, data, "Get Order Cancellation by Doc Num successfully")
}

// GetOrderCancellations godoc
// @Tags Order Cancellation
// @Summary Get all Order Cancellation
// @Description Get all Order Cancellation
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Param status path string false "Status of Cancel Order .Refunded, Returned, Closed"
// @Param country_id path string false "Country Id. of Order Cancellation"
// @Param sales_channel_id path string false "Sales Channel Id of Order Cancellation."
// @Param keyword path string false "Keyword for Doc Number, Customer name and Email searching."
// @Success 200 {object} []model.OrderCancellation
// @Header 200 {string} Token "qwerty"
// @Router /OrderCancellation [get]
func GetOrderCancellations(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetOrderCancellations")
	var (
		collection                   *mongo.Collection
		allOrderCancellationResponse []bson.M //[]model.OrderCancellationResponse
		matchFilter                  bson.A
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.OrderCancellationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	status := request.URL.Query().Get("status")
	if status != "" {
		cancelStatus := enum.CancelOrderStatus(status)
		if cancelStatus, err = cancelStatus.IsValid(); err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
			return
		} else {
			if cancelStatus != enum.CancelOrderStatusAll {
				matchFilter = append(matchFilter, bson.D{{"doc_status", cancelStatus}})
			}
		}
	}

	countryId := request.URL.Query().Get("country_id")
	if countryId != "" {
		matchFilter = append(matchFilter, bson.D{{"country_id", sHelper.RegexStringID(countryId)}})
	}

	salesChannelId := request.URL.Query().Get("sales_channel_id")
	if salesChannelId != "" {
		matchFilter = append(matchFilter, bson.D{{"sales_channel_id", sHelper.RegexStringID(salesChannelId)}})
	}

	keyword := request.URL.Query().Get("keyword")
	if keyword != "" {
		or := bson.D{{Key: "$or", Value: bson.A{
			bson.D{{"doc_num", sHelper.RegexStringID(keyword)}},
			bson.D{{"customer_name", sHelper.RegexStringID(keyword)}},
			bson.D{{"email", sHelper.RegexStringID(keyword)}},
		}}}
		matchFilter = append(matchFilter, or)
	}

	if len(matchFilter) == 0 {
		matchFilter = bson.A{bson.D{{"_id", bson.D{{"$ne", bsonx.ObjectID(primitive.NilObjectID)}}}}}
	}

	andArray := bson.M{
		"$and": matchFilter,
	}

	//Match with Id matchFilter
	match := bson.D{{
		"$match", andArray,
	}}

	sort := bson.D{{"$sort", bson.D{{"doc_date", -1}}}}
	skip := bson.D{{"$skip", limit * (page - 1)}}
	lim := bson.D{{"$limit", limit}}
	//Match with Id
	//match := bson.D{{
	//	"$match", bson.D{
	//		{"$and", bson.A{
	//			bson.D{{"_id", bsonx.ObjectID(id)}},
	//		},
	//		},
	//	},
	//}}
	//Look up for foreign key
	lookupDetail := bson.D{
		{
			"$lookup", bson.D{
			{"from", "order_cancellation_detail"},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", "order_cancellation_details"},
		},
		}}

	lookupPayment := bson.D{
		{
			"$lookup", bson.D{
			{"from", "order_cancellation_payment"},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", "order_cancellation_payments"},
		},
		}}

	lookupComment := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.OrderCancellationCommentTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.OrderCancellationCommentTable + "s"},
		},
		}}

	lookupShippingDetail := bson.D{
		{
			"$lookup", bson.D{
			{"from", setting.OrderCancellationShippingDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.OrderCancellationShippingDetailTable + "s"},
		},
		}}

	lookupSalesOrderDetails := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SalesOrderDetailTable},
			{"localField", "base_doc_key"},
			{"foreignField", "doc_key"},
			{"as", "sales_order_details"},
		},
		}}
	sumSalesOrderDetailQuantity := bson.D{
		{"$addFields", bson.D{
			{"order_quantity", bson.D{
				{"$sum", "$sales_order_details.quantity"},
			}},
		}},
	}
	sumCancelQuantity := bson.D{
		{"$addFields", bson.D{
			{"cancel_quantity", bson.D{
				{"$sum", "$sales_order_details.cancel_quantity"},
			}},
		}},
	}
	projection := bson.D{
		{"$project", bson.M{
			"doc_num":          1,
			"base_doc_num":     1,
			"doc_date":         1,
			"doc_status":       1,
			"customer_name":    1,
			"country_id":       1,
			"sales_channel_id": 1,
			"order_quantity":   1,
			"cancel_quantity":  1,
			"total_after_tax":  1,
			"currency_id":      1,
			"order_date":       1,
		},
		},
	}

	pipeLine := mongo.Pipeline{
		match,
		sort,
		skip,
		lim,
		lookupDetail,
		lookupPayment,
		lookupComment,
		lookupShippingDetail,
		lookupSalesOrderDetails,
		sumSalesOrderDetailQuantity,
		sumCancelQuantity,
		projection,
	}

	cursor, err := collection.Aggregate(ctx, pipeLine) //findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR"+err.Error())
		return
	}
	defer cursor.Close(ctx)
	if err = cursor.All(ctx, &allOrderCancellationResponse); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Cancel Order with Details ERROR"+err.Error())
		return
	}

	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR"+err.Error())
		return
	}

	count, err := collection.CountDocuments(ctx, andArray)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Count ERROR"+err.Error())
		return
	}

	data := bson.M{
		"order_cancellation": allOrderCancellationResponse,
		"count":              count,
	}
	api_helper.SuccessResponse(response, data, "Get Order Cancellation successfully")
}

func ExportOrderCancellation(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetOrderCancellations")
	var (
		collection            *mongo.Collection
		allOrderCancellations []model.OrderCancellationExports
		matchFilter           bson.A
		matchProductCode      bson.D
		//brand          model.Brand
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.OrderCancellationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	status := request.URL.Query().Get("status")
	if status != "" {
		orderStatus := enum.OrderStatus(status)
		if orderStatus, err = orderStatus.IsValid(); err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
			return
		} else {
			if orderStatus != enum.OrderStatusAll {
				matchFilter = append(matchFilter, bson.D{{"order_status", orderStatus.ToCorrectCase()}})
			}
		}
	}

	countryId := request.URL.Query().Get("country_id")
	if countryId != "" {
		matchFilter = append(matchFilter, bson.D{{"country_id", sHelper.RegexStringID(countryId)}})
	}

	salesChannelId := request.URL.Query().Get("sales_channel_id")
	if salesChannelId != "" {
		matchFilter = append(matchFilter, bson.D{{"sales_channel_id", sHelper.RegexStringID(salesChannelId)}})
	}

	email := request.URL.Query().Get("email")
	if email != "" {
		matchFilter = append(matchFilter, bson.D{{"email", sHelper.RegexStringID(email)}})
	}

	ean := request.URL.Query().Get("ean")
	if ean != "" {
		matchProductCode = bson.D{
			{"$match", bson.M{
				setting.OrderCancellationDetailTable + "s": bson.M{
					"$elemMatch": bson.M{
						"product_code": sHelper.RegexStringID(ean),
					},
				},
			}},
		}
	} else {
		matchProductCode = bson.D{
			{"$match", bson.M{
				"_id": bson.M{"$ne": bsonx.ObjectID(primitive.NilObjectID)},
			}},
		}
	}

	keyword := request.URL.Query().Get("keyword")
	if keyword != "" {
		or := bson.D{{Key: "$or", Value: bson.A{
			bson.D{{"doc_num", sHelper.RegexStringID(keyword)}},
			bson.D{{"customer_name", sHelper.RegexStringID(keyword)}},
			bson.D{{"email", sHelper.RegexStringID(keyword)}},
		}}}
		matchFilter = append(matchFilter, or)
	}

	fromDate, toDate, err := api_helper.GetFromAndToDate(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get From and To Date ERROR - "+err.Error())
		return
	}

	matchFilter = append(matchFilter, bson.M{
		"doc_date": bson.M{
			"$gte": time.Unix(fromDate, 0),
			"$lte": time.Unix(toDate, 0),
		},
	})

	if len(matchFilter) == 0 {
		matchFilter = bson.A{bson.D{{"_id", bson.D{{"$ne", bsonx.ObjectID(primitive.NilObjectID)}}}}}
	}

	//Match with Id matchFilter
	match := bson.D{{
		"$match", bson.D{
			{"$and", matchFilter},
		},
	}}
	//sort, skip and limit
	sort := bson.D{{"$sort", bson.D{{"doc_date", -1}}}}
	//Look up for foreign key
	lookupOrderCancellationDetails := bson.D{
		{"$lookup", bson.D{
			{"from", setting.OrderCancellationDetailTable},
			{"localField", "_id"},
			{"foreignField", "doc_key"},
			{"as", setting.OrderCancellationDetailTable + "s"},
		},
		}}

	detailStr := "$" + setting.OrderCancellationDetailTable + "s"
	//Unwind array to object
	unwindOrderCancellationDetail := bson.D{
		{"$unwind", bson.D{
			{"path", detailStr},
			{"preserveNullAndEmptyArrays", true}},
		},
	}

	projection := bson.D{
		{"$project", bson.D{
			{"_id", 0},
			{"cma_no", "$doc_num"},
			{"order_no", "$base_doc_num"},
			{"cancel_date", "$doc_date"},
			{"order_date", "$order_date"},
			{"customer", "$customer_name"},
			{"country", "$country_id"},
			{"sales_channel", "$sales_channel_id"},
			{"cancel_status", "$doc_status"},
			{"cancel_amount", "$total_after_tax"},
			{"ean", detailStr + ".product_code"},
			{"description", detailStr + ".description"},
			{"size", detailStr + ".product_size"},
			{"color", detailStr + ".product_color"},
			{"base_price", detailStr + ".price"},
			{"line_status", detailStr + ".doc_line_status"},
			{"order_quantity", detailStr + ".order_quantity"},
			{"cancel_quantity", detailStr + ".quantity"},
			{"discount_amount", detailStr + ".line_discount_amount"},
			{"shipping_fee", detailStr + ".freight_amount"},
			{"sub_total", bson.M{
				"$subtract": bson.A{
					bson.M{
						"$multiply": bson.A{
							detailStr + ".quantity",
							detailStr + ".price",
						}},
					detailStr + ".line_discount_amount",
				},
			}},
		}},
	}

	pipeline := mongo.Pipeline{
		match,
		lookupOrderCancellationDetails,
		matchProductCode,
		sort,
		unwindOrderCancellationDetail,
		projection,
	}

	cursor, err := collection.Aggregate(ctx, pipeline) //findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	if err = cursor.All(ctx, &allOrderCancellations); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"sales_order": allOrderCancellations}
	api_helper.SuccessResponse(response, data, "Export OrderCancellation Order successfully")
}

// Get count Order Cancellation godoc
// @Tags Order Cancellation
// @Summary Get count Order Cancellation
// @Description Get count Order Cancellation
// @Accept  json
// @Produce  json
// @Param status path string false "Status of order . Refunded, Returned, Closed"
// @Param country_id path string false "Country Id. of Order Cancellation"
// @Param sales_channel_id path string false "Sales Channel Id of Order Cancellation."
// @Param keyword path string false "Keyword for Doc Number, Customer name and Email searching."
// @Success 200 {object} model.Count
// @Header 200 {string} Token "qwerty"
// @Router /OrderCancellation/count [get]
func GetCountOrderCancellation(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetCountOrderCancellation")

	var (
		collection  *mongo.Collection
		matchFilter = bson.D{}
		andArray    = bson.A{
			bson.M{"_id": bson.M{"$ne": bsonx.ObjectID(primitive.NilObjectID)}},
		}
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.OrderCancellationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	status := request.URL.Query().Get("status")
	keyword := request.URL.Query().Get("keyword")
	if keyword != "" {
		or := bson.D{{Key: "$or", Value: bson.A{
			bson.D{{"doc_num", sHelper.RegexStringID(keyword)}},
			bson.D{{"customer_name", sHelper.RegexStringID(keyword)}},
			bson.D{{"email", sHelper.RegexStringID(keyword)}},
		}}}
		andArray = append(andArray, or)
	}
	if status != "" {
		orderStatus := enum.OrderStatus(status)
		if orderStatus, err = orderStatus.IsValid(); err != nil {
			api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
			return
		} else {
			if orderStatus != enum.OrderStatusAll {
				andArray = append(andArray, bson.D{{"order_status", orderStatus.ToCorrectCase()}})
			}
		}
	}

	countryId := request.URL.Query().Get("country_id")
	if countryId != "" {
		//filter := bson.M{"country_id": countryId}
		//
		//collection = database.Database.Collection(setting.CountryTable)
		//err = collection.FindOne(ctx, filter).Decode(&country)
		//if err != nil {
		//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Country object ERROR - "+err.Error())
		//	return
		//}
		andArray = append(andArray, bson.D{{"country_id", sHelper.RegexStringID(countryId)}})
	}

	salesChannelId := request.URL.Query().Get("sales_channel_id")
	if salesChannelId != "" {
		//filter := bson.M{"sales_channel_id": salesChannelId}
		//
		//collection = database.Database.Collection(setting.SalesChannelTable)
		//err = collection.FindOne(ctx, filter).Decode(&salesChannel)
		//if err != nil {
		//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Sales Channel object ERROR - "+err.Error())
		//	return
		//}
		andArray = append(andArray, bson.D{{"sales_channel_id", sHelper.RegexStringID(salesChannelId)}})
	}

	//brandId := request.URL.Query().Get("brand_id")
	//if brandId != "" {
	//	collection = database.Database.Collection(setting.SalesChannelTable)
	//	err = collection.FindOne(ctx, bson.M{"brand_id": brandId}).Decode(&brand)
	//	if err != nil {
	//		api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Decode Sales Channel object ERROR - "+err.Error())
	//		return
	//	}
	//	matchFilter = append(matchFilter, bson.D{{"brand_key",brand.ID}})
	//}

	count, err := collection.CountDocuments(ctx, matchFilter)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Count document ERROR - "+err.Error())
		return
	}

	data := bson.M{"count": count}
	api_helper.SuccessResponse(response, data, "Get count Order Cancellation successfully")
}

//------------------------------ Unused ----------------------------------

//------------------ Unused ---------------------
// CreateManyOrderCancellation func
func CreateManyOrderCancellation(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManyOrderCancellation")
	var (
		collection                *mongo.Collection
		orderCancellationsRequest []model.OrderCancellationRequest
		uiOrderCancellation       []interface{}
		uiOrderCancellationDetail []interface{}
	)

	collection = database.Database.Collection(setting.OrderCancellationTable)
	response.Header().Set("content-type", "application/json")

	err = json.NewDecoder(request.Body).Decode(&orderCancellationsRequest)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	currentTime := time.Now()
	for _, orderCancellationRequest := range orderCancellationsRequest {
		var (
			orderCancellation model.OrderCancellation
		)
		copier.Copy(&orderCancellation, &orderCancellationRequest)

		orderCancellation.ID = primitive.NewObjectID()
		orderCancellation.CreatedDate = currentTime
		orderCancellation.UpdatedDate = currentTime
		uiOrderCancellation = append(uiOrderCancellation, orderCancellation)

		// Order Cancellation Detail
		for _, detail := range orderCancellationRequest.OrderCancellationDetailRequest {
			var (
				orderCancellationDetail model.OrderCancellationDetail
			)
			copier.Copy(&orderCancellationDetail, &detail)
			orderCancellationDetail.DocKey = orderCancellation.ID
			orderCancellationDetail.ID = primitive.NewObjectID()
			orderCancellationDetail.CreatedDate = currentTime
			orderCancellationDetail.UpdatedDate = currentTime
			uiOrderCancellationDetail = append(uiOrderCancellationDetail, orderCancellationDetail)
		}

	}

	// Order Cancellation
	_, err = collection.InsertMany(context.TODO(), uiOrderCancellation)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	if len(uiOrderCancellationDetail) > 0 {
		collection = database.Database.Collection(setting.OrderCancellationDetailTable)
		// Order Cancellation Detail
		_, err = collection.InsertMany(context.TODO(), uiOrderCancellationDetail)
		if err != nil {
			response.WriteHeader(http.StatusInternalServerError)
			response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
			return
		}
	} else {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + "Please enter at least one line for order cancellation detail." + `" }`))
		return
	}

	json.NewEncoder(response).Encode(map[string]interface{}{
		"order_cancellation":        uiOrderCancellation,
		"order_cancellation_detail": uiOrderCancellationDetail,
	})
}

// UpdateOrderCancellation godoc
// @Tags Order Cancellation
// @Summary Update Order Cancellation
// @Description Update Order Cancellation
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Order Cancellation need to be found"
// @Param body body model.OrderCancellationRequest true "Update Order Cancellation"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /OrderCancellation/{id} [put]
func UpdateOrderCancellation(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateOrderCancellation")
	var (
		collection                *mongo.Collection
		orderCancellationRequest  model.OrderCancellationRequest
		orderCancellation         model.OrderCancellation
		uiOrderCancellationDetail []interface{}
	)
	collection = database.Database.Collection(setting.OrderCancellationTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&orderCancellationRequest)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	copier.Copy(&orderCancellation, &orderCancellationRequest)
	currentTime := time.Now()
	orderCancellation.UpdatedDate = currentTime

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": orderCancellation}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	// Order Cancellation Detail
	if len(orderCancellationRequest.OrderCancellationDetailRequest) > 0 {
		collection = database.Database.Collection(setting.OrderCancellationDetailTable)

		for _, detail := range orderCancellationRequest.OrderCancellationDetailRequest {
			var (
				orderCancellationDetail model.OrderCancellationDetail
			)
			copier.Copy(&orderCancellationDetail, &detail)
			orderCancellationDetail.UpdatedDate = currentTime

			if orderCancellationDetail.ID != primitive.NilObjectID {
				filter = bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(orderCancellationDetail.ID)}}
				update = bson.M{"$set": orderCancellationDetail}
				result, err = collection.UpdateOne(ctx, filter, update)
				if err != nil {
					response.WriteHeader(http.StatusInternalServerError)
					response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
					return
				}
			} else {
				uiOrderCancellationDetail = append(uiOrderCancellationDetail, orderCancellationDetail)
			}
		}

		// Insert New
		if len(uiOrderCancellationDetail) > 0 {
			collection = database.Database.Collection(setting.OrderCancellationDetailTable)
			// Order Cancellation Detail
			_, err = collection.InsertMany(context.TODO(), uiOrderCancellationDetail)
			if err != nil {
				response.WriteHeader(http.StatusInternalServerError)
				response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
				return
			}
		}
	}

	json.NewEncoder(response).Encode(result)
}

// DeleteSoftOrderCancellation godoc
// @Tags Order Cancellation
// @Summary Soft Delete Order Cancellation
// @Description Soft Order Cancellation
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Order Cancellation need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /OrderCancellation/{id} [delete]
func DeleteSoftOrderCancellation(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftOrderCancellation")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.OrderCancellationTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
