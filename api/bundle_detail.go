package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../database"
	"../model"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// Add BundleDetail godoc
// @Tags BundleDetail
// @Summary Add BundleDetail
// @Description Add BundleDetail
// @Accept  json
// @Produce  json
// @Param body body model.BundleDetailRequest true "Add BundleDetail"
// @Success 200 {object} model.BundleDetail
// @Header 200 {string} Token "qwerty"
// @Router /BundleDetail [POST]
func CreateBundleDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBundleDetail")
	var (
		collection      *mongo.Collection
		bundleDetail    model.BundleDetail
		bundleDetailReq model.BundleDetailRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.BundleDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&bundleDetailReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&bundleDetail, &bundleDetailReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set Date = time.Now()
	currentTime := time.Now()
	bundleDetail.ID = primitive.NewObjectID()
	bundleDetail.CreatedDate = currentTime
	bundleDetail.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, bundleDetail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	data := bson.M{"bundle_detail": bundleDetail}
	api_helper.SuccessResponse(response, data, "Create Bundle Detail successfully")
}

// Get BundleDetail By Id godoc
// @Tags BundleDetail
// @Summary Get BundleDetail by Id
// @Description Get BundleDetail by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of BundleDetail need to be found"
// @Success 200 {object} model.BundleDetail
// @Header 200 {string} Token "qwerty"
// @Router /BundleDetail/{id} [get]
func GetBundleDetailById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetBundleDetailById")
	var (
		collection   *mongo.Collection
		bundleDetail model.BundleDetail
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.BundleDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	err := collection.FindOne(ctx, filter).Decode(&bundleDetail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	data := bson.M{"bundle_detail": bundleDetail}
	api_helper.SuccessResponse(response, data, "Get Bundle Detail successfully")
}

// Get number of BundleDetails godoc
// @Tags BundleDetail
// @Summary Get number of BundleDetails sort by created_date field descending
// @Description Get number of BundleDetails depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.BundleDetail
// @Header 200 {string} Token "qwerty"
// @Router /BundleDetail [get]
func GetBundleDetails(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetBundleDetails")
	var (
		bundleDetail    model.BundleDetail
		allBundleDetail []model.BundleDetail
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.BundleDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{"is_deleted": false}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		_ = cursor.Decode(&bundleDetail)
		allBundleDetail = append(allBundleDetail, bundleDetail)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"bundle_detail": allBundleDetail}
	api_helper.SuccessResponse(response, data, "Get Bundle Detail successfully")
}

// Update BundleDetail godoc
// @Tags BundleDetail
// @Summary Update BundleDetail
// @Description Update BundleDetail
// @Accept  json
// @Produce  json
// @Param id path string true "Id of BundleDetail need to be found"
// @Param body body model.BundleDetailRequest true "Update BundleDetail"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /BundleDetail/{id} [put]
func UpdateBundleDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var (
		collection      *mongo.Collection
		bundleDetail    model.BundleDetail
		bundleDetailReq model.BundleDetailRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.BundleDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err = collection.FindOne(ctx, filter).Decode(&bundleDetail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = json.NewDecoder(request.Body).Decode(&bundleDetailReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&bundleDetail, &bundleDetailReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set lai thoi gian luc update
	bundleDetail.UpdatedDate = time.Now()

	update := bson.M{"$set": bundleDetail}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"bundle": bundleDetail}
	api_helper.SuccessResponse(response, data, "Update Bundle Detail Successfully")
}

// Soft Delete BundleDetail godoc
// @Tags BundleDetail
// @Summary Soft Delete BundleDetail
// @Description Soft Delete BundleDetail
// @Accept  json
// @Produce  json
// @Param id path string true "Id of BundleDetail need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /BundleDetail/{id} [delete]
func DeleteSoftBundleDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteSoftBundleDetail")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.BundleDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	api_helper.SuccessResponse(response, id, "Delete Bundle Detail Successfully")
}

//-------------------- Unused -----------------------
func CreateManyBundleDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyBundleDetail")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.BundleDetailTable)
	response.Header().Set("content-type", "application/json")
	var manyBundleDetail []model.BundleDetail
	err = json.NewDecoder(request.Body).Decode(&manyBundleDetail)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, bundleDetail := range manyBundleDetail {
		bundleDetail.ID = primitive.NewObjectID()
		bundleDetail.CreatedDate = time.Now()
		bundleDetail.UpdatedDate = time.Now()
		ui = append(ui, bundleDetail)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

//DeleteHardBundleDetail
/*
func DeleteHardBundleDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardBundleDetail")
	var collection *mongo.Collection
	collection = database.Database.Collection("bundleDetail")
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
