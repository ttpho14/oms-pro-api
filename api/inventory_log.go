package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"reflect"
	"time"

	"../database"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

func CreateInventoryLog(invLog model.InventoryLog) (model.InventoryLog, error) {
	currentTime := time.Now()
	invLog.ID = primitive.NewObjectID()
	invLog.LogDate = currentTime
	invLog.CreatedDate = currentTime
	invLog.UpdatedDate = currentTime

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	collection := database.Database.Collection(setting.InventoryLogTable)
	_, err := collection.InsertOne(ctx, invLog)
	if err != nil {
		return model.InventoryLog{}, err
	}
	return invLog, nil
}

// Get number of Inventory Logs godoc
// @Tags Inventory Log
// @Summary Get number of Inventory Logs sort by created_date field descending
// @Description Get number of Inventory Logs depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.InventoryLog
// @Header 200 {string} Token "qwerty"
// @Router /InventoryLog [get]
func GetInventoryLogs(response http.ResponseWriter, request *http.Request) {
	//Get Limit and Page
	var (
		inventoryLog    model.InventoryLog
		allInventoryLog []model.InventoryLog
		filter          = bson.D{}
	)

	response.Header().Set("content-type", "application/json")
	collection := database.Database.Collection(setting.InventoryLogTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	productId := request.URL.Query().Get("product_id")
	if productId != "" {
		filter = append(filter, bson.E{Key: "product_id", Value: productId})
	}

	siteId := request.URL.Query().Get("site_id")
	if siteId != "" {
		filter = append(filter, bson.E{Key: "site_id", Value: siteId})
	}

	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		cursor.Decode(&inventoryLog)
		allInventoryLog = append(allInventoryLog, inventoryLog)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"inventory_log": allInventoryLog}
	api_helper.SuccessResponse(response, data, "Get Inventory Log successfully")
}

func GetCountInventoryLogs(response http.ResponseWriter, request *http.Request) {
	//Get Limit and Page
	var (
		filter = bson.D{}
	)

	response.Header().Set("content-type", "application/json")
	collection := database.Database.Collection(setting.InventoryLogTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	productId := request.URL.Query().Get("product_id")
	if productId != "" {
		filter = append(filter, bson.E{Key: "product_id", Value: productId})
	}

	siteId := request.URL.Query().Get("site_id")
	if siteId != "" {
		filter = append(filter, bson.E{Key: "site_id", Value: siteId})
	}

	// Set page
	count, err := collection.CountDocuments(ctx, filter)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	data := bson.M{"count": count}
	api_helper.SuccessResponse(response, data, "Get Count Inventory Log successfully")
}

//-------------------- Unused -----------------------

func CreateManyInventoryLog(response http.ResponseWriter, request *http.Request) {
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.InventoryLogTable)
	response.Header().Set("content-type", "application/json")
	var manyInventoryLog []model.InventoryLog
	err = json.NewDecoder(request.Body).Decode(&manyInventoryLog)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, inventoryLog := range manyInventoryLog {
		inventoryLog.ID = primitive.NewObjectID()
		inventoryLog.CreatedDate = time.Now()
		inventoryLog.UpdatedDate = time.Now()
		ui = append(ui, inventoryLog)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

func GetInventoryLogById(response http.ResponseWriter, request *http.Request) {
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.InventoryLogTable)
	response.Header().Set("content-type", "application/json")
	var inventoryLog model.InventoryLog
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//_id = id and is_deleted = false
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	err := collection.FindOne(ctx, filter).Decode(&inventoryLog)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(inventoryLog)
}

func UpdateInventoryLog(response http.ResponseWriter, request *http.Request) {
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.InventoryLogTable)
	response.Header().Set("content-type", "application/json")
	var inventoryLog model.InventoryLog
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	err = json.NewDecoder(request.Body).Decode(&inventoryLog)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	//Set lai thoi gian luc update
	inventoryLog.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": inventoryLog}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

func DeleteSoftInventoryLog(response http.ResponseWriter, request *http.Request) {
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.InventoryLogTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("UpdateOne() result:", result)
		fmt.Println("UpdateOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("UpdateOne() result MatchedCount:", result.MatchedCount)
		fmt.Println("UpdateOne() result ModifiedCount:", result.ModifiedCount)
		fmt.Println("UpdateOne() result UpsertedCount:", result.UpsertedCount)
		fmt.Println("UpdateOne() result UpsertedID:", result.UpsertedID)
	}
	json.NewEncoder(response).Encode(result)
}

//DeleteHardInventoryLog
/*
func DeleteHardInventoryLog(response http.ResponseWriter, request *http.Request) {
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.InventoryLogTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
