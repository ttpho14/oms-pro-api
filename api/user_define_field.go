package api

import (
	"../database"
	"../setting"
	"./api_helper"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"net/http"
	"os"
	"reflect"
	"time"

	"../model"
)

// Add UserDefineField godoc
// @Tags User Define Field
// @Summary Add UserDefineField
// @Description Add UserDefineField
// @Accept  json
// @Produce  json
// @Param body body []model.AddUserDefineField true "Add UserDefineField"
// @Success 200 {object} []model.UserDefineField
// @Header 200 {string} Token "qwerty"
// @Router /UserDefineField [POST]
func CreateUserDefineField(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.UserDefineFieldTable)
	response.Header().Set("content-type", "application/json")
	var userDefineField model.UserDefineField
	err = json.NewDecoder(request.Body).Decode(&userDefineField)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	//Set Date = time.Now()
	userDefineField.CreatedDate = time.Now()
	userDefineField.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	result, err := collection.InsertOne(ctx, userDefineField)
	if err != nil {
		json.NewEncoder(response).Encode(err.Error())
		return
	}
	id := result.InsertedID
	userDefineField.ID = id.(primitive.ObjectID)
	json.NewEncoder(response).Encode(userDefineField)
}

func CreateManyUserDefineField(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.UserDefineFieldTable)
	response.Header().Set("content-type", "application/json")
	var manyUserDefineField []model.UserDefineField
	err = json.NewDecoder(request.Body).Decode(&manyUserDefineField)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, userDefineField := range manyUserDefineField {
		userDefineField.ID = primitive.NewObjectID()
		userDefineField.CreatedDate = time.Now()
		userDefineField.UpdatedDate = time.Now()
		ui = append(ui, userDefineField)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// Get UserDefineField By Id godoc
// @Tags User Define Field
// @Summary Get UserDefineField by Id
// @Description Get UserDefineField by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of UserDefineField need to be found"
// @Success 200 {object} model.UserDefineField
// @Header 200 {string} Token "qwerty"
// @Router /UserDefineField/{id} [get]
func GetUserDefineFieldById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetUserDefineFieldById")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.UserDefineFieldTable)
	response.Header().Set("content-type", "application/json")
	var userDefineField model.UserDefineField
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//_id = id and is_deleted = false
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}, "is_deleted": bson.M{"$eq": false}}
	err := collection.FindOne(ctx, filter).Decode(&userDefineField)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(userDefineField)
}

// Get number of User Define Fields godoc
// @Tags User Define Field
// @Summary Get number of User Define Fields sort by created_date field descending
// @Description Get number of User Define Fields depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.UserDefineField
// @Header 200 {string} Token "qwerty"
// @Router /UserDefineField [get]
func GetUserDefineFields(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetUserDefineFields")
	//Get Limit and Page
	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	collection := database.Database.Collection(setting.UserDefineFieldTable)
	response.Header().Set("content-type", "application/json")
	var allUserDefineField []model.UserDefineField
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"is_deleted": bson.M{"$eq": false}}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var userDefineField model.UserDefineField
		cursor.Decode(&userDefineField)
		allUserDefineField = append(allUserDefineField, userDefineField)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(allUserDefineField)
}

// Update UserDefineField godoc
// @Tags User Define Field
// @Summary Update UserDefineField
// @Description Update UserDefineField
// @Accept  json
// @Produce  json
// @Param id path string true "Id of UserDefineField need to be found"
// @Param body body model.AddUserDefineField true "Update UserDefineField"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /UserDefineField/{id} [put]
func UpdateUserDefineField(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "UpdateUserDefineField")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.UserDefineFieldTable)
	response.Header().Set("content-type", "application/json")
	var userDefineField model.UserDefineField
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	err = json.NewDecoder(request.Body).Decode(&userDefineField)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	//Set lai thoi gian luc update
	userDefineField.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	fmt.Println("filter UserDefineField", filter)
	update := bson.M{"$set": userDefineField}
	result, err := collection.UpdateOne(ctx, filter, update)
	fmt.Println("Result UserDefineField", result)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

// Soft Delete UserDefineField godoc
// @Tags User Define Field
// @Summary Soft Delete UserDefineField
// @Description Soft Delete UserDefineField
// @Accept  json
// @Produce  json
// @Param id path string true "Id of UserDefineField need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /UserDefineField/{id} [delete]
func DeleteSoftUserDefineField(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteSoftUserDefineField")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.UserDefineFieldTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("UpdateOne() result:", result)
		fmt.Println("UpdateOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("UpdateOne() result MatchedCount:", result.MatchedCount)
		fmt.Println("UpdateOne() result ModifiedCount:", result.ModifiedCount)
		fmt.Println("UpdateOne() result UpsertedCount:", result.UpsertedCount)
		fmt.Println("UpdateOne() result UpsertedID:", result.UpsertedID)
	}
	json.NewEncoder(response).Encode(result)
}

//DeleteHardUserDefineField
/*
func DeleteHardUserDefineField(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.UserDefineFieldTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
//-------------------- REGION API - END -----------------------
