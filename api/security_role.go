package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"../database"
	"../service"
	"../setting"
	help "./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

// Add Security Role godoc
// @Tags Security Role
// @Summary Add Security Role
// @Description Add Security Role
// @Accept  json
// @Produce  json
// @Param body body model.SecurityRoleRequest true "Add Security Role"
// @Success 200 {object} model.SecurityRole
// @Header 200 {string} Token "qwerty"
// @Router /SecurityRole [POST]
func CreateSecurityRole(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "CreateSecurityRole")
	var (
		collection      *mongo.Collection
		securityRole    model.SecurityRole
		securityRoleReq model.SecurityRoleRequest
		securityRoleRes model.SecurityRoleResponse
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.SecurityRoleTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&securityRoleReq)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	if err = securityRoleReq.Validation(); err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Validation ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&securityRole, &securityRoleReq)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set Date = time.Now()
	currentTime := time.Now()

	securityRole.ID = primitive.NewObjectID()
	securityRole.CreatedDate = currentTime
	securityRole.UpdatedDate = currentTime

	defaultListDetail, err := service.DefaultSecurityDetailList(securityRole.ID)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Create default Security Detail list ERROR - "+err.Error())
		return
	}

	_ = copier.Copy(&securityRoleRes, &securityRole)

	for _, detail := range defaultListDetail {
		var securityDetailRes model.SecurityDetailResponse

		if _, err = database.Database.Collection(setting.SecurityDetailTable).InsertOne(ctx, detail); err != nil {
			help.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Security Detail ERROR - "+err.Error())
			return
		}

		_ = copier.Copy(&securityDetailRes, &detail)

		securityRoleRes.SecurityDetail = append(securityRoleRes.SecurityDetail, securityDetailRes)
	}

	_, err = collection.InsertOne(ctx, securityRole)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Security Role ERROR - "+err.Error())
		return
	}

	data := bson.M{"security_role": securityRoleRes}
	help.SuccessResponse(response, data, "Create Security Role successfully")
}

// Get SecurityRole By Id godoc
// @Tags Security Role
// @Summary Get Security Role by Id
// @Description Get Security Role by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Security Role need to be found"
// @Success 200 {object} model.SecurityRole
// @Header 200 {string} Token "qwerty"
// @Router /SecurityRole/{id} [get]
func GetSecurityRoleByKey(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "GetSecurityRoleByKey")
	var (
		collection      *mongo.Collection
		securityRoleRes model.SecurityRoleResponse
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.SecurityRoleTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//Match with Id
	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.D{{"_id", bsonx.ObjectID(id)}},
			},
			},
		},
	}}
	//Look up for foreign key
	lookupDetail := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SecurityDetailTable},
			{"localField", "_id"},
			{"foreignField", "security_key"},
			{"as", setting.SecurityDetailTable + "s"},
		},
		}}

	pipeline := mongo.Pipeline{
		match,
		lookupDetail,
	}

	cursor, err := collection.Aggregate(ctx, pipeline)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR"+err.Error())
		return
	}
	defer cursor.Close(ctx)
	if cursor.Next(ctx) {
		err := cursor.Decode(&securityRoleRes)
		if err != nil {
			help.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Security Role with details ERROR"+err.Error())
			return
		}
		//salesOrders = append(salesOrders, salesOrder)
	}
	if err := cursor.Err(); err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR"+err.Error())
		return
	}

	data := bson.M{"security_role": securityRoleRes}
	help.SuccessResponse(response, data, "Get Security Role successfully")
}

// Get number of Security Roles godoc
// @Tags Security Role
// @Summary Get number of Security Roles sort by created_date field descending
// @Description Get number of Security Roles depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.SecurityRole
// @Header 200 {string} Token "qwerty"
// @Router /SecurityRole [get]
func GetSecurityRoles(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "GetSecurityRoles")
	var (
		collection         *mongo.Collection
		allSecurityRoleRes []model.SecurityRoleResponse
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.SecurityRoleTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := help.GetLimitAndPage(request)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	//Match with Id
	match := bson.D{{
		"$match", bson.D{
			{"$and", bson.A{
				bson.D{{"is_deleted", false}},
			},
			},
		},
	}}

	//sort, skip and limit
	sort := bson.D{{"$sort", bson.D{{"created_date", -1}}}}
	skip := bson.D{{"$skip", limit * (page - 1)}}
	lim := bson.D{{"$limit", limit}}
	//Look up for foreign key
	lookupDetail := bson.D{
		{"$lookup", bson.D{
			{"from", setting.SecurityDetailTable},
			{"localField", "_id"},
			{"foreignField", "security_key"},
			{"as", setting.SecurityDetailTable + "s"},
		},
		}}

	pipeline := mongo.Pipeline{
		match,
		sort,
		skip,
		lim,
		lookupDetail,
	}

	cursor, err := collection.Aggregate(ctx, pipeline)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR"+err.Error())
		return
	}
	defer cursor.Close(ctx)
	if err = cursor.All(ctx, &allSecurityRoleRes); err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch cursor to Security Role ERROR"+err.Error())
		return
	}

	if err := cursor.Err(); err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR"+err.Error())
		return
	}

	data := bson.M{"security_role": allSecurityRoleRes}
	help.SuccessResponse(response, data, "Get Security Role successfully")
}

// Update Security Role godoc
// @Tags Security Role
// @Summary Update Security Role
// @Description Update Security Role
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Security Role need to be found"
// @Param body body model.SecurityRoleRequest true "Update Security Role"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /SecurityRole/{id} [put]
func UpdateSecurityRole(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "UpdateSecurityRole")
	var (
		collection         *mongo.Collection
		securityRole       model.SecurityRole
		securityRoleReq    model.SecurityRoleUpdateRequest
		securityRoleReqInt interface{}
		currentTime        = time.Now()
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.SecurityRoleTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Minute)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&securityRoleReq)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	securityRoleReq, err := securityRoleReq.Validation(id)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Validate Security Role Update Request ERROR - "+err.Error())
		return
	}

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err = collection.FindOne(ctx, filter).Decode(&securityRole)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&securityRole, &securityRoleReq)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	if len(securityRoleReq.SecurityDetail) > 0 {
		for _, detail := range securityRoleReq.SecurityDetail {
			filterDetail := bson.M{"_id": bsonx.ObjectID(detail.ID), "security_key": bsonx.ObjectID(id)}
			updateDetail := bson.M{
				"$set": bson.M{
					"access_right": detail.AccessRight,
					"updated_date": currentTime,
				},
			}
			if _, err = database.Database.Collection(setting.SecurityDetailTable).UpdateOne(ctx, filterDetail, updateDetail); err != nil {
				help.ErrorResponse(response, http.StatusInternalServerError, 0, fmt.Sprintf("Update Security Detail with key '%s' ERROR - %s", detail.ID.Hex(), err.Error()))
				return
			}
		}
		securityRoleReq.SecurityDetail = nil
	}
	//Set lai thoi gian luc update

	sb, _ := bson.Marshal(securityRoleReq)
	// To Interface{} with fields which is not empty
	_ = bson.Unmarshal(sb, &securityRoleReqInt)
	// ----------//
	securityRole.UpdatedDate = currentTime

	//Update Sales Order.
	update := bson.A{
		bson.M{"$set": securityRole}, // Set Main model first
	}

	//check sales order request is empty.
	if string(sb) != "{}" {
		update = append(update, bson.M{"$set": securityRoleReqInt}) // Set Request Interface second
	}

	_, err = collection.UpdateOne(ctx, filter, update)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	defaultListDetail, err := service.DefaultSecurityDetailList(securityRole.ID)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Create default Security Detail list ERROR - "+err.Error())
		return
	}

	for _, detail := range defaultListDetail {
		if _, err = database.Database.Collection(setting.SecurityDetailTable).InsertOne(ctx, detail); err != nil {
			help.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert New Security Detail ERROR - "+err.Error())
			return
		}
	}

	data := bson.M{"security_role": securityRole}
	help.SuccessResponse(response, data, "Update Security Role successfully")
}

// Soft Delete Security Role godoc
// @Tags Security Role
// @Summary Soft Delete Security Role
// @Description Soft Delete Security Role
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Security Role need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /SecurityRole/{id} [delete]
func DeleteSoftSecurityRole(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "DeleteSoftSecurityRole")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.SecurityRoleTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//var securityRole model.SecurityRole
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)

	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	help.SuccessResponse(response, id, "Delete Security Role successfully")
}

//DeleteHardSecurity Role
/*
func DeleteHardSecurityRole(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "DeleteHardSecurityRole")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.SecurityRoleTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
//-------------------- Unused -----------------------

func CreateManySecurityRole(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "CreateManySecurityRole")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.SecurityRoleTable)
	response.Header().Set("content-type", "application/json")
	var manySecurityRole []model.SecurityRole
	err = json.NewDecoder(request.Body).Decode(&manySecurityRole)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, securityRole := range manySecurityRole {
		securityRole.ID = primitive.NewObjectID()
		securityRole.CreatedDate = time.Now()
		securityRole.UpdatedDate = time.Now()
		ui = append(ui, securityRole)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}
