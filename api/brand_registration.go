package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../database"
	"../setting"
	help "./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

// Add Brand Registration godoc
// @Tags Brand Registration
// @Summary Add Brand Registration
// @Description Add Brand Registration
// @Accept  json
// @Produce  json
// @Param body body model.BrandRegistrationRequest true "Add Brand Registration"
// @Success 200 {object} model.BrandRegistration
// @Header 200 {string} Token "qwerty"
// @Router /BrandRegistration [POST]
func CreateBrandRegistration(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "CreateBrandRegistration")
	var (
		collection           *mongo.Collection
		brandRegistration    model.BrandRegistration
		brandRegistrationReq model.BrandRegistrationRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.BrandRegistrationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&brandRegistrationReq)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&brandRegistration, &brandRegistrationReq)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set Date = time.Now()
	currentTime := time.Now()

	brandRegistration.ID = primitive.NewObjectID()
	brandRegistration.CreatedDate = currentTime
	brandRegistration.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, brandRegistration)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	data := bson.M{"brand_registration": brandRegistration}
	help.SuccessResponse(response, data, "Create Brand Registration successfully")
}

// Get BrandRegistration By Id godoc
// @Tags Brand Registration
// @Summary Get Brand Registration by Id
// @Description Get Brand Registration by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Brand Registration need to be found"
// @Success 200 {object} model.BrandRegistration
// @Header 200 {string} Token "qwerty"
// @Router /BrandRegistration/{id} [get]
func GetBrandRegistrationById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "GetBrandRegistrationById")
	var (
		collection        *mongo.Collection
		brandRegistration model.BrandRegistration
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.BrandRegistrationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err := collection.FindOne(ctx, filter).Decode(&brandRegistration)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	data := bson.M{"brand_registration": brandRegistration}
	help.SuccessResponse(response, data, "Get Brand Registration successfully")
}

// Get number of Brand Registrations godoc
// @Tags Brand Registration
// @Summary Get number of Brand Registrations sort by created_date field descending
// @Description Get number of Brand Registrations depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.BrandRegistration
// @Header 200 {string} Token "qwerty"
// @Router /BrandRegistration [get]
func GetBrandRegistrations(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "GetBrandRegistrations")
	var (
		collection           *mongo.Collection
		allBrandRegistration []model.BrandRegistration
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.BrandRegistrationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := help.GetLimitAndPage(request)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{"is_deleted": false}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var brandRegistration model.BrandRegistration
		cursor.Decode(&brandRegistration)
		allBrandRegistration = append(allBrandRegistration, brandRegistration)
	}

	if err := cursor.Err(); err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"brand_registration": allBrandRegistration}
	help.SuccessResponse(response, data, "Get Brand Registration successfully")
}

// Update Brand Registration godoc
// @Tags Brand Registration
// @Summary Update Brand Registration
// @Description Update Brand Registration
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Brand Registration need to be found"
// @Param body body model.BrandRegistrationRequest true "Update Brand Registration"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /BrandRegistration/{id} [put]
func UpdateBrandRegistration(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "UpdateBrandRegistration")
	var (
		collection           *mongo.Collection
		brandRegistration    model.BrandRegistration
		brandRegistrationReq model.BrandRegistrationRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.BrandRegistrationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&brandRegistrationReq)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err = collection.FindOne(ctx, filter).Decode(&brandRegistration)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&brandRegistration, &brandRegistrationReq)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}
	//Set lai thoi gian luc update
	brandRegistration.UpdatedDate = time.Now()

	update := bson.M{"$set": brandRegistration}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"brand_registration": brandRegistration}
	help.SuccessResponse(response, data, "Update Brand Registration successfully")
}

// Soft Delete Brand Registration godoc
// @Tags Brand Registration
// @Summary Soft Delete Brand Registration
// @Description Soft Delete Brand Registration
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Brand Registration need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /BrandRegistration/{id} [delete]
func DeleteSoftBrandRegistration(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "DeleteSoftBrandRegistration")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.BrandRegistrationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//var brandRegistration model.BrandRegistration
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)

	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		help.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	help.SuccessResponse(response, id, "Delete Brand Registration successfully")
}

//DeleteHardBrand Registration
/*
func DeleteHardBrandRegistration(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "DeleteHardBrandRegistration")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.BrandRegistrationTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
//-------------------- Unused -----------------------

func CreateManyBrandRegistration(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer help.RecoverError(response, "CreateManyBrandRegistration")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.BrandRegistrationTable)
	response.Header().Set("content-type", "application/json")
	var manyBrandRegistration []model.BrandRegistration
	err = json.NewDecoder(request.Body).Decode(&manyBrandRegistration)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, brandRegistration := range manyBrandRegistration {
		brandRegistration.ID = primitive.NewObjectID()
		brandRegistration.CreatedDate = time.Now()
		brandRegistration.UpdatedDate = time.Now()
		ui = append(ui, brandRegistration)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}
