package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"../database"
	"../model/enum"
	"../service"
	"../setting"
	helper "./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

func CreateInventoryStatusByProduct(product model.Product) ([]interface{}, []interface{}, error) {
	var (
		collection          *mongo.Collection
		filter              bson.D
		invStatus           model.InventoryStatus
		allSite             []model.Site
		uiInvStatus         []interface{}
		invLog              model.InventoryLog
		uiInvLog            []interface{}
		invStatusCollection = database.Database.Collection(setting.InventoryStatusTable)
		invLogCollection    = database.Database.Collection(setting.InventoryLogTable)
	)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter = bson.D{{"is_deleted", false}}

	collection = database.Database.Collection(setting.SiteTable)
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		return nil, nil, err
	}

	defer cursor.Close(ctx)
	if err := cursor.All(context.Background(), &allSite); err != nil {
		return nil, nil, err
	}
	for _, site := range allSite {
		currentTime := time.Now()

		invStatus.ID = primitive.NewObjectID()
		invStatus.ProductKey = product.ID
		invStatus.ProductId = product.ProductId
		invStatus.ProductName = product.ShortDescription
		invStatus.SiteKey = site.ID
		invStatus.SiteId = site.SiteId
		invStatus.SiteName = site.Description
		invStatus.ObjectType = setting.InventoryStatusTable
		invStatus.IsActive = true
		invStatus.IsDeleted = false
		invStatus.CreatedDate = currentTime
		invStatus.CreatedBy = product.CreatedBy
		invStatus.CreatedApplicationKey = product.CreatedApplicationKey
		invStatus.UpdatedDate = currentTime
		invStatus.UpdatedBy = product.UpdatedBy
		invStatus.UpdatedApplicationKey = product.UpdatedApplicationKey
		//Copy Inv Status to Inv Log
		_ = copier.Copy(&invLog, invStatus)
		invLog.ID = primitive.NewObjectID()

		_, err = invStatusCollection.InsertOne(context.Background(), invStatus)
		if err != nil {
			return nil, nil, err
		}
		uiInvStatus = append(uiInvStatus, invStatus)

		_, err = invLogCollection.InsertOne(context.Background(), invLog)
		if err != nil {
			return nil, nil, err
		}
		uiInvLog = append(uiInvLog, invLog)
	}

	return uiInvStatus, uiInvLog, nil
}

func CreateInventoryStatusBySite(site model.Site) ([]interface{}, []interface{}, error) {
	var (
		collection *mongo.Collection
		filter     bson.D
		//product             model.Product
		allProduct          []model.Product
		invStatus           model.InventoryStatus
		invLog              model.InventoryLog
		invStatusCollection = database.Database.Collection(setting.InventoryStatusTable)
		invLogCollection    = database.Database.Collection(setting.InventoryLogTable)
		uiInvStatus         []interface{}
		uiInvLog            []interface{}
	)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter = bson.D{{"is_deleted", false}}

	collection = database.Database.Collection(setting.ProductTable)

	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		return nil, nil, err
	}
	defer cursor.Close(ctx)

	if err := cursor.All(context.Background(), &allProduct); err != nil {
		return nil, nil, err
	}
	for _, product := range allProduct {
		// For each Site ID ==> Insert Product & Site to Inventory Status, May be InsertMany
		//_ = cursor.Decode(&product)
		currentTime := time.Now()

		invStatus.ID = primitive.NewObjectID()
		invStatus.ProductKey = product.ID
		invStatus.ProductId = product.ProductId
		invStatus.ProductName = product.ShortDescription
		invStatus.SiteKey = site.ID
		invStatus.SiteId = site.SiteId
		invStatus.SiteName = site.Description
		invStatus.ObjectType = setting.InventoryStatusTable
		invStatus.IsActive = true
		invStatus.IsDeleted = false
		invStatus.CreatedDate = currentTime
		invStatus.CreatedBy = product.CreatedBy
		invStatus.CreatedApplicationKey = product.CreatedApplicationKey
		invStatus.UpdatedDate = currentTime
		invStatus.UpdatedBy = product.UpdatedBy
		invStatus.UpdatedApplicationKey = product.UpdatedApplicationKey

		//Copy Inv Status to Inv Log
		_ = copier.Copy(&invLog, invStatus)
		invLog.ID = primitive.NewObjectID()

		_, err = invStatusCollection.InsertOne(context.Background(), invStatus)
		if err != nil {
			return nil, nil, err
		}
		uiInvStatus = append(uiInvStatus, invStatus)

		_, err = invLogCollection.InsertOne(context.Background(), invLog)
		if err != nil {
			return nil, nil, err
		}
		uiInvLog = append(uiInvLog, invLog)
	}

	return uiInvStatus, uiInvLog, nil
}

// Update InventoryStatus godoc
// @Tags Inventory Status
// @Summary Update InventoryStatus
// @Description Update InventoryStatus
// @Accept  json
// @Produce  json
// @Param body body model.InventoryStatusRequest true "Update InventoryStatus"
// @Success 200 {object} model.InventoryStatus
// @Header 200 {string} Token "qwerty"
// @Router /InventoryStatus [PUT]
func UpdateInventoryStatus(response http.ResponseWriter, request *http.Request) {
	var (
		inventoryStatus    model.InventoryStatus
		inventoryStatusReq model.InventoryStatusRequest
		//inventoryLog       model.InventoryLog
		//collection         *mongo.Collection
	)

	response.Header().Set("content-type", "application/json")
	//ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	//defer cancel()
	err = json.NewDecoder(request.Body).Decode(&inventoryStatusReq)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}
	request.Body.Close()

	if inventoryStatus, err = service.UpdateInventoryStatusFunc(
		inventoryStatusReq.ProductId,
		inventoryStatusReq.SiteId,
		inventoryStatusReq.OnHandQty,
		inventoryStatusReq.OnOrderQty,
		true); err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Inventory Status ERROR - "+err.Error())
		return
	}

	/*
		filter := bson.D{
			//{"site_id", inventoryStatusReq.SiteId},
			{"product_id", inventoryStatusReq.ProductId},
			{"is_deleted", false},
		}

		//Get inventory status
		collection = database.Database.Collection(setting.InventoryStatusTable)
		err = collection.FindOne(ctx, filter).Decode(&inventoryStatus)
		if err != nil {
			helper.ErrorResponse(response, http.StatusInternalServerError,0, "Inventory Status - "+err.Error())
			return
		}

		err = copier.Copy(&inventoryStatus, &inventoryStatusReq)
		if err != nil {
			helper.ErrorResponse(response, http.StatusInternalServerError,0, "Copy error : "+err.Error())
			return
		}

		//Set Date = time.Now()
		currentTime := time.Now()
		inventoryStatus.UpdatedDate = currentTime
		inventoryStatus.UpdatedBy = primitive.NewObjectID()
		inventoryStatus.UpdatedApplicationKey = primitive.NewObjectID()

		err = copier.Copy(&inventoryLog, &inventoryStatus)
		if err != nil {
			helper.ErrorResponse(response, http.StatusInternalServerError,0, "Copy error - "+err.Error())
			return
		}

		inventoryStatus, _ = inventoryStatus.RecalculateQuantity()

		update := bson.D{{"$set", inventoryStatus}}
		collection = database.Database.Collection(setting.InventoryStatusTable)
		//_, err = collection.InsertOne(ctx, inventoryStatus)
		_, err = collection.UpdateOne(ctx, filter, update)
		if err != nil {
			helper.ErrorResponse(response, http.StatusInternalServerError,0, "Update Error - "+err.Error())
			return
		}

	*/

	//invLog, err := CreateIntegrationLog(inventoryLog)
	//if err != nil {
	//	helper.ErrorResponse(response, http.StatusInternalServerError,0, "Inventory Log : "+err.Error())
	//	return
	//}

	data := bson.M{
		"inventory_status": inventoryStatus,
		//"inventory_log":    invLog,
	}
	helper.SuccessResponse(response, data, "Update Inventory Status successfully")
	return
}

// Get InventoryStatus By Id godoc
// @Tags Inventory Status
// @Summary Get InventoryStatus by Id
// @Description Get InventoryStatus by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of InventoryStatus need to be found"
// @Success 200 {object} model.InventoryStatus
// @Header 200 {string} Token "qwerty"
// @Router /InventoryStatus/{id} [get]
func GetInventoryStatusById(response http.ResponseWriter, request *http.Request) {
	var (
		collection      *mongo.Collection
		inventoryStatus model.InventoryStatus
	)

	response.Header().Set("content-type", "application/json")
	collection = database.Database.Collection(setting.InventoryStatusTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err := collection.FindOne(ctx, filter).Decode(&inventoryStatus)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Input into model ERROR - "+err.Error())
		return
	}

	data := bson.M{"inventory_status": inventoryStatus}
	helper.SuccessResponse(response, data, "Get Inventory Status successfully")
}

// Get InventoryStatus By Product Id and Site Id godoc
// @Tags Inventory Status
// @Summary Get InventoryStatus by Product Id and Site Id
// @Description Get InventoryStatus by Product Id and Site Id
// @Accept  json
// @Produce  json
// @Param product_id path string true "Product Id"
// @Param site_id path string true "Site Id"
// @Success 200 {object} model.InventoryStatus
// @Header 200 {string} Token "qwerty"
// @Router /InventoryStatus/site/{site_id}/product/{product_id} [get]
func GetInventoryStatusByProductAndSite(response http.ResponseWriter, request *http.Request) {
	var (
		collection      *mongo.Collection
		inventoryStatus model.InventoryStatus
	)

	response.Header().Set("content-type", "application/json")
	collection = database.Database.Collection(setting.InventoryStatusTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	prodId, _ := mux.Vars(request)["product_id"]
	siteId, _ := mux.Vars(request)["site_id"]

	if prodId == "" || siteId == "" {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Product Id or Site Id is empty ERROR - "+err.Error())
		return
	}

	//_id = id and is_deleted = false
	filter := bson.M{"product_id": prodId, "site_id": siteId, "is_deleted": false}

	err := collection.FindOne(ctx, filter).Decode(&inventoryStatus)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Input into model ERROR - "+err.Error())
		return
	}

	data := bson.M{"inventory_status": inventoryStatus}
	helper.SuccessResponse(response, data, "Get Inventory Status by Product Id and Site Id successfully")
}

// Get number of Inventory Statuss godoc
// @Tags Inventory Status
// @Summary Get number of Inventory Statuss sort by created_date field descending
// @Description Get number of Inventory Statuss depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.InventoryStatus
// @Header 200 {string} Token "qwerty"
// @Router /InventoryStatus [get]
func GetInventoryStatus(response http.ResponseWriter, request *http.Request) {
	//Get Limit and Page
	var (
		allInventoryStatus []model.InventoryStatus
		inventoryStatus    model.InventoryStatus
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.InventoryStatusTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, page, err := helper.GetLimitAndPage(request)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{"is_deleted": bson.M{"$eq": false}}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		cursor.Decode(&inventoryStatus)
		allInventoryStatus = append(allInventoryStatus, inventoryStatus)
	}
	if err := cursor.Err(); err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"inventory_status": allInventoryStatus}
	helper.SuccessResponse(response, data, "Get Inventory Status successfully")
}

func ExportInventoryStatus(response http.ResponseWriter, request *http.Request) {
	var (
		allInventoryStatus []bson.M
		inventoryStatus    bson.M
		filter             = bson.D{}
	)

	response.Header().Set("content-type", "text/plain")

	collection := database.Database.Collection(setting.InventoryStatusTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//limit, page, err := helper.GetLimitAndPage(request)
	//if err != nil {
	//	helper.ErrorResponse(response, http.StatusInternalServerError,0, "Get Limit or Page ERROR - "+err.Error())
	//	return
	//}

	siteId := request.URL.Query().Get("site_id")
	if siteId != "" {
		filter = append(filter, bson.E{Key: "site_id", Value: siteId})
	}

	filter = append(filter, bson.E{Key: "is_deleted", Value: bson.M{"$eq": false}}, bson.E{Key: "ych_qty", Value: bson.M{"$ne": 0}})

	fromDate, toDate, err := helper.GetFromAndToDate(request)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get From and To Date ERROR - "+err.Error())
		return
	}

	filter = append(filter, bson.E{
		Key: "updated_date", Value: bson.M{
			"$gte": time.Unix(fromDate, 0),
			"$lte": time.Unix(toDate, 0),
		},
	})

	findOptions := options.Find()

	//// Set limit record of 1 page
	//findOptions.SetLimit(limit)
	//// Set page
	//findOptions.SetSkip(limit * (page - 1))

	findOptions.SetProjection(bson.M{
		"_id":           0,
		"product_id":    1,
		"site_id":       1,
		"on_hand_qty":   1,
		"ych_qty":       1,
		"on_order_qty":  1,
		"on_commit_qty": 1,
		"on_tsf_qty":    1,
		"reserve_qty":   1,
		"shipped_qty":   1,
		"created_date":  1,
		"updated_date":  1,
	})
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		cursor.Decode(&inventoryStatus)
		allInventoryStatus = append(allInventoryStatus, inventoryStatus)
	}
	if err := cursor.Err(); err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"inventory_status": allInventoryStatus}

	bytes, _ := json.Marshal(data)
	json.NewEncoder(response).Encode(string(bytes))
}

func RecalculateQuantity(response http.ResponseWriter, request *http.Request) {
	var (
		inventoryStatus    model.InventoryStatus
		inventoryStatusReq model.InventoryStatusRequest
		filter             = bson.D{}
	)

	type OrderQty struct {
		Quantity float64 `bson:"qty"`
	}

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.InventoryStatusTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&inventoryStatusReq)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	prodId := inventoryStatusReq.ProductId

	siteId := inventoryStatusReq.SiteId

	filter = append(filter, bson.E{Key: "site_id", Value: siteId}, bson.E{Key: "product_id", Value: prodId})

	if err = collection.FindOne(ctx, filter).Decode(&inventoryStatus); err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, fmt.Sprintf("Find Inventory Status with Product Code [%s] and Site Id [%s] ERROR - %s", prodId, siteId, err.Error()))
		return
	}

	if siteId == "" || prodId == "" {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Site Id or Product Id is missing.")
		return
	}

	//updatedDateTimeGMT8 := bson.M{"date": "$updated_date", "timezone": "+08:00"}
	//dateTimeGMT8 := bson.M{
	//	"date":     "$$NOW",
	//	"timezone": "+08:00",
	//}
	fmt.Sprint()

	matchOrderQty := bson.D{
		{"$match", bson.M{
			"product_code":    prodId,
			"doc_line_status": bson.M{"$in": bson.A{enum.OrderLineStatusNew, enum.OrderLineStatusProcessing, enum.OrderLineStatusPicked, enum.OrderLineStatusPacked}},
		}},
	}

	//matchOnHandQty := bson.D{
	//	{"$match", bson.M{
	//		"$expr": bson.M{
	//			"$and": bson.A{
	//				bson.M{"$eq": bson.A{"$product_code", prodId}},
	//				bson.M{"$in": bson.A{"$doc_line_status", bson.A{enum.OrderLineStatusShipped, enum.OrderLineStatusDelivered}}},
	//				bson.M{"$eq": bson.A{bson.M{"$year": updatedDateTimeGMT8}, bson.M{"$year": dateTimeGMT8}}},
	//				bson.M{"$eq": bson.A{bson.M{"$month": updatedDateTimeGMT8}, bson.M{"$month": dateTimeGMT8}}},
	//				bson.M{"$eq": bson.A{bson.M{"$dayOfMonth": updatedDateTimeGMT8}, bson.M{"$dayOfMonth": dateTimeGMT8}}},
	//			},
	//		},
	//	}},
	//}

	lookupSalesOrder := bson.D{
		{"$lookup", bson.M{
			"from": setting.SalesOrderTable,
			"let":  bson.M{"key": "$doc_key"},
			"pipeline": bson.A{
				bson.D{
					{"$match", bson.M{
						"$expr": bson.M{
							"$and": bson.A{
								bson.M{"$eq": bson.A{"$_id", "$$key"}},
								//bson.M{"$eq": bson.A{"$site_id", siteId}},
							},
						},
					}},
				},
			},
			"as": setting.SalesOrderTable,
		},
		},
	}

	matchSite := bson.D{
		{"$match", bson.M{
			"sales_order.site_id": siteId,
		}},
	}

	groupOrderQty := bson.D{
		{"$group", bson.D{
			{"_id", "$product_code"},
			{"qty", bson.M{
				"$sum": bson.M{
					"$add": bson.A{
						"$quantity",
						bson.M{"$multiply": bson.A{"$return_quantity", -1}},
						bson.M{"$multiply": bson.A{"$cancel_quantity", -1}},
					},
				},
			}},
		}},
	}

	//groupOnHandQty := bson.D{
	//	{"$group", bson.D{
	//		{"_id", "$product_code"},
	//		{"qty", bson.M{
	//			"$sum": bson.M{
	//				"$add": bson.A{
	//					"$quantity",
	//					bson.M{"$multiply": bson.A{"$cancel_quantity", -1}},
	//				},
	//			},
	//		}},
	//	}},
	//}

	pipelineOrderQty := mongo.Pipeline{
		matchOrderQty,
		lookupSalesOrder,
		matchSite,
		groupOrderQty,
	}

	//pipelineOnHandQty := mongo.Pipeline{
	//	matchOnHandQty,
	//	lookupSalesOrder,
	//	matchSite,
	//	groupOnHandQty,
	//}

	cursorOrderQty, err := database.Database.Collection(setting.SalesOrderDetailTable).Aggregate(ctx, pipelineOrderQty)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Order Quantity of Product ERROR - "+err.Error())
		return
	}

	//cursorOrderQty.Close(ctx)
	if cursorOrderQty.Next(ctx) {
		var qty OrderQty
		cursorOrderQty.Decode(&qty)
		inventoryStatus.OnOrderQty = qty.Quantity
	} else {
		inventoryStatus.OnOrderQty = 0
	}

	//cursorOnHandQty, err := database.Database.Collection(setting.SalesOrderDetailTable).Aggregate(ctx, pipelineOnHandQty)
	//if err != nil {
	//	helper.ErrorResponse(response, http.StatusInternalServerError,0, "Find On Hand Quantity of Product ERROR - "+err.Error())
	//	return
	//}
	//
	////cursorOnHandQty.Close(ctx)
	//if cursorOnHandQty.Next(ctx) {
	//	var qty OrderQty
	//	cursorOnHandQty.Decode(&qty)
	//	inventoryStatus.OnHandQty = inventoryStatus.YchQty - qty.Quantity
	//	fmt.Println("qty", qty)
	//}else {
	//	inventoryStatus.OnHandQty = inventoryStatus.YchQty
	//}

	inventoryStatus.OnAvailableQty = inventoryStatus.OnHandQty - inventoryStatus.OnOrderQty
	inventoryStatus.UpdatedDate = time.Now()

	update := bson.M{
		"$set": inventoryStatus,
	}

	if _, err = collection.UpdateOne(ctx, filter, update); err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, fmt.Sprintf("Update Inventory Status with Product Code [%s] and Site Id [%s] ERROR - %s", prodId, siteId, err.Error()))
		return
	}

	data := bson.M{"inventory_status": inventoryStatus}
	helper.SuccessResponse(response, data, "Recalculate Product Quantity Successfully.")
}

//--------------------- Service ---------------------
/*
	Paras:
ProductID
OnhandQty
OrderQty
IsOverwriteOnHand

If IsOverwriteOnhand=True (for YCH  Integration)
         InventoryStatus.Onhand= New Onhand
          InventoryStatus.YCHQty =new onhand qty
else
         InventoryStatus.Onhand=InventoryStatus.Onhand + (-) Para.OnhandQty. (>> Shipped Qty)
         InventoryStatus.OrderQty=InventoryStatus.OrderQty + (-) Para.OrderQty (>> New Order, Cancelled order)

*/
//

//--------------------- Unused ------------------------
func CreateManyInventoryStatus(response http.ResponseWriter, request *http.Request) {
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.InventoryStatusTable)
	response.Header().Set("content-type", "application/json")
	var manyInventoryStatus []model.InventoryStatus
	err = json.NewDecoder(request.Body).Decode(&manyInventoryStatus)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, inventoryStatus := range manyInventoryStatus {
		inventoryStatus.ID = primitive.NewObjectID()
		inventoryStatus.CreatedDate = time.Now()
		inventoryStatus.UpdatedDate = time.Now()
		ui = append(ui, inventoryStatus)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

func DeleteSoftInventoryStatus(response http.ResponseWriter, request *http.Request) {
	var collection *mongo.Collection
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.InventoryStatusTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bsonx.ObjectID(id)}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}
	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete Inventory Status ERROR - "+err.Error())
		return
	}
	helper.SuccessResponse(response, id, "Delete Inventory Status successfully")
}

//DeleteHardInventoryStatus
/*
func DeleteHardInventoryStatus(response http.ResponseWriter, request *http.Request) {
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.InventoryStatusTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
