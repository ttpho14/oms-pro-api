package api

import (
	"../setting"
	"context"
	"encoding/json"
	"net/http"
	"os"
	"time"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateSalesOrderShippingDetail godoc
// @Tags Sales Order Shipping Detail
// @Summary Add Sales Order Shipping Detail
// @Description Add Sales Order Shipping Detail
// @Accept  json
// @Produce  json
// @Param body body []model.SalesOrderShippingDetailRequest true "Add Sales Order Shipping Detail"
// @Success 200 {object} []model.SalesOrderShippingDetail
// @Header 200 {string} Token "qwerty"
// @Router /SaleOrderShippingDetail [POST]
func CreateSalesOrderShippingDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateSalesOrderShippingDetail")
	var (
		collection               *mongo.Collection
		salesOrderShippingDetail model.SalesOrderShippingDetail
	)

	collection = database.Database.Collection(setting.SalesOrderShippingDetailTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&salesOrderShippingDetail)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	currentTime := time.Now()
	salesOrderShippingDetail.CreatedDate = currentTime
	salesOrderShippingDetail.UpdatedDate = currentTime

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	result, err := collection.InsertOne(ctx, salesOrderShippingDetail)
	if err != nil {
		json.NewEncoder(response).Encode(err.Error())
		return
	}
	id := result.InsertedID
	salesOrderShippingDetail.ID = id.(primitive.ObjectID)
	json.NewEncoder(response).Encode(salesOrderShippingDetail)
}

// CreateManySalesOrderShippingDetail func
func CreateManySalesOrderShippingDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManySalesOrderShippingDetail")
	var (
		collection                *mongo.Collection
		salesOrderShippingDetails []model.SalesOrderShippingDetail
	)
	collection = database.Database.Collection(setting.SalesOrderShippingDetailTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&salesOrderShippingDetails)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	currentTime := time.Now()
	for _, salesOrderShippingDetail := range salesOrderShippingDetails {
		salesOrderShippingDetail.ID = primitive.NewObjectID()
		salesOrderShippingDetail.CreatedDate = currentTime
		salesOrderShippingDetail.UpdatedDate = currentTime
		ui = append(ui, salesOrderShippingDetail)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// GetSalesOrderShippingDetailByID godoc
// @Tags Sales Order Shipping Detail
// @Summary Get Sales Order Shipping Detail by Id
// @Description Get Sales Order Shipping Detail by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Order Shipping Detail need to be found"
// @Success 200 {object} model.SalesOrderShippingDetail
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderShippingDetail/{id} [get]
func GetSalesOrderShippingDetailByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSaleOrderShippingDetailByID")
	var (
		collection               *mongo.Collection
		salesOrderShippingDetail model.SalesOrderShippingDetail
	)
	collection = database.Database.Collection(setting.SalesOrderShippingDetailTable)

	response.Header().Set("content-type", "application/json")

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	err := collection.FindOne(ctx, filter).Decode(&salesOrderShippingDetail)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesOrderShippingDetail)
}

// GetSalesOrderShippingDetails godoc
// @Tags Sales Order Shipping Detail
// @Summary Get all Sales Order Shipping Detail
// @Description Get all Sales Order Shipping Detail
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.SalesOrderShippingDetail
// @Header 200 {string} Token "qwerty"
// @Router /SaleOrderShippingDetail [get]
func GetSalesOrderShippingDetails(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesOrderShippingDetails")
	var (
		collection                *mongo.Collection
		salesOrderShippingDetails []model.SalesOrderShippingDetail
		findOptions               = options.Find()
		filter                    = bson.M{}
	)
	collection = database.Database.Collection(setting.SalesOrderShippingDetailTable)

	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions) //findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var saleOrderShippingDetail model.SalesOrderShippingDetail
		cursor.Decode(&saleOrderShippingDetail)
		salesOrderShippingDetails = append(salesOrderShippingDetails, saleOrderShippingDetail)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesOrderShippingDetails)
}

// UpdateSalesOrderShippingDetail godoc
// @Tags Sales Order Shipping Detail
// @Summary Update Sales Order Shipping Detail
// @Description Update Sales Order Shipping Detail
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Order need to be found"
// @Param body body model.SalesOrderRequest true "Update Sales Order Shipping Detail"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderShippingDetail/{id} [put]
func UpdateSalesOrderShippingDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateSaleOrderShippingDetail")
	var (
		collection              *mongo.Collection
		saleOrderShippingDetail model.SalesOrderShippingDetail
	)
	collection = database.Database.Collection(setting.SalesOrderShippingDetailTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&saleOrderShippingDetail)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	saleOrderShippingDetail.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": saleOrderShippingDetail}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

func DeleteSoftSalesOrderShippingDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftSalesOrderShippingDetail")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.SalesOrderShippingDetailTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
