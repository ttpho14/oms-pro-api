package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../database"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

// Add CategoryDetail godoc
// @Tags CategoryDetail
// @Summary Add CategoryDetail
// @Description Add CategoryDetail
// @Accept  json
// @Produce  json
// @Param body body model.CategoryDetailRequest true "Add CategoryDetail"
// @Success 200 {object} model.CategoryDetail
// @Header 200 {string} Token "qwerty"
// @Router /CategoryDetail [POST]
func CreateCategoryDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateCategoryDetail")
	var (
		collection        *mongo.Collection
		categoryDetail    model.CategoryDetail
		categoryDetailReq model.CategoryDetailRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.CategoryDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&categoryDetailReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&categoryDetail, &categoryDetailReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set Date = time.Now()
	currentTime := time.Now()
	categoryDetail.ID = primitive.NewObjectID()
	categoryDetail.CreatedDate = currentTime
	categoryDetail.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, categoryDetail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	data := bson.M{"bundle_detail": categoryDetail}
	api_helper.SuccessResponse(response, data, "Create Category Detail successfully")
}

// Get CategoryDetail By Id godoc
// @Tags CategoryDetail
// @Summary Get CategoryDetail by Id
// @Description Get CategoryDetail by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of CategoryDetail need to be found"
// @Success 200 {object} model.CategoryDetail
// @Header 200 {string} Token "qwerty"
// @Router /CategoryDetail/{id} [get]
func GetCategoryDetailById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetCategoryDetailById")
	var (
		collection     *mongo.Collection
		categoryDetail model.CategoryDetail
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.CategoryDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	err := collection.FindOne(ctx, filter).Decode(&categoryDetail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	data := bson.M{"bundle_detail": categoryDetail}
	api_helper.SuccessResponse(response, data, "Get Category Detail successfully")
}

// Get number of CategoryDetails godoc
// @Tags CategoryDetail
// @Summary Get number of CategoryDetails sort by created_date field descending
// @Description Get number of CategoryDetails depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.CategoryDetail
// @Header 200 {string} Token "qwerty"
// @Router /CategoryDetail [get]
func GetCategoryDetails(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetCategoryDetails")
	var (
		categoryDetail    model.CategoryDetail
		allCategoryDetail []model.CategoryDetail
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.CategoryDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{"is_deleted": false}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		_ = cursor.Decode(&categoryDetail)
		allCategoryDetail = append(allCategoryDetail, categoryDetail)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"bundle_detail": allCategoryDetail}
	api_helper.SuccessResponse(response, data, "Get Category Detail successfully")
}

// Update CategoryDetail godoc
// @Tags CategoryDetail
// @Summary Update CategoryDetail
// @Description Update CategoryDetail
// @Accept  json
// @Produce  json
// @Param id path string true "Id of CategoryDetail need to be found"
// @Param body body model.CategoryDetailRequest true "Update CategoryDetail"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /CategoryDetail/{id} [put]
func UpdateCategoryDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var (
		collection        *mongo.Collection
		categoryDetail    model.CategoryDetail
		categoryDetailReq model.CategoryDetailRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.CategoryDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err = collection.FindOne(ctx, filter).Decode(&categoryDetail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = json.NewDecoder(request.Body).Decode(&categoryDetailReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&categoryDetail, &categoryDetailReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set lai thoi gian luc update
	categoryDetail.UpdatedDate = time.Now()

	update := bson.M{"$set": categoryDetail}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"bundle": categoryDetail}
	api_helper.SuccessResponse(response, data, "Update Category Detail Successfully")
}

// Soft Delete CategoryDetail godoc
// @Tags CategoryDetail
// @Summary Soft Delete CategoryDetail
// @Description Soft Delete CategoryDetail
// @Accept  json
// @Produce  json
// @Param id path string true "Id of CategoryDetail need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /CategoryDetail/{id} [delete]
func DeleteSoftCategoryDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteSoftCategoryDetail")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.CategoryDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	api_helper.SuccessResponse(response, id, "Delete Category Detail Successfully")
}

//DeleteHardCategoryDetail
/*
func DeleteHardCategoryDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardCategoryDetail")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.CategoryDetailTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
//-------------------- Unused -----------------------
func CreateManyCategoryDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyCategoryDetail")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.CategoryDetailTable)
	response.Header().Set("content-type", "application/json")
	var manyCategoryDetail []model.CategoryDetail
	err = json.NewDecoder(request.Body).Decode(&manyCategoryDetail)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, bundleDetail := range manyCategoryDetail {
		bundleDetail.ID = primitive.NewObjectID()
		bundleDetail.CreatedDate = time.Now()
		bundleDetail.UpdatedDate = time.Now()
		ui = append(ui, bundleDetail)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}
