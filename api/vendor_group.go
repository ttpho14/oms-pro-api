package api

import (
	"../database"
	"../setting"
	"./api_helper"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"net/http"
	"os"
	"reflect"
	"time"

	"../model"
)

// Add VendorGroup godoc
// @Tags Vendor Group
// @Summary Add VendorGroup
// @Description Add VendorGroup
// @Accept  json
// @Produce  json
// @Param body body []model.AddVendorGroup true "Add VendorGroup"
// @Success 200 {object} []model.VendorGroup
// @Header 200 {string} Token "qwerty"
// @Router /VendorGroup [POST]
func CreateVendorGroup(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateVendorGroup")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.VendorGroupTable)
	response.Header().Set("content-type", "application/json")
	var vendorGroup model.VendorGroup
	err = json.NewDecoder(request.Body).Decode(&vendorGroup)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	//Set Date = time.Now()
	vendorGroup.CreatedDate = time.Now()
	vendorGroup.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	result, err := collection.InsertOne(ctx, vendorGroup)
	if err != nil {
		json.NewEncoder(response).Encode(err.Error())
		return
	}
	id := result.InsertedID
	vendorGroup.ID = id.(primitive.ObjectID)
	json.NewEncoder(response).Encode(vendorGroup)
}

func CreateManyVendorGroup(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyVendorGroup")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.VendorGroupTable)
	response.Header().Set("content-type", "application/json")
	var manyVendorGroup []model.VendorGroup
	err = json.NewDecoder(request.Body).Decode(&manyVendorGroup)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, vendorGroup := range manyVendorGroup {
		vendorGroup.ID = primitive.NewObjectID()
		vendorGroup.CreatedDate = time.Now()
		vendorGroup.UpdatedDate = time.Now()
		ui = append(ui, vendorGroup)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// Get VendorGroup By Id godoc
// @Tags Vendor Group
// @Summary Get VendorGroup by Id
// @Description Get VendorGroup by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of VendorGroup need to be found"
// @Success 200 {object} model.VendorGroup
// @Header 200 {string} Token "qwerty"
// @Router /VendorGroup/{id} [get]
func GetVendorGroupById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetVendorGroupById")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.VendorGroupTable)
	response.Header().Set("content-type", "application/json")
	var vendorGroup model.VendorGroup
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//_id = id and is_deleted = false
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}, "is_deleted": bson.M{"$eq": false}}
	err := collection.FindOne(ctx, filter).Decode(&vendorGroup)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(vendorGroup)
}

// Get number of Vendor Groups godoc
// @Tags Vendor Group
// @Summary Get number of Vendor Groups sort by created_date field descending
// @Description Get number of Vendor Groups depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.VendorGroup
// @Header 200 {string} Token "qwerty"
// @Router /VendorGroup [get]
func GetVendorGroups(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetVendorGroups")
	//Get Limit and Page
	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	collection := database.Database.Collection(setting.VendorGroupTable)
	response.Header().Set("content-type", "application/json")
	var allVendorGroup []model.VendorGroup
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"is_deleted": bson.M{"$eq": false}}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var vendorGroup model.VendorGroup
		cursor.Decode(&vendorGroup)
		allVendorGroup = append(allVendorGroup, vendorGroup)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(allVendorGroup)
}

// Update VendorGroup godoc
// @Tags Vendor Group
// @Summary Update VendorGroup
// @Description Update VendorGroup
// @Accept  json
// @Produce  json
// @Param id path string true "Id of VendorGroup need to be found"
// @Param body body model.AddVendorGroup true "Update VendorGroup"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /VendorGroup/{id} [put]
func UpdateVendorGroup(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "UpdateVendorGroup")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.VendorGroupTable)
	response.Header().Set("content-type", "application/json")
	var vendorGroup model.VendorGroup
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	err = json.NewDecoder(request.Body).Decode(&vendorGroup)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	//Set lai thoi gian luc update
	vendorGroup.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	fmt.Println("filter VendorGroup", filter)
	update := bson.M{"$set": vendorGroup}
	result, err := collection.UpdateOne(ctx, filter, update)
	fmt.Println("Result VendorGroup", result)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

// Soft Delete VendorGroup godoc
// @Tags Vendor Group
// @Summary Soft Delete VendorGroup
// @Description Soft Delete VendorGroup
// @Accept  json
// @Produce  json
// @Param id path string true "Id of VendorGroup need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /VendorGroup/{id} [delete]
func DeleteSoftVendorGroup(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteSoftVendorGroup")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.VendorGroupTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("UpdateOne() result:", result)
		fmt.Println("UpdateOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("UpdateOne() result MatchedCount:", result.MatchedCount)
		fmt.Println("UpdateOne() result ModifiedCount:", result.ModifiedCount)
		fmt.Println("UpdateOne() result UpsertedCount:", result.UpsertedCount)
		fmt.Println("UpdateOne() result UpsertedID:", result.UpsertedID)
	}
	json.NewEncoder(response).Encode(result)
}

//DeleteHardVendorGroup
/*
func DeleteHardVendorGroup(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardVendorGroup")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.VendorGroupTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
//-------------------- REGION API - END -----------------------
