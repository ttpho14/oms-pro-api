package api

import (
	"../setting"
	"context"
	"encoding/json"
	"net/http"
	"os"
	"time"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateSalesInvoiceDetail godoc
// @Tags Sales Invoice Detail
// @Summary Add Sales Invoice Detail
// @Description Add Sales Invoice Detail
// @Accept  json
// @Produce  json
// @Param body body []model.AddSalesInvoiceDetail true "Add Sales Invoice Detail"
// @Success 200 {object} []model.SalesInvoiceDetail
// @Header 200 {string} Token "qwerty"
// @Router /SalesInvoiceDetail [POST]
func CreateSalesInvoiceDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateSalesInvoiceDetail")
	var (
		collection         *mongo.Collection
		salesInvoiceDetail model.SalesInvoiceDetail
	)

	collection = database.Database.Collection(setting.SalesInvoiceDetailTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&salesInvoiceDetail)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	currentTime := time.Now()
	salesInvoiceDetail.CreatedDate = currentTime
	salesInvoiceDetail.UpdatedDate = currentTime

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	result, err := collection.InsertOne(ctx, salesInvoiceDetail)
	if err != nil {
		json.NewEncoder(response).Encode(err.Error())
		return
	}
	id := result.InsertedID
	salesInvoiceDetail.ID = id.(primitive.ObjectID)
	json.NewEncoder(response).Encode(salesInvoiceDetail)
}

// CreateManySalesInvoiceDetail func
func CreateManySalesInvoiceDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManySalesInvoiceDetail")
	var (
		collection          *mongo.Collection
		salesInvoiceDetails []model.SalesInvoiceDetail
	)
	collection = database.Database.Collection(setting.SalesInvoiceDetailTable)
	response.Header().Set("content-type", "application/json")

	err = json.NewDecoder(request.Body).Decode(&salesInvoiceDetails)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	currentTime := time.Now()
	for _, salesInvoiceDetail := range salesInvoiceDetails {
		salesInvoiceDetail.ID = primitive.NewObjectID()
		salesInvoiceDetail.CreatedDate = currentTime
		salesInvoiceDetail.UpdatedDate = currentTime
		ui = append(ui, salesInvoiceDetail)
	}
	_, err = collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// GetSalesInvoiceDetailByID godoc
// @Tags Sales Invoice Detail
// @Summary Get Sales Invoice Detail by Id
// @Description Get Sales Invoice Detail by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Invoice Detail need to be found"
// @Success 200 {object} model.SalesInvoiceDetail
// @Header 200 {string} Token "qwerty"
// @Router /SalesInvoiceDetail/{id} [get]
func GetSalesInvoiceDetailByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetsaleInvoiceDetailByID")
	var (
		collection         *mongo.Collection
		salesInvoiceDetail model.SalesInvoiceDetail
	)
	collection = database.Database.Collection(setting.SalesInvoiceDetailTable)

	response.Header().Set("content-type", "application/json")

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	err := collection.FindOne(ctx, filter).Decode(&salesInvoiceDetail)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesInvoiceDetail)
}

// GetSalesInvoiceDetails godoc
// @Tags Sales Invoice Detail
// @Summary Get all Sales Invoice Detail
// @Description Get all Sales Invoice Detail
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.SalesInvoiceDetail
// @Header 200 {string} Token "qwerty"
// @Router /SalesInvoiceDetail [get]
func GetSalesInvoiceDetails(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSalesInvoiceDetails")
	var (
		collection          *mongo.Collection
		salesInvoiceDetails []model.SalesInvoiceDetail
		findOptions         = options.Find()
		filter              = bson.M{}
	)
	collection = database.Database.Collection(setting.SalesInvoiceDetailTable)
	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions) //findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var saleInvoiceDetail model.SalesInvoiceDetail
		cursor.Decode(&saleInvoiceDetail)
		salesInvoiceDetails = append(salesInvoiceDetails, saleInvoiceDetail)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(salesInvoiceDetails)
}

// UpdateSalesInvoiceDetail godoc
// @Tags Sales Invoice Detail
// @Summary Update Sales Invoice Detail
// @Description Update Sales Invoice Detail
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Sales Invoice Detail need to be found"
// @Param body body model.AddSalesInvoiceDetail true "Update Sales Invoice Detail"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /SalesInvoiceDetail/{id} [put]
func UpdateSalesInvoiceDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdatesaleInvoiceDetail")
	var (
		collection        *mongo.Collection
		saleInvoiceDetail model.SalesInvoiceDetail
	)
	collection = database.Database.Collection(setting.SalesInvoiceDetailTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&saleInvoiceDetail)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	saleInvoiceDetail.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": saleInvoiceDetail}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

func DeleteSoftSalesInvoiceDetail(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftSalesInvoiceDetail")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.SalesInvoiceDetailTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
