package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../model/enum"
	"../service"
	"../setting"
	"github.com/jinzhu/copier"

	"../database"
	"../model"
	"./api_helper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateSalesOrderPaymentRefund godoc
// @Tags Sales Order PaymentRefund
// @Summary Add Sales Order PaymentRefund
// @Description Add Sales Order PaymentRefund
// @Accept  json
// @Produce  json
// @Param body body model.SalesOrderPaymentRefundRequest true "Add Sales Order PaymentRefund"
// @Success 200 {object} model.SalesOrderPaymentRefund
// @Header 200 {string} Token "qwerty"
// @Router /SalesOrderPaymentRefund [POST]
func CreateSalesOrderPaymentRefund(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateSalesOrderPaymentRefund")
	var (
		collection                 *mongo.Collection
		salesOrder                 model.SalesOrder
		returnObj                  model.Return
		salesOrderPaymentRefund    model.SalesOrderPaymentRefund
		salesOrderPaymentRefundReq model.SalesOrderPaymentRefundRequest
	)

	collection = database.Database.Collection(setting.SalesOrderPaymentRefundTable)
	response.Header().Set("content-type", "application/json")
	if err = json.NewDecoder(request.Body).Decode(&salesOrderPaymentRefundReq); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Sales Order Payment Capture ERROR - "+err.Error())
		return
	}

	if err = copier.Copy(&salesOrderPaymentRefund, &salesOrderPaymentRefundReq); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	currentTime := time.Now()
	salesOrderPaymentRefund.ID = primitive.NewObjectID()
	salesOrderPaymentRefund.Status = enum.PaymentStatusRefunded
	salesOrderPaymentRefund.CreatedDate = currentTime
	salesOrderPaymentRefund.UpdatedDate = currentTime

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err = collection.InsertOne(ctx, salesOrderPaymentRefund)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Sales Order Payment Capture ERROR - "+err.Error())
		return
	}

	filterSalesOrder := bson.M{"_id": bsonx.ObjectID(salesOrderPaymentRefund.DocKey)}
	updateSalesOrder := bson.M{"$set": bson.M{"payment_status": salesOrderPaymentRefund.Status}}
	if _, err = database.Database.Collection(setting.SalesOrderTable).UpdateOne(ctx, filterSalesOrder, updateSalesOrder); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Sales Order Payment Status ERROR - "+err.Error())
		return
	}

	if err = database.Database.Collection(setting.SalesOrderTable).FindOne(ctx, filterSalesOrder).Decode(&salesOrder); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Sales Order ERROR - "+err.Error())
		return
	}

	if _, err = service.HandleOrderStatusChange(salesOrder.DocNum, enum.OrderStatusReturned, false); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Sales Order Status ERROR - "+err.Error())
		return
	}

	//Find Return Doc num
	if err = database.Database.Collection(setting.ReturnTable).FindOne(ctx, bson.M{"_id": bsonx.ObjectID(salesOrderPaymentRefundReq.ReturnKey)}).Decode(&returnObj); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find Return Order ERROR - "+err.Error())
		return
	}

	//Update Return order status
	//if _, err = service.HandleReturnStatusChange(returnObj.DocNum,enum.ReturnStatusAccepted);err != nil {
	//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Update Return Order Status ERROR - "+err.Error())
	//	return
	//}

	//update Return payment status
	if _, err = service.HandleReturnPaymentStatusChange(returnObj.DocNum, string(enum.PaymentStatusRefunded)); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update Return Order Payment Status ERROR - "+err.Error())
		return
	}

	//if _, err = database.Database.Collection(setting.ReturnTable).UpdateOne(ctx, bson.M{"_id": bsonx.ObjectID(salesOrderPaymentRefundReq.ReturnKey)}, bson.M{"$set": bson.M{"doc_status": enum.ReturnStatusAccepted, "payment_status": enum.ReturnStatusRefunded}}); err != nil {
	//	api_helper.ErrorResponse(response, http.StatusInternalServerError,0, "Update Return Order status to [ACCEPTED] ERROR - "+err.Error())
	//	return
	//}

	data := bson.M{"sales_order_payment_capture": salesOrderPaymentRefund}
	api_helper.SuccessResponse(response, data, "Create Sales Order Payment Capture successfully")
}
