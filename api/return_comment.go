package api

import (
	"context"
	"encoding/json"
	"net/http"
	"os"
	"time"

	"../setting"
	"github.com/jinzhu/copier"

	"../database"
	"../model"
	"./api_helper"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CreateReturnComment godoc
// @Tags Return Order Comment
// @Summary Add Return Order Comment
// @Description Add Return Order Comment
// @Accept  json
// @Produce  json
// @Param body body model.ReturnCommentRequest true "Add Return Order Comment"
// @Success 200 {object} model.ReturnComment
// @Header 200 {string} Token "qwerty"
// @Router /ReturnComment [POST]
func CreateReturnComment(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateReturnComment")
	var (
		collection       *mongo.Collection
		returnComment    model.ReturnComment
		returnCommentReq model.ReturnCommentRequest
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ReturnCommentTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&returnCommentReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Return Order Comment Input ERROR - "+err.Error())
		return
	}

	_ = copier.Copy(&returnComment, &returnCommentReq)

	currentTime := time.Now()
	returnComment.ID = primitive.NewObjectIDFromTimestamp(currentTime)
	returnComment.ObjectType = setting.ReturnCommentTable
	returnComment.CreatedDate = currentTime
	returnComment.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, returnComment)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert Return Order Comment ERROR - "+err.Error())
		return
	}

	data := bson.M{"return_comment": returnComment}
	api_helper.SuccessResponse(response, data, "Create Return Order Comment successfully")
}

// CreateManyReturnComment func
func CreateManyReturnComment(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "CreateManyReturnComment")
	var (
		collection     *mongo.Collection
		returnComments []model.ReturnComment
	)
	collection = database.Database.Collection(setting.ReturnCommentTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&returnComments)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	currentTime := time.Now()
	for _, returnComment := range returnComments {
		returnComment.ID = primitive.NewObjectID()
		returnComment.CreatedDate = currentTime
		returnComment.UpdatedDate = currentTime
		ui = append(ui, returnComment)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

// GetReturnCommentByID godoc
// @Tags Return Order Comment
// @Summary Get Return Order Comment by Id
// @Description Get Return Order Comment by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Return Order Comment need to be found"
// @Success 200 {object} model.ReturnComment
// @Header 200 {string} Token "qwerty"
// @Router /ReturnComment/{id} [get]
func GetReturnCommentByID(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetSaleOrderCommentByID")
	var (
		collection    *mongo.Collection
		returnComment model.ReturnComment
	)
	collection = database.Database.Collection(setting.ReturnCommentTable)

	response.Header().Set("content-type", "application/json")

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	err := collection.FindOne(ctx, filter).Decode(&returnComment)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(returnComment)
}

// GetReturnComments godoc
// @Tags Return Order Comment
// @Summary Get all Return Order Comment
// @Description Get all Return Order Comment
// @Accept  json
// @Produce  json
// @Success 200 {object} []model.ReturnComment
// @Header 200 {string} Token "qwerty"
// @Router /ReturnComment [get]
func GetReturnComments(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "GetReturnComments")
	var (
		collection     *mongo.Collection
		returnComments []model.ReturnComment
		findOptions    = options.Find()
		filter         = bson.M{}
	)
	collection = database.Database.Collection(setting.ReturnCommentTable)

	response.Header().Set("content-type", "application/json")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	limit, err := api_helper.ParseParamStringToIntFromURLQuery(request, "limit", 300, 500)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	page, err := api_helper.ParseParamStringToIntFromURLQuery(request, "page", 1, 0)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions) //findOptions)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var returnComment model.ReturnComment
		cursor.Decode(&returnComment)
		returnComments = append(returnComments, returnComment)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(returnComments)
}

// UpdateReturnComment godoc
// @Tags Return Order Comment
// @Summary Update Return Order Comment
// @Description Update Return Order Comment
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Return need to be found"
// @Param body body model.ReturnRequest true "Update Return Order Comment"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /ReturnComment/{id} [put]
func UpdateReturnComment(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "UpdateSaleOrderComment")
	var (
		collection    *mongo.Collection
		returnComment model.ReturnComment
	)
	collection = database.Database.Collection(setting.ReturnCommentTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	err = json.NewDecoder(request.Body).Decode(&returnComment)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	returnComment.UpdatedDate = time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	update := bson.M{"$set": returnComment}
	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(result)
}

func DeleteSoftReturnComment(response http.ResponseWriter, request *http.Request) {
	defer api_helper.RecoverError(response, "DeleteSoftReturnComment")
	var (
		collection *mongo.Collection
	)
	collection = database.Database.Collection(setting.ReturnCommentTable)

	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": bson.M{"updated_date": time.Now()}}
	result, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		os.Exit(1)
	}
	json.NewEncoder(response).Encode(result)
}
