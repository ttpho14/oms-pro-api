package api

import (
	"context"
	"net/http"
	"time"

	"../database"
	"../model"
	"../setting"
	helper "./api_helper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Get number of APILogs godoc
// @Tags API Log
// @Summary Get number of APILogs sort by date field descending
// @Description Get number of APILogs depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.ApiLog
// @Header 200 {string} Token "qwerty"
// @Router /ApiLog [get]
func GetApiLogs(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "GetAPILogs")

	var (
		allApiLogs []model.ApiLog
		collection *mongo.Collection
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ApiLogTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := helper.GetLimitAndPage(request)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))

	cursor, err := collection.Find(ctx, bson.M{}, findOptions)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get API Logs ERROR - "+err.Error())
		return
	}

	if err = cursor.All(ctx, &allApiLogs); err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor API Logs ERROR - "+err.Error())
		return
	}

	data := bson.M{"api_log": allApiLogs}
	helper.SuccessResponse(response, data, "Get Api Logs successfully")
}

// Get Count of APILogs godoc
// @Tags API Log
// @Summary Get number of APILogs sort by date field descending
// @Description Get number of APILogs depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Success 200 {object} model.ApiLog
// @Header 200 {string} Token "qwerty"
// @Router /ApiLog/count [get]
func GetCountApiLog(response http.ResponseWriter, request *http.Request) {
	defer helper.RecoverError(response, "GetAPILogs")
	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.ApiLogTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	count, err := collection.CountDocuments(ctx, bson.M{})
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Count Api Logs ERROR - "+err.Error())
		return
	}

	data := bson.M{"count": count}
	helper.SuccessResponse(response, data, "Get Api Logs successfully")
}
