package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../database"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

// Add Bundle godoc
// @Tags Bundle
// @Summary Add Bundle
// @Description Add Bundle
// @Accept  json
// @Produce  json
// @Param body body model.BundleRequest true "Add Bundle"
// @Success 200 {object} model.Bundle
// @Header 200 {string} Token "qwerty"
// @Router /Bundle [POST]
func CreateBundle(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBundle")
	var (
		collection *mongo.Collection
		bundle     model.Bundle
		bundleReq  model.BundleRequest
	)

	response.Header().Set("content-type", "application/json")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	collection = database.Database.Collection(setting.BundleTable)
	err = json.NewDecoder(request.Body).Decode(&bundleReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&bundle, &bundleReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set Date = time.Now()
	currentTime := time.Now()
	bundle.ID = primitive.NewObjectID()
	bundle.CreatedDate = currentTime
	bundle.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, bundle)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	data := bson.M{"bundle": bundle}
	api_helper.SuccessResponse(response, data, "Create Bundle successfully")
}

// Get Bundle By Id godoc
// @Tags Bundle
// @Summary Get Bundle by Id
// @Description Get Bundle by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Bundle need to be found"
// @Success 200 {object} model.Bundle
// @Header 200 {string} Token "qwerty"
// @Router /Bundle/{id} [get]
func GetBundleById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetBundleById")
	var (
		collection *mongo.Collection
		bundle     model.Bundle
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.BundleTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err := collection.FindOne(ctx, filter).Decode(&bundle)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	data := bson.M{"bundle": bundle}
	api_helper.SuccessResponse(response, data, "Get Bundle Successfully")
}

// Get number of Bundles godoc
// @Tags Bundle
// @Summary Get number of Bundles sort by created_date field descending
// @Description Get number of Bundles depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.Bundle
// @Header 200 {string} Token "qwerty"
// @Router /Bundle [get]
func GetBundles(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetBundles")

	var (
		allBundle []model.Bundle
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.BundleTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{"is_deleted": bson.M{"$eq": false}}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))

	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var bundle model.Bundle
		_ = cursor.Decode(&bundle)
		allBundle = append(allBundle, bundle)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"bundle": allBundle}
	api_helper.SuccessResponse(response, data, "Get Bundle Successfully")
}

// Update Bundle godoc
// @Tags Bundle
// @Summary Update Bundle
// @Description Update Bundle
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Bundle need to be found"
// @Param body body model.BundleRequest true "Update Bundle"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Bundle/{id} [put]
func UpdateBundle(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var (
		collection *mongo.Collection
		bundle     model.Bundle
		bundleReq  model.BundleRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.BundleTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err = collection.FindOne(ctx, filter).Decode(&bundle)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = json.NewDecoder(request.Body).Decode(&bundleReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&bundle, &bundleReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set lai thoi gian luc update
	bundle.UpdatedDate = time.Now()

	update := bson.M{"$set": bundle}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"bundle": bundle}
	api_helper.SuccessResponse(response, data, "Update Bundle Successfully")
}

// Soft Delete Bundle godoc
// @Tags Bundle
// @Summary Soft Delete Bundle
// @Description Soft Delete Bundle
// @Accept  json
// @Produce  json
// @Param id path string true "Id of Bundle need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /Bundle/{id} [delete]
func DeleteSoftBundle(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.BundleTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	api_helper.SuccessResponse(response, id, "Delete Bundle successfully")
}

//-------------------- Unused -----------------------
func CreateManyBundle(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyBundle")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.BundleTable)
	response.Header().Set("content-type", "application/json")
	var manyBundle []model.Bundle
	err = json.NewDecoder(request.Body).Decode(&manyBundle)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, bundle := range manyBundle {
		bundle.ID = primitive.NewObjectID()
		bundle.CreatedDate = time.Now()
		bundle.UpdatedDate = time.Now()
		ui = append(ui, bundle)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}

//DeleteHardBundle
/*
func DeleteHardBundle(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.BundleTable)
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
