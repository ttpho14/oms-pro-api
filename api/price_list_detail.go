package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../database"
	"../setting"
	"./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

// Add PriceListDetail godoc
// @Tags PriceListDetail
// @Summary Add PriceListDetail
// @Description Add PriceListDetail
// @Accept  json
// @Produce  json
// @Param body body model.PriceListDetailRequest true "Add PriceListDetail"
// @Success 200 {object} model.PriceListDetail
// @Header 200 {string} Token "qwerty"
// @Router /PriceListDetail [POST]
func CreatePriceListDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreatePriceListDetail")
	var (
		collection      *mongo.Collection
		priceListDetail model.PriceListDetail
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.PriceListDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = json.NewDecoder(request.Body).Decode(&priceListDetail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode Input to Request Model ERROR - "+err.Error())
		return
	}
	//Set Date = time.Now()
	currentTime := time.Now()
	priceListDetail.ID = primitive.NewObjectID()
	priceListDetail.CreatedDate = currentTime
	priceListDetail.UpdatedDate = currentTime

	_, err := collection.InsertOne(ctx, priceListDetail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	data := bson.M{"price_list_detail": priceListDetail}
	api_helper.SuccessResponse(response, data, "Price List Detail successfully")
}

// Get PriceListDetail By Id godoc
// @Tags PriceListDetail
// @Summary Get PriceListDetail by Id
// @Description Get PriceListDetail by Id
// @Accept  json
// @Produce  json
// @Param id path string true "Id of PriceListDetail need to be found"
// @Success 200 {object} model.PriceListDetail
// @Header 200 {string} Token "qwerty"
// @Router /PriceListDetail/{id} [get]
func GetPriceListDetailById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetPriceListDetailById")
	var (
		collection      *mongo.Collection
		priceListDetail model.PriceListDetail
	)
	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.PriceListDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//_id = id and is_deleted = false
	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	err := collection.FindOne(ctx, filter).Decode(&priceListDetail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	data := bson.M{"bundle_detail": priceListDetail}
	api_helper.SuccessResponse(response, data, "Get Price List Detail successfully")
}

// Get number of PriceListDetails godoc
// @Tags PriceListDetail
// @Summary Get number of PriceListDetails sort by created_date field descending
// @Description Get number of PriceListDetails depends on Page number and Limit number of record in 1 page. Sort by created_date field descending
// @Accept  json
// @Produce  json
// @Param limit path int false "Limit number of record in one page. Maximum value is '500'. If missing, default value is '300'"
// @Param page path int false "Page number. If missing, default value is '1'"
// @Success 200 {object} []model.PriceListDetail
// @Header 200 {string} Token "qwerty"
// @Router /PriceListDetail [get]
func GetPriceListDetails(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "GetPriceListDetails")
	var (
		priceListDetail    model.PriceListDetail
		allPriceListDetail []model.PriceListDetail
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.PriceListDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := api_helper.GetLimitAndPage(request)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Limit or Page ERROR - "+err.Error())
		return
	}

	filter := bson.M{"is_deleted": false}
	findOptions := options.Find()
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))
	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		_ = cursor.Decode(&priceListDetail)
		allPriceListDetail = append(allPriceListDetail, priceListDetail)
	}
	if err := cursor.Err(); err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch Cursor ERROR - "+err.Error())
		return
	}

	data := bson.M{"price_list_detail": allPriceListDetail}
	api_helper.SuccessResponse(response, data, "Get Price List Detail successfully")
}

// Update PriceListDetail godoc
// @Tags PriceListDetail
// @Summary Update PriceListDetail
// @Description Update PriceListDetail
// @Accept  json
// @Produce  json
// @Param id path string true "Id of PriceListDetail need to be found"
// @Param body body model.PriceListDetailRequest true "Update PriceListDetail"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /PriceListDetail/{id} [put]
func UpdatePriceListDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateBrand")
	var (
		collection         *mongo.Collection
		priceListDetail    model.PriceListDetail
		priceListDetailReq model.PriceListDetailRequest
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.PriceListDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}

	err = collection.FindOne(ctx, filter).Decode(&priceListDetail)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	err = json.NewDecoder(request.Body).Decode(&priceListDetailReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	err = copier.Copy(&priceListDetail, &priceListDetailReq)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	//Set lai thoi gian luc update
	priceListDetail.UpdatedDate = time.Now()

	update := bson.M{"$set": priceListDetail}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"price_list_detail": priceListDetail}
	api_helper.SuccessResponse(response, data, "Update Price List Detail Successfully")
}

// Soft Delete PriceListDetail godoc
// @Tags PriceListDetail
// @Summary Soft Delete PriceListDetail
// @Description Soft Delete PriceListDetail
// @Accept  json
// @Produce  json
// @Param id path string true "Id of PriceListDetail need to be deleted"
// @Success 200 {object} model.UpdatedResponse
// @Header 200 {string} Token "qwerty"
// @Router /PriceListDetail/{id} [delete]
func DeleteSoftPriceListDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteSoftPriceListDetail")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.PriceListDetailTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": bson.M{"is_deleted": true, "updated_date": time.Now()}}

	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		api_helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	api_helper.SuccessResponse(response, id, "Delete Price List Detail Successfully")
}

//DeleteHardPriceListDetail
/*
func DeleteHardPriceListDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "DeleteHardPriceListDetail")
	var collection *mongo.Collection
	collection = database.Database.Collection("priceListDetail")
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	filter := bson.M{"_id": bson.M{"$eq": bsonx.ObjectID(id)}}
	result, err := collection.DeleteOne(context.Background(), filter)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("DeleteOne() result:", result)
		fmt.Println("DeleteOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("DeleteOne() result DeletedCount:", result.DeletedCount)
	}
	json.NewEncoder(response).Encode(result)
}
*/
//-------------------- Unused  -----------------------

func CreateManyPriceListDetail(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer api_helper.RecoverError(response, "CreateManyPriceListDetail")
	var collection *mongo.Collection
	collection = database.Database.Collection(setting.PriceListDetailTable)
	response.Header().Set("content-type", "application/json")
	var manyPriceListDetail []model.PriceListDetail
	err = json.NewDecoder(request.Body).Decode(&manyPriceListDetail)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	var ui []interface{}
	for _, priceListDetail := range manyPriceListDetail {
		priceListDetail.ID = primitive.NewObjectID()
		priceListDetail.CreatedDate = time.Now()
		priceListDetail.UpdatedDate = time.Now()
		ui = append(ui, priceListDetail)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(ui)
}
