package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"../database"
	"../service"
	"../setting"
	helper "./api_helper"
	"github.com/gorilla/mux"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"../model"
)

func CreateApplicationConfiguration(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "CreateApplicationConfiguration")
	var (
		collection                  *mongo.Collection
		applicationConfiguration    model.ApplicationConfiguration
		applicationConfigurationReq model.ApplicationConfigurationRequest
	)
	response.Header().Set("content-type", "application/json")

	// set collection
	collection = database.Database.Collection(setting.ApplicationConfigurationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Decode to Application Configuration Input
	err = json.NewDecoder(request.Body).Decode(&applicationConfigurationReq)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Decode ERROR - "+err.Error())
		return
	}

	// Copy Input to Main Model Application Configuration
	err = copier.Copy(&applicationConfiguration, &applicationConfigurationReq)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Copy ERROR - "+err.Error())
		return
	}

	currentDate := time.Now()
	applicationConfiguration.ID = primitive.NewObjectID()
	applicationConfiguration.CreatedDate = currentDate
	applicationConfiguration.UpdatedDate = currentDate

	_, err := collection.InsertOne(ctx, applicationConfiguration)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Insert ERROR - "+err.Error())
		return
	}

	// If Match document => Update and result
	data := bson.M{"application_configuration": applicationConfiguration}
	helper.SuccessResponse(response, data, "Insert Application Configuration successfully.")
}

func GetApplicationConfigurationById(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "GetApplicationConfigurationById")
	var (
		collection               *mongo.Collection
		applicationConfiguration model.ApplicationConfiguration
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ApplicationConfigurationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])

	//configurationId = id and is_deleted = false
	filter := bson.D{{"_id", bsonx.ObjectID(id)}, {"is_deleted", false}}

	err := collection.FindOne(ctx, filter).Decode(&applicationConfiguration)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}
	data := bson.M{"application_configuration": applicationConfiguration}
	helper.SuccessResponse(response, data, "Get Application Configuration successfully.")
}

func GetApplicationConfigurations(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "GetApplicationConfigurations")
	var (
		applicationConfiguration    model.ApplicationConfiguration
		allApplicationConfiguration []model.ApplicationConfiguration
		findOptions                 = options.Find()
	)

	response.Header().Set("content-type", "application/json")

	collection := database.Database.Collection(setting.ApplicationConfigurationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//Get Limit and Page
	limit, page, err := helper.GetLimitAndPage(request)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(response).Encode(bson.M{"message": err.Error()})
		return
	}

	filter := bson.D{{"is_deleted", false}}
	// Sort query with created date (-1) => desc , (1) => asc
	findOptions.SetSort(bson.D{{"created_date", -1}})
	// Set limit record of 1 page
	findOptions.SetLimit(limit)
	// Set page
	findOptions.SetSkip(limit * (page - 1))

	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Find ERROR - "+err.Error())
		return
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		cursor.Decode(&applicationConfiguration)
		allApplicationConfiguration = append(allApplicationConfiguration, applicationConfiguration)
	}
	if err := cursor.Err(); err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Fetch ERROR - "+err.Error())
		return
	}

	data := bson.M{"application_configuration": allApplicationConfiguration}
	helper.SuccessResponse(response, data, "Get Application Configurations successfully.")
}

func UpdateApplicationConfiguration(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "UpdateApplicationConfiguration")

	var (
		collection               *mongo.Collection
		applicationConfiguration model.ApplicationConfiguration
	)

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ApplicationConfigurationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	err = json.NewDecoder(request.Body).Decode(&applicationConfiguration)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
		return
	}
	//Set lai thoi gian luc update

	applicationConfiguration.UpdatedDate = time.Now()

	filter := bson.M{"_id": bsonx.ObjectID(id), "is_deleted": false}
	update := bson.M{"$set": applicationConfiguration}

	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Update ERROR - "+err.Error())
		return
	}

	data := bson.M{"application_configuration": applicationConfiguration}
	helper.SuccessResponse(response, data, "Update Application Configurations successfully.")
}

func GetApplicationConfigurationValue(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "GetApplicationConfigurations")

	response.Header().Set("content-type", "application/json")

	appId := request.URL.Query().Get("application_id")
	configId := request.URL.Query().Get("configuration_id")

	if appId == "" || configId == "" {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Application Id or Configuration Id is missing")
		return
	}

	configValue, err := service.GetApplicationConfigValue(appId, configId)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Get Application Configuration Value ERROR - "+err.Error())
		return
	}
	data := bson.M{
		"application_id":      appId,
		"configuration_id":    configId,
		"configuration_value": configValue,
	}
	helper.SuccessResponse(response, data, "Get Application Configurations Value successfully.")
}

func DeleteSoftApplicationConfiguration(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "DeleteSoftApplicationConfiguration")
	var collection *mongo.Collection

	response.Header().Set("content-type", "application/json")

	collection = database.Database.Collection(setting.ApplicationConfigurationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//var applicationConfiguration model.ApplicationConfiguration
	id, _ := primitive.ObjectIDFromHex(mux.Vars(request)["id"])
	//json.NewDecoder(request.Body).Decode(&applicationConfiguration)
	//Set lai thoi gian luc update
	//applicationConfiguration.UpdatedDate = time.Now()
	//applicationConfiguration.IsDeleted = true
	//ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	//defer cancel()
	filter := bson.D{{"_id", id}}
	update := bson.D{{"$set", bson.D{
		{"is_deleted", true},
		{"updated_date", time.Now()},
	}}}

	_, err := collection.UpdateOne(ctx, filter, update)
	// Check for error, else print the UpdateOne() API call results
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, "Delete ERROR - "+err.Error())
		return
	}

	helper.SuccessResponse(response, id, "Delete Application Configurations successfully.")
}

//-------------------- Unused -----------------------
func ImportApplicationConfiguration(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "CreateApplicationConfiguration")
	response.Header().Set("content-type", "application/json")
	var (
		collection                  *mongo.Collection
		applicationConfiguration    model.ApplicationConfiguration
		addApplicationConfiguration model.ApplicationConfigurationRequest
	)
	// Decode to Application Configuration Input
	err = json.NewDecoder(request.Body).Decode(&addApplicationConfiguration)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
		return
	}

	// set filter and upsert
	opts := options.Update().SetUpsert(true)
	filter := bson.D{
		{"application_key", addApplicationConfiguration.ApplicationKey},
		{"configuration_id", addApplicationConfiguration.ConfigurationId},
		{"is_deleted", false},
	}

	// set collection
	collection = database.Database.Collection(setting.ApplicationConfigurationTable)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Get value from DB and copy to Application Configuration
	_ = collection.FindOne(ctx, filter).Decode(&applicationConfiguration)

	// Copy Input to Main Model Application Configuration
	err = copier.Copy(&applicationConfiguration, &addApplicationConfiguration)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
		return
	}

	currentDate := time.Now()
	if primitive.ObjectID.IsZero(applicationConfiguration.ID) {
		applicationConfiguration.ID = primitive.NewObjectID()
		applicationConfiguration.CreatedDate = currentDate
	}

	applicationConfiguration.UpdatedDate = currentDate

	update := bson.D{{"$set", applicationConfiguration}}

	result, err := collection.UpdateOne(ctx, filter, update, opts)
	if err != nil {
		helper.ErrorResponse(response, http.StatusInternalServerError, 0, err.Error())
		return
	}

	// If Match document => Update and result
	data := bson.M{"application_configuration": applicationConfiguration}
	if result.MatchedCount != 0 {
		helper.SuccessResponse(response, data, "Match 1 document. Updated Application Configuration successfully.")
	} else { // else if MatchedCount = 0 => UpsertedCount != 0
		helper.SuccessResponse(response, data, "Match 0 document. Insert Application Configuration successfully.")
	}
}

func CreateManyApplicationConfiguration(response http.ResponseWriter, request *http.Request) {
	//handle error
	defer helper.RecoverError(response, "CreateManyApplicationConfiguration")
	var (
		collection                      *mongo.Collection
		manyAddApplicationConfiguration []model.ApplicationConfigurationRequest
		applicationConfiguration        model.ApplicationConfiguration
	)
	collection = database.Database.Collection(setting.ApplicationConfigurationTable)
	response.Header().Set("content-type", "application/json")
	err = json.NewDecoder(request.Body).Decode(&manyAddApplicationConfiguration)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(response).Encode(bson.M{"message": err.Error()})
		return
	}
	var ui []interface{}
	for _, addApplicationConfiguration := range manyAddApplicationConfiguration {
		err = copier.Copy(&applicationConfiguration, &addApplicationConfiguration)
		if err != nil {
			response.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(response).Encode(bson.M{"message": err.Error()})
			return
		}
		applicationConfiguration.ID = primitive.NewObjectID()
		applicationConfiguration.CreatedDate = time.Now()
		applicationConfiguration.UpdatedDate = time.Now()
		ui = append(ui, applicationConfiguration)
	}
	_, err := collection.InsertMany(context.TODO(), ui)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(response).Encode(bson.M{"message": err.Error()})
		return
	}
	json.NewEncoder(response).Encode(ui)
}
