package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func SiteAPIs(router *mux.Router, srv *server.Server) {
	var object = "Site"
	router.HandleFunc(path+object, validateToken(api.GetSites, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetSiteById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateSite, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateSite, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftSite, srv)).Methods("DELETE")
}

//------------------- Unused ---------------------------
//router.HandleFunc(path+object, validateToken(api.CreateManySite, srv)).Methods("POST")
//router.HandleFunc(path+"Site/DelHard/{id}", validateToken(api.DeleteHardTenant, srv)).Methods("DELETE")
