package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func CountryAPIs(router *mux.Router, srv *server.Server) {
	var object = "Country"
	router.HandleFunc(path+object, validateToken(api.GetCountries, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetCountryById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateCountry, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateCountry, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftCountry, srv)).Methods("DELETE")
}

//---------------- Unused ----------------------
//router.HandleFunc(path+object, validateToken(api.CreateManyCountry, srv)).Methods("POST")
