package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func SalesChannelAPIs(router *mux.Router, srv *server.Server) {
	var object = "SalesChannel"
	router.HandleFunc(path+object, validateToken(api.GetSalesChannels, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetSalesChannelById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateManySalesChannel, srv)).Methods("POST")
	//router.HandleFunc(path + object + "/CreateMany", validateToken(api.CreateManySalesChannel, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateSalesChannel, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftSalesChannel, srv)).Methods("DELETE")
	//router.HandleFunc(path+"Country/DelHard/{id}", validateToken(api.DeleteHardTenant, srv)).Methods("DELETE")
}
