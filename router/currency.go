package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func CurrencyAPIs(router *mux.Router, srv *server.Server) {
	var object = "Currency"
	router.HandleFunc(path+object, validateToken(api.GetCurrencies, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetCurrencyById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateCurrency, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateCurrency, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftCurrency, srv)).Methods("DELETE")
}
