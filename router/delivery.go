package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// DeliveryAPIs func
func DeliveryAPIs(router *mux.Router, srv *server.Server) {

	var object = "Delivery"

	router.HandleFunc(path+object, validateToken(api.GetDeliveries, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetDeliveryByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateManyDelivery, srv)).Methods("POST")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateDelivery, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftDelivery, srv)).Methods("DELETE")
}
