package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func ProductGroupAPIs(router *mux.Router, srv *server.Server) {
	var object = "ProductGroup"
	router.HandleFunc(path+object, validateToken(api.GetProductGroups, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetProductGroupById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateProductGroup, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateProductGroup, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftProductGroup, srv)).Methods("DELETE")
}

//------------------------ Unused -------------------------
//router.HandleFunc(path+object, validateToken(api.CreateManyProductGroup, srv)).Methods("POST")
