package router

import (
	"context"
	_ "crypto/x509"
	"encoding/json"
	_ "flag"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	apiHelper "../api/api_helper"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"../api"
	help "../api/api_helper"
	"../database"
	"../model"
	"../setting"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-session/session"
	"github.com/google/uuid"
	_ "github.com/google/uuid"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/go-oauth2/mongo.v3"
	"gopkg.in/oauth2.v3/errors"
	"gopkg.in/oauth2.v3/generates"
	"gopkg.in/oauth2.v3/manage"
	"gopkg.in/oauth2.v3/models"
	"gopkg.in/oauth2.v3/server"
	"gopkg.in/oauth2.v3/store"
)

var (
	path        string
	mongoConfig *mongo.Config
)

func InIt() {
	path = setting.BasePath
	mongoConfig = mongo.NewConfig(
		setting.MongodbUri,
		setting.Database,
	)
	router := mux.NewRouter().StrictSlash(true)
	//Swagger UI
	sh := http.StripPrefix("/swaggerui/", http.FileServer(http.Dir("./swaggerui/")))
	docswag := http.StripPrefix("/docs/", http.FileServer(http.Dir("./docs/")))
	router.PathPrefix("/swaggerui/").Handler(sh)
	router.PathPrefix("/docs/").Handler(docswag)
	srv := OAuth2InIt(router)
	APIs(router, srv)

	origins := handlers.AllowedOrigins([]string{"*"})
	headers := handlers.AllowedHeaders([]string{"X-Request-With", "Content-Type", "Authorization"})
	methods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE"})

	//-----------------------------------
	/*
		caCert, err := ioutil.ReadFile(setting.CertificateFilePath)
		if err != nil {
			log.Fatal(err)
		}
		caCertPool := x509.NewCertPool()
		caCertPool.AppendCertsFromPEM(caCert)

		// Create the TLS Config with the CA pool and enable Client certificate validation
		tlsConfig := &tls.Config{
			ClientCAs: caCertPool,
			ClientAuth: tls.RequireAndVerifyClientCert,
		}
		tlsConfig.BuildNameToCertificate()

		// Create a Server instance to listen on port 8443 with the TLS config
		server := &http.Server{
			Addr:      ":8080",
			TLSConfig: tlsConfig,
			Handler: handlers.CORS(headers, origins, methods)(router),
		}

		// Listen to HTTPS connections with the server certificate and wait

	*/

	log.Println("OMS Pro API - ", setting.Version)
	if setting.IsHttps == true { //----------- HTTPS ------------------------
		log.Println("Server is running on HTTPS : ", setting.ServerPort)
		log.Fatal(http.ListenAndServeTLS(":"+setting.ServerPort, setting.CertificateFilePath, setting.PrivateKeyFilePath, handlers.CORS(headers, origins, methods)(router))) //https Server
	} else { // HTTP
		log.Println("Server is running on HTTP : ", setting.ServerPort)
		log.Fatal(http.ListenAndServe(":"+setting.ServerPort, handlers.CORS(headers, origins, methods)(router))) //http
	}

	//log.Fatal(http.ListenAndServe(":"+setting.ServerPort, handlers.CORS(headers, origins, methods)(router)))
	//log.Fatal(http.ListenAndServe(setting.ServerPort, handlers.CORS(headers, origins, methods)(router))) //http
	//log.Fatal(http.ListenAndServeTLS(":8080","localhost.crt","localhost.key", router)) //https local
}

func APIs(router *mux.Router, srv *server.Server) {
	//Initialization
	InitializationAPIs(router, srv)
	DashboardAPIs(router, srv)
	ApplicationAPIs(router, srv)
	ApplicationConfigurationAPIs(router, srv)
	UserAPIs(router, srv)
	UserLicenseAPIs(router, srv)
	UserPasswordAPIs(router, srv)
	RegionAPIs(router, srv)
	TenantAPIs(router, srv)
	TenantSettingAPIs(router, srv)
	CountryAPIs(router, srv)
	SalesChannelAPIs(router, srv)
	BrandAPIs(router, srv)
	BrandRegistrationAPIs(router, srv)
	PriceListAPIs(router, srv)
	PriceListDetailAPIs(router, srv)
	CustomerGroupAPIs(router, srv)
	CustomerAPIs(router, srv)
	VendorGroupAPIs(router, srv)
	VendorAPIs(router, srv)
	UserBrandDetailAPIs(router, srv)
	UserDefineFieldAPIs(router, srv)
	SiteAPIs(router, srv)
	ReasonAPIs(router, srv)
	CurrencyAPIs(router, srv)
	AddressAPIs(router, srv)
	CategoryDetailAPIs(router, srv)
	TaxCodeAPIs(router, srv)
	BundleDetailAPIs(router, srv)
	BundleAPIs(router, srv)
	ProductVariantAPIs(router, srv)
	ProductMediaAPIs(router, srv)
	ProductAPIs(router, srv)
	ProductGroupAPIs(router, srv)
	PaymentTypeAPIs(router, srv)
	SecurityDetailAPIs(router, srv)
	SecurityRoleAPIs(router, srv)
	FreightAPIs(router, srv)
	CategoryAPIs(router, srv)

	//----------- Web Hook ---------------
	WebHookAPIs(router, srv)
	//---------------------------------------
	//Sales Order
	SalesOrderAPIs(router, srv)
	//SalesOrderDetailAPIs(router, srv)
	SalesOrderCommentAPIs(router, srv)
	SalesOrderPaymentCaptureAPIs(router, srv)
	SalesOrderPaymentRefundAPIs(router, srv)
	SalesOrderPaymentReverseAPIs(router, srv)
	//SalesOrderPackageAPIs(router, srv)
	//SalesOrderDiscountAPIs(router, srv)
	//SalesOrderShippingDetailAPIs(router, srv)
	//SalesOrderFreightAPIs(router, srv)
	//SalesOrderPaymentAPIs(router, srv)

	//Delivery
	DeliveryAPIs(router, srv)
	//DeliveryDetailAPIs(router, srv)
	//DeliveryFreightAPIs(router, srv)

	//Return
	ReturnAPIs(router, srv)
	ReturnCommentAPIs(router, srv)
	//ReturnShippingDetailAPIs(router, srv)
	//ReturnDetailAPIs(router, srv)
	//ReturnFreightAPIs(router, srv)

	//Order Cancellation
	OrderCancellationAPIs(router, srv)
	OrderCancellationCommentAPIs(router, srv)
	//OrderCancellationShippingDetailAPIs(router, srv)
	//OrderCancellationDetailAPIs(router, srv)

	//Sales Invoice
	SalesInvoiceAPIs(router, srv)
	//SalesInvoiceDetailAPIs(router, srv)
	//SalesInvoiceFreightAPIs(router, srv)

	//Inventory table
	InventoryStatusAPIs(router, srv)
	InventoryLogAPIs(router, srv)

	//Queue
	QueueAPIs(router, srv)

	//Api Log
	ApiLogsAPIs(router, srv)

	//Integration
	IntegrationFileAPIs(router, srv)
	IntegrationServiceAPIs(router, srv)
	IntegrationLogAPIs(router, srv)

	//router.HandleFunc("/login", loginHandler)
	router.HandleFunc("/auth", authHandler)
	router.HandleFunc("/authorize", func(w http.ResponseWriter, r *http.Request) {
		store, err := session.Start(nil, w, r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		var form url.Values
		if v, ok := store.Get("ReturnUri"); ok {
			form = v.(url.Values)
		}
		r.Form = form

		store.Delete("ReturnUri")
		store.Save()

		err = srv.HandleAuthorizeRequest(w, r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
	})
	router.HandleFunc("/token", func(w http.ResponseWriter, r *http.Request) {
		err := srv.HandleTokenRequest(w, r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})
	//
	//router.HandleFunc("/set_expired", func(w http.ResponseWriter, r *http.Request) {
	//	token, err := srv.ValidationBearerToken(r)
	//	if err != nil {
	//		apiHelper.ErrorResponse(w, http.StatusBadRequest, "Validate Token ERROR - "+err.Error())
	//		return
	//	}
	//
	//	srv.Manager.token
	//
	//	token.SetAccessExpiresIn(1*time.Second)
	//
	//	token, err = srv.ValidationBearerToken(r)
	//	if err != nil {
	//		apiHelper.ErrorResponse(w, http.StatusBadRequest, "Validate Token ERROR - "+err.Error())
	//		return
	//	}
	//
	//	w.Header().Set("content-type", "application/json")
	//	data := bson.M{"token": token}
	//
	//	json.NewEncoder(w).Encode(data)
	//})

	router.HandleFunc("/test", func(w http.ResponseWriter, r *http.Request) {
		token, err := srv.ValidationBearerToken(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		data := map[string]interface{}{
			"expires_in": int64(token.GetAccessCreateAt().Add(token.GetAccessExpiresIn()).Sub(time.Now()).Seconds()),
			"client_id":  token.GetClientID(),
			"user_id":    token.GetUserID(),
		}
		e := json.NewEncoder(w)
		e.SetIndent("", "  ")
		e.Encode(data)
	})
}

func OAuth2InIt(router *mux.Router) *server.Server {
	manager := manage.NewDefaultManager()
	manager.SetClientTokenCfg(manage.DefaultClientTokenCfg)
	manager.SetAuthorizeCodeTokenCfg(manage.DefaultAuthorizeCodeTokenCfg)
	manager.MustTokenStorage(store.NewMemoryTokenStore())
	manager.MapTokenStorage(mongo.NewTokenStore(mongoConfig))
	manager.MapAccessGenerate(generates.NewJWTAccessGenerate([]byte("00000000"), jwt.SigningMethodHS512))
	clientStore := mongo.NewClientStore(mongoConfig)
	manager.MapClientStorage(clientStore)
	manager.SetRefreshTokenCfg(manage.DefaultRefreshTokenCfg)
	srv := server.NewDefaultServer(manager)
	srv.SetAllowGetAccessRequest(true)
	srv.SetUserAuthorizationHandler(userAuthorizeHandler)
	srv.SetPasswordAuthorizationHandler(func(username, password string) (userID string, err error) {
		if username == "test" && password == "test" {
			userID = "test"
		}
		return
	})
	srv.SetClientInfoHandler(server.ClientFormHandler)
	srv.SetInternalErrorHandler(func(err error) (re *errors.Response) {
		log.Println("Internal Error: ", err.Error())
		return
	})
	srv.SetResponseErrorHandler(func(re *errors.Response) {
		log.Println("Response Error: ", re.Error.Error())
	})
	router.HandleFunc("/credentials", func(writer http.ResponseWriter, request *http.Request) {
		clientId := uuid.New().String()[:8]
		clientSecret := uuid.New().String()[:8]
		err := clientStore.Set(&models.Client{
			ID:     clientId,
			Secret: clientSecret,
			Domain: setting.ClientDomain,
		})
		if err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
			writer.Write([]byte(`{ "message": "` + err.Error() + `" }`))
			return
		}
	})

	return srv

}

func userAuthorizeHandler(w http.ResponseWriter, r *http.Request) (userID string, err error) {
	store, err := session.Start(nil, w, r)
	if err != nil {
		return
	}

	uid, ok := store.Get("UserID")
	if !ok {
		if r.Form == nil {
			r.ParseForm()
		}
		store.Set("ReturnUri", r.Form)
		store.Save()

		w.Header().Set("Location", "/login")
		w.WriteHeader(http.StatusFound)
		return
	}
	userID = uid.(string)
	store.Delete("UserID")
	store.Save()
	return
}
func loginHandler(w http.ResponseWriter, r *http.Request) {
	store, err := session.Start(nil, w, r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if r.Method == "POST" {
		//Handle User login here
		if r.Form == nil {
			err := r.ParseForm()
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}
		var result model.User
		collection := database.Database.Collection("user")
		userId := r.Form.Get("user_id")
		userPassword := r.Form.Get("user_password")
		err := collection.FindOne(context.TODO(), bson.D{{"user_id", userId}}).Decode(&result)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(`{ "message": "Invalid User name" }`))
			return
		}

		err = bcrypt.CompareHashAndPassword([]byte(result.Password), []byte(userPassword))
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(`{ "message": "Invalid Password" }`))
			return
		}
		//----------------------
		result.TenantKey = primitive.NewObjectID()
		store.Set("LoggedInUserID", userId) //userId
		store.Save()

		w.Header().Set("Location", "/auth")
		w.WriteHeader(http.StatusFound)
		return
	}
	outputHTML(w, r, "html/login.html")
}
func authHandler(w http.ResponseWriter, r *http.Request) {
	store, err := session.Start(nil, w, r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if _, ok := store.Get("LoggedInUserID"); !ok {
		w.Header().Set("Location", "/login")
		w.WriteHeader(http.StatusFound)
		return
	}

	if r.Method == "POST" {
		var form url.Values
		if v, ok := store.Get("ReturnUri"); ok {
			form = v.(url.Values)
		}
		u := new(url.URL)
		u.Path = "/authorize"
		u.RawQuery = form.Encode()
		w.Header().Set("Location", u.String())
		w.WriteHeader(http.StatusFound)
		store.Delete("Form")

		if v, ok := store.Get("LoggedInUserID"); ok {
			store.Set("UserID", v)
		}
		store.Save()

		return
	}
	outputHTML(w, r, "html/auth.html")
}

func outputHTML(w http.ResponseWriter, req *http.Request, filename string) {
	file, err := os.Open(filename)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	defer file.Close()
	fi, _ := file.Stat()
	http.ServeContent(w, req, file.Name(), fi.ModTime(), file)
}

func validateToken(f http.HandlerFunc, srv *server.Server) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("content-type", "application/json")
		accessToken, _ := srv.BearerAuth(r)

		if accessToken == setting.YchToken {
			f.ServeHTTP(w, r)
			return
		}

		token, err := srv.ValidationBearerToken(r)
		if err != nil {
			apiHelper.ErrorResponse(w, http.StatusBadRequest,0, "Validate Token ERROR - "+err.Error())
			return
		}

		setting.Token = token

		f.ServeHTTP(w, r)

		//application, err := apiHelper.GetApplicationFromToken(setting.Token.GetClientID())
		//if err != nil {
		//	apiHelper.ErrorResponse(w, http.StatusBadRequest, "Get Application From Token ERROR - "+err.Error())
		//	return
		//}
		//
		//if err = service.RouterHandleRequestAndResponse(f, w, r, application); err != nil {
		//	apiHelper.ErrorResponse(w, http.StatusBadRequest, "Handle Request and Response"+err.Error())
		//	return
		//}

	})
}

func ValidateInitialization(f http.HandlerFunc, srv *server.Server) http.HandlerFunc {
	return http.HandlerFunc(func(response http.ResponseWriter, request *http.Request) {
		accessToken, _ := srv.BearerAuth(request)

		if accessToken != setting.SystemSecretKey {
			response.Header().Set("content-type", "application/json")
			help.ErrorResponse(response, http.StatusInternalServerError, 0, "Permission is denied.")
			return
		}
		f.ServeHTTP(response, request)

		clientStore := mongo.NewClientStore(mongoConfig)
		clientId := api.GetClientId()
		clientSecret := api.GetSecretKey()
		clientStore.Set(&models.Client{
			ID:     clientId,
			Secret: clientSecret,
			Domain: "http://localhost:8080",
		})
	})
}

//create function use for create application with clientId and secretKey
func CreateApplication(f http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		f.ServeHTTP(w, r)
		clientStore := mongo.NewClientStore(mongoConfig)
		clientId := api.GetClientId()
		clientSecret := api.GetSecretKey()
		clientStore.Set(&models.Client{
			ID:     clientId,
			Secret: clientSecret,
			Domain: "http://localhost:8080",
		})
	})
}
