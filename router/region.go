package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func RegionAPIs(router *mux.Router, srv *server.Server) {
	var object = "Region"
	router.HandleFunc(path+object, validateToken(api.GetRegions, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetRegionById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateRegion, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateRegion, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftRegion, srv)).Methods("DELETE")
}

//--------------------- Unused ----------------------
//router.HandleFunc(path+object, validateToken(api.CreateManyRegion, srv)).Methods("POST")
