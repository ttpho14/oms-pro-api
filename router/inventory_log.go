package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func InventoryLogAPIs(router *mux.Router, srv *server.Server) {
	var object = "InventoryLog"
	router.HandleFunc(path+object+"/count", validateToken(api.GetCountInventoryLogs, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.GetInventoryLogs, srv)).Methods("GET")
	//router.HandleFunc(path+object, validateToken(api.CreateInventoryLog, srv)).Methods("POST")
}
