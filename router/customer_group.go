package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func CustomerGroupAPIs(router *mux.Router, srv *server.Server) {
	var object = "CustomerGroup"
	router.HandleFunc(path+object, validateToken(api.GetCustomerGroups, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetCustomerGroupById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateCustomerGroup, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateCustomerGroup, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftCustomerGroup, srv)).Methods("DELETE")
}

//-------------- Unused ------------------
//router.HandleFunc(path+object, validateToken(api.CreateManyCustomerGroup, srv)).Methods("POST")
