package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// DeliveryDetailAPIs func
func DeliveryDetailAPIs(router *mux.Router, srv *server.Server) {

	var object = "DeliveryDetail"

	router.HandleFunc(path+object, validateToken(api.GetDeliveryDetails, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetDeliveryDetailByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateManyDeliveryDetail, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateDeliveryDetail, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftDeliveryDetail, srv)).Methods("DELETE")
}
