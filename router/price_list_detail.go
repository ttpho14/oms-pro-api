package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func PriceListDetailAPIs(router *mux.Router, srv *server.Server) {
	var object = "PriceListDetail"
	router.HandleFunc(path+object, validateToken(api.GetPriceListDetails, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetPriceListDetailById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreatePriceListDetail, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdatePriceListDetail, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftPriceListDetail, srv)).Methods("DELETE")
}

//-------------- Unused -------------
//router.HandleFunc(path+object, validateToken(api.CreateManyPriceListDetail, srv)).Methods("POST")
