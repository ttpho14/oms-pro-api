package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func ApiLogsAPIs(router *mux.Router, srv *server.Server) {
	var object = "ApiLog"
	router.HandleFunc(path+object+"/count", validateToken(api.GetCountApiLog, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.GetApiLogs, srv)).Methods("GET")
}
