package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func ApplicationConfigurationAPIs(router *mux.Router, srv *server.Server) {
	var object = "ApplicationConfiguration"
	router.HandleFunc(path+object, validateToken(api.GetApplicationConfigurations, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetApplicationConfigurationById, srv)).Methods("GET")
	router.HandleFunc(path+object+"Value", validateToken(api.GetApplicationConfigurationValue, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateApplicationConfiguration, srv)).Methods("POST")
	//router.HandleFunc(path+"ApplicationConfiguration/CreateMany", validateToken(api.CreateManyApplicationConfiguration, srv)).Methods("POST")
	router.HandleFunc(path + object + "/{id}", validateToken(api.UpdateApplicationConfiguration, srv)).Methods("PUT")
	router.HandleFunc(path + object + "/{id}", validateToken(api.DeleteSoftApplicationConfiguration, srv)).Methods("DELETE")
}
