package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// SalesOrderDetailAPIs func
func SalesOrderDetailAPIs(router *mux.Router, srv *server.Server) {

	var object = "SalesOrderDetail"

	router.HandleFunc(path+object, validateToken(api.GetSalesOrderDetails, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetSalesOrderDetailByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateManySalesOrderDetail, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateSalesOrderDetail, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftSalesOrderDetail, srv)).Methods("DELETE")
}
