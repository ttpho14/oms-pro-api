package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func TaxCodeAPIs(router *mux.Router, srv *server.Server) {
	var object = "TaxCode"
	router.HandleFunc(path+object, validateToken(api.GetTaxCodes, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetTaxCodeById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateManyTaxCode, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateTaxCode, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftTaxCode, srv)).Methods("DELETE")
}
