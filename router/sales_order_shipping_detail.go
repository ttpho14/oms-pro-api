package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// SalesOrderShippingDetailAPIs func
func SalesOrderShippingDetailAPIs(router *mux.Router, srv *server.Server) {

	var object = "SalesOrderShippingDetail"

	router.HandleFunc(path+object, validateToken(api.GetSalesOrderShippingDetails, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetSalesOrderShippingDetailByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateManySalesOrderShippingDetail, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateSalesOrderShippingDetail, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftSalesOrderShippingDetail, srv)).Methods("DELETE")
}
