package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// SalesOrderDiscountAPIs func
func SalesOrderDiscountAPIs(router *mux.Router, srv *server.Server) {

	var object = "SalesOrderDiscount"

	router.HandleFunc(path+object, validateToken(api.GetSalesOrderDiscounts, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetSalesOrderDiscountByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateManySalesOrderDiscount, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateSalesOrderDiscount, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftSalesOrderDiscount, srv)).Methods("DELETE")
}
