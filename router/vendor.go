package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func VendorAPIs(router *mux.Router, srv *server.Server) {
	var object = "Vendor"
	router.HandleFunc(path+object, validateToken(api.GetVendors, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetVendorById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateManyVendor, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateVendor, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftVendor, srv)).Methods("DELETE")
}
