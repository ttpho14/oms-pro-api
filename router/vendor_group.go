package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func VendorGroupAPIs(router *mux.Router, srv *server.Server) {
	var object = "VendorGroup"
	router.HandleFunc(path+object, validateToken(api.GetVendorGroups, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetVendorGroupById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateManyVendorGroup, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateVendorGroup, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftVendorGroup, srv)).Methods("DELETE")
}
