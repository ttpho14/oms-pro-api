package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// OrderCancellationCommentAPIs func
func OrderCancellationCommentAPIs(router *mux.Router, srv *server.Server) {

	var object = "OrderCancellationComment"

	router.HandleFunc(path+object, validateToken(api.GetOrderCancellationComments, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetOrderCancellationCommentByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateOrderCancellationComment, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateOrderCancellationComment, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftOrderCancellationComment, srv)).Methods("DELETE")
}
//-------------- Unused --------------
//router.HandleFunc(path+object, validateToken(api.CreateManyOrderCancellationComment, srv)).Methods("POST")
