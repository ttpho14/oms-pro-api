package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// SalesOrderFreightAPIs func
func SalesOrderFreightAPIs(router *mux.Router, srv *server.Server) {

	var object = "SalesOrderFreight"

	router.HandleFunc(path+object, validateToken(api.GetSalesOrderFreights, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetSalesOrderFreightByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateManySalesOrderFreight, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateSalesOrderFreight, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftSalesOrderFreight, srv)).Methods("DELETE")
}
