package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func BrandRegistrationAPIs(router *mux.Router, srv *server.Server) {
	var object = "BrandRegistration"
	router.HandleFunc(path+object, validateToken(api.GetBrandRegistrations, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetBrandRegistrationById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateBrandRegistration, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateBrandRegistration, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftBrandRegistration, srv)).Methods("DELETE")
}
