package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// ReturnFreightAPIs func
func ReturnFreightAPIs(router *mux.Router, srv *server.Server) {

	var object = "ReturnFreight"

	router.HandleFunc(path+object, validateToken(api.GetReturnFreights, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetReturnFreightByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateReturnFreight, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateReturnFreight, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftDeliveryFreight, srv)).Methods("DELETE")
}

//-------------------------- Unused --------------------------------
//	router.HandleFunc(path+object, validateToken(api.CreateManyReturnFreight, srv)).Methods("POST")