package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// SalesOrderAPIs func
func SalesOrderAPIs(router *mux.Router, srv *server.Server) {
	var object = "SalesOrder"

	router.HandleFunc(path+object, validateToken(api.GetSalesOrders, srv)).Methods("GET")
	router.HandleFunc(path+"Dashboard", api.GetDashBoard).Methods("GET")
	router.HandleFunc(path+object+"/export", validateToken(api.ExportSalesOrders, srv)).Methods("GET")
	router.HandleFunc(path+object+"/count", validateToken(api.GetCountSalesOrder, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetSalesOrderByID, srv)).Methods("GET")
	router.HandleFunc(path+object+"/date/{date}", api.GetSalesOrdersByDate).Methods("GET")
	router.HandleFunc(path+object+"/doc_num/{doc_num}", validateToken(api.GetSalesOrderByDocNum, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateSalesOrder, srv)).Methods("POST")

	// Update Sales Order Status ==>>> Used by SALE IN ALL Integration Service
	router.HandleFunc(path+"Update"+object+"/{doc_num}", validateToken(api.UpdateSalesOrderStatus, srv)).Methods("PUT")
}

//----------------------------- Unused --------------------------------------
//router.HandleFunc(path+object+"TestView", validateToken(api.GetSalesOrdersTestView, srv)).Methods("GET")
//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftSalesOrder, srv)).Methods("DELETE")
//router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateSalesOrder, srv)).Methods("PUT")
