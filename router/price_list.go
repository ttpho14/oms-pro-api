package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func PriceListAPIs(router *mux.Router, srv *server.Server) {
	var object = "PriceList"
	router.HandleFunc(path+object, validateToken(api.GetPriceLists, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetPriceListById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreatePriceList, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdatePriceList, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftPriceList, srv)).Methods("DELETE")
	//----- Test -----
	//router.HandleFunc(path+"Query", validateToken(api.GetPriceListsWithQuery, srv)).Methods("GET")
}
//---------------------- Un Used -------------------------
//router.HandleFunc(path+object, validateToken(api.CreateManyPriceList, srv)).Methods("POST")
