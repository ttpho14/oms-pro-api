package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func InitializationAPIs(router *mux.Router, srv *server.Server) {
	var object = "Initialization"

	router.HandleFunc(path+object, ValidateInitialization(api.OmsInitialization, srv)).Methods("POST")
}
