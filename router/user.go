package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func UserAPIs(router *mux.Router, srv *server.Server) {
	var object = "User"
	//Login, Register, Change password, Reset password
	router.HandleFunc(path+object+"/login", api.UserLogin).Methods("POST")
	router.HandleFunc(path+object+"/logout", api.UserLogout).Methods("POST")
	router.HandleFunc(path+object+"/change_password", api.UserChangePassword).Methods("POST")
	router.HandleFunc(path+object+"/reset_password", api.UserResetPassword).Methods("POST")

	router.HandleFunc(path+object, validateToken(api.GetUsers, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetUserById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateUser, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateUser, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftUser, srv)).Methods("DELETE")

	//router.HandleFunc(path+"register", api.CreateUser).Methods("POST")
}
