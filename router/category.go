package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func CategoryAPIs(router *mux.Router, srv *server.Server) {
	var object = "Category"
	router.HandleFunc(path+object, validateToken(api.GetCategories, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetCategoryById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateCategory, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateCategory, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftCategory, srv)).Methods("DELETE")
}

// ------------------- Unused -----------------------
//router.HandleFunc(path+object, validateToken(api.CreateManyCategory, srv)).Methods("POST")
//router.HandleFunc(path+object+"/test", validateToken(api.CreateManyCategoryTest, srv)).Methods("POST")
