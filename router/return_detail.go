package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// ReturnDetailAPIs func
func ReturnDetailAPIs(router *mux.Router, srv *server.Server) {

	var object = "ReturnDetail"

	router.HandleFunc(path+object, validateToken(api.GetReturnDetails, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetReturnDetailByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateReturnDetail, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateReturnDetail, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftDeliveryDetail, srv)).Methods("DELETE")
}

//------------------- Unused ------------------------
//router.HandleFunc(path+object, validateToken(api.CreateManyReturnDetail, srv)).Methods("POST")
