package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// OrderCancellationDetailAPIs func
func OrderCancellationDetailAPIs(router *mux.Router, srv *server.Server) {

	var object = "OrderCancellationDetail"

	router.HandleFunc(path+object, validateToken(api.GetOrderCancellationDetails, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetOrderCancellationDetailByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateOrderCancellationDetail, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateOrderCancellationDetail, srv)).Methods("PUT")
}

//------------------- Unused -------------------
//router.HandleFunc(path+object, validateToken(api.CreateManyOrderCancellationDetail, srv)).Methods("POST")
