package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// SalesOrderAPIs func
func DashboardAPIs(router *mux.Router, srv *server.Server) {
	var object = "Dashboard"

	router.HandleFunc(path+object, api.GetDashBoard).Methods("GET")
	router.HandleFunc(path+object+"/search", api.GetGlobalSearch).Methods("GET")
}

