package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func IntegrationLogAPIs(router *mux.Router, srv *server.Server) {
	var object = "IntegrationLog"
	router.HandleFunc(path+object, validateToken(api.CreateIntegrationLog, srv)).Methods("POST")
	router.HandleFunc(path+object+"/count", validateToken(api.GetCountIntegrationLog, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.GetIntegrationLog, srv)).Methods("GET")
}

//------------------------ Unused -------------------------
//router.HandleFunc(path+object, validateToken(api.CreateManyProductGroup, srv)).Methods("POST")
