package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func PaymentTypeAPIs(router *mux.Router, srv *server.Server) {
	var object = "PaymentType"
	router.HandleFunc(path+object, validateToken(api.GetPaymentTypes, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetPaymentTypeById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreatePaymentType, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdatePaymentType, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftPaymentType, srv)).Methods("DELETE")
}

//---------------------- Unused ------------------------
//router.HandleFunc(path + object + "/CreateMany", validateToken(api.CreateManyPaymentType, srv)).Methods("POST")
