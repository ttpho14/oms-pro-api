package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func UserBrandDetailAPIs(router *mux.Router, srv *server.Server) {
	var object = "UserBrandDetail"
	router.HandleFunc(path+object, validateToken(api.GetUserBrandDetails, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetUserBrandDetailById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateManyUserBrandDetail, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateUserBrandDetail, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftUserBrandDetail, srv)).Methods("DELETE")
}
