package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func SecurityDetailAPIs(router *mux.Router, srv *server.Server) {
	var object = "SecurityDetail"
	router.HandleFunc(path+object, validateToken(api.GetSecurityDetails, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetSecurityDetailById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateManySecurityDetail, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateSecurityDetail, srv)).Methods("PUT")
	//router.HandleFunc(path + object + "/{id}", validateToken(api.DeleteSoftSecurityDetail, srv)).Methods("DELETE")
}
