package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// ReturnShippingDetailAPIs func
func ReturnShippingDetailAPIs(router *mux.Router, srv *server.Server) {

	var object = "ReturnShippingDetail"

	router.HandleFunc(path+object, validateToken(api.GetReturnShippingDetails, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetReturnShippingDetailByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateReturnShippingDetail, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateReturnShippingDetail, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftReturnShippingDetail, srv)).Methods("DELETE")
}
//-------------- Unused --------------
//router.HandleFunc(path+object, validateToken(api.CreateManyReturnShippingDetail, srv)).Methods("POST")
