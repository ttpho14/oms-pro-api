package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func BrandAPIs(router *mux.Router, srv *server.Server) {
	var object = "Brand"
	router.HandleFunc(path+object, validateToken(api.GetBrands, srv)).Methods("GET")
	//router.HandleFunc(path+object+"/test", validateToken(api.GetBrandsTest, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetBrandById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateBrand, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateBrand, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftBrand, srv)).Methods("DELETE")
}
