package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func BundleDetailAPIs(router *mux.Router, srv *server.Server) {
	var object = "BundleDetail"
	router.HandleFunc(path+object, validateToken(api.GetBundleDetails, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetBundleDetailById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateBundleDetail, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateBundleDetail, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftBundleDetail, srv)).Methods("DELETE")
}
