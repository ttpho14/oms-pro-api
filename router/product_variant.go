package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func ProductVariantAPIs(router *mux.Router, srv *server.Server) {
	var object = "ProductVariant"
	router.HandleFunc(path+object, validateToken(api.GetProductVariants, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetProductVariantById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateManyProductVariant, srv)).Methods("POST")
	//router.HandleFunc(path + object + "/CreateMany", validateToken(api.CreateManyProductVariant, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateProductVariant, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftProductVariant, srv)).Methods("DELETE")
}
