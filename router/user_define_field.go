package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func UserDefineFieldAPIs(router *mux.Router, srv *server.Server) {
	var object = "UserDefineField"
	router.HandleFunc(path+object, validateToken(api.GetUserDefineFields, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetUserDefineFieldById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateManyUserDefineField, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateUserDefineField, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftUserDefineField, srv)).Methods("DELETE")
}
