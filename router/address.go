package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func AddressAPIs(router *mux.Router, srv *server.Server) {
	var object = "Address"
	router.HandleFunc(path+object, validateToken(api.GetAddresses, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetAddressById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateAddress, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateAddress, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftAddress, srv)).Methods("DELETE")
}
