package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func UserLicenseAPIs(router *mux.Router, srv *server.Server) {
	var object = "UserLicense"

	router.HandleFunc(path+object+"/count", validateToken(api.GetCountUserLicenses, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.GetUserLicenses, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteUserLicense, srv)).Methods("DELETE")

	//router.HandleFunc(path+"register", api.CreateUserLicense).Methods("POST")
}
