package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func WebHookAPIs(router *mux.Router, srv *server.Server) {
	router.HandleFunc(path+"OrderPack", validateToken(api.UpdateOrderPack, srv)).Methods("PUT")
	router.HandleFunc(path+"Milestone", validateToken(api.UpdateMilestone, srv)).Methods("PUT")
	router.HandleFunc(path+"UpdateSalesOrderLabelUrl", validateToken(api.UpdateSalesOrderLabelUrl, srv)).Methods("PUT") // Web Hook for RCI
	router.HandleFunc(path+"WarehouseCancelOrder", validateToken(api.UpdateWarehouseCancelOrder, srv)).Methods("PUT")
}
