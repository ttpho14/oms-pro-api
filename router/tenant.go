package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func TenantAPIs(router *mux.Router, srv *server.Server) {
	var object = "Tenant"
	router.HandleFunc(path+object, validateToken(api.GetTenants, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetTenantById, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateTenant, srv)).Methods("PUT")
	router.HandleFunc(path+object, validateToken(api.CreateTenant, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftTenant, srv)).Methods("DELETE")
	//router.HandleFunc(path + object + "/DelHard/{id}", validateToken(api.DeleteHardTenant, srv)).Methods("DELETE")
}

//------------------------ Unused ------------------------------
//router.HandleFunc(path+object, validateToken(api.CreateManyTenant, srv)).Methods("POST")
