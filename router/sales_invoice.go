package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// SalesInvoiceAPIs func
func SalesInvoiceAPIs(router *mux.Router, srv *server.Server) {

	var object = "SalesInvoice"

	router.HandleFunc(path+object, validateToken(api.GetSalesInvoices, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetSalesInvoiceByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateManySalesInvoice, srv)).Methods("POST")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateSalesInvoice, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftSalesInvoice, srv)).Methods("DELETE")
}
