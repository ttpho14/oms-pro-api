package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// SalesOrderPaymentCaptureAPIs func
func SalesOrderPaymentRefundAPIs(router *mux.Router, srv *server.Server) {

	var object = "SalesOrderPaymentRefund"

	router.HandleFunc(path+object, validateToken(api.CreateSalesOrderPaymentRefund, srv)).Methods("POST")
}
