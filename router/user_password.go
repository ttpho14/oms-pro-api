package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func UserPasswordAPIs(router *mux.Router, srv *server.Server) {
	var object = "UserPassword"

	router.HandleFunc(path+object, validateToken(api.GetUserPasswords, srv)).Methods("GET")
}
