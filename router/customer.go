package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func CustomerAPIs(router *mux.Router, srv *server.Server) {
	var object = "Customer"
	router.HandleFunc(path+object, validateToken(api.GetCustomers, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetCustomerById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateCustomer, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateCustomer, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftCustomer, srv)).Methods("DELETE")
}

//----------------------------- Unused ----------------------------------
//router.HandleFunc(path+object, validateToken(api.CreateManyCustomer, srv)).Methods("POST")
