package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func ApplicationAPIs(router *mux.Router, srv *server.Server) {
	var object = "Application"
	router.HandleFunc(path+object, validateToken(api.GetApplications, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{key}", validateToken(api.GetApplicationByKey, srv)).Methods("GET")
	router.HandleFunc(path+object+"/id/{id}", validateToken(api.GetApplicationById, srv)).Methods("GET")
	router.HandleFunc(path+object, CreateApplication(api.CreateApplication)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateApplication, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftApplication, srv)).Methods("DELETE")
}

//--------------- Unused -----------------

