package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func IntegrationFileAPIs(router *mux.Router, srv *server.Server) {
	var object = "IntegrationFile"
	router.HandleFunc(path+object, validateToken(api.GetIntegrationFiles, srv)).Methods("GET")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.GetIntegrationFileById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateIntegrationFile, srv)).Methods("POST")
	router.HandleFunc(path+object+"/Update", validateToken(api.UpdateIntegrationFileStatus, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/UpdateInvalidProducts", validateToken(api.UpdateIntegrationFileInvalidProducts, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftIntegrationFile, srv)).Methods("DELETE")
}
