package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func QueueAPIs(router *mux.Router, srv *server.Server) {
	var object = "Queue"
	router.HandleFunc(path+object, validateToken(api.GetQueues, srv)).Methods("GET")
	//router.HandleFunc(path + object +"/{id}", validateToken(api.GetQueueById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateQueue, srv)).Methods("POST")
	router.HandleFunc(path+object+"/count", validateToken(api.GetCountQueue, srv)).Methods("GET")
	router.HandleFunc(path + object +"/{id}", validateToken(api.UpdateQueue, srv)).Methods("PUT")
	//router.HandleFunc(path + object +"/{id}", validateToken(api.DeleteSoftQueue, srv)).Methods("DELETE")
}
