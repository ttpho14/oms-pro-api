package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func TenantSettingAPIs(router *mux.Router, srv *server.Server) {
	var object = "TenantSetting"
	//router.HandleFunc(path+object, validateToken(api.GetTenantSettings, srv)).Methods("GET")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.GetTenantSettingById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateTenantSetting, srv)).Methods("POST")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateTenantSetting, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftTenantSetting, srv)).Methods("DELETE")
	//router.HandleFunc(path + object + "/DelHard/{id}", validateToken(api.DeleteHardTenantSetting, srv)).Methods("DELETE")
}
