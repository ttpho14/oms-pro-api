package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// SalesOrderPaymentAPIs func
func SalesOrderPaymentAPIs(router *mux.Router, srv *server.Server) {

	var object = "SalesOrderPayment"

	router.HandleFunc(path+object, validateToken(api.GetSalesOrderPayments, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetSalesOrderPaymentByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateManySalesOrderPayment, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateSalesOrderPayment, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftSalesOrderPayment, srv)).Methods("DELETE")
}
