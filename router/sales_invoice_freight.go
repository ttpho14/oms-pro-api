package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// SalesInvoiceFreightAPIs func
func SalesInvoiceFreightAPIs(router *mux.Router, srv *server.Server) {

	var object = "SalesInvoiceFreight"

	router.HandleFunc(path+object, validateToken(api.GetSalesInvoiceFreights, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetSalesInvoiceFreightByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateManySalesInvoiceFreight, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateSalesInvoiceFreight, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftSalesInvoiceFreight, srv)).Methods("DELETE")
}
