package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// SalesOrderPackageAPIs func
func SalesOrderPackageAPIs(router *mux.Router, srv *server.Server) {

	var object = "SalesOrderPackage"

	router.HandleFunc(path+object, validateToken(api.GetSalesOrderPackages, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetSalesOrderPackageByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateSalesOrderPackage, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateSalesOrderPackage, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftSalesOrderPackage, srv)).Methods("DELETE")
}
//-------------- Unused --------------
//router.HandleFunc(path+object, validateToken(api.CreateManySalesOrderPackage, srv)).Methods("POST")
