package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// SalesOrderPaymentCaptureAPIs func
func SalesOrderPaymentCaptureAPIs(router *mux.Router, srv *server.Server) {

	var object = "SalesOrderPaymentCapture"

	router.HandleFunc(path+object, validateToken(api.CreateSalesOrderPaymentCapture, srv)).Methods("POST")
}
