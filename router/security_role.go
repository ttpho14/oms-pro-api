package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func SecurityRoleAPIs(router *mux.Router, srv *server.Server) {
	var object = "SecurityRole"
	router.HandleFunc(path+object, validateToken(api.GetSecurityRoles, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetSecurityRoleByKey, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateSecurityRole, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateSecurityRole, srv)).Methods("PUT")
	//router.HandleFunc(path + object + "/{id}", validateToken(api.DeleteSoftSecurityRole, srv)).Methods("DELETE")
}
