package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// ReturnAPIs func
func ReturnAPIs(router *mux.Router, srv *server.Server) {

	var object = "Return"

	router.HandleFunc(path+object, validateToken(api.GetReturns, srv)).Methods("GET")
	router.HandleFunc(path+object+"/export", validateToken(api.ExportReturns, srv)).Methods("GET")
	router.HandleFunc(path+object+"/doc_num/{doc_num}", validateToken(api.GetReturnByDocNum, srv)).Methods("GET")
	router.HandleFunc(path+object+"/count", validateToken(api.GetCountReturn, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetReturnByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateReturn, srv)).Methods("POST")
	router.HandleFunc(path+"Update"+object, validateToken(api.UpdateReturnStatusByBaseDocNum, srv)).Methods("PUT")
	router.HandleFunc(path+"Update"+object+"/{docNum}", validateToken(api.UpdateReturnStatus, srv)).Methods("PUT")
	//router.HandleFunc(path+"Update"+object+"/doc_num/{doc_num}", validateToken(api.UpdateReturnStatusByDocNum, srv)).Methods("PUT")
}

//------------------- Unused --------------------
//router.HandleFunc(path+object, validateToken(api.CreateManyReturn, srv)).Methods("POST")
//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftDelivery, srv)).Methods("DELETE")
//router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateReturn, srv)).Methods("PUT")
