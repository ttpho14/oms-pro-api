package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func ProductAPIs(router *mux.Router, srv *server.Server) {
	var object = "Product"
	router.HandleFunc(path+object, validateToken(api.GetProducts, srv)).Methods("GET")
	router.HandleFunc(path+object+"/count", validateToken(api.GetProductsCount, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{key}", validateToken(api.GetProductByKey, srv)).Methods("GET")
	router.HandleFunc(path+object+"/product_id/{prodId}", validateToken(api.GetProductByProductId, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateProduct, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftProduct, srv)).Methods("DELETE")
}
