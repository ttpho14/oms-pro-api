package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// OrderCancellationAPIs func
func OrderCancellationAPIs(router *mux.Router, srv *server.Server) {

	var object = "OrderCancellation"

	router.HandleFunc(path+object, validateToken(api.GetOrderCancellations, srv)).Methods("GET")
	router.HandleFunc(path+object+"/export", validateToken(api.ExportOrderCancellation, srv)).Methods("GET")
	router.HandleFunc(path+object+"/count", validateToken(api.GetCountOrderCancellation, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetOrderCancellationByID, srv)).Methods("GET")
	router.HandleFunc(path+object+"/doc_num/{doc_num}", validateToken(api.GetOrderCancellationByDocNum, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateOrderCancellation, srv)).Methods("POST")
}

//-------------------- Unused -------------------------
//router.HandleFunc(path+object, validateToken(api.CreateManyOrderCancellation, srv)).Methods("POST")
