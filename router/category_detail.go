package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func CategoryDetailAPIs(router *mux.Router, srv *server.Server) {
	var object = "CategoryDetail"
	router.HandleFunc(path+object, validateToken(api.GetCategoryDetails, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetCategoryDetailById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateCategoryDetail, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateCategoryDetail, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftCategoryDetail, srv)).Methods("DELETE")
}

//---------------- Unused ----------------------
//router.HandleFunc(path+object, validateToken(api.CreateManyCategoryDetail, srv)).Methods("POST")
