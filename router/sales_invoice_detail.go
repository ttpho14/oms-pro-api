package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// SalesInvoiceDetailAPIs func
func SalesInvoiceDetailAPIs(router *mux.Router, srv *server.Server) {

	var object = "SalesInvoiceDetail"

	router.HandleFunc(path+object, validateToken(api.GetSalesInvoiceDetails, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetSalesInvoiceDetailByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateManySalesInvoiceDetail, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateSalesInvoiceDetail, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftSalesInvoiceDetail, srv)).Methods("DELETE")
}
