package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func FreightAPIs(router *mux.Router, srv *server.Server) {
	var object = "Freight"
	router.HandleFunc(path+object, validateToken(api.GetFreights, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetFreightById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateFreight, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateFreight, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftFreight, srv)).Methods("DELETE")
}

//--------------------------- Unused ----------------------------------
//router.HandleFunc(path+"Freight/CreateMany", validateToken(api.CreateManyFreight, srv)).Methods("POST")
//router.HandleFunc(path+"Freight/DelHard/{id}", validateToken(api.DeleteHardTenant, srv)).Methods("DELETE")
