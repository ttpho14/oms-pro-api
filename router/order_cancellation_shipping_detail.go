package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// OrderCancellationShippingDetailAPIs func
func OrderCancellationShippingDetailAPIs(router *mux.Router, srv *server.Server) {

	var object = "OrderCancellationShippingDetail"

	router.HandleFunc(path+object, validateToken(api.GetOrderCancellationShippingDetails, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetOrderCancellationShippingDetailByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateOrderCancellationShippingDetail, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateOrderCancellationShippingDetail, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftOrderCancellationShippingDetail, srv)).Methods("DELETE")
}
//-------------- Unused --------------
//router.HandleFunc(path+object, validateToken(api.CreateManyOrderCancellationShippingDetail, srv)).Methods("POST")
