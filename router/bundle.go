package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func BundleAPIs(router *mux.Router, srv *server.Server) {
	var object = "Bundle"
	router.HandleFunc(path+object, validateToken(api.GetBundles, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetBundleById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateBundle, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateBundle, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftBundle, srv)).Methods("DELETE")
}
