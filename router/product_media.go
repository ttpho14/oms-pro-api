package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func ProductMediaAPIs(router *mux.Router, srv *server.Server) {
	var object = "ProductMedia"
	router.HandleFunc(path+object, validateToken(api.GetProductMedias, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetProductMediaById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateProductMedia, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateProductMedia, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftProductMedia, srv)).Methods("DELETE")
}

//----------------------- Unused -------------------------------
//router.HandleFunc(path+object, validateToken(api.CreateManyProductMedia, srv)).Methods("POST")
