package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func InventoryStatusAPIs(router *mux.Router, srv *server.Server) {
	var object = "InventoryStatus"
	router.HandleFunc(path+object+"/export", validateToken(api.ExportInventoryStatus, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.GetInventoryStatus, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetInventoryStatusById, srv)).Methods("GET")
	router.HandleFunc(path+object+"/site/{site_id}/product/{product_id}", validateToken(api.GetInventoryStatusByProductAndSite, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.UpdateInventoryStatus, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/recalculate", validateToken(api.RecalculateQuantity, srv)).Methods("PUT")
}

//---------------- Unused ---------------------
//router.HandleFunc(path+"InventoryStatus/CreateMany", validateToken(api.CreateManyInventoryStatus, srv)).Methods("POST")
//router.HandleFunc(path + object + "/{id}", validateToken(api.UpdateInventoryStatus, srv)).Methods("PUT")
//router.HandleFunc(path + object + "/{id}", validateToken(api.DeleteSoftInventoryStatus, srv)).Methods("DELETE")
