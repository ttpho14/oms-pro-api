package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// SalesOrderCommentAPIs func
func SalesOrderCommentAPIs(router *mux.Router, srv *server.Server) {

	var object = "SalesOrderComment"

	router.HandleFunc(path+object, validateToken(api.GetSalesOrderComments, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetSalesOrderCommentByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateSalesOrderComment, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateSalesOrderComment, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftSalesOrderComment, srv)).Methods("DELETE")
}
//-------------- Unused --------------
//router.HandleFunc(path+object, validateToken(api.CreateManySalesOrderComment, srv)).Methods("POST")
