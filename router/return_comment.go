package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// ReturnCommentAPIs func
func ReturnCommentAPIs(router *mux.Router, srv *server.Server) {

	var object = "ReturnComment"

	router.HandleFunc(path+object, validateToken(api.GetReturnComments, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetReturnCommentByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateReturnComment, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateReturnComment, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftReturnComment, srv)).Methods("DELETE")
}
//-------------- Unused --------------
//router.HandleFunc(path+object, validateToken(api.CreateManyReturnComment, srv)).Methods("POST")
