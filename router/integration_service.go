package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func IntegrationServiceAPIs(router *mux.Router, srv *server.Server) {
	var object = "IntegrationService"
	router.HandleFunc(path+object, validateToken(api.GetIntegrationServiceInfo, srv)).Methods("GET")
	router.HandleFunc(path+object+"/command", validateToken(api.IntegrationServiceCommand, srv)).Methods("POST")
}

//------------------------ Unused -------------------------
//router.HandleFunc(path+object, validateToken(api.CreateManyProductGroup, srv)).Methods("POST")
