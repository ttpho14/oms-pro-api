package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

func ReasonAPIs(router *mux.Router, srv *server.Server) {
	var object = "Reason"
	router.HandleFunc(path+object, validateToken(api.GetReasons, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetReasonById, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateReason, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateReason, srv)).Methods("PUT")
	router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftReason, srv)).Methods("DELETE")
}

//------------------------------------ Unused --------------------------------------------
//router.HandleFunc(path+object, validateToken(api.CreateManyReason, srv)).Methods("POST")
