package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// SalesOrderPaymentCaptureAPIs func
func SalesOrderPaymentReverseAPIs(router *mux.Router, srv *server.Server) {

	var object = "SalesOrderPaymentReverse"

	router.HandleFunc(path+object, validateToken(api.CreateSalesOrderPaymentReverse, srv)).Methods("POST")
}
