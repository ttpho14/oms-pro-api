package router

import (
	"../api"
	"github.com/gorilla/mux"
	"gopkg.in/oauth2.v3/server"
)

// DeliveryFreightAPIs func
func DeliveryFreightAPIs(router *mux.Router, srv *server.Server) {

	var object = "DeliveryFreight"

	router.HandleFunc(path+object, validateToken(api.GetDeliveryFreights, srv)).Methods("GET")
	router.HandleFunc(path+object+"/{id}", validateToken(api.GetDeliveryFreightByID, srv)).Methods("GET")
	router.HandleFunc(path+object, validateToken(api.CreateManyDeliveryFreight, srv)).Methods("POST")
	router.HandleFunc(path+object+"/{id}", validateToken(api.UpdateDeliveryFreight, srv)).Methods("PUT")
	//router.HandleFunc(path+object+"/{id}", validateToken(api.DeleteSoftDeliveryFreight, srv)).Methods("DELETE")
}
