#!/usr/bin/expect
# shellcheck disable=SC2016
version=V79
service=OMS.Pro.API
user=root
#host=54.179.207.184            #PRODUCTION
#pwd='Oms@123'
#host=207.148.119.44            #DEV
#pwd='$b3L!VY${PfR.@Ka'
host=45.32.120.165              #TEST
pwd='bY{9A%Gw+u[6N7Wf'
fileBuilt=$service.$version.$(date +"%Y%m%d%I%M%S%p")
mainGoPath='C:\Go\'$service'\main.go'
builtPath='C:\Go\'$service'\'$fileBuilt
commandsFile='C:\Go\'$service'\deploy_commands.txt'
apiFolder=/usr/OMS_Services/$service

GOOS=linux GOARCH=amd64 go build -o "$builtPath" $mainGoPath

pscp -pw "$pwd" "$builtPath" $user@$host:$apiFolder/oldAPI

#copy from old api to main api
plink -v -ssh $user@$host -pw "$pwd" "cp $apiFolder/oldAPI/$fileBuilt $apiFolder"

#stop service
plink -v -ssh $user@$host -pw "$pwd" "sudo systemctl stop $service"

#Rename API file
# shellcheck disable=SC2086
plink -v -ssh $user@$host -pw $pwd "mv $apiFolder/$fileBuilt $apiFolder/$service"

#Run multiples commands permission
plink -v -ssh $user@$host -pw "$pwd" -m $commandsFile

#
read -t 10 -p "I am going to wait for 10 seconds only ..."